﻿
using Newtonsoft.Json;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using UtiltiyVM;

/// <summary>
/// Summary description for ChatBotAIService
/// </summary>
public class CopilotService
{
    SubSonic.StoredProcedure sp;
    string DatabaseURL;
    int ErrorSts = 0;
    //Reddy And Amit AI

    //HDModel mn = new HDModel();
    //mn.CompanyId = Tenant.CompanyId;
    //string DB = _ReturnDB(mn);
    //return DB;

    public string _ReturnDB(CPModel Tenant)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "CHECK_USER_TENANT");
            sp.Command.AddParameter("@TENANT_NAME", Tenant.CompanyId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                DatabaseURL = ds.Tables[0].Rows[0]["TENANT_ID"].ToString();
                HttpContext.Current.Session["useroffset"] = ds.Tables[0].Rows[0]["OFFSET"].ToString();
                if (ds.Tables[0].Rows[0]["TENANT_ID"].ToString() == "" && ds.Tables[0].Rows[0]["OFFSET"].ToString() == "")
                { return (string)null; }
                else if (ds.Tables[0].Rows.Count == 0)
                { return (string)null; }
                else
                { return DatabaseURL; }
            }
            else { return MessagesVM.Err; }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }

    public object GetMenu(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_MENU]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            DataSet ds = sp.GetDataSet();
            return ds.Tables[0].AsEnumerable().Select(x => x.Field<string>(0)).ToList();
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object GetSubMenu(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_SUB_MENU]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@MENU", Model.Name, DbType.String);
            DataSet ds = sp.GetDataSet();
            return ds.Tables[0].AsEnumerable().Select(x => x.Field<string>(0)).ToList();
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object GetMenuLinks(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_MENU_LINKS]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@MENU", Model.Name, DbType.String);
            DataSet ds = sp.GetDataSet();
            return ds.Tables[0].AsEnumerable().Select(x => x.Field<string>(0)).ToList();
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }

    public object UserSapceHistoryDetails(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Book Seat", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_RECENT_BOOKING_HISTORY_AI]");
                sp.Command.Parameters.Add("@AUR_ID", Model.UserId, DbType.String);
                ds = sp.GetDataSet();
                if (ds.Tables[0].Rows[0]["SeatBooking"].ToString() == "Already you having today bookings")
                {
                    return new
                    {
                        SPC_ID = "",
                        SSA_SRNREQ_ID = "",
                        SSAD_SRN_REQ_ID = "",
                        SH_CODE = "",
                        Cost_Center_Code = "",
                        VERTICAL = "",
                        AUR_ID = "",
                        STATUS = 0,
                        FROM_TIME = "",
                        TO_TIME = "",
                        STACHECK = 0,
                        ticked = false,
                        SHIFT_TYPE = "",
                        SeatBooking = "",
                        FROM_DATE = "",
                        TO_DATE = "",
                        Status = 2,
                        Message = "Seat Already Booked for Today"
                    };
                }

                var row = ds.Tables[0].Rows[0];
                var obj = new
                {
                    SPC_ID = row["SPC_ID"],
                    SSA_SRNREQ_ID = row["SSA_SRNREQ_ID"],
                    SSAD_SRN_REQ_ID = row["SSAD_SRN_REQ_ID"],
                    SH_CODE = row["SH_CODE"],
                    Cost_Center_Code = row["Cost_Center_Code"],
                    VERTICAL = row["VERTICAL"],
                    AUR_ID = row["AUR_ID"],
                    STATUS = row["STATUS"],
                    FROM_TIME = row["FROM_TIME"],
                    TO_TIME = row["TO_TIME"],
                    STACHECK = row["STACHECK"],
                    ticked = row["ticked"],
                    SHIFT_TYPE = row["SHIFT_TYPE"],
                    SeatBooking = row["SeatBooking"],
                    FROM_DATE = row["FROM_DATE"],
                    TO_DATE = row["TO_DATE"],
                    Status = res,
                    Message = MessagesVM.AF_OK
                };
                return obj;
            }
            else
            {
                return new
                {
                    SPC_ID = "",
                    SSA_SRNREQ_ID = "",
                    SSAD_SRN_REQ_ID = "",
                    SH_CODE = "",
                    Cost_Center_Code = "",
                    VERTICAL = "",
                    AUR_ID = "",
                    STATUS = 0,
                    FROM_TIME = "",
                    TO_TIME = "",
                    STACHECK = 0,
                    ticked = false,
                    SHIFT_TYPE = "",
                    SeatBooking = "",
                    FROM_DATE = "",
                    TO_DATE = "",
                    Status = 0,
                    Message = ds1.Tables[0].Rows[0][1]
                };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }

    public object UserSelectSapceDetails(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Book Seat", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                // Assuming sp is declared somewhere in your code.
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_RECENT_BOOKING_HISTORY3_AI]");
                sp.Command.Parameters.Add("@AUR_ID", Model.UserId, DbType.String);
                ds = sp.GetDataSet();
                var Table = new List<object>();
                Table.Add(new
                {
                    AUR_ID = "",
                    Cost_Center_Code = "",
                    FROM_DATE = "",
                    FROM_TIME = "",
                    SeatBooking = "",
                    SH_CODE = "",
                    SHIFT_TYPE = "",
                    SPC_ID = "",
                    SSA_SRNREQ_ID = "",
                    SSAD_SRN_REQ_ID = "",
                    STACHECK = 0,
                    STATUS = 0,
                    ticked = false,
                    TO_DATE = "",
                    TO_TIME = "",
                    VERTICAL = ""
                });
                if (ds.Tables[0].Rows[0]["SeatBooking"].ToString() == "Already you having today bookings")
                {
                    return new { Status = 2, Message = "Seat Already Booked for Today", Data = new { Table = Table } };
                }

                var dataList = ds.Tables[0].AsEnumerable().Select(row => row.Table.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => row[col])).ToList();

                return new { Status = res, Message = MessagesVM.AF_OK, Data = new { Table = dataList } };
            }
            else
            {
                var Table = new List<object>();
                Table.Add(new
                {
                    AUR_ID = "",
                    Cost_Center_Code = "",
                    FROM_DATE = "",
                    FROM_TIME = "",
                    SeatBooking = "",
                    SH_CODE = "",
                    SHIFT_TYPE = "",
                    SPC_ID = "",
                    SSA_SRNREQ_ID = "",
                    SSAD_SRN_REQ_ID = "",
                    STACHECK = 0,
                    STATUS = 0,
                    ticked = false,
                    TO_DATE = "",
                    TO_TIME = "",
                    VERTICAL = ""
                });
                return new
                {
                    Status = res,
                    Message = ds1.Tables[0].Rows[0][1].ToString(),
                    Data = new { Table = Table }
                };
            }
        }
        catch (Exception ex)
        {
            var Table = new List<object>();
            Table.Add(new
            {
                AUR_ID = "",
                Cost_Center_Code = "",
                FROM_DATE = "",
                FROM_TIME = "",
                SeatBooking = "",
                SH_CODE = "",
                SHIFT_TYPE = "",
                SPC_ID = "",
                SSA_SRNREQ_ID = "",
                SSAD_SRN_REQ_ID = "",
                STACHECK = 0,
                STATUS = 0,
                ticked = false,
                TO_DATE = "",
                TO_TIME = "",
                VERTICAL = ""
            });
            return new
            {
                Status = ErrorSts,
                Message = MessagesVM.UM_NO_REC,
                Data = new { Table = Table }
            };
        }
    }

    //public object ShiftDetailsBySpaceID(CPModel Model)
    //{
    //    try
    //    {
    //        DataSet ds = new DataSet();
    //        sp = new SubSonic.StoredProcedure((HttpContext.Current.Session["TENANT"]) + "." + "[GET_SHIFTS_BY_SPACE_AI]");
    //        sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
    //        sp.Command.Parameters.Add("@SPCID", SPCID, DbType.String);
    //        ds = sp.GetDataSet();
    //        return ds.Tables[0].AsEnumerable().Select(x => x.Field<string>(0)).ToList();
    //    }
    //    catch (Exception ex)
    //    {
    //       return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
    //    }
    //}

    public object GETFLOORSBYUSERID(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Book Seat", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_GET_FLOORS_USER_LOCATION]");
                sp.Command.Parameters.Add("@AUR_ID", Model.UserId, DbType.String);
                ds = sp.GetDataSet();

                if (ds.Tables[0].Rows.Count == 0)
                {
                    var Table = new List<object>();
                    Table.Add(new
                    {
                        FLR_CODE = "",
                        FLR_NAME = "",
                    });
                    return new { Status = 0, Message = "No Floor Found", data = new { Table = Table } };
                }

                var dataList = ds.Tables[0].AsEnumerable().Select(row => row.Table.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => row[col])).ToList();

                return new { Status = res, Message = MessagesVM.AF_OK, data = new { Table = dataList } };
            }
            else
            {
                var Table = new List<object>();
                Table.Add(new
                {
                    FLR_CODE = "",
                    FLR_NAME = "",
                });
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), data = new { Table = Table } };
            }
        }
        catch (Exception ex)
        {
            var Table = new List<object>();
            Table.Add(new
            {
                FLR_CODE = "",
                FLR_NAME = "",
            });
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, data = new { Table = Table } };
        }
    }


    public object GETVACANTSPACESBYFLOOR(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Book Seat", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_FOOLR_VACANT_SPACES]");
                sp.Command.Parameters.Add("@AUR_ID", Model.UserId, DbType.String);
                sp.Command.Parameters.Add("@FloorID", Model.code, DbType.String);
                ds = sp.GetDataSet();

                if (ds.Tables[0].Rows[0]["SeatBooking"].ToString() == "Already you having today bookings")
                {
                    return new { Status = 2, Message = "Seat Already Booked for Today", Data = (object)null };
                }

                var dataList = ds.Tables[0].AsEnumerable().Select(row => row.Table.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => row[col])).ToList();

                return new { Status = res, Message = MessagesVM.AF_OK, data = new { Table = dataList } };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object GETCURRENTBOOKING(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Release Seat", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_GET_CURRENT_BOOKING]");
                sp.Command.Parameters.Add("@AUR_ID", Model.UserId, DbType.String);
                ds = sp.GetDataSet();
                if (ds.Tables[0].Rows.Count == 0)
                {
                    var obj = new
                    {
                        SPC_ID = "",
                        SSAD_SRN_REQ_ID = "",
                        SSA_SRNREQ_ID = "",
                        SSAD_FROM_DATE = "",
                        SSAD_TO_DATE = ""
                    };
                    var Table = new List<object>();
                    Table.Add(obj);
                    return new
                    {
                        Status = 0,
                        Message = "No Seat Booked",
                        data = new
                        {
                            Table = Table
                        }
                    };
                }
                var dataList = ds.Tables[0].AsEnumerable().Select(row => row.Table.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => row[col])).ToList();

                return new { Status = res, Message = MessagesVM.AF_OK, data = new { Table = dataList } };
            }
            else
            {
                var obj = new
                {
                    SPC_ID = "",
                    SSAD_SRN_REQ_ID = "",
                    SSA_SRNREQ_ID = "",
                    SSAD_FROM_DATE = "",
                    SSAD_TO_DATE = ""
                };
                var Table = new List<object>();
                Table.Add(obj);
                return new
                {
                    Status = res,
                    Message = ds1.Tables[0].Rows[0][1].ToString(),
                    data = new
                    {
                        Table = Table
                    }
                };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }
    public object AllocateSeatsCopilot(CP_SPACE_ALLOCATE_DETAILS_LST allocDetLst)
    {
        try
        {
            CPModel mo = new CPModel();

            mo.CompanyId = allocDetLst.COMPANYID;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", allocDetLst.AUR_ID, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Book Seat", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                List<HD_SPACE_ALLOCATE_DETAILS> hdl = new List<HD_SPACE_ALLOCATE_DETAILS>();
                hdl.Add(new HD_SPACE_ALLOCATE_DETAILS
                {
                    SPC_ID = allocDetLst.SPC_ID,
                    SSA_SRNREQ_ID = allocDetLst.SSA_SRNREQ_ID,
                    SSAD_SRN_REQ_ID = allocDetLst.SSAD_SRN_REQ_ID,
                    SH_CODE = allocDetLst.SH_CODE,
                    VERTICAL = allocDetLst.VERTICAL,
                    Cost_Center_Code = allocDetLst.Cost_Center_Code,
                    AUR_ID = allocDetLst.AUR_ID,
                    STATUS = allocDetLst.STATUS,
                    FROM_DATE = allocDetLst.FROM_DATE,
                    TO_DATE = allocDetLst.TO_DATE,
                    FROM_TIME = allocDetLst.FROM_TIME,
                    TO_TIME = allocDetLst.TO_TIME,
                    STACHECK = allocDetLst.STACHECK,
                    ticked = allocDetLst.ticked,
                    SHIFT_TYPE = allocDetLst.SHIFT_TYPE,
                    emp = allocDetLst.emp
                }
                    );

                int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
                var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();

                String proc_name = DB + ".[COPILOT_HD_SMS_SPACE_ALLOC_MAP]";
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(details);
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[1].Value = allocDetLst.AUR_ID;
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[2].Value = 1;
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
                string Result;
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = "" };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.ErrorMessage, Data = "" };
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    public object LocationMapping(CPModel CPM)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = CPM.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", CPM.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Location Mapping", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {

                sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_INSERT_USER_MAPPING_DETAILS]");
                sp.Command.AddParameter("@Location", CPM.code, DbType.String);
                sp.Command.AddParameter("@UDM_AUR_ID", CPM.UserId, DbType.String);
                //sp.ExecuteScalar();
                DataSet ds = sp.GetDataSet();
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = "" };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.ErrorMessage, data = "" };
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    public object ReleaseSeatCopilot(CP_SPACE_REL_DETAILS RLSeat)
    {
        try
        {

            CPModel mo = new CPModel();

            mo.CompanyId = RLSeat.COMPANYID;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", RLSeat.AUR_ID, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Release Seat", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {

                List<CP_SPACE_ALLOCATE_DETAILS> hdl = new List<CP_SPACE_ALLOCATE_DETAILS>();
                hdl.Add(new CP_SPACE_ALLOCATE_DETAILS
                {
                    SPC_ID = RLSeat.SPC_ID,
                    SSA_SRNREQ_ID = RLSeat.SSA_SRNREQ_ID,
                    SSAD_SRN_REQ_ID = RLSeat.SSAD_SRN_REQ_ID,
                    SH_CODE = RLSeat.SH_CODE,
                    VERTICAL = RLSeat.VERTICAL,
                    Cost_Center_Code = RLSeat.Cost_Center_Code,
                    AUR_ID = RLSeat.AUR_ID,
                    STATUS = RLSeat.STATUS,
                    FROM_DATE = RLSeat.FROM_DATE,
                    TO_DATE = RLSeat.TO_DATE,
                    STACHECK = RLSeat.STACHECK,
                    ticked = RLSeat.ticked,
                    SHIFT_TYPE = RLSeat.SHIFT_TYPE,
                    emp = RLSeat.emp
                }
                    );

                int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
                var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();

                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@REL_DET", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(details);
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[1].Value = RLSeat.AUR_ID;
                param[2] = new SqlParameter("@RELTYPE", SqlDbType.NVarChar);
                param[2].Value = RLSeat.reltype;
                param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[3].Value = 1;
                int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DB + ".[COPILOT_SMS_SPACE_REL_MAP]", param);
                if (RETVAL == 1)
                    return new { Status = res, Message = MessagesVM.UM_OK, data = RLSeat.reltype };
                else
                    return new { Status = res, Message = MessagesVM.ErrorMessage, data = 0 };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), data = 0 };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.ErrorMessage, data = 0 };
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };

    }


    public object DownloadSpaceReport([FromBody] Helpdesk hd)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = hd.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", hd.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "DownloadSpaceReport", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {

                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[SPACE_DOWNLOAD_CUSTOMIZABLE_REPORT_AI]");
                sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
                sp.Command.AddParameter("@FromDate", hd.FromDate, DbType.String);
                sp.Command.AddParameter("@ToDate", hd.ToDate, DbType.String);
                ds = sp.GetDataSet();
                var fileName = "report_" + Guid.NewGuid().ToString() + ".xlsx";
                var filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadFiles"), fileName);
                CreateExcelFile.CreateExcelDocument(ds, filePath);
                var baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                var downloadLink = Path.Combine(baseUrl, "UploadFiles", fileName);
                //return downloadLink;
                var downloadUrl = baseUrl + "/UploadFiles/" + fileName;
                return new { Status = res, Message = MessagesVM.AF_OK, Data = downloadUrl };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }


    //HD Module

    //Get Total No.Of HD Request Count
    public string GetTotalHDReqCountAI(string DB)
    {
        try
        {
            string cnt = "0";
            DataSet ds = new DataSet();
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GET_TODAYS_REQUEST_ID");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                cnt = Convert.ToInt32((ds.Tables[0].Rows[0]["cnt"])).ToString();
                return cnt;
            }
            else { return Convert.ToInt32("0").ToString(); }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }



    public object ChidCatInSubCat(HDRaiseRequest child)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = child.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", child.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Raise HDM Request", DbType.String);
            int res = (int)sp.ExecuteScalar();
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_API_CHILDCATS_SUBCAT]");
                sp.Command.Parameters.Add("@AUR_ID", child.UserId, DbType.String);
                sp.Command.AddParameter("@PROB_DESC", child.probdesc, DbType.String);
                ds = sp.GetDataSet();
                var dataList = ds.Tables[0].AsEnumerable().Select(row => row.Table.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => row[col])).ToList();

                return new { Status = res, Message = MessagesVM.AF_OK, Data = dataList };
            }
            else
            {
                return new { Status = res, Message = MessagesVM.Cop_Reg, Data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }


    public object _RaiseHDRequestAI(HDRaiseRequest child)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = child.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", child.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Raise HDM Request", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                var cnt = GetTotalHDReqCountAI(DB);
                string HelDeskReq = "";
                HelDeskReq = (DateTime.Now.ToString("ddMMyyyy") + '/' + cnt).ToString();
                sp = new SubSonic.StoredProcedure(DB + "." + "[API_AI_RAISE_HELP_DESK_REQUEST]");
                sp.Command.AddParameter("@REQ_ID", HelDeskReq, DbType.String);
                sp.Command.AddParameter("@STATUS", 1, DbType.String);
                //sp.Command.AddParameter("@LOC_CODE", "", DbType.String);
                //sp.Command.AddParameter("@MNC_CODE", VALTYPES.MAINCAT, DbType.String);
                //sp.Command.AddParameter("@SUB_CAT", VALTYPES.SUBCAT, DbType.String);
                sp.Command.AddParameter("@CHILD_CAT", child.ChildCat, DbType.String);
                sp.Command.AddParameter("@REPEATCALL", "N", DbType.String);
                sp.Command.AddParameter("@PROB_DESC", child.probdesc, DbType.String);
                sp.Command.AddParameter("@IMPACT", "MNR", DbType.String);
                sp.Command.AddParameter("@URGENCY", "MED", DbType.String);
                sp.Command.AddParameter("@SER_CALL_LOG_BY", child.UserId, DbType.String);
                sp.Command.AddParameter("@SER_REQ_TYPE", 0, DbType.String);
                sp.Command.AddParameter("@RAISED_BY", child.UserId, DbType.String);
                sp.Command.AddParameter("@COMPANYID", 1, DbType.Int32);
                sp.Command.AddParameter("@PHONENO", "", DbType.String);
                //sp.ExecuteScalar();
                int i = (int)sp.ExecuteScalar();
                if (i == 1)
                {
                    return new { Status = res, Message = "Request Raised Successfully \nRequest Id = ", Data = HelDeskReq };
                }
                else if (i == 2)
                {
                    return new { Status = res, Message = "Service Incharges are Not mapped to the selected service.", Data = "" };
                }
                else if (i == 3)
                {
                    return new { Status = res, Message = "SLA was not Defined to the selected service.", Data = "" };
                }
                else
                {
                    return new { Status = res, Message = "Invaild child category", Data = "" };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }


    public object GetInchargeByReqID(Helpdesk hd)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = hd.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", hd.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Reqid Incharge", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {

                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_INCAHRGE_BY_REQID]");
                sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
                sp.Command.Parameters.Add("@REQID", hd.reqid, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = "" };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }

    public object GetStatusAndSlaByReqID(Helpdesk hd)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = hd.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", hd.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Reqid Status and SLA", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_statussla_BY_REQID]");
                sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
                sp.Command.Parameters.Add("@REQID", hd.reqid, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = "" };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }

    public object GetClosedTickets(Helpdesk hd)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = hd.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", hd.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Closed Helpdesk Tickets", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_Closed_Count]");
                sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = "" };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object DownloadReport([FromBody] Helpdesk hd)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = hd.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", hd.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Download Helpdesk Report", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {

                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[DOWNLOAD_CUSTOMIZABLE_REPORT_AI]");
                sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
                sp.Command.AddParameter("@FromDate", hd.FromDate, DbType.String);
                sp.Command.AddParameter("@ToDate", hd.ToDate, DbType.String);
                ds = sp.GetDataSet();
                var fileName = "report_" + Guid.NewGuid().ToString() + ".xlsx";
                var filePath = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadFiles"), fileName);
                CreateExcelFile.CreateExcelDocument(ds, filePath);
                var baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                var downloadLink = Path.Combine(baseUrl, "UploadFiles", fileName);
                //return downloadLink;
                var downloadUrl = baseUrl + "/UploadFiles/" + fileName;
                return new { Status = res, Message = MessagesVM.AF_OK, Data = downloadUrl };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return ex.ToString();
        }
    }
    public object PendingReqApproval(AST_COP_REQ COPAST)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COPAST.CompanyId;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Show Asset Pending", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[AST_PENDING_REQ_APPROVAL]");
                sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
                ds = sp.GetDataSet();
                if (ds.Tables[0].Rows.Count != 0)
                    return new { Status = res, Message = MessagesVM.AF_OK, Data = ds };
                else
                    return new { Status = 0, Message = "No Approval Requests Found", Data = ds };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = new { Table = new object[] { } } };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object Cop_Approve_Req(AST_COP_REQ COPAST)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COPAST.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Approve Asset", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "COP_REQ_APPROVE");
                sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
                sp.Command.Parameters.Add("@REQID", COPAST.AIR_REQ_ID, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = MessagesVM.AF_OK, Data = ds };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = new { Table = new object[] { } } };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object Bk_Meet_Room(COP_RESERVE Resrv)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = Resrv.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Resrv.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Book Meeting Room", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {

                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "COP_BK_MEET_ROOM");
                sp.Command.Parameters.Add("@AURID", Resrv.UserId, DbType.String);
                Resrv.REQID = DateTime.Now.Year + "/" + Resrv.UserId + "/" + DateTime.Now.ToString("dd/MM/yyyy/HH/mm/ss");
                sp.Command.Parameters.Add("@REQID", Resrv.REQID, DbType.String);
                sp.Command.Parameters.Add("@ROOM_CODE", Resrv.RMId, DbType.String);
                sp.Command.Parameters.Add("@FROM_DT", Resrv.FROM_DT, DbType.DateTime);
                sp.Command.Parameters.Add("@TO_DT", Resrv.TO_DT, DbType.DateTime);
                sp.Command.Parameters.Add("@FROM_TIME", Resrv.FROM_TIME, DbType.String);
                sp.Command.Parameters.Add("@TO_TIME", Resrv.TO_TIME, DbType.String);
                ds = sp.GetDataSet();
                //var dataList = ds.Tables[0].AsEnumerable().Select(row => row.Table.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => row[col])).ToList();

                //return new { Status = res, Message = MessagesVM.AF_OK, Data = new { Table = dataList } };
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = "" };
                //return ds.Tables[0].AsEnumerable().Select(x => x.Field<string>(0)).ToList();
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }

    // Reservation Cancel active booking today
    public object CancelTodayReservationBooking(Helpdesk hd)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = hd.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", hd.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Cancel Booking Today", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[CANCEL_RESERVATION_ACTIVEBOOKING_TODAY_AI]");
                sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = "" };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    //Today's Reservation booking list
    public object MeetingSchedulesToday(Helpdesk hd)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = hd.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", hd.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Cancel Booking Today", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "[MEETINGS_SCHEDULES_TODAY]");
                sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
                ds= sp.GetDataSet();
                return new { Status = res, Message = "", Data = "Todays Bookings Count: " + ds.Tables[0].Rows[0]["TodaysBookingsCount"].ToString() };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }

    public object GetPendingValidationChk(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Show Checklist Validate", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[BCL_GET_SAVED_LIST_ZONALCENTRAL_COPilot]");
                sp.Command.Parameters.Add("@AUR_ID", Model.UserId, DbType.String);
                sp.Command.Parameters.Add("@BCLStatus", 2, DbType.Int32);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is the List of Tickets for Validation-", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Tickets found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object GetPendingApprovalChk(CPModel Model)
    {
        try
        {
            string DB = _ReturnDB(Model);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Model.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Show Checklist Approval", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[BCL_GET_SAVED_LIST_ZONALCENTRAL_COPilot]");
                sp.Command.Parameters.Add("@AUR_ID", Model.UserId, DbType.String);
                sp.Command.Parameters.Add("@BCLStatus", 3, DbType.Int32);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is the List of Tickets for Approval-", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Tickets found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object GetAstLowStockList(AST_COP_REQ COPAST)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COPAST.CompanyId;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Show Low Stock Asset", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_LOW_STOCK_AST_LIST]");
                sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is List of Low Stock Assets", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Asset found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object TodaysPos(AST_COP_REQ COPAST)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COPAST.CompanyId;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Today's PO's", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_TODAYS_POs]");
                sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is List of PO's", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No PO's found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object Ast_Tot_Val(AST_COP_REQ COPAST)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COPAST.CompanyId;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Total Asset Value", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[COP_TOT_AST_VAL]");
                sp.Command.Parameters.Add("@AURID", COPAST.UserId, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = MessagesVM.AF_OK, Data = "Total cost: " + ds.Tables[0].Rows[0]["totalcost"].ToString() };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object PVM_Today_Active(MM_COP_REQ COMM)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COMM.CompanyId;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COMM.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Show Today's PPM Activites", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[PVM_TODAY_PLAN]");
                sp.Command.Parameters.Add("@AURID", COMM.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is List of Active PPMs", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Data found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }

        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object PVM_CurrnentMonthCost(MM_COP_REQ COMM)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COMM.CompanyId;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COMM.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Current Month Maintenance Cost", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[PVM_CURRENT_MONTH_COST]");
                sp.Command.Parameters.Add("@AURID", COMM.UserId, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = MessagesVM.AF_OK, Data = "Total cost: " + ds.Tables[0].Rows[0]["TOTAL COST"].ToString() };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object ExpiringAMC(MM_COP_REQ COMM)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COMM.CompanyId;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COMM.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Expiring AMC's", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[EXPIRING_AMC]");
                sp.Command.Parameters.Add("@AURID", COMM.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is List of Expiring AMC", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No AMCs found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object UpcommingMMActive(MM_COP_REQ COMM)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = COMM.CompanyId;
            string DB = _ReturnDB(mo);

            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", COMM.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Upcoming Maintenance Activities", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[UPCOMING_MM_ACTIVE]");
                sp.Command.Parameters.Add("@AURID", COMM.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is List of Upcoming AMC", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Plans Found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object Avlb_GH(GH_COP_REQ GH)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = GH.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", GH.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Available Guest House", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[COP_AVBL_GH]");
                sp.Command.Parameters.Add("@AURID", GH.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is List of Available Guest House", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Details Found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }

    public object Total_Prty_Lease_Cost(PROPERTY_MODULE TC)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = TC.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", TC.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Total Properties Lease Cost", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[TOTAL_LEASE_PRTY_COST]");
                sp.Command.Parameters.Add("@AURID", TC.UserId, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = "" };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }

    public object Ren_Nxt_Quarter(PROPERTY_MODULE TC)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = TC.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", TC.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Next Quarter Rent", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[LIST_RENEW_NXT_QUARTER]");
                sp.Command.Parameters.Add("@AURID", TC.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is List of Renewals", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Details Found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }

    public object InsertChecklist(ChecklistIns data)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = data.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Insert Checklist", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[DYNAMIC_CHECKLIST_COPILOT]");
                sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
                sp.Command.Parameters.Add("@LCM_CODE", data.Loc_id, DbType.String);
                sp.Command.Parameters.Add("@BCL_TYPE_ID", data.Type, DbType.Int32);
                sp.Command.Parameters.Add("@STATE", data.State, DbType.Int32);
                ds = sp.GetDataSet();
                return new { Status = res, Message = MessagesVM.AF_OK, Data = ds };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = new { Table = new object[] { } } };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object getLocation(ChecklistIns data)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = data.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Insert Checklist", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_LOCATION_CHK]");
                sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
                ds = sp.GetDataSet();
                return new { Status = res, Message = MessagesVM.AF_OK, Data = ds };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = new { Table = new object[] { } } };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object getType(ChecklistIns data)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = data.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Insert Checklist", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET__CHECKLIST_TYPE]");
                ds = sp.GetDataSet();
                return new { Status = res, Message = MessagesVM.AF_OK, Data = ds };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = new { Table = new object[] { } } };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object ChkGetSubLst(ChecklistIns data)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = data.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Insert Checklist", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_SUBMITTED_CHECKLIST]");
                sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                return new { Status = res, Message = ds.ToString(), Data = "" };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object ChkGetLstMnth(ChecklistIns data)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = data.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Last Month Cheklist's", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[GET_CHECKLIST_LSTMNTH]");
                sp.Command.Parameters.Add("@AURID", data.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is the List of Last Month Not Ok Cases", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Not Ok Cases Found for Last Month", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object Cur_Avail_Book(COP_RESERVE Resrv)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = Resrv.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Resrv.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Available Rooms", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[CURR_AVAIL_BOOKING]");
                sp.Command.Parameters.Add("@AURID", Resrv.UserId, DbType.String);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                if (ds != null && ds != "")
                {
                    return new { Status = res, Message = "Here is List of Available Room", Data = ds };
                }
                else
                {
                    return new { Status = res, Message = "No Available Rooms Found", Data = ds };
                }
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object Avail_Time_Book(COP_RESERVE Resrv)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = Resrv.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Resrv.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Available Booking in this time", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];
            if (res == 1)
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "[AVAIL_TIME_BOOKING]");
                sp.Command.Parameters.Add("@AURID", Resrv.UserId, DbType.String);
                sp.Command.Parameters.Add("@FROM_TIME", Resrv.FROM_TIME, DbType.DateTime);
                sp.Command.Parameters.Add("@TO_TIME", Resrv.TO_TIME, DbType.DateTime);
                string ds = sp.ExecuteScalar().ToString().Replace("\\r\\n", "\r\n");
                return new { Status = res, Message = MessagesVM.AF_OK, Data = ds };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = "" };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = "" };
        }
    }
    public object Edit_My_Active_Bookings(COP_RESERVE Resrv)
    {
        try
        {
            CPModel mo = new CPModel();
            mo.CompanyId = Resrv.CompanyId;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + "." + "[COPILOT_ACTIVATION]");
            sp.Command.Parameters.Add("@AURID", Resrv.UserId, DbType.String);
            sp.Command.Parameters.Add("@CLS_NAME", "Edit Room Booking", DbType.String);
            DataSet ds1 = sp.GetDataSet(); int res = (int)ds1.Tables[0].Rows[0][0];

            if (res == 1)
            {
                DataSet ds;
                sp = new SubSonic.StoredProcedure(DB + "." + "[EDIT_MY_ACTIVE_BOOK]");
                sp.Command.Parameters.Add("@AURID", Resrv.UserId, DbType.String);
                sp.Command.Parameters.Add("@FROM_TIME", Resrv.FROM_TIME, DbType.DateTime);
                sp.Command.Parameters.Add("@TO_TIME", Resrv.TO_TIME, DbType.DateTime);
                ds = sp.GetDataSet();
                return new { Status = res, Message = ds.Tables[0].Rows[0][0].ToString(), Data = ds };
            }
            else
            {
                return new { Status = res, Message = ds1.Tables[0].Rows[0][1].ToString(), Data = new { Table = new object[] { } } };
            }
        }
        catch (Exception ex)
        {
            return new { Status = ErrorSts, Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
}


