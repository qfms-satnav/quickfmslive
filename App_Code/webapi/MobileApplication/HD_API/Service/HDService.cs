﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Text;
using System.Web.Script.Serialization;
using System.ComponentModel;
using System.Collections;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Configuration;

public partial class HDService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    #region API Core Logics
    string DatabaseURL;
    SecureAPIKey Secure = new SecureAPIKey();

    //Space Module

    //public string ReadFile(string floorCode, out bool isError)
    //{
    //    isError = true;
    //    try
    //    {
    //        var fileContent = "";
    //        var jsonFileServer = string.IsNullOrWhiteSpace(System.Configuration.ConfigurationManager.AppSettings["JsonHostingURL"]) ? "" : System.Configuration.ConfigurationManager.AppSettings["JsonHostingURL"].ToString();

    //        //var webRequest = WebRequest.Create(jsonFileServer + "/" + floorCode + ".json");
    //        var webRequest = WebRequest.Create(jsonFileServer + "/" + (HttpContext.Current.Session["TENANT"]) + "/" + floorCode + ".json");
    //        using (var response = webRequest.GetResponse())
    //        using (var content = response.GetResponseStream())
    //        using (var reader = new StreamReader(content))
    //        {
    //            fileContent = reader.ReadToEnd();
    //            isError = false;
    //        }

    //        return fileContent;
    //    }
    //    catch (Exception ex)
    //    {
    //        return "";
    //    }
    //}by vinod

    public string ReadFile(string floorCode, string DB ,out bool isError)
    {
        isError = true;

        try
        {
            var fileContent = string.Empty;
            var jsonFileServer = System.Configuration.ConfigurationManager.AppSettings["JsonHostingURL"];

            if (string.IsNullOrWhiteSpace(jsonFileServer))
            {

                return fileContent;
            }

            var tenant = DB.ToString();
            if (string.IsNullOrWhiteSpace(tenant))
            {

                return fileContent;
            }

            var url = jsonFileServer + "/" + tenant + "/" + floorCode + ".json";
            var webRequest = WebRequest.Create(url);
            using (var response = webRequest.GetResponse())
            using (var content = response.GetResponseStream())
            using (var reader = new StreamReader(content))
            {
                fileContent = reader.ReadToEnd();
                isError = false;
            }

            return fileContent;
        }
        catch (WebException ex)
        {
            return string.Empty;
        }
        catch (IOException ex)
        {
            return string.Empty;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }

    }
    public object GetEmployeeSpaceDetails(GetSpaceDetails main)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = main.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "HD_GET_SPACE_DETAILS");
                sp.Command.AddParameter("@AUR_ID", main.AURID, DbType.String);
                sp.Command.AddParameter("@FROM_DATE", main.FromDate, DbType.String);
                sp.Command.AddParameter("@FROM_TIME", main.FromTime, DbType.String);
                sp.Command.AddParameter("@TO_DATE", main.ToDate, DbType.String);
                sp.Command.AddParameter("@TO_TIME", main.ToTime, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();

                if (ds.Tables.Count != 0)
                    return new { data = ds.Tables[0] };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object SEAT_STATUS_CHECK(SEAT_CHECK SC)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = SC.COMPANY_ID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("Seat Check" + JsonConvert.SerializeObject(SC));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "HD_SEAT_STATUS_CHECK");
                sp.Command.Parameters.Add("@AUR_ID", SC.AUR_ID, DbType.String);
                sp.Command.Parameters.Add("@SPC_ID", SC.SPC_ID, DbType.String);
                sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int32);
                sp.Command.Parameters.Add("@FROM_DATE", SC.FROM_DATE, DbType.DateTime);
                sp.Command.Parameters.Add("@TO_DATE", SC.TO_DATE, DbType.DateTime);
                ds = sp.GetDataSet();
                return ds.Tables[0];
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetMarkers(GetSpaceDetails svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("GetMarkers" + JsonConvert.SerializeObject(svm));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_MARKER_ALL");
                sp.Command.Parameters.Add("@AUR_ID", svm.AURID, DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.Floor_Id, DbType.String);
                sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int32);

                ds = sp.GetDataSet();
                return ds.Tables[0];
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetFloorDetails(Maplistfloors main)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = main.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "GETUSERFLOORLIST");
                sp.Command.AddParameter("@AUR_ID", main.AURID, DbType.String);
                // sp.Command.AddParameter("@COMPANYID", main.COMPANYID, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();

                if (ds.Tables.Count != 0)
                    return new { data = ds.Tables[0] };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object GetSpaceDetailsByREQID(GetSpaceDetails svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("GetSpaceDetailsByREQID" + JsonConvert.SerializeObject(svm));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_SPACEDETAILS_BYREQID");
                sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
                sp.Command.Parameters.Add("@SPC_ID", svm.category, DbType.String);
                sp.Command.Parameters.Add("@REQ_ID", svm.subitem, DbType.String);
                sp.Command.Parameters.Add("@AUR_ID", svm.AURID, DbType.String);
                ds = sp.GetDataSet();
                return ds.Tables[0];


            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object SeatRestriction(Space_Restrict_Seat svm)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = svm.COMPANYID;
        string DB = _ReturnDB(mo);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "Seat_Restrictions_Checking");
        sp.Command.Parameters.Add("@AUR_ID", svm.AUR_ID, DbType.String);
        sp.Command.Parameters.Add("@Seat_type", svm.SPC_ID, DbType.String);
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object SpaceRestriction(Space_Restrict_Seat svm)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = svm.COMPANYID;
        string DB = _ReturnDB(mo);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "RESTRICT_DAYS_COUNT");
        ds = sp.GetDataSet();
        return ds.Tables[0];
    }

    public object GetRoleAndReportingManger(Space_Restrict_Seat svm)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = svm.COMPANYID;
        string DB = _ReturnDB(mo);
        List<GetRoleAndRM> RM_lst = new List<GetRoleAndRM>();
        GetRoleAndRM RM;
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_ROLE_AND_REPORTING_MNAGER_BY_USER_ID");
        sp.Command.AddParameter("@AUR_ID", svm.AUR_ID, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                RM = new GetRoleAndRM();
                RM.AUR_ROLE = sdr["URL_ROL_ID"].ToString();
                RM.AUR_REPOTING_TO = sdr["AUR_REPORTING_TO"].ToString();
                RM.AUR_ID = sdr["AUR_ID"].ToString();
                RM.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                RM.VER_CODE = sdr["VER_CODE"].ToString();
                RM.VER_NAME = sdr["VER_NAME"].ToString();
                RM.COST_CENTER_CODE = sdr["COST_CENTER_CODE"].ToString();
                RM.COST_CENTER_NAME = sdr["COST_CENTER_NAME"].ToString();
                RM_lst.Add(RM);
            }
        }
        if (RM_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = RM_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSysPreferences(Space_Restrict_Seat svm)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = svm.COMPANYID;
        string DB = _ReturnDB(mo);
        List<SysPreference> SysPrflst = new List<SysPreference>();
        SysPreference SysPrf;
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_SYSTEM_PREFERENCE_ADM");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SysPrf = new SysPreference();
                SysPrf.SYSP_CODE = sdr["SYSP_CODE"].ToString();
                SysPrf.SYSP_VAL1 = sdr["SYSP_VAL1"].ToString();
                SysPrf.SYSP_VAL2 = sdr["SYSP_VAL2"].ToString();
                SysPrf.SYSP_OPR = sdr["SYSP_OPR"].ToString();
                SysPrf.SYSP_STA_ID = Convert.ToInt32(sdr["SYSP_STA_ID"]);
                SysPrflst.Add(SysPrf);
            }
        }
        if (SysPrflst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SysPrflst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object SEAT_RESTRICTION_BOOKING(Space_Restrict_Seat_details svm)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = svm.COMPANYID;
        string DB = _ReturnDB(mo);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "SEAT_RESTRICTION_BOOKING");
        sp.Command.Parameters.Add("@FROMDATE", svm.FROM_DATE, DbType.String);
        sp.Command.Parameters.Add("@SPC_ID", svm.SPC_ID, DbType.String);
        sp.Command.Parameters.Add("@AUR_ID", svm.AUR_ID, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GETEMPLOYEETYPE(Space_Restrict_Seat_details svm)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = svm.COMPANYID;
        string DB = _ReturnDB(mo);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "EMPLOY_TYPE_DATA");
        sp.Command.Parameters.Add("@AUR_ID", svm.AUR_ID, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetEmployees(Space_Restrict_Seat svm)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = svm.COMPANYID;
        string DB = _ReturnDB(mo);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "Get_Employees");
        sp.Command.Parameters.Add("@AUR_ID", svm.AUR_ID, DbType.String);
        sp.Command.AddParameter("@COMPANYID", 1, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetEmpAllocSeat(GetSpaceDetails svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("GetEmpAllocSeat" + JsonConvert.SerializeObject(svm));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "SMS_MAP_EMP_ALLOC_SEAT_DETAILS");
                sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
                sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
                sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
                sp.Command.Parameters.Add("@AUR_ID", svm.Item, DbType.String);
                sp.Command.Parameters.Add("@MODE", svm.mode, DbType.String);
                ds = sp.GetDataSet();
                return ds.Tables[0];

            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object AllocateSeats(HD_SPACE_ALLOCATE_DETAILS_LST allocDetLst)
    {
        try
        {
            HDModel mo = new HDModel();

            mo.CompanyId = allocDetLst.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("AllocateSeats" + JsonConvert.SerializeObject(allocDetLst.LST));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                List<HD_SPACE_ALLOCATE_DETAILS> hdl = new List<HD_SPACE_ALLOCATE_DETAILS>();
                hdl.Add(allocDetLst.LST);

                int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
                var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();

                String proc_name = DB + ".HD_SMS_SPACE_ALLOC_MAP";
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(details);
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[1].Value = allocDetLst.AUR_ID;
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[2].Value = 1;
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
                string Result;
                return ds.Tables[0];
                //List<HD_SPACE_ALLOCATE_DETAILS> spcdet = new List<HD_SPACE_ALLOCATE_DETAILS>();
                //HD_SPACE_ALLOCATE_DETAILS spc;

                //    using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, DB+"."+"SMS_SPACE_ALLOC_MAP", param))
                //{
                //    while (sdr.Read())
                //    {
                //        string REQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                //        spc = new HD_SPACE_ALLOCATE_DETAILS();
                //        spc.AUR_ID = sdr["AUR_ID"].ToString();
                //        spc.VERTICAL = sdr["VERTICAL"].ToString();
                //        spc.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                //        spc.FROM_DATE = Convert.ToDateTime(sdr["FROM_DATE"]);
                //        spc.TO_DATE = Convert.ToDateTime(sdr["TO_DATE"]);
                //        spc.SH_CODE = sdr["SH_CODE"].ToString();
                //        spc.SPC_ID = sdr["SPC_ID"].ToString();
                //        spc.SSAD_SRN_REQ_ID = sdr["SSAD_SRN_REQ_ID"].ToString();
                //        spc.SSA_SRNREQ_ID = sdr["SSA_SRNREQ_ID"].ToString();
                //        spc.STACHECK = (int)RequestState.Modified;
                //        spc.STATUS = Convert.ToInt16(sdr["STATUS"]);
                //        spc.ticked = true;
                //        spc.SHIFT_TYPE = sdr["SPACE_TYPE"].ToString();
                //        spc.emp = sdr["emp"].ToString();
                //        spcdet.Add(spc);

                //        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "SEND_MAIL_EMPLOYEE_ALLOCATION_ACTION_SM");
                //        sp.Command.AddParameter("@REQID", REQ_ID, DbType.String);
                //        sp.Command.AddParameter("@CREATED_BY", spc.AUR_ID, DbType.String);
                //        sp.Command.AddParameter("@COMPANY_ID", 1, DbType.Int32);
                //        sp.ExecuteScalar();
                //    }


                //}
                //foreach (HD_SPACE_ALLOCATE_DETAILS obj in hdl.Where(x => x.STACHECK == (int)RequestState.Deleted).ToList())
                //{
                //    spc = new HD_SPACE_ALLOCATE_DETAILS();
                //    spc.SPC_ID = obj.SPC_ID;
                //    spc.STATUS = obj.STATUS;
                //    spc.ticked = true;
                //    spcdet.Add(spc);
                //}
                //if (spcdet.Count != 0)
                //    return new { Message = MessagesVM.UM_OK, data = spcdet };
                //else
                //    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

            }

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public Object GetZonalFmLocations(ZonalFmLocations zonalFmLocations)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = zonalFmLocations.companyId;
            string DB = _ReturnDB(hDModel);

            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_ID_BY_LOCATION_WISE_ZONALS");
            sp.Command.Parameters.Add("@AUR_ID", zonalFmLocations.userId, DbType.String);
            sp.Command.Parameters.Add("@ID", zonalFmLocations.submittedCode, DbType.Int32);
            ds = sp.GetDataSet();

            if (ds.Tables.Count != 0)
                return new { Message = "OK", data = ds.Tables[0] };
            //return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = "Not OK", data = (object)null };

        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public object GET_VERTICAL_WISE_ALLOCATIONS(GetSpaceDetails svm)
    {
        DataSet ds = new DataSet();
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(svm));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_VERTICAL_WISE_ALLOCATIONS");
                sp.Command.AddParameter("@FLR_CODE", svm.Floor_Id, DbType.String);
                ds = sp.GetDataSet();
                List<object> verticals = ds.Tables[0].Rows.Cast<DataRow>().Select(r => r.ItemArray[0]).Distinct().ToList();
                return new { data = verticals, data1 = ds.Tables[0], data2 = ds.Tables[1] };
            }

        }
        catch (Exception ex)
        {
            return new { data = (object)null };
        }
    }

    public object SubmitSpaceDetails(SubmitSpaceDetails main)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = main.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "HD_SUBMIT_SPACE_DETAILS" + "");
                sp.Command.AddParameter("@AUR_ID", main.AURID, DbType.String);
                sp.Command.AddParameter("@FROM_DATE", main.FromDate, DbType.String);
                sp.Command.AddParameter("@FROM_TIME", main.FromTime, DbType.String);
                sp.Command.AddParameter("@TO_DATE", main.ToDate, DbType.String);
                sp.Command.AddParameter("@TO_TIME", main.ToTime, DbType.String);
                sp.Command.AddParameter("@SPC_ID", main.SPC_ID, DbType.String);
                sp.Command.AddParameter("@CMP_ID", main.COMPANYID, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();

                if (ds.Tables.Count != 0)
                    return new { data = ds.Tables[0] };
                else
                    return new { Message = MessagesVM.MOBil_SPACE, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //QRCODE Scanner Service

    public object GetAssetCodeDetails(QrCodeVariable QrCode)
    {
        try
        {
            List<QrCodeVariable> QList = new List<QrCodeVariable>();
            QrCodeVariable Qt;
            HDModel login = new HDModel();
            login.CompanyId = "GMR";
            string DB = _ReturnDB(login);
            int cnt;
            int Expiry;
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(QrCode.AssetCode));
                sp = new SubSonic.StoredProcedure(DB + "." + "API_QR_GET_ASSET_DETAILS");
                sp.Command.AddParameter("@AssetCode", QrCode.AssetCode, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {
                    while (sdr.Read())
                    {
                        Qt = new QrCodeVariable();
                        Qt.AssetID = sdr["AssetID"].ToString();
                        Qt.AssetName = sdr["AssetName"].ToString();
                        Qt.Location = sdr["Location"].ToString();
                        Qt.Brand = sdr["Brand"].ToString();
                        Qt.Model = sdr["Model"].ToString();
                        Qt.State = sdr["STATE"].ToString();
                        Qt.SerialNumber = sdr["AAT_AST_SERIALNO"].ToString();
                        Qt.AssignedTo = sdr["AAT_TAG_STATUS"].ToString();
                        Qt.ActucalCost = sdr["ActucalCost"].ToString();
                        Qt.AdditionalCost = sdr["AdditionalCost"].ToString();
                        Qt.LastPPMDate = sdr["LastPPMDate"].ToString();
                        Qt.LabourCost = sdr["LabourCost"].ToString();
                        Qt.SparePartsCost = sdr["SparePartsCost"].ToString();
                        Qt.BreakdownStatus = sdr["BreakdownStatus"].ToString();
                        Qt.BreakdownNumbers = sdr["BreakdownNumbers"].ToString();
                        Qt.ImagePath = sdr["ImagePath"].ToString();
                        QList.Add(Qt);
                    }
                }
            }
            return new { Status = "true", Data = QList };


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    // Get Mobile Menu

    public object GetMainMenu(HDModel main)
    {
        try
        {
            List<MainMenuItems> MList = new List<MainMenuItems>();
            MainMenuItems Mt;
            SubMenuItems st;
            //HDModel login = new HDModel();
            //login.CompanyId = "ABC_UAT";
            string DB = _ReturnDB(main);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_MAIN_MENU_ITEMS");
                //sp.Command.AddParameter("@AssetCode", main.Aurid, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Mt = new MainMenuItems();
                    Mt.MOD_ID = ds.Tables[0].Rows[i]["MOD_ID"].ToString();
                    Mt.MOD_NAME = ds.Tables[0].Rows[i]["MOD_NAME"].ToString();
                    Mt.ImagePath = ds.Tables[0].Rows[i]["MOD_IMG"].ToString();
                    Mt.SubMenu = new List<SubMenuItems>();
                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                    {

                        if (ds.Tables[0].Rows[i]["MOD_ID"].ToString() == ds.Tables[1].Rows[j]["MAIN_MOD_ID"].ToString())
                        {
                            st = new SubMenuItems();

                            st.SUB_MOD_ID = ds.Tables[1].Rows[j]["SUB_MOD_ID"].ToString();
                            st.SUB_MOD_NAME = ds.Tables[1].Rows[j]["SUB_MOD_NAME"].ToString();
                            st.SUB_MOD_IMG = ds.Tables[1].Rows[j]["SUB_MOD_IMG"].ToString();
                            st.SUB_MOD_PAGE_PATH = ds.Tables[1].Rows[j]["SUB_MOD_PAGE_PATH"].ToString();
                            st.MAIN_MOD_ID = ds.Tables[1].Rows[j]["MAIN_MOD_ID"].ToString();

                            Mt.SubMenu.Add(st);
                        }
                    }
                    MList.Add(Mt);
                }
                //using (IDataReader sdr = sp.GetReader())
                //{
                //    while (sdr.Read())
                //    {
                //        Mt = new MainMenuItems();
                //        Mt.MOD_ID = sdr["MOD_ID"].ToString();
                //        Mt.MOD_NAME = sdr["MOD_NAME"].ToString();
                //        Mt.ImagePath = sdr["MOD_IMG"].ToString();
                //        if (sdr["SUB_MOD_ID"].ToString() != "")
                //        {
                //            st = new SubMenuItems();
                //            st.SUB_MOD_ID = sdr["SUB_MOD_ID"].ToString();
                //            st.SUB_MOD_NAME = sdr["SUB_MOD_NAME"].ToString();
                //            st.SUB_MOD_IMG = sdr["SUB_MOD_IMG"].ToString();
                //            st.MAIN_MOD_ID = sdr["MOD_ID"].ToString();
                //            Mt.SubMenu = new List<SubMenuItems>();
                //            Mt.SubMenu.Add(st);
                //        }
                //        //Qt.Location = sdr["Location"].ToString();
                //        //Qt.Brand = sdr["Brand"].ToString();
                //        //Qt.Model = sdr["Model"].ToString();
                //        MList.Add(Mt);
                //    }
                //}
            }
            return new { Status = "true", Data = MList };


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Get Menu By Role
    public object GetMenuByRole(MenuByRole main)
    {
        try
        {
            List<MainMenuItems> MList = new List<MainMenuItems>();
            MainMenuItems Mt;
            SubMenuItems st;
            HDModel login = new HDModel();
            login.CompanyId = main.CompanyId;
            string DB = _ReturnDB(login);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_ROLE_BASED_MENU");
                sp.Command.AddParameter("@Rol_id", main.RoleId, DbType.String);
                sp.Command.AddParameter("@MOD_ID", main.MOD_ID, DbType.Int32);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                return new { MenuListByRole = ds.Tables[0] };
            }



        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //Get Main Menu By Role
    public object GetMainMenuByRole(MenuByRole main)
    {
        try
        {
            List<MainMenuItems> MList = new List<MainMenuItems>();
            MainMenuItems Mt;
            SubMenuItems st;
            HDModel login = new HDModel();
            login.CompanyId = main.CompanyId;
            string DB = _ReturnDB(login);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_ROLE_BASED_MAIN_MENU");
                sp.Command.AddParameter("@Rol_id", main.RoleId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                return new { MenuListByRole = ds.Tables[0] };
            }



        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //Multiple Image Save
    public object UploadMultipleImages(HttpRequest httpRequest)
    {
        try
        {
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {
                HDModel hd = new HDModel();
                hd.CompanyId = httpRequest.Params["CompanyId"];
                string DB = _ReturnDB(hd);
                String LCM_CODE = httpRequest.Params["LCM_CODE"];
                String INSP_BY = httpRequest.Params["INSP_BY"];
                String INSP_DT = httpRequest.Params["INSP_DT"];
                String AUR_ID = httpRequest.Params["AUR_ID"];
                var req = httpRequest.Files[i];
                var fn = req.FileName;

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(DB + "." + "API_HD_INSERT_CHECKLIST_FILES");
                sp2.Command.AddParameter("@LCM_CODE", LCM_CODE, DbType.String);
                sp2.Command.AddParameter("@INSP_BY", INSP_BY, DbType.String);
                sp2.Command.AddParameter("@INSP_DT", INSP_DT, DbType.String);
                sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", AUR_ID, DbType.String);
                sp2.ExecuteScalar();
                var path = HttpRuntime.AppDomainAppPath;
                var filePath = path + "UploadFiles\\" + DB + "\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn;
                req.SaveAs(filePath);
            }
            string Result;
            return new { Result = "Success" };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    //Insert Contact Details


    //public object InsertContactDetails(HD_INSERT_CONTACT_DETAILS reldet)
    //{
    //    try
    //    {

    //        HDModel hd = new HDModel();
    //        hd.CompanyId = reldet.CompanyId;
    //        string DB = _ReturnDB(hd);
    //        SqlParameter[] param = new SqlParameter[4];
    //        param[0] = new SqlParameter("@FNAME", SqlDbType.Structured);
    //        param[0].Value = reldet.Full_Name;
    //        param[1] = new SqlParameter("@CNAME", SqlDbType.NVarChar);
    //        param[1].Value = reldet.Company_Name;
    //        param[2] = new SqlParameter("@EMAIL", SqlDbType.NVarChar);
    //        param[2].Value = reldet.Email;
    //        param[3] = new SqlParameter("@COUNTRY", SqlDbType.NVarChar);
    //        param[3].Value = reldet.Country;
    //        param[4] = new SqlParameter("@PHONENUM", SqlDbType.NVarChar);
    //        param[4].Value = reldet.PhoneNo;

    //        int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DB + ".HD_INSERT_CONTACT_DET", param);

    //        if (RETVAL == 1)
    //            return new { Message = MessagesVM.UM_OK, data = reldet.Email };
    //        else
    //            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler erhndlr = new ErrorHandler();
    //        erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };

    //    }


    //}
    //Space Release Details
    public object GetLegendsSummary(Maplistfloors svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_CONSOLIDATE_REPORT_MAP_LEGEND");
            sp.Command.Parameters.Add("@FLR_CODE", svm.FLR_CODE, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", svm.AURID, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", 1, DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables.Count != 0)
                return new { data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object GetAllocEmpDetails(Space_mapVM svm)
    {
        HDModel hd = new HDModel();
        hd.CompanyId = svm.CompanyId;
        string DB = _ReturnDB(hd);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_ALLOC_EMP_DETAILS");
        sp.Command.Parameters.Add("@LCM_CODE", svm.lcm_code, DbType.String);
        sp.Command.Parameters.Add("@TWR_CODE", svm.twr_code, DbType.String);
        sp.Command.Parameters.Add("@FLR_CODE", svm.flr_code, DbType.String);
        sp.Command.Parameters.Add("@ITEM", svm.Item, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", 1, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetLegendsCount(Maplistfloors svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "MOB_GET_CONSOLIDATE_REPORT_MAP");
            sp.Command.Parameters.Add("@FLR_CODE", svm.FLR_CODE, DbType.String);
            sp.Command.Parameters.Add("@FLAG", svm.FLR_USR_MAP, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", 1, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", svm.AURID, DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables.Count != 0)
                return new { data = ds.Tables };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }

    }

    public object GetShifts(Locationlstt loc)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = loc.CompanyId;
        string DB = _ReturnDB(mo);
        List<Shiftlst> Shflst = new List<Shiftlst>();
        Shiftlst shf;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@LOC", SqlDbType.NVarChar);
        param[0].Value = loc.LCM_CODE;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, DB + ".HD_SMS_GET_SHIFTS_BY_LOC", param))
        {
            while (sdr.Read())
            {
                shf = new Shiftlst();
                shf.SH_CODE = sdr["SH_CODE"].ToString();
                shf.SH_NAME = sdr["SH_NAME"].ToString();
                shf.SH_LOC_ID = sdr["SH_LOC_ID"].ToString();
                shf.SH_SEAT_TYPE = sdr["SH_SEAT_TYPE"].ToString();
                shf.REP_COL = sdr["REP_COL"].ToString();
                shf.ticked = false;
                Shflst.Add(shf);
            }
            sdr.Close();
            if (Shflst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = Shflst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }

    }
    public object GeyFloorbyentity(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<FloorDetails> FLT = new List<FloorDetails>();
                    FloorDetails FL;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "USP_getFloorsentity");
                    sp.Command.AddParameter("@Aurid", RType.Floor, DbType.String);
                    sp.Command.AddParameter("@Entity", RType.Problem, DbType.String);

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            FL = new FloorDetails();
                            FL.FLR_NAME = sdr["FLR_NAME"].ToString();
                            FL.FLR_CODE = sdr["FLR_CODE"].ToString();
                            FLT.Add(FL);
                        }
                    }
                    return new { Status = "true", items = FLT, total_count = FLT.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }


    //QRCODE STATUS UPDATE for OPTUM

    public object _QRCodeStatus(QR_CODE_STUS HDSAC)
    {
        HDModel mn = new HDModel();
        mn.CompanyId = HDSAC.companyId;
        string DB = _ReturnDB(mn);
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "QR_CODE_UPDATE_STATUS");
        DataSet ds = new DataSet();
        sp.Command.Parameters.Add("@SSAD_AUR_ID", HDSAC.AUR_ID, DbType.String);
        sp.Command.Parameters.Add("@SSAD_SRN_REQ_ID", HDSAC.SSAD_SRN_REQ_ID, DbType.String);
        sp.Command.Parameters.Add("@QR_STATUS", HDSAC.QR_STATUS, DbType.String);
        sp.Command.Parameters.Add("@QR_CODE", HDSAC.QR_CODE, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object DIRECTFINDPATH(QR_CODE_STUS HDSAC)
    {
        HDModel mn = new HDModel();
        mn.CompanyId = HDSAC.companyId;
        string DB = _ReturnDB(mn);
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "DIRECTFINDPATH");
        DataSet ds = new DataSet();
        sp.Command.Parameters.Add("@AUR_ID", HDSAC.AUR_ID, DbType.String);
        sp.Command.Parameters.Add("@SELECT_SPC_ID", HDSAC.SSAD_SRN_REQ_ID, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data1 = ds.Tables[1], data2 = ds.Tables[2] };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null, data1 = (object)null, data2 = (object)null };
    }

    //Get space from EmpID for OPTUM

    public object _GetSpaceFromEMPID(QR_CODE_STUS HDSAC)
    {
        HDModel mn = new HDModel();
        mn.CompanyId = HDSAC.companyId;
        string DB = _ReturnDB(mn);
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GETSPACEFROMEMPID");
        DataSet ds = new DataSet();
        sp.Command.Parameters.Add("@AUR_ID", HDSAC.AUR_ID, DbType.String);
        sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int64);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = MessagesVM.UM_OK, data = ds };
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object ReleaseSelectedseat(HD_SPACE_REL_DETAILS reldet)
    {
        try
        {

            HDModel hd = new HDModel();
            hd.CompanyId = reldet.CompanyId;
            string DB = _ReturnDB(hd);
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@REL_DET", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(reldet.sad);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = reldet.User_Id;
            param[2] = new SqlParameter("@RELTYPE", SqlDbType.NVarChar);
            param[2].Value = reldet.reltype;
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
            param[3].Value = 1;

            int RETVAL = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DB + ".SMS_SPACE_REL_MAP", param);

            if (RETVAL == 1)
                return new { Message = MessagesVM.UM_OK, data = reldet.reltype };
            else
                return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };

        }


    }
    public Object GetBookedSeatsByUser(HD_SPACE_ALLOCATE_DETAILS_LST svm)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = svm.COMPANYID;
            string DB = _ReturnDB(mo);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "MOB_SPACE_SEAT_BOOKING_HISTORY");
            sp.Command.Parameters.Add("@USER", svm.AUR_ID, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //Space Map Details
    public object GetBindMapDetails(SpaceMapDet svm)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = svm.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(hd.UserId));
            DataSet ds = new DataSet();
            var fileContent = "";
            if (svm.key_value == 1)
            {
                var isError = false;
                fileContent = ReadFile(svm.flr_code, DB, out isError);

                if (isError)
                {
                    sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_FLOORMAPS_View_In_Map");
                    sp.Command.Parameters.Add("@AUR_ID", svm.User_Id, DbType.String);
                    sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                    ds = sp.GetDataSet();
                    Hashtable sendData = new Hashtable();

                    foreach (DataRow drIn in ds.Tables[1].Rows)
                    {
                        sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                    }

                    return new { mapDetails = ds.Tables[0], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[2] };
                }
                else
                {
                    //get geomstring using FileSystem method
                    sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_FLOORMAPS_View_In_Map_GMR_V1");
                    sp.Command.Parameters.Add("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
                    sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                    ds = sp.GetDataSet();
                    Hashtable sendData = new Hashtable();

                    foreach (DataRow drIn in ds.Tables[1].Rows)
                    {
                        sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                    }
                    var geomObject = JsonConvert.DeserializeObject<List<GeomStringObject>>(fileContent);
                    var spaceItems = GeomRelatedQuery.GetSpaceItemByFloorCode(svm.flr_code, DB);
                    var spaceTypeItems = GeomRelatedQuery.GetSpaceTypes(DB);

                    var list1 = new List<GeomStringObject>();

                    list1 = geomObject.Where(e => !e.Layer.Contains("_G")).ToList();

                    var loadLayers = 0;

                    var queryTableZero = (from lst1 in list1
                                          join spcLeft in spaceItems on lst1.SpaceId equals spcLeft.SPC_ID into spcDflt
                                          from spc in spcDflt.DefaultIfEmpty()
                                          join stLeft in spaceTypeItems on (spc == null ? null : spc.SPC_LAYER) equals stLeft.SPC_TYPE_CODE into stDflt
                                          from spcType in stDflt.DefaultIfEmpty()
                                          select (new
                                          {
                                              ID = lst1.Id,
                                              SPACE_ID = lst1.SpaceId ?? "",
                                              Wkt = lst1.GeomString ?? "",
                                              flr_id = lst1.FloorId ?? "",
                                              layer = lst1.Layer ?? "",
                                              SEATTYPE = spc == null ? "" : (spc.SPACE_TYPE ?? ""),
                                              SPC_SUB_TYPE = spc == null ? "" : spc.SPC_SUB_TYPE ?? "",
                                              STAID = 1,
                                              LOADLAYERS = loadLayers,
                                              SPC_TYPE_STATUS = spcType == null ? 0 : spcType.SPC_TYPE_STATUS
                                          })).ToList();

                    var resultTableZero = queryTableZero.OrderBy(e => e.SPACE_ID)
                        .ThenByDescending(e => e.layer == "CHA" ? 0 : (e.SPC_TYPE_STATUS == 4 ? 0 : (e.SPC_TYPE_STATUS == 2 ? 1 : 0)))
                        .ToList();

                    return new { mapDetails = resultTableZero, FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[2] };
                }
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "HD_SMS_GET_FLOORMAPS");
                sp.Command.Parameters.Add("@AUR_ID", svm.User_Id, DbType.String);
                sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                ds = sp.GetDataSet();
                Hashtable sendData = new Hashtable();

                foreach (DataRow drIn in ds.Tables[2].Rows)
                {
                    sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                }

                return new { mapDetails = ds.Tables[0], mapDetails2 = ds.Tables[1], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[3] };

                //var isError = false; by vinod
                //fileContent = ReadFile(svm.flr_code, DB, out isError);
                //if (isError)
                //{
                //    sp = new SubSonic.StoredProcedure(DB + "." + "HD_SMS_GET_FLOORMAPS");
                //    sp.Command.Parameters.Add("@AUR_ID", svm.User_Id, DbType.String);
                //    sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                //    ds = sp.GetDataSet();
                //    Hashtable sendData = new Hashtable();

                //    foreach (DataRow drIn in ds.Tables[2].Rows)
                //    {
                //        sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                //    }

                //    return new { mapDetails = ds.Tables[0], mapDetails2 = ds.Tables[1], FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[3] };
                //}
                //else
                //{
                //    //get geomstring using FileSystem method

                //    sp = new SubSonic.StoredProcedure(DB + "." + "SMS_GET_FLOORMAPS_GMR_V1");
                //    sp.Command.Parameters.Add("@AUR_ID", svm.User_Id, DbType.String);
                //    sp.Command.Parameters.Add("@FLR_ID", svm.flr_code, DbType.String);
                //    ds = sp.GetDataSet();
                //    Hashtable sendData = new Hashtable();

                //    foreach (DataRow drIn in ds.Tables[2].Rows)
                //    {
                //        sendData.Add(drIn["TYPE"].ToString(), drIn["COLOR"].ToString());
                //    }
                //    var geomObject = JsonConvert.DeserializeObject<List<GeomStringObject>>(fileContent);
                //    var spaceItems = GeomRelatedQuery.GetSpaceItemByFloorCode(svm.flr_code, DB);
                //    var spaceTypeItems = GeomRelatedQuery.GetSpaceTypes(DB);

                //    var areaAndChairCountDt = ds.Tables[4];
                //    Int64 area = 0;
                //    int chairCount = 0;
                //    int loadLayers = 0;
                //    if (areaAndChairCountDt != null && areaAndChairCountDt.Rows.Count > 0)
                //    {
                //        Int64.TryParse((areaAndChairCountDt.Rows[0]["area"] == DBNull.Value ? "" : areaAndChairCountDt.Rows[0]["area"].ToString()), out area);
                //        int.TryParse((areaAndChairCountDt.Rows[0]["chairCount"] == DBNull.Value ? "" : areaAndChairCountDt.Rows[0]["chairCount"].ToString()), out chairCount);
                //    }
                //    var list1 = new List<GeomStringObject>();
                //    var list2 = new List<GeomStringObject>();

                //    if (geomObject != null)
                //    {
                //        if (area > 30000 && chairCount > 100)
                //        {
                //            list1 = geomObject.Where(e => e.Layer != "CHA" && e.Layer != "FUR" && !e.Layer.Contains("_G")).ToList();
                //            list2 = geomObject.Where(e => e.Layer == "CHA" || e.Layer == "FUR").ToList();
                //            loadLayers = 1;
                //        }
                //        else
                //        {
                //            list1 = geomObject.Where(e => !e.Layer.Contains("_G")).ToList();
                //        }
                //    }
                //    var queryTableZero = (from lst1 in list1
                //                          join spcLeft in spaceItems on lst1.SpaceId equals spcLeft.SPC_ID into spcDflt
                //                          from spc in spcDflt.DefaultIfEmpty()
                //                          join stLeft in spaceTypeItems on (spc == null ? null : spc.SPC_LAYER) equals stLeft.SPC_TYPE_CODE into stDflt
                //                          from spcType in stDflt.DefaultIfEmpty()
                //                          select (new
                //                          {
                //                              ID = lst1.Id,
                //                              SPACE_ID = lst1.SpaceId ?? "",
                //                              Wkt = lst1.GeomString ?? "",
                //                              flr_id = lst1.FloorId ?? "",
                //                              layer = lst1.Layer ?? "",
                //                              SEATTYPE = spc == null ? "" : (spc.SPACE_TYPE ?? ""),
                //                              SPC_SUB_TYPE = spc == null ? "" : spc.SPC_SUB_TYPE ?? "",
                //                              STAID = 1,
                //                              LOADLAYERS = loadLayers,
                //                              SPC_TYPE_STATUS = spcType == null ? 0 : spcType.SPC_TYPE_STATUS
                //                          })).ToList();

                //    var resultTableZero = queryTableZero.OrderBy(e => e.SPACE_ID)
                //        .ThenByDescending(e => e.layer == "CHA" ? 0 : (e.SPC_TYPE_STATUS == 4 ? 0 : (e.SPC_TYPE_STATUS == 2 ? 1 : 0)))
                //        .ToList();

                //    var queryTableOne = (from lst2 in list2
                //                         join spcLeft in spaceItems on lst2.SpaceId equals spcLeft.SPC_ID into spcDflt
                //                         from spc in spcDflt.DefaultIfEmpty()
                //                         select (new
                //                         {
                //                             ID = lst2.Id,
                //                             SPACE_ID = lst2.SpaceId ?? "",
                //                             Wkt = lst2.GeomString ?? "",
                //                             flr_id = lst2.FloorId ?? "",
                //                             layer = lst2.Layer ?? "",
                //                             SEATTYPE = spc == null ? "" : spc.SPACE_TYPE ?? "",
                //                             SPC_SUB_TYPE = spc == null ? "" : spc.SPC_SUB_TYPE ?? "",
                //                             STAID = 1,
                //                             LOADLAYERS = loadLayers
                //                         })).ToList();

                //    return new { mapDetails = resultTableZero, mapDetails2 = queryTableOne, FloorDetails = svm.flr_code, COLOR = sendData, BBOX = ds.Tables[3] };
                //}
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    // ProjectCheckList

    public object GetProjectType(HDModel main)
    {
        try
        {
            string DB = _ReturnDB(main);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                DataSet ds = new DataSet();
                sp = new SubSonic.StoredProcedure(DB + "." + "PCL_GET_PROJECTTYPES");
                ds = sp.GetDataSet();
                if (ds.Tables.Count != 0)
                    return new { ProjectType = ds.Tables[0] };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public List<Modules> GetModuleList(HDModel main)
    {
        List<Modules> ModList = new List<Modules>();
        string DB = _ReturnDB(main);
        Modules MOD;
        int i = 0;
        try

        {
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_ASSET_MODULE_CHECK_FOR_USER1");

            sp.Command.AddParameter("@TENANT_ID", main.CompanyId + ".dbo", DbType.String);

            DataSet ds = new DataSet();
            ds = sp.GetDataSet();

            for (i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                MOD = new Modules();
                MOD.TNAME = ds.Tables[0].Rows[i]["TNAME"].ToString();
                MOD.T_MODULE = ds.Tables[0].Rows[i]["T_MODULE"].ToString();
                ModList.Add(MOD);
            }
            return ModList;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return ModList;
        }
    }
    public object GetProjectCheckList(GetProjectList main)
    {
        try
        {
            int i = 0;
            List<MainMenuItems> MList = new List<MainMenuItems>();
            List<ProjectCheckList> PCList = new List<ProjectCheckList>();
            ProjectCheckList PCL;
            List<childList> childlist = new List<childList>();
            childList cl;
            List<Subchildcategory> scorelist = new List<Subchildcategory>();
            Subchildcategory sc;
            List<subchildcat1> subchildlist = new List<subchildcat1>();
            subchildcat1 scc1;
            MainMenuItems Mt;
            SubMenuItems st;
            TextBoxValues txb;

            HDModel login = new HDModel();
            login.CompanyId = main.CompanyId;
            string DB = _ReturnDB(login);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "GET_PROJECT_CHECKLIST_DETAILS");
                sp.Command.AddParameter("@PROJECTID", main.PTCode, DbType.Int32);
                //sp.Command.AddParameter("@InspectedDate", "", DbType.String);
                //sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                int childcount = 0;
                PCL = new ProjectCheckList();
                for (i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (i != 0)
                    {
                        if (ds.Tables[0].Rows[i]["Subcategory_Code"].ToString() != ds.Tables[0].Rows[i - 1]["Subcategory_Code"].ToString())
                        {
                            PCList.Add(PCL);
                            childlist = new List<childList>();
                            PCL = new ProjectCheckList();
                        }
                    }
                    PCL.Category = ds.Tables[0].Rows[i]["Category"].ToString();
                    PCL.CatCode = ds.Tables[0].Rows[i]["Category_code"].ToString();
                    PCL.SubCatCode = ds.Tables[0].Rows[i]["Subcategory_Code"].ToString();
                    PCL.SubCategory = ds.Tables[0].Rows[i]["SubCategory"].ToString();
                    PCL.Mandatory = ds.Tables[0].Rows[i]["Mandatory"].ToString();
                    if (i == 0)
                    {
                        if (childcount == 0)
                        {
                            childlist = new List<childList>();
                            childcount++;
                        }
                        cl = new childList();
                        cl.ChildCode = ds.Tables[0].Rows[i]["ChildCategoryCode"].ToString();
                        cl.ChildCategory = ds.Tables[0].Rows[i]["ChildCategory"].ToString();
                        cl.childfiled = ds.Tables[0].Rows[i]["childfiled"].ToString();

                        scorelist = new List<Subchildcategory>();
                        sc = new Subchildcategory();

                        subchildlist = new List<subchildcat1>();


                        string a = ds.Tables[0].Rows[i]["PM_CL_SCC_CODE"].ToString();
                        string b = ds.Tables[0].Rows[i]["PM_CL_SCC_NAME"].ToString();
                        string c = ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString();
                        string d = ds.Tables[0].Rows[i]["PM_CL_SCC1_NAME"].ToString();
                        string e = ds.Tables[0].Rows[i]["PM_SCC1_R_C_T"].ToString();
                        string[] authorsList = a.Split(',');
                        string[] authorsList1 = b.Split(',');
                        string[] subchildcatList = c.Split(',');
                        string[] subchildcatListName = d.Split(',');
                        string[] subchildcatListfield = e.Split(',');
                        int j = 0, h = 0;
                        foreach (string author in authorsList)
                        {
                            sc = new Subchildcategory();
                            sc.ScoreCode = author;
                            sc.ScoreName = authorsList1[j];
                            sc.ScoreText = ds.Tables[0].Rows[i]["PM_CL_SCC_RC"].ToString();
                            int z = 0;
                            if (ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString() != "")
                            {
                                for (int ss = 0; ss < 2; ss++)
                                {
                                    scc1 = new subchildcat1();
                                    scc1.SubchildcatCode = subchildcatList[h];
                                    scc1.Subchildcatname = subchildcatListName[h];
                                    scc1.Subchildcatfield = subchildcatListfield[h];
                                    subchildlist.Add(scc1);
                                    h++;
                                }
                            }

                            sc.subchildcat1 = subchildlist;
                            scorelist.Add(sc);
                            j++;
                        }
                        cl.Subchildcategory = scorelist;
                        childlist.Add(cl);
                        PCL.childlist = childlist;
                    }
                    else if (ds.Tables[0].Rows[i]["Subcategory_Code"].ToString() == ds.Tables[0].Rows[i - 1]["Subcategory_Code"].ToString())
                    {
                        if (childcount == 0)
                        {
                            PCL.childlist = new List<childList>();
                            childcount++;
                        }
                        cl = new childList();
                        cl.ChildCode = ds.Tables[0].Rows[i]["ChildCategoryCode"].ToString();
                        cl.ChildCategory = ds.Tables[0].Rows[i]["ChildCategory"].ToString();
                        cl.childfiled = ds.Tables[0].Rows[i]["childfiled"].ToString();

                        scorelist = new List<Subchildcategory>();
                        sc = new Subchildcategory();

                        subchildlist = new List<subchildcat1>();
                        scc1 = new subchildcat1();
                        string a = ds.Tables[0].Rows[i]["PM_CL_SCC_CODE"].ToString();
                        string b = ds.Tables[0].Rows[i]["PM_CL_SCC_NAME"].ToString();
                        string c = ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString();
                        string d = ds.Tables[0].Rows[i]["PM_CL_SCC1_NAME"].ToString();
                        string e = ds.Tables[0].Rows[i]["PM_SCC1_R_C_T"].ToString();
                        string[] authorsList = a.Split(',');
                        string[] authorsList1 = b.Split(',');
                        string[] subchildcatList = c.Split(',');
                        string[] subchildcatListName = d.Split(',');
                        string[] subchildcatListfield = e.Split(',');
                        int j = 0, h = 0;
                        foreach (string author in authorsList)
                        {
                            sc = new Subchildcategory();
                            sc.ScoreCode = author;
                            sc.ScoreName = authorsList1[j];
                            sc.ScoreText = ds.Tables[0].Rows[i]["PM_CL_SCC_RC"].ToString();
                            int z = 0;
                            if (ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString() != "")
                            {
                                for (int ss = 0; ss < 2; ss++)
                                {
                                    if (subchildcatList.Length != h)
                                    {
                                        scc1 = new subchildcat1();
                                        scc1.SubchildcatCode = subchildcatList[h];
                                        scc1.Subchildcatname = subchildcatListName[h];
                                        scc1.Subchildcatfield = subchildcatListfield[h];
                                        subchildlist.Add(scc1);
                                        h++;
                                    }
                                }
                            }

                            sc.subchildcat1 = subchildlist;
                            scorelist.Add(sc);
                            subchildlist = new List<subchildcat1>();
                            j++;
                        }
                        cl.Subchildcategory = scorelist;
                        childlist.Add(cl);
                        PCL.childlist = childlist;
                    }
                    else
                    {
                        childcount = 0;
                        if (childcount == 0)
                        {
                            PCL.childlist = new List<childList>();
                            childcount++;
                        }
                        cl = new childList();
                        cl.ChildCode = ds.Tables[0].Rows[i]["ChildCategoryCode"].ToString();
                        cl.ChildCategory = ds.Tables[0].Rows[i]["ChildCategory"].ToString();
                        cl.childfiled = ds.Tables[0].Rows[i]["childfiled"].ToString();
                        scorelist = new List<Subchildcategory>();
                        sc = new Subchildcategory();

                        subchildlist = new List<subchildcat1>();
                        scc1 = new subchildcat1();
                        string a = ds.Tables[0].Rows[i]["PM_CL_SCC_CODE"].ToString();
                        string b = ds.Tables[0].Rows[i]["PM_CL_SCC_NAME"].ToString();
                        string c = ds.Tables[0].Rows[i]["PM_CL_SCC1_CODE"].ToString();
                        string[] authorsList = a.Split(',');
                        string[] authorsList1 = b.Split(',');
                        string[] subchildcatList = c.Split(',');
                        int j = 0;
                        foreach (string author in authorsList)
                        {
                            sc = new Subchildcategory();
                            sc.ScoreCode = author;
                            sc.ScoreName = authorsList1[j];
                            sc.ScoreText = ds.Tables[0].Rows[i]["PM_CL_SCC_RC"].ToString();
                            scorelist.Add(sc);
                            j++;
                        }
                        cl.Subchildcategory = scorelist;
                        childlist.Add(cl);
                        PCL.childlist = childlist;
                    }
                }
                if (i == ds.Tables[0].Rows.Count)
                {
                    PCList.Add(PCL);
                }
                return new { Status = "true", Data = PCList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }



    public object InsertPPSaveAsDraftData(ProposalDraftDetails dataobj)
    {
        try
        {
            List<ImageClas> Imgclass = new List<ImageClas>();

            List<ImageClas> Imgclass1 = new List<ImageClas>();

            List<ImageClas> Imgclass2 = new List<ImageClas>();

            List<ImageClas> Imgclass3 = new List<ImageClas>();

            HDModel hd = new HDModel();
            hd.CompanyId = dataobj.CompanyId;
            string DB = _ReturnDB(hd);

            if (dataobj.SIGNAGEIMAGES != null)
            {
                Imgclass.Add(dataobj.SIGNAGEIMAGES);
            }
            if (dataobj.AC_OUTDOOR_IMAGE != null)
            {
                Imgclass1.Add(dataobj.AC_OUTDOOR_IMAGE);
            }
            if (dataobj.GSB_IMAGE != null)
            {
                Imgclass2.Add(dataobj.GSB_IMAGE);
            }
            if (dataobj.IMAGES != null)
            {
                Imgclass3 = dataobj.IMAGES;
            }

            String proc_name = DB + ".PM_PROPERTY_SAVEASDRAFT_DETAILS_MOBILE";
            SqlParameter[] param = new SqlParameter[133];
            param[0] = new SqlParameter("@REQUEST_ID", SqlDbType.VarChar);
            param[0].Value = dataobj.PM_PPT_PM_REQ_ID;
            param[1] = new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar);
            param[1].Value = dataobj.PM_PPT_REQ_TYPE;
            param[2] = new SqlParameter("@PROP_NATURE", SqlDbType.VarChar);
            param[2].Value = dataobj.PM_PPT_NATURE;
            param[3] = new SqlParameter("@ACQ_TRH", SqlDbType.VarChar);
            param[3].Value = dataobj.PM_PPT_ACQ_THR;
            param[4] = new SqlParameter("@CITY", SqlDbType.VarChar);
            param[4].Value = dataobj.PM_PPT_CTY_CODE;
            param[5] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            param[5].Value = dataobj.PM_PPT_LOC_CODE;
            param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
            param[6].Value = dataobj.PM_PPT_TOW_CODE;
            param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
            param[7].Value = dataobj.PM_PPT_FLR_CODE;
            param[8] = new SqlParameter("@TOI_BLKS", SqlDbType.Int);
            param[8].Value = dataobj.PM_PPT_TOT_TOI;
            param[9] = new SqlParameter("@PROP_TYPE", SqlDbType.VarChar);
            param[9].Value = dataobj.PM_PPT_TYPE;
            param[10] = new SqlParameter("@PROP_NAME", SqlDbType.VarChar);
            param[10].Value = dataobj.PM_PPT_NAME;
            param[11] = new SqlParameter("@ESTD_YR", SqlDbType.DateTime);
            param[11].Value = dataobj.PM_PPT_ESTD;
            param[12] = new SqlParameter("@AGE", SqlDbType.VarChar);
            param[12].Value = dataobj.PM_PPT_AGE;
            param[13] = new SqlParameter("@PROP_ADDR", SqlDbType.VarChar);
            param[13].Value = dataobj.PM_PPT_ADDRESS;
            param[14] = new SqlParameter("@SING_LOC", SqlDbType.VarChar);
            param[14].Value = dataobj.PM_PPT_SIGN_LOC;
            param[15] = new SqlParameter("@SOC_NAME", SqlDbType.VarChar);
            param[15].Value = dataobj.PM_PPT_SOC_NAME;
            param[16] = new SqlParameter("@LATITUDE", SqlDbType.VarChar);
            param[16].Value = dataobj.PM_PPT_LAT;
            param[17] = new SqlParameter("@LONGITUDE", SqlDbType.VarChar);
            param[17].Value = dataobj.PM_PPT_LONG;
            param[18] = new SqlParameter("@SCOPE_WK", SqlDbType.VarChar);
            param[18].Value = dataobj.PM_PPT_OWN_SCOPE;
            param[19] = new SqlParameter("@RECM_PROP", SqlDbType.VarChar);
            param[19].Value = dataobj.PM_PPT_RECOMMENDED;
            param[20] = new SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar);
            param[20].Value = dataobj.PM_PPT_RECO_REM;
            //Owner Details
            param[21] = new SqlParameter("@OWN_NAME", SqlDbType.VarChar);
            param[21].Value = dataobj.PM_OWN_NAME;
            param[22] = new SqlParameter("@OWN_PH", SqlDbType.VarChar);
            param[22].Value = dataobj.PM_OWN_PH_NO;
            param[23] = new SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar);
            param[23].Value = dataobj.PM_PREV_OWN_NAME;
            param[24] = new SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar);
            param[24].Value = dataobj.PM_PREV_OWN_PH_NO;
            param[25] = new SqlParameter("@OWN_EMAIL", SqlDbType.VarChar);
            param[25].Value = dataobj.PM_OWN_EMAIL;
            //Area Details
            param[26] = new SqlParameter("@CARPET_AREA", SqlDbType.Decimal);
            param[26].Value = Convert.ToDecimal(dataobj.PM_AR_CARPET_AREA);
            param[27] = new SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal);
            param[27].Value = Convert.ToDecimal(dataobj.PM_AR_BUA_AREA);
            param[28] = new SqlParameter("@COMMON_AREA", SqlDbType.Decimal);
            param[28].Value = Convert.ToDecimal(dataobj.PM_AR_COM_AREA);
            param[29] = new SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal);
            param[29].Value = Convert.ToDecimal(dataobj.PM_AR_RENT_AREA);
            param[30] = new SqlParameter("@USABLE_AREA", SqlDbType.Decimal);
            param[30].Value = Convert.ToDecimal(dataobj.PM_AR_USABEL_AREA);
            param[31] = new SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal);
            param[31].Value = Convert.ToDecimal(dataobj.PM_AR_SBU_AREA);
            param[32] = new SqlParameter("@PLOT_AREA", SqlDbType.Decimal);
            param[32].Value = Convert.ToDecimal(dataobj.PM_AR_PLOT_AREA);
            param[33] = new SqlParameter("@FTC_HIGHT", SqlDbType.Decimal);
            param[33].Value = dataobj.PM_AR_FTC_HIGHT;
            param[34] = new SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal);
            param[34].Value = dataobj.PM_AR_FTBB_HIGHT;
            param[35] = new SqlParameter("@MAX_CAPACITY", SqlDbType.Int);
            param[35].Value = Convert.ToInt32(dataobj.PM_AR_MAX_CAP);
            param[36] = new SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int);
            param[36].Value = dataobj.PM_AR_OPT_CAP;
            param[37] = new SqlParameter("@SEATING_CAPACITY", SqlDbType.Int);
            param[37].Value = dataobj.PM_AR_SEATING_CAP;
            param[38] = new SqlParameter("@FLR_TYPE", SqlDbType.VarChar);
            param[38].Value = dataobj.PM_AR_FLOOR_TYPE;
            param[39] = new SqlParameter("@FSI", SqlDbType.VarChar);
            param[39].Value = dataobj.PM_AR_FSI;
            param[40] = new SqlParameter("@FSI_RATIO", SqlDbType.Decimal);
            param[40].Value = dataobj.PM_AR_FSI_RATIO;
            param[41] = new SqlParameter("@PFR_EFF", SqlDbType.VarChar);
            param[41].Value = dataobj.PM_AR_PREF_EFF;
            //PURCHASE DETAILS
            param[42] = new SqlParameter("@PUR_PRICE", SqlDbType.Decimal);
            param[42].Value = dataobj.PM_PUR_PRICE;
            param[43] = new SqlParameter("@PUR_DATE", SqlDbType.DateTime);
            param[43].Value = dataobj.PM_PUR_DATE;
            param[44] = new SqlParameter("@MARK_VALUE", SqlDbType.Decimal);
            param[44].Value = dataobj.PM_PUR_MARKET_VALUE;
            //GOVT DETAILS
            param[45] = new SqlParameter("@IRDA", SqlDbType.VarChar);
            param[45].Value = dataobj.PM_GOV_IRDA;
            param[46] = new SqlParameter("@PC_CODE", SqlDbType.VarChar);
            param[46].Value = dataobj.PM_GOV_PC_CODE;
            param[47] = new SqlParameter("@PROP_CODE", SqlDbType.VarChar);
            param[47].Value = dataobj.PM_GOV_PROP_CODE;
            param[48] = new SqlParameter("@UOM_CODE", SqlDbType.VarChar);
            param[48].Value = dataobj.PM_GOV_UOM_CODE;
            //INSURANCE DETAILS
            param[49] = new SqlParameter("@IN_TYPE", SqlDbType.VarChar);
            param[49].Value = dataobj.PM_INS_TYPE;
            param[50] = new SqlParameter("@IN_VENDOR", SqlDbType.VarChar);
            param[50].Value = dataobj.PM_INS_VENDOR;
            param[51] = new SqlParameter("@IN_AMOUNT", SqlDbType.Decimal);
            param[51].Value = dataobj.PM_INS_AMOUNT;
            param[52] = new SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar);
            param[52].Value = dataobj.PM_INS_PNO;
            param[53] = new SqlParameter("@IN_SDATE", SqlDbType.DateTime);
            param[53].Value = dataobj.PM_INS_START_DT;
            param[54] = new SqlParameter("@IN_EDATE", SqlDbType.DateTime);
            param[54].Value = dataobj.PM_INS_END_DT;
            param[55] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[55].Value = dataobj.AUR_ID;
            param[56] = new SqlParameter("@CMP_ID", SqlDbType.VarChar);
            param[56].Value = 1;
            param[57] = new SqlParameter("@OWN_LANDLINE", SqlDbType.VarChar);
            param[57].Value = dataobj.PM_OWN_LANDLINE;
            param[58] = new SqlParameter("@POA_SIGNED", SqlDbType.VarChar);
            param[58].Value = dataobj.PM_POA_SIGN;
            //POA Details
            param[59] = new SqlParameter("@POA_NAME", SqlDbType.VarChar);
            param[59].Value = dataobj.PM_POA_NAME;
            param[60] = new SqlParameter("@POA_ADDRESS", SqlDbType.VarChar);
            param[60].Value = dataobj.PM_POA_ADDRESS;
            param[61] = new SqlParameter("@POA_MOBILE", SqlDbType.VarChar);
            param[61].Value = dataobj.PM_POA_MOBILE;
            param[62] = new SqlParameter("@POA_EMAIL", SqlDbType.VarChar);
            param[62].Value = dataobj.PM_POA_EMAIL;
            param[63] = new SqlParameter("@POA_LLTYPE", SqlDbType.VarChar);
            param[63].Value = dataobj.PM_POA_LL_TYPE;
            param[64] = new SqlParameter("@IMAGES", SqlDbType.Structured);
            param[64].Value = UtilityService.ConvertToDataTable(Imgclass3);
            param[65] = new SqlParameter("@ENTITY", SqlDbType.VarChar);
            param[65].Value = dataobj.PM_PPT_ENTITY;
            //Physical Condition
            param[66] = new SqlParameter("@PHYCDTN_WALLS", SqlDbType.VarChar);
            param[66].Value = dataobj.PM_PHYCDTN_WALLS;
            param[67] = new SqlParameter("@PHYCDTN_ROOF", SqlDbType.VarChar);
            param[67].Value = dataobj.PM_PHYCDTN_ROOF;
            param[68] = new SqlParameter("@PHYCDTN_CEILING", SqlDbType.VarChar);
            param[68].Value = dataobj.PM_PHYCDTN_CEILING;
            param[69] = new SqlParameter("@PHYCDTN_WINDOWS", SqlDbType.VarChar);
            param[69].Value = dataobj.PM_PHYCDTN_WINDOWS;
            param[70] = new SqlParameter("@PHYCDTN_DAMAGE", SqlDbType.VarChar);
            param[70].Value = dataobj.PM_PHYCDTN_DAMAGE;
            param[71] = new SqlParameter("@PHYCDTN_SEEPAGE", SqlDbType.VarChar);
            param[71].Value = dataobj.PM_PHYCDTN_SEEPAGE;
            param[72] = new SqlParameter("@PHYCDTN_OTHRTNT", SqlDbType.VarChar);
            param[72].Value = dataobj.PM_PHYCDTN_OTHR_TNT;

            //Landlord's Scope of work
            param[73] = new SqlParameter("@LL_VITRIFIED", SqlDbType.VarChar);
            param[73].Value = dataobj.PM_LLS_VITRIFIED;
            param[74] = new SqlParameter("@LL_VITRIFIED_RMKS", SqlDbType.VarChar);
            param[74].Value = dataobj.PM_LLS_VITRIFIED_RMKS;
            param[75] = new SqlParameter("@LL_WASHRMS", SqlDbType.VarChar);
            param[75].Value = dataobj.PM_LLS_WASHROOMS;
            param[76] = new SqlParameter("@LL_WASHRM_RMKS", SqlDbType.VarChar);
            param[76].Value = dataobj.PM_LLS_WASH_RMKS;
            param[77] = new SqlParameter("@LL_PANTRY", SqlDbType.VarChar);
            param[77].Value = dataobj.PM_LLS_PANTRY;
            param[78] = new SqlParameter("@LL_PANTRY_RMKS", SqlDbType.VarChar);
            param[78].Value = dataobj.PM_LLS_PANTRY_RMKS;
            param[79] = new SqlParameter("@LL_SHUTTER", SqlDbType.VarChar);
            param[79].Value = dataobj.PM_LLS_SHUTTER;
            param[80] = new SqlParameter("@LL_SHUTTER_RMKS", SqlDbType.VarChar);
            param[80].Value = dataobj.PM_LLS_SHUTTER_RMKS;
            param[81] = new SqlParameter("@LL_OTHERS", SqlDbType.VarChar);
            param[81].Value = dataobj.PM_LLS_OTHERS;
            param[82] = new SqlParameter("@LL_OTHERS_RMKS", SqlDbType.VarChar);
            param[82].Value = dataobj.PM_LLS_OTHERS_RMKS;
            param[83] = new SqlParameter("@LL_WORK_DAYS", SqlDbType.VarChar);
            param[83].Value = dataobj.PM_LLS_WORK_DAYS;
            param[84] = new SqlParameter("@LL_ELEC_EXISTING", SqlDbType.VarChar);
            param[84].Value = dataobj.PM_LLS_ELEC_EXISTING;
            param[85] = new SqlParameter("@LL_ELEC_REQUIRED", SqlDbType.VarChar);
            param[85].Value = dataobj.PM_LLS_ELEC_REQUIRED;
            //Other Details
            param[86] = new SqlParameter("@DOC_TAX", SqlDbType.VarChar);
            param[86].Value = dataobj.PM_DOC_TAX;
            param[87] = new SqlParameter("@DOC_TAX_RMKS", SqlDbType.VarChar);
            param[87].Value = dataobj.PM_DOC_TAX_RMKS;
            param[88] = new SqlParameter("@DOC_OTHR_INFO", SqlDbType.VarChar);
            param[88].Value = dataobj.PM_DOC_OTHER_INFO;
            param[89] = new SqlParameter("@OTHR_FLOORING", SqlDbType.VarChar);
            param[89].Value = dataobj.PM_OTHR_FLOORING;
            param[90] = new SqlParameter("@OTHR_FLRG_RMKS", SqlDbType.VarChar);
            param[90].Value = dataobj.PM_OTHR_FLRG_RMKS;
            param[91] = new SqlParameter("@OTHR_WASH_EXISTING", SqlDbType.VarChar);
            param[91].Value = dataobj.PM_OTHR_WASHRM_EXISTING;
            param[92] = new SqlParameter("@OTHR_WASH_REQUIRED", SqlDbType.VarChar);
            param[92].Value = dataobj.PM_OTHR_WASHRM_REQUIRED;
            param[93] = new SqlParameter("@OTHR_POTABLE_WTR", SqlDbType.VarChar);
            param[93].Value = dataobj.PM_OTHR_POTABLE_WTR;

            //Cost Details
            param[94] = new SqlParameter("@COST_RENT", SqlDbType.VarChar);
            param[94].Value = dataobj.PM_COST_RENT;
            param[95] = new SqlParameter("@COST_RENT_SFT", SqlDbType.VarChar);
            param[95].Value = dataobj.PM_COST_RENT_SFT;
            param[96] = new SqlParameter("@COST_RATIO", SqlDbType.VarChar);
            param[96].Value = dataobj.PM_COST_RATIO;
            param[97] = new SqlParameter("@COST_OWN_SHARE", SqlDbType.VarChar);
            param[97].Value = dataobj.PM_COST_OWN_SHARE;
            param[98] = new SqlParameter("@COST_SECDEP", SqlDbType.VarChar);
            param[98].Value = dataobj.PM_COST_SECDEPOSIT;
            param[99] = new SqlParameter("@COST_GST", SqlDbType.VarChar);
            param[99].Value = dataobj.PM_COST_GST;
            param[100] = new SqlParameter("@COST_MAINTENANCE", SqlDbType.VarChar);
            param[100].Value = dataobj.PM_COST_MAINTENANCE;
            param[101] = new SqlParameter("@COST_ESC", SqlDbType.VarChar);
            param[101].Value = dataobj.PM_COST_ESC_RENTALS;
            param[102] = new SqlParameter("@COST_RENT_FREE", SqlDbType.VarChar);
            param[102].Value = dataobj.PM_COST_RENT_FREE;
            param[103] = new SqlParameter("@COST_STAMP", SqlDbType.VarChar);
            param[103].Value = dataobj.PM_COST_STAMP;
            param[104] = new SqlParameter("@COST_AGREE", SqlDbType.VarChar);
            param[104].Value = dataobj.PM_COST_AGREEMENT_PERIOD;
            //DOCUMENTS VAILABLE
            param[105] = new SqlParameter("@DOC_TITLE", SqlDbType.VarChar);
            param[105].Value = dataobj.PM_DOC_TITLE;
            param[106] = new SqlParameter("@DOC_TITLE_RMKS", SqlDbType.VarChar);
            param[106].Value = dataobj.PM_DOC_TITLLE_RMKS;
            param[107] = new SqlParameter("@DOC_OCCUP", SqlDbType.VarChar);
            param[107].Value = dataobj.PM_DOC_OCCUPANCY;
            param[108] = new SqlParameter("@DOC_OCCUP_RMKS", SqlDbType.VarChar);
            param[108].Value = dataobj.PM_DOC_OCC_RMKS;
            param[109] = new SqlParameter("@DOC_BUILD", SqlDbType.VarChar);
            param[109].Value = dataobj.PM_DOC_BUILD;
            param[110] = new SqlParameter("@DOC_BUILD_RMKS", SqlDbType.VarChar);
            param[110].Value = dataobj.PM_DOC_BUILD_RMKS;
            param[111] = new SqlParameter("@DOC_PAN", SqlDbType.VarChar);
            param[111].Value = dataobj.PM_DOC_PAN;
            param[112] = new SqlParameter("@DOC_PAN_RMKS", SqlDbType.VarChar);
            param[112].Value = dataobj.PM_DOC_PAN_RMKS;

            param[113] = new SqlParameter("@SIGNAGEIMAGES", SqlDbType.Structured);
            param[113].Value = UtilityService.ConvertToDataTable(Imgclass);
            param[114] = new SqlParameter("@SIG_LENGTH", SqlDbType.VarChar);
            param[114].Value = dataobj.PM_PPT_SIG_LENGTH;
            param[115] = new SqlParameter("@SIG_WIDTH", SqlDbType.VarChar);
            param[115].Value = dataobj.PM_PPT_SIG_WIDTH;
            param[116] = new SqlParameter("@AC_OUTDOOR", SqlDbType.VarChar);
            param[116].Value = dataobj.PM_PPT_AC_OUTDOOR;
            param[117] = new SqlParameter("@GSB", SqlDbType.VarChar);
            param[117].Value = dataobj.PM_PPT_GSB;
            param[118] = new SqlParameter("@AC_OUTDOOR_IMAGE", SqlDbType.Structured);
            param[118].Value = UtilityService.ConvertToDataTable(Imgclass1);
            param[119] = new SqlParameter("@GSB_IMAGE", SqlDbType.Structured);
            param[119].Value = UtilityService.ConvertToDataTable(Imgclass2);
            param[120] = new SqlParameter("@INSPECTION_DATE", SqlDbType.DateTime);
            param[120].Value = dataobj.PM_PPT_INSPECTED_DT;
            param[121] = new SqlParameter("@INSPECTION_BY", SqlDbType.VarChar);
            param[121].Value = dataobj.PM_PPT_INSPECTED_BY;
            param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
            param[122].Value = dataobj.PM_PPT_TOT_FLRS;
            param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
            param[123].Value = dataobj.PM_PPT_RELOC_NAME;
            param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
            param[124].Value = dataobj.PM_PPT_RELOC_ADDRESS;
            param[125] = new SqlParameter("@OWN_ADDRESS", SqlDbType.VarChar);
            param[125].Value = dataobj.PM_OWN_ADDRESS;
            param[126] = new SqlParameter("@PHYCDTN_OTHRBLDG", SqlDbType.VarChar);
            param[126].Value = dataobj.PM_PHYCDTN_OTHRBU;
            param[127] = new SqlParameter("@OTHR_POTABLE_WTR_RMKS", SqlDbType.VarChar);
            param[127].Value = dataobj.PM_OTHR_POTABLE_WTR_REMARKS;
            param[128] = new SqlParameter("@OTHR_IT_INSTALL", SqlDbType.VarChar);
            param[128].Value = dataobj.PM_OTHR_IT_INSTALL;
            param[129] = new SqlParameter("@OTHR_IT_INSTALL_RMKS", SqlDbType.VarChar);
            param[129].Value = dataobj.PM_OTHR_IT_INSTALL_REMARKS;
            param[130] = new SqlParameter("@OTHR_DG_INSTALL", SqlDbType.VarChar);
            param[130].Value = dataobj.PM_OTHR_DG_INSTALL;
            param[131] = new SqlParameter("@OTHR_DG_INSTALL_RMKS", SqlDbType.VarChar);
            param[131].Value = dataobj.PM_OTHR_DG_INSTALL_RMKS;
            param[132] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            param[132].Value = "5019";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            string Result;
            return new { Result = ds.Tables[0].Rows[0]["result"].ToString() };
        }

        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }



    public object InsertCheckList(SaveProjectChecklistInsert dataobj)
    {
        try
        {
            List<BCLLocations> BCLLocationsList = new List<BCLLocations>();
            BCLLocations BCLLoc = new BCLLocations();
            HDModel hd = new HDModel();
            hd.CompanyId = dataobj.CompanyId;
            string DB = _ReturnDB(hd);
            String proc_name = DB + ".SAVE_PROJECTCHECKLIST_DETAILS_MOBILE";
            SqlParameter[] param = new SqlParameter[12];
            param[0] = new SqlParameter("@ProjectChecklistDetails", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(dataobj.SelectedLists);
            param[1] = new SqlParameter("@Country", SqlDbType.NVarChar);
            param[1].Value = dataobj.CNY_CODE;
            param[2] = new SqlParameter("@City", SqlDbType.VarChar);
            param[2].Value = dataobj.CTY_CODE;
            param[3] = new SqlParameter("@Location", SqlDbType.VarChar);
            param[3].Value = dataobj.LCM_CODE;
            param[4] = new SqlParameter("@BusinessUnit", SqlDbType.VarChar);
            param[4].Value = dataobj.VER_CODE;
            param[5] = new SqlParameter("@Inspectionby", SqlDbType.VarChar);
            param[5].Value = dataobj.INSPECTOR;
            param[6] = new SqlParameter("@ProjectType", SqlDbType.VarChar);
            param[6].Value = dataobj.PT_CODE;
            param[7] = new SqlParameter("@FromDate", SqlDbType.VarChar);
            param[7].Value = dataobj.VISITDATE;
            param[8] = new SqlParameter("@ProjectName", SqlDbType.VarChar);
            param[8].Value = dataobj.PROJECTNAME;
            param[9] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[9].Value = dataobj.AUR_ID;
            //param[10] = new SqlParameter("@COMPANY", SqlDbType.Int);
            //param[10].Value = dataobj.CompanyId;
            param[10] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[10].Value = dataobj.FLAG;
            param[11] = new SqlParameter("@OVER_ALL_CMTS", SqlDbType.VarChar);
            param[11].Value = dataobj.OVERALL_CMTS;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            string Result;
            return new { Result = ds.Tables[0].Rows[0]["result"].ToString() };
        }

        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    /// <summary>
    /// SAVE MULTPLE PROJECT CHECKLIST IMAGES
    /// </summary>
    /// <param name="httpRequest"></param>
    /// <returns></returns>
    public object UploadPCLMultipleImages(HttpRequest httpRequest)
    {
        try
        {
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {
                HDModel hd = new HDModel();
                hd.CompanyId = httpRequest.Params["CompanyId"];
                string DB = _ReturnDB(hd);
                String LCM_CODE = httpRequest.Params["LCM_CODE"];
                String INSP_BY = httpRequest.Params["INSP_BY"];
                String INSP_DT = httpRequest.Params["INSP_DT"];
                String AUR_ID = httpRequest.Params["AUR_ID"];
                var req = httpRequest.Files[i];
                var fn = req.FileName;

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(DB + "." + "API_HD_INSERT_PCL_CHECKLIST_FILES");
                sp2.Command.AddParameter("@LCM_CODE", LCM_CODE, DbType.String);
                sp2.Command.AddParameter("@INSP_BY", INSP_BY, DbType.String);
                sp2.Command.AddParameter("@INSP_DT", INSP_DT, DbType.String);
                sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", AUR_ID, DbType.String);
                sp2.ExecuteScalar();
                var path = HttpRuntime.AppDomainAppPath;
                var filePath = path + "UploadFiles\\" + DB + "\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn;
                req.SaveAs(filePath);
            }
            string Result;
            return new { Result = "Success" };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    // Get Saved List ProjectChecklist
    public object getSavedList(HDModel main)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PCL_GET_SAVED_LIST");
            sp.Command.Parameters.Add("@AUR_ID", main.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object getDashboardList(HDModel main)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_CHECKLIST_DASHBOARD");
            sp.Command.Parameters.Add("@AUR_ID", main.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    //GetSavedDraftsData
    public object GetSavedDraftsdata(GetSavedDraftsList savedDraftsList)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = savedDraftsList.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(savedDraftsList.AUR_ID));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_SAVED_PCLLIST_ITEMS_MOBILE");
            sp.Command.Parameters.Add("@LCM_CODE", savedDraftsList.LCM_CODE, DbType.String);
            sp.Command.Parameters.Add("@INSPECTEDDATE", savedDraftsList.PM_CL_MAIN_VISIT_DT, DbType.String);
            sp.Command.Parameters.Add("@FLAG", savedDraftsList.Flag, DbType.Int32);
            ds = sp.GetDataSet();
            ErrorHandler err12 = new ErrorHandler();
            err12._WriteErrorLog_string("data" + JsonConvert.SerializeObject(ds.Tables[0]));
            ErrorHandler err13 = new ErrorHandler();
            err13._WriteErrorLog_string("SelectedLists" + JsonConvert.SerializeObject(ds.Tables[0]));
            ErrorHandler err14 = new ErrorHandler();
            err14._WriteErrorLog_string("ImageList" + JsonConvert.SerializeObject(ds.Tables[1]));
            if (ds.Tables.Count != 0)

                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], SelectedLists = ds.Tables[0], ImageList = ds.Tables[1] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetPropertyProposalSavedDraft(ProSavedDraft savedDraftsList)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = savedDraftsList.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(savedDraftsList.AUR_ID));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_PROPERTYSAVEASDRAFTBY_REQS");
            sp.Command.Parameters.Add("@CMP_ID", 1, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", savedDraftsList.AUR_ID, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object GetPPSavedDetails(ProposalDraftDetails savedDraftsList)
    {
        try
        {
            HDModel hd = new HDModel();
            hd.CompanyId = savedDraftsList.CompanyId;
            string DB = _ReturnDB(hd);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(savedDraftsList.AUR_ID));
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_PRPTYSAVEASDRAFT_DETAILS");
            sp.Command.Parameters.Add("@PROP_ID", savedDraftsList.PM_PPT_PM_REQ_ID, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data1 = ds.Tables[1] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    // Get Bussiness Unit 
    public object GetBussinessUnit(HDModel main)
    {
        try
        {
            List<Verticallst> verticallst = new List<Verticallst>();
            Verticallst verticals = new Verticallst();
            string DB = _ReturnDB(main);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "Get_Vertical_ADM");
                sp.Command.Parameters.Add("@MODE", 1, DbType.Int16);
                sp.Command.Parameters.Add("@AUR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    verticals = new Verticallst();
                    verticals.VER_CODE = ds.Tables[0].Rows[i]["VER_CODE"].ToString();
                    verticals.VER_NAME = ds.Tables[0].Rows[i]["VER_NAME"].ToString();
                    verticallst.Add(verticals);
                }

                return new { verticallst = verticallst };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    // BranchCheckList
    public object GetBranchCheckList(HDModel main)
    {
        try
        {
            List<MainMenuItems> MList = new List<MainMenuItems>();
            List<BranchChecklist> BCList = new List<BranchChecklist>();
            MainMenuItems Mt;
            SubMenuItems st;
            BranchChecklist BCL;
            Score sc;
            TextBoxValues txb;
            //HDModel login = new HDModel();
            //login.CompanyId = "ABC_UAT";
            string DB = _ReturnDB(main);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_CATEGORIES");
                sp.Command.AddParameter("@LOC_ID", "", DbType.String);
                sp.Command.AddParameter("@InspectedDate", "", DbType.String);
                sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BCL = new BranchChecklist();
                    BCL.BCL_MC_CODE = ds.Tables[0].Rows[i]["BCL_MC_CODE"].ToString();
                    BCL.BCL_MC_NAME = ds.Tables[0].Rows[i]["BCL_MC_NAME"].ToString();
                    BCL.BCL_CH_SUB_CODE = ds.Tables[0].Rows[i]["BCL_CH_SUB_CODE"].ToString();
                    BCL.BCL_SUB_NAME = ds.Tables[0].Rows[i]["BCL_SUB_NAME"].ToString();
                    BCL.BCL_SUB_TYPE = ds.Tables[0].Rows[i]["BCL_SUB_TYPE"].ToString();
                    BCL.Score = new List<Score>();
                    BCL.TextBoxValues = new List<TextBoxValues>();
                    if (BCL.BCL_SUB_TYPE == "Radio")
                    {
                        string a = ds.Tables[0].Rows[i]["SCORE"].ToString();
                        string[] authorsList = a.Split(',');
                        foreach (string author in authorsList)
                        {
                            sc = new Score();
                            sc.ScoreValues = author;
                            BCL.Score.Add(sc);
                        }
                    }
                    else if (BCL.BCL_SUB_TYPE == "Text" || BCL.BCL_SUB_TYPE == "Multiple")
                    {
                        string a = ds.Tables[0].Rows[i]["SCORE"].ToString();
                        string[] authorsList = a.Split(',');
                        foreach (string author in authorsList)
                        {
                            txb = new TextBoxValues();
                            txb.Names = author;
                            BCL.TextBoxValues.Add(txb);
                        }
                    }


                    BCList.Add(BCL);

                }

                return new { Status = "true", Data = BCList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }


    //Branch CheckList Inspectors

    public object GetInspectors(Inspectors main)
    {
        try
        {
            List<Inspectors> InspectorsList = new List<Inspectors>();
            Inspectors inspect = new Inspectors();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hDModel);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_INSPECTORS");
                sp.Command.Parameters.Add("@PRJCKL", main.PRJCKL, DbType.Int16);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    inspect = new Inspectors();
                    inspect.INSPECTOR = ds.Tables[0].Rows[i]["INSPECTOR"].ToString();
                    InspectorsList.Add(inspect);
                }

                return new { InspectorsList = InspectorsList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Branch CheckList Locations

    public object GetLocations(HDModel main)
    {
        try
        {
            List<BCLLocations> BCLLocationsList = new List<BCLLocations>();
            BCLLocations BCLLoc = new BCLLocations();
            string DB = _ReturnDB(main);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_LOCATIONS");
                sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                //sp.Command.AddParameter("@InspectedDate", "", DbType.String);
                //sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BCLLoc = new BCLLocations();
                    BCLLoc.LCM_CODE = ds.Tables[0].Rows[i]["LCM_CODE"].ToString();
                    BCLLoc.LCM_NAME = ds.Tables[0].Rows[i]["LCM_NAME"].ToString();
                    BCLLoc.CTY_CODE = ds.Tables[0].Rows[i]["CTY_CODE"].ToString();
                    BCLLoc.CNY_CODE = ds.Tables[0].Rows[i]["CNY_CODE"].ToString();
                    BCLLoc.LCM_CNP = ds.Tables[0].Rows[i]["LCM_CNP"].ToString();
                    BCLLoc.CTY_NAME = ds.Tables[0].Rows[i]["CTY_NAME"].ToString();
                    BCLLocationsList.Add(BCLLoc);
                }

                return new { LocationsList = BCLLocationsList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Branch CheckList Insert/Save As Draft

    public object BCLInsert(BranchCheckListInsert main)
    {
        try
        {
            List<BCLLocations> BCLLocationsList = new List<BCLLocations>();
            BCLLocations BCLLoc = new BCLLocations();
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            int cnt;
            int Expiry;

            //String proc_name = "[" + main.CompanyId + "].[dbo].BCL_INSERT_CHECKLIST_MOBILE";
            String proc_name = DB + ".BCL_INSERT_CHECKLIST_MOBILE";

            SqlParameter[] param = new SqlParameter[11];
            param[0] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(main.SelectedLists);
            param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
            param[1].Value = main.INSPECTOR;
            param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
            param[2].Value = main.VISITDATE;
            param[3] = new SqlParameter("@SelCompany", SqlDbType.NVarChar);
            param[3].Value = main.LCM_CNP;
            param[4] = new SqlParameter("@LCM_CODE", SqlDbType.NVarChar);
            param[4].Value = main.LCM_CODE;
            param[5] = new SqlParameter("@CTY_CODE", SqlDbType.NVarChar);
            param[5].Value = main.CTY_CODE;
            param[6] = new SqlParameter("@CNY_CODE", SqlDbType.NVarChar);
            param[6].Value = main.CNY_CODE;
            param[7] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[7].Value = main.AUR_ID;
            param[8] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[8].Value = 1;
            param[9] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[9].Value = main.FLAG;
            param[10] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
            param[10].Value = main.OVERALL_CMTS;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            string Result;
            return new { Result = ds.Tables[0].Rows[0]["Result"].ToString() };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Saved Drafts List  ---BranchCheckList Creation

    public object BCLGetSavedDrafts(HDModel main)
    {
        try
        {
            List<BranchCheckListSavedDrafts> BCLSavedDraftsList = new List<BranchCheckListSavedDrafts>();
            BranchCheckListSavedDrafts BCLSavedDrafts = new BranchCheckListSavedDrafts();

            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_SAVED_LIST");
            sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                BCLSavedDrafts = new BranchCheckListSavedDrafts();
                BCLSavedDrafts.LCM_CODE = ds.Tables[0].Rows[i]["LCM_CODE"].ToString();
                BCLSavedDrafts.LCM_NAME = ds.Tables[0].Rows[i]["LCM_NAME"].ToString();
                BCLSavedDrafts.CTY_CODE = ds.Tables[0].Rows[i]["CTY_CODE"].ToString();
                BCLSavedDrafts.CNY_CODE = ds.Tables[0].Rows[i]["CNY_CODE"].ToString();
                BCLSavedDrafts.LCM_CNP = ds.Tables[0].Rows[i]["LCM_CNP"].ToString();
                BCLSavedDrafts.VISITDATE = ds.Tables[0].Rows[i]["BCL_SELECTED_DT"].ToString();
                BCLSavedDrafts.SavedCount = ds.Tables[0].Rows[i]["SavedCount"].ToString();
                BCLSavedDraftsList.Add(BCLSavedDrafts);
            }

            return new { SavedDraftsList = BCLSavedDraftsList };



        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object GetCheckListDetails(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        int BCL_ID = Int32.Parse(hDModel.Password);
        sp = new SubSonic.StoredProcedure(DB + "." + "DCL_GET_CHECKLIST_DETAILS");
        sp.Command.Parameters.Add("@BCL_ID", BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = "OK", data = ds.Tables[0], data1 = ds.Tables[1] };
        //return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = "Not OK", data = (object)null };
    }

    //Saved Drafts List Location-wise  ---BranchCheckList Saved Drafts

    public object BCLGetSavedDraftsBasedOnLoc(BranchCheckListInsert main)
    {
        try
        {
            List<BranchCheckListInsert> BCLSavedDraftsListByLoc = new List<BranchCheckListInsert>();
            BranchCheckListInsert BCLSavedLoc = new BranchCheckListInsert();
            SelectedList selectedList = new SelectedList();
            ImagesList images = new ImagesList();

            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.AUR_ID));
            HDModel hd = new HDModel();
            hd.CompanyId = main.CompanyId;
            string DB = _ReturnDB(hd);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_SAVED_LIST_ITEMS_MOBILE");
            sp.Command.AddParameter("@LCM_CODE", main.LCM_CODE, DbType.String);
            sp.Command.AddParameter("@AUR_ID", main.AUR_ID, DbType.String);
            sp.Command.AddParameter("@INSPECTEDDATE", main.VISITDATE, DbType.String);
            DataSet ds = new DataSet();
            ds = sp.GetDataSet();

            BCLSavedLoc = new BranchCheckListInsert();
            BCLSavedLoc.CNY_CODE = ds.Tables[0].Rows[0]["BCL_CNY_CODE"].ToString();
            BCLSavedLoc.LCM_CODE = ds.Tables[0].Rows[0]["BCL_LOC_CODE"].ToString();
            BCLSavedLoc.CTY_CODE = ds.Tables[0].Rows[0]["BCL_CTY_CODE"].ToString();
            BCLSavedLoc.INSPECTOR = ds.Tables[0].Rows[0]["BCL_INSPECTED_BY"].ToString();
            BCLSavedLoc.VISITDATE = ds.Tables[0].Rows[0]["BCL_SELECTED_DT"].ToString();
            BCLSavedLoc.LCM_CNP = ds.Tables[0].Rows[0]["BCL_SELECTED_CNP"].ToString();
            BCLSavedLoc.OVERALL_CMTS = ds.Tables[0].Rows[0]["BCL_OVERALL_CMTS"].ToString();
            BCLSavedLoc.SelectedLists = new List<SelectedList>();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                selectedList = new SelectedList();
                selectedList.CatCode = ds.Tables[0].Rows[i]["BCLD_CAT_CODE"].ToString();
                selectedList.SubcatCode = ds.Tables[0].Rows[i]["BCLD_SUBCAT_CODE"].ToString();
                selectedList.CatName = ds.Tables[0].Rows[i]["BCL_MC_NAME"].ToString();
                selectedList.SubCatName = ds.Tables[0].Rows[i]["BCL_SUB_NAME"].ToString();
                selectedList.ScoreCode = ds.Tables[0].Rows[i]["BCLD_SCORE_CODE"].ToString();
                selectedList.ScoreName = ds.Tables[0].Rows[i]["BCLD_SCORE_NAME"].ToString();
                selectedList.txtdata = ds.Tables[0].Rows[i]["BCLD_TEXTDATA"].ToString();
                selectedList.Date = ds.Tables[0].Rows[i]["BCLD_DATE"].ToString();
                selectedList.SubScore = ds.Tables[0].Rows[i]["BCLD_SUB_SCORE"].ToString();
                selectedList.FilePath = ds.Tables[0].Rows[i]["BCLD_FILE_UPLD"].ToString();
                BCLSavedLoc.SelectedLists.Add(selectedList);
            }
            BCLSavedLoc.imagesList = new List<ImagesList>();
            for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
            {
                images = new ImagesList();
                images.Imagepath = ds.Tables[1].Rows[i]["BCL_UPL_PATH"].ToString();
                BCLSavedLoc.imagesList.Add(images);
            }


            BCLSavedDraftsListByLoc.Add(BCLSavedLoc);

            return new { SavedDraftsList = BCLSavedDraftsListByLoc };



        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    #endregion

    #region Database Configuration
    //Return DataBase
    public string _ReturnDB(HDModel Tenant)
    {
        try
        {
            DataSet ds = new DataSet();
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "CHECK_USER_TENANT");
            sp.Command.AddParameter("@TENANT_NAME", Tenant.CompanyId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                DatabaseURL = ds.Tables[0].Rows[0]["TENANT_ID"].ToString();
                HttpContext.Current.Session["useroffset"] = ds.Tables[0].Rows[0]["OFFSET"].ToString();
                if (ds.Tables[0].Rows[0]["TENANT_ID"].ToString() == "" && ds.Tables[0].Rows[0]["OFFSET"].ToString() == "")
                { return (string)null; }
                else if (ds.Tables[0].Rows.Count == 0)
                { return (string)null; }
                else
                { return DatabaseURL; }
            }
            else { return MessagesVM.Err; }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    public string _ReturnDBByGmailId(string Tenant)
    {
        try
        {
            DataSet ds = new DataSet();
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "CHECK_USER_TENANT");
            sp.Command.AddParameter("@TENANT_NAME", Tenant, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                DatabaseURL = ds.Tables[0].Rows[0]["TENANT_ID"].ToString();
                HttpContext.Current.Session["useroffset"] = ds.Tables[0].Rows[0]["OFFSET"].ToString();
                if (ds.Tables[0].Rows[0]["TENANT_ID"].ToString() == "" && ds.Tables[0].Rows[0]["OFFSET"].ToString() == "")
                { return (string)null; }
                else if (ds.Tables[0].Rows.Count == 0)
                { return (string)null; }
                else
                { return DatabaseURL; }
            }
            else { return MessagesVM.Err; }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    #endregion

    #region MileStone #02
    //Return UserName
    public string _GetUserName(string UID, string DB)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_GET_USERNAME");
            sp.Command.AddParameter("@UID", UID, DbType.String);
            return (string)sp.ExecuteScalar();
        }
        catch (Exception)
        {
            return MessagesVM.Err;
        }
    }
    public string _GetUserNameByEmailId(string EMAIL_ID, string DB)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_GET_USERNAME_BY_EMAIL_ID");
            sp.Command.AddParameter("@EMAIL_ID", EMAIL_ID, DbType.String);
            return (string)sp.ExecuteScalar();
        }
        catch (Exception)
        {
            return MessagesVM.Err;
        }
    }
    //User Role
    public int _GetRoles(string UID, string DB)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_GET_USER_ROLE");
            sp.Command.AddParameter("@UID", UID, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                int flag = Convert.ToInt32(ds.Tables[0].Rows[0]["ROL_ID"].ToString());
                return flag;
            }
            else
            {
                return 0;
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return 0;
        }
    }

    //Get Role By EmailID

    public int _GetRolesByEmailId(string EMAIL_ID, string DB)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_GET_USER_ROLE_BY_EMAIL_ID");
            sp.Command.AddParameter("@EMAIL_ID", EMAIL_ID, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                int flag = Convert.ToInt32(ds.Tables[0].Rows[0]["ROL_ID"].ToString());
                return flag;
            }
            else
            {
                return 0;
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return 0;
        }
    }
    //Sign In
    public object _LoginValidate(HDModel Login)
    {
        try
        {
            string DB = _ReturnDB(Login);
            int cnt;
            int Invldcnt;
            int Expiry;
            if (DB == null)
            { return new { Message = MessagesVM.InvalidCompany, data = (object)null }; }
            else
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_VALIDATE_USER");
                sp.Command.AddParameter("@COMPANY", Login.CompanyId, DbType.String);
                sp.Command.AddParameter("@USR_ID", Login.UserId, DbType.String);
                sp.Command.AddParameter("@USR_LOGIN_PASSWORD", Login.Password, DbType.String);
                cnt = (int)sp.ExecuteScalar();
                string UNAME = _GetUserName(Login.UserId, DB);
                int ROLE = _GetRoles(Login.UserId, DB);
                // List<Module> modules = new List<Module>();
                List<Modules> modules = GetModuleList(Login);
                if (cnt == 1)
                {
                    string Key = Guid.NewGuid().ToString();
                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "LOGIN_HISTORY");
                    sp1.Command.AddParameter("@COMPANY", Login.CompanyId, DbType.String);
                    sp1.Command.AddParameter("@USR_ID", Login.UserId, DbType.String);
                    sp1.Command.AddParameter("@KEY", Key, DbType.String);
                    Expiry = (int)sp1.ExecuteScalar();
                    if (Expiry == 1)
                    {
                        return new { Status = "false", Message = MessagesVM.License, LicensePeriodExpired = Expiry };
                    }
                    else
                    {
                        insertLoginSuccess(DB, Login.UserId);
                        return new { Status = "true", Message = MessagesVM.LoginSuccess, data = Key, User = UNAME, EmployeeRole = ROLE, Modules = modules, LoginSno = HttpContext.Current.Session["LoginUniqueID1"] };
                    }
                }
                else
                {
                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(DB + "." + "USR_INVALID_CNT");
                    sp1.Command.AddParameter("@USR_ID", Login.UserId, DbType.String);
                    Invldcnt = (int)sp1.ExecuteScalar();
                    if (Invldcnt >= 3)
                    {
                        return new { Status = "false", Message = MessagesVM.moreattempts, data = (object)null };
                    }
                    else
                    {
                        return new { Status = "false", Message = MessagesVM.InvalidLogin, data = (object)null };
                    }
                }
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }



    //LogOut API
    public object _SignOut(HDModel Login)
    {
        try
        {
            string DB = _ReturnDB(Login);
            int cnt;
            int Invldcnt;
            int Expiry;
            if (DB == null)
            { return new { Message = MessagesVM.InvalidCompany, data = (object)null }; }
            else
            {

                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "[UPDATE_LOGOUT_TIME]");
                sp.Command.AddParameter("@USR_ID", Login.UserId, DbType.String);
                sp.Command.AddParameter("@MODE", "4", DbType.String);
                sp.Command.AddParameter("@LoginUniqueID", Login.LoginSno, DbType.String);
                sp.ExecuteScalar();

                return new { Status = "true", Message = MessagesVM.LogoutSuccess };
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }

    void insertLoginSuccess(string db, string userId)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(db + "." + "SAVE_USER_LASTLOGIN");
            sp.Command.AddParameter("@USR_ID", userId, DbType.String);
            sp.Command.AddParameter("@MODE", "1", DbType.String);
            sp.Command.AddParameter("@TIMEOUT", ConfigurationManager.AppSettings["timeout"], DbType.Int32);
            sp.Command.AddParameter("@LOGINMEDIUM", "App", DbType.String);

            DataSet ds = new DataSet();
            ds = sp.GetDataSet();
            HttpContext.Current.Session["LoginUniqueID1"] = ds.Tables[0].Rows[0]["SNO"];

        }
        catch (Exception ex)
        {
        }
    }
    public object _LoginValidateWithADFS(HDModel ADFSLogin)
    {
        try
        {
            string DB = _ReturnDB(ADFSLogin);
            int cnt;
            int Expiry;
            if (DB == null)
            { return new { Message = MessagesVM.InvalidCompany, data = (object)null }; }
            else
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_VALIDATE_USER_ADFS");
                sp.Command.AddParameter("@COMPANY", ADFSLogin.CompanyId, DbType.String);
                sp.Command.AddParameter("@USR_ID", ADFSLogin.UserId, DbType.String);

                cnt = (int)sp.ExecuteScalar();
                string UNAME = _GetUserName(ADFSLogin.UserId, DB);
                int ROLE = _GetRoles(ADFSLogin.UserId, DB);
                List<Modules> modules = GetModuleList(ADFSLogin);
                if (cnt == 1)
                {
                    string Key = Guid.NewGuid().ToString();
                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "LOGIN_HISTORY");
                    sp1.Command.AddParameter("@COMPANY", ADFSLogin.CompanyId, DbType.String);
                    sp1.Command.AddParameter("@USR_ID", ADFSLogin.UserId, DbType.String);
                    sp1.Command.AddParameter("@KEY", Key, DbType.String);
                    Expiry = (int)sp1.ExecuteScalar();
                    if (Expiry == 1)
                    {
                        return new { Status = "false", Message = MessagesVM.License, LicensePeriodExpired = Expiry };
                    }
                    else
                    {
                        return new { Status = "true", Message = MessagesVM.LoginSuccess, data = Key, User = UNAME, EmployeeRole = ROLE, Modules = modules };
                    }
                }
                else { return new { Status = "false", Message = MessagesVM.InvalidLogin, data = (object)null }; }
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex);
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }

    //Sign In With Gmail
    //public object SignInWithGmail()
    //{
    //    try
    //    {
    //        GoogleConnect.ClientId = "140086361902-h8balga4l6fhaq5ip6r4ip8e0278o1mc.apps.googleusercontent.com";
    //        GoogleConnect.ClientSecret = "gnQK3yUuUAVgfJHzNRefXPxs";
    //        GoogleConnect.RedirectUri = HttpContext.Current.Request.Url.AbsoluteUri.Split('?')[0];
    //        if (!string.IsNullOrEmpty(HttpContext.Current.Request.QueryString["code"]))
    //        {

    //            string json = GoogleConnect.Fetch("me", HttpContext.Current.Request.QueryString["code"]);
    //            GoogleProfile profile = new JavaScriptSerializer().Deserialize<GoogleProfile>(json);
    //            var EMAIL_ID = profile.Emails.Find(email => email.Type == "account").Value;
    //            string DB = _ReturnDBByGmailId(EMAIL_ID);
    //            int cnt;
    //            int Expiry;
    //            if (DB == null)
    //            { return new { Message = MessagesVM.InvalidCompany, data = (object)null }; }
    //            else
    //            {

    //                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(DB + "." + "GN_VALIDATE_USER_BY_EMAIL_ID");
    //                //sp.Command.AddParameter("@COMPANY", Login.CompanyId, DbType.String);
    //                sp.Command.AddParameter("@EMAIL_ID", EMAIL_ID, DbType.String);
    //                //sp.Command.AddParameter("@USR_LOGIN_PASSWORD", Login.Password, DbType.String);
    //                cnt = (int)sp.ExecuteScalar();
    //                string UNAME = _GetUserNameByEmailId(EMAIL_ID, DB);
    //                int ROLE = _GetRolesByEmailId(EMAIL_ID, DB);
    //                if (cnt == 1)
    //                {
    //                    string Key = Guid.NewGuid().ToString();
    //                    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "LOGIN_HISTORY_BY_EMAIL_ID");

    //                    sp1.Command.AddParameter("@EMAIL_ID", EMAIL_ID, DbType.String);
    //                    sp1.Command.AddParameter("@KEY", Key, DbType.String);
    //                    Expiry = (int)sp1.ExecuteScalar();
    //                    if (Expiry == 1)
    //                    {
    //                        return new { Status = "false", Message = MessagesVM.License, LicensePeriodExpired = Expiry };
    //                    }
    //                    else
    //                    {
    //                        return new { Status = "true", Message = MessagesVM.LoginSuccess, data = Key, User = UNAME, EmployeeRole = ROLE, UserImage = profile.Image.Url };
    //                    }
    //                }
    //                else
    //                {
    //                    return new { Status = "false", Message = MessagesVM.InvalidLogin, data = (object)null };
    //                }

    //            }

    //        }
    //        else
    //        {
    //            GoogleConnect.Authorize("profile", "email");
    //            return new { Status = "false", Message = MessagesVM.InvalidLogin, data = (object)null };
    //        }

    //    }
    //    catch (SqlException ex)
    //    {
    //        ErrorHandler erhndlr = new ErrorHandler();
    //        erhndlr._WriteErrorLog(ex);
    //        return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
    //    }
    //    catch (Exception ex)
    //    {
    //        ErrorHandler erhndlr = new ErrorHandler();
    //        erhndlr._OtherExceptionsInDBErrorLog(ex);
    //        return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
    //    }
    //}
    //Forgot Password
    public object _PasswordForget(HDModel Password)
    {
        try
        {
            string TenantValidate = _ReturnDB(Password);
            if (TenantValidate != null | TenantValidate != "")
            {
                if (Password.UserId != null | Password.UserId != "")
                {
                    SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(TenantValidate + "." + "GN_PASSWORD_FORGET");
                    sp.Command.AddParameter("@USR_ID", Password.UserId, DbType.String);
                    int vldpwd = (int)sp.ExecuteScalar();
                    if (vldpwd == 1)
                    {
                        return new { Status = "true", Message = MessagesVM.ForgotPassword };
                    }
                    else if (vldpwd == 2)
                    {
                        return new { Status = "false", Message = MessagesVM.InvalidUserID };
                    }
                    else
                    {
                        return new { Status = "false", Message = MessagesVM.Err };
                    }
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            else { return new { Status = "false", Message = MessagesVM.Err }; }
        }
        catch (SqlException Sql)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(Sql); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }
    //User Profile
    public object _GetDashboardProfile(HDModel Profile)
    {
        if (!(Profile.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(Profile.APIKey);
                if (VALDB.AURID != null)
                {
                    DashboardProfile Dashboard = new DashboardProfile();
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_EMP_DETAILS");
                    sp.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            Dashboard.EMP_ID = sdr["EMP_ID"].ToString();
                            Dashboard.EMP_NAME = sdr["EMP_NAME"].ToString();
                            Dashboard.EMP_EMAIL = sdr["EMP_EMAIL"].ToString();
                            Dashboard.EMP_DESIG = sdr["DESIGNATION"].ToString();
                            Dashboard.DEPARTMENT = sdr["DEPARTMENT"].ToString();
                            Dashboard.REPORTING = sdr["REPORTING"].ToString();
                            Dashboard.DOJ = sdr["DOJ"].ToString();
                            Dashboard.DOB = sdr["DOB"].ToString();
                            Dashboard.EXTENSION = sdr["EXTENSION"].ToString();
                            Dashboard.STATUS = sdr["STATUS"].ToString();
                            Dashboard.EMP_REPNAME = sdr["REPORTING"].ToString();
                            Dashboard.AUR_RES_NUMBER = sdr["AUR_RES_NUMBER"].ToString();
                            Dashboard.EMP_IMG = sdr["EMP_IMG"].ToString();
                        }
                    }
                    if (Dashboard.EMP_ID != null)
                        return new { Status = "true", data = Dashboard };
                    else
                        return new { Status = "false", data = (object)null };
                }
                else
                {
                    return new { Status = "false", Message = MessagesVM.Err };
                }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(e);
                return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }

    //Company Logo
    public object _GetCompanyLogo(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "Update_Get_LogoImage");
        sp.Command.AddParameter("@type", "2", DbType.String);
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            return new { Message = "OK", data = ds.Tables[0] };
        }
        else
            return new { Message = "Not OK", data = (object)null };
    }
    //Update User Profile
    public object _UpdateProfile(Profile Prof)
    {
        try
        {
            int ValidateProfile;
            ValidateKey VALDB = Secure._ValidateAPIKey(Prof.APIKey);
            if (VALDB.AURID != null)
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_UPDATE_USER_PROFILE");
                sp.Command.AddParameter("@ID", VALDB.AURID, DbType.String);
                sp.Command.AddParameter("@AUR_FIRST_NAME", Prof.Name, DbType.String);
                sp.Command.AddParameter("@AUR_EMAIL", Prof.Email, DbType.String);
                sp.Command.AddParameter("@AUR_DEP_ID", Prof.Department, DbType.String);
                sp.Command.AddParameter("@AUR_STA_ID", Prof.Status, DbType.Int32);
                if (!((Prof.Phone) == null | (Prof.Phone) == ""))
                {
                    sp.Command.AddParameter("@AUR_RES_NUMBER", Prof.Phone, DbType.String);
                }
                else { sp.Command.AddParameter("@AUR_RES_NUMBER", "", DbType.String); }
                if (!((Prof.Extension) == null | (Prof.Extension) == ""))
                {
                    sp.Command.AddParameter("@AUR_EXTENSION", Prof.Extension, DbType.String);
                }
                else { sp.Command.AddParameter("@AUR_EXTENSION", "", DbType.String); }
                sp.Command.AddParameter("@AUR_DOB", Prof.DOB, DbType.Date);
                sp.Command.AddParameter("@AUR_DOJ", Prof.DOJ, DbType.Date);
                sp.Command.AddParameter("@AUR_REPORTING_TO", Prof.ReportingManager, DbType.String);
                ValidateProfile = (int)sp.ExecuteScalar();
                if (ValidateProfile == 1) { return new { Status = "true", Message = MessagesVM.ProfileUpdated }; } else { return new { Status = "false", Message = MessagesVM.Err, data = (object)null }; }
            }
            else
            {
                return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }
        }
        catch (SqlException ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (Exception e)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
    }
    //Change Password
    public object _ChangePasswod(ChangePwd ChangePwd)
    {
        if (!(ChangePwd.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(ChangePwd.APIKey);
                if (VALDB.AURID != null)
                {
                    //Validate Old Password & Then Update To New One
                    int Passwodcnt;
                    SubSonic.StoredProcedure sp0 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_VALIDATE_OLD");
                    sp0.Command.AddParameter("@USER", VALDB.AURID, DbType.String);
                    sp0.Command.AddParameter("@OLDPWD", ChangePwd.OldPwd, DbType.String);
                    Passwodcnt = (int)sp0.ExecuteScalar();
                    if (Passwodcnt == 1)
                    {
                        try
                        {
                            int cnt;
                            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_CHANGE_PWD");
                            sp.Command.AddParameter("@USER", VALDB.AURID, DbType.String);
                            sp.Command.AddParameter("@PWD", ChangePwd.NewPwd, DbType.String);
                            cnt = (int)sp.ExecuteScalar();
                            if (cnt == 1)
                            {
                                return new { Status = "true", Message = MessagesVM.ChangePassword };
                            }
                            else { return new { Status = "false", Message = MessagesVM.Err }; }
                        }
                        catch (SqlException E)
                        {
                            ErrorHandler erhndlr = new ErrorHandler();
                            erhndlr._WriteErrorLog(E); return E.ToString();
                        }
                        catch (Exception ex)
                        {
                            ErrorHandler erhndlr = new ErrorHandler();
                            erhndlr._OtherExceptionsInDBErrorLog(ex); return ex.ToString();
                        }
                    }
                    else { return new { Status = "false", Message = MessagesVM.InvalidOldPassword }; }
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (SqlException sq)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(sq); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }
        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }

    #region Dropdown Binding Methods
    //Departments
    public object _GetDepartments(Key AKey)
    {
        if (!(AKey.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(AKey.APIKey);
                if (VALDB.AURID != null)
                {
                    try
                    {
                        List<Department> verlst = new List<Department>();
                        Department ver;
                        sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GET_DEPARTMENTS");
                        using (IDataReader sdr = sp.GetReader())
                        {
                            while (sdr.Read())
                            {
                                ver = new Department();
                                ver.DEP_NAME = sdr["DEP_NAME"].ToString();
                                ver.DEP_CODE = sdr["DEP_CODE"].ToString();
                                ver.ticked = false;
                                verlst.Add(ver);
                            }
                        }
                        if (verlst.Count != 0)
                            return new { Status = "true", data = verlst };
                        else
                            return new { Status = "false", data = (object)null };
                    }
                    catch (Exception e)
                    {
                        ErrorHandler erhndlr = new ErrorHandler();
                        erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
                    }
                }
                else { return new { Status = "false", Message = MessagesVM.Err, data = (object)null }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(e); return new { Status = "false", Message = MessagesVM.Err };
            }
        }
        else { return new { Status = "false", Message = MessagesVM.Err, data = (object)null }; }

    }
    //Countries
    public object _GetCountries(Key AKey)
    {
        if (!(AKey.APIKey == null))
        {
            ValidateKey VALDB = Secure._ValidateAPIKey(AKey.APIKey);
            if (VALDB.AURID != null)
            {
                try
                {
                    List<Countrylst> cnylst = new List<Countrylst>();
                    Countrylst cny;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "Get_Countries_ADM");
                    sp.Command.AddParameter("@MODE", 1, DbType.Int32);
                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            cny = new Countrylst();
                            cny.CNY_CODE = sdr["CNY_CODE"].ToString();
                            cny.CNY_NAME = sdr["CNY_NAME"].ToString();
                            cny.ticked = false;
                            cnylst.Add(cny);
                        }
                    }
                    if (cnylst.Count != 0)
                        return new { Status = "true", data = cnylst };
                    else
                        return new { Status = "false", data = (object)null };
                }
                catch (Exception e)
                {
                    ErrorHandler erhndlr = new ErrorHandler();
                    erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
                }
            }
            else
            { return new { Status = "false", Message = MessagesVM.Err }; }
        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }

    #region InsertSecurity Gaurd Submit List

    public object InsertDailyCheckList(SelectedDataModel objd)
    {
        //Get Company name from Hd module 
        //Get Company name from Hd module 
        HDModel mn = new HDModel();
        mn.CompanyId = objd.Company;
        string DB = _ReturnDB(mn);
        ErrorHandler err1 = new ErrorHandler();
        //err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));

        String proc_name = DB + "." + "DCL_INSERT_CHECKLIST";
        String proc_name1 = DB + "." + "DCL_CHECK_DETAILS_DELETE";
        String proc_name2 = DB + "." + "DCL_INSERT_CHECK_DETAILS";
        string proc_name3 = DB + "." + "[SEND_SECURITYGUARD_INCEPTION]";

        int sem_id = 0;
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[0].Value = objd.BCL_ID;
        param[1] = new SqlParameter("@Location", SqlDbType.VarChar);
        param[1].Value = objd.Location;
        param[2] = new SqlParameter("@InspectdBy", SqlDbType.VarChar);
        param[2].Value = objd.InspectdBy;
        param[3] = new SqlParameter("@InspectdDate", SqlDbType.DateTime);
        param[3].Value = objd.InspectdDate;
        /*Convert.ToDateTime(objd.InspectdDate);*/ /*DateTime.Parse(objd.InspectdDate);*/
        //DateTime.ParseExact(dateString, "MM-dd-yyyy", provider); // 10/22/2015 12:00:00 AM /**/ /*objd.InspectdDate.ToString();*/
        param[4] = new SqlParameter("@CreatedBy", SqlDbType.VarChar);
        param[4].Value = objd.CreatedBy;
        param[5] = new SqlParameter("@Company", SqlDbType.VarChar);
        param[5].Value = 1;
        param[6] = new SqlParameter("@SaveFlag", SqlDbType.VarChar);
        param[6].Value = objd.SaveFlag;
        param[7] = new SqlParameter("@FLAG", SqlDbType.Int);
        param[7].Value = objd.Flag;

        object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name, param);
        sem_id = (int)o;
        if (sem_id > 0)
        {
            SqlParameter[] param2 = new SqlParameter[1];
            param2[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param2[0].Value = objd.BCL_ID;
            object ChkDelete = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name1, param2);

            for (int i = 0; i < objd.Seldata.Count; i++)
            {
                SqlParameter[] param1 = new SqlParameter[6];
                param1[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
                param1[0].Value = objd.BCL_ID;

                param1[1] = new SqlParameter("@CatCode", SqlDbType.VarChar);
                param1[1].Value = objd.Seldata[i].CatCode;
                param1[2] = new SqlParameter("@SubcatCode", SqlDbType.VarChar);
                param1[2].Value = objd.Seldata[i].SubcatCode;
                param1[3] = new SqlParameter("@WorkCondition", SqlDbType.VarChar);
                param1[3].Value = objd.Seldata[i].ScoreName;
                param1[4] = new SqlParameter("@Comments", SqlDbType.VarChar);
                param1[4].Value = objd.Seldata[i].txtdata;
                param1[5] = new SqlParameter("@filepath", SqlDbType.VarChar);
                param1[5].Value = objd.Seldata[i].FilePath;
                object ChkDetails = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name2, param1);
            }
        }

        SqlParameter[] param3 = new SqlParameter[1];
        param3[0] = new SqlParameter("@ID", SqlDbType.Int);
        param3[0].Value = objd.BCL_ID;
        object ChkDetailsS = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name3, param3);

        string msg;
        if (sem_id == 1)
            msg = "Saved as Draft";
        else
            msg = "CheckList Submitted Successfully";
        return new { data = msg, Message = msg };

    }
    #endregion


    public object GET_ZONAL_ACTIVE_IDS(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        List<Zonal_ID> Zon_lst = new List<Zonal_ID>();
        Zonal_ID id;
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_BCL_ID");
        sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
        sp.Command.Parameters.Add("@loc", hDModel.Password, DbType.String);

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                id = new Zonal_ID();
                id.id = sdr["ST"].ToString() + "/" + sdr["BCL_ID"].ToString();
                id.BCL_ID = sdr["BCL_ID"].ToString();
                id.ticked = false;
                Zon_lst.Add(id);
            }
        }
        if (Zon_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Zon_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetZonaLFmDetails(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        int BCL_ID = Int32.Parse(hDModel.Password);
        sp = new SubSonic.StoredProcedure(DB + "." + "Zonal_Get_Details_On_Id");
        sp.Command.Parameters.Add("@BCL_ID", BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = "OK", data = ds.Tables[0] };
        //return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = "Not OK", data = (object)null };
    }


    #region getNewCheckListID
    public object _GetNewSecurityCheckListID(HDModel hDModel)
    {

        string DB = _ReturnDB(hDModel);

        String proc_name = DB + "." + "DCL_GET_NEW_CHECKLIST_ID";
        String proc_name1 = DB + "." + "DCLC_GET_INSPECTORS";

        String proc_name2 = DB + "." + "DCLC_GET_LOCATIONS";

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@CREATED_BY", SqlDbType.VarChar);
        param[0].Value = hDModel.UserId;

        SqlParameter[] param2 = new SqlParameter[1];
        param2[0] = new SqlParameter("@USER", SqlDbType.VarChar);
        param2[0].Value = hDModel.UserId;

        object CheckListId = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name, param);

        object Inspectors = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name1, param2);

        object Location = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name2, param2);

        if (CheckListId != null && Inspectors != null && Location != null)
        {
            return new { checkListId = CheckListId.ToString(), inspectors = Inspectors.ToString(), locatio = Location.ToString(), message = "OK" };
        }
        else
        {
            return new { message = "Something went wrong" };

        }

    }
    #endregion


    //Side Menu Count
    public object _HDCount(HDModel HDCount)
    {
        if (!(HDCount.APIKey == null))
        {
            ValidateKey VALDB = Secure._ValidateAPIKey(HDCount.APIKey);
            if (VALDB.AURID != null)
            {
                try
                {
                    List<HDDashboardCount> cntlst = new List<HDDashboardCount>();
                    HDDashboardCount Dshcnt = new HDDashboardCount();
                    SqlParameter[] param = new SqlParameter[3];
                    param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                    param[0].Value = VALDB.AURID;
                    param[1] = new SqlParameter("@CMP_ID", SqlDbType.NVarChar);
                    param[1].Value = VALDB.COMPANYID;
                    param[2] = new SqlParameter("@ROLE_ID", SqlDbType.Int);
                    param[2].Value = HDCount.Role_id;
                    using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, VALDB.TENANT_ID + "." + "API_HD_GET_HELPDESK_STS_DASHBOARD", param))
                    {
                        if (dr.Read())
                        {
                            Dshcnt.TOTAL = dr["TOTAL"].ToString();
                            Dshcnt.PENDING = dr["PENDING"].ToString();
                            Dshcnt.INPROGRESS = dr["INPROGRESS"].ToString();
                            Dshcnt.CLOSED = dr["CLOSED"].ToString();
                            Dshcnt.ONHOLD = dr["ONHOLD"].ToString();
                            Dshcnt.REJECTED = dr["REJECTED"].ToString();
                            Dshcnt.SLA_VIOLATION = dr["VIOLATION"].ToString();
                            return new { Status = "true", data = Dshcnt };
                        }
                        else
                        {
                            return new { Status = "false", data = (object)null };
                        }
                    }
                }
                catch (Exception e)
                {
                    ErrorHandler erhndlr = new ErrorHandler();
                    erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
                }
            }
            else
            { return new { Status = "false", Message = MessagesVM.Err }; }
        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }

    public object getRequestID(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "DCL_REQUEST_ID");

        sp.Command.AddParameter("@CREATED_BY", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
        {
            List<string> actualData = new List<string>();
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                actualData.Add(dr["BCL_ID"].ToString() + "/" + dr["ST"].ToString());
            }

            return new { Message = "OK", data = actualData };
        }
        else
            return new { Message = "Not OK", data = (object)null };
    }


    //Reporting To 
    public object _ReportingManagers(HDModel name)
    {
        if (!(name.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(name.APIKey);
                if (VALDB.AURID != null)
                {
                    List<EmployeeNameID> emplst = new List<EmployeeNameID>();
                    EmployeeNameID emp;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "HD_API_GET_REPORTING_MANAGERS");
                    sp.Command.AddParameter("@NAME", name.UserId, DbType.String);

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            emp = new EmployeeNameID();
                            emp.AUR_ID = sdr["id"].ToString();
                            emp.NAME = sdr["name"].ToString();
                            emp.ticked = false;
                            emplst.Add(emp);
                        }
                    }
                    return new { Status = "true", items = emplst, total_count = emplst.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    #endregion

    #endregion

    #region MileStone #03

    //Get Total No.Of HD Request Count
    public string GetTotalHDReqCount(string SKey)
    {
        try
        {
            string cnt = "0";
            DataSet ds = new DataSet();
            ValidateKey VALDB = Secure._ValidateAPIKey(SKey);
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GET_TODAYS_REQUEST_ID");
            sp.Command.AddParameter("@dummy", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                cnt = Convert.ToInt32((ds.Tables[0].Rows[0]["cnt"])).ToString();
                return cnt;
            }
            else { return Convert.ToInt32("0").ToString(); }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    //Service Type 
    public object _ServiceType(RaiseRequest SType)
    {
        if (!(SType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(SType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<ServiceType> SList = new List<ServiceType>();
                    ServiceType St;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_CHILD_CATEGORY");
                    sp.Command.AddParameter("@CHILD", SType.ServiceType, DbType.String);
                    sp.Command.AddParameter("@PageNo", SType.PageNo, DbType.String);
                    sp.Command.AddParameter("@PageSize", SType.PageSize, DbType.String);
                    sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.Int32);

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            St = new ServiceType();
                            St.SERVICE_TYPE_CODE = sdr["CCAT_CODE"].ToString();
                            St.SERVICE_TYPE_NAME = sdr["CCAT_NAME"].ToString();
                            SList.Add(St);
                        }
                    }
                    return new { Status = "true", items = SList, total_count = SList.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }


    // Location Type

    public object _LocationType(RaiseRequest LType)
    {
        if (!(LType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(LType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<LocationType> SList = new List<LocationType>();
                    LocationType St;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_LOCATION");
                    sp.Command.AddParameter("@USER_ID", VALDB.AURID, DbType.String);
                    sp.Command.AddParameter("@LOCATION", LType.LocationType, DbType.String);
                    sp.Command.AddParameter("@PageNo", LType.PageNo, DbType.String);
                    sp.Command.AddParameter("@PageSize", LType.PageSize, DbType.String);
                    sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.Int32);

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            St = new LocationType();
                            St.LCM_CODE = sdr["LCM_CODE"].ToString();
                            St.LCM_NAME = sdr["LCM_NAME"].ToString();
                            SList.Add(St);
                        }
                    }
                    return new { Status = "true", items = SList, total_count = SList.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }

    // Request Type

    public object _RequestType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<RequestType> RList = new List<RequestType>();
                    RequestType Rt;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_REQUEST_TYPES");
                    sp.Command.AddParameter("@RequestType", RType.RequestType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            Rt = new RequestType();
                            Rt.PM_RT_SNO = sdr["PM_RT_SNO"].ToString();
                            Rt.PM_RT_TYPE = sdr["PM_RT_TYPE"].ToString();
                            RList.Add(Rt);
                        }
                    }
                    return new { Status = "true", items = RList, total_count = RList.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }

    public object _LocationType_cor(RaiseRequest LType)
    {
        if (!(LType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(LType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<LocationType_cor> SList = new List<LocationType_cor>();
                    LocationType_cor St;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_LOCATION_cor");
                    sp.Command.AddParameter("@USER_ID", VALDB.AURID, DbType.String);
                    sp.Command.AddParameter("@LOCATION", LType.LocationType, DbType.String);
                    sp.Command.AddParameter("@PageNo", LType.PageNo, DbType.String);
                    sp.Command.AddParameter("@PageSize", LType.PageSize, DbType.String);
                    sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.Int32);

                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            St = new LocationType_cor();
                            St.LCM_CODE = sdr["LCM_CODE"].ToString();
                            St.LCM_NAME = sdr["LCM_NAME"].ToString();
                            St.LAT = sdr["LAT"].ToString();
                            St.LONG = sdr["LONG"].ToString();
                            SList.Add(St);
                        }
                    }
                    return new { Status = "true", items = SList, total_count = SList.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    //Aquisition Through

    public object _AquisitionThr(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<AquisitionThr> ATL = new List<AquisitionThr>();
                    AquisitionThr At;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_ACQISITION_THROUGH");
                    sp.Command.AddParameter("@AquisitionThr", RType.AcqThr, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            At = new AquisitionThr();
                            At.PN_ACQISITION_ID = sdr["PN_ACQISITION_ID"].ToString();
                            At.PN_ACQISITION_THROUGH = sdr["PN_ACQISITION_THROUGH"].ToString();
                            ATL.Add(At);
                        }
                    }
                    return new { Status = "true", items = ATL, total_count = ATL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }


    //Entity Type

    public object _EntityType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<EntityType> ETL = new List<EntityType>();
                    EntityType Et;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_ENTITY_TYPES");
                    sp.Command.AddParameter("@EntityType", RType.EntityType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            Et = new EntityType();
                            Et.CHE_CODE = sdr["CHE_CODE"].ToString();
                            Et.CHE_NAME = sdr["CHE_NAME"].ToString();
                            ETL.Add(Et);
                        }
                    }
                    return new { Status = "true", items = ETL, total_count = ETL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }


    //Floor 

    public object _FloorDetails(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "USP_getFloors_masters");
            sp.Command.Parameters.Add("@TWR_ID", hDModel.code, DbType.String);
            sp.Command.Parameters.Add("@FLR_LOC_ID", hDModel.Role_id, DbType.String);
            sp.Command.Parameters.Add("@FLR_CITY_CODE", hDModel.Password, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables[0].Rows.Count == 0)
            {
                return new { Message = "Not OK" };
            }
            else
            {
                return new { Message = "OK", Data = ds.Tables[0] };

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public object _PropertyType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<PropType> PRL = new List<PropType>();
                    PropType PT;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_ACTPROPTYPE");
                    sp.Command.AddParameter("@PropertyType", RType.PropertyType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            PT = new PropType();
                            PT.PN_PROPERTYTYPE = sdr["PN_PROPERTYTYPE"].ToString();
                            PT.PN_TYPEID = sdr["PN_TYPEID"].ToString();
                            PRL.Add(PT);
                        }
                    }
                    return new { Status = "true", items = PRL, total_count = PRL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    public object _FlooringType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<FlooringType> FTL = new List<FlooringType>();
                    FlooringType FT;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "PM_GET_FLOORING_TYPES");
                    sp.Command.AddParameter("@FlooringType", RType.FlooringType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            FT = new FlooringType();
                            FT.PM_FT_SNO = sdr["PM_FT_SNO"].ToString();
                            FT.PM_FT_TYPE = sdr["PM_FT_TYPE"].ToString();
                            FTL.Add(FT);
                        }
                    }
                    return new { Status = "true", items = FTL, total_count = FTL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    public object _InsuranceType(RaiseRequest RType)
    {
        if (!(RType.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(RType.APIKey);
                if (VALDB.AURID != null)
                {
                    List<InsuranceType> ITL = new List<InsuranceType>();
                    InsuranceType IT;
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GET_INSURANCE_TYPE");
                    sp.Command.AddParameter("@InsuranceType", RType.InsurenceType, DbType.String);


                    using (IDataReader sdr = sp.GetReader())
                    {
                        while (sdr.Read())
                        {
                            IT = new InsuranceType();
                            IT.ID = sdr["ID"].ToString();
                            IT.INSURANCE_TYPE = sdr["INSURANCE_TYPE"].ToString();
                            ITL.Add(IT);
                        }
                    }
                    return new { Status = "true", items = ITL, total_count = ITL.Count };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (Exception e)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(e); return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
            }

        }
        else { return new { Status = "false", Message = MessagesVM.Err }; }
    }
    //Return Categories
    public Categories _ReturnCategories(string SType, string Key)
    {
        Categories Categ = new Categories();
        if (!(Key == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(Key);
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_CATEGORIES");
                sp.Command.AddParameter("@CCAT", SType, DbType.String);
                sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {
                    while (sdr.Read())
                    {
                        Categ.MAINCAT = sdr["MAINCAT"].ToString();
                        Categ.SUBCAT = sdr["SUBCAT"].ToString();
                        Categ.CHILDCAT = sdr["CHILDCAT"].ToString();
                    }
                }
                return Categ;
            }
            catch (SqlException ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(ex);
                return Categ;
            }
        }
        else
        {
            return Categ;
        }
    }
    //View SLA
    public object _ViewSLADetails(RaiseRequest K)
    {
        if (!(K.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
                Categories VALTYPES = _ReturnCategories(K.ServiceType, K.APIKey);
                if (VALDB.AURID != null)
                {
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_GET_SLA_DETAILS_BY_LOCATION");
                    sp.Command.AddParameter("@LOC", VALDB.AUR_LOCATION, DbType.String);
                    sp.Command.AddParameter("@MCAT", VALTYPES.MAINCAT, DbType.String);
                    sp.Command.AddParameter("@SCAT", VALTYPES.SUBCAT, DbType.String);
                    sp.Command.AddParameter("@CCAT", VALTYPES.CHILDCAT, DbType.String);
                    sp.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.Int32);
                    ds = sp.GetDataSet();

                    return new { Status = "true", data = new { SLA = ds.Tables[0], Mapping = ds.Tables[1] } };
                }
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        return new { Status = "false", Message = MessagesVM.Err };
    }

    //Get SLA
    public object GetSLA(GETSLA K)
    {
        if (!(K.APIKey == null))
        {
            try
            {
                ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
                Categories VALTYPES = _ReturnCategories(K.ChildCategory, K.APIKey);
                if (VALDB.AURID != null)
                {
                    sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "CHECK_SERVICE_INCHARGE");
                    sp.Command.AddParameter("@LOC", K.Location, DbType.String);
                    sp.Command.AddParameter("@MAIN_CATEGORY", VALTYPES.MAINCAT, DbType.String);
                    sp.Command.AddParameter("@SUB_CATEGORY", VALTYPES.SUBCAT, DbType.String);
                    sp.Command.AddParameter("@CHILD_CATEGORY", VALTYPES.CHILDCAT, DbType.String);
                    sp.Command.AddParameter("@CMP_ID", VALDB.COMPANYID, DbType.Int32);
                    //ds = sp.GetDataSet();
                    int i = (int)sp.ExecuteScalar();
                    return new { Status = i };
                }
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        return new { Status = "false", Message = MessagesVM.Err };
    }

    public object Getserviceinchege(GETINCH svm)
    {
        try
        {
            //ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
            //Categories VALTYPES = _ReturnCategories(svm.ChildCategory, svm.APIKey);

            DataSet ds = new DataSet();
            HDModel mn = new HDModel();
            mn.CompanyId = svm.CompanyId;
            string DB = _ReturnDB(mn);
            //string DB = "Marks_Spencer.[dbo]";
            sp = new SubSonic.StoredProcedure(DB + "." + "[CHECK_SERVICE_INCHARGE]");
            sp.Command.Parameters.Add("@LOC", svm.Location, DbType.String);
            sp.Command.AddParameter("@MAIN_CATEGORY", svm.MAINCAT, DbType.String);
            sp.Command.Parameters.Add("@SUB_CATEGORY", svm.SUBCAT, DbType.String);
            sp.Command.Parameters.Add("@CHILD_CATEGORY", svm.CHILDCAT, DbType.String);
            sp.Command.AddParameter("@CMP_ID", svm.COMPANYID, DbType.Int32);
            ds = sp.GetDataSet();

            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                int resultValue = Convert.ToInt32(ds.Tables[0].Rows[0][0]); // Assuming the result is in the first column of the first row
                if (resultValue == 0)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                // Handle the case when there's no result or an error occurred
                // Return an appropriate value based on your requirements
                return -1; // For example, returning -1 to indicate an error or no result
            }

        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };

    }

    public object SlaNotDefine(SLAND svm)
    {
        try
        {
            //ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
            //Categories VALTYPES = _ReturnCategories(svm.ChildCategory, svm.APIKey);

            DataSet ds = new DataSet();
            HDModel mn = new HDModel();
            mn.CompanyId = svm.CompanyId;
            string DB = _ReturnDB(mn);
            //string DB = "Marks_Spencer.[dbo]";
            sp = new SubSonic.StoredProcedure(DB + "." + "[CHECK_ESCALTIONS]");
            sp.Command.Parameters.Add("@LCM_CODE", svm.Location, DbType.String);
            sp.Command.AddParameter("@MAIN_CATEGORY", svm.MAINCAT, DbType.String);
            sp.Command.Parameters.Add("@SUB_CATEGORY", svm.SUBCAT, DbType.String);
            sp.Command.Parameters.Add("@CHILD_CATEGORY", svm.CHILDCAT, DbType.String);
            sp.Command.AddParameter("@CMP_ID", svm.COMPANYID, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                int resultValue = Convert.ToInt32(ds.Tables[0].Rows[0][0]); // Assuming the result is in the first column of the first row
                if (resultValue == 0)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                // Handle the case when there's no result or an error occurred
                // Return an appropriate value based on your requirements
                return -1; // For example, returning -1 to indicate an error or no result
            }

        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };

    }


    //Submit Raise Request
    public object _RaiseHDRequest(HttpRequest httpRequest)
    {
        try
        {
            String Key = httpRequest.Params["APIKey"];
            String LType = httpRequest.Params["LocationType"];
            String SType = httpRequest.Params["ServiceType"];
            String Prb = httpRequest.Params["Problem"];
            String RCall = httpRequest.Params["RepeatCall"];
            String ICall = httpRequest.Params["ImpactType"];
            String UCall = httpRequest.Params["UrgencyType"];
            String UPhone = httpRequest.Params["PhoneNo"];

            var cnt = GetTotalHDReqCount(Key);
            string HelDeskReq = "";
            HelDeskReq = (DateTime.Now.ToString("ddMMyyyy") + '/' + cnt).ToString();

            ValidateKey VALDB = Secure._ValidateAPIKey(Key);
            Categories VALTYPES = _ReturnCategories(SType, Key);
            if (httpRequest.Files.AllKeys.Length == 0)
            {
                if (VALDB.AURID != null)
                {
                    SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_RAISE_HELP_DESK_REQUEST");
                    sp.Command.AddParameter("@REQ_ID", HelDeskReq, DbType.String);
                    sp.Command.AddParameter("@STATUS", 1, DbType.String);
                    sp.Command.AddParameter("@LOC_CODE", LType, DbType.String);
                    sp.Command.AddParameter("@MNC_CODE", VALTYPES.MAINCAT, DbType.String);
                    sp.Command.AddParameter("@SUB_CAT", VALTYPES.SUBCAT, DbType.String);
                    sp.Command.AddParameter("@CHILD_CAT", VALTYPES.CHILDCAT, DbType.String);
                    sp.Command.AddParameter("@REPEATCALL", RCall, DbType.String);
                    sp.Command.AddParameter("@PROB_DESC", Prb, DbType.String);
                    sp.Command.AddParameter("@IMPACT", ICall, DbType.String);
                    sp.Command.AddParameter("@URGENCY", UCall, DbType.String);
                    sp.Command.AddParameter("@SER_CALL_LOG_BY", VALDB.AURID, DbType.String);
                    sp.Command.AddParameter("@SER_REQ_TYPE", 0, DbType.String);
                    sp.Command.AddParameter("@RAISED_BY", VALDB.AURID, DbType.String);
                    sp.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.Int32);
                    sp.Command.AddParameter("@PHONENO", UPhone, DbType.String);
                    sp.ExecuteScalar();
                    return new { Status = "true", Message = MessagesVM.RequestRaised, RequestId = HelDeskReq };
                }
            }
            else
            {
                string obj = InsertUploadImages(httpRequest, HelDeskReq, VALDB.TENANT_ID, VALDB.AURID);
                if (obj == "1")
                {
                    try
                    {
                        if (VALDB.AURID != null)
                        {
                            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_RAISE_HELP_DESK_REQUEST");
                            sp.Command.AddParameter("@REQ_ID", HelDeskReq, DbType.String);
                            sp.Command.AddParameter("@STATUS", 1, DbType.String);
                            sp.Command.AddParameter("@LOC_CODE", LType, DbType.String);
                            sp.Command.AddParameter("@MNC_CODE", VALTYPES.MAINCAT, DbType.String);
                            sp.Command.AddParameter("@SUB_CAT", VALTYPES.SUBCAT, DbType.String);
                            sp.Command.AddParameter("@CHILD_CAT", VALTYPES.CHILDCAT, DbType.String);
                            sp.Command.AddParameter("@REPEATCALL", RCall, DbType.String);
                            sp.Command.AddParameter("@PROB_DESC", Prb, DbType.String);
                            sp.Command.AddParameter("@IMPACT", ICall, DbType.String);
                            sp.Command.AddParameter("@URGENCY", UCall, DbType.String);
                            sp.Command.AddParameter("@SER_CALL_LOG_BY", VALDB.AURID, DbType.String);
                            sp.Command.AddParameter("@SER_REQ_TYPE", 0, DbType.String);
                            sp.Command.AddParameter("@RAISED_BY", VALDB.AURID, DbType.String);
                            sp.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.Int32);
                            sp.Command.AddParameter("@PHONENO", UPhone, DbType.String);
                            sp.ExecuteScalar();
                            return new { Status = "true", Message = MessagesVM.RequestRaised, RequestId = HelDeskReq };
                        }
                        else
                        {
                            return new { Status = "false", data = (object)null };
                        }
                    }
                    catch (SqlException SqlExcp)
                    {
                        ErrorHandler erhndlr = new ErrorHandler();
                        erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
                    }
                    catch (Exception ex)
                    {
                        ErrorHandler erhndlr = new ErrorHandler();
                        erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
                    };
                }
                else
                {
                    return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
                }
            }
            return new { Status = "false", Message = MessagesVM.Err, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    //Upload Image
    public string InsertUploadImages(HttpRequest httpRequest, string RequestId, string DB, string User)
    {
        try
        {
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {
                var req = httpRequest.Files[i];
                var fn = req.FileName;

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(DB + "." + "API_HD_INSERT_UPLOADED_FILES");
                sp2.Command.AddParameter("@REQ_ID", RequestId, DbType.String);
                sp2.Command.AddParameter("@STATUS_ID", 1, DbType.Int32);
                sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", User, DbType.String);
                sp2.ExecuteScalar();

                var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "HelpdeskManagement\\HDImages\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                req.SaveAs(filePath);
            }
            return "1";
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }
    //View Requests
    public object _ViewRequests(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_VIEW_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.Int32);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.Int32);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }

    //View&Update Requests
    public object _ViewUpdateRequestList(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_UPDATE_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.Int32);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.Int32);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Search Requests
    public object _SearchRequests(string Key, string FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        ViewReq req;
        List<ViewReq> Srchlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_FILTER_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FILTER", FilterParam, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        Srchlst.Add(req);
                    }
                }
                return new { Status = "true", data = Srchlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Get Status
    public object _Status(string Key, int Type)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        HDStatus Sts;
        List<HDStatus> Stalst = new List<HDStatus>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_STATUS");
                sp2.Command.AddParameter("@TYPE", Type, DbType.Int32);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        Sts = new HDStatus();
                        Sts.STA_ID = Convert.ToInt32(sdr["STA_ID"].ToString());
                        Sts.STA_TITLE = sdr["STA_DESC"].ToString();
                        Stalst.Add(Sts);
                    }
                }
                return new { Status = "true", data = Stalst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Cancel Request
    public object _CancelRequest(HDParams Param)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_CANCEL_VIEW_REQUISITON");
                sp.Command.AddParameter("@SER_ID", Param.RequestID, DbType.String);
                sp.Command.AddParameter("@SER_STATUS", 3, DbType.String);
                sp.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp.Command.AddParameter("@SER_PROB_DESC", Param.ProblemDesc, DbType.String);
                int pcnt = (int)sp.ExecuteScalar();
                if (pcnt == 1)
                {
                    return new { Status = "true", Message = MessagesVM.CancelRequest };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Modify Request
    public object _ModifyRequest(ModifyViewReq MReq)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(MReq.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_MODIFY_REQUEST");
                sp2.Command.AddParameter("@REQ_ID", MReq.RequestId, DbType.String);
                sp2.Command.AddParameter("@STATUS_ID", 2, DbType.Int32);
                sp2.Command.AddParameter("@IMPACT", MReq.Impact, DbType.String);
                sp2.Command.AddParameter("@URGENCY", MReq.Urgency, DbType.String);
                sp2.Command.AddParameter("@REPEAT", MReq.Repeat, DbType.String);
                sp2.Command.AddParameter("@REMARKS", MReq.Remarks, DbType.String);
                sp2.Command.AddParameter("@PROBLEM", MReq.Problem, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FEEDBACK", MReq.FeedBack, DbType.String);
                sp2.ExecuteScalar();
                return new { Status = "true", Message = MessagesVM.UPDATE_REQ };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Submit Feedback
    public object _SubmitFeedback(HDParams Param)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_INSERT_USER_FEEDBACK");
                sp.Command.AddParameter("@REQ_ID", Param.RequestID, DbType.String);
                sp.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp.Command.AddParameter("@FBD_CODE", Param.Feedback, DbType.String);
                sp.Command.AddParameter("@REMARKS", Param.Remarks, DbType.String);
                sp.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                int pcnt = (int)sp.ExecuteScalar();
                if (pcnt == 1)
                {
                    return new { Status = "true", Message = MessagesVM.FeedbackRequest };
                }
                else { return new { Status = "false", Message = MessagesVM.Err }; }
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Reassign Users
    public object _ReAssign(RaiseRequest Param)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        Categories VALTYPES = _ReturnCategories(Param.ServiceType, Param.APIKey);
        ReAssignName ReAsg;
        List<ReAssignName> ReAsglst = new List<ReAssignName>();
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_BIND_INCHARGES");
                sp.Command.AddParameter("@SEM_LOC_CODE", Param.LocationType, DbType.String);
                sp.Command.AddParameter("@SEM_MNC_CODE", VALTYPES.MAINCAT, DbType.String);
                sp.Command.AddParameter("@SEM_SUBC_CODE", VALTYPES.SUBCAT, DbType.String);
                sp.Command.AddParameter("@SEM_CHC_CODE", VALTYPES.CHILDCAT, DbType.String);
                sp.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {

                    while (sdr.Read())
                    {
                        ReAsg = new ReAssignName();
                        ReAsg.REASSING_EMP_ID = sdr["AUR_ID"].ToString();
                        ReAsg.REASSING_EMP_NAME = sdr["ASSIGNED_TO"].ToString();
                        ReAsglst.Add(ReAsg);
                    }

                }
                return new { Status = "true", data = ReAsglst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Update Picture
    public object _UpdatePicture(HttpRequest httpRequest)
    {
        String Key = httpRequest.Params["APIKey"];
        String RID = httpRequest.Params["RequestId"];

        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
                {
                    var req = httpRequest.Files[i];
                    var fn = req.FileName;

                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_UPDATE_PICTURE");
                    sp2.Command.AddParameter("@REQ_ID", RID, DbType.String);
                    sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                    sp2.Command.AddParameter("@STATUS_ID", 1, DbType.Int32);
                    sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                    sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                    sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                    sp2.ExecuteScalar();

                    //var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "HelpdeskManagement\\HDImages\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                    var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + VALDB.TENANT_ID + "\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                    req.SaveAs(filePath);
                }
                return new { Status = "true" };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Update AssetImage
    public object _UpdateAssetImage(HttpRequest httpRequest)
    {
        String Key = httpRequest.Params["APIKey"];
        String RID = httpRequest.Params["AstId"];
        String COMPANYID = httpRequest.Params["COMPANYID"];
        String AURID = httpRequest.Params["AURID"];

        HDModel mn = new HDModel();
        mn.CompanyId = COMPANYID;
        string DB = _ReturnDB(mn);
        //ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        if (AURID != null | AURID != "")
        {
            try
            {
                for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
                {
                    var req = httpRequest.Files[i];
                    var fn = req.FileName;
                    //var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + DB + "\\" + "AssetImages" + "\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);

                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(DB + "." + "API_HD_UPDATE_ASSET_IMAGES");
                    sp2.Command.AddParameter("@REQ_ID", RID, DbType.String);
                    sp2.Command.AddParameter("@FILENAME_DN", (DateTime.Now.ToString("ddMMyyyyHHmmss").ToString()) + fn, DbType.String);
                    sp2.Command.AddParameter("@FILENAME", fn, DbType.String);
                    //sp2.Command.AddParameter("@STATUS_ID", 1, DbType.Int32);
                    //sp2.Command.AddParameter("@UPLOAD_PATH", (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn, DbType.String);
                    sp2.Command.AddParameter("@COMPANYID", COMPANYID, DbType.String);
                    sp2.Command.AddParameter("@AURID", AURID, DbType.String);
                    sp2.ExecuteScalar();
                    if (!Directory.Exists(Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + DB + "\\" + "ASSET_IMG")))
                    {
                        Directory.CreateDirectory(Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + DB + "\\" + "ASSET_IMG"));
                    }
                    //var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "HelpdeskManagement\\HDImages\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);              
                    var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles\\" + DB + "\\" + "ASSET_IMG" + "\\" + (DateTime.Now.ToString("ddMMyyyyHHmmss").ToString()) + fn);

                    req.SaveAs(filePath);
                }
                return new { Status = "true" };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Remove Picture
    public object _RemovePicture(Image Img)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Img.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_REMOVE_UPLOADED_FILES");
                sp2.Command.AddParameter("@ID", Img.IMG_ID, DbType.String);
                sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                sp2.ExecuteScalar();
                return new { Status = "true" };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Get Request Details
    public object _ViewDetails(HDParams Params)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Params.APIKey);
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_VIEW_DETAILS");
                sp2.Command.AddParameter("@REQ_ID", Params.RequestID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                ds = sp2.GetDataSet();
                return new { Status = "true", data = new { RequestDetails = ds.Tables[0], Images = ds.Tables[1] } };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Dropdown List Impact, Urgency, Repeat , Feedback,Status
    public object _ConsolidatedDDL(string Key)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_CONSOLIDATED_DDLIST");
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                DataSet cds = sp2.GetDataSet();
                return new { Status = "true", data = new { Impact = cds.Tables[0], Urgency = cds.Tables[1], Repeat = cds.Tables[2], Feedback = cds.Tables[3], Status = cds.Tables[4] } };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    #endregion

    #region MileStone #04
    //Get Update Request
    public object _UpdateViewRequests(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_PENDING_REQUESTS_INCHARGE");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.String);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.REQ_STATUS = sdr["SER_STATUS"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Search Update Request
    public object _SearchUpdateRequests(string Key, string FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        ViewReq req;
        List<ViewReq> Srchlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_FILTER_UPDATE_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FILTER", FilterParam, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        Srchlst.Add(req);
                    }
                }
                return new { Status = "true", data = Srchlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Search ReAssign Request
    public object _SearchReAssignRequests(string Key, string FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        ViewReq req;
        List<ViewReq> Srchlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_FILTER_REASSIGN_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FILTER", FilterParam, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        Srchlst.Add(req);
                    }
                }
                return new { Status = "true", data = Srchlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Multiple Request Approval
    public object _UpdateMultipleRequest(ListHDParams Param)
    {

        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                foreach (var values in Param.HdParmsList)
                {
                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_VIEW_UPDATE_REQ_UPDATE");
                    sp2.Command.AddParameter("@REQ_ID", values.RequestID, DbType.String);
                    sp2.Command.AddParameter("@STA_ID", values.Status, DbType.Int32);
                    sp2.Command.AddParameter("@REMARKS", values.Remarks, DbType.String);
                    sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                    sp2.Command.AddParameter("@COST", values.Cost, DbType.String);
                    sp2.ExecuteScalar();
                }
                return new { Status = "true", Message = MessagesVM.UPDATE_REQ };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }
    //Single Request Approval
    public object _UpadteSingleRequest(HttpRequest httpRequest)
    {

        String Key = httpRequest.Params["APIKey"];
        String RID = httpRequest.Params["RequestId"];
        String RMKS = httpRequest.Params["Remarks"];
        String STS = httpRequest.Params["Status"];
        string CST = httpRequest.Params["Cost"];
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_VIEW_UPDATE_REQ_UPDATE");
                sp2.Command.AddParameter("@REQ_ID", RID, DbType.String);
                sp2.Command.AddParameter("@STA_ID", STS, DbType.Int32);
                sp2.Command.AddParameter("@REMARKS", RMKS, DbType.String);
                sp2.Command.AddParameter("@COST", CST, DbType.String);

                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.ExecuteScalar();
                for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
                {
                    var req = httpRequest.Files[i];
                    var fn = req.FileName;

                    string obj = InsertUploadImages(httpRequest, RID, VALDB.TENANT_ID, VALDB.AURID);
                    if (obj == "1")
                    {

                        var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "HelpdeskManagement\\HDImages\\" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
                        req.SaveAs(filePath);
                    }
                }
                return new { Status = "true", Message = MessagesVM.UPDATE_REQ };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { data = (object)null };
        }
    }
    //Get ReAssign Details
    public object _ReAssignViewRequests(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_VIEW_REQUISITIONS_TO_ASSIGN");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.String);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MAINCAT_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBCAT_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHILDCAT_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.REQ_STATUS = sdr["SER_STATUS"].ToString();
                        req.ASSIGN = sdr["ASSIGNED_TO"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.REQ_STATUS_NAME = sdr["STATUS"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.ASSIGNED_TO_NAME = sdr["ASSIGNED_TO_NAME"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //ReAssign Single and Multiple Requests Approval
    public object _ReAssignMultipleRequest(ListHDParams Param)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                foreach (var values in Param.HdParmsList)
                {
                    SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_ASSIGN_REQUEST");
                    sp2.Command.AddParameter("@REQ_ID", values.RequestID, DbType.String);
                    sp2.Command.AddParameter("@ASSIGN_TO", values.AssignTo, DbType.String);
                    sp2.Command.AddParameter("@UPDATE_REMARKS", values.Remarks, DbType.String);
                    sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                    sp2.ExecuteScalar();
                }
                return new { Status = "true", Message = MessagesVM.REQ_REASSIGN_SUBMIT };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }

    //Reopen View Request Details
    public object _ReOpenViewRequests(Pagination K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_CLOSED_TICKET");
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@pageNum", K.PageNo, DbType.String);
                sp2.Command.AddParameter("@pageSize", K.PageSize, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHILD_CAT_CODE"].ToString();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.ASSIGN = sdr["ASSIGNED_TO"].ToString();
                        req.REQ_STATUS = sdr["SER_STATUS"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    //Reopen Ticket
    public object _ReOpenRequest(HDParams Param)
    {

        ValidateKey VALDB = Secure._ValidateAPIKey(Param.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_MODIFY_REQ_REOPEN_TICKET");
                sp2.Command.AddParameter("@REQ_ID", Param.RequestID, DbType.String);
                sp2.Command.AddParameter("@REMARKS", Param.Remarks, DbType.String);
                sp2.Command.AddParameter("@SER_CALL_TYPE", "Y", DbType.String);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.ExecuteScalar();
                return new { Status = "true", Message = MessagesVM.UM_RE_OPEN };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }
    //Search Reopen
    public object _SearchReOpenRequests(string Key, string FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(Key);
        ViewReq req;
        List<ViewReq> Srchlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_FILTER_REOPEN_REQUISITIONS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FILTER", FilterParam, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_STATUS = sdr["STA_ID"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_STATUS"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        req.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        Srchlst.Add(req);
                    }
                }
                return new { Status = "true", data = Srchlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    #endregion

    #region MileStone #05
    public object _GetReport(ReportParams Params)
    {
        ReportDataSet RP;
        List<ReportDataSet> RPLST = new List<ReportDataSet>();
        ValidateKey VALDB = Secure._ValidateAPIKey(Params.APIKey);
        if (VALDB.AURID != null | VALDB.AURID != "")
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_REPORT_BY_USER");
                sp2.Command.AddParameter("@FROMDATE", Params.FromDate, DbType.DateTime);
                sp2.Command.AddParameter("@TODATE", Params.ToDate, DbType.DateTime);
                sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@FLAG", 1, DbType.String);
                DataSet ds = new DataSet();
                ds = sp2.GetDataSet();
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        RP = new ReportDataSet();
                        RP.REQUEST_ID = sdr["RequisitionId"].ToString();
                        RP.SERVICE_TYPE = sdr["ChildCategory"].ToString();
                        RP.LOCATION = sdr["LocName"].ToString();
                        RP.REQ_BY = sdr["REQUESTEDBY_NAME"].ToString();
                        RP.STATUS = sdr["STA_TITLE"].ToString();
                        RP.SLA = sdr["DEFINED_TAT"].ToString();
                        RP.TAT = sdr["TAT"].ToString();
                        RP.REQ_DATE = sdr["CallLogDate"].ToString();
                        RP.ASSIGNED_TO = sdr["Helpdesk_InchargeName"].ToString();
                        RPLST.Add(RP);
                    }
                    sdr.Close();
                }
                return new { Status = "true", data = RPLST };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return new { Status = "false", Message = MessagesVM.Err };
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", Message = MessagesVM.Err };
        }
    }

    public object _SearchReport(ReportFilterParams FilterParam)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(FilterParam.APIKey);
        ReportDataSet RP;
        List<ReportDataSet> RPLST = new List<ReportDataSet>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_REPORT_BY_USER");
                sp2.Command.AddParameter("@FILTER", FilterParam.Filter, DbType.String);
                sp2.Command.AddParameter("@FROMDATE", FilterParam.FromDate, DbType.DateTime);
                sp2.Command.AddParameter("@TODATE", FilterParam.ToDate, DbType.DateTime);
                sp2.Command.AddParameter("@AUR_ID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@FLAG", 2, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        RP = new ReportDataSet();
                        RP.REQUEST_ID = sdr["SER_REQ_ID"].ToString();
                        RP.SERVICE_TYPE = sdr["CHC_TYPE_NAME"].ToString();
                        RP.LOCATION = sdr["LCM_NAME"].ToString();
                        RP.REQ_BY = sdr["AUR_KNOWN_AS"].ToString();
                        RP.STATUS = sdr["STA_TITLE"].ToString();
                        RP.SLA = sdr["SLA"].ToString();
                        RP.TAT = sdr["TAT_SLA"].ToString();
                        RP.SERE_ROL_ID = sdr["SERE_ROL_ID"].ToString();
                        RP.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                        RP.MOBILE_NO = sdr["SER_MOBILE"].ToString();
                        RPLST.Add(RP);
                    }
                }
                return new { Status = "true", data = RPLST };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }

    public object _GetTotalDashboardDetails(DashboardDetails K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        ViewReq req;
        List<ViewReq> reqlst = new List<ViewReq>();
        if (VALDB.AURID != null)
        {
            try
            {

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GN_TICKETS_DETAILS");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                sp2.Command.AddParameter("@STATUS", K.Status, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        req = new ViewReq();
                        req.CHILD_CATEGORY = sdr["CHILD_CATEGORY"].ToString();
                        req.REQUEST_ID = sdr["REQUEST_ID"].ToString();
                        req.REQ_BY = sdr["REQUESTED_BY"].ToString();
                        req.REQ_STATUS = sdr["REQ_STATUS"].ToString();
                        req.REQ_STATUS_NAME = sdr["REQ_NAME"].ToString();
                        req.MAIN_CAT_CODE = sdr["MNC_CODE"].ToString();
                        req.SUB_CAT_CODE = sdr["SUBC_CODE"].ToString();
                        req.CHILD_CAT_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        req.ASSIGN = sdr["ASSIGN"].ToString();
                        req.REQ_DATE = sdr["REQUESTED_DATE"].ToString();
                        req.LOCATION = sdr["LOCATION"].ToString();
                        req.MOBILENO = sdr["SER_MOBILE"].ToString();
                        reqlst.Add(req);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }

    public object _SLAViolation(Key K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        SLAViolation data;
        List<SLAViolation> reqlst = new List<SLAViolation>();
        if (VALDB.AURID != null)
        {
            try
            {

                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "GN_SLA_VIOLATION");
                sp2.Command.AddParameter("@AURID", VALDB.AURID, DbType.String);
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        data = new SLAViolation();
                        data.REQUEST_ID = sdr["SERE_SER_REQ_ID"].ToString();
                        data.MAIN_CATEGORY = sdr["MNC_NAME"].ToString();
                        data.SUB_CATEGORY = sdr["SUBC_NAME"].ToString();
                        data.SERVICE_TYPE = sdr["CHC_TYPE_NAME"].ToString();
                        data.PROBLEM_DESC = sdr["SER_PROB_DESC"].ToString();
                        data.STATUS = sdr["STA_TITLE"].ToString();
                        reqlst.Add(data);
                    }
                }
                return new { Status = "true", data = reqlst };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    #endregion

    #region Device Token Update

    public object _UpdateDeviceToken(DeviceToken Tkn)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "UPDATE_DEVICE_TOKEN");
            sp.Command.AddParameter("@API", Tkn.APIKey, DbType.String);
            sp.Command.AddParameter("@TOKEN", Tkn.Token, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows[0]["TKN"].ToString() == "1")
            {
                return new { Status = "true", Message = MessagesVM.UPDATE_TOKEN };
            }
            else
            {
                return new { Status = "false", Message = MessagesVM.Err };
            }
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };

    }
    #endregion

    #region Emergency Request
    public object _Emergency(Key K)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(K.APIKey);
        Emergency EM;
        List<Emergency> EMLST = new List<Emergency>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HDM_GET_EMERGENCY_REQUEST");
                sp2.Command.AddParameter("@COMPANYID", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        EM = new Emergency();
                        EM.SERVICE_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                        EM.SERVICE_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                        EM.SERVICE_TYPE_IMAGE = sdr["CHC_TYPE_IMAGE"].ToString();
                        EMLST.Add(EM);
                    }
                }
                return new { Status = "true", data = EMLST };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }
    #endregion

    #region Notifications

    public object _NotificationList(string APIKey)
    {
        ValidateKey VALDB = Secure._ValidateAPIKey(APIKey);
        NotificationHistory NT;
        List<NotificationHistory> NTLST = new List<NotificationHistory>();
        if (VALDB.AURID != null)
        {
            try
            {
                SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_NOTIFICATION_LIST");
                sp2.Command.AddParameter("@COMPANY", VALDB.COMPANYID, DbType.String);
                using (IDataReader sdr = sp2.GetReader())
                {
                    while (sdr.Read())
                    {
                        NT = new NotificationHistory();
                        NT.SNO = sdr["SNO"].ToString();
                        NT.NOTIF_BODY = sdr["NOTIF_BODY"].ToString();
                        NT.NOTIF_RESULT = sdr["NOTIF_RESULT"].ToString();
                        NT.NOTIF_REQUEST_ID_STATUS = sdr["NOTIF_REQUEST_ID_STATUS"].ToString();
                        NT.NOTIF_REQUEST_ID = sdr["NOTIF_REQUEST_ID"].ToString();
                        NTLST.Add(NT);
                    }
                }
                //return new { Status = "Ok", data = NTLST };
                return new { data = NTLST };
            }
            catch (SqlException SqlExcp)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
            }
            catch (Exception ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
            };
        }
        else
        {
            return new { Status = "false", data = (object)null };
        }
    }

    public object _UpdateNotificationStatus(StatusUpdate Tkn)
    {
        try
        {
            ValidateKey VALDB = Secure._ValidateAPIKey(Tkn.APIKey);
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(VALDB.TENANT_ID + "." + "API_HD_UPDATE_NOTIF_STATUS");
            sp.Command.AddParameter("@RID", Tkn.RequestId, DbType.String);
            sp.Command.AddParameter("@SNO", Tkn.Sno, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows[0]["STS"].ToString() == "1")
            {
                return new { Status = "true", Message = MessagesVM.StatusUpdated };
            }
            else
            {
                return new { Status = "false", Message = MessagesVM.Err };
            }
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };

    }

    //public object _ValidateGoogleEmail() { 

    //}
    #endregion

    #region Add Property

    //General Details
    public object GetGeneralDetails(HDModel main)
    {
        try
        {
            List<GetGeneralDetails> GDList = new List<GetGeneralDetails>();
            GetGeneralDetails GD = new GetGeneralDetails();
            List<RequestTypes> RequestList = new List<RequestTypes>();
            RequestTypes Request = new RequestTypes();
            List<PropertyNature> PMList = new List<PropertyNature>();
            PropertyNature PM = new PropertyNature();
            List<AcquistionThrgh> AQTList = new List<AcquistionThrgh>();
            AcquistionThrgh AQT = new AcquistionThrgh();
            List<Entity> EntityList = new List<Entity>();
            Entity Entity = new Entity();
            List<Locations> LocationsList = new List<Locations>();
            Locations Locations = new Locations();
            List<PropertyType> PropertyTypeList = new List<PropertyType>();
            PropertyType PropertyType = new PropertyType();
            List<Recommened> RecommenedList = new List<Recommened>();
            Recommened Recommened = new Recommened();
            List<FlooringTypes> FloorsList = new List<FlooringTypes>();
            FlooringTypes Floors = new FlooringTypes();
            List<InsuranceTypes> InsuranceTypesList = new List<InsuranceTypes>();
            InsuranceTypes InsuranceTypes = new InsuranceTypes();
            List<StampDuty> StampDutyList = new List<StampDuty>();
            StampDuty StampDuty = new StampDuty();

            string DB = _ReturnDB(main);
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_GENERAL_DETAILS_MOBILE");
                sp.Command.AddParameter("@USR_ID", main.UserId, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Request = new RequestTypes();
                    Request.PM_RT_SNO = Convert.ToInt32(ds.Tables[0].Rows[i]["PM_RT_SNO"].ToString());
                    Request.PM_RT_TYPE = ds.Tables[0].Rows[i]["PM_RT_TYPE"].ToString();
                    RequestList.Add(Request);
                }
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    PM = new PropertyNature();
                    PM.PM_PN_ID = Convert.ToInt32(ds.Tables[1].Rows[i]["PM_PN_ID"].ToString());
                    PM.PM_PN_NAME = ds.Tables[1].Rows[i]["PM_PN_NAME"].ToString();
                    PMList.Add(PM);
                }
                for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                {
                    AQT = new AcquistionThrgh();
                    AQT.PN_ACQISITION_ID = Convert.ToInt32(ds.Tables[2].Rows[i]["PN_ACQISITION_ID"].ToString());
                    AQT.PN_ACQISITION_THROUGH = ds.Tables[2].Rows[i]["PN_ACQISITION_THROUGH"].ToString();
                    AQTList.Add(AQT);
                }
                for (int i = 0; i < ds.Tables[3].Rows.Count; i++)
                {
                    Entity = new Entity();
                    Entity.CHE_CODE = ds.Tables[3].Rows[i]["CHE_CODE"].ToString();
                    Entity.CHE_NAME = ds.Tables[3].Rows[i]["CHE_NAME"].ToString();
                    EntityList.Add(Entity);
                }
                for (int i = 0; i < ds.Tables[4].Rows.Count; i++)
                {
                    Locations = new Locations();
                    Locations.LCM_CODE = ds.Tables[4].Rows[i]["LCM_CODE"].ToString();
                    Locations.LCM_NAME = ds.Tables[4].Rows[i]["LCM_NAME"].ToString();
                    Locations.LCM_CNY_ID = ds.Tables[4].Rows[i]["LCM_CNY_ID"].ToString();
                    Locations.LCM_CTY_ID = ds.Tables[4].Rows[i]["LCM_CTY_ID"].ToString();
                    LocationsList.Add(Locations);
                }
                for (int i = 0; i < ds.Tables[5].Rows.Count; i++)
                {
                    PropertyType = new PropertyType();
                    PropertyType.PN_TYPEID = Convert.ToInt32(ds.Tables[5].Rows[i]["PN_TYPEID"].ToString());
                    PropertyType.PN_PROPERTYTYPE = ds.Tables[5].Rows[i]["PN_PROPERTYTYPE"].ToString();
                    PropertyTypeList.Add(PropertyType);
                }
                for (int i = 0; i < ds.Tables[6].Rows.Count; i++)
                {
                    Recommened = new Recommened();
                    Recommened.PM_RCMD_ID = Convert.ToInt32(ds.Tables[6].Rows[i]["PM_RCMD_ID"].ToString());
                    Recommened.PM_RCMD_NAME = ds.Tables[6].Rows[i]["PM_RCMD_NAME"].ToString();
                    RecommenedList.Add(Recommened);
                }
                for (int i = 0; i < ds.Tables[7].Rows.Count; i++)
                {
                    Floors = new FlooringTypes();
                    Floors.PM_FT_SNO = Convert.ToInt32(ds.Tables[7].Rows[i]["PM_FT_SNO"].ToString());
                    Floors.PM_FT_TYPE = ds.Tables[7].Rows[i]["PM_FT_TYPE"].ToString();
                    FloorsList.Add(Floors);
                }
                for (int i = 0; i < ds.Tables[8].Rows.Count; i++)
                {
                    InsuranceTypes = new InsuranceTypes();
                    InsuranceTypes.ID = Convert.ToInt32(ds.Tables[8].Rows[i]["ID"].ToString());
                    InsuranceTypes.INSURANCE_TYPE = ds.Tables[8].Rows[i]["INSURANCE_TYPE"].ToString();
                    InsuranceTypesList.Add(InsuranceTypes);
                }
                for (int i = 0; i < ds.Tables[9].Rows.Count; i++)
                {
                    StampDuty = new StampDuty();
                    StampDuty.PM_STP_NAME = ds.Tables[9].Rows[i]["PM_STP_NAME"].ToString();
                    StampDutyList.Add(StampDuty);
                }
                GD.REQ_ID = DateTime.Now.ToString("MMddyyyy") + "/PRPREQ/" + ds.Tables[10].Rows[0]["REQ"].ToString();
                GD.RequestTypes = RequestList;
                GD.PropertyNature = PMList;
                GD.AcquistionThrgh = AQTList;
                GD.Entity = EntityList;
                GD.Locations = LocationsList;
                GD.PropertyType = PropertyTypeList;
                GD.Recommened = RecommenedList;
                GD.FlooringTypes = FloorsList;
                GD.InsuranceTypes = InsuranceTypesList;
                GD.StampDuty = StampDutyList;
                GDList.Add(GD);

                return new { GeneralDetailsList = GDList };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //Submit Propert Details
    public object SubmitPropertyDetails(ProposalDraftDetails main)
    {
        try
        {

            List<ImageClas> Imgclass = new List<ImageClas>();

            List<ImageClas> Imgclass1 = new List<ImageClas>();

            List<ImageClas> Imgclass2 = new List<ImageClas>();

            List<ImageClas> Imgclass3 = new List<ImageClas>();


            if (main.SIGNAGEIMAGES != null)
            {
                Imgclass.Add(main.SIGNAGEIMAGES);
            }
            if (main.AC_OUTDOOR_IMAGE != null)
            {
                Imgclass1.Add(main.AC_OUTDOOR_IMAGE);
            }
            if (main.GSB_IMAGE != null)
            {
                Imgclass2.Add(main.GSB_IMAGE);
            }
            if (main.IMAGES != null)
            {
                Imgclass3 = main.IMAGES;
            }

            HDModel mn = new HDModel();
            mn.CompanyId = main.CompanyId;
            string DB = _ReturnDB(mn);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.AUR_ID));
            //sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_GENERAL_DETAILS_MOBILE");
            String proc_name = DB + "." + "PM_PROPERTY_SAVEASDRAFT_DETAILS_MOBILE";
            SqlParameter[] param = new SqlParameter[132];

            // General Details
            param[0] = new SqlParameter("@REQUEST_ID", SqlDbType.VarChar);
            param[0].Value = main.PM_PPT_PM_REQ_ID;
            param[1] = new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar);
            param[1].Value = main.PM_PPT_REQ_TYPE;
            param[2] = new SqlParameter("@PROP_NATURE", SqlDbType.VarChar);
            param[2].Value = main.PM_PPT_NATURE;
            param[3] = new SqlParameter("@ACQ_TRH", SqlDbType.VarChar);
            param[3].Value = main.PM_PPT_ACQ_THR;
            param[4] = new SqlParameter("@CITY", SqlDbType.VarChar);
            param[4].Value = main.PM_PPT_CTY_CODE;
            if (main.PM_PPT_REQ_TYPE == "New")
            {

                param[5] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
                param[5].Value = main.PM_PPT_LOC_CODE;
                param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
                param[6].Value = main.PM_PPT_TOW_CODE;
                param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
                param[7].Value = main.PM_PPT_FLR_CODE;
                param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
                param[122].Value = main.PM_PPT_TOT_FLRS;
                param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
                param[123].Value = "";
                param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
                param[124].Value = "";
            }
            else if (main.PM_PPT_REQ_TYPE == "Relocation")
            {
                param[5] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
                param[5].Value = main.PM_PPT_LOC_CODE;
                param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
                param[6].Value = main.PM_PPT_TOW_CODE;
                param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
                param[7].Value = main.PM_PPT_FLR_CODE;
                param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
                param[122].Value = main.PM_PPT_TOT_FLRS;
                param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
                param[123].Value = main.PM_PPT_RELOC_NAME;
                param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
                param[124].Value = main.PM_PPT_RELOC_ADDRESS;
            }
            else
            {
                param[5] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
                param[5].Value = main.PM_PPT_LOC_CODE;
                param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
                param[6].Value = main.PM_PPT_TOW_CODE;
                param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
                param[7].Value = main.PM_PPT_FLR_CODE;
                param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
                param[122].Value = main.PM_PPT_TOT_FLRS;
                param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
                param[123].Value = "";
                param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
                param[124].Value = "";
            }

            param[8] = new SqlParameter("@TOI_BLKS", SqlDbType.Int);
            param[8].Value = main.PM_PPT_TOT_TOI;
            param[9] = new SqlParameter("@PROP_TYPE", SqlDbType.VarChar);
            param[9].Value = main.PM_PPT_TYPE;
            param[10] = new SqlParameter("@PROP_NAME", SqlDbType.VarChar);
            param[10].Value = main.PM_PPT_NAME;
            param[11] = new SqlParameter("@ESTD_YR", SqlDbType.DateTime);
            param[11].Value = main.PM_PPT_ESTD;
            param[12] = new SqlParameter("@AGE", SqlDbType.VarChar);
            param[12].Value = main.PM_PPT_AGE;
            param[13] = new SqlParameter("@PROP_ADDR", SqlDbType.VarChar);
            param[13].Value = main.PM_PPT_ADDRESS;
            param[14] = new SqlParameter("@SING_LOC", SqlDbType.VarChar);
            param[14].Value = main.PM_PPT_SIGN_LOC;
            param[15] = new SqlParameter("@SOC_NAME", SqlDbType.VarChar);
            param[15].Value = main.PM_PPT_SOC_NAME;
            param[16] = new SqlParameter("@LATITUDE", SqlDbType.VarChar);
            param[16].Value = main.PM_PPT_LAT;
            param[17] = new SqlParameter("@LONGITUDE", SqlDbType.VarChar);
            param[17].Value = main.PM_PPT_LONG;
            param[18] = new SqlParameter("@SCOPE_WK", SqlDbType.VarChar);
            param[18].Value = main.PM_PPT_OWN_SCOPE;
            param[19] = new SqlParameter("@RECM_PROP", SqlDbType.VarChar);
            param[19].Value = main.PM_PPT_RECOMMENDED;
            param[20] = new SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar);
            param[20].Value = main.PM_PPT_RECO_REM;

            //Owner Details
            param[21] = new SqlParameter("@OWN_NAME", SqlDbType.VarChar);
            param[21].Value = main.PM_OWN_NAME;
            param[22] = new SqlParameter("@OWN_PH", SqlDbType.VarChar);
            param[22].Value = main.PM_OWN_PH_NO;
            param[23] = new SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar);
            param[23].Value = main.PM_PREV_OWN_NAME;
            param[24] = new SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar);
            param[24].Value = main.PM_PREV_OWN_PH_NO;
            param[25] = new SqlParameter("@OWN_EMAIL", SqlDbType.VarChar);
            param[25].Value = main.PM_OWN_EMAIL;

            //Area Details
            param[26] = new SqlParameter("@CARPET_AREA", SqlDbType.Decimal);
            param[26].Value = Convert.ToDecimal(main.PM_AR_CARPET_AREA);
            param[27] = new SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal);
            param[27].Value = Convert.ToDecimal(main.PM_AR_BUA_AREA);
            param[28] = new SqlParameter("@COMMON_AREA", SqlDbType.Decimal);
            param[28].Value = Convert.ToDecimal(main.PM_AR_COM_AREA);
            param[29] = new SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal);
            param[29].Value = Convert.ToDecimal(main.PM_AR_RENT_AREA);
            param[30] = new SqlParameter("@USABLE_AREA", SqlDbType.Decimal);
            param[30].Value = Convert.ToDecimal(main.PM_AR_USABEL_AREA);
            param[31] = new SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal);
            param[31].Value = Convert.ToDecimal(main.PM_AR_SBU_AREA);
            param[32] = new SqlParameter("@PLOT_AREA", SqlDbType.Decimal);
            param[32].Value = Convert.ToDecimal(main.PM_AR_PLOT_AREA);
            param[33] = new SqlParameter("@FTC_HIGHT", SqlDbType.Decimal);
            param[33].Value = main.PM_AR_FTC_HIGHT;
            param[34] = new SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal);
            param[34].Value = main.PM_AR_FTBB_HIGHT;
            param[35] = new SqlParameter("@MAX_CAPACITY", SqlDbType.Int);
            param[35].Value = Convert.ToInt32(main.PM_AR_MAX_CAP);
            param[36] = new SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int);
            param[36].Value = main.PM_AR_OPT_CAP;
            param[37] = new SqlParameter("@SEATING_CAPACITY", SqlDbType.Int);
            param[37].Value = main.PM_AR_SEATING_CAP;
            param[38] = new SqlParameter("@FLR_TYPE", SqlDbType.VarChar);
            param[38].Value = main.PM_AR_FLOOR_TYPE;
            param[39] = new SqlParameter("@FSI", SqlDbType.VarChar);
            param[39].Value = main.PM_AR_FSI;
            param[40] = new SqlParameter("@FSI_RATIO", SqlDbType.Decimal);
            param[40].Value = main.PM_AR_FSI_RATIO;
            param[41] = new SqlParameter("@PFR_EFF", SqlDbType.VarChar);
            param[41].Value = main.PM_AR_PREF_EFF;
            //PURCHASE DETAILS
            param[42] = new SqlParameter("@PUR_PRICE", SqlDbType.Decimal);
            param[42].Value = main.PM_PUR_PRICE;
            param[43] = new SqlParameter("@PUR_DATE", SqlDbType.DateTime);
            param[43].Value = main.PM_PUR_DATE;
            param[44] = new SqlParameter("@MARK_VALUE", SqlDbType.Decimal);
            param[44].Value = main.PM_PUR_MARKET_VALUE;
            //GOVT DETAILS
            param[45] = new SqlParameter("@IRDA", SqlDbType.VarChar);
            param[45].Value = main.PM_GOV_IRDA;
            param[46] = new SqlParameter("@PC_CODE", SqlDbType.VarChar);
            param[46].Value = main.PM_GOV_PC_CODE;
            param[47] = new SqlParameter("@PROP_CODE", SqlDbType.VarChar);
            param[47].Value = main.PM_GOV_PROP_CODE;
            param[48] = new SqlParameter("@UOM_CODE", SqlDbType.VarChar);
            param[48].Value = main.PM_GOV_UOM_CODE;
            //INSURANCE DETAILS
            param[49] = new SqlParameter("@IN_TYPE", SqlDbType.VarChar);
            param[49].Value = main.PM_INS_TYPE;
            param[50] = new SqlParameter("@IN_VENDOR", SqlDbType.VarChar);
            param[50].Value = main.PM_INS_VENDOR;
            param[51] = new SqlParameter("@IN_AMOUNT", SqlDbType.Decimal);
            param[51].Value = main.PM_INS_AMOUNT;
            param[52] = new SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar);
            param[52].Value = main.PM_INS_PNO;
            param[53] = new SqlParameter("@IN_SDATE", SqlDbType.DateTime);
            param[53].Value = main.PM_INS_START_DT;
            param[54] = new SqlParameter("@IN_EDATE", SqlDbType.DateTime);
            param[54].Value = main.PM_INS_END_DT;
            param[55] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[55].Value = main.AUR_ID;
            param[56] = new SqlParameter("@CMP_ID", SqlDbType.VarChar);
            param[56].Value = 1;
            param[57] = new SqlParameter("@OWN_LANDLINE", SqlDbType.VarChar);
            param[57].Value = main.PM_OWN_LANDLINE;
            param[58] = new SqlParameter("@POA_SIGNED", SqlDbType.VarChar);
            param[58].Value = main.PM_POA_SIGN;


            //POA Details
            param[59] = new SqlParameter("@POA_NAME", SqlDbType.VarChar);
            param[59].Value = main.PM_POA_NAME;
            param[60] = new SqlParameter("@POA_ADDRESS", SqlDbType.VarChar);
            param[60].Value = main.PM_POA_ADDRESS;
            param[61] = new SqlParameter("@POA_MOBILE", SqlDbType.VarChar);
            param[61].Value = main.PM_POA_MOBILE;
            param[62] = new SqlParameter("@POA_EMAIL", SqlDbType.VarChar);
            param[62].Value = main.PM_POA_EMAIL;
            param[63] = new SqlParameter("@POA_LLTYPE", SqlDbType.VarChar);
            param[63].Value = main.PM_POA_LL_TYPE;
            param[64] = new SqlParameter("@IMAGES", SqlDbType.Structured);
            param[64].Value = UtilityService.ConvertToDataTable(Imgclass3);
            param[65] = new SqlParameter("@ENTITY", SqlDbType.VarChar);
            param[65].Value = main.PM_PPT_ENTITY;
            //Physical Condition
            param[66] = new SqlParameter("@PHYCDTN_WALLS", SqlDbType.VarChar);
            param[66].Value = main.PM_PHYCDTN_WALLS;
            param[67] = new SqlParameter("@PHYCDTN_ROOF", SqlDbType.VarChar);
            param[67].Value = main.PM_PHYCDTN_ROOF;
            param[68] = new SqlParameter("@PHYCDTN_CEILING", SqlDbType.VarChar);
            param[68].Value = main.PM_PHYCDTN_CEILING;
            param[69] = new SqlParameter("@PHYCDTN_WINDOWS", SqlDbType.VarChar);
            param[69].Value = main.PM_PHYCDTN_WINDOWS;
            param[70] = new SqlParameter("@PHYCDTN_DAMAGE", SqlDbType.VarChar);
            param[70].Value = main.PM_PHYCDTN_DAMAGE;
            param[71] = new SqlParameter("@PHYCDTN_SEEPAGE", SqlDbType.VarChar);
            param[71].Value = main.PM_PHYCDTN_SEEPAGE;
            param[72] = new SqlParameter("@PHYCDTN_OTHRTNT", SqlDbType.VarChar);
            param[72].Value = main.PM_PHYCDTN_OTHR_TNT;

            //Landlord's Scope of work
            param[73] = new SqlParameter("@LL_VITRIFIED", SqlDbType.VarChar);
            param[73].Value = main.PM_LLS_VITRIFIED;
            param[74] = new SqlParameter("@LL_VITRIFIED_RMKS", SqlDbType.VarChar);
            param[74].Value = main.PM_LLS_VITRIFIED_RMKS;
            param[75] = new SqlParameter("@LL_WASHRMS", SqlDbType.VarChar);
            param[75].Value = main.PM_LLS_WASHROOMS;
            param[76] = new SqlParameter("@LL_WASHRM_RMKS", SqlDbType.VarChar);
            param[76].Value = main.PM_LLS_WASH_RMKS;
            param[77] = new SqlParameter("@LL_PANTRY", SqlDbType.VarChar);
            param[77].Value = main.PM_LLS_PANTRY;
            param[78] = new SqlParameter("@LL_PANTRY_RMKS", SqlDbType.VarChar);
            param[78].Value = main.PM_LLS_PANTRY_RMKS;
            param[79] = new SqlParameter("@LL_SHUTTER", SqlDbType.VarChar);
            param[79].Value = main.PM_LLS_SHUTTER;
            param[80] = new SqlParameter("@LL_SHUTTER_RMKS", SqlDbType.VarChar);
            param[80].Value = main.PM_LLS_SHUTTER_RMKS;
            param[81] = new SqlParameter("@LL_OTHERS", SqlDbType.VarChar);
            param[81].Value = main.PM_LLS_OTHERS;
            param[82] = new SqlParameter("@LL_OTHERS_RMKS", SqlDbType.VarChar);
            param[82].Value = main.PM_LLS_OTHERS_RMKS;
            param[83] = new SqlParameter("@LL_WORK_DAYS", SqlDbType.VarChar);
            param[83].Value = main.PM_LLS_WORK_DAYS;
            param[84] = new SqlParameter("@LL_ELEC_EXISTING", SqlDbType.VarChar);
            param[84].Value = main.PM_LLS_ELEC_EXISTING;
            param[85] = new SqlParameter("@LL_ELEC_REQUIRED", SqlDbType.VarChar);
            param[85].Value = main.PM_LLS_ELEC_REQUIRED;
            //Other Details
            param[86] = new SqlParameter("@DOC_TAX", SqlDbType.VarChar);
            param[86].Value = main.PM_DOC_TAX;
            param[87] = new SqlParameter("@DOC_TAX_RMKS", SqlDbType.VarChar);
            param[87].Value = main.PM_DOC_TAX_RMKS;
            param[88] = new SqlParameter("@DOC_OTHR_INFO", SqlDbType.VarChar);
            param[88].Value = main.PM_DOC_OTHER_INFO;
            param[89] = new SqlParameter("@OTHR_FLOORING", SqlDbType.VarChar);
            param[89].Value = main.PM_OTHR_FLOORING;
            param[90] = new SqlParameter("@OTHR_FLRG_RMKS", SqlDbType.VarChar);
            param[90].Value = main.PM_OTHR_FLRG_RMKS;
            param[91] = new SqlParameter("@OTHR_WASH_EXISTING", SqlDbType.VarChar);
            param[91].Value = main.PM_OTHR_WASHRM_EXISTING;
            param[92] = new SqlParameter("@OTHR_WASH_REQUIRED", SqlDbType.VarChar);
            param[92].Value = main.PM_OTHR_WASHRM_REQUIRED;
            param[93] = new SqlParameter("@OTHR_POTABLE_WTR", SqlDbType.VarChar);
            param[93].Value = main.PM_OTHR_POTABLE_WTR;

            //Cost Details
            param[94] = new SqlParameter("@COST_RENT", SqlDbType.VarChar);
            param[94].Value = main.PM_COST_RENT;
            param[95] = new SqlParameter("@COST_RENT_SFT", SqlDbType.VarChar);
            param[95].Value = main.PM_COST_RENT_SFT;
            param[96] = new SqlParameter("@COST_RATIO", SqlDbType.VarChar);
            param[96].Value = main.PM_COST_RATIO;
            param[97] = new SqlParameter("@COST_OWN_SHARE", SqlDbType.VarChar);
            param[97].Value = main.PM_COST_OWN_SHARE;
            param[98] = new SqlParameter("@COST_SECDEP", SqlDbType.VarChar);
            param[98].Value = main.PM_COST_SECDEPOSIT;
            param[99] = new SqlParameter("@COST_GST", SqlDbType.VarChar);
            param[99].Value = main.PM_COST_GST;
            param[100] = new SqlParameter("@COST_MAINTENANCE", SqlDbType.VarChar);
            param[100].Value = main.PM_COST_MAINTENANCE;
            param[101] = new SqlParameter("@COST_ESC", SqlDbType.VarChar);
            param[101].Value = main.PM_COST_ESC_RENTALS;
            param[102] = new SqlParameter("@COST_RENT_FREE", SqlDbType.VarChar);
            param[102].Value = main.PM_COST_RENT_FREE;
            param[103] = new SqlParameter("@COST_STAMP", SqlDbType.VarChar);
            param[103].Value = main.PM_COST_STAMP;
            param[104] = new SqlParameter("@COST_AGREE", SqlDbType.VarChar);
            param[104].Value = main.PM_COST_AGREEMENT_PERIOD;
            //DOCUMENTS VAILABLE
            param[105] = new SqlParameter("@DOC_TITLE", SqlDbType.VarChar);
            param[105].Value = main.PM_DOC_TITLE;
            param[106] = new SqlParameter("@DOC_TITLE_RMKS", SqlDbType.VarChar);
            param[106].Value = main.PM_DOC_TITLLE_RMKS;
            param[107] = new SqlParameter("@DOC_OCCUP", SqlDbType.VarChar);
            param[107].Value = main.PM_DOC_OCCUPANCY;
            param[108] = new SqlParameter("@DOC_OCCUP_RMKS", SqlDbType.VarChar);
            param[108].Value = main.PM_DOC_OCC_RMKS;
            param[109] = new SqlParameter("@DOC_BUILD", SqlDbType.VarChar);
            param[109].Value = main.PM_DOC_BUILD;
            param[110] = new SqlParameter("@DOC_BUILD_RMKS", SqlDbType.VarChar);
            param[110].Value = main.PM_DOC_BUILD_RMKS;
            param[111] = new SqlParameter("@DOC_PAN", SqlDbType.VarChar);
            param[111].Value = main.PM_DOC_PAN;
            param[112] = new SqlParameter("@DOC_PAN_RMKS", SqlDbType.VarChar);
            param[112].Value = main.PM_DOC_PAN_RMKS;

            param[113] = new SqlParameter("@SIGNAGEIMAGES", SqlDbType.Structured);
            param[113].Value = UtilityService.ConvertToDataTable(Imgclass);
            param[114] = new SqlParameter("@SIG_LENGTH", SqlDbType.VarChar);
            param[114].Value = main.PM_PPT_SIG_LENGTH;
            param[115] = new SqlParameter("@SIG_WIDTH", SqlDbType.VarChar);
            param[115].Value = main.PM_PPT_SIG_WIDTH;
            param[116] = new SqlParameter("@AC_OUTDOOR", SqlDbType.VarChar);
            param[116].Value = main.PM_PPT_AC_OUTDOOR;
            param[117] = new SqlParameter("@GSB", SqlDbType.VarChar);
            param[117].Value = main.PM_PPT_GSB;
            param[118] = new SqlParameter("@AC_OUTDOOR_IMAGE", SqlDbType.Structured);
            param[118].Value = UtilityService.ConvertToDataTable(Imgclass1);
            param[119] = new SqlParameter("@GSB_IMAGE", SqlDbType.Structured);
            param[119].Value = UtilityService.ConvertToDataTable(Imgclass2);
            param[120] = new SqlParameter("@INSPECTION_DATE", SqlDbType.DateTime);
            param[120].Value = main.PM_PPT_INSPECTED_DT;
            param[121] = new SqlParameter("@INSPECTION_BY", SqlDbType.VarChar);
            param[121].Value = main.PM_PPT_INSPECTED_BY;
            param[122] = new SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar);
            param[122].Value = main.PM_PPT_TOT_FLRS;
            param[123] = new SqlParameter("@RELOCATION", SqlDbType.VarChar);
            param[123].Value = main.PM_PPT_RELOC_NAME;
            param[124] = new SqlParameter("@RELOCADDRS", SqlDbType.VarChar);
            param[124].Value = main.PM_PPT_RELOC_ADDRESS;
            param[125] = new SqlParameter("@OWN_ADDRESS", SqlDbType.VarChar);
            param[125].Value = main.PM_OWN_ADDRESS;
            param[126] = new SqlParameter("@PHYCDTN_OTHRBLDG", SqlDbType.VarChar);
            param[126].Value = main.PM_PHYCDTN_OTHRBU;
            param[127] = new SqlParameter("@OTHR_POTABLE_WTR_RMKS", SqlDbType.VarChar);
            param[127].Value = main.PM_OTHR_POTABLE_WTR_REMARKS;
            param[128] = new SqlParameter("@OTHR_IT_INSTALL", SqlDbType.VarChar);
            param[128].Value = main.PM_OTHR_IT_INSTALL;
            param[129] = new SqlParameter("@OTHR_IT_INSTALL_RMKS", SqlDbType.VarChar);
            param[129].Value = main.PM_OTHR_IT_INSTALL_REMARKS;
            param[130] = new SqlParameter("@OTHR_DG_INSTALL", SqlDbType.VarChar);
            param[130].Value = main.PM_OTHR_DG_INSTALL;
            param[131] = new SqlParameter("@OTHR_DG_INSTALL_RMKS", SqlDbType.VarChar);
            param[131].Value = main.PM_OTHR_DG_INSTALL_RMKS;
            param[125] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            param[125].Value = "4001";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            string Result;
            return new { Result = ds.Tables[0].Rows[0]["Result"].ToString() };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }




    #endregion

    public object Insert_ZF_CheckList(SelectedDataModel objd)
    {
        //Get Company name from Hd module 
        HDModel mn = new HDModel();
        mn.CompanyId = objd.Company;
        string DB = _ReturnDB(mn);
        ErrorHandler err1 = new ErrorHandler();
        //err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));

        String proc_name = DB + "." + "ZONAL_INSERT_CHECKLIST";

        String proc_name1 = DB + "." + "ZONAL_INSERT_CHECK_DETAILS";
        String proc_name2 = DB + "." + "[SEND_ZONAL_INCEPTION]";

        int sem_id = 0;
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[0].Value = objd.BCL_ID;
        param[1] = new SqlParameter("@SaveFlag", SqlDbType.VarChar);
        param[1].Value = objd.SaveFlag;
        param[2] = new SqlParameter("@Datetime", SqlDbType.DateTime);
        param[2].Value = objd.InspectdDate;
        param[3] = new SqlParameter("@user", SqlDbType.NVarChar);
        param[3].Value = objd.CreatedBy;
        param[4] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
        param[4].Value = "APP";

        object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name, param);
        sem_id = (int)o;
        if (sem_id > 0)
        {
            for (int i = 0; i < objd.Seldata.Count; i++)
            {
                SqlParameter[] param1 = new SqlParameter[7];
                param1[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
                param1[0].Value = objd.BCL_ID;

                param1[1] = new SqlParameter("@CatCode", SqlDbType.VarChar);
                param1[1].Value = objd.Seldata[i].CatCode;
                param1[2] = new SqlParameter("@SubcatCode", SqlDbType.VarChar);
                param1[2].Value = objd.Seldata[i].SubcatCode;
                param1[3] = new SqlParameter("@WorkCondition", SqlDbType.VarChar);
                param1[3].Value = objd.Seldata[i].ScoreName;
                param1[4] = new SqlParameter("@Comments", SqlDbType.VarChar);
                param1[4].Value = objd.Seldata[i].txtdata;
                param1[5] = new SqlParameter("@USER", SqlDbType.VarChar);
                param1[5].Value = objd.Seldata[i].row;
                param1[6] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
                param1[6].Value = "APP";

                object ChkDetails = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name1, param1);
            }
        }

        SqlParameter[] param2 = new SqlParameter[1];
        param2[0] = new SqlParameter("@ID", SqlDbType.Int);
        param2[0].Value = objd.BCL_ID;
        object ChkDetailss = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name2, param2);

        string msg;
        if (sem_id == 1)
            msg = "Record Submitted Successfully.";
        else
            msg = "Something Went Wrong";
        return new { Message = msg };

    }





    #region Central Team

    //Get Central Team Locations
    public Object GetCentralTeamLocations(ZonalFmLocations zonalFmLocations)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = zonalFmLocations.companyId;
            string DB = _ReturnDB(hDModel);

            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_ID_BY_LOCATION_WISE_Central");
            sp.Command.Parameters.Add("@AUR_ID", zonalFmLocations.userId, DbType.String);
            sp.Command.Parameters.Add("@ID", zonalFmLocations.submittedCode, DbType.Int32);
            ds = sp.GetDataSet();

            if (ds.Tables.Count != 0)
                return new { Message = "OK", data = ds.Tables[0] };
            //return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = "Not OK", data = (object)null };

        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    //Get Central Team Details 
    public object GetCentralTeamDetails(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        int BCL_ID = Int32.Parse(hDModel.Password);
        sp = new SubSonic.StoredProcedure(DB + "." + "Central_Get_Details_On_Id");
        sp.Command.Parameters.Add("@BCL_ID", BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = "OK", data = ds.Tables[0] };
        //return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = "Not OK", data = (object)null };
    }

    //Get CentralTeam Active IDs
    public object GetCentralTeamIds(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        List<Zonal_ID> Zon_lst = new List<Zonal_ID>();
        Zonal_ID id;
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_CNTRL_ID");
        sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
        sp.Command.Parameters.Add("@loc", hDModel.Password, DbType.String);

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                id = new Zonal_ID();
                id.id = sdr["BCL_ID"].ToString() + "/" + sdr["ST"].ToString();
                id.BCL_ID = sdr["BCL_ID"].ToString();
                id.ticked = false;
                Zon_lst.Add(id);
            }
        }
        if (Zon_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Zon_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }




    #endregion


    public List<string> UploaImages(HttpRequest httpRequest)
    {
        var filePaths = new List<string>();

        for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
        {
            var req = httpRequest.Files[i];
            var fn = req.FileName;

            var filePath = Path.Combine(HttpRuntime.AppDomainAppPath, "UploadFiles/" + "MaxLife_Demo/" + (DateTime.Now.ToString("ddMMyyyy").ToString()) + fn);
            req.SaveAs(filePath);
            filePaths.Add(filePath);

        }
        return filePaths;
    }

    //Property Proposel RequistID generation
    public string GenerateRequestId(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "GENERATE_REQUESTID_Mobile");
            sp.Command.Parameters.Add("@REQ_TYPE", hDModel.code, DbType.String);
            var id = sp.ExecuteScalar();
            if (id != null)
                return DateTime.Now.ToString("MMddyyyy") + "/PRPREQ/" + id.ToString();
            else
                return "No Id";
        }
        catch (Exception ex)
        {

            return "No Id";
        }
    }


    //Upload multipul images and files dinamically --Raghav --20-02-2021
    public List<string> UploadFiles(HttpRequest httpRequest)
    {
        try
        {
            HDModel hDModel = new HDModel
            {
                CompanyId = httpRequest.Params[0]
            };
            string companyId = _ReturnDB(hDModel);

            List<string> fileNames = new List<string>();
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {

                var path = HttpRuntime.AppDomainAppPath;
                var req = httpRequest.Files[i];
                var fn = req.FileName;
                string fileName = (DateTime.Now.ToString("ddMMyyyyhhmmss").ToString()) + fn; ;
                string filePath;
                if (companyId == "[MAXLIFE].dbo")
                {

                    filePath = "C:\\WebAppUAT\\Maxlife\\UploadFiles\\ChecklistFiles\\" + fileName;

                }
                else
                {
                    filePath = path + "UploadFiles\\" + companyId + "\\" + fileName;
                }

                req.SaveAs(filePath);

                fileNames.Add(fileName);
            }
            return fileNames;
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }



    //Property Proposal get location
    public object GetLocationBasedOnCityCode(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_LOCATION_CITY");
            sp.Command.Parameters.Add("@CITY", hDModel.code, DbType.String);
            sp.Command.Parameters.Add("@USR_ID", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables[0].Rows.Count == 0)
            {
                return new { Message = "Not OK" };
            }
            else
            {
                return new { Message = "OK", Data = ds.Tables[0] };

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Property proposal get city
    public object GetCityID(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_ACTCTY");
            sp.Command.Parameters.Add("@DUMMY", "", DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables[0].Rows.Count == 0)
            {
                return new { Message = "Not OK" };
            }
            else
            {
                return new { Message = "OK", Data = ds.Tables[0] };

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //property proposal get tower
    public object GetTowerID(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_TOWERSBYLCMID");
            sp.Command.Parameters.Add("@LCMID", hDModel.code, DbType.String);
            sp.Command.Parameters.Add("@USER_ID", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables[0].Rows.Count == 0)
            {
                return new { Message = "Not OK" };
            }
            else
            {
                return new { Message = "OK", Data = ds.Tables[0] };

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //property proposal getentities
    public object GetEntities(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "PM_GET_ENTITY_TYPES");
            ds = sp.GetDataSet();

            if (ds.Tables[0].Rows.Count == 0)
            {
                return new { Message = "Not OK" };
            }
            else
            {
                return new { Message = "OK", Data = ds.Tables[0] };

            }
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }

    //Branch Performance Module Insert Central Team.
    public object InsertCentralTeam(SelectedDataModel objd)
    {
        //Get Company name from Hd module 
        HDModel mn = new HDModel();
        mn.CompanyId = objd.Company;
        string DB = _ReturnDB(mn);
        ErrorHandler err1 = new ErrorHandler();
        //err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));

        String proc_name = DB + "." + "CENTRAL_INSERT_CHECKLIST";

        String proc_name1 = DB + "." + "CENTRAL_INSERT_CHECK_DETAILS";
        String proc_name2 = DB + "." + "SEND_CentralTeam_INCEPTION";

        int sem_id = 0;
        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param[0].Value = objd.BCL_ID;
        param[1] = new SqlParameter("@SaveFlag", SqlDbType.VarChar);
        param[1].Value = objd.SaveFlag;
        param[2] = new SqlParameter("@Datetime", SqlDbType.DateTime);
        param[2].Value = objd.InspectdDate;
        param[3] = new SqlParameter("@user", SqlDbType.NVarChar);
        param[3].Value = objd.CreatedBy;

        object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name, param);
        sem_id = (int)o;
        if (sem_id > 0)
        {
            for (int i = 0; i < objd.Seldata.Count; i++)
            {
                SqlParameter[] param1 = new SqlParameter[6];
                param1[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
                param1[0].Value = objd.BCL_ID;

                param1[1] = new SqlParameter("@CatCode", SqlDbType.VarChar);
                param1[1].Value = objd.Seldata[i].CatCode;
                param1[2] = new SqlParameter("@SubcatCode", SqlDbType.VarChar);
                param1[2].Value = objd.Seldata[i].SubcatCode;
                param1[3] = new SqlParameter("@WorkCondition", SqlDbType.VarChar);
                param1[3].Value = objd.Seldata[i].ScoreName;
                param1[4] = new SqlParameter("@Comments", SqlDbType.VarChar);
                param1[4].Value = objd.Seldata[i].txtdata;
                param1[5] = new SqlParameter("@USER", SqlDbType.VarChar);
                param1[5].Value = objd.Seldata[i].row;

                object ChkDetails = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name1, param1);
            }
        }

        SqlParameter[] param2 = new SqlParameter[1];
        param2[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
        param2[0].Value = objd.BCL_ID;
        object ChkDetailsS = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name2, param2);

        string msg;
        if (sem_id == 1)
            msg = "Record Submitted Successfully.";
        else
            msg = "Something Went Wrong";
        return new { Message = msg };
    }

    //Get SecurityGuard Inspection categories
    public object GetSecurityGuardForm(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            String proc_name1 = DB + "." + "DCLC_GET_INSPECTORS";
            String proc_name2 = DB + "." + "DCLC_GET_LOCATIONS";

            SqlParameter[] param2 = new SqlParameter[1];
            param2[0] = new SqlParameter("@USER", SqlDbType.VarChar);
            param2[0].Value = hDModel.UserId;

            sp = new SubSonic.StoredProcedure(DB + "." + "DCL_GET_NEW_CHECKLIST_ID");
            sp.Command.Parameters.Add("@CREATED_BY", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();

            object Inspectors = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name1, param2);
            object Location = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, proc_name2, param2);

            if (Inspectors != null && Location != null)
            {
                return new { checkListId = ds.Tables[0], inspectors = Inspectors.ToString(), locatio = Location.ToString(), message = "OK" };
            }
            else
            {
                return new { message = "Something went wrong" };
            }
        }
        catch (Exception ex)
        {
            return new { message = "Something went wrong" };
        }
    }

    //Branch Module Dashboard
    public object GetDashBoard(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);

            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_INSPECTIONS_LIST_COUNT");
            sp.Command.Parameters.Add("@usrid", hDModel.UserId, DbType.String);
            sp.Command.Parameters.Add("@loccode", hDModel.code, DbType.String);
            sp.Command.Parameters.Add("@date", hDModel.Role_id, DbType.String);
            ds = sp.GetDataSet();

            return new { data = ds.Tables[0], locationData = ds.Tables[1], Dates = ds.Tables[2] };

        }
        catch (Exception ex)
        {
            return new { message = "Something went wrong" };
        }
    }

    //View id's for zonal and central Team
    public object GetViewIds(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);

            sp = new SubSonic.StoredProcedure(DB + "." + "GET_BCL_ID_VIEW");
            sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
            sp.Command.Parameters.Add("@loc", hDModel.code, DbType.String);
            sp.Command.Parameters.Add("@submit_code", hDModel.Password, DbType.Int32);
            sp.Command.Parameters.Add("@Score", hDModel.Role_id, DbType.Int32);
            ds = sp.GetDataSet();

            return new { data = ds.Tables[0] };

        }
        catch (Exception ex)
        {
            return new { message = "Something went wrong" };
        }
    }

    //View Details for zonal and central Team
    public object GetViewDetails(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);

            sp = new SubSonic.StoredProcedure(DB + "." + "Zonal_Get_View_Details_On_Id");
            sp.Command.Parameters.Add("@BCL_ID", hDModel.Role_id, DbType.String);
            sp.Command.Parameters.Add("@SCORECODE", hDModel.code, DbType.String);
            ds = sp.GetDataSet();

            return new { data = ds.Tables[0] };

        }
        catch (Exception ex)
        {
            return new { message = "Something went wrong" };
        }
    }


    //Get Locations and Count for view.
    public object GetInspectionLocations(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);

            sp = new SubSonic.StoredProcedure(DB + "." + "Get_Inspection_Locations_Count");
            sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
            sp.Command.Parameters.Add("@ID", hDModel.code, DbType.String);
            ds = sp.GetDataSet();

            return new { data = ds.Tables[0] };

        }
        catch (Exception ex)
        {
            return new { message = "Something went wrong" };
        }
    }

    //search space for shedule seat booking.
    public object SearchSpaces(ScheduleMySeatBookingsModelMobile Data)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = Data.companyId;
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_SPACE_DETAILS_BY_DATE");
            sp.Command.Parameters.Add("@From_Date", Data.FROM_DATE, DbType.Date);
            sp.Command.Parameters.Add("@To_Date", Data.TO_DATE, DbType.Date);
            sp.Command.Parameters.Add("@FLR_ID", Data.flrlst, DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", Data.userid, DbType.String);
            ds = sp.GetDataSet();
            return ds.Tables;

        }
        catch (Exception e)
        {
            throw;
        }
    }


    //allocate seats for space booking
    public int Allocate_Space_Seats(SPACE_ALLOC_DETAILS_TIME_Mobile allocDetLst)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = allocDetLst.CompanyID,
            UserId = allocDetLst.UserId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();

        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = allocDetLst.Data;


        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AURID", SqlDbType.NVarChar);
        param[1].Value = allocDetLst.UserId;
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.Int);
        param[2].Value = 1;

        List<SPACE_ALLOC_DETAILS_TIME> spcdet = new List<SPACE_ALLOC_DETAILS_TIME>();
        SPACE_ALLOC_DETAILS_TIME spc;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, DB + ".SMS_SPACE_ALLOC_MAP11", param))
        {
            while (sdr.Read())
            {

            }
        }

        return 1;

    }

    // from utility services web
    public object getAurNames(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "Get_Employees");
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@COMPANYID", hDModel.CompanyId, DbType.String);
        ds = sp.GetDataSet();

        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    // from utility services web
    public object GetCities(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_CITY_ADM");
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@MODE", hDModel.code, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.CNY_CODE = sdr["CNY_CODE"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    // from utility services web
    public object GetLocationsData(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        List<Locationlst> Loc_lst = new List<Locationlst>();
        Locationlst Loc;
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_LOCATION_ADM");
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@MODE", hDModel.code, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new Locationlst();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    // from utility services web
    public object GetTowersData(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        List<Towerlst> Tow_lst = new List<Towerlst>();
        Towerlst Tow;
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_TOWER_ADM");
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@MODE", hDModel.code, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Tow = new Towerlst();
                Tow.TWR_CODE = sdr["TWR_CODE"].ToString();
                Tow.TWR_NAME = sdr["TWR_NAME"].ToString();
                Tow.LCM_CODE = sdr["LCM_CODE"].ToString();
                Tow.CTY_CODE = sdr["CTY_CODE"].ToString();
                Tow.CNY_CODE = sdr["CNY_CODE"].ToString();
                Tow.ticked = false;
                Tow_lst.Add(Tow);
            }
        }
        if (Tow_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Tow_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    // from utility services web
    public object GetTowerByLocation(LocationData Loc)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = Loc.CompanyId,
            UserId = Loc.UserId,
        };
        string DB = _ReturnDB(hDModel);

        List<Towerlst> towerlst = new List<Towerlst>();
        Towerlst twr;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        if (Loc == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(Loc.Locationlstts);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = hDModel.UserId;
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = 2;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, DB + ".GET_TOWER_BY_LOCATIONLST_ADM", param))
        {
            while (sdr.Read())
            {
                twr = new Towerlst();
                twr.TWR_CODE = sdr["TWR_CODE"].ToString();
                twr.TWR_NAME = sdr["TWR_NAME"].ToString();
                twr.LCM_CODE = sdr["LCM_CODE"].ToString();
                twr.CTY_CODE = sdr["CTY_CODE"].ToString();
                twr.CNY_CODE = sdr["CNY_CODE"].ToString();
                twr.ticked = false;
                towerlst.Add(twr);
            }
        }
        if (towerlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = towerlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    // from utility services web
    public static DataTable ConvertToDataTable<T>(List<T> data)
    {
        DataTable dataTable = new DataTable();
        PropertyDescriptorCollection propertyDescriptorCollection =
            TypeDescriptor.GetProperties(typeof(T));
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            Type type = propertyDescriptor.PropertyType;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);


            dataTable.Columns.Add(propertyDescriptor.Name, type);
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T iListItem in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }
    // from utility services web
    public object GetFloorByTower(TowerData tower)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = tower.CompanyID,
            UserId = tower.UserId,
        };
        string DB = _ReturnDB(hDModel);
        List<Floorlst> floorlst = new List<Floorlst>();
        Floorlst flr;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@TWRLST", SqlDbType.Structured);

        if (tower == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(tower.Towerlsts);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = hDModel.UserId;
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = 2;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, DB + ".GET_FLOOR_BY_TOWERLST_ADM", param))
        {
            while (sdr.Read())
            {
                flr = new Floorlst();
                flr.FLR_CODE = sdr["FLR_CODE"].ToString();
                flr.FLR_NAME = sdr["FLR_NAME"].ToString();
                flr.TWR_CODE = sdr["TWR_CODE"].ToString();
                flr.LCM_CODE = sdr["LCM_CODE"].ToString();
                flr.CTY_CODE = sdr["CTY_CODE"].ToString();
                flr.CNY_CODE = sdr["CNY_CODE"].ToString();
                flr.ticked = false;
                floorlst.Add(flr);
            }
        }
        if (floorlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = floorlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    // from utility services web
    public object GetCountries(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        List<Countrylst> cnylst = new List<Countrylst>();
        Countrylst cny;
        sp = new SubSonic.StoredProcedure(DB + "." + "Get_Countries_ADM");
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@MODE", hDModel.code, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cny = new Countrylst();
                cny.CNY_CODE = sdr["CNY_CODE"].ToString();
                cny.CNY_NAME = sdr["CNY_NAME"].ToString();
                cny.ticked = false;
                cnylst.Add(cny);
            }
        }
        if (cnylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = cnylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetInspectionView(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "View_Actions");
        sp.Command.AddParameter("@usr", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@loc", hDModel.code, DbType.String);
        sp.Command.AddParameter("@date", hDModel.Role_id, DbType.String);
        ds = sp.GetDataSet();

        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, InspectionList = ds.Tables[0], LocationList = ds.Tables[1], DateList = ds.Tables[2] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetHelpDesk(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_HDM_TICKTS");
        sp.Command.AddParameter("@AURID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();

        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, InspectionList = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    //UploadProfile
    //writtern for uploading profile picture from mobile
    public List<string> UploadProfile(HttpRequest httpRequest)
    {
        try
        {
            List<string> fileNames = new List<string>();
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {

                var path = HttpRuntime.AppDomainAppPath;
                var req = httpRequest.Files[i];

                var fn = req.FileName;
                var filePath = path + "userprofiles\\" + fn;
                req.SaveAs(filePath);

                fileNames.Add((filePath));

            }
            return fileNames;
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    //Written to Delete File uploded to the server 
    //written by raghav 15-05-2021
    public string DeleteFileFromPath(string fileName)
    {
        try
        {
            var serverPath = HttpRuntime.AppDomainAppPath;
            // Check if file exists with its full path    
            if (File.Exists(Path.Combine(serverPath, fileName)))
            {
                // If file found, delete it    
                File.Delete(Path.Combine(serverPath, fileName));
                return "File deleted.";
            }
            else return "File not found";
        }
        catch (IOException ioExp)
        {
            return "File not found";
            //Console.WriteLine(ioExp.Message);
        }
    }



    //Written by raghav to Update user profile for mobile.
    public object UpdateProfile(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "Update_Get_ProfileImage");
        sp.Command.AddParameter("@aur_id", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@path", hDModel.Password, DbType.String);
        sp.ExecuteScalar();

        return new { Message = "Profile Updated" };

    }

    //Written by raghav to get user profile for mobile.
    public object GetProfile(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GetProfile");
        sp.Command.AddParameter("@AURID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Profile = ds.Tables[0].Rows[0].ItemArray };
        else
            return new { Message = MessagesVM.UM_NO_REC, Profile = (object)null };

    }

    //Written by raghav to get maintanance modulle menu list (Ekart)
    public object GetMaintainanceMenu(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "Mobile_Menu");
        sp.Command.AddParameter("@User_id", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, MaintenanceMenu = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Profile = (object)null };
    }

    //Written by Raghav Get user location (Ekart maintanance module)
    public object GetUserLocation(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "BDMP_GET_ALL_LOCATIONS");
        sp.Command.AddParameter("@USER_ID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Locations = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Locations = (object)null };
    }

    //Written by Raghav Get Asset by location (Ekart maintanance module)
    public object GetAssetOnLocation(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "BDMP_CREATE_GET_ASSET_GROUP_BY_LOCATION");
        sp.Command.AddParameter("@BLDG", hDModel.code, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Asset = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Asset = (object)null };
    }

    //Written by Raghav Get Assert subcategory by location and main category (Ekart maintanance module)
    public object GetAssertSubcategoryByAssertCategory(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "BDMP_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP");
        sp.Command.AddParameter("@BLDG", hDModel.code, DbType.String);
        sp.Command.AddParameter("@GRPID", hDModel.Role_id, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, SubAsset = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, SubAsset = (object)null };
    }

    //Written by Raghav Get Assert Brand(Ekart maintanance module)
    public object GetAssetBrand(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "BDMP_GET_ASSETBRAND_LOCGRUPTYPE");
        sp.Command.AddParameter("@BLDG", hDModel.code, DbType.String);
        sp.Command.AddParameter("@GRPID", hDModel.Role_id, DbType.String);
        sp.Command.AddParameter("@GRPTYPID", hDModel.Password, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Brand = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Brand = (object)null };
    }

    //Written by Raghav Get assert vendor (Ekart maintanance module)

    public object GetAssertVendor(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "BDMP_GET_ASSET_VENDOR_BY_LOCGRUPBRND");
        sp.Command.AddParameter("@BLDG", hDModel.code, DbType.String);
        sp.Command.AddParameter("@GRPID", hDModel.Role_id, DbType.String);
        sp.Command.AddParameter("@GRPTYPID", hDModel.Password, DbType.String);
        sp.Command.AddParameter("@VEND_ID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Vendor = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Vendor = (object)null };
    }
    //Quick select modules,sub modules and Icons Reddy
    public object QUICKSELECTICONSMOBILE(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[QUICK_SELECT_ICONS_MOBILE]");
        sp.Command.AddParameter("@User_id", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@TMODULE", hDModel.code, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, MaintenanceMenu = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Profile = (object)null };
    }



    public object QUICKICONSSUBMODMOBILE(MaintenanceMenu MM)
    {
        HDModel hDModel = new HDModel();
        hDModel.CompanyId = MM.CompanyId;

        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[QUICK_ICONS_SUBMOD_MOBILE]");
        sp.Command.AddParameter("@User_id", MM.UserId, DbType.String);
        sp.Command.AddParameter("@TMODULE", MM.code, DbType.String);
        ds = sp.GetDataSet();

        // Assuming the first row in the DataTable contains the column names
        var columnNames = ds.Tables[0].Columns.Cast<DataColumn>().Select(col => col.ColumnName).ToList();

        var moduleMenuData = ds.Tables[0].AsEnumerable()
            .GroupBy(row => row.Field<string>(columnNames[0])) // Use the first column as the ModuleName
            .Select(group => new
            {
                ModuleName = group.Key,
                Menus = group.Select(menuRow => new
                {
                    Sub_Menu = menuRow.Field<string>(columnNames[1]), // Use the second column as the Sub_Menu
                    IMG_PATH = menuRow.Field<string>(columnNames[2]), // Use the third column as the IMG_PATH
                    CLS_SORT_ORDER = menuRow.Field<int>(columnNames[3]), // Use the fourth column as the CLS_SORT_ORDER
                    PAGE_NAME = menuRow.Field<string>(columnNames[4]) // Use the fifth column as the PAGE_NAME
                }).ToList()
            })
            .ToList();

        return new
        {
            Message = "Ok",
            MaintanaceMenu = moduleMenuData
        };

    }
    //Written by Raghav Get Ticket Ids (Ekart maintanance module)
    public object GetTicketIds(AssertTicketRequestModel assertTicketRequest)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = assertTicketRequest.CompanyId
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_BDMP_VENDORASSETS");
        sp.Command.AddParameter("@BLDG", assertTicketRequest.LocationCode, DbType.String);
        sp.Command.AddParameter("@GRPID", assertTicketRequest.CategoryId, DbType.String);
        sp.Command.AddParameter("@GRPTYPID", assertTicketRequest.SubcategoryId, DbType.String);
        sp.Command.AddParameter("@BRANDID", assertTicketRequest.BrandId, DbType.String);
        sp.Command.AddParameter("@VENDOR_ID", assertTicketRequest.VendorId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Tickets = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Tickets = (object)null };
    }
    //Written by Vamsi Get Ticket Type (Ekart maintanance module)
    public object GetTicketType(string companyid)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = companyid;
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            DataSet ds1 = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_BDMP_PRIORITY");
            ds = sp.GetDataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_BDMP_TICKET_TYPE");
            ds1 = sp.GetDataSet();
            if (ds.Tables.Count != 0 && ds1.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, Priority = ds.Tables[0], TicketType = ds1.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Priority = (object)null, TicketType = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    //Written by Vamsi Insert Ticket(Ekart maintanance module)
    //Updated by Raghav
    public object UpdatebreakDown(MaintanceTicketModel maintanceTicketModel)
    {
        try
        {
            NotificationService notification = new NotificationService();
            HDModel hDModel = new HDModel()
            {
                CompanyId = maintanceTicketModel.CompanyId
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".UPDATE_DETAILS_OF_BREAKDOWN";
            SqlParameter[] param = new SqlParameter[22];
            param[0] = new SqlParameter("@REQID", SqlDbType.VarChar);
            param[0].Value = maintanceTicketModel.RequesteId;
            param[1] = new SqlParameter("@PREVENTIVE_ACTION", SqlDbType.VarChar);
            param[1].Value = maintanceTicketModel.PreventiveAction;
            param[2] = new SqlParameter("@CORRECTIVE_ACTION", SqlDbType.VarChar);
            param[2].Value = maintanceTicketModel.CorrectiveAction;
            param[3] = new SqlParameter("@USER", SqlDbType.VarChar);
            param[3].Value = maintanceTicketModel.User;
            param[4] = new SqlParameter("@ATTACHMENT", SqlDbType.VarChar);
            param[4].Value = maintanceTicketModel.AttachmentPath;
            param[5] = new SqlParameter("@PRODUCTION_IMPCT", SqlDbType.VarChar);
            param[5].Value = maintanceTicketModel.ProductionImpact;
            param[6] = new SqlParameter("@PROBLEM_OWNER", SqlDbType.VarChar);
            param[6].Value = maintanceTicketModel.ProoblemOwner;
            param[7] = new SqlParameter("@ROOT_CAUSE", SqlDbType.VarChar);
            param[7].Value = maintanceTicketModel.RootCause;
            param[8] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            param[8].Value = maintanceTicketModel.Status;
            param[9] = new SqlParameter("@INCHARE_ID", SqlDbType.VarChar);
            param[9].Value = maintanceTicketModel.InchargeId;
            param[10] = new SqlParameter("@FROMDATE", SqlDbType.VarChar);
            param[10].Value = maintanceTicketModel.FromDate;
            param[11] = new SqlParameter("@TODATE", SqlDbType.VarChar);
            param[11].Value = maintanceTicketModel.ToDate;
            param[12] = new SqlParameter("@REMARKS", SqlDbType.VarChar);
            param[12].Value = maintanceTicketModel.Remarks;
            param[13] = new SqlParameter("@SPAREPARTDETAILS", SqlDbType.Structured);
            param[13].Value = UtilityService.ConvertToDataTable(maintanceTicketModel.spareParts);
            param[14] = new SqlParameter("@BDMP_DWNTIME_FACTOR", SqlDbType.VarChar);
            param[14].Value = maintanceTicketModel.DownTieFactor;
            param[15] = new SqlParameter("@BDMP_DOWNTIME", SqlDbType.VarChar);
            param[15].Value = maintanceTicketModel.DownTime;
            param[16] = new SqlParameter("@BDMP_BRACHED", SqlDbType.VarChar);
            param[16].Value = maintanceTicketModel.Breached;
            //@BDMP_AST_SUB_GROUP_ID
            param[17] = new SqlParameter("@BDMP_AST_SUB_GROUP_ID", SqlDbType.VarChar);
            param[17].Value = maintanceTicketModel.SubAsset;
            param[18] = new SqlParameter("@EQUIPMENT_CODE", SqlDbType.VarChar);
            param[18].Value = maintanceTicketModel.AssetCode;
            param[19] = new SqlParameter("@ERROR_DESCRIPTION_CODE", SqlDbType.VarChar);
            param[19].Value = maintanceTicketModel.ErrorCode;
            param[20] = new SqlParameter("@BDMP_OTHERPROB_OWNER", SqlDbType.VarChar);
            param[20].Value = maintanceTicketModel.OtherProblemOwner;
            param[21] = new SqlParameter("@OTHER_ERROR_DESCRIPTION", SqlDbType.VarChar);
            param[21].Value = maintanceTicketModel.OtherErrorDesc;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (data == "success")
            {
                if (maintanceTicketModel.Status == "7")
                {
                    notification.GetDeviceIdforIncharge(DB, maintanceTicketModel.InchargeId, "BreakDown Ticket Updated", "Breakdown Ticket  Assigned TO " + maintanceTicketModel.InchargeId + "\nTicket ID : " + maintanceTicketModel.RequesteId + "\nStatus : " + "In Progress");
                }
                else if (maintanceTicketModel.Status == "9")
                {
                    notification.GetDeviceIdforIncharge(DB, maintanceTicketModel.InchargeId, "BreakDown Ticket Updated", "Breakdown Ticket Assigned TO " + maintanceTicketModel.InchargeId + "\nTicket ID : " + maintanceTicketModel.RequesteId + "\nStatus : " + "Reviewed and Closed");
                }
                else if (maintanceTicketModel.Status == "46")
                {
                    notification.GetDeviceIdforIncharge(DB, maintanceTicketModel.InchargeId, "BreakDown Ticket Updated", "Breakdown Ticket Assigned TO " + maintanceTicketModel.InchargeId + "\nTicket ID : " + maintanceTicketModel.RequesteId + "\nStatus : " + "Submission for Review");
                }
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, result = ds.Tables[0].Rows[0]["Result"].ToString() };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }


    //Written by Raghav Insert BreakDown Ticket  (Ekart maintanance module)
    public object InsertBreakDownTicket(GenerateBreakDownTicket GenerateBreakDownTicket)
    {
        try
        {
            NotificationService notification = new NotificationService();

            HDModel hDModel = new HDModel()
            {
                CompanyId = GenerateBreakDownTicket.CompanyId,
            };

            string DB = _ReturnDB(hDModel);
            //DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "HDM_GENERATE_BREAKDOWN_TICKET");
            sp.Command.AddParameter("@TICKETTYPE", GenerateBreakDownTicket.TicketType, DbType.String);
            sp.Command.AddParameter("@SUBCAT", GenerateBreakDownTicket.SubCategoryCode, DbType.String);
            sp.Command.AddParameter("@ERRORCODEDESC", GenerateBreakDownTicket.ErrorDescription, DbType.String);
            sp.Command.AddParameter("@LOC", GenerateBreakDownTicket.Location, DbType.String);
            sp.Command.AddParameter("@COMMENTS", GenerateBreakDownTicket.Comments, DbType.String);
            sp.Command.AddParameter("@USER", GenerateBreakDownTicket.UserId, DbType.String);
            sp.Command.AddParameter("@IMPACT", GenerateBreakDownTicket.Impact, DbType.String);
            sp.Command.AddParameter("@URGENCY", GenerateBreakDownTicket.Urgency, DbType.String);
            sp.Command.AddParameter("@FROMDATE", GenerateBreakDownTicket.StartDate, DbType.String);
            sp.Command.AddParameter("@OtherDesc", GenerateBreakDownTicket.OtherErrorDescription, DbType.String);
            sp.Command.AddParameter("@SUBEQUIPMENT", GenerateBreakDownTicket.SubEquipment, DbType.String);
            var ds = sp.ExecuteScalar();
            var data = ds.ToString().Split(' ');
            if (data[0] == "success")
            {
                hDModel.code = GenerateBreakDownTicket.Location;
                hDModel.Password = "BreakDown Ticket";
                hDModel.Role_id = "New BreakDown has been occured.\n Ticket ID : " + data[1] + "\n Error Description : " + GenerateBreakDownTicket.ErrorDescriptionText;
                notification.getDeviceIds(hDModel);
                return new { Message = MessagesVM.UM_OK, result = data[0] };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    //gethdinchargeroleid
    public object GetHDIncharge(GenerateBreakDownTicket GenerateBreakDownTicket)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = GenerateBreakDownTicket.CompanyId,
            };

            string DB = _ReturnDB(hDModel);
            //DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[BREAKDOWN_BIND_INCHARGES]");
            sp.Command.AddParameter("@TICKET_TYPE", GenerateBreakDownTicket.TicketType, DbType.String);
            sp.Command.AddParameter("@EQUIPMENT", GenerateBreakDownTicket.SubCategoryCode, DbType.String);
            sp.Command.AddParameter("@ERRORDESCRIPTION", GenerateBreakDownTicket.ErrorDescription, DbType.String);
            sp.Command.AddParameter("@LOC", GenerateBreakDownTicket.Location, DbType.String);
            var ds = sp.GetDataSet();

            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    //Written by Raghav Get Error Description  (Ekart maintanance module)
    public object GetBreakDownErrorDescription(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "HDM_GET_ERROR_DESCRIPTION");
        sp.Command.AddParameter("@CATEGORY", hDModel.code, DbType.String);
        sp.Command.AddParameter("@TICKETTYPE", hDModel.Role_id, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, ErrorDescriptionList = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, ErrorDescriptionList = (object)null };
    }

    //Written by Vamsi Break Down Status (Ekart maintanance module)

    public object BreakDownStatus(BreakDownStatusModel breakdownStatusModel)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = breakdownStatusModel.CompanyId
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "VIEW_BDMP_STATUS");
            sp.Command.AddParameter("@aur_id", breakdownStatusModel.UserId, DbType.String);
            sp.Command.AddParameter("@loc", breakdownStatusModel.Location, DbType.String);
            sp.Command.AddParameter("@type", breakdownStatusModel.SubCategory, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, BreakDownStatus = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, BreakDownStatus = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public object GetSpareParts(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[GET_SPAREPARTS_DETAILS]");
        sp.Command.AddParameter("@loc", hDModel.code, DbType.String);
        sp.Command.AddParameter("@Equipment", hDModel.Role_id, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, SpareParts = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, SpareParts = (object)null };
    }

    public object GetBreakDownData(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[GET_DETAILS_OF_BREAKDOWN]");
        sp.Command.AddParameter("@REQID", hDModel.code, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, BreakDownList = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, BreakDownList = (object)null };
    }

    public object GetAsset(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[BreakDownAssetList]");
        //sp.Command.AddParameter("@REQID", hDModel.code, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, AssetList = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, AssetList = (object)null };
    }


    public object Get_Maintainance_Id_Status(GetMaintainanceidStatusModel getmaintainanceidstatusModel)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = getmaintainanceidstatusModel.CompanyId
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_PPM_PLANDATA");
            sp.Command.AddParameter("@AUR_ID", getmaintainanceidstatusModel.UserId, DbType.String);
            sp.Command.AddParameter("@LOC_CODE", getmaintainanceidstatusModel.LocationCode, DbType.String);
            sp.Command.AddParameter("@CATEGORY", getmaintainanceidstatusModel.Category, DbType.String);
            sp.Command.AddParameter("@PLANTYPE", getmaintainanceidstatusModel.PlanType, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, MaintainanceStatus = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, MaintainanceStatus = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public object Get_Checklists_Data_On_Asset_Id(GetChecklistsDataOnAssetIdModel getChecklistsDataOnAssetIdModel)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = getChecklistsDataOnAssetIdModel.CompanyId
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_CHECKLISTS_DATA_ON_ASSETID");
            sp.Command.AddParameter("@ASSETID", getChecklistsDataOnAssetIdModel.AssetsId, DbType.String);
            sp.Command.AddParameter("@AUR_ID", getChecklistsDataOnAssetIdModel.UserId, DbType.String);
            sp.Command.AddParameter("@PVD_ID", getChecklistsDataOnAssetIdModel.PvdID, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
            {
                if (ds.Tables.Count > 1)
                {
                    return new { Message = MessagesVM.UM_OK, AssetId = ds.Tables[0], SpareParts = ds.Tables[1] };
                }
                else
                {
                    return new { Message = MessagesVM.UM_OK, AssetId = ds.Tables[0], SpareParts = (object)null };
                }

            }
            else
                return new { Message = MessagesVM.UM_NO_REC, AssetId = (object)null, SpareParts = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public object GetUrgency(string companyid)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = companyid;
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();

            sp = new SubSonic.StoredProcedure(DB + "." + "HDM_BIND_URGENCY");
            ds = sp.GetDataSet();

            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, Priority = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Priority = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }


    public object Get_User_Status_By_Role(HDModel hDModel)
    {

        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_USER_STATUS_BYROLE");
            sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, Status = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Status = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public object Get_User_Status_By_ID(HDModel hDModel)
    {

        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[GET_USER_STATUS_BY_ID]");
            sp.Command.AddParameter("@ID", Int32.Parse(hDModel.code), DbType.Int16);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, Status = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Status = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }


    public object Get_Break_Down_Tickets_To_ValiDate(HDModel hDModel)
    {

        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_BREAKDOWNTICKETS_TO_VALIDATE");
            sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, ValiDate = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, ValiDate = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public object GetOwnerList(string companyid)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = companyid;
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();

            sp = new SubSonic.StoredProcedure(DB + "." + "GET_HDM_OWNER");
            ds = sp.GetDataSet();

            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, OwnerList = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, OwnerList = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }
    public object Get_BreakDownTickets_By_Search(GetSearchTicketModel getsearchticketmodel)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = getsearchticketmodel.CompanyId
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_BREAKDOWNTICKETS_BY_SEARCH");
            sp.Command.AddParameter("@AUR_ID", getsearchticketmodel.UserId, DbType.String);
            sp.Command.AddParameter("@Screen", getsearchticketmodel.Screen, DbType.String);

            //sp.Command.AddParameter("@LCMCODE", getsearchticketmodel.LocationCode, DbType.String);

            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, ValiDate = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, ValiDate = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }

    public object Attach_Tickets_List(HDModel hDModel)
    {

        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "ATTACHTICKETSLIST");
            sp.Command.AddParameter("@LCMCODE", hDModel.code, DbType.String);


            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, TicketList = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, TicketList = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }

    public object GetCurrentInventory(HDModel hDModel)
    {

        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_ADDSPARES");
            sp.Command.AddParameter("@USERID", hDModel.code, DbType.String);


            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, Inventory = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Inventory = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }
    //Get sparepart details for validate spare (Ekart)
    public object GetValidatespare(HDModel hDModel)
    {

        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[VIEW_SPAREPARTS_STATUS]");
            sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, Tickets = ds.Tables[0], SparData = ds.Tables[1] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Tickets = (object)null, SparData = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }

    }
    public object InsetSpareParts(InsertAddSpareModel insertAddSpareModel)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = insertAddSpareModel.CompanyId
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".INS_UPD_DET_ADDSPARES";
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@ASP_REQ_LOC", SqlDbType.VarChar);
            param[0].Value = insertAddSpareModel.RequestLocation;
            param[1] = new SqlParameter("@ASP_REMARKS", SqlDbType.VarChar);
            param[1].Value = insertAddSpareModel.Remarks;
            param[2] = new SqlParameter("@ASP_AUR_ID", SqlDbType.VarChar);
            param[2].Value = insertAddSpareModel.UserId;
            param[3] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
            param[3].Value = "1";
            param[4] = new SqlParameter("@FILENAME", SqlDbType.VarChar);
            param[4].Value = insertAddSpareModel.FileName;
            param[5] = new SqlParameter("@ASP_MAINT_ID", SqlDbType.VarChar);
            param[5].Value = insertAddSpareModel.MAINT_ID;
            param[6] = new SqlParameter("@COST_OF_INVENTORY", SqlDbType.VarChar);
            param[6].Value = insertAddSpareModel.CostOfInventory;
            param[7] = new SqlParameter("@TYPE", SqlDbType.VarChar);
            param[7].Value = insertAddSpareModel.Type;
            param[8] = new SqlParameter("@SPAREPARTDETAILS", SqlDbType.Structured);
            param[8].Value = UtilityService.ConvertToDataTable(insertAddSpareModel.spareParts);

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            //var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (ds.Tables[0] != null)
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
            else
                return new { Message = MessagesVM.UM_NO_REC, result = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }


    //[UPDATE_SPAREPARTS_STATUS]
    public object ValidateSpare(ValidateSpareRequestModel validateSpareRequestModel)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = validateSpareRequestModel.CompanyID
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".UPDATE_SPAREPARTS_STATUS";
            SqlParameter[] param = new SqlParameter[4];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[0].Value = validateSpareRequestModel.UserID;
            param[1] = new SqlParameter("@REMARKS", SqlDbType.VarChar);
            param[1].Value = validateSpareRequestModel.Remarks;
            param[2] = new SqlParameter("@TYPE", SqlDbType.VarChar);
            param[2].Value = validateSpareRequestModel.Type;
            param[3] = new SqlParameter("@REQ_ID", SqlDbType.VarChar);
            param[3].Value = validateSpareRequestModel.RequestID;

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            //var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (ds.Tables[0] != null)
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
            else
                return new { Message = MessagesVM.UM_NO_REC, result = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }

    }

    //vinod sparedetails
    public object GetSparePart_Details(HDModel hDModel)
    {

        try
        {

            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".[GET_SPAREPARTDETAILS]";
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SPAREPART_CODE", SqlDbType.VarChar);
            param[0].Value = hDModel.code;

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            //var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (ds.Tables[0] != null)
            {
                return new { Message = MessagesVM.UM_OK, SpareData = ds.Tables[0], AttachedTickets = ds.Tables[1] };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, SpareData = (object)null, AttachedTickets = (object)null };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }

    }


    public object GetMaintainanceMenuTemp(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[Mobile_Menu_Temp]");
        sp.Command.AddParameter("@User_id", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@ModuleName", hDModel.code, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, MaintenanceMenu = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Profile = (object)null };
    }

    //Business Card Vamsi

    public object BusinessCardInsertDetails(BusinessCardInsertModel businessCardInsertModel)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = businessCardInsertModel.CompanyID
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".[BC_INSERT_DETAILS_Mobile]";
            SqlParameter[] param = new SqlParameter[13];
            param[0] = new SqlParameter("@DEPARTMENT", SqlDbType.VarChar);
            param[0].Value = businessCardInsertModel.Department;
            param[1] = new SqlParameter("@EMPLOYEE", SqlDbType.VarChar);
            param[1].Value = businessCardInsertModel.UserID;
            param[2] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            param[2].Value = businessCardInsertModel.Location;
            param[3] = new SqlParameter("@DESIGNATION", SqlDbType.VarChar);
            param[3].Value = businessCardInsertModel.Designation;
            param[4] = new SqlParameter("@EMAIL", SqlDbType.VarChar);
            param[4].Value = businessCardInsertModel.Mails;
            param[5] = new SqlParameter("@FAX", SqlDbType.VarChar);
            param[5].Value = businessCardInsertModel.Fax;
            param[6] = new SqlParameter("@TELEPHONE", SqlDbType.VarChar);
            param[6].Value = businessCardInsertModel.TelePhone;
            param[7] = new SqlParameter("@MOBILE", SqlDbType.VarChar);
            param[7].Value = businessCardInsertModel.Mobile;
            param[8] = new SqlParameter("@ADDRESS", SqlDbType.VarChar);
            param[8].Value = businessCardInsertModel.Address;
            param[9] = new SqlParameter("@CARDS", SqlDbType.Int);
            param[9].Value = businessCardInsertModel.TotalCards;
            param[10] = new SqlParameter("@CREATEDBY", SqlDbType.VarChar);
            param[10].Value = businessCardInsertModel.CreatedBy;
            param[11] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[11].Value = "1";
            param[12] = new SqlParameter("@REMARKS", SqlDbType.VarChar);
            param[12].Value = businessCardInsertModel.Remarks;


            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            //var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (ds.Tables[0] != null)
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
            else
                return new { Message = MessagesVM.UM_NO_REC, result = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }

    public object GetBusinessCardDetails(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[BC_GET_BUSINESS_CARD_DETAILS]");
        sp.Command.AddParameter("@AURID", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@COMPANYID", 1, DbType.Int64);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, BusinessCardDetails = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, BusinessCardDetails = (object)null };
    }

    public object ModifyBusinessCard(BusinessCardInsertModel businessCardInsertModel)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = businessCardInsertModel.CompanyID
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".[BC_MODIFY_BUSINESS_CARD_Mobile]";
            SqlParameter[] param = new SqlParameter[14];
            param[0] = new SqlParameter("@DEPARTMENT", SqlDbType.VarChar);
            param[0].Value = businessCardInsertModel.Department;
            param[1] = new SqlParameter("@EMPLOYEE", SqlDbType.VarChar);
            param[1].Value = businessCardInsertModel.UserID;
            param[2] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
            param[2].Value = businessCardInsertModel.Location;
            param[3] = new SqlParameter("@DESIGNATION", SqlDbType.VarChar);
            param[3].Value = businessCardInsertModel.Designation;
            param[4] = new SqlParameter("@EMAIL", SqlDbType.VarChar);
            param[4].Value = businessCardInsertModel.Mails;
            param[5] = new SqlParameter("@FAX", SqlDbType.VarChar);
            param[5].Value = businessCardInsertModel.Fax;
            param[6] = new SqlParameter("@TELEPHONE", SqlDbType.VarChar);
            param[6].Value = businessCardInsertModel.TelePhone;
            param[7] = new SqlParameter("@MOBILE", SqlDbType.VarChar);
            param[7].Value = businessCardInsertModel.Mobile;
            param[8] = new SqlParameter("@ADDRESS", SqlDbType.VarChar);
            param[8].Value = businessCardInsertModel.Address;
            param[9] = new SqlParameter("@CARDS", SqlDbType.Int);
            param[9].Value = businessCardInsertModel.TotalCards;
            param[10] = new SqlParameter("@UPDATEDBY", SqlDbType.VarChar);
            param[10].Value = businessCardInsertModel.CreatedBy;
            param[11] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[11].Value = "1";
            param[12] = new SqlParameter("@REMARKS", SqlDbType.VarChar);
            param[12].Value = businessCardInsertModel.Remarks;
            param[13] = new SqlParameter("@BID", SqlDbType.VarChar);
            param[13].Value = businessCardInsertModel.Id;


            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            //var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (ds.Tables[0] != null)
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
            else
                return new { Message = MessagesVM.UM_NO_REC, result = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }



    public object GetBusinessCardStatus(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[BUSINESS_STATUS]");

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, BusinessCardStatus = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, BusinessCardStatus = (object)null };
    }


    public object RequisitionApproval(BusinessCardInsertModel businessCardInsertModel)
    {


        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = businessCardInsertModel.CompanyID
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".[BC_REQUISITION_APPROVAL_Mobile]";
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@BVEN", SqlDbType.VarChar);
            param[0].Value = businessCardInsertModel.Vendor;
            param[1] = new SqlParameter("@COMPANYID", SqlDbType.Int);
            param[1].Value = "1";
            param[2] = new SqlParameter("@BREM", SqlDbType.VarChar);
            param[2].Value = businessCardInsertModel.Remarks;
            param[3] = new SqlParameter("@BREQ", SqlDbType.VarChar);
            param[3].Value = businessCardInsertModel.Id;
            param[4] = new SqlParameter("@BSTA", SqlDbType.VarChar);
            param[4].Value = businessCardInsertModel.Status;
            param[5] = new SqlParameter("@BAPP", SqlDbType.VarChar);
            param[5].Value = businessCardInsertModel.Approved;

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            //var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (ds.Tables[0] != null)
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
            else
                return new { Message = MessagesVM.UM_NO_REC, result = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }

    public object InsertPPMChecklistData(InsertPPMChecklistDataModel insertPPMChecklistDataModel)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = insertPPMChecklistDataModel.CompanyId
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".[BCL_CHECKLIST_PPM_DATA]";
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@Loccode", SqlDbType.VarChar);
            param[0].Value = insertPPMChecklistDataModel.LocationCode;
            param[1] = new SqlParameter("@InspectdBy", SqlDbType.VarChar);
            param[1].Value = insertPPMChecklistDataModel.Inspectd;
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[2].Value = insertPPMChecklistDataModel.UserId;
            param[3] = new SqlParameter("@COMPANY", SqlDbType.VarChar);
            param[3].Value = "1";
            param[4] = new SqlParameter("@date", SqlDbType.VarChar);
            param[4].Value = insertPPMChecklistDataModel.Date;
            param[5] = new SqlParameter("@REQ", SqlDbType.VarChar);
            param[5].Value = insertPPMChecklistDataModel.Request;
            param[6] = new SqlParameter("@FLAG", SqlDbType.VarChar);
            param[6].Value = insertPPMChecklistDataModel.Key;
            param[7] = new SqlParameter("@ASSET_ID", SqlDbType.VarChar);
            param[7].Value = insertPPMChecklistDataModel.AssetId;
            param[8] = new SqlParameter("@OVER_CMTS", SqlDbType.VarChar);
            param[8].Value = insertPPMChecklistDataModel.OverCmts;
            param[9] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
            param[9].Value = UtilityService.ConvertToDataTable(insertPPMChecklistDataModel.spareParts);
            param[10] = new SqlParameter("@StartDate", SqlDbType.DateTime);
            param[10].Value = insertPPMChecklistDataModel.StartDate;
            param[11] = new SqlParameter("@EndDate", SqlDbType.DateTime);
            param[11].Value = insertPPMChecklistDataModel.EndDate;

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            //var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (ds.Tables[0] != null)
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
            else
                return new { Message = MessagesVM.UM_NO_REC, result = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }

    }



    public object GetAllVendors(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[USP_GETALLVENDORS]");

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, BusinessCardVendors = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, BusinessCardVendors = (object)null };
    }

    public object GetEmployeesData(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[GET_EMPLOYEES_DATA_BY_EMPID]");
        sp.Command.AddParameter("@EMPID", hDModel.UserId, DbType.String);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, EmployeeDetails = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, EmployeeDetails = (object)null };
    }

    public object GetAllData(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[GET_LOCATION_DESIGNATION_DEPARTMENT_ATTENDESS_EMPDATA]");


        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Location = ds.Tables[0], Designation = ds.Tables[1], Department = ds.Tables[2], Attendess = ds.Tables[3], EmpData = ds.Tables[4] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Location = (object)null, Designation = (object)null, Department = (object)null, Attendess = (object)null, EmpData = (object)null };
    }

    public object GetMainSubChidCategory(ChildCategory childCategory)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = childCategory.CompanyId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[API_HD_GET_MAIN_SUB_CHILD_CATEGORY]");
        sp.Command.AddParameter("@CHILD", childCategory.TypeName, DbType.String);
        sp.Command.AddParameter("@PageNo", childCategory.PageNo, DbType.Int64);
        sp.Command.AddParameter("@PageSize", childCategory.PageSize, DbType.Int64);
        sp.Command.AddParameter("@COMPANY", 1, DbType.Int64);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, ServiceCategory = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Profile = (object)null };
    }

    //[GET_PPM_PLANDATA_TO_VALIDATE]
    public object GetValidatePPM(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[GET_PPM_PLANDATA_TO_VALIDATE]");
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, MaintainanceStatus = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, MaintainanceStatus = new object() };
    }


    //Business Card 
    public object BusinessCardUserAcknowledge(Acknowledge acknowledge)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = acknowledge.CompanyId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[BC_USERACKNOWLEDGE_Mobile]");
        sp.Command.AddParameter("@BREQ", acknowledge.RequestId, DbType.String);
        sp.Command.AddParameter("@EMP_ACK", acknowledge.EmployeName, DbType.String);
        sp.Command.AddParameter("@COMPANYID", 1, DbType.Int64);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Acknowledge = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Acknowledge = (object)null };
    }


    public object BusinessCardRejection(Acknowledge acknowledge)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = acknowledge.CompanyId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[BC_REQUISITION_REJECTION_Mobile]");
        sp.Command.AddParameter("@BREQ", acknowledge.RequestId, DbType.String);
        sp.Command.AddParameter("@COMPANYID", 1, DbType.Int64);

        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, Rejection = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Rejection = (object)null };
    }



    public object BusinessCardDetails(Acknowledge acknowledge)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = acknowledge.CompanyId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "BC_BUSINESS_DETAILS_Mobile");
        sp.Command.AddParameter("@BID", acknowledge.RequestId, DbType.String);
        sp.Command.AddParameter("@COMPANYID", 1, DbType.Int16);

        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Acknowledge = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Acknowledge = (object)null };
    }

    public object GetBusinessCardApprove(HDModel hDModel)
    {

        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "BC_GET_CARD_DETAILS");
        sp.Command.AddParameter("@AURID", hDModel.UserId, DbType.String);
        sp.Command.AddParameter("@COMPANYID", 1, DbType.Int16);


        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }



    public object GetSpareDashboard(SpareDashboardModel spareDashboardModel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = spareDashboardModel.CompanyID,
        };


        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "SpareAvailability_Zone_Wise");
        sp.Command.AddParameter("@fromdate", spareDashboardModel.Fromedate, DbType.DateTime);
        sp.Command.AddParameter("@todate", spareDashboardModel.ToDate, DbType.DateTime);


        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, SpareDashboardData = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, SpareDashboardData = (object)null };
    }

    public object StatusWiseZone(SpareDashboardModel spareDashboardModel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = spareDashboardModel.CompanyID,
        };


        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "PPM_StatusWise_Zone");
        sp.Command.AddParameter("@fromdate", spareDashboardModel.Fromedate, DbType.DateTime);
        sp.Command.AddParameter("@todate", spareDashboardModel.ToDate, DbType.DateTime);


        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, PPMStatus = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, PPMStatus = (object)null };
    }


    public object Getcovidterm(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GetCovidTerms");
        sp.Command.AddParameter("@User_ID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }

    public object GetRCAdata(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[GET_RCA_DATA]");
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, RCATickets = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, RCATickets = (object)null };
    }

    public object GetCovidNorms(CovidModel covidModel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = covidModel.companyId
        };
        string DB = _ReturnDB(hDModel);
        String proc_name = DB + ".CovidNorms";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@Aur_id", SqlDbType.VarChar);
        param[0].Value = covidModel.UserId;
        param[1] = new SqlParameter("@COVID", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(covidModel.covidTerms);
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
        var data = ds.Tables[0].Rows[0]["Result"].ToString();

        if (data == "success")
            return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
        else
            return new { Message = MessagesVM.UM_Not_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };

    }


    public object InsertRCAquestionary(RCAquestionary rcaquestionary)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = rcaquestionary.companyId,
        };

        string DB = _ReturnDB(hDModel);
        String proc_name = DB + ".Insert_RCA_QUESTIONARY";
        SqlParameter[] param = new SqlParameter[12];
        param[0] = new SqlParameter("@RCA_CNY_CODE", SqlDbType.VarChar);
        param[0].Value = rcaquestionary.RCACNYCode;
        param[1] = new SqlParameter("@RCA_CTY_CODE", SqlDbType.VarChar);
        param[1].Value = rcaquestionary.RCACityCode;
        param[2] = new SqlParameter("@RCA_LOC_CODE", SqlDbType.VarChar);
        param[2].Value = rcaquestionary.RCALocationCode;
        param[3] = new SqlParameter("@RCA_SELECTED_DT", SqlDbType.VarChar);
        param[3].Value = rcaquestionary.RCADate;
        param[4] = new SqlParameter("@RCA_SELECTED_CNP", SqlDbType.Int);
        param[4].Value = rcaquestionary.RCACNP;
        param[5] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[5].Value = rcaquestionary.UserId;
        param[6] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
        param[6].Value = rcaquestionary.companyId;
        param[7] = new SqlParameter("@RCA_OVERALL_CMTS", SqlDbType.NVarChar);
        param[7].Value = rcaquestionary.RCAoverallComments;
        param[8] = new SqlParameter("@RCA_SUBMIT", SqlDbType.Int);
        param[8].Value = rcaquestionary.RCASubmit;
        param[9] = new SqlParameter("@breakdownid", SqlDbType.VarChar);
        param[9].Value = rcaquestionary.RCAbreakdownid;
        param[10] = new SqlParameter("@RCADSUBCATCODEList", SqlDbType.Structured);
        param[10].Value = UtilityService.ConvertToDataTable(rcaquestionary.rCASubCats);
        param[11] = new SqlParameter("@RCA_ID", SqlDbType.Int);
        param[11].Value = rcaquestionary.RCAId;
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
        var data = ds.Tables[0].Rows[0]["Result"].ToString();
        if (data == "success")
            return new { Message = MessagesVM.UM_OK, result = ds };
        else
            return new { Message = MessagesVM.UM_Not_OK, result = ds };
    }



    public object GetRCAQuestionaryData(RCAquestionary rcaquestionary)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = rcaquestionary.companyId
            };
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_RCA_QUESTIONARY_Data");
            sp.Command.AddParameter("@rca_id", rcaquestionary.RcaId, DbType.String);
            sp.Command.AddParameter("@BdId", rcaquestionary.BearkDownId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, RCATickets = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, RCATickets = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.UM_NO_REC, RCATickets = (object)null };
        }
    }


    public object UpdateRCAquestionary(RCAquestionary rcaquestionary)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = rcaquestionary.companyId,
        };

        string DB = _ReturnDB(hDModel);
        String proc_name = DB + ".Update_RCA_QUESTIONARY";
        SqlParameter[] param = new SqlParameter[5];
        param[0] = new SqlParameter("@RCA_ID", SqlDbType.Int);
        param[0].Value = rcaquestionary.RCAId;
        param[1] = new SqlParameter("@RCA_LOC_CODE", SqlDbType.VarChar);
        param[1].Value = rcaquestionary.RCALocationCode;
        param[2] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
        param[2].Value = rcaquestionary.UserId;
        param[3] = new SqlParameter("@FinalSubmit", SqlDbType.TinyInt);
        param[3].Value = rcaquestionary.RCASubmit;
        param[4] = new SqlParameter("@RCADSUBCATCODEList", SqlDbType.Structured);
        param[4].Value = UtilityService.ConvertToDataTable(rcaquestionary.rCASubCats);
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
        var data = ds.Tables[0].Rows[0]["Result"].ToString();
        if (data == "success")
            return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
        else
            return new { Message = MessagesVM.UM_Not_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
    }


    public object GetWFHterm(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GetWFHTerms");
        sp.Command.AddParameter("@User_ID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }


    public object GetWFHNorms(CovidModel covidModel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = covidModel.companyId
        };
        string DB = _ReturnDB(hDModel);
        String proc_name = DB + ".WFHNorms";
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@Aur_id", SqlDbType.VarChar);
        param[0].Value = covidModel.UserId;
        param[1] = new SqlParameter("@COVID", SqlDbType.Structured);
        param[1].Value = UtilityService.ConvertToDataTable(covidModel.covidTerms);
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
        var data = ds.Tables[0].Rows[0]["Result"].ToString();

        if (data == "success")
            return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
        else
            return new { Message = MessagesVM.UM_Not_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };

    }


    public object GetRCATicketTiming(TicketModel ticketModel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = ticketModel.companyId
        };
        string DB = _ReturnDB(hDModel);
        String proc_name = DB + ".Insert_Rca_Ticket_Timeings";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@Aur_id", SqlDbType.VarChar);
        param[0].Value = ticketModel.UserId;
        param[1] = new SqlParameter("@BreakDownId", SqlDbType.VarChar);
        param[1].Value = ticketModel.BreakdownId;
        param[2] = new SqlParameter("@Rca_Ticket_TimeList", SqlDbType.Structured);
        param[2].Value = UtilityService.ConvertToDataTable(ticketModel.ticketTimings);
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
        var data = ds.Tables[0].Rows[0]["Result"].ToString();

        if (data == "success")
            return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
        else
            return new { Message = MessagesVM.UM_Not_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };

    }



    // Bapco

    public object TempVisitorDetails(VisitorDetails visitorDetails)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = visitorDetails.companyId
        };
        string DB = _ReturnDB(hDModel);
        String proc_name = DB + ".Temp_Visitor_Details_Mobile";
        SqlParameter[] param = new SqlParameter[13];
        param[0] = new SqlParameter("@Request_Id", SqlDbType.NVarChar);
        param[0].Value = visitorDetails.RequestId;
        param[1] = new SqlParameter("@T_Visitor_GuestName", SqlDbType.NVarChar);
        param[1].Value = visitorDetails.VisitorName;
        param[2] = new SqlParameter("@T_Visitor_VisitorStartDate", SqlDbType.DateTime);
        param[2].Value = visitorDetails.VisitingDate;
        param[3] = new SqlParameter("@T_Visitor_VisitorDuration", SqlDbType.Int);
        param[3].Value = visitorDetails.VisitingTime;
        param[4] = new SqlParameter("@T_Visitor_VisitorRelation", SqlDbType.NVarChar);
        param[4].Value = visitorDetails.Relation;
        param[5] = new SqlParameter("@T_Visitor_Remarks", SqlDbType.NVarChar);
        param[5].Value = visitorDetails.Remarks;
        param[6] = new SqlParameter("@T_Visitor_Status", SqlDbType.NVarChar);
        param[6].Value = visitorDetails.Status;
        param[7] = new SqlParameter("@Vehicle_RegistrationNumber", SqlDbType.NVarChar);
        param[7].Value = visitorDetails.VehicleRegNumber;
        param[8] = new SqlParameter("@VehicleMake", SqlDbType.NVarChar);
        param[8].Value = visitorDetails.VehicleMake;
        param[9] = new SqlParameter("@VehicleType", SqlDbType.NVarChar);
        param[9].Value = visitorDetails.VehicleType;
        param[10] = new SqlParameter("@Attachments", SqlDbType.NVarChar);
        param[10].Value = visitorDetails.OwnerAttachment;
        param[11] = new SqlParameter("@Vehicle_CPRAttachment", SqlDbType.NVarChar);
        param[11].Value = visitorDetails.CPRAttachment;
        param[12] = new SqlParameter("@Validity", SqlDbType.DateTime);
        param[12].Value = visitorDetails.validation;
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
        var data = ds.Tables[0].Rows[0]["Result"].ToString();

        if (data == "success")
            return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
        else
            return new { Message = MessagesVM.UM_Not_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
    }


    public object GetTempVisitorDetailsMobile(VisitorDetails visitorDetails)
    {

        HDModel hDModel = new HDModel()
        {
            CompanyId = visitorDetails.companyId
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "Get_Temp_Visitor_Details_Mobile");
        sp.Command.AddParameter("@Request_Id", visitorDetails.RequestId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, VisitorDetails = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, VisitorDetails = (object)null };
    }

    //Push Notification


    public object GetNotification(PushNotification pushNotification)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = pushNotification.CompanyId
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GetNotifications");
        sp.Command.AddParameter("@User_ID", pushNotification.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }


    public object UpdateNotification(PushNotification pushNotification)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = pushNotification.CompanyId
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "INSERT_UPDATE_NOTIFICATIONS");
        sp.Command.AddParameter("@User_ID", pushNotification.UserId, DbType.String);
        sp.Command.AddParameter("@TYPE", pushNotification.DeviceType, DbType.String);
        sp.Command.AddParameter("@DEVICE_ID", pushNotification.DeviceId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }
    public object NotificationDetails(PushNotification pushNotification)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = pushNotification.CompanyId
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "INSERT_NOTIFICATIONS_DETAILS");
        sp.Command.AddParameter("@User_ID", pushNotification.UserId, DbType.String);
        sp.Command.AddParameter("@TYPE", pushNotification.DeviceType, DbType.String);
        sp.Command.AddParameter("@DEVICE_ID", pushNotification.DeviceId, DbType.String);
        sp.Command.AddParameter("@TOCKEN", pushNotification.TokenId, DbType.String);
        sp.Command.AddParameter("@DESCRIPTION", pushNotification.Description, DbType.String);
        sp.Command.AddParameter("@REMARKS", pushNotification.Remarks, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }




    public List<string> InsertSpareIcon(HttpRequest httpRequest)
    {
        try
        {
            HDModel hDModel = new HDModel
            {
                CompanyId = httpRequest.Params[0]
            };
            string companyId = _ReturnDB(hDModel);

            List<string> fileNames = new List<string>();
            for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            {

                var path = HttpRuntime.AppDomainAppPath;
                var req = httpRequest.Files[i];
                var fn = req.FileName;
                string fileName = (DateTime.Now.ToString("ddMMyyyyhhmmss").ToString()) + fn; ;
                string filePath;

                filePath = path + "SpareIcons\\" + companyId + "\\" + fileName;


                req.SaveAs(filePath);

                fileNames.Add(fileName);
            }
            return fileNames;
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public object GetLowStock(HttpRequest httpRequest)
    {
        try
        {
            HDModel hDModel = new HDModel
            {
                CompanyId = httpRequest.Params[0]
            };
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GetLowStockIDs");
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }



    //OptumAllocatSeat


    public object UpdateSpareImage(SpareImagePath spareImagePath)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = spareImagePath.companyId,
        };

        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "UpdateSpareImagePath");
        sp.Command.AddParameter("@SparePartID", spareImagePath.SpareId, DbType.String);
        sp.Command.AddParameter("@SparePartImage", spareImagePath.SpareImage, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }
    public object AllocateSeatsOpt(SPACE_ALLOCATE_DETAILS_LST_HDO allocDetLst)
    {
        try
        {
            HDModel mo = new HDModel();

            mo.CompanyId = allocDetLst.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("AllocateSeats" + JsonConvert.SerializeObject(allocDetLst.LST));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                List<SPACE_ALLOCATE_DETAILS_HDO> hdl = new List<SPACE_ALLOCATE_DETAILS_HDO>();
                hdl.Add(allocDetLst.LST);

                int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
                var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();

                String proc_name = DB + ".SMS_SPACE_ALLOC_MAP_HD";
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(details);
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[1].Value = allocDetLst.AUR_ID;
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[2].Value = 1;
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
                string Result;
                return ds.Tables[0];
            }

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }



    //Honda

    public object ConferenceRoomTimeSlots(GetSpaceDetails getspacedetails)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = getspacedetails.COMPANYID,
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[CONFERENCE_TIME_SLOT_MOBILE]");
            sp.Command.AddParameter("@LCM_CODE", getspacedetails.lcm_code, DbType.String);
            sp.Command.AddParameter("@TWR_CODE", getspacedetails.twr_code, DbType.String);
            sp.Command.AddParameter("@FLR_CODE", getspacedetails.flr_code, DbType.String);
            sp.Command.AddParameter("@CONF_NAME", getspacedetails.Space_Id, DbType.String);
            sp.Command.AddParameter("@StartDate", getspacedetails.FromDate, DbType.DateTime);
            sp.Command.AddParameter("@EndDate", getspacedetails.ToDate, DbType.DateTime);
            ds = sp.GetDataSet();


            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }


    public object GetFloorList(HDModel hdmodel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = hdmodel.CompanyId,
        };
        string DB = _ReturnDB(hdmodel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_FLOOR_MOBILE");
        sp.Command.AddParameter("@AUR_ID", hdmodel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }


    public object Getfacility(AssertTicketRequestModel assertTicketRequestModel)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = assertTicketRequestModel.CompanyId
            };
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GETFACILITIES_MOBILE");
            sp.Command.AddParameter("@LCM_CODE", assertTicketRequestModel.LocationCode, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
        }
    }



    public object Getuserdetails(HDModel hdmodel)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = hdmodel.CompanyId,
            };
            string DB = _ReturnDB(hdmodel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "HRDGET_USER_DETAILS_Mobile");
            sp.Command.AddParameter("@AUR_ID", hdmodel.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
        }
        catch (Exception ex)
        {

            return new { Message = ex.Message };
        }
    }



    public object GetUpdateModifyWithhold(BlockSeatRequest blockSeatRequest)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = blockSeatRequest.companyId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "CONF_UPDATE_WITHHOLD_MOBILE");
        sp.Command.AddParameter("@REQ_ID", blockSeatRequest.RequestId, DbType.String);
        sp.Command.AddParameter("@FROM_DATE", blockSeatRequest.FromDate, DbType.Date);
        sp.Command.AddParameter("@TO_DATE", blockSeatRequest.ToDate, DbType.Date);
        sp.Command.AddParameter("@FROM_TIME", blockSeatRequest.FromTime, DbType.DateTime);
        sp.Command.AddParameter("@TO_TIME", blockSeatRequest.ToTime, DbType.DateTime);
        sp.Command.AddParameter("@STATUSID", blockSeatRequest.Status, DbType.Int64);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }



    public object GetReleaseRequest(BlockSeatRequest blockSeatRequest)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = blockSeatRequest.companyId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "CONF_RELEASE_WITHHOLD_MOBILE");
        sp.Command.AddParameter("@REQ_ID", blockSeatRequest.RequestId, DbType.String);
        sp.Command.AddParameter("@AUR_ID", blockSeatRequest.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }



    //private string UpdateAllocationOccupied(BlockSeatRequest blockSeatRequest, string db)
    //{
    //    string ftime = "";
    //    string ttime = "";
    //    ftime = blockSeatRequest.FromTime.ToString();
    //    ttime = blockSeatRequest.ToTime.ToString();
    //    int sta = blockSeatRequest.Status;
    //    string RIDDS = RIDGENARATION("VerticalReq");
    //    String procedureName = db + ".USP_CONF_MODIFY_SEAT";
    //    SqlParameter[] param3 = new SqlParameter[10];
    //    param3[0] = new SqlParameter("@REQID", SqlDbType.NVarChar, 200);
    //    param3[0].Value = blockSeatRequest.RequestId;
    //    param3[1] = new SqlParameter("@EMPID", SqlDbType.NVarChar, 200);
    //    param3[1].Value = blockSeatRequest.UserId;
    //    param3[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
    //    param3[2].Value = blockSeatRequest.FromDate;
    //    param3[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
    //    param3[3].Value = blockSeatRequest.ToDate;
    //    param3[4] = new SqlParameter("@FROMTIME", SqlDbType.DateTime);
    //    param3[4].Value = ftime;
    //    param3[5] = new SqlParameter("@TOTIME", SqlDbType.DateTime);
    //    param3[5].Value = ttime;
    //    param3[6] = new SqlParameter("@STATUSID", SqlDbType.NVarChar, 200);
    //    param3[6].Value = sta;
    //    param3[7] = new SqlParameter("@INT_ATND", SqlDbType.NVarChar, 1000);
    //    param3[7].Value = blockSeatRequest.InstanceAttendance;
    //    param3[8] = new SqlParameter("@EXT_ATND", SqlDbType.NVarChar, 1000);
    //    //param3[8].Value = Regex.Replace(Regex.Replace("".TrimEnd(), @"\t|\n|\r", ","), ",,+", ",");
    //    param3[8].Value = blockSeatRequest.ExtendAttendance;
    //    param3[9] = new SqlParameter("@DESC", SqlDbType.NVarChar, 200);
    //    param3[9].Value = blockSeatRequest.Description;
    //    DataSet ds1 = new DataSet();
    //    ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, procedureName, param3);
    //    SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(db + "." + "SEND_MAIL_CONF_MODIFY_REQUEST");
    //    sp.Command.AddParameter("@REQID", blockSeatRequest.RequestId, DbType.String);
    //    sp.Command.AddParameter("@AUR_ID", blockSeatRequest.UserId, DbType.String);
    //    sp.ExecuteScalar();
    //    if (ds1 != null)
    //    {
    //        return "Modified Successfully";
    //    }
    //    else
    //    {
    //        return "Invalida Data";
    //    }
    //}

    public object GetEditModify(BlockSeatRequest blockSeatRequest)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = blockSeatRequest.companyId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "VIEW_CONFERENCE_WITHHOLD_MOBILE");
        sp.Command.AddParameter("@REQ_ID", blockSeatRequest.RequestId, DbType.String);
        sp.Command.AddParameter("@AUR_ID", blockSeatRequest.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC };
    }




    public object GetReservationRoomlist(GetSpaceDetails getspacedetails)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = getspacedetails.COMPANYID,
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[AM_GET_SPACE_CONFERENCE_BY_ANY_ONE_MOBILE]");
            sp.Command.AddParameter("@CTY_CODE", getspacedetails.city_code, DbType.String);
            sp.Command.AddParameter("@LCM_CODE", getspacedetails.lcm_code, DbType.String);
            sp.Command.AddParameter("@TWR_CODE", getspacedetails.twr_code, DbType.String);
            sp.Command.AddParameter("@FLR_CODE", getspacedetails.flr_code, DbType.String);
            sp.Command.AddParameter("@CAPACITY", getspacedetails.capacity, DbType.String);
            sp.Command.AddParameter("@type", getspacedetails.mode, DbType.String);
            ds = sp.GetDataSet();


            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    private string UpdateAllocationOccupied(BlockSeatRequest blockSeatRequest, string db)
    {
        string ftime = "";
        string ttime = "";
        ftime = blockSeatRequest.FromTime.ToString();
        ttime = blockSeatRequest.ToTime.ToString();
        int sta = blockSeatRequest.Status;
        string RIDDS = RIDGENARATION("VerticalReq");
        String procedureName = db + ".USP_CONF_MODIFY_SEAT";
        SqlParameter[] param3 = new SqlParameter[10];
        param3[0] = new SqlParameter("@REQID", SqlDbType.NVarChar, 200);
        param3[0].Value = blockSeatRequest.RequestId;
        param3[1] = new SqlParameter("@EMPID", SqlDbType.NVarChar, 200);
        param3[1].Value = blockSeatRequest.UserId;
        param3[2] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
        param3[2].Value = blockSeatRequest.FromDate;
        param3[3] = new SqlParameter("@TODATE", SqlDbType.DateTime);
        param3[3].Value = blockSeatRequest.ToDate;
        param3[4] = new SqlParameter("@FROMTIME", SqlDbType.DateTime);
        param3[4].Value = ftime;
        param3[5] = new SqlParameter("@TOTIME", SqlDbType.DateTime);
        param3[5].Value = ttime;
        param3[6] = new SqlParameter("@STATUSID", SqlDbType.NVarChar, 200);
        param3[6].Value = sta;
        param3[7] = new SqlParameter("@INT_ATND", SqlDbType.NVarChar, 1000);
        param3[7].Value = blockSeatRequest.InstanceAttendance;
        param3[8] = new SqlParameter("@EXT_ATND", SqlDbType.NVarChar, 1000);
        //param3[8].Value = Regex.Replace(Regex.Replace("".TrimEnd(), @"\t|\n|\r", ","), ",,+", ",");
        param3[8].Value = blockSeatRequest.ExtendAttendance;
        param3[9] = new SqlParameter("@DESC", SqlDbType.NVarChar, 200);
        param3[9].Value = blockSeatRequest.Description;
        DataSet ds1 = new DataSet();
        ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, procedureName, param3);
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(db + "." + "SEND_MAIL_CONF_MODIFY_REQUEST");
        sp.Command.AddParameter("@REQID", blockSeatRequest.RequestId, DbType.String);
        sp.Command.AddParameter("@AUR_ID", blockSeatRequest.UserId, DbType.String);
        sp.ExecuteScalar();
        if (ds1 != null)
        {
            return "Modified Successfully";
        }
        else
        {
            return "Invalida Data";
        }
    }



    public object GetCapacitylist(GetSpaceDetails getspacedetails)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = getspacedetails.COMPANYID,
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[AM_GET_SPACE_CAPACITY_MOBILE]");
            sp.Command.AddParameter("@CTY_CODE", getspacedetails.city_code, DbType.String);
            sp.Command.AddParameter("@LCM_CODE", getspacedetails.lcm_code, DbType.String);
            sp.Command.AddParameter("@TWR_CODE", getspacedetails.twr_code, DbType.String);
            sp.Command.AddParameter("@FLR_CODE", getspacedetails.flr_code, DbType.String);
            sp.Command.AddParameter("@type", getspacedetails.mode, DbType.String);

            ds = sp.GetDataSet();


            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }



    public object GetModifyBookedDetails(ConferencedBooked conferencedBooked)
    {
        try
        {
            string returnMessage = "";
            HDModel hDModel = new HDModel()
            {
                CompanyId = conferencedBooked.COMPANYID,
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[VIEW_CONFERENCE_WITHHOLD_MOBILE]");
            sp.Command.AddParameter("@REQ_ID", "", DbType.String);
            sp.Command.AddParameter("@AUR_ID", conferencedBooked.UserID, DbType.String);
            sp.Command.AddParameter("@type", conferencedBooked.Type, DbType.String);
            ds = sp.GetDataSet();

            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    //Conf Room Modify Request
    //public object UpdateSeatRequest(BlockSeatRequest blockSeatRequest)
    //{
    //    HDModel hDModel = new HDModel()
    //    {
    //        CompanyId = blockSeatRequest.companyId
    //    };
    //    string DB = _ReturnDB(hDModel);

    //    var data = UpdateAllocationOccupied(blockSeatRequest, DB);

    //    return new { Message = MessagesVM.UM_OK, result = data };

    //}



    //Maxlife check if user has completed today inspection 
    //written by raghav
    public object CheckUserInspection(HDModel hdmodel)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = hdmodel.CompanyId,
            };
            string DB = _ReturnDB(hdmodel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "CHECK_USER_INSPECTION");
            sp.Command.AddParameter("@USER_ID", hdmodel.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Result = ds.Tables[0].Rows[0]["RESULT"] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Result = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = ex.Message };
        }
    }


    //Conf Room Modify Request
    public object UpdateSeatRequest(BlockSeatRequest blockSeatRequest)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = blockSeatRequest.companyId
        };
        string DB = _ReturnDB(hDModel);

        var data = UpdateAllocationOccupied(blockSeatRequest, DB);

        return new { Message = MessagesVM.UM_OK, result = data };

    }


    public object GetConfBookingDetails(GetSpaceDetails getspacedetails)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = getspacedetails.COMPANYID,
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[SEARCH_CONFERENCE_REQUESTS_Mobile]");
            sp.Command.AddParameter("@REQ_ID", getspacedetails.RequestId, DbType.String);
            sp.Command.AddParameter("@LOCATION", getspacedetails.lcm_code, DbType.String);
            sp.Command.AddParameter("@CONFROOM", getspacedetails.Space_Id, DbType.String);
            sp.Command.AddParameter("@FROMDATE", getspacedetails.FromDate, DbType.String);
            sp.Command.AddParameter("@TODATE", getspacedetails.ToDate, DbType.String);
            ds = sp.GetDataSet();


            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, result = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }


    public object Getuseremaildetails(HDModel hdmodel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = hdmodel.CompanyId,
        };
        string DB = _ReturnDB(hdmodel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_INT_ATTENDESS_MOBILE");
        sp.Command.AddParameter("@SEARCH_CRITERIA", hdmodel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }


    public object ConfSubmit(ConferencedBooked conferencedBooked)
    {
        try
        {
            string returnMessage = "";
            HDModel hDModel = new HDModel()
            {
                CompanyId = conferencedBooked.COMPANYID,
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[GET_CONFERENCE_BOOKED_SLOTS_MOBILE]");
            sp.Command.AddParameter("@REQ_ID", "", DbType.String);
            sp.Command.AddParameter("@SPC_ID", conferencedBooked.ConferenceId, DbType.String);
            sp.Command.AddParameter("@FROM_DATE", conferencedBooked.FromDate, DbType.Date);
            sp.Command.AddParameter("@TO_DATE", conferencedBooked.ToDate, DbType.Date);
            sp.Command.AddParameter("@FROM_TIME", conferencedBooked.FromTime, DbType.String);
            sp.Command.AddParameter("@TO_TIME", conferencedBooked.ToTime, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
            {
                returnMessage = "Note: selected time interval is already booked, please select other available slots.";
            }
            else
            {
            }

            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }


    private string SubmitAllocationOccupied(BlockSeatRequest blockSeatRequest, string db)
    {
        try
        {

        

        string REQID = REQGENARATION_REQ(blockSeatRequest.VerticalName, "SSA_ID", db + ".", "CONFERENCE_BOOKING");
        string verticalreqid = REQID;
        int cntWst = 0;
        int cntHCB = 0;
        int cntFCB = 0;
        string twr = "";
        string flr = "";
        string wng = "";
        Int16 intCount = 0;
        string ftime = "";
        string ttime = "";
        string StartHr = "00";
        string EndHr = "00";
        string StartMM = "00";
        string EndMM = "00";
        string remarks = "";

        ftime = blockSeatRequest.FromTime.ToString();
        ttime = blockSeatRequest.ToTime.ToString();

        int sta = blockSeatRequest.Status;

        string RIDDS = RIDGENARATION("VerticalReq");

        String proc_name = db + ".USP_CHECK_VERTICAL_REQID";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@VC_REQID", SqlDbType.NVarChar, 50);
        param[0].Value = RIDDS;
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);


        if (ds.Tables[0].Rows[0]["Result"].ToString() == "1")
        {
            return "Request is already raised ";

        }
        else
        {
            cntWst = 1;

            Int32 cnt = 0;
            string lblspcid;
            lblspcid = blockSeatRequest.SpaceId;
            verticalreqid = REQGENARATION_REQ(blockSeatRequest.VerticalRequestId, "ssa_id", db + ".", "SMS_space_allocation");
            string Conf_REQ_ID = verticalreqid;
            String procedureName = db + ".BLOCK_SEATS_REQUEST_PART3";

            SqlParameter[] param3 = new SqlParameter[16];

            param3[0] = new SqlParameter("@REQID", SqlDbType.NVarChar, 200);
            param3[0].Value = REQID;
            param3[1] = new SqlParameter("@VERTICALREQID", SqlDbType.NVarChar, 200);
            param3[1].Value = verticalreqid;
            param3[2] = new SqlParameter("@VERTICAL", SqlDbType.NVarChar, 200);
            param3[2].Value = blockSeatRequest.VerticalRequestId;
            param3[3] = new SqlParameter("@SPCID", SqlDbType.NVarChar, 200);
            param3[3].Value = lblspcid;
            param3[4] = new SqlParameter("@EMPID", SqlDbType.NVarChar, 200);
            param3[4].Value = blockSeatRequest.UserId;
            param3[5] = new SqlParameter("@FROMDATE", SqlDbType.DateTime);
            param3[5].Value = blockSeatRequest.FromDate;
            param3[6] = new SqlParameter("@TODATE", SqlDbType.DateTime);
            param3[6].Value = blockSeatRequest.ToDate;
            param3[7] = new SqlParameter("@SPACETYPE", SqlDbType.NVarChar, 50);
            param3[7].Value = "2";
            param3[8] = new SqlParameter("@FROMTIME", SqlDbType.DateTime);
            param3[8].Value = ftime;
            param3[9] = new SqlParameter("@TOTIME", SqlDbType.DateTime);
            param3[9].Value = ttime;
            param3[10] = new SqlParameter("@STATUSID", SqlDbType.NVarChar, 200);
            param3[10].Value = sta;
            param3[11] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200);
            param3[11].Value = "1";
            param3[12] = new SqlParameter("@INT_ATND", SqlDbType.NVarChar, 1000);
            param3[12].Value = blockSeatRequest.InstanceAttendance;
            param3[13] = new SqlParameter("@EXT_ATND", SqlDbType.NVarChar, 1000);
            param3[13].Value = Regex.Replace(Regex.Replace("".TrimEnd(), @"\t|\n|\r", ","), ",,+", ",");
            param3[14] = new SqlParameter("@DESC", SqlDbType.NVarChar, 200);
            param3[14].Value = blockSeatRequest.Description;
            param3[15] = new SqlParameter("@USER_ID", SqlDbType.NVarChar, 200);
            param3[15].Value = blockSeatRequest.UserId;

            DataSet ds1 = new DataSet();
            ds1 = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, procedureName, param3);

            // -------------------SEND MAIL-----------------

            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(db + "." + "SEND_MAIL_CONF_BOOKING_REQUEST");
            sp.Command.AddParameter("@REQID", REQID, DbType.String);
            sp.ExecuteScalar();

            return "Booked Successfully";
        }
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public string REQGENARATION_REQ(string Strfield, string countfield, string DataBase, string Table, string Wherecondiction = "1=1", string field1 = "0", string field2 = "0")
    {
        string TmpReqseqid;
        int Reqseqid;

        string strsql = "SELECT isnull(MAX(" + countfield + "),0)+1 FROM " + DataBase + Table + " where " + Wherecondiction;
        Reqseqid = Int32.Parse(SqlHelper.ExecuteScalar(CommandType.Text, strsql).ToString());

        if (Reqseqid < 10)
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year + "/" + Strfield + "/000000" + Reqseqid;
        else if (Reqseqid < 100)
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year + "/" + Strfield + "/00000" + Reqseqid;
        else if (Reqseqid < 1000)
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year + "/" + Strfield + "/0000" + Reqseqid;
        else if (Reqseqid < 10000)
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year + "/" + Strfield + "/000" + Reqseqid;
        else if (Reqseqid < 100000)
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year + "/" + Strfield + "/00" + Reqseqid;
        else if (Reqseqid < 1000000)
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year + "/" + Strfield + "/0" + Reqseqid;
        else
            TmpReqseqid = getoffsetdatetime(DateTime.Now).Year + "/" + Strfield + "/" + Reqseqid;
        return TmpReqseqid;
    }
    public string RIDGENARATION(string field1)
    {
        string RID;
        RID = getoffsetdatetime(DateTime.Now).ToString().Replace("/", "");
        RID = RID.Replace("AM", "");
        RID = RID.Replace("PM", "");
        RID = RID.Replace(":", "");
        RID = RID.Replace(" ", "");
        RID = RID.Replace("#", "");
        RID = field1 + "REQ" + RID;
        return RID;
    }

    public DateTime getoffsetdatetime(DateTime date)
    {
        Object O = "+05:30";
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
                Temp = Temp.Insert(0, "+");

            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }
            return _now;
        }
        else
            return DateTime.Now;
    }




    public object GetSeatRequest(BlockSeatRequest blockSeatRequest)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = blockSeatRequest.companyId
        };
        string DB = _ReturnDB(hDModel);

        var data = SubmitAllocationOccupied(blockSeatRequest, DB);

        return new { Message = MessagesVM.UM_OK, result = data };

    }


    public object GetEmpDetails(HDModel hdmodel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = hdmodel.CompanyId,
        };
        string DB = _ReturnDB(hdmodel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_EMP_NAMES_AUTO_COMPLETE_Mobile");
        sp.Command.AddParameter("@NAME", hdmodel.Name, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }

    public object GetBookedDetails(ConferencedBooked conferencedBooked)
    {
        try
        {
            string returnMessage = "";
            HDModel hDModel = new HDModel()
            {
                CompanyId = conferencedBooked.COMPANYID,
            };

            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[VIEW_CONFERENCE_Mobile]");
            sp.Command.AddParameter("@REQ_ID", "", DbType.String);
            sp.Command.AddParameter("@AUR_ID", conferencedBooked.UserID, DbType.String);
            ds = sp.GetDataSet();

            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public void GetupdateNotfi(HDModel hdmodel)
    {
        try
        {


            string DB = _ReturnDB(hdmodel);
            sp = new SubSonic.StoredProcedure(DB + "." + "[UpdateNotification]");
            sp.Command.AddParameter("@sno", hdmodel.Role_id, DbType.String);

            sp.ExecuteScalar();


        }
        catch (Exception ex)
        {

            throw ex;
        }
    }

    public object GetCancelRequest(BlockSeatRequest blockSeatRequest)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = blockSeatRequest.companyId,
        };
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "CancelConferenceRoom_Mobile");
        sp.Command.AddParameter("@REQ_ID", blockSeatRequest.RequestId, DbType.String);
        sp.Command.AddParameter("@AUR_ID", blockSeatRequest.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }


    public object Support(HDModel hdmodel)
    {
        try
        {

            string DB = _ReturnDB(hdmodel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "Support_Customer_Matrix");
            ds = sp.GetDataSet();

            if (ds != null)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }

    }


    //LTTS


    public object ApprovalList(HDModel hdmodel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = hdmodel.CompanyId,
        };
        string DB = _ReturnDB(hdmodel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_SMS_SPACE_MANAGER_APPROVAL_MAP_Mobile");
        sp.Command.AddParameter("@USER", hdmodel.UserId, DbType.String);
        sp.Command.AddParameter("@ROLID", hdmodel.Role_id, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }

    public object ApprovalBooking(HDModel hdmodel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = hdmodel.CompanyId,
        };
        string DB = _ReturnDB(hdmodel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "UPD_SMS_SPACE_MANAGER_APPROVAL_MAP_Mobile");
        sp.Command.AddParameter("@SSMA_ID", hdmodel.code, DbType.String);
        sp.Command.AddParameter("@USER", hdmodel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }


    public object AllocateSpaceValidation(HD_SPACE_ALLOCATE_DETAILS_LST allocDetLst)
    {
        try
        {
            HDModel mo = new HDModel();

            mo.CompanyId = allocDetLst.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("AllocateSeats" + JsonConvert.SerializeObject(allocDetLst.LST));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                List<HD_SPACE_ALLOCATE_DETAILS> hdl = new List<HD_SPACE_ALLOCATE_DETAILS>();
                hdl.Add(allocDetLst.LST);

                int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
                var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();

                String proc_name = DB + ".SMS_SPACE_ALLOC_MAP_VALIDATION_Mobile";
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(details);
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[1].Value = allocDetLst.AUR_ID;
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[2].Value = 1;
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
                string Result;
                return ds.Tables[0];


            }

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object AllocateSpaceManagerValidation(HD_SPACE_ALLOCATE_DETAILS_LST allocDetLst)
    {
        try
        {
            HDModel mo = new HDModel();

            mo.CompanyId = allocDetLst.COMPANYID;
            string DB = _ReturnDB(mo);
            ErrorHandler err1 = new ErrorHandler();
            err1._WriteErrorLog_string("AllocateSeats" + JsonConvert.SerializeObject(allocDetLst.LST));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                List<HD_SPACE_ALLOCATE_DETAILS> hdl = new List<HD_SPACE_ALLOCATE_DETAILS>();
                hdl.Add(allocDetLst.LST);

                int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
                var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();

                String proc_name = DB + ".SMS_SPACE_MANAGER_APPROVAL_MAP_Mobile";
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
                param[0].Value = UtilityService.ConvertToDataTable(details);
                param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
                param[1].Value = allocDetLst.AUR_ID;
                param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
                param[2].Value = 1;
                param[3] = new SqlParameter("@UPL_TYPE", SqlDbType.NVarChar);
                param[3].Value = "FROMMAP";
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
                string Result;
                return ds.Tables[0];


            }

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //LTTSHotDeskingATKINS
    public object GetEmployeeData(HDModel hdmodel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = hdmodel.CompanyId,
        };
        string DB = _ReturnDB(hdmodel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_EMPLOYEE_DETAILS_FOR_HOTDESKING_Mobile");
        sp.Command.AddParameter("@AUR_ID", hdmodel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }


    public object GetEmployeeStatus(HDModel hdmodel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = hdmodel.CompanyId,
        };
        string DB = _ReturnDB(hdmodel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "CHECK_EMPLOYEE_STATUS_Mobile");
        sp.Command.AddParameter("@AUR_ID", hdmodel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }


    public object GetEmployeeCostCenterDetails(HDModel hdmodel)
    {
        HDModel hDModel = new HDModel()
        {
            CompanyId = hdmodel.CompanyId,
        };
        string DB = _ReturnDB(hdmodel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_VERTICAL_COSTCENTER_BY_AUR_ID_Mobile");
        sp.Command.AddParameter("@AUR_ID", hdmodel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
    }

    public object GetRequestDetails(GetSpaceDetails HDCNT)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = HDCNT.COMPANYID;
        string DB = _ReturnDB(mo);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "HelpDes_Request_Details");
        sp.Command.AddParameter("@user_id ", HDCNT.AURID, DbType.String);
        sp.Command.AddParameter("@COMPANYID", 1, DbType.Int32);
        sp.Command.AddParameter("@FROM_DATE", HDCNT.FromDate, DbType.String);
        sp.Command.AddParameter("@TO_DATE", HDCNT.ToDate, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

    }
    public object GetRequestDetailsSatusWise(GetSpaceDetails HDCNT)
    {
        HDModel mo = new HDModel();
        mo.CompanyId = HDCNT.COMPANYID;
        string DB = _ReturnDB(mo);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "HelpDes_Request_Details_StatusWise");
        sp.Command.AddParameter("@user_id ", HDCNT.AURID, DbType.String);
        sp.Command.AddParameter("@COMPANYID", 1, DbType.Int32);
        sp.Command.AddParameter("@FROM_DATE", HDCNT.FromDate, DbType.String);
        sp.Command.AddParameter("@TO_DATE", HDCNT.ToDate, DbType.String);
        sp.Command.AddParameter("@Status", HDCNT.mode, DbType.String);
        sp.Command.AddParameter("@Location", HDCNT.lcm_code, DbType.String);
        sp.Command.AddParameter("@Category", HDCNT.category, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

    }
    public object GetEmployeeHotDesking(HDModel hdmodel)
    {

        try
        {
            HDModel mo = new HDModel();
            string DB = _ReturnDB(hdmodel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_HOT_DESKING_REQUESTS_Mobile");
            sp.Command.AddParameter("@USER_ID", hdmodel.UserId, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int32);
            sp.Command.Parameters.Add("@MODE", hdmodel.code, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }



    }

    public object RaiseRequst(HotDeskRequisitionMobile hotDeskRequisitionMobile)
    {
        try
        {



            HDModel hDModel = new HDModel()
            {
                CompanyId = hotDeskRequisitionMobile.companyId,
            };

            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".REQUEST_HOT_DESKING_SEATS_NEWEMP_Mobile";
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@HDLST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(hotDeskRequisitionMobile.hDDetails);
            param[1] = new SqlParameter("@REM", SqlDbType.VarChar);
            param[1].Value = hotDeskRequisitionMobile.EmployeeRemarks;
            param[2] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[2].Value = hotDeskRequisitionMobile.User_ID;
            param[3] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
            param[3].Value = hotDeskRequisitionMobile.Allocation_ID;
            param[4] = new SqlParameter("@LCMLST", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(hotDeskRequisitionMobile.locationlstts);
            param[5] = new SqlParameter("@TWRLST", SqlDbType.Structured);
            param[5].Value = UtilityService.ConvertToDataTable(hotDeskRequisitionMobile.towerlsts);
            param[6] = new SqlParameter("@FLRLST", SqlDbType.Structured);
            param[6].Value = UtilityService.ConvertToDataTable(hotDeskRequisitionMobile.floorlsts);
            param[7] = new SqlParameter("@REQ_ID", SqlDbType.NVarChar);
            param[7].Value = hotDeskRequisitionMobile.RequestID;
            param[8] = new SqlParameter("@L1_REM", SqlDbType.VarChar);
            param[8].Value = hotDeskRequisitionMobile.L1Remarks;
            param[9] = new SqlParameter("@L2_REM", SqlDbType.VarChar);
            param[9].Value = hotDeskRequisitionMobile.L2Remarks;

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            var data = ds.Tables[0].Rows[0]["REQ_ID"].ToString();
            if (data.Length > 0)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_Not_OK, result = ds };
        }

        catch (Exception E)
        {
            return new { Message = MessagesVM.UM_Not_OK, result = "Something Went Wrong" };
        }


    }

    public object RequstApprove(HotDeskApproveMobile hotDeskApproveMobile)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = hotDeskApproveMobile.companyId,
            };

            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".HD_APPROVE_REQUISITIONS_Mobile";
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@HD_REQ_ID", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(hotDeskApproveMobile.hDRequistIDs);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
            param[1].Value = hotDeskApproveMobile.User_ID;
            param[2] = new SqlParameter("@ALLOCSTA", SqlDbType.Int);
            param[2].Value = hotDeskApproveMobile.Allocation_ID;
            param[3] = new SqlParameter("@L1_REM", SqlDbType.VarChar);
            param[3].Value = hotDeskApproveMobile.L1Remarks;
            param[4] = new SqlParameter("@L2_REM", SqlDbType.VarChar);
            param[4].Value = hotDeskApproveMobile.L2Remarks;

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            var data = ds.Tables[0].Rows[0]["Result"].ToString();
            if (data == "success")
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.UM_Not_OK, result = "Something Went Wrong" };
        }



    }


    public object FinalApprovel(HotDeskRequisitionMobile hotDeskRequisitionMobile)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = hotDeskRequisitionMobile.companyId,
            };

            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".EDIT_VIEW_HOTDESK_REQUISITION_Mobile";
            sp.Command.AddParameter("@REQ_ID", hotDeskRequisitionMobile.RequestID, DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }



    }

    public object GetAppVersion(string Type)
    {
        try
        {
            sp = new SubSonic.StoredProcedure("GET_VERSION");
            sp.Command.Parameters.Add("@TYPE", Type, DbType.Int32);
            ds = sp.GetDataSet();

            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }



    }


    public object InsertDownTime(DownTimeModel downTimeModel)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = downTimeModel.CompanyID;
            string DB = _ReturnDB(mo);
            sp = new SubSonic.StoredProcedure(DB + ".SP_InsertDownTime_Mobile");
            sp.Command.Parameters.Add("@SER_REQ_ID", downTimeModel.RequestID, DbType.Int32);
            sp.Command.Parameters.Add("@TODATE", downTimeModel.ToDate, DbType.Int32);
            sp.Command.Parameters.Add("@FROMDATE", downTimeModel.FromDate, DbType.Int32);
            sp.Command.Parameters.Add("@BDMP_DWNTIME_FACTOR", downTimeModel.DownTimeFactor, DbType.Int32);
            sp.Command.Parameters.Add("@BDMP_DOWNTIME", downTimeModel.DownTime, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object DeleteDownTime(breaktime1 reldet)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = reldet.CompanyId;
            string DB = _ReturnDB(mo);
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SER_DOWN_ID", SqlDbType.Int);
            param[0].Value = reldet.SER_DOWN_ID;
            Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DB + ".SP_DeleteDownTime", param);
            if (obj != null)
                return new { Message = MessagesVM.UM_OK };
            else
                return new { Message = MessagesVM.ErrorMessage };
        }
        catch (Exception ex)
        {

            return new { Message = MessagesVM.ErrorMessage };
        }
    }

    public object GetFloor(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_FLOOR_BY_TOWER");
            sp.Command.Parameters.Add("@TWR_CODE", hDModel.code, DbType.String);
            ds = sp.GetDataSet();

            if (ds.Tables[0].Rows.Count == 0)
            {
                return new { Message = "Not OK" };
            }
            else
            {
                return new { Message = "OK", Data = ds.Tables[0] };

            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public object getDownTimeData(DownTimeRpt1 CustRpt)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = CustRpt.CompanyID;
            string DB = _ReturnDB(mo);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "SP_GetDownDetail");
            sp.Command.Parameters.Add("@SER_REQ_ID", CustRpt.REQID, DbType.String);
            ds = sp.GetDataSet();
            //return ds.Tables[0];
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0], data2 = ds.Tables[1] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null, data2 = (object)null };
        }
        catch (Exception)
        {

            return new { Message = MessagesVM.UM_NO_REC, data = (object)null, data2 = (object)null };
        }
    }
    public object HelpdeskApprovalList(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + ".[HDM_VIEW_REQUISITIONS_FOR_APPROVALS]");
            sp.Command.Parameters.Add("@AURID", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    internal object RejectApprovalRequest(HDApproveModel hDApproveModel)
    {
        try
        {
            HDModel hDModel = new HDModel
            {
                CompanyId = hDApproveModel.CompanyID
            };
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + ".[HDM_REJECT_REQUISITION_BY_REQ_ID]");
            sp.Command.Parameters.Add("@REQID", hDApproveModel.RequestID, DbType.String);
            sp.Command.Parameters.Add("@AURID", hDApproveModel.UserID, DbType.String);
            sp.Command.Parameters.Add("@STA_ID", hDApproveModel.StatusID, DbType.String);
            sp.Command.Parameters.Add("@SER_LOC_CODE", hDApproveModel.LocationCode, DbType.String);
            sp.Command.Parameters.Add("@SER_MNC_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_SUB_CAT_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_CHILD_CAT_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_APPR_COMMENTS", hDApproveModel.Remarks, DbType.String);
            sp.Command.Parameters.Add("@SER_APPR_AMOUNT", hDApproveModel.ApprovalAmount, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }


    internal object SkipApprovalRequest(HDApproveModel hDApproveModel)
    {
        try
        {
            HDModel hDModel = new HDModel
            {
                CompanyId = hDApproveModel.CompanyID
            };
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + ".[HDM_SKIP_REQUISITION_BY_REQ_ID]");
            sp.Command.Parameters.Add("@REQID", hDApproveModel.RequestID, DbType.String);
            sp.Command.Parameters.Add("@AURID", hDApproveModel.UserID, DbType.String);
            sp.Command.Parameters.Add("@STA_ID", hDApproveModel.StatusID, DbType.String);
            sp.Command.Parameters.Add("@SER_LOC_CODE", hDApproveModel.LocationCode, DbType.String);
            sp.Command.Parameters.Add("@SER_MNC_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_SUB_CAT_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_CHILD_CAT_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_APPR_COMMENTS", hDApproveModel.Remarks, DbType.String);
            sp.Command.Parameters.Add("@SER_AMOUNT", hDApproveModel.ApprovalAmount, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    internal object ApproveApprovalRequest(HDApproveModel hDApproveModel)
    {
        try
        {
            HDModel hDModel = new HDModel
            {
                CompanyId = hDApproveModel.CompanyID
            };
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + ".[HDM_APPROVE_REQUISITION_BY_REQ_ID]");
            sp.Command.Parameters.Add("@REQID", hDApproveModel.RequestID, DbType.String);
            sp.Command.Parameters.Add("@AURID", hDApproveModel.UserID, DbType.String);
            sp.Command.Parameters.Add("@STA_ID", hDApproveModel.StatusID, DbType.String);
            sp.Command.Parameters.Add("@SER_LOC_CODE", hDApproveModel.LocationCode, DbType.String);
            sp.Command.Parameters.Add("@SER_MNC_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_SUB_CAT_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_CHILD_CAT_CODE", "", DbType.String);
            sp.Command.Parameters.Add("@SER_APPR_COMMENTS", hDApproveModel.Remarks, DbType.String);
            sp.Command.Parameters.Add("@SER_APPR_AMOUNT", hDApproveModel.ApprovalAmount, DbType.String);
            sp.Command.Parameters.Add("@SER_CLAIM_AMT", hDApproveModel.ClaimAmount, DbType.String);
            sp.Command.Parameters.Add("@COMPANYID", 1, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    //PPM update for M&S
    public object GetPlanViewMobile(PvmGetPlans PGP)
    {
        try
        {
            HDModel hDModel = new HDModel
            {
                CompanyId = PGP.CompanyId
            };
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "GET_PPM_PLAN_VIEW_MOBILE");
            sp.Command.Parameters.Add("@AUR_ID", PGP.UserId, DbType.String);
            sp.Command.AddParameter("@PageNo", PGP.PageNo, DbType.String);
            sp.Command.AddParameter("@PageSize", PGP.PageSize, DbType.String);
            ds = sp.GetDataSet();

            return new { data = ds.Tables[0] };

        }
        catch (Exception ex)
        {
            return new { message = "Something went wrong" };
        }
    }


    public object PvmUpdatePlansMobile(PvmUpdatePlansMobile PvmUpdatePlansMobiles)
    {

        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = PvmUpdatePlansMobiles.COMPANYID
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".MN_PVM_UPDATE_PLAN_DTLS_MOBILE";
            SqlParameter[] param = new SqlParameter[9];
            param[0] = new SqlParameter("@PVD_PLANEND_TIME", SqlDbType.VarChar);
            param[0].Value = PvmUpdatePlansMobiles.PVD_PLANEND_TIME;
            param[1] = new SqlParameter("@PVD_PLANEXEC_DT", SqlDbType.DateTime);
            param[1].Value = PvmUpdatePlansMobiles.PVD_PLANEXEC_DT;
            param[2] = new SqlParameter("@PVD_PLANSPARES_COST", SqlDbType.VarChar);
            param[2].Value = PvmUpdatePlansMobiles.PVD_PLANSPARES_COST;
            param[3] = new SqlParameter("@PVD_PLANLABOUR_COST", SqlDbType.VarChar);
            param[3].Value = PvmUpdatePlansMobiles.PVD_PLANLABOUR_COST;
            param[4] = new SqlParameter("@PVD_PLAN_REMARKS", SqlDbType.VarChar);
            param[4].Value = PvmUpdatePlansMobiles.PVD_PLAN_REMARKS;
            param[5] = new SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int);
            param[5].Value = PvmUpdatePlansMobiles.PVD_PLANSTA_ID;
            param[6] = new SqlParameter("@PVD_PLAN_ID", SqlDbType.VarChar);
            param[6].Value = PvmUpdatePlansMobiles.PVD_PLAN_ID;
            param[7] = new SqlParameter("@PVD_ID", SqlDbType.Int);
            param[7].Value = PvmUpdatePlansMobiles.PVD_ID;
            param[8] = new SqlParameter("@PVD_UPLOADED_DOC", SqlDbType.VarChar);
            param[8].Value = PvmUpdatePlansMobiles.PVD_UPLOADED_DOC;
            //param[9] = new SqlParameter("@COMPANYID", SqlDbType.VarChar);
            //param[9].Value = PvmUpdatePlansMobiles.COMPANYID;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            //var data = ds.Tables[0].Rows[0]["Result"].ToString();

            if (ds.Tables[0] != null)
                return new { Message = MessagesVM.UM_OK, result = ds.Tables[0].Rows[0]["Result"].ToString() };
            else
                return new { Message = MessagesVM.UM_NO_REC, result = (object)null };
        }
        catch (Exception ex)
        {

            throw ex;
        }
    }


    public object UploadPPMMultipleImages(HttpRequest httpRequest)
    {
        try
        {
            //for (int i = 0; i < httpRequest.Files.AllKeys.Length; i++)
            //{
            //HDModel hd = new HDModel();
            //hd.CompanyId = httpRequest.Params["CompanyId"];
            //string DB = _ReturnDB(hd);

            HDModel hDModel = new HDModel
            {
                CompanyId = httpRequest.Params[0]
            };
            string DB = _ReturnDB(hDModel);

            var req = httpRequest.Files[0];
            var fn = DateTime.Now.ToString("ddMMyyyy").ToString() + req.FileName;
            var path = HttpRuntime.AppDomainAppPath;
            var filePath = path + "UploadFiles\\" + DB + "\\" + "Maintenance" + "\\" + fn;
            req.SaveAs(filePath);
            //}
            string Result;
            return new { Result = fn };
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        };
    }



    public object GetLocationsByCity(HDModel main)
    {
        try
        {
            List<BCLLocationsByCity> BCLLocationsList = new List<BCLLocationsByCity>();
            BCLLocationsByCity BCLLoc = new BCLLocationsByCity();
            string DB = _ReturnDB(main);
            int cnt;
            int Expiry;
            ErrorHandler err2 = new ErrorHandler();
            err2._WriteErrorLog_string("inside" + JsonConvert.SerializeObject(main.UserId));
            ErrorHandler err = new ErrorHandler();
            err._WriteErrorLog_string("DatabaseCheck" + JsonConvert.SerializeObject(DB));
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {

                ErrorHandler err1 = new ErrorHandler();
                err1._WriteErrorLog_string("InsideDatabase" + JsonConvert.SerializeObject(main.UserId));
                sp = new SubSonic.StoredProcedure(DB + "." + "[BCL_GET_LOCATIONS_BYCITY]");
                sp.Command.AddParameter("@AUR_ID", main.UserId, DbType.String);
                sp.Command.AddParameter("@City", main.City, DbType.String);
                DataSet ds = new DataSet();
                ds = sp.GetDataSet();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    BCLLoc = new BCLLocationsByCity();
                    BCLLoc.LCM_CODE = ds.Tables[0].Rows[i]["LCM_CODE"].ToString();
                    BCLLoc.LCM_NAME = ds.Tables[0].Rows[i]["LCM_NAME"].ToString();
                    BCLLocationsList.Add(BCLLoc);
                }

                return new { LocationsList = BCLLocationsList };
            }


        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }

    public object GetAssetBarScan(HDModel hd)
    {
        try
        {
            string DB = _ReturnDB(hd);
            sp = new SubSonic.StoredProcedure(DB + "." + "[GETASSET_BARCODE_SCAN]");
            sp.Command.AddParameter("@AssetID", hd.code, DbType.String);
            sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
            sp.Command.AddParameter("@Lcmcode", hd.City, DbType.String);
            using (IDataReader sdr = sp.GetReader())
            {
                AssetBarScan assetBarScan = new AssetBarScan();
                while (sdr.Read())
                {
                    assetBarScan.AssetID = sdr["ABD_AST_CODE"].ToString();
                    assetBarScan.AssetName = sdr["ABD_AST_NAME"].ToString();
                    assetBarScan.LcmCode = sdr["ABD_LOC_CODE"].ToString();
                    assetBarScan.AssetDescription = sdr["ABD_AST_DESC"].ToString();
                    assetBarScan.Scan_Flag = sdr["SCAN_FLAG"].ToString();
                }
                if (!string.IsNullOrEmpty(assetBarScan.AssetID))
                {
                    return new { Message = MessagesVM.AF_OK, Data = assetBarScan };
                }
                return new { Message = MessagesVM.UM_NO_REC, Data = (object)null };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }

    }

    public object AssetData_To_Reconsolidate(PostAssetBarScan hd)
    {
        try
        {
            HDModel hDModel = new HDModel
            {
                CompanyId = hd.CompanyId
            };
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "[INSERT_ASSET_BARCODE]");
            sp.Command.AddParameter("@AssetID", hd.AssetID, DbType.String);
            sp.Command.AddParameter("@AssetName", hd.AssetName, DbType.String);
            sp.Command.AddParameter("@LcmCode", hd.LcmCode, DbType.String);
            sp.Command.AddParameter("@AUR_ID", hd.UserId, DbType.String);
            sp.Command.AddParameter("@Asset_Status", hd.Ast_Status_Code, DbType.String);
            int i = (int)sp.ExecuteScalar();
            if (i == 1)
            {
                return new { Message = MessagesVM.EM_INSERTED, Flag = i };
            }
            else
            {
                return new { Message = MessagesVM.Err, Flag = i };
            }

        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.Err, Flag = 0 };
        }

    }
    public object GetLocationsScore(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "[GET_LOCATION_SCORE_APP]");
        sp.Command.AddParameter("@AURID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();

        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, InspectionList = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    //Get employee details based on login for Incentive
    public object GetDetails(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        //int BCL_ID = Int32.Parse(hDModel.Password);
        sp = new SubSonic.StoredProcedure(DB + "." + "[GET_BICDETAILS_FOR_BRANCHINCENTIVE]");
        sp.Command.AddParameter("@AUR_ID", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = "OK", data = ds.Tables[0] };
        //return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = "Not OK", data = (object)null };
    }

    //getgrid data for Incentive

    public object GetGriddataIncentive(NewCheckListV2 newCheckListV2)
    {

        DataSet dataSet = new DataSet();
        HDModel hDModel = new HDModel();
        hDModel.CompanyId = newCheckListV2.CompanyId;
        string DB = _ReturnDB(hDModel);
        sp = new SubSonic.StoredProcedure(DB + "." + "[BCL_GET_SCORECARD_CATEGORIES]");
        sp.Command.Parameters.Add("@AUR_ID", newCheckListV2.User_ID, DbType.String);
        sp.Command.Parameters.Add("@BCLID", newCheckListV2.BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        List<ChecklistDetail1> details = UtilityService.ConvertDataTableToList<ChecklistDetail1>(ds.Tables[1]);
        List<ChecklistData1> checklistDatas = UtilityService.ConvertDataTableToList<ChecklistData1>(ds.Tables[0]);
        List<ChecklistScore> ChecklistScore = UtilityService.ConvertDataTableToList<ChecklistScore>(ds.Tables[2]);

        foreach (var chkitem in ChecklistScore)
        {
            foreach (var item in checklistDatas)
            {
                if (chkitem.BCL_TP_ID == item.BCL_TP_ID)
                {
                    foreach (var items in details)
                    {
                        if (item.BCL_SUB_CODE == items.BCL_CH_SUB_CODE)
                        {
                            item.checklistDetails.Add(items);
                        }
                    }
                    chkitem.ChecklistScore1.Add(item);
                }
            }
        }
        if (checklistDatas.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ChecklistScore };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    public object SaveIncentive(selecteddata1 dataobj)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = dataobj.CompanyId;
            string DB = _ReturnDB(mo);
            List<selecteddata1> HDSEM = new List<selecteddata1>();
            SqlParameter[] param = new SqlParameter[13];
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(dataobj.LCMLST);
            param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
            param[1].Value = dataobj.InspectdBy;
            param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
            param[2].Value = dataobj.date;
            param[3] = new SqlParameter("@SelCompany", SqlDbType.NVarChar);
            param[3].Value = dataobj.SelCompany;
            param[4] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
            param[4].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
            param[5] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[5].Value = dataobj.UserId;
            param[6] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[6].Value = dataobj.SelCompany;
            param[7] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[7].Value = dataobj.Flag;
            param[8] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
            param[8].Value = dataobj.OVERALL_CMTS;
            param[9] = new SqlParameter("@BCL_TYPE_ID", SqlDbType.Int);
            param[9].Value = dataobj.BCL_TYPE_ID;
            param[10] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[10].Value = dataobj.BCL_ID;
            param[11] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
            param[11].Value = "APP";
            param[12] = new SqlParameter("@FPERCENT", SqlDbType.Float);
            param[12].Value = dataobj.F_Percentage;
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, DB + "." + "[DYNAMIC_SCORE_CHECKLIST]", param);
            int sem_id = (int)dt.Rows[0][0];
            string msg;
            if (sem_id == 0)
            {
                msg = "Saved as Draft";
            }
            else
            {
                msg = "Submitted Succesfully";
            }
            return new { data = msg, Message = msg };
        }
        catch (SqlException SqlExcp)
        {
            return new { data = MessagesVM.Err, Message = MessagesVM.Err };

        }

    }

    public object Asset_Type(HDModel hd)
    {
        List<Asset_Type> Asset_Type_Lst = new List<Asset_Type>();
        try
        {
            string DB = _ReturnDB(hd);
            sp = new SubSonic.StoredProcedure(DB + "." + "[USP_GET_ASSET_TYPES]");
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    Asset_Type Asset_Type = new Asset_Type();
                    Asset_Type.TAG_CODE = sdr["TAG_CODE"].ToString();
                    Asset_Type.TAG_NAME = sdr["TAG_NAME"].ToString();
                    Asset_Type_Lst.Add(Asset_Type);
                }
                return new { Message = MessagesVM.AF_OK, Data = Asset_Type_Lst };
            }
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.UM_NO_REC, Data = (object)null };
        }
    }
    public object AssetScanReport(AssetScanModel ASTModel)
    {
        try
        {
            HDModel hDModel = new HDModel()
            {
                CompanyId = ASTModel.CompanyId
            };
            string DB = _ReturnDB(hDModel);
            String proc_name = DB + ".[ASTSCANSTATUS]";
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(ASTModel.Locations, "LCM_CODE");
            //param[1] = new SqlParameter("@AST_LST", SqlDbType.Structured);
            //param[1].Value = UtilityService.ConvertToDataTable(ASTModel.AstDetails,"AST_NAME");
            param[1] = new SqlParameter("@AST_TYPE", SqlDbType.Int);
            param[1].Value = ASTModel.AstType;
            param[2] = new SqlParameter("@SCANED_MONTH", SqlDbType.DateTime);
            param[2].Value = ASTModel.Month;
            param[3] = new SqlParameter("@PageNo", SqlDbType.Int);
            param[3].Value = ASTModel.PageNo;
            param[4] = new SqlParameter("@PageSize", SqlDbType.Int);
            param[4].Value = ASTModel.PageSize;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, Details = ds };
            else
                return new { Message = MessagesVM.UM_NO_REC, Details = (object)null };

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
    public object allocateSeatsValidation(List<HD_SPACE_ALLOCATE_DETAILS_Val> allocDetLst)
    {
        HDModel mo = new HDModel();

        mo.CompanyId = allocDetLst[0].COMPANYID;
        string DB = _ReturnDB(mo);
        ErrorHandler err1 = new ErrorHandler();
        List<HD_SPACE_ALLOCATE_DETAILS> hdl = new List<HD_SPACE_ALLOCATE_DETAILS>();
        hdl.Add(new HD_SPACE_ALLOCATE_DETAILS
        {
            SPC_ID = allocDetLst[0].SPC_ID,
            SSA_SRNREQ_ID = allocDetLst[0].SSA_SRNREQ_ID,
            SSAD_SRN_REQ_ID = allocDetLst[0].SSAD_SRN_REQ_ID,
            SH_CODE = allocDetLst[0].SH_CODE,
            VERTICAL = allocDetLst[0].VERTICAL,
            Cost_Center_Code = allocDetLst[0].Cost_Center_Code,
            AUR_ID = allocDetLst[0].AUR_ID,
            FROM_DATE = allocDetLst[0].FROM_DATE,
            TO_DATE = allocDetLst[0].TO_DATE,
            FROM_TIME = allocDetLst[0].FROM_TIME,
            TO_TIME = allocDetLst[0].TO_TIME,
            STACHECK = allocDetLst[0].STACHECK,
            ticked = allocDetLst[0].ticked,
            SHIFT_TYPE = allocDetLst[0].SHIFT_TYPE,
            emp = allocDetLst[0].emp
        });
        int[] arr = { (int)RequestState.Modified, (int)RequestState.Added };
        var details = hdl.Where(x => arr.Contains(x.STACHECK)).ToList();


        String proc_name = DB + ".SMS_SPACE_ALLOC_MAP_VALIDATION_FOR_AUR_ID";
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@ALLOC_DET", SqlDbType.Structured);
        param[0].Value = UtilityService.ConvertToDataTable(details);
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = allocDetLst[0].AUR_ID;
        param[2] = new SqlParameter("@COMPANYID", SqlDbType.NVarChar);
        param[2].Value = 1;
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);

        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    public object validateuserspacedetails(HDModel hDModel)
    {
        string DB = _ReturnDB(hDModel);
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_RECENT_BOOKING_LIST_BY_AUR_ID_MOBILE");
        sp.Command.AddParameter("@USER", hDModel.UserId, DbType.String);
        ds = sp.GetDataSet();
        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
}



#region API Key Validation and Getting DB


public partial class SecureAPIKey
{
    public ValidateKey _ValidateAPIKey(string API)
    {
        ValidateKey ver = new ValidateKey();
        if (!(API == null))
        {

            try
            {

                SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "GN_VALIDATE_API");
                sp.Command.AddParameter("@API", API, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {
                    while (sdr.Read())
                    {
                        ver.TENANT_ID = sdr["TENANT_ID"].ToString();
                        ver.COMPANYID = Convert.ToInt32(sdr["COMPANYID"]);
                        HttpContext.Current.Session["useroffset"] = sdr["TENANT_DT_OFFSET"].ToString();
                        ver.AURID = sdr["USER_ID"].ToString();
                        ver.AUR_LOCATION = sdr["AUR_LOCATION"].ToString();
                    }
                }
                return ver;
            }
            catch (SqlException ex)
            {
                ErrorHandler erhndlr = new ErrorHandler();
                erhndlr._WriteErrorLog(ex);
                return ver;
            }
        }
        else
        {
            return ver;
        }
    }
}

public partial class ErrorHandler
{
    #region Error Log Logic
    public void _WriteErrorLog(Exception ex)
    {
        string errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string path = HttpContext.Current.Server.MapPath("~/ErrorLogFiles/" + errorLogFilename);
        if (File.Exists(path))
        {
            using (StreamWriter stwriter = new StreamWriter(path, true))
            {
                stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
                stwriter.WriteLine("Message:" + ex.ToString());
                stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            }
        }
        else
        {
            StreamWriter stwriter = File.CreateText(path);
            stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.WriteLine("Message: " + ex.ToString());
            stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.Close();
        }
    }
    #endregion
    #region Inserting Other Error's In DB Error Log
    public void _OtherExceptionsInDBErrorLog(Exception ex)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "ERROR_LOG");
            sp.Command.AddParameter("@ERROR", ex.ToString(), DbType.String);
            sp.ExecuteScalar();
        }
        catch (SqlException e)
        {
            _WriteErrorLog(e);
        }
    }
    #endregion

    public void _WriteErrorLog_string(String ex)
    {
        string errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string path = HttpContext.Current.Server.MapPath("~/ErrorLogFiles/" + errorLogFilename);
        if (File.Exists(path))
        {
            using (StreamWriter stwriter = new StreamWriter(path, true))
            {
                stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
                stwriter.WriteLine("Message:" + ex.ToString());
                stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            }
        }
        else
        {
            StreamWriter stwriter = File.CreateText(path);
            stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.WriteLine("Message: " + ex.ToString());
            stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.Close();
        }
    }
    

}

#endregion

