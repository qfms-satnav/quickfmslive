﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for APIIntegrationService
/// </summary>
public class APIIntegrationService
{
    HDService ser = new HDService();
    public object InsertHRMSDataService(List<APIIntegrationModel> lst,HDModel login)
    {
        try
        {
            //List<BCLLocations> BCLLocationsList = new List<BCLLocations>();
            //BCLLocations BCLLoc = new BCLLocations();
            //HDModel hd = new HDModel();
            //hd.CompanyId = "atkins";

            //string DB = _ReturnDB(hd);

            string DB =ser._ReturnDB(login);
            int cnt;
            int Expiry;
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                //lst = new List<APIIntegrationModel>();
                foreach (var main in lst)
                {
                    String proc_name = DB + "." + "UPLOAD_HRMS_DATA_API_INTEGRATION";
                    SqlParameter[] param = new SqlParameter[21];
                    param[0] = new SqlParameter("@EMP_ID", SqlDbType.VarChar);
                    param[0].Value = main.EmployeeID;
                    param[1] = new SqlParameter("@FNAME", SqlDbType.NVarChar);
                    param[1].Value = main.Name;
                    param[2] = new SqlParameter("@GENDER", SqlDbType.VarChar);
                    param[2].Value = main.Gender;
                    param[3] = new SqlParameter("@EMAIL_ID", SqlDbType.VarChar);
                    param[3].Value = main.EmailId;
                    param[4] = new SqlParameter("@SUPV_ID", SqlDbType.VarChar);
                    param[4].Value = main.ReportingManager;
                    param[5] = new SqlParameter("@DESIGNATION", SqlDbType.VarChar);
                    param[5].Value = main.Designation;
                    param[6] = new SqlParameter("@PHONE_NO", SqlDbType.VarChar);
                    param[6].Value = main.MobileNumber;
                    param[7] = new SqlParameter("@DEPARTMENT", SqlDbType.VarChar);
                    param[7].Value = main.Department;
                    param[8] = new SqlParameter("@PENTITY", SqlDbType.VarChar);
                    param[8].Value = main.ParentEntity;
                    param[9] = new SqlParameter("@CENTITY", SqlDbType.VarChar);
                    param[9].Value = main.ChildEntity;
                    param[10] = new SqlParameter("@VERTICAL", SqlDbType.VarChar);
                    param[10].Value = main.Vertical;
                    param[11] = new SqlParameter("@COSTCENTER", SqlDbType.VarChar);
                    param[11].Value = main.Costcenter;
                    param[12] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
                    param[12].Value = main.Location;
                    param[13] = new SqlParameter("@CITY", SqlDbType.VarChar);
                    param[13].Value = main.City;
                    param[14] = new SqlParameter("@COUNTRY", SqlDbType.VarChar);
                    param[14].Value = main.Country;
                    param[15] = new SqlParameter("@JOINING_DATE", SqlDbType.VarChar);
                    param[15].Value = main.JoiningDate;
                    param[16] = new SqlParameter("@EMP_TYPE", SqlDbType.VarChar);
                    param[16].Value = main.EmployeeType;
                    param[17] = new SqlParameter("@GRADE", SqlDbType.VarChar);
                    param[17].Value = main.Grade;
                    param[18] = new SqlParameter("@STATUS", SqlDbType.VarChar);
                    param[18].Value = main.Status;
                    param[19] = new SqlParameter("@TIMEZONE", SqlDbType.VarChar);
                    param[19].Value = main.Timezone;
                    param[20] = new SqlParameter("@REMARKS", SqlDbType.VarChar);
                    param[20].Value = main.Remarks;
                    DataSet ds = new DataSet();
                    ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
                  
                    //return null;
                }
                string Result;
                return null;
            }

        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }
    }
}