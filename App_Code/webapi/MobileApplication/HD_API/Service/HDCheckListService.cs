﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for HDService1
/// </summary>
public partial class HDService
{
    #region+++++++++++++++++++++++Check List Creation/Validation/Approval++++++++++++++++++++++++++++++++++++
    /// <summary>
    /// This function is used for getting API result in Drop Down, of Main Type in Check List Creation/Validation/Approval
    /// API:http://localhost:62729/api/HD/GetMainType?CompanyId=GMR
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object GetMainType(string CompanyId, string UserID)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = CompanyId;
            hDModel.UserId = UserID;
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "SP_GetBClMaintype");
            sp.Command.Parameters.Add("@User_id", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    /// <summary>
    /// This function is used for getting API result in Drop Down ,of Inspectors/Validators/Approver in Check List Creation/Validation/Approval
    /// API:http://localhost:62729/api/HD/getInspectorsValidatorsApprover
    /// //{"CompanyId":"GMR","TPM_TP_ID":1,"TPM_LCM_CODE":"BHA","TPMD_APPR_LEVELS":0}
    /// </summary>
    /// <param name="CompanyId"></param>// Same as getInspectorsZonalCentral 
    /// <returns></returns>
    //public object getInspectorsValidatorsApprover(string CompanyId,int TPM_TP_ID, string TPM_LCM_CODE, int TPMD_APPR_LEVELS)
    public object getInspectorsValidatorsApprover(InspectorsValidatorsApprover iva)
    {
        try
        {
            DataSet ds = new DataSet();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = iva.CompanyId;
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_INSPECTORSZONALCENTRAL");
            sp.Command.Parameters.Add("@TPM_TP_ID", iva.TPM_TP_ID, DbType.Int32);
            sp.Command.Parameters.Add("@TPM_LCM_CODE", iva.TPM_LCM_CODE, DbType.String);
            sp.Command.Parameters.Add("@TPMD_APPR_LEVELS", iva.TPMD_APPR_LEVELS, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }
    /// <summary>
    /// This function is used for getting API result in Drop Down ,of Inspectors/Validators/Approver in Check List Creation/Validation/Approval
    /// API:http://localhost:62729/api/HD/getInspectors?CompanyId=GMR
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object getInspectors(string CompanyId)
    {
        try
        {
            DataSet ds = new DataSet();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = CompanyId;
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_INSPECTORS");
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    /// <summary>
    /// This function is used for getting API result in Grid ,of Subcategory/score data of Inspectors/Validators/Approver in Check List Creation/Validation/Approval
    /// API:http://localhost:62729/api/HD/GetGriddata
    /// {"CompanyId":"GMR","UserId":"7777","BCL_TP_ID":1}
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object GetGriddata(GridDataCheck gd)
    {
        try
        {
            DataSet ds = new DataSet();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = gd.CompanyId;
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_CATEGORIES");
            sp.Command.Parameters.Add("@LOC_ID", "", DbType.String);
            sp.Command.Parameters.Add("@InspectedDate", "", DbType.String);
            sp.Command.Parameters.Add("@AUR_ID", gd.UserId, DbType.String);
            sp.Command.Parameters.Add("@BCL_TP_ID", gd.BCL_TP_ID, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }

    }

    /// <summary>
    /// This function is used for getting API result of Saved Data of check list of Inspectors in Check List Creation/Validation/Approval
    /// API:http://localhost:62729/api/HD/getSavedListInspector
    /// //{"CompanyId":"GMR","UserId":"7777","BCLStatus":1,"BCL_TP_ID":1}
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object getSavedListInspector(SavedList sv)
    {
        try
        {
            DataSet ds = new DataSet();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = sv.CompanyId;
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_SAVED_LIST");
            sp.Command.Parameters.Add("@AUR_ID", sv.UserId, DbType.String);
            sp.Command.Parameters.Add("@BCLStatus", sv.BCLStatus, DbType.Int32);
            sp.Command.Parameters.Add("@BCL_TP_ID", sv.BCL_TP_ID, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    /// <summary>
    /// This function is used for getting API result of Saved Data of check list of Inspectors in Check List Creation
    /// API:http://localhost:62729/api/HD/getSavedListZonalCentral
    /// //{"CompanyId":"GMR","UserId":"7777","BCLStatus":1}
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object getSavedListZonalCentral(SavedListZonalCentral svz)
    {
        try
        {
            DataSet ds = new DataSet();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = svz.CompanyId;
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_SAVED_LIST_ZONALCENTRAL");
            sp.Command.Parameters.Add("@AUR_ID", svz.UserId, DbType.String);
            sp.Command.Parameters.Add("@BCLStatus", svz.BCLStatus, DbType.Int32);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    /// <summary>
    /// This function is used for getting API result of Saved Data By ID of check list of Validators/Approver in Check List Validation/Approval
    /// API:http://localhost:62729/api/HD/getCheckedListDetail
    /// //{"CompanyId":"GMR","BCLID":1}
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object getCheckedListDetail(CheckedListDetail chk)
    {
        try
        {
            DataSet ds = new DataSet();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = chk.CompanyId;
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_CHECKLIST_DETAIL_UPDATED");
            sp.Command.Parameters.Add("@BCL_ID", chk.BCLID, DbType.Int32);
            sp.Command.Parameters.Add("@aur_id", chk.UserID, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    /// <summary>
    /// This function is used for getting API result in Drop Down of Validators/Approver Status in Grid Data in Check List Validation/Approval
    /// API:http://localhost:62729/api/HD/getZonalCentralStatus
    /// //{"CompanyId":"GMR","BCL_TP_ID":1 ,"typeName":"Validation Status"}
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object getZonalCentralStatus(ZonalCentralStatus zcs)
    {
        try
        {
            DataSet ds = new DataSet();
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = zcs.CompanyId;
            string DB = _ReturnDB(hDModel);
            sp = new SubSonic.StoredProcedure(DB + "." + "spGetZonalCentralStatus");
            sp.Command.Parameters.Add("@BCL_TP_MC_ID", zcs.BCL_TP_ID, DbType.Int32);
            sp.Command.Parameters.Add("@BCL_MC_TYPENAME", zcs.typeName, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    /// <summary>
    /// This function is used for getting API result of Deleting Check List in Check List Validation/Approval
    /// API:http://localhost:62729/api/HD/DeleteCheckList
    /// //{"CompanyId":"GMR","BCLID":1}
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object DeleteCheckList(DeleteCheck Dc)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = Dc.CompanyId;
            string DB = _ReturnDB(hDModel);
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[0].Value = Dc.BCLID;
            Object obj = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DB + "." + "BCL_DELETECHECKLIST_DETAIL", param);
            //if (obj != null)
            return new { Message = MessagesVM.UM_OK,data = "CheckList Deleted Successfully" };
            //else
            //return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }

    /// <summary>
    /// This function is used for Insert/Update check list of Inspectors/Validators/Approver in Check List Creation/Validation/Approval
    /// http://localhost:62729/api/HD/InsertCheckList?CompanyId=GMR&UserId=7777&ComId=1
    /// {"CompanyId":"GMR","UserId":"7777","InspectdBy":"Aman  Jain/AmanJain","date":"01/28/2022","SelCompany":"1","Flag":"2","OVERALL_CMTS":"OK","BCL_TYPE_ID":"1",
    ///"ReviewedBy":null,"BCL_ID":"","Revieweddate":null,"ApprovedBy":null,"Approveddate":null,
    ///"LCMLST":[{"CNY_CODE":"00966","CTY_CODE":"00966","LCM_CODE":"001","LCM_NAME":"DAMMAM/001","ticked":true}],
    //"Seldata":[
    ///   {"CatCode":"BCLC1","SubcatCode":"BCLSC1","ScoreCode":"Working","ScoreName":"","txtdata":"","Date":"01/01/1900","SubScore":"","FilePath":null
    ///,"BCLD_ZF_ACTIONS":null,"BCLD_ZF_COMENTS":null,"BCLD_CENTRAL_ACTIONS":null,"BCLD_CENTRAL_TEAM_CMTS":null}
    ///]}
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object DynamicInsertCheckList(selecteddata dataobj)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = dataobj.CompanyId;
            string DB = _ReturnDB(mo);
            List<selecteddata> HDSEM = new List<selecteddata>();
            SqlParameter[] param = new SqlParameter[18];
            param[0] = new SqlParameter("@LCM_LIST", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(dataobj.LCMLST);
            param[1] = new SqlParameter("@InspectdBy", SqlDbType.NVarChar);
            param[1].Value = dataobj.InspectdBy;
            param[2] = new SqlParameter("@date", SqlDbType.NVarChar);
            param[2].Value = dataobj.date;

            param[3] = new SqlParameter("@BCLD_REVIEWED_BY", SqlDbType.NVarChar);
            param[3].Value = dataobj.ReviewedBy;
            param[4] = new SqlParameter("@BCLD_REVIEWED_DT", SqlDbType.NVarChar);
            param[4].Value = dataobj.Revieweddate;
            param[5] = new SqlParameter("@BCLD_APPROVED_BY", SqlDbType.NVarChar);
            param[5].Value = dataobj.ApprovedBy;
            param[6] = new SqlParameter("@BCLD_APPROVED_DT", SqlDbType.NVarChar);
            param[6].Value = dataobj.Approveddate;

            param[7] = new SqlParameter("@SelCompany", SqlDbType.NVarChar);
            param[7].Value = dataobj.SelCompany;
            param[8] = new SqlParameter("@SelectedRadio", SqlDbType.Structured);
            param[8].Value = UtilityService.ConvertToDataTable(dataobj.Seldata);
            param[9] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[9].Value = dataobj.UserId;
            param[10] = new SqlParameter("@COMPANY", SqlDbType.Int);
            param[10].Value = dataobj.SelCompany;
            param[11] = new SqlParameter("@FLAG", SqlDbType.Int);
            param[11].Value = dataobj.Flag;
            param[12] = new SqlParameter("@OVER_CMTS", SqlDbType.NVarChar);
            param[12].Value = dataobj.OVERALL_CMTS;
            param[13] = new SqlParameter("@BCL_TYPE_ID", SqlDbType.Int);
            param[13].Value = dataobj.BCL_TYPE_ID;
            param[14] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[14].Value = dataobj.BCL_ID;
            param[15] = new SqlParameter("@WEBORAPP", SqlDbType.NVarChar);
            param[15].Value = "APP";
            param[16] = new SqlParameter("@LAST_SUBCAT", SqlDbType.NVarChar);
            param[16].Value = dataobj.LAST_SUBCAT;
            param[17] = new SqlParameter("@ON_BEHALF_SUBMITTED_BY", SqlDbType.NVarChar);
            param[17].Value = dataobj.ON_BEHALF_SUBMITTED_BY;
            //Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DB + "." + "[DYNAMIC_CHECKLIST]", param);
            DataTable dt = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, DB + "." + "[DYNAMIC_CHECKLIST]", param);
            //int sem_id = (int)o;
            //string msg;
            int sem_id = (int)dt.Rows[0][0];
            string msg;
            if (sem_id == 0)
            {
                msg = "Request ID : " + dt.Rows[0][1].ToString() + " Saved as Draft";
            }
            else

            {
                Space_Restrict_Seat space_Restrict = new Space_Restrict_Seat();
                space_Restrict.COMPANYID = mo.CompanyId;
                dynamic dynamicResult = GetSysPreferences(space_Restrict);
                if (dynamicResult.Message == MessagesVM.UM_OK)
                {
                    List<SysPreference> SysPrflst = dynamicResult.data;
                    if (SysPrflst.Any(d => d.SYSP_CODE == "CHECKLIST_PDF_MAIL" && d.SYSP_VAL1 == "1"))
                    {
                        GenerateCheck_Report((int)dt.Rows[0][1], mo);
                    }
                }


                msg = "Request ID : " + dt.Rows[0][1].ToString() + " Submitted Succesfully";
            }
            return new { data = msg, Message = msg };
        }
        catch (SqlException SqlExcp)
        {
            return new { data = MessagesVM.Err, Message = MessagesVM.Err };

        }
        catch (Exception ex)
        {
            return new { data = MessagesVM.Err, Message = MessagesVM.Err };
        }
    }


    /// <summary>
    /// This function is used Get Login user details.
    /// http://localhost:62729/api/HD/GetUserDetails?CompanyId=GMR&UserId=7777
    /// {"CompanyId":"GMR","UserId":"7777"}
    /// </summary>
    /// <param name="CompanyId"></param>
    /// <returns></returns>
    public object GetUserDetails(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[GET_USER_DETAILS]");
            sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, UserDetails = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, UserDetails = (object)null };

        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }
    public object NewCheckListV2(NewCheckListV2 newCheckListV2)
    {

        DataSet dataSet = new DataSet();
        HDModel hDModel = new HDModel();
        hDModel.CompanyId = newCheckListV2.CompanyId;
        string DB = _ReturnDB(hDModel);
        sp = new SubSonic.StoredProcedure(DB + "." + "BCL_GET_CATEGORIES_New_Mobile");
        sp.Command.Parameters.Add("@AUR_ID", newCheckListV2.User_ID, DbType.String);
        sp.Command.Parameters.Add("@BCL_TP_ID", newCheckListV2.BCL_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCLID", newCheckListV2.BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        List<ChecklistDetailMob> details = UtilityService.ConvertDataTableToList<ChecklistDetailMob>(ds.Tables[1]);
        List<ChecklistDataV2Mob> checklistDatas = UtilityService.ConvertDataTableToList<ChecklistDataV2Mob>(ds.Tables[0]);
        foreach (var item in checklistDatas)
        {
            foreach (var items in details)
            {
                if (item.BCL_SUB_CODE == items.BCL_CH_SUB_CODE)
                {
                    item.checklistDetails.Add(items);
                }
            }
        }
        if (checklistDatas.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = checklistDatas, Page = ds.Tables[2].Rows[0][0].ToString() };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null, Page = "" };

    }

    //public object GetChecklist_Data_Approval(int BCL_TP_ID, int BCL_ID, int BCL_SUBMIT)
    public object GetChecklist_Data_Approval(NewCheckListV2 newCheckListV2)
    {
        DataSet dataSet = new DataSet();
        HDModel hDModel = new HDModel();
        hDModel.CompanyId = newCheckListV2.CompanyId;
        string DB = _ReturnDB(hDModel);
        sp = new SubSonic.StoredProcedure(DB + "." + "[BCL_GET_CATEGORIES_Approval]");
        sp.Command.Parameters.Add("@AUR_ID", newCheckListV2.User_ID, DbType.String);
        sp.Command.Parameters.Add("@BCL_TP_ID", newCheckListV2.BCL_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCLID", newCheckListV2.BCL_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCL_SUBMIT", newCheckListV2.BCL_SUBMIT, DbType.Int32);
        ds = sp.GetDataSet();
        List<ChecklistDetail> details = UtilityService.ConvertDataTableToList<ChecklistDetail>(ds.Tables[1]);
        List<ChecklistDataV2> checklistDatas = UtilityService.ConvertDataTableToList<ChecklistDataV2>(ds.Tables[0]);
        foreach (var item in checklistDatas)
        {
            foreach (var items in details)
            {
                if (item.BCL_SUB_CODE == items.BCL_CH_SUB_CODE)
                {
                    item.checklistDetails.Add(items);
                }
            }
        }
        if (checklistDatas.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = checklistDatas };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetChecklist_Data_Validation(NewCheckListV2 newCheckListV2)
    {

        DataSet dataSet = new DataSet();
        HDModel hDModel = new HDModel();
        hDModel.CompanyId = newCheckListV2.CompanyId;
        string DB = _ReturnDB(hDModel);
        sp = new SubSonic.StoredProcedure(DB + "." + "[BCL_GET_CATEGORIES_Validation]");
        sp.Command.Parameters.Add("@AUR_ID", newCheckListV2.User_ID, DbType.String);
        sp.Command.Parameters.Add("@BCL_TP_ID", newCheckListV2.BCL_TP_ID, DbType.Int32);
        sp.Command.Parameters.Add("@BCLID", newCheckListV2.BCL_ID, DbType.Int32);
        ds = sp.GetDataSet();
        List<ChecklistDetailMob> details = UtilityService.ConvertDataTableToList<ChecklistDetailMob>(ds.Tables[1]);
        List<ChecklistDataV2Mob> checklistDatas = UtilityService.ConvertDataTableToList<ChecklistDataV2Mob>(ds.Tables[0]);
        foreach (var item in checklistDatas)
        {
            foreach (var items in details)
            {
                if (item.BCL_SUB_CODE == items.BCL_CH_SUB_CODE)
                {
                    item.checklistDetails.Add(items);
                }
            }
        }
        if (checklistDatas.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = checklistDatas };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    #endregion
    public String GetApplicationName(String ext)
    {
        switch (ext)
        {
            case "xls": return "Excel";
            case "xlsx": return "EXCELOPENXML";
            case "doc": return "Word";
            case "pdf": return "PDF";
        }
        return "PDF";
    }

    public DataSet GetCheckList_Report(SqlParameter[] param, string db)
    {
        return SqlHelper.ExecuteDataset(CommandType.StoredProcedure, db + "." + "CheckList_Report", param);
    }



    private string BindLogo(string DB)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@type", SqlDbType.VarChar);
            param[0].Value = "2";
            DataTable dataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, DB + ".Update_Get_LogoImage", param);
            if (dataTable.Rows.Count > 0)
            {
                return new Uri(HttpContext.Current.Server.MapPath(dataTable.Rows[0][1].ToString())).AbsoluteUri;
            }
            else
            {
                return new Uri(HttpContext.Current.Server.MapPath("~/BootStrapCSS/images/yourlogo.png")).AbsoluteUri;
            }
        }
        catch (Exception ex)
        {
            return new Uri(HttpContext.Current.Server.MapPath("~/BootStrapCSS/images/yourlogo.png")).AbsoluteUri;
        }

    }
    public bool GenerateCheck_Report(int id, HDModel hD)
    {
        try
        {

            string DB = _ReturnDB(hD);
            using (var viewer = new ReportViewer())
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@BCLID", SqlDbType.Int);
                param[0].Value = id;
                DataSet dataSet = GetCheckList_Report(param, DB);
                viewer.ProcessingMode = ProcessingMode.Local;
                viewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/CheckList_Mgmt/CheckList_Creator.rdlc");
                Microsoft.Reporting.WebForms.Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;
                viewer.LocalReport.DataSources.Clear();
                viewer.LocalReport.DataSources.Add(new ReportDataSource("User_Info", dataSet.Tables[0]));
                viewer.LocalReport.DataSources.Add(new ReportDataSource("CheckList_Details", dataSet.Tables[1]));
                //viewer.LocalReport.DataSources.Add(new ReportDataSource("RatingColor", dataSet.Tables[5]));
                viewer.LocalReport.DataSources.Add(new ReportDataSource("CheckList_User", dataSet.Tables[2]));
                //viewer.LocalReport.DataSources.Add(new ReportDataSource("CheckList_File", dataSet.Tables[7]));
                viewer.LocalReport.EnableHyperlinks = true;
                viewer.LocalReport.EnableExternalImages = true;
                viewer.LocalReport.SetParameters(new ReportParameter("ImagePath", BindLogo(DB)));
                viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                viewer.LocalReport.Refresh();
                String apptype = GetApplicationName("pdf");
                byte[] bytes = viewer.LocalReport.Render(
                    apptype, null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);
                //string filePath = HttpContext.Current.Server.MapPath("~/Reports_RDLC/Report_Output/" + "Branch_Report_" + DateTime.Now.ToString("MMddyyyy") + ".pdf");
                //using (FileStream fs = new FileStream(filePath, FileMode.Create))
                //{
                //    fs.Write(bytes, 0, bytes.Length);
                //}
                using (SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["Host"], Convert.ToInt16(System.Configuration.ConfigurationManager.AppSettings["Port"])))
                {
                    smtp.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailid"].ToString(), System.Configuration.ConfigurationManager.AppSettings["password"].ToString());
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                    using (MailMessage msg = new MailMessage())
                    {
                        msg.Subject = dataSet.Tables[3].Rows[0][3].ToString();
                        msg.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["from"].ToString());
                        foreach (var tomail in dataSet.Tables[3].Rows[0][1].ToString().Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(address => new MailAddress(address.Trim())).Where(mailTo => !msg.To.Contains(mailTo)))
                        {
                            msg.To.Add(tomail);
                        }
                        foreach (var ccmail in dataSet.Tables[3].Rows[0][2].ToString().Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(address => new MailAddress(address.Trim())).Where(mailCc => !msg.CC.Contains(mailCc) && !msg.To.Contains(mailCc)))
                        {
                            msg.CC.Add(ccmail);
                        }
                        msg.IsBodyHtml = true;
                        msg.Body = dataSet.Tables[3].Rows[0][0].ToString();
                        msg.Attachments.Add(new Attachment(new MemoryStream(bytes), "Branch_Visit_Report_" + DateTime.Now.ToString("MMddyyyy") + ".pdf"));
                        smtp.Send(msg);
                    }
                }
            };
            return true;
        }
        catch (Exception ex)
        {

            //throw;
            return false;
        }

    }
    public object BindDesignation(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[GET_EMP_DESIGNATION_MOB]");
            //sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
            sp.Command.Parameters.Add("@SEARCH", hDModel.searchValue, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, UserDetails = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, UserDetails = (object)null };

        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }
    }
    // Schedule visit
    public object GetScheduleData(HDModel hDModel)
    {
        try
        {
            string DB = _ReturnDB(hDModel);
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DB + "." + "[GET_SCHEDULE_DETAILS]");
            sp.Command.Parameters.Add("@AUR_ID", hDModel.UserId, DbType.String);
            ds = sp.GetDataSet();
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, UserDetails = ds.Tables[0] };
            else
                return new { Message = MessagesVM.UM_NO_REC, UserDetails = (object)null };

        }
        catch (SqlException SqlExcp)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(SqlExcp); return MessagesVM.Err;
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._OtherExceptionsInDBErrorLog(ex); return (string)null;
        }

    }

    public object CreateScheduleVisits(CreateScheduleVisits CSV)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = CSV.Companyid;
            string DB = _ReturnDB(mo);
            String proc_name = DB + ".SUBMIT_SCHEDULE_VISITS";
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@ScheduleVisitsList", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(CSV.ScheduleVisits);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = CSV.UserId;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_Not_OK, result = ds };
        }

        catch (Exception E)
        {
            return new { Message = MessagesVM.UM_Not_OK, result = "Something Went Wrong" };
        }


    }

    public object ModifyScheduleVisits(ViewAndModifyScheduleModel VAMmodel)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = VAMmodel.Companyid;
            string DB = _ReturnDB(mo);
            String proc_name = DB + ".MODIFY_SCHEDULE_VISITS";
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@ScheduleVisitsList", SqlDbType.Structured);
            param[0].Value = UtilityService.ConvertToDataTable(VAMmodel.ModifyScheduleVisitsList);
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = VAMmodel.UserId;
            param[2] = new SqlParameter("@BCLDS_ID", SqlDbType.Int);
            param[2].Value = VAMmodel.BCLDS_ID;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_Not_OK, result = ds };
        }

        catch (Exception E)
        {
            return new { Message = MessagesVM.UM_Not_OK, result = "Something Went Wrong" };
        }
    }


    public object DeleteMySchedule(ViewAndModifyScheduleModel VAMmodel)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = VAMmodel.Companyid;
            string DB = _ReturnDB(mo);
            String proc_name = DB + ".DELETE_MY_SCHEDULE";
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@BCLDS_ID", SqlDbType.Int);
            param[0].Value = VAMmodel.BCLDS_ID;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            if (ds.Tables.Count > 0)
                return new { Message = MessagesVM.UM_OK, result = ds };
            else
                return new { Message = MessagesVM.UM_Not_OK, result = ds };
        }

        catch (Exception E)
        {
            return new { Message = MessagesVM.UM_Not_OK, result = "Something Went Wrong" };
        }


    }

    public object GetLocationByCity_CheckList(NewCheckListV2_For_Location CSV)
    {
        try
        {
            HDModel hDModel = new HDModel();
            hDModel.CompanyId = CSV.CompanyId;
            string DB = _ReturnDB(hDModel);
            List<NewCheckListV2_For_Location> CheckListLOCs = new List<NewCheckListV2_For_Location>();
            sp = new SubSonic.StoredProcedure(DB + "." + "[GET_LOCATION_BY_CITY_FOR_CHECKLIST]");
            sp.Command.AddParameter("@AUR_ID", CSV.User_ID, DbType.String);
            sp.Command.AddParameter("@CITY_CODE", CSV.CTY_CODE, DbType.String);
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    NewCheckListV2_For_Location newCheckListV2 = new NewCheckListV2_For_Location();
                    newCheckListV2.CTY_CODE = sdr["CTY_CODE"].ToString();
                    newCheckListV2.LCM_CODE = sdr["LCM_CODE"].ToString();
                    newCheckListV2.LCM_NAME = sdr["LCM_NAME"].ToString();
                    newCheckListV2.LAT = sdr["LAT"].ToString();
                    newCheckListV2.LONG = sdr["LONG"].ToString();
                    CheckListLOCs.Add(newCheckListV2);
                }
            }
            if (CheckListLOCs.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = CheckListLOCs };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }

        catch (Exception E)
        {
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }


    }
    public object LocationCords(LocationCords LCM_CORD)
    {

        DataSet ds = new DataSet();
        HDModel hDModel = new HDModel();
        hDModel.CompanyId = LCM_CORD.CompanyId;
        string DB = _ReturnDB(hDModel);
        sp = new SubSonic.StoredProcedure(DB + "." + "GET_LOCATION_COORDINATES");
        sp.Command.Parameters.Add("@AUR_ID", LCM_CORD.User_ID, DbType.String);
        sp.Command.Parameters.Add("@LCM_CODE", LCM_CORD.LCM_CODE, DbType.String);
        sp.Command.Parameters.Add("@BCL_TYPE", LCM_CORD.BCL_TYPE, DbType.String);
        ds = sp.GetDataSet();

        if (ds.Tables.Count > 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null, Page = "" };

    }
    public object DeleteDraftBCL(DeleteCheck DDrfModel)
    {
        try
        {
            HDModel mo = new HDModel();
            mo.CompanyId = DDrfModel.CompanyId;
            string DB = _ReturnDB(mo);
            String proc_name = DB + ".DELETE_DRAFT_BCL";
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@BCL_ID", SqlDbType.Int);
            param[0].Value = DDrfModel.BCLID;
            param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            param[1].Value = DDrfModel.userId;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, proc_name, param);
            if ((int)ds.Tables[0].Rows[0][0] == 1)
                return new { Message = MessagesVM.UM_OK, result = "Deleted " + DDrfModel.BCLID.ToString() + " successfully" };
            else
                return new { Message = MessagesVM.UM_Not_OK, result = "Something Went Wrong" };
        }

        catch (Exception E)
        {
            return new { Message = MessagesVM.UM_Not_OK, result = "Something Went Wrong" };
        }


    }
}