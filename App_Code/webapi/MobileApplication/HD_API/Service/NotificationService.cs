﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using UtiltiyVM;

/// <summary>
/// Summary description for NotificationService
/// </summary>
public class NotificationService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;
    string DatabaseURL;
    HDService hDService = new HDService();
    public NotificationService()
    {
    }


    public object getDeviceIds(HDModel hDModel)
    {
        try
        {
            string DB = hDService._ReturnDB(hDModel);
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                List<string> androidNotificationIds = new List<string>();
                List<string> iosNotificationIDs = new List<string>();
                sp = new SubSonic.StoredProcedure(DB + "." + "[GetDeviceIds]");
                sp.Command.AddParameter("@LCM_CODE", hDModel.code, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["ANDROID_ID"].ToString() != "")
                        {
                            androidNotificationIds.Add(sdr["ANDROID_ID"].ToString());
                        }
                        if (sdr["IOS_ID"].ToString() != "")
                        {
                            androidNotificationIds.Add(sdr["IOS_ID"].ToString());
                        }
                    }
                }
                sendNotification(androidNotificationIds, hDModel.Password, hDModel.Role_id, DB);
                if (androidNotificationIds.Count > 0 || iosNotificationIDs.Count > 0)
                    return new { Message = MessagesVM.UM_OK, AndroidList = androidNotificationIds, IosList = iosNotificationIDs };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }

    }

    public object GetDeviceIdforIncharge(string DB,string UserID,string Title,string Message)
    {
        try
        {
            if (DB == null)
            {
                return new { Message = MessagesVM.InvalidCompany, data = (object)null };
            }
            else
            {
                List<string> androidNotificationIds = new List<string>();
                List<string> iosNotificationIDs = new List<string>();
                sp = new SubSonic.StoredProcedure(DB + "." + "[GetDeviceIdsOnUserID]");
                sp.Command.AddParameter("@user", UserID, DbType.String);
                using (IDataReader sdr = sp.GetReader())
                {
                    while (sdr.Read())
                    {
                        if (sdr["ANDROID_ID"].ToString() != "")
                        {
                            androidNotificationIds.Add(sdr["ANDROID_ID"].ToString());
                        }
                        if (sdr["IOS_ID"].ToString() != "")
                        {
                            androidNotificationIds.Add(sdr["IOS_ID"].ToString());
                        }
                    }
                }
                sendNotification(androidNotificationIds, Title, Message, DB);
                if (androidNotificationIds.Count > 0 || iosNotificationIDs.Count > 0)
                    return new { Message = MessagesVM.UM_OK, AndroidList = androidNotificationIds, IosList = iosNotificationIDs };
                else
                    return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
            }
        }
        catch (Exception ex)
        {
            ErrorHandler erhndlr = new ErrorHandler();
            erhndlr._WriteErrorLog(ex); return new { Status = "false", Message = ex.Message, data = (object)null };
        }

    }

    public void sendNotification(List<string> DeviceIDs, string Title, string Message,string DB)
    {
        try
        {
            if (DeviceIDs.Count > 0)
            {
                var applicationID = "AAAAsTYGRc8:APA91bGy6BgqXXgOzkV81uUUbTvVWvYjX-7cZpvXzwW8pLvujQB6AcgeFJH6pIPUYJjKQfBymEXDoWO5GGAzJ03Qy45aQoQto_MdEfvSsAQhahqVmly449aeF_9tjALggTM5bt-pRGIV";

                var senderId = "761115592143";

                //string deviceId = "euxqdp------ioIdL87abVL";
                foreach (var item in DeviceIDs)
                {
                    WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");

                    tRequest.Method = "post";

                    tRequest.ContentType = "application/json";

                    var data = new

                    {

                        to = item,

                        notification = new

                        {

                            body = Message,

                            title = Title,

                            //aps = new
                            //{
                            //    alert = new
                            //    {

                            //        title = "Test Visible Russian Title",
                            //        body = "Test Visible Russian Title",
                            //        sound = "default"
                            //    }
                            //}

                        }

                    };

                    var serializer = new JavaScriptSerializer();

                    var json = serializer.Serialize(data);

                    Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                    tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                    tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                    tRequest.ContentLength = byteArray.Length;


                    using (Stream dataStream = tRequest.GetRequestStream())
                    {

                        dataStream.Write(byteArray, 0, byteArray.Length);


                        using (WebResponse tResponse = tRequest.GetResponse())
                        {

                            using (Stream dataStreamResponse = tResponse.GetResponseStream())
                            {

                                using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {

                                    String sResponseFromServer = tReader.ReadToEnd();

                                    string str = sResponseFromServer;
                                    sp = new SubSonic.StoredProcedure(DB + "." + "INSERT_NOTIFICATIONS_DETAILS");
                                    sp.Command.AddParameter("@User_ID", "", DbType.String);
                                    sp.Command.AddParameter("@TYPE", "ANDROID", DbType.String);
                                    sp.Command.AddParameter("@DEVICE_ID", item, DbType.String);
                                    sp.Command.AddParameter("@TOCKEN", "ANDROID", DbType.String);
                                    sp.Command.AddParameter("@DESCRIPTION", Message, DbType.String);
                                    sp.Command.AddParameter("@REMARKS", Title, DbType.String);
                                    sp.ExecuteScalar();

                                }
                            }
                        }
                    }
                }
            }

        }

        catch (Exception ex)
        {

            string str = ex.Message;

        }

    }
}