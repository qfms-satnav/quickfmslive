﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using Newtonsoft.Json;

public class EditParcelService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public object GetParcelReqid()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_PARCEL_REQID");
        sp.Command.Parameters.Add("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();

        return ds.Tables[0];
    }
   

    public IEnumerable<EDSaveformData> GetParcelDetails(string ReqId)
    {
        List<EDSaveformData> GetformReqId = new List<EDSaveformData>();

        try
        {
            // Prepare the stored procedure
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_PARCEL_DETAILS_BY_REQID");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@REQID", ReqId, DbType.String);

            // Execute the query and get the reader
            using (IDataReader sdr = sp.GetReader())
            {
                EDSaveformData currentParcel = null;

                // Loop through the data reader
                while (sdr.Read())
                {
                    currentParcel = new EDSaveformData()
                    {
                        ReqId = sdr["REQID"].ToString(),
                        todate = Convert.ToDateTime(sdr["BOOKING_DT"]),

                        // Sender details
                        SenderName = sdr["SENDER_NAME"].ToString(),
                        SenderId = sdr["SENDER_ID"].ToString(),
                        SenderContactno = sdr["SENDER_CONTACT_NO"].ToString(),
                        SENDEREMAIL = sdr["SENDER_EMAIL"].ToString(),
                        SenderDepartment = sdr["SENDER_DEPT"].ToString(),
                        senderLocation = sdr["SENDER_LOCATION"].ToString(),
                        SENDERCITY = sdr["SENDER_CITY"].ToString(),
                        SENDERCOUNTRY = sdr["SENDER_COUNTRY"].ToString(),
                        SENDERPINCODE = sdr["SENDER_PINCODE"].ToString(),

                        // Recipient details
                        RecipientName = sdr["RECV_NAME"].ToString(),
                        RecipientId = sdr["RECV_ID"].ToString(),
                        RecipientContactno = sdr["DESTINATION_CONTACT_NO"].ToString(),
                        RecipientEmail = sdr["DESTINATION_EMAIL"].ToString(),
                        RecipientDepartment = sdr["DESTINATION_DEPT"].ToString(),
                        RecipientLocation = sdr["DESTINATION_LOCATION"].ToString(),
                        RecipientCity = sdr["DESTINATION_CITY"].ToString(),
                        RecipientCountry = sdr["DESTINATION_COUNTRY"].ToString(),
                        RecipientPincode = sdr["DESTINATION_PINCODE"].ToString(),

                        // Parcel details
                        CourierAgency = sdr["PARCEL_AGENCY"].ToString(),
                        AWBPODNO = sdr["PARCEL_AWB_NO"].ToString(),
                        Mode = sdr["PARCEL_MODE"].ToString(),
                        ParcelCategory = sdr["PARCEL_CATEGORY"].ToString(),
                        ParcelDetails = sdr["PARCEL_TYPE"].ToString(),
                        INVOICENO = sdr["INVOICE_NO"].ToString(),
                        InvoiceAMOUNT = sdr["INVOICE_AMOUNT"].ToString(),
                        Weight = sdr["PARCEL_WEIGHT"].ToString(),
                        NOOFPIECES = sdr["Item_Count"].ToString(),
                        DeliveryRemarks = sdr["DELIVERY_NOTE_REMARKS"].ToString()
                    };

                    // Add the current parcel to the list
                    GetformReqId.Add(currentParcel);
                }

                // If there are document details to read
                if (sdr.NextResult() && currentParcel != null)
                {
                    while (sdr.Read())
                    {
                        // Add documents related to the current parcel
                        currentParcel.Documents.Add(new EDMailDoc
                        {
                            DocReqId = sdr["MLRM_DOC_REQID"].ToString(),
                            DocName = sdr["MLRM_DOC_NAME"].ToString(),
                            DocPath = sdr["MLRM_DOC_PATH"].ToString()
                        });
                    }
                }

                sdr.Close();
            }
        }
        catch (Exception ex)
        {
            // Handle exception (log or rethrow)
            // Example logging:
            // Logger.LogError("Error in GetParcelDetails method: " + ex.Message);
            throw new ApplicationException("An error occurred while retrieving parcel details.", ex);
        }

        // Return the list of parcels
        return GetformReqId;
    }

    //public IEnumerable<Status_VM> GetStatus()
    //{
    //    List<Status_VM> Status = new List<Status_VM>();
    //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Mail_Room_Status");
    //    using (IDataReader sdr = sp.GetReader())
    //    {
    //        while (sdr.Read())
    //        {
    //            Status.Add(new Status_VM()
    //            {
    //                STA_CODE = sdr.GetValue(0).ToString(),
    //                STA_TITLE = sdr.GetValue(1).ToString(),
    //                ticked = false
                   
    //            });
                
                
    //        }

    //        sdr.Close();
    //        return Status;
    //    }
    //}
    public IEnumerable<LocationParcel> BindLocation()
    {
        List<LocationParcel> LocationList = new List<LocationParcel>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UTILITY_LOCATION");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                LocationList.Add(new LocationParcel()
                {
                    LCM_CODE = sdr.GetValue(0).ToString(),
                    LCM_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return LocationList;
        }
    }
    public IEnumerable<User> BindUser()
    {
        List<User> UserList = new List<User>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_USER_LIST");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                UserList.Add(new User()
                {
                    AUR_ID = sdr.GetValue(0).ToString(),
                    AUR_KNOWN_AS = sdr.GetValue(1).ToString(),
                    AUR_DEP_ID = sdr.GetValue(2).ToString(),
                    AUR_RES_NUMBER = sdr.GetValue(3).ToString(),
                    AUR_EMAIL = sdr.GetValue(4).ToString(),
                    CTY_NAME = sdr.GetValue(5).ToString(),
                    LCM_PINCODE = sdr.GetValue(6).ToString(),
                    CNY_NAME = sdr.GetValue(7).ToString(),
                    LCM_CODE = sdr.GetValue(8).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return UserList;
        }
    }
    public IEnumerable<CourierAgency> BindCourierAgency()
    {
        List<CourierAgency> CourierAgencyList = new List<CourierAgency>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_COURIER_AGENCY");
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                CourierAgencyList.Add(new CourierAgency()
                {
                    COURIER_CODE = sdr.GetValue(0).ToString(),
                    COURIER_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return CourierAgencyList;
        }
    }
    public IEnumerable<CourierMode> BindCourierMode()
    {
        List<CourierMode> CourierModeList = new List<CourierMode>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_COURIER_MODE");
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                CourierModeList.Add(new CourierMode()
                {
                    COURIERMODE_CODE = sdr.GetValue(0).ToString(),
                    COURIERMODE_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return CourierModeList;
        }
    }
    public IEnumerable<ParcelCategory> BindCourierCategory()
    {
        List<ParcelCategory> CourierCategoryist = new List<ParcelCategory>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_COURIER_CATEGORY");
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                CourierCategoryist.Add(new ParcelCategory()
                {
                    PARCEL_CODE = sdr.GetValue(0).ToString(),
                    PARCEL_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return CourierCategoryist;
        }
    }
    public IEnumerable<ParcelDetails> BindCourierDetails()
    {
        List<ParcelDetails> CourierDetailsList = new List<ParcelDetails>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_COURIER_DETAILS");
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                CourierDetailsList.Add(new ParcelDetails()
                {
                    COURIERDETAILS_CODE = sdr.GetValue(0).ToString(),
                    COURIERDETAILS_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return CourierDetailsList;
        }
    }
    public object SaveData(SaveformData Save)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[29];
            param[0] = new SqlParameter("@Requestid", SqlDbType.NVarChar);
            param[0].Value = Save.BRNCODE;
            param[1] = new SqlParameter("@BookingDate", SqlDbType.DateTime);
            param[1].Value = Save.todate;
            param[2] = new SqlParameter("@SenderLocation", SqlDbType.NVarChar);
            param[2].Value = Save.senderLocation;
            param[3] = new SqlParameter("@SenderName", SqlDbType.NVarChar);
            param[3].Value = Save.SenderName;
            param[4] = new SqlParameter("@SenderDepartment", SqlDbType.NVarChar);
            param[4].Value = Save.SenderDepartment;
            param[5] = new SqlParameter("@SenderContactno", SqlDbType.NVarChar);
            param[5].Value = Save.SenderContactno;
            param[6] = new SqlParameter("@SENDEREMAIL", SqlDbType.NVarChar);
            param[6].Value = Save.SENDEREMAIL;
            param[7] = new SqlParameter("@SENDERCITY", SqlDbType.NVarChar);
            param[7].Value = Save.SENDERCITY;
            param[8] = new SqlParameter("@SENDERPINCODE", SqlDbType.NVarChar);
            param[8].Value = Save.SENDERPINCODE;
            param[9] = new SqlParameter("@SENDERCOUNTRY", SqlDbType.NVarChar);
            param[9].Value = Save.SENDERCOUNTRY;
            param[10] = new SqlParameter("@RecipientLocation", SqlDbType.NVarChar);
            param[10].Value = Save.RecipientLocation;
            param[11] = new SqlParameter("@RecipientName", SqlDbType.NVarChar);
            param[11].Value = Save.RecipientName;
            param[12] = new SqlParameter("@RecipientDepartment", SqlDbType.NVarChar);
            param[12].Value = Save.RecipientDepartment;
            param[13] = new SqlParameter("@RecipientContactno", SqlDbType.NVarChar);
            param[13].Value = Save.RecipientContactno;
            param[14] = new SqlParameter("@RecipientEmail", SqlDbType.NVarChar);
            param[14].Value = Save.RecipientEmail;
            param[15] = new SqlParameter("@RecipientCity", SqlDbType.NVarChar);
            param[15].Value = Save.RecipientCity;
            param[16] = new SqlParameter("@RecipientPincode", SqlDbType.NVarChar);
            param[16].Value = Save.RecipientPincode;
            param[17] = new SqlParameter("@RecipientCountry", SqlDbType.NVarChar);
            param[17].Value = Save.RecipientCountry;
            param[18] = new SqlParameter("@CourierAgency", SqlDbType.NVarChar);
            param[18].Value = Save.CourierAgency;
            param[19] = new SqlParameter("@AWBPODNO", SqlDbType.NVarChar);
            param[19].Value = Save.AWBPODNO;
            param[20] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            param[20].Value = Save.Mode;
            param[21] = new SqlParameter("@ParcelCategory", SqlDbType.NVarChar);
            param[21].Value = Save.ParcelCategory;
            param[22] = new SqlParameter("@ParcelDetails", SqlDbType.NVarChar);
            param[22].Value = Save.ParcelDetails;
            param[23] = new SqlParameter("@INVOICENO", SqlDbType.NVarChar);
            param[23].Value = Save.INVOICENO;
            param[24] = new SqlParameter("@InvoiceAMOUNT", SqlDbType.NVarChar);
            param[24].Value = Save.InvoiceAMOUNT;
            param[25] = new SqlParameter("@Weight", SqlDbType.NVarChar);
            param[25].Value = Save.Weight;
            param[26] = new SqlParameter("@NOOFPIECES", SqlDbType.NVarChar);
            param[26].Value = Save.NOOFPIECES;
            param[27] = new SqlParameter("@DeliveryRemarks", SqlDbType.NVarChar);
            param[27].Value = Save.DeliveryRemarks;
            param[28] =new SqlParameter("@CreatedBy", SqlDbType.NVarChar);
            param[28].Value = HttpContext.Current.Session["UID"];



            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "INSERT_COURIER_DATA", param))

            {
                if (dr.Read())
                {
                    return new { Message = "Data Inserted Successfully", Data = Save };
                }
            }

            return new { Message = "Data Inserted Successfully", Data = Save, AuthToken = (string)null };
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }



    //public dynamic UploadTemplate(HttpRequest httpRequest)
    //{
    //    GetDocumentDetailsVM FDT;
    //    List<GetDocumentDetailsVM> FD = new List<GetDocumentDetailsVM>();
    //    if (httpRequest.Files.Count > 0)
    //    {
    //        String login = httpRequest.Params["login"];
    //        String typdoc = httpRequest.Params["CurrObj"];

    //        var obj = FileValidator.SaveImageFile(httpRequest.Files[0], "Contract_Doc");
    //        FDT = new GetDocumentDetailsVM();
    //        FDT.DFDC_DDT_CODE = typdoc;
    //        FDT.DFDC_ACT_NAME = obj.ImageName;
    //        FDT.DFDC_NAME = obj.filePathWithFolder;
    //        FD.Add(FDT);
    //        // }
    //        return new { Message = MessagesVM.UAD_UPLNOREC, data = FD };
    //    }
    //    else
    //        return new { Message = MessagesVM.UAD_UPLNOREC, data = (object)null };
    //}

}