﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using UtiltiyVM;


/// <summary>
/// Summary description for ParcelMasterService
/// </summary>
public class ParcelMasterService
{
    SubSonic.StoredProcedure sp;
    public int SaveParcelDetails(ParcelMasterModel BicModel)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[INSERT_PARCEL_DETAILS]");
            sp.Command.AddParameter("@PAR_CODE", BicModel.PAR_CODE, DbType.String);
            sp.Command.AddParameter("@PAR_NAME", BicModel.PAR_NAME, DbType.String);
            sp.Command.AddParameter("@BICM_STA_ID", BicModel.BICM_STA_ID, DbType.String);
            sp.Command.AddParameter("@BICM_CREATED_BY", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }
    public IEnumerable<ParcelMasterModel> BindGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_PARCEL_GRID]").GetReader())
        {
            try
            {
                List<ParcelMasterModel> BicModel = new List<ParcelMasterModel>();
                while (reader.Read())
                {
                    BicModel.Add(new ParcelMasterModel()
                    {
                        PAR_CODE = reader.GetValue(0).ToString(),
                        PAR_NAME = reader.GetValue(1).ToString(),
                        BICM_STA_ID = reader.GetValue(2).ToString(),
                    });
                }
                reader.Close();
                return BicModel;
            }
            catch
            {
                throw;
            }
        }

    }


   
    public void ModifyParcelDetails(ParcelMasterModel BicModel)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[MODIFY_PARCEL_DETAILS]");
            sp.Command.AddParameter("@PAR_CODE", BicModel.PAR_CODE, DbType.String);
            sp.Command.AddParameter("@PAR_NAME", BicModel.PAR_NAME, DbType.String);
            sp.Command.AddParameter("@BICM_STA_ID", BicModel.BICM_STA_ID, DbType.String);
            sp.Command.AddParameter("@BICM_UPDATED_BY", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }
    public int SaveCourierAgencyDetails(CourierAgencyMasterModel BicModel)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[INSERT_COURIERAGENCY_DETAILS]");
            sp.Command.AddParameter("@CAR_CODE", BicModel.CAR_CODE, DbType.String);
            sp.Command.AddParameter("@CAR_NAME", BicModel.CAR_NAME, DbType.String);
            sp.Command.AddParameter("@BICM_STA_ID", BicModel.BICM_STA_ID, DbType.String);
            sp.Command.AddParameter("@BICM_CREATED_BY", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            int flag = (int)sp.ExecuteScalar();
            return flag;
        }
        catch
        {
            throw;
        }
    }
    public IEnumerable<CourierAgencyMasterModel> GetCourierAgencyDetailsGridData()
    {
        using (IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[BIND_COURIER_GRID]").GetReader())
        {
            try
            {
                List<CourierAgencyMasterModel> BicModel = new List<CourierAgencyMasterModel>();
                while (reader.Read())
                {
                    BicModel.Add(new CourierAgencyMasterModel()
                    {
                        CAR_CODE = reader.GetValue(0).ToString(),
                        CAR_NAME = reader.GetValue(1).ToString(),
                        BICM_STA_ID = reader.GetValue(2).ToString(),
                    });
                }
                reader.Close();
                return BicModel;
            }
            catch
            {
                throw;
            }
        }

    }



    public void ModifyCourierAgencyDetails(CourierAgencyMasterModel BicModel)
    {
        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[MODIFY_COURIER_DETAILS]");
            sp.Command.AddParameter("@CAR_CODE", BicModel.CAR_CODE, DbType.String);
            sp.Command.AddParameter("@CAR_NAME", BicModel.CAR_NAME, DbType.String);
            sp.Command.AddParameter("@BICM_STA_ID", BicModel.BICM_STA_ID, DbType.String);
            sp.Command.AddParameter("@BICM_UPDATED_BY", HttpContext.Current.Session["UID"].ToString(), DbType.String);
            sp.ExecuteScalar();
        }
        catch
        {
            throw;
        }
    }
}