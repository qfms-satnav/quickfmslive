﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using UtiltiyVM;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using Newtonsoft.Json;

public class BookParcelService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public IEnumerable<BookingDetails> GetGridData()
    {
        try
        {

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar)
            {
                Value = HttpContext.Current.Session["UID"]
            };


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "Edit_Parcel_Bind_Grid", param))
            {
                List<BookingDetails> resrvlist = new List<BookingDetails>();
                while (reader.Read())
                {
                    resrvlist.Add(new BookingDetails
                    {
                        DFDC_ACT_NAME = reader["DFDC_ACT_NAME"].ToString(),
                        MLRMD_AWB_NO = reader["MLRMD_AWB_NO"].ToString(),
                        SENDER_DEPT = reader["SENDER_DEPT"].ToString(),
                        SEND_LOC = reader["SEND_LOC"].ToString(),
                        DES_DEP = reader["DES_DEP"].ToString(),
                        DES_LOC = reader["DES_LOC"].ToString(),
                        PARCEL_AWB_NO = reader["PARCEL_AWB_NO"].ToString(),
                        COURIER_NAME = reader["COURIER_NAME"].ToString(),
                        STA_TITLE = reader["STA_TITLE"].ToString()
                    });
                }
                return resrvlist;
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException("Error fetching grid data", ex);
        }
    }

    public IEnumerable<BookingDetails> Assign_Courier_GetGridData()
    {
        try
        {

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar)
            {
                Value = HttpContext.Current.Session["UID"]
            };
            

            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "Parcel_Bind_Grid", param))
            {
                List<BookingDetails> resrvlist = new List<BookingDetails>();
                while (reader.Read())
                {
                    resrvlist.Add(new BookingDetails
                    {
                        DFDC_ACT_NAME = reader["DFDC_ACT_NAME"].ToString(),
                        MLRMD_AWB_NO = reader["MLRMD_AWB_NO"].ToString(),
                        SENT_DATE = reader["SENT_DATE"].ToString(),
                        RECIPTENT = reader["RECIPTENT"].ToString(),
                        Parcel_Type = reader["Parcel_Type"].ToString(),
                        DES_LOC = reader["DES_LOC"].ToString(),
                        PARCEL_AWB_NO = reader["PARCEL_AWB_NO"].ToString(),
                        COURIER_NAME = reader["COURIER_NAME"].ToString(),
                        STA_TITLE = reader["STA_TITLE"].ToString()
                    });
                }
                return resrvlist;
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException("Error fetching grid data", ex);
        }
    }
    public object Getrefnum()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_AUTO_GENERATED_ID");
        sp.Command.Parameters.Add("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();

        return ds.Tables[0].Rows[0]["RefNum"];
    }
    public IEnumerable<LocationParcel> BindLocation()
    {
        List<LocationParcel> LocationList = new List<LocationParcel>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_UTILITY_LOCATION");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                LocationList.Add(new LocationParcel()
                {
                    LCM_CODE = sdr.GetValue(0).ToString(),
                    LCM_NAME = sdr.GetValue(1).ToString(),
                    Destination_LCM_CODE = sdr.GetValue(2).ToString(),
                    Destination_LCM_NAME = sdr.GetValue(3).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return LocationList;
        }
    }
    public IEnumerable<User> BindUser()
    {
        List<User> UserList = new List<User>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_USER_LIST");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                UserList.Add(new User()
                {
                    AUR_ID = sdr.GetValue(0).ToString(),
                    AUR_KNOWN_AS = sdr.GetValue(1).ToString(),
                    AUR_DEP_ID = sdr.GetValue(2).ToString(),
                    AUR_RES_NUMBER = sdr.GetValue(3).ToString(),
                    AUR_EMAIL = sdr.GetValue(4).ToString(),
                    CTY_NAME = sdr.GetValue(5).ToString(),
                    LCM_PINCODE = sdr.GetValue(6).ToString(),
                    CNY_NAME = sdr.GetValue(7).ToString(),
                    LCM_CODE = sdr.GetValue(8).ToString(),
                    Destination_AUR_ID = sdr.GetValue(0).ToString(),
                    Destination_AUR_KNOWN_AS = sdr.GetValue(1).ToString(),
                    Destination_AUR_DEP_ID = sdr.GetValue(2).ToString(),
                    Destination_AUR_RES_NUMBER = sdr.GetValue(3).ToString(),
                    Destination_AUR_EMAIL = sdr.GetValue(4).ToString(),
                    Destination_CTY_NAME = sdr.GetValue(5).ToString(),
                    Destination_LCM_PINCODE = sdr.GetValue(6).ToString(),
                    Destination_CNY_NAME = sdr.GetValue(7).ToString(),
                    Destination_LCM_CODE = sdr.GetValue(8).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return UserList;
        }
    }
    public IEnumerable<CourierAgency> BindCourierAgency()
    {
        List<CourierAgency> CourierAgencyList = new List<CourierAgency>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_COURIER_AGENCY");
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                CourierAgencyList.Add(new CourierAgency()
                {
                    COURIER_CODE = sdr.GetValue(0).ToString(),
                    COURIER_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return CourierAgencyList;
        }
    }
    public IEnumerable<CourierMode> BindCourierMode()
    {
        List<CourierMode> CourierModeList = new List<CourierMode>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_COURIER_MODE");
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                CourierModeList.Add(new CourierMode()
                {
                    COURIERMODE_CODE = sdr.GetValue(0).ToString(),
                    COURIERMODE_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return CourierModeList;
        }
    }
    public IEnumerable<ParcelCategory> BindCourierCategory()
    {
        List<ParcelCategory> CourierCategoryist = new List<ParcelCategory>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_COURIER_CATEGORY");
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                CourierCategoryist.Add(new ParcelCategory()
                {
                    PARCEL_CODE = sdr.GetValue(0).ToString(),
                    PARCEL_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return CourierCategoryist;
        }
    }
    public IEnumerable<ParcelDetails> BindCourierDetails()
    {
        List<ParcelDetails> CourierDetailsList = new List<ParcelDetails>();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "BIND_COURIER_DETAILS");
        using (IDataReader sdr = sp.GetReader())
        {

            while (sdr.Read())
            {

                CourierDetailsList.Add(new ParcelDetails()
                {
                    COURIERDETAILS_CODE = sdr.GetValue(0).ToString(),
                    COURIERDETAILS_NAME = sdr.GetValue(1).ToString(),
                    ticked = false
                });
            }

            sdr.Close();
            return CourierDetailsList;
        }
    }
    public object SaveData(SaveformData Save)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[27];
            param[0] = new SqlParameter("@Requestid", SqlDbType.NVarChar);
            param[0].Value = Save.BRNCODE;
            param[1] = new SqlParameter("@BookingDate", SqlDbType.DateTime);
            param[1].Value = Save.todate;
            param[2] = new SqlParameter("@SenderLocation", SqlDbType.NVarChar);
            param[2].Value = Save.senderLocation;
            param[3] = new SqlParameter("@SenderName", SqlDbType.NVarChar);
            param[3].Value = Save.SenderName;
            param[4] = new SqlParameter("@SenderDepartment", SqlDbType.NVarChar);
            param[4].Value = Save.SenderDepartment;
            param[5] = new SqlParameter("@SenderContactno", SqlDbType.NVarChar);
            param[5].Value = Save.SenderContactno;
            param[6] = new SqlParameter("@SENDEREMAIL", SqlDbType.NVarChar);
            param[6].Value = Save.SENDEREMAIL;
            param[7] = new SqlParameter("@SENDERCITY", SqlDbType.NVarChar);
            param[7].Value = Save.SENDERCITY;
            param[8] = new SqlParameter("@SENDERPINCODE", SqlDbType.NVarChar);
            param[8].Value = Save.SENDERPINCODE;
            param[9] = new SqlParameter("@SENDERCOUNTRY", SqlDbType.NVarChar);
            param[9].Value = Save.SENDERCOUNTRY;
            param[10] = new SqlParameter("@RecipientLocation", SqlDbType.NVarChar);
            param[10].Value = Save.RecipientLocation;
            param[11] = new SqlParameter("@RecipientName", SqlDbType.NVarChar);
            param[11].Value = Save.RecipientName;
            param[12] = new SqlParameter("@RecipientDepartment", SqlDbType.NVarChar);
            param[12].Value = Save.RecipientDepartment;
            param[13] = new SqlParameter("@RecipientContactno", SqlDbType.NVarChar);
            param[13].Value = Save.RecipientContactno;
            param[14] = new SqlParameter("@RecipientEmail", SqlDbType.NVarChar);
            param[14].Value = Save.RecipientEmail;
            param[15] = new SqlParameter("@RecipientCity", SqlDbType.NVarChar);
            param[15].Value = Save.RecipientCity;
            param[16] = new SqlParameter("@RecipientPincode", SqlDbType.NVarChar);
            param[16].Value = Save.RecipientPincode;
            param[17] = new SqlParameter("@RecipientCountry", SqlDbType.NVarChar);
            param[17].Value = Save.RecipientCountry;
            param[18] = new SqlParameter("@CreatedBy", SqlDbType.NVarChar);
            param[18].Value = HttpContext.Current.Session["UID"];
            param[19] = new SqlParameter("@BORecipientLocation", SqlDbType.NVarChar);
            param[19].Value = Save.BORecipientLocation;
            param[20] = new SqlParameter("@BORecipientName", SqlDbType.NVarChar);
            param[20].Value = Save.BORecipientName;
            param[21] = new SqlParameter("@BORecipientContactno", SqlDbType.NVarChar);
            param[21].Value = Save.BORecipientContactno;
            param[22] = new SqlParameter("@BORecipientEmail", SqlDbType.NVarChar);
            param[22].Value = Save.BORecipientEmail;
            param[23] = new SqlParameter("@BORecipientCity", SqlDbType.NVarChar);
            param[23].Value = Save.BORecipientCity;
            param[24] = new SqlParameter("@BORecipientPincode", SqlDbType.NVarChar);
            param[24].Value = Save.BORecipientPincode;
            param[25] = new SqlParameter("@BORecipientCountry", SqlDbType.NVarChar);
            param[25].Value = Save.BORecipientCountry;
            param[26] = new SqlParameter("@recipientType", SqlDbType.NVarChar);
            param[26].Value = Save.recipientType;




            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "INSERT_COURIER_DATA", param))

            {
                if (dr.Read())
                {
                    return new { Message = "Parcel Initiated Successfully", Data = Save };
                }
            }

            return new { Message = "Parcel Initiated Successfully", Data = Save, AuthToken = (string)null };
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }

    public object UpdateData(UpdateformData Update)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[29];
            param[0] = new SqlParameter("@Requestid", SqlDbType.NVarChar);
            param[0].Value = Update.BRNCODE;
            param[1] = new SqlParameter("@BookingDate", SqlDbType.DateTime);
            param[1].Value = Update.todate;
            param[2] = new SqlParameter("@SenderLocation", SqlDbType.NVarChar);
            param[2].Value = Update.senderLocation;
            param[3] = new SqlParameter("@SenderName", SqlDbType.NVarChar);
            param[3].Value = Update.SenderName;
            param[4] = new SqlParameter("@SenderDepartment", SqlDbType.NVarChar);
            param[4].Value = Update.SenderDepartment;
            param[5] = new SqlParameter("@SenderContactno", SqlDbType.NVarChar);
            param[5].Value = Update.SenderContactno;
            param[6] = new SqlParameter("@SENDEREMAIL", SqlDbType.NVarChar);
            param[6].Value = Update.SENDEREMAIL;
            param[7] = new SqlParameter("@SENDERCITY", SqlDbType.NVarChar);
            param[7].Value = Update.SENDERCITY;
            param[8] = new SqlParameter("@SENDERPINCODE", SqlDbType.NVarChar);
            param[8].Value = Update.SENDERPINCODE;
            param[9] = new SqlParameter("@SENDERCOUNTRY", SqlDbType.NVarChar);
            param[9].Value = Update.SENDERCOUNTRY;
            param[10] = new SqlParameter("@RecipientLocation", SqlDbType.NVarChar);
            param[10].Value = Update.RecipientLocation;
            param[11] = new SqlParameter("@RecipientName", SqlDbType.NVarChar);
            param[11].Value = Update.RecipientName;
            param[12] = new SqlParameter("@RecipientDepartment", SqlDbType.NVarChar);
            param[12].Value = Update.RecipientDepartment;
            param[13] = new SqlParameter("@RecipientContactno", SqlDbType.NVarChar);
            param[13].Value = Update.RecipientContactno;
            param[14] = new SqlParameter("@RecipientEmail", SqlDbType.NVarChar);
            param[14].Value = Update.RecipientEmail;
            param[15] = new SqlParameter("@RecipientCity", SqlDbType.NVarChar);
            param[15].Value = Update.RecipientCity;
            param[16] = new SqlParameter("@RecipientPincode", SqlDbType.NVarChar);
            param[16].Value = Update.RecipientPincode;
            param[17] = new SqlParameter("@RecipientCountry", SqlDbType.NVarChar);
            param[17].Value = Update.RecipientCountry;
            param[18] = new SqlParameter("@CourierAgency", SqlDbType.NVarChar);
            param[18].Value = Update.CourierAgency;
            param[19] = new SqlParameter("@AWBPODNO", SqlDbType.NVarChar);
            param[19].Value = Update.AWBPODNO;
            param[20] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            param[20].Value = Update.Mode;
            param[21] = new SqlParameter("@ParcelCategory", SqlDbType.NVarChar);
            param[21].Value = Update.ParcelCategory;
            param[22] = new SqlParameter("@ParcelDetails", SqlDbType.NVarChar);
            param[22].Value = Update.ParcelDetails;
            param[23] = new SqlParameter("@INVOICENO", SqlDbType.NVarChar);
            param[23].Value = Update.INVOICENO;
            param[24] = new SqlParameter("@InvoiceAMOUNT", SqlDbType.NVarChar);
            param[24].Value = Update.InvoiceAMOUNT;
            param[25] = new SqlParameter("@Weight", SqlDbType.NVarChar);
            param[25].Value = Update.Weight;
            param[26] = new SqlParameter("@NOOFPIECES", SqlDbType.NVarChar);
            param[26].Value = Update.NOOFPIECES;
            param[27] = new SqlParameter("@DeliveryRemarks", SqlDbType.NVarChar);
            param[27].Value = Update.DeliveryRemarks;
            param[28] = new SqlParameter("@CreatedBy", SqlDbType.NVarChar);
            param[28].Value = HttpContext.Current.Session["UID"];



            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[UPDATE_COURIER_DATA]", param))
            {
                if (dr != null && dr.HasRows)
                {
                    if (dr.Read())
                    {
                        return new { Message = "Data Updated Successfully", Data = Update };
                    }
                }
                else
                {

                    return new { Message = "No data returned by the stored procedure.", Data = (object)null };
                }
            }


            return new { Message = "Data Updated Successfully", Data = Update, AuthToken = (string)null };
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }
    public object UpdateDispatchData(UpdateDispatchData Update)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@Requestid", SqlDbType.NVarChar);
            param[0].Value = Update.BRNCODE;
            param[1] = new SqlParameter("@CourierAgency", SqlDbType.NVarChar);
            param[1].Value = Update.CourierAgency;
            param[2] = new SqlParameter("@AWBPODNO", SqlDbType.NVarChar);
            param[2].Value = Update.AWBPODNO;
            param[3] = new SqlParameter("@Mode", SqlDbType.NVarChar);
            param[3].Value = Update.Mode;
            param[4] = new SqlParameter("@ParcelCategory", SqlDbType.NVarChar);
            param[4].Value = Update.ParcelCategory;
            param[5] = new SqlParameter("@ParcelDetails", SqlDbType.NVarChar);
            param[5].Value = Update.ParcelDetails;
            param[6] = new SqlParameter("@Weight", SqlDbType.NVarChar);
            param[6].Value = Update.Weight;
            param[7] = new SqlParameter("@NOOFPIECES", SqlDbType.NVarChar);
            param[7].Value = Update.NOOFPIECES;
            param[8] = new SqlParameter("@DeliveryRemarks", SqlDbType.NVarChar);
            param[8].Value = Update.DeliveryRemarks;
            param[9] = new SqlParameter("@CreatedBy", SqlDbType.NVarChar);
            param[9].Value = HttpContext.Current.Session["UID"];



            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[UPDATE_COURIER_DISPATCH_DATA]", param))
            {
                if (dr != null && dr.HasRows)
                {
                    if (dr.Read())
                    {
                        return new { Message = "Dispatched Sucessfully", Data = Update };
                    }
                }
                else
                {

                    return new { Message = "Something went wrong. Please try again later", Data = (object)null };
                }
            }


            return new { Message = "Dispatched Sucessfully", Data = Update, AuthToken = (string)null };
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }

    public object RejectData(UpdateformData Update)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@Requestid", SqlDbType.NVarChar);
            param[0].Value = Update.BRNCODE;
            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[Reject_COURIER_DATA]", param))
            {
                if (dr != null && dr.HasRows)
                {
                    if (dr.Read())
                    {
                        return new { Message = "Data Updated Successfully", Data = Update };
                    }
                }
                else
                {

                    return new { Message = "No data returned by the stored procedure.", Data = (object)null };
                }
            }


            return new { Message = "Data Updated Successfully", Data = Update, AuthToken = (string)null };
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }
    public object UpdateiNBOUNDData(UpdateformData Update)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@Requestid", SqlDbType.NVarChar);
            param[0].Value = Update.BRNCODE;
            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[UPDATE_INBOUND_COURIER_DATA]", param))
            {
                if (dr != null && dr.HasRows)
                {
                    if (dr.Read())
                    {
                        return new { Message = "Data Updated Successfully", Data = Update };
                    }
                }
                else
                {

                    return new { Message = "No data returned by the stored procedure.", Data = (object)null };
                }
            }


            return new { Message = "Data Updated Successfully", Data = Update, AuthToken = (string)null };
        }
        catch (Exception ex)
        {
            return ex.Message;
        }

    }
    public object UpdateStatus(UpdateStatus Update)
    {
        try
        {
            SqlParameter[] statusParams = new SqlParameter[1];
            statusParams[0] = new SqlParameter("@EmployeeAcknowledge", SqlDbType.Structured);
            statusParams[0].Value = UtilityService.ConvertToDataTable(Update.updatedStatusList);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "[UPDATE_STATUS]", statusParams);
            return new { Message = " Employee acknowledgment was successful", Data = Update };
        }
        catch (Exception ex)
        {

            return new { Message = ex.Message };
        }
    }

    public object RejectStatus(UpdateStatus Update)

    {
        try
        {
            SqlParameter[] statusParams = new SqlParameter[1];
            statusParams[0] = new SqlParameter("@EmployeeAcknowledge", SqlDbType.Structured);
            statusParams[0].Value = UtilityService.ConvertToDataTable(Update.updatedStatusList);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "[Reject_STATUS]", statusParams);



            return new { Message = " Employee acknowledgment was Rejected", Data = Update };
        }
        catch (Exception ex)
        {

            return new { Message = ex.Message };
        }
    }

    public IEnumerable<BookingDetails> GetEMPLOYEEACKGRID()
    {
        try
        {

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar)
            {
                Value = HttpContext.Current.Session["UID"]
            };


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "Employee_Ack_Bind_Grid", param))
            {
                List<BookingDetails> resrvlist = new List<BookingDetails>();
                while (reader.Read())
                {
                    resrvlist.Add(new BookingDetails
                    {
                        DFDC_ACT_NAME = reader["DFDC_ACT_NAME"].ToString(),
                        //MLRMD_AWB_NO = reader["MLRMD_AWB_NO"].ToString(),
                        SENDER_DEPT = reader["SENDER_DEPT"].ToString(),
                        SEND_LOC = reader["SEND_LOC"].ToString(),
                        DES_DEP = reader["DES_DEP"].ToString(),
                        DES_LOC = reader["DES_LOC"].ToString(),
                        PARCEL_AWB_NO = reader["PARCEL_AWB_NO"].ToString(),
                        COURIER_NAME = reader["COURIER_NAME"].ToString(),
                        STA_TITLE = reader["STA_TITLE"].ToString()
                    });
                }
                return resrvlist;
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException("Error fetching grid data", ex);
        }
    }
    public IEnumerable<EDSaveformData> GetParcelDetails(string ReqId)
    {
        List<EDSaveformData> GetformReqId = new List<EDSaveformData>();

        try
        {
            // Prepare the stored procedure
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_PARCEL_DETAILS_BY_REQID");
            sp.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
            sp.Command.AddParameter("@REQID", ReqId, DbType.String);

            // Execute the query and get the reader
            using (IDataReader sdr = sp.GetReader())
            {
                EDSaveformData currentParcel = null;

                // Loop through the data reader
                while (sdr.Read())
                {
                    currentParcel = new EDSaveformData()
                    {
                        ReqId = sdr["REQID"].ToString(),
                        todate = Convert.ToDateTime(sdr["BOOKING_DT"]),

                        // Sender details
                        SenderName = sdr["SENDER_NAME"].ToString(),
                        SenderId = sdr["SENDER_ID"].ToString(),
                        SenderContactno = sdr["SENDER_CONTACT_NO"].ToString(),
                        SENDEREMAIL = sdr["SENDER_EMAIL"].ToString(),
                        SenderDepartment = sdr["SENDER_DEPT"].ToString(),
                        senderLocation = sdr["SENDER_LOCATION"].ToString(),
                        SENDERCITY = sdr["SENDER_CITY"].ToString(),
                        SENDERCOUNTRY = sdr["SENDER_COUNTRY"].ToString(),
                        SENDERPINCODE = sdr["SENDER_PINCODE"].ToString(),
                        LCM_NAME = sdr["LCM_NAME"].ToString(),

                        // Recipient details
                        RecipientName = sdr["RECV_NAME"].ToString(),
                        RecipientId = sdr["RECV_ID"].ToString(),
                        RecipientContactno = sdr["DESTINATION_CONTACT_NO"].ToString(),
                        RecipientEmail = sdr["DESTINATION_EMAIL"].ToString(),
                        RecipientDepartment = sdr["DESTINATION_DEPT"].ToString(),
                        RecipientLocation = sdr["DESTINATION_LOCATION"].ToString(),
                        RecipientCity = sdr["DESTINATION_CITY"].ToString(),
                        RecipientCountry = sdr["DESTINATION_COUNTRY"].ToString(),
                        RecipientPincode = sdr["DESTINATION_PINCODE"].ToString(),
                        DESTINATION_LCM_NAME= sdr["DESTINATION_LCM_NAME"].ToString(),

                        // Parcel details
                        CourierAgency = sdr["PARCEL_AGENCY"].ToString(),
                        AWBPODNO = sdr["PARCEL_AWB_NO"].ToString(),
                        Mode = sdr["PARCEL_MODE"].ToString(),
                        ParcelCategory = sdr["PARCEL_CATEGORY"].ToString(),
                        ParcelDetails = sdr["PARCEL_TYPE"].ToString(),
                        INVOICENO = sdr["INVOICE_NO"].ToString(),
                        InvoiceAMOUNT = sdr["INVOICE_AMOUNT"].ToString(),
                        Weight = sdr["PARCEL_WEIGHT"].ToString(),
                        NOOFPIECES = sdr["Item_Count"].ToString(),
                        DeliveryRemarks = sdr["DELIVERY_NOTE_REMARKS"].ToString()
                    };

                    // Add the current parcel to the list
                    GetformReqId.Add(currentParcel);
                }

                // If there are document details to read
                if (sdr.NextResult() && currentParcel != null)
                {
                    while (sdr.Read())
                    {
                        // Add documents related to the current parcel
                        currentParcel.Documents.Add(new EDMailDoc
                        {
                            DocReqId = sdr["MLRM_DOC_REQID"].ToString(),
                            DocName = sdr["MLRM_DOC_NAME"].ToString(),
                            DocPath = sdr["MLRM_DOC_PATH"].ToString()
                        });
                    }
                }

                sdr.Close();
            }
        }
        catch (Exception ex)
        {
            // Handle exception (log or rethrow)
            // Example logging:
            // Logger.LogError("Error in GetParcelDetails method: " + ex.Message);
            throw new ApplicationException("An error occurred while retrieving parcel details.", ex);
        }

        // Return the list of parcels
        return GetformReqId;
    }
    public object EmployeeAcknowledge(UpdateStatusAcknowledge Update)
    {
        try
        {
            SqlParameter[] statusParams = new SqlParameter[1];
            statusParams[0] = new SqlParameter("@EmployeeAcknowledge", SqlDbType.Structured);
            statusParams[0].Value = UtilityService.ConvertToDataTable(Update.UpdateStatusAcknowledgeList);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "[UPDATE_STATUS_EMP_ACK]", statusParams);
            


            return new { Message = " Employee acknowledgment was successful", Data = Update };
        }
        catch (Exception ex)
        {

            return new { Message = ex.Message };
        }
    }
    public object EmployeeAcknowledgeReject(UpdateStatusAcknowledge Update)
    {
        try
        {
            SqlParameter[] statusParams = new SqlParameter[1];
            statusParams[0] = new SqlParameter("@EmployeeAcknowledge", SqlDbType.Structured);
            statusParams[0].Value = UtilityService.ConvertToDataTable(Update.UpdateStatusAcknowledgeList);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "[UPDATE_STATUS_EMP_ACK_Reject]", statusParams);



            return new { Message = " Employee acknowledgment was Rejected", Data = Update };
        }
        catch (Exception ex)
        {

            return new { Message = ex.Message };
        }
    }

    public object AssignCourierData(AssignCourierformData Save)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@Requestid", SqlDbType.NVarChar);
            param[0].Value = Save.BRNCODE;
            param[1] = new SqlParameter("@RecipientLocation", SqlDbType.NVarChar);
            param[1].Value = Save.RecipientLocation;
            param[2] = new SqlParameter("@RecipientName", SqlDbType.NVarChar);
            param[2].Value = Save.RecipientName;
            param[3] = new SqlParameter("@RecipientDepartment", SqlDbType.NVarChar);
            param[3].Value = Save.RecipientDepartment;
            param[4] = new SqlParameter("@RecipientContactno", SqlDbType.NVarChar);
            param[4].Value = Save.RecipientContactno;
            param[5] = new SqlParameter("@RecipientEmail", SqlDbType.NVarChar);
            param[5].Value = Save.RecipientEmail;
            param[6] = new SqlParameter("@RecipientCity", SqlDbType.NVarChar);
            param[6].Value = Save.RecipientCity;
            param[7] = new SqlParameter("@RecipientPincode", SqlDbType.NVarChar);
            param[7].Value = Save.RecipientPincode;
            param[8] = new SqlParameter("@RecipientCountry", SqlDbType.NVarChar);
            param[8].Value = Save.RecipientCountry;
            param[9] = new SqlParameter("@Status", SqlDbType.NVarChar);
            param[9].Value = Save.Status;

            using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "ASSIGN_COURIER_DATA", param))
            {
                if (dr != null && dr.Read()) 
                {
                    return new { Message = "Data Assigned Successfully", Data = Save };
                }
            }

            return new { Message = "Data Assigned Successfully", Data = Save, AuthToken = (string)null }; 

        }
        catch (Exception ex)
        {
            return new { Message = "An error occurred.", Error = ex.Message };
        }
    }
    public CustomizableReportData BindGridCustomizableReport(ExpenseReportModel Params)
    {
        DataSet ds = new DataSet();
        List<CustomizedGridColss> gridcols = new List<CustomizedGridColss>();
        CustomizedGridColss gridcolscls;
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MAIL_ROOM_CUSTOMIZABLE_RPT");
        List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                               select dc.ColumnName).ToList();
        foreach (string col in Colstr)
        {
            gridcolscls = new CustomizedGridColss();
            gridcolscls.cellClass = "grid-align";
            gridcolscls.field = col;
            gridcolscls.headerName = col;
            gridcols.Add(gridcolscls);
        }

        return new CustomizableReportData
        {
            GridData = ds.Tables[0],
            ColumnDefinitions = gridcols
        };
    }
    public CustomizableReportData BindGrid_Search(ExpenseReportModel Params)
    {
        try
        {
            DataSet ds = new DataSet();
            List<CustomizedGridColss> gridcols = new List<CustomizedGridColss>();
            CustomizedGridColss gridcolscls;
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SearchBy", SqlDbType.NVarChar);
            param[0].Value = (Params.loclst);
           
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "MAIL_ROOM_CUSTOMIZABLE_RPT_SEARCH", param);
            List<string> Colstr = (from dc in ds.Tables[0].Columns.Cast<DataColumn>()
                                   select dc.ColumnName).ToList();
            foreach (string col in Colstr)
            {
                gridcolscls = new CustomizedGridColss();
                gridcolscls.cellClass = "grid-align";
                gridcolscls.field = col;
                gridcolscls.headerName = col;
                gridcols.Add(gridcolscls);
            }
            return new CustomizableReportData { GridData = ds.Tables[0], ColumnDefinitions = gridcols };

        }
        catch (SqlException) { throw; }
    }
    public IEnumerable<DispatchDetails> GetDispatcherGridData(string recipientType)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@AUR_ID", SqlDbType.VarChar)
            {
                Value = HttpContext.Current.Session["UID"]
            };
            param[1] = new SqlParameter("@SearchBy", SqlDbType.NVarChar);
            param[1].Value = (recipientType);


            using (SqlDataReader reader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "DISPATCH_BIND_GRID", param))
            {
                List<DispatchDetails> resrvlist = new List<DispatchDetails>();
                while (reader.Read())
                {
                    resrvlist.Add(new DispatchDetails
                    {
                        DFDC_ACT_NAME = reader["DFDC_ACT_NAME"].ToString(),
                        SENDER_DEPT = reader["SENDER_DEPT"].ToString(),
                        SEND_LOC = reader["SEND_LOC"].ToString(),
                        DES_DEP = reader["DES_DEP"].ToString(),
                        DES_LOC = reader["DES_LOC"].ToString(),
                        STA_TITLE = reader["STA_TITLE"].ToString()
                    });
                }
                return resrvlist;
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException("Error fetching grid data", ex);
        }
    }

}