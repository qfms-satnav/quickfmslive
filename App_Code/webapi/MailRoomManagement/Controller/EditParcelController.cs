﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class EditParcelController : ApiController
{
    EditParcelService BPS = new EditParcelService();
    public object GetParcelReqid()
    {
        var obj = BPS.GetParcelReqid();
        return obj;
    }

    //[HttpGet]
    //public HttpResponseMessage GetStatus()
    //{
    //    IEnumerable<Status_VM> GetformReqId = BPS.GetStatus();
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, GetformReqId);
    //    return response;
    //}


    [HttpGet]
    public HttpResponseMessage GetParcelDetails(String ReqId)
    {
        IEnumerable<EDSaveformData> GetformReqId = BPS.GetParcelDetails(ReqId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, GetformReqId);
        return response;
    }


    [HttpGet]
    public HttpResponseMessage BindLocation()
    {
        IEnumerable<LocationParcel> LocationList = BPS.BindLocation();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, LocationList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage BindUser()
    {
        IEnumerable<User> UserList = BPS.BindUser();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, UserList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage BindCourierAgency()
    {
        IEnumerable<CourierAgency> CourierAgencyList = BPS.BindCourierAgency();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CourierAgencyList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage BindCourierMode()
    {
        IEnumerable<CourierMode> CourierModeList = BPS.BindCourierMode();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CourierModeList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage BindCourierCategory()
    {
        IEnumerable<ParcelCategory> CourierCategoryist = BPS.BindCourierCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CourierCategoryist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage BindCourierDetails()
    {
        IEnumerable<ParcelDetails> CourierDetailsList = BPS.BindCourierDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CourierDetailsList);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage SaveData(SaveformData Save)
    {
        var obj = BPS.SaveData(Save);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpPost]
    //public HttpResponseMessage UploadTemplate()
    //{
    //    var httpRequest = HttpContext.Current.Request;
    //    if (validateFile.IsValid)
    //    {
    //        httpRequest = HttpContext.Current.Request;
    //        dynamic retobj = BPS.UploadTemplate(httpRequest);
    //        string retContent = "";
    //        if (retobj.data != null)
    //            retContent = JsonConvert.SerializeObject(retobj);
    //        else
    //            retContent = JsonConvert.SerializeObject(retobj);

    //        var response = Request.CreateResponse(HttpStatusCode.OK);
    //        response.Content = new StringContent(retContent, Encoding.UTF8, "text/plain");
    //        response.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(@"text/html");
    //        return response;
    //    }
    //    else
    //    {
    //        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.BadRequest, new { Message = validateFile.ErrorMessage, data = (object)null });
    //        return response;
    //    }

    //}
}
