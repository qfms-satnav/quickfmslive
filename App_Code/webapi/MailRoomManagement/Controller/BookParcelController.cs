﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BookParcelController : ApiController
{
    BookParcelService BPS = new BookParcelService();
    public object Getrefnum()
    {
        var obj = BPS.Getrefnum();
        return obj;
    }

    [HttpGet]
    public HttpResponseMessage BindLocation()
    {
        IEnumerable<LocationParcel> LocationList = BPS.BindLocation();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, LocationList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<BookingDetails> griddata = BPS.GetGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage Assign_Courier_GetGridData()
    {
        IEnumerable<BookingDetails> griddata = BPS.Assign_Courier_GetGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }
    public HttpResponseMessage GetEMPLOYEEACKGRID()
    {
        IEnumerable<BookingDetails> griddata = BPS.GetEMPLOYEEACKGRID();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage BindUser()
    {
        IEnumerable<User> UserList = BPS.BindUser();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, UserList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage BindCourierAgency()
    {
        IEnumerable<CourierAgency> CourierAgencyList = BPS.BindCourierAgency();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CourierAgencyList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage BindCourierMode()
    {
        IEnumerable<CourierMode> CourierModeList = BPS.BindCourierMode();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CourierModeList);
        return response;
    }
    [HttpGet]
    public HttpResponseMessage BindCourierCategory()
    {
        IEnumerable<ParcelCategory> CourierCategoryist = BPS.BindCourierCategory();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CourierCategoryist);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage BindCourierDetails()
    {
        IEnumerable<ParcelDetails> CourierDetailsList = BPS.BindCourierDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CourierDetailsList);
        return response;
    }
    
    [HttpPost]
    public HttpResponseMessage AssignCourierData(AssignCourierformData Save)
    {
        var obj = BPS.AssignCourierData(Save);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage SaveData(SaveformData Save)
    {
        var obj = BPS.SaveData(Save);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage UpdateData(UpdateformData Update)
    {
        var obj = BPS.UpdateData(Update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage UpdateDispatchData(UpdateDispatchData Update)
    {
        var obj = BPS.UpdateDispatchData(Update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage RejectData(UpdateformData Update)
    {
        var obj = BPS.RejectData(Update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage UpdateiNBOUNDData(UpdateformData Update)
    {
        var obj = BPS.UpdateiNBOUNDData(Update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage UpdateStatus(UpdateStatus Update)
    {
        var obj = BPS.UpdateStatus(Update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage RejectStatus(UpdateStatus Update)
    {
        var obj = BPS.RejectStatus(Update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage EmployeeAcknowledge(UpdateStatusAcknowledge Update)
    {
        var obj = BPS.EmployeeAcknowledge(Update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage EmployeeAcknowledgeReject(UpdateStatusAcknowledge Update)
    {
        var obj = BPS.EmployeeAcknowledgeReject(Update);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage BindGridCustomizableReport(ExpenseReportModel Params)
    {
        var obj = BPS.BindGridCustomizableReport(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage BindGrid_Search(ExpenseReportModel Params)
    {
        var obj = BPS.BindGrid_Search(Params);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetDispatcherGridData(string recipientType)
    {
        IEnumerable<DispatchDetails> griddata = BPS.GetDispatcherGridData(recipientType);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, griddata);
        return response;
    }

    [HttpGet]
    public HttpResponseMessage GetParcelDetails(String ReqId)
    {
        IEnumerable<EDSaveformData> GetformReqId = BPS.GetParcelDetails(ReqId);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, GetformReqId);
        return response;
    }

}
