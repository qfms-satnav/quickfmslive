﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class ParcelMasterController : ApiController
{
    ParcelMasterService BICS = new ParcelMasterService();

    [HttpPost]
    public HttpResponseMessage SaveParcelDetails(ParcelMasterModel BicModel)
    {
        if (BICS.SaveParcelDetails(BicModel) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "ParcelCode already Exists");
    }
    [HttpPost]
    public HttpResponseMessage GetGridData()
    {
        IEnumerable<ParcelMasterModel> BicModel = BICS.BindGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ModifyParcelDetails(ParcelMasterModel BicModel)
    {
        try
        {
            BICS.ModifyParcelDetails(BicModel); // Call the method that performs the operation
            return Request.CreateResponse(HttpStatusCode.OK, "Parcel details modified successfully.");
        }
        catch (Exception ex)
        {
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
        }
    }

    [HttpPost]
    public HttpResponseMessage SaveCourierAgencyDetails(CourierAgencyMasterModel BicModel)
    {
        if (BICS.SaveCourierAgencyDetails(BicModel) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "ParcelCode already Exists");
    }
    [HttpPost]
    public HttpResponseMessage GetCourierAgencyDetailsGridData()
    {
        IEnumerable<CourierAgencyMasterModel> BicModel = BICS.GetCourierAgencyDetailsGridData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BicModel);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage ModifyCourierAgencyDetails(CourierAgencyMasterModel BicModel)
    {
        try
        {
            BICS.ModifyCourierAgencyDetails(BicModel); // Call the method that performs the operation
            return Request.CreateResponse(HttpStatusCode.OK, "Parcel details modified successfully.");
        }
        catch (Exception ex)
        {
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
        }
    }


}
