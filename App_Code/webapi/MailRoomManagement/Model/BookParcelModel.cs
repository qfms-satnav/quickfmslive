﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;



    public class LocationParcel
    {
        public string LCM_CODE { get; set; }
        public string LCM_NAME { get; set; }
    public string Destination_LCM_CODE { get; set; }
    public string Destination_LCM_NAME { get; set; }

    public bool ticked { get; set; }
    }

public class Status_VM
{
    public string STA_CODE { get; set; }

    public string STA_TITLE { get; set; }
    public bool ticked { get; set; }

   

}
public class CourierAgency
{
    public string COURIER_CODE { get; set; }
    public string COURIER_NAME { get; set; }

    public bool ticked { get; set; }
}
public class CourierMode
{
    public string COURIERMODE_CODE { get; set; }
    public string COURIERMODE_NAME { get; set; }

    public bool ticked { get; set; }
}
public class ParcelCategory
{
    public string PARCEL_CODE { get; set; }
    public string PARCEL_NAME { get; set; }

    public bool ticked { get; set; }
}
public class ParcelDetails
{
    public string COURIERDETAILS_CODE { get; set; }
    public string COURIERDETAILS_NAME { get; set; }

    public bool ticked { get; set; }
}
public class User
{
    public string AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string AUR_DEP_ID { get; set; }
    public string AUR_RES_NUMBER { get; set; }
    public string AUR_EMAIL { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_PINCODE { get; set; }
    public string CNY_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public string Destination_AUR_ID { get; set; }
    public string Destination_AUR_KNOWN_AS { get; set; }
    public string Destination_AUR_DEP_ID { get; set; }
    public string Destination_AUR_RES_NUMBER { get; set; }
    public string Destination_AUR_EMAIL { get; set; }
    public string Destination_CTY_NAME { get; set; }
    public string Destination_LCM_PINCODE { get; set; }
    public string Destination_CNY_NAME { get; set; }
    public string Destination_LCM_CODE { get; set; }
    public bool ticked { get; set; }
}
public class SaveformData
{
    public string BRNCODE { get; set; }
    public DateTime todate { get; set; }
    public string senderLocation { get; set; }
    public string SenderName { get; set; }
    public string SenderDepartment { get; set; }
    public string SenderContactno { get; set; }

    public string SENDEREMAIL { get; set; }
    public string SENDERCITY { get; set; }
    public string SENDERPINCODE { get; set; }
    public string SENDERCOUNTRY { get; set; }
    public string RecipientLocation { get; set; }
    public string RecipientName { get; set; }

    public string RecipientDepartment { get; set; }
    public string RecipientContactno { get; set; }
    public string RecipientEmail { get; set; }
    public string RecipientCity { get; set; }
    public string RecipientPincode { get; set; }
    public string RecipientCountry { get; set; }
    public string CourierAgency { get; set; }
    public string AWBPODNO { get; set; }

    public string Mode { get; set; }
    public string ParcelCategory { get; set; }
    public string ParcelDetails { get; set; }
    public string INVOICENO { get; set; }
    public string InvoiceAMOUNT { get; set; }
    public string Weight { get; set; }

    public string NOOFPIECES { get; set; }
    public string DeliveryRemarks { get; set; }
    public string BORecipientLocation { get; set; }
    public string BORecipientName { get; set; }

    public string BORecipientContactno { get; set; }
    public string BORecipientEmail { get; set; }
    public string BORecipientCity { get; set; }
    public string BORecipientPincode { get; set; }
    public string BORecipientCountry { get; set; }
    public string recipientType { get; set; }
    
}

public class AssignCourierformData
{
    public string BRNCODE { get; set; }
    public string RecipientLocation { get; set; }
    public string RecipientName { get; set; }
    public string RecipientDepartment { get; set; }
    public string RecipientContactno { get; set; }
    public string RecipientEmail { get; set; }
    public string RecipientCity { get; set; }
    public string RecipientPincode { get; set; }
    public string RecipientCountry { get; set; }
    public string Status { get; set; }
}
public class UpdateformData
{
    public string BRNCODE { get; set; }
    public DateTime todate { get; set; }
    public string senderLocation { get; set; }
    public string SenderName { get; set; }
    public string SenderDepartment { get; set; }
    public string SenderContactno { get; set; }

    public string SENDEREMAIL { get; set; }
    public string SENDERCITY { get; set; }
    public string SENDERPINCODE { get; set; }
    public string SENDERCOUNTRY { get; set; }
    public string RecipientLocation { get; set; }
    public string RecipientName { get; set; }

    public string RecipientDepartment { get; set; }
    public string RecipientContactno { get; set; }
    public string RecipientEmail { get; set; }
    public string RecipientCity { get; set; }
    public string RecipientPincode { get; set; }
    public string RecipientCountry { get; set; }
    public string CourierAgency { get; set; }
    public string AWBPODNO { get; set; }

    public string Mode { get; set; }
    public string ParcelCategory { get; set; }
    public string ParcelDetails { get; set; }
    public string INVOICENO { get; set; }
    public string InvoiceAMOUNT { get; set; }
    public string Weight { get; set; }

    public string NOOFPIECES { get; set; }
    public string DeliveryRemarks { get; set; }
}

public class UpdateDispatchData
{
    public string BRNCODE { get; set; }
   
    public string CourierAgency { get; set; }
    public string AWBPODNO { get; set; }

    public string Mode { get; set; }
    public string ParcelCategory { get; set; }
    public string ParcelDetails { get; set; }
 
    public string Weight { get; set; }

    public string NOOFPIECES { get; set; }
    public string DeliveryRemarks { get; set; }
}
public class StatusList
{
    //public string STA_CODE { get; set; }  
    public string DFDC_ACT_NAME { get; set; }  

}
public class UpdateStatus
{
    public List<StatusList> updatedStatusList { get; set; } 

    
}
public class UpdateStatusAcknowledge
{
    public List<StatusLists> UpdateStatusAcknowledgeList { get; set; }


}
public class StatusLists
{
    public string DFDC_ACT_NAME { get; set; }

}
public class CustomizableReportData
{
    public DataTable GridData { get; set; }
    public List<CustomizedGridColss> ColumnDefinitions { get; set; }
}
public class CustomizedGridColss
{
    public string headerName { get; set; }
    public string field { get; set; }
    public int width { get; set; }
    public string cellClass { get; set; }
    public bool suppressMenu { get; set; }
}
public class ExpenseReportModel
{
    public string LCM_CODE { get; set; }
    public string LCM_ZONE { get; set; }
    public string STATE { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_NAME { get; set; }
    public string TWR_NAME { get; set; }
    public int FLAG { get; set; }
    public string FLR_CODE { get; set; }
    public string PageSize { get; set; }
    public string SPC_ID { get; set; }
    public string SPACE_TYPE { get; set; }
    public string SSAD_AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string LegalEntity { get; set; }
    public string Platform { get; set; }
    public string Business_Unit { get; set; }
    public string Organisation_Unit { get; set; }
    public string Role { get; set; }
    public string AUR_DESGN_ID { get; set; }
    public string AUR_GRADE { get; set; }
    public string SSA_FROM_DATE { get; set; }
    public string SSA_TO_DATE { get; set; }
    public string SH_FRM_HRS { get; set; }
    public string SH_TO_HRS { get; set; }
    public string STA_TITLE { get; set; }
    public string loclst { get; set; }
    


}
public class DispatchDetails
{
    public string DFDC_ACT_NAME { get; set; }
    public string SENDER_DEPT { get; set; }
    public string SEND_LOC { get; set; }
    public string DES_DEP { get; set; }
    public string DES_LOC { get; set; }
    public string STA_TITLE { get; set; }

    public string SENT_DATE { get; set; }

    public string RECIPTENT { get; set; }

    public string Parcel_Type { get; set; }


}