﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ParcelMasterModel
/// </summary>
public class ParcelMasterModel
{
   
        public string PAR_CODE { get; set; }
        public string PAR_NAME { get; set; }
        public string BICM_STA_ID { get; set; }
   
}

public class CourierAgencyMasterModel
{
   
        public string CAR_CODE { get; set; }
        public string CAR_NAME { get; set; }
        public string BICM_STA_ID { get; set; }
   
}