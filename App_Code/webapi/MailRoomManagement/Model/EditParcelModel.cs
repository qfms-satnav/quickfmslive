﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;



    public class EDLocationParcel
    {
        public string LCM_CODE { get; set; }
        public string LCM_NAME { get; set; }
      
        public bool ticked { get; set; }
    }
public class EDCourierAgency
{
    public string COURIER_CODE { get; set; }
    public string COURIER_NAME { get; set; }

    public bool ticked { get; set; }
}
public class EDCourierMode
{
    public string COURIERMODE_CODE { get; set; }
    public string COURIERMODE_NAME { get; set; }

    public bool ticked { get; set; }
}
public class EDParcelCategory
{
    public string PARCEL_CODE { get; set; }
    public string PARCEL_NAME { get; set; }

    public bool ticked { get; set; }
}
public class EDParcelDetails
{
    public string COURIERDETAILS_CODE { get; set; }
    public string COURIERDETAILS_NAME { get; set; }

    public bool ticked { get; set; }
}
public class EDUser
{
    public string AUR_ID { get; set; }
    public string AUR_KNOWN_AS { get; set; }
    public string AUR_DEP_ID { get; set; }
    public string AUR_RES_NUMBER { get; set; }
    public string AUR_EMAIL { get; set; }
    public string CTY_NAME { get; set; }
    public string LCM_PINCODE { get; set; }
    public string CNY_NAME { get; set; }
    public string LCM_CODE { get; set; }
    public bool ticked { get; set; }
}
public class EDSaveformData
{
    public string ReqId { get; set; }
    public DateTime todate { get; set; }
    public string senderLocation { get; set; }
    public string SenderName { get; set; }
    public string SenderId { get; set; }
    public string SenderDepartment { get; set; }
    public string SenderContactno { get; set; }

    public string SENDEREMAIL { get; set; }
    public string SENDERCITY { get; set; }
    public string SENDERPINCODE { get; set; }
    public string SENDERCOUNTRY { get; set; }
    public string RecipientLocation { get; set; }
    public string RecipientName { get; set; }
    public string RecipientId { get; set; }
    public string RecipientDepartment { get; set; }
    public string RecipientContactno { get; set; }
    public string RecipientEmail { get; set; }
    public string RecipientCity { get; set; }
    public string RecipientPincode { get; set; }
    public string RecipientCountry { get; set; }
    public string CourierAgency { get; set; }
    public string AWBPODNO { get; set; }

    public string Mode { get; set; }
    public string ParcelCategory { get; set; }
    public string ParcelDetails { get; set; }
    public string INVOICENO { get; set; }
    public string InvoiceAMOUNT { get; set; }
    public string Weight { get; set; }

    public string NOOFPIECES { get; set; }
    public string DeliveryRemarks { get; set; }
    public List<EDMailDoc> Documents = new List<EDMailDoc>();
    public string LCM_NAME { get; set; }
    public string DESTINATION_LCM_NAME { get; set; }
    
}
public class EDMailDoc
{
    public string DocReqId { get; set; }
    public string DocName { get; set; }
    public string DocPath { get; set; }
}