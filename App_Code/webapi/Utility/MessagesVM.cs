﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UtiltiyVM
{
    public class MessagesVM
    {
        #region HelpDesk Mobile Application Comments

        public const string InvalidCompany = "Invalid Company";
        public const string LoginSuccess = "Successful Login";
        public const string LogoutSuccess = "Logout Successful";
        public const string InvalidLogin = "Invalid Login or Password";
        public const string moreattempts = "Your Account Has been Blocked,Please try after 30 minutes";
        public const string ForgotPassword = "Password Sent To Mail Address";
        public const string Err = "Something Went Wrong";
        public const string MOBil_SPACE = "Successfully Booked";
        public const string ProfileUpdated = "User Profile Updated Successfully";
        public const string InvalidOldPassword = "Invalid Old Password! Check Again";
        public const string ChangePassword = "Password Changed Successfully";
        public const string License = "License Period Expired..Contact Administrator";
        public const string InvalidUserID = "Invalid User Id, Please Check";
        public const string ErrImage = "Upload Image Failed";
        public const string CancelRequest = "Request Cancelled Successfully";
        public const string FeedbackRequest = "Feedback Submitted Successfully";
        public const string RequestRaised = "Request Raised Successfully";
        public const string StatusUpdated = "Status Updated Successfully";
        public const string UPDATE_REQ = "Request(s) Updated Successfully";
        public const string UM_RE_OPEN = "Request Reopened Successfully";
        public const string REQ_REASSIGN_SUBMIT = "Request(s) Reassigned Successfully";
        public const string UPDATE_TOKEN = "Token Updated Successfully";
        public const string UPDATE_PASSWORD = "Password Updated Successfully";
        public const string UM_Not_OK = "NotOK";

        #endregion

        #region
        public const string CNP_INSERTED = "Company Details Added Successfully";
        public const string CNP_MODIFY = "Company Details Modified Successfully";
        public const string CNP_EXIST = "Company Name Already Existed";
        public const string CNP_ERROR = "Something Went Wrong. Try Again Later";

        public const string GRPCNP_INSERTED = "Group Company Details Added Successfully";
        public const string GRPCNP_MODIFY = "Group Company Details Modified Successfully";
        public const string GRPCNP_EXIST = "Group Company Name Already Existed";
        #endregion

        #region Country Master Messages
        public const string CRY_EXISTS = "Country code already exists";
        public const string CRYCHK_UPDATED = "Edited & waiting for approval";
        public const string CRYCHK_INSERT = "Submitted & waiting for approval";
        public const string CRYAPPR_INSERT = "Approved & Inserted successfully";
        public const string CRYINACTIVEMSG = "First inactivate all the regions under this country.";
        public const string CRYCHK_REJECT = "Rejected";
        public const string CRY_OK = "ok";
        public const string CRY_UNMAP = "You have not mapped to any country";
        #endregion

        #region Region Master Messages
        public const string RGN_EXISTS = "Region code already exists";
        public const string RGN_INSERTED = "Submitted & waiting for approval";
        public const string RGN_UPDATED = "Edited & waiting for approval";
        public const string RGNAPPR_INSERT = "Approved & Inserted successfully";
        public const string RGNINACTIVEMSG = "First inactivate all the cities under this region.";
        public const string RGNCHK_REJECT = "Rejected";
        public const string RGN_UNMAP = "You have not mapped to any region";
        #endregion

        #region   City Master Messages
        public const string CTY_EXISTS = "City code already exists";
        public const string CTYCHK_INSERTED = "Submitted & waiting for approval";
        public const string CTYCHK_UPDATED = "Edited & waiting for approval";
        public const string CTYAPPR_INSERT = "Approved & Inserted successfully";
        public const string CTYCHK_REJECT = "Rejected";
        public const string CTYINACTIVEMSG = "First inactivate all the clusters under this city.";
        #endregion

        #region Location Master Messages
        public const string LOC_EXISTS = "Location code already exists";
        public const string LOCCHK_INSERTED = "Submitted & waiting for approval";
        public const string LOCCHK_UPDATED = "Edited & waiting for approval";
        public const string LOCAPPR_INSERT = "Approved & Inserted successfully";
        public const string LOCCHK_REJECT = "Rejected";
        #endregion

        #region Tower Master Messages
        public const string TWR_EXISTS = "Tower code already exists";
        public const string TWRCHK_INSERTED = "Submitted & waiting for approval";
        public const string TWRCHK_UPDATED = "Edited & waiting for approval";
        public const string TWRAPPR_INSERT = "Approved & Inserted successfully";
        public const string TWRCHK_REJECT = "Rejected";
        public const string TWR_UNMAP = "Selected tower is not mapped to any child Category";
        public const string TWR_EMPUNMAP = "Selected tower is not mapped to any tower";
        #endregion

        #region Officetype Master Messages
        public const string OFT_EXISTS = "Office Type code already exists";
        public const string OFTCHK_INSERTED = "Submitted & waiting for approval";
        public const string OFTCHK_UPDATED = "Edited & waiting for approval";
        public const string OFTAPPR_INSERT = "Approved & Inserted successfully";
        public const string OFTCHK_REJECT = "Rejected";
        public const string OFT_EMPMAP = "You have not map to any tower/branch";
        public const string OFT_EMPMAP_BRN = "You have not map to any branch";
        public const string OFT_EMPMAP_TWR = "You have not map to any tower";
        #endregion

        #region Floor Master Messages
        public const string FLR_EXISTS = "Floor code already exists";
        public const string FLRCHK_INSERTED = "Submitted & waiting for approval";
        public const string FLRCHK_UPDATED = "Edited & waiting for approval";
        public const string FLRAPPR_INSERT = "Approved & Inserted successfully";
        public const string FLRCHK_REJECT = "Rejected";
        public const string FLRINACTIVEMSG = "First inactivate all the wings under this floor.";
        #endregion

        #region Privileges Master Messages
        public const string PRG_EXISTS = "Privileges code already exists";
        public const string PRGCHK_INSERTED = "Submitted & waiting for approval";
        public const string PRGCHK_UPDATED = "Edited & waiting for approval";
        public const string PRGAPPR_INSERT = "Approved & Inserted successfully";
        public const string PRGCHK_REJECT = "Rejected";
        public const string PRG_REQ = "/PRVGREQ/";
        #endregion

        #region UserRoleMapping Master Messages
        public const string USR_EXISTS = "Privileges code already exists";
        public const string USRCHK_INSERTED = "Submitted & waiting for approval";
        public const string USRCHK_UPDATED = "Edited & waiting for approval";
        public const string USRAPPR_INSERT = "Approved & Inserted successfully";
        public const string USRCHK_REJECT = "Rejected";
        public const string USR_REQ = "/USRREQ/";
        public const string USR_RGN_MSG = "No regions found under seleted country.";
        public const string USR_CTY_MSG = "No cities found under seleted region.";
        public const string USR_LCM_MSG = "No clusters found under seleted city.";
        public const string USR_TWRBRN_MSG = "No tower/branch found under seleted cluster.";
        public const string USR_FLR_MSG = "No floors found under seleted tower/branch.";
        #endregion

        #region ServiceEscalation Master Messages
        public const string SERESC_EXISTS = "Privileges code already exists";
        public const string SEM_REQ = "/UPLREQ/";
        public const string SERESCCHK_INSERTED = "Submitted & waiting for approval";
        public const string SERESCCHK_UPDATED = "Edited & waiting for approval";
        public const string SERESCAPPR_INSERT = "Approved & Inserted successfully";
        public const string SERERROR = "There is a transcation on this record, please close and come back";
        public const string SERESCCHK_REJECT = "Rejected";
        public const string ALREADY_EXITS = "Record already exists";
        #endregion

        #region Wing Master Messages
        public const string WNG_EXISTS = "Wing code already exists";
        public const string WNGCHK_INSERTED = "Submitted & waiting for approval";
        public const string WNGCHK_UPDATED = "Edited & waiting for approval";
        public const string WNGAPPR_INSERT = "Approved & Inserted successfully";
        public const string WNGCHK_REJECT = "Rejected";
        #endregion

        #region Population Master Messages
        public const string PCN_EXISTS = " Population code already exists";
        public const string PCNCHK_INSERTED = "Submitted & waiting for approval";
        public const string PCNCHK_UPDATED = "Edited & waiting for approval";
        public const string PCNAPPR_INSERT = "Approved & Inserted successfully";
        public const string PCNCHK_REJECT = "Rejected";
        #endregion

        #region Tier Master Messages
        public const string TCN_EXISTS = " Tier code already exists";
        public const string TCNCHK_INSERTED = "Submitted & waiting for approval";
        public const string TCNCHK_UPDATED = "Edited & waiting for approval";
        public const string TCNAPPR_INSERT = "Approved & Inserted successfully";
        public const string TCNCHK_REJECT = "Rejected";
        #endregion

        #region Star Master Messages
        public const string SRC_EXISTS = "Star Category code already exists";
        public const string SRCCHK_INSERTED = "Submitted & waiting for approval";
        public const string SRCCHK_UPDATED = "Edited & waiting for approval";
        public const string SRCAPPR_INSERT = "Approved & Inserted successfully";
        public const string SRCCHK_REJECT = "Rejected";
        #endregion

        #region Branch Category Master Messages
        public const string BRN_EXISTS = "Branch Category code already exists";
        public const string BRNCATCHK_INSERTED = "Submitted & waiting for approval";
        public const string BRNCATCHK_UPDATED = "Edited & waiting for approval";
        public const string BRNCATAPPR_INSERT = "Approved & Inserted successfully";
        public const string BRNCATCHK_REJECT = "Rejected";
        #endregion

        #region Branch Master Messages
        public const string BranchMas_EXISTS = "Branch code already exists";
        public const string BRNCHK_INSERTED = "Submitted & waiting for approval";
        public const string BRNCHK_UPDATED = "Edited & waiting for approval";
        public const string BRNAPPR_INSERT = "Approved & Inserted successfully";
        public const string BRNCHK_REJECT = "Rejected";
        public const string BranchMas_UNMAP = "Selected Branch Not Mapped to Any Region";
        public const string BRN_EMPUNMAP = "Selected office type is not mapped to any branch";
        #endregion

        #region Space Type Master Messages
        public const string SPCTYPE_EXISTS = "Space Type code already exists";
        public const string SPCTYPECHK_INSERTED = "Submitted & waiting for approval";
        public const string SPCTYPECHK_UPDATED = "Edited & waiting for approval";
        public const string SPCTYPEAPPR_INSERT = "Approved & Inserted successfully";
        public const string SPCTYPECHK_REJECT = "Rejected";
        public const string SPCINACTIVEMSG = "In Sub type master there is a transcation on this record, inactive there and come back here.";
        #endregion


        #region Space Sub Type Master Messages
        public const string SPCSUBTYPE_EXISTS = "Space Sub Type code already exists";
        public const string SPCSUBTYPECHK_INSERTED = "Submitted & waiting for approval";
        public const string SPCSUBTYPECHK_UPDATED = "Edited & waiting for approval";
        public const string SPCSUBTYPEAPPR_INSERT = "Approved & Inserted successfully";
        public const string SPCSUBTYPECHK_REJECT = "Rejected";
        #endregion

        #region Vertical Menu Master Messages
        public const string VMM_EXISTS = "Vertical requisition already exists";
        public const string VMMCHK_INSERTED = "Submitted & waiting for approval";
        public const string VMMCHK_UPDATED = "Edited & waiting for approval";
        public const string VMMAPPR_INSERT = "Approved & Inserted successfully";
        public const string VMMCHK_REJECT = "Rejected";
        #endregion

        #region Space Menu Master Messages
        public const string SMM_EXISTS = "Space requisition already exists";
        public const string SMMCHK_INSERTED = "Submitted & waiting for approval";
        public const string SMMCHK_UPDATED = "Edited & waiting for approval";
        public const string SMMAPPR_INSERT = "Approved & Inserted successfully";
        public const string SMMCHK_REJECT = "Rejected";
        #endregion

        #region Shift Master Messages
        public const string SFT_EXISTS = "Shift code already exists";
        public const string SFTCHK_INSERTED = "Submitted & waiting for approval";
        public const string SFTCHK_UPDATED = "Edited & waiting for approval";
        public const string SFTAPPR_INSERT = "Approved & Inserted successfully";
        public const string SFTCHK_REJECT = "Rejected";
        #endregion

        #region HDM Main Category Messages
        public const string MainCat_EXISTS = "Main Category code already exists";
        public const string MainCatCHK_UPDATED = "Edited & waiting for approval";
        public const string MainCatCHK_INSERT = "Submitted & waiting for approval";
        public const string MainCatCHKAPPR_INSERT = "Approved & Inserted successfully";
        public const string MainCatCHK_REJECT = "Rejected";
        public const string Main_EMPUNMAP = "Selected branch/tower is not mapped to main category";
        public const string MAININACTIVEMSG = "In Sub category master there is a transcation on this record, inactive there and come back here.";
        #endregion

        #region Asset Location Master Messages
        public const string ASTLOC_EXISTS = "Asset location code already exists";
        public const string ASTLOCCHK_INSERTED = "Submitted & waiting for approval";
        public const string ASTLOCCHK_UPDATED = "Edited & waiting for approval";
        public const string ASTLOCAPPR_INSERT = "Approved & Inserted successfully";
        public const string ASTLOCCHK_REJECT = "Rejected";
        #endregion

        #region HDM Sub Category Messages
        public const string SubCat_EXISTS = "Sub Category code already exists";
        public const string SubCatCHK_UPDATED = "Edited & waiting for approval";
        public const string SubCatCHK_INSERT = "Submitted & waiting for approval";
        public const string SubCatCHKAPPR_INSERT = "Approved & Inserted successfully";
        public const string SubCatCHK_REJECT = "Rejected";
        public const string SubCat_EMPUNMAP = "Selected main category is not mapped to any sub category";
        public const string SUBINACTIVEMSG = "In Child category master there is a transcation on this record, inactive there and come back here.";
        #endregion

        #region HDM Child Category Messages
        public const string ChildCat_EXISTS = "Child Category code already exists";
        public const string ChildCatCHK_UPDATED = "Edited & waiting for approval";
        public const string ChildCatCHK_INSERT = "Submitted & waiting for approval";
        public const string ChildCatCHKAPPR_INSERT = "Approved & Inserted successfully";
        public const string ChildCatCHK_REJECT = "Rejected";
        public const string ChildCat_MAPPED = "Child Category has already been mapped for this selection. please edit and update";
        public const string childCat_EMPUNMAP = "Selected sub category is not mapped to any child category";
        public const string ChildINACTIVEMSG = "In child category mapping master there is a transcation on this record, inactive there and come back here.";
        #endregion

        #region Branch Child category Mapping Messages
        public const string BCC_INSERTED = "Submitted & waiting for approval";
        public const string BCC_UPDATED = "Editted & waiting for approval";
        public const string BCC_APPROVED = "Approved & Inserted successfully";
        public const string BCC_REJECTED = "Rejected";
        public const string BCC_UNMAP = "Selected branch is not mapped to any child Category";
        public const string ChildmapINACTIVEMSG = "In helpdesk requisition flow there is a transcation on this record, inactive there and come back here.";
        #endregion

        #region Business Master Messages
        public const string BSM_EXISTS = "Business code already exists";
        public const string BSM_INSERTED = "Business inserted successfully";
        public const string BSM_UPDATED = "Business updated Successfully";
        #endregion

        #region Vertical Master Messages
        public const string VER_EXISTS = "Vertical code already exists";
        public const string VER_INSERTED = "Vertical inserted successfully";
        public const string VER_UPDATED = "Vertical updated Successfully";
        #endregion

        #region Costcenter Master Messages
        public const string CST_EXISTS = "Costcenter code already exists";
        public const string CST_INSERTED = "Costcenter inserted successfully";
        public const string CST_UPDATED = "Costcenter updated Successfully";
        #endregion

        #region Process Master Messages
        public const string PO_EXISTS = "Costcenter code already exists";
        public const string PO_INSERTED = "Costcenter inserted successfully";
        public const string PO_UPDATED = "Costcenter updated Successfully";
        #endregion

        #region Designation Master Messages
        public const string DSN_EXISTS = "Designation code already exists";
        public const string DSN_INSERTED = "Designation inserted successfully";
        public const string DSN_UPDATED = "Designation updated Successfully";
        #endregion

        #region Department Master Messages
        public const string DEP_EXISTS = "Department code already exists";
        public const string DEP_INSERTED = "Department Inserted Successfully";
        public const string DEP_UPDATED = "Department updated Successfully";
        #endregion

        #region Main Master Messages
        public const string Main_EXISTS = "Main code already exists";
        public const string Main_INSERTED = "Main Inserted Successfully";
        public const string Main_UPDATED = "Main updated Successfully";
        #endregion

        #region Bulk Space Requisition
        public const string BSR_VERTREQ = "/VERTREQ/";
        public const string BSR_VERTREQDTLS = "/VERTREQDTLS/";
        public const string BSR_OK = "OK";
        public const string BSR_SUBTYPES = "No Spaces Available";
        public const string BSR_INSERTED = "Request Raised Successfully";
        public const string BSR_UPDATED = "Request Updated Successfully";
        public const string BSR_CANCELED = "Request Canceled Successfully";
        public const string BSR_REJECTED = "Request Rejected Successfully";
        public const string BSR_APPROVED = "Request Approved Successfully";
        public const string BSR_ALLOCATED = " Allocation Successfully";
        public const string BSR_BLOCKED = "Requested Seats Blocked Successfully";
        public const string FBD_SUBMIT = "Feedback submitted successfully";
        public const string BSR_NODET = "Please Select atleast one Space ID for to raise request";
        public const string BSR_NO_USER_MAP = "You have not assigned with any privilege to access the above";
        #endregion

        #region Energy Managment

        public const string EM_INSERTED = "Submitted Successfully";
        public const string EM_NO_REC_INSR = "No Records Inserted";

        #endregion

        #region Utility Messages
        public const string UM_OK = "Ok";
        public const string UM_NOTEXISTS = "Code Not Exists";
        public const string UM_NO_REC = "No Records Found";
        #endregion

        #region Request Raise Messages
        public const string RRM_INSERTED = "Request raised successfully";
        public const string RRM_UPDATED = "Request updated successfully";
        public const string RRM_DRAFT = "Request saved successfully";
        public const string RRM_CANCELLED = "Request cancelled successfully";
        public const string RRM_MAP_CATESC_NA = "Selected child category is not defined any escalations/assignments";
        public const string RRM_CAT_ESCL_NA = "SLA is not defined for selected child category or selected branch/tower population code"; // "Selected child category is not defined any SLA";
        public const string RRM_MAP_NA = "Selected child category is not defined both escalations/assignments and SLA";
        public const string RRM_HID_NA = "Selected categories are not mapped with helpdesk incharge";
        public const string RRM_NO_REQ = "No requests raised";
        public const string RRM_ASSG_MODIFY = "Request Assigned Successfully";
        public const string RRM_BRNIC_UNMAP = "Branch Incharge is not mapped";
        #endregion

        #region Check List Model
        public const string CList_INSERTED = "CheckList saved successfully";
        #endregion

        #region Space Requisition
        public const string SPC_REQ = "/SPCREQ/";
        public const string SPC_REQDTLS = "/SPCREQDTLS/";
        public const string SPC_REQ_OK = "OK";
        public const string SPC_REQ_SUBTYPES = "Sub space types are not available to the selected Space type";
        public const string SPC_REQ_INSERTED = "Space Request Raised Successfully";
        public const string SPC_REQ_UPDATED = "Space Request Updated Successfully";
        public const string SPC_REQ_CANCELED = "Space Request Canceled Successfully";
        public const string SPC_REQ_REJECTED = "Space Request Rejected Successfully";
        public const string SPC_REQ_APPROVED = "Space Request Approved Successfully";
        public const string SPC_REQ_CLOSED = "Space Request Closed Successfully";
        #endregion

        #region Space Release Requisition
        public const string SPC_REL_REQ = "/SPCRELREQ/";
        public const string SPC_REL_REQDTLS = "/SPCRELREQDTLS/";
        public const string SPC_REL_REQ_OK = "OK";
        public const string SPC_REL_REQ_INSERTED = "Space Release Request Raised Successfully";
        public const string SPC_REL_REQ_UPDATED = "Space Release Request Updated Successfully";
        public const string SPC_REL_REQ_CANCELED = "Space Release Request Canceled Successfully";
        public const string SPC_REL_REQ_REJECTED = "Space Release Request Rejected Successfully";
        public const string SPC_REL_REQ_APPROVED = "Space Release Request Approved Successfully";
        #endregion

        #region Release Request
        public const string VER_RELREQ = "/VERTRELREQ/";
        public const string VER_RELREQ_INSERTED = "Request Raised Successfully";
        public const string VER_RELREQ_UPDATED = "Request Updated Successfully";
        public const string VER_RELREQ_CANCELED = "Request Canceled Successfully";
        public const string VER_RELREQ_REJECTED = "Request Rejected Successfully";
        public const string VER_RELREQ_APPROVED = "Request Approved Successfully";
        public const string VER_RELREQ_NOREC = "No seats Allocated to the selected Vertical";
        #endregion

        #region Upload Allocation Data
        public const string UAD_OK = "Please click the Link to Download the Available Seats";
        public const string UAD_FAIL = "Download Failed";
        public const string UAD_REQ = "/UPLREQ/";
        public const string UAD_FILEREQ = "Please select the exact Filename which was downloaded earlier";
        public const string UAD_UPLOK = "Uploaded Successfully";
        public const string UAD_UPLOK1 = "Invalid Credentials";
        public const string UAD_UPDOK = "Request submitted successfully";
        public const string UAD_UPLFAIL = "Uploaded Failed";
        public const string UAD_UPLNO = "No Files Uploaded";
        public const string UAD_UPLNOREC = "No Records found in the file uploaded";
        public const string UAD_UPLVALDAT = "Please Upload Valid Data";
        public const string UAD_UPLMOD = "Modified Successfully";
        public const string UAD_UPLCANC = "Canceled Successfully";
        public const string UAD_UPLAPPR = "Approved and Updated Successfully";
        public const string UAD_UPLREJ = "Rejected Successfully";
        public const string UAD_EMPEXISTS = "Employee already Exists";
        #endregion

        #region SLA Master Messages
        public const string SLA_EXISTS = "SLA for the selected criteria already exists";
        public const string SLACHK_UPDATED = "Edited & waiting for approval";
        public const string SLACHK_INSERT = "Submitted & waiting for approval";
        public const string SLAAPPR_INSERT = "Approved & Inserted successfully";
        public const string SLACHK_REJECT = "Rejected";
        public const string SLA_UNMAP = "You have not mapped to any country";
        public const string SLA_REQID = "/SLAREQ/";
        #endregion

        #region Space Projection Details
        public const string PRJ_REC = "No Records Found.";
        public const string PRJ_INSERTED = "Space Projection Details Saved as Draft";
        public const string PRJ_ERROR = "Insertion Failed";
        public const string PRJ_SUBMITTED_MAIL = "Space Projection Details Submitted and Mail sent Successfully.";
        #endregion

        #region Vertical Release Messages
        public const string VR_OK = "Ok";
        public const string VR_SUCCESS = "Selected Spaces Released Successfully";
        public const string VR_NOTEXISTS = "Code Not Exists";
        public const string VR_NO_REC = "No Records Found";
        public const string VR_FAILURE = "Vertical Release Failed Because Requisition has already generated for the space ids :- ";
        #endregion

        #region Space Extension Request
        public const string SER_OK = "No Records Found";
        public const string REQEST_RAISE_SUCCESS = "Space Extension Request has been Submitted Successfully for Selected Spaces.";
        public const string SER_FAIL = "Space Extension Request FAILED";

        public const string SPC_EXT_REQ_INSERTED = "Space Extension Request Raised Successfully";
        public const string SPC_EXT_REQ_UPDATED = "Space Extension Request Updated Successfully";
        public const string SPC_EXT_REQ_CANCELED = "Space Extension Request Canceled Successfully";
        public const string SPC_EXT_REQ_APPROVED = "Space Extension Request Approved Successfully";
        public const string SPC_EXT_REQ_REJECTED = "Space Extension Request Rejected Successfully";
        public const string SPC_UNDER_REQUISITION = "Spaces are Already Under Requisition";

        #endregion

        #region Space Release
        public const string SR_SUCCESS = "Selected Spaces released successfully.";
        public const string SR_FAIL = "Space Release Failed";
        #endregion

        #region HDM Utility Messages
        public const string HDM_UM_OK = "Ok";
        public const string HDM_UM_NOTEXISTS = "Code Not Exists";
        public const string HDM_UM_NO_REC = "No Records Found";
        #endregion
        #region L1 Approval
        public const string L1_NOSPC = "Please Select Atlest One Space to Approve Or Reject";
        public const string L1_NOREQ = "Please Select Atlest One Requisition to Approve Or Reject";
        #endregion

        #region Space Allocation
        public const string SPC_ALL_SUC = "Spaces Allocated successfully";

        #endregion
        #region Space Allocation		
        public const string SPC_ALL_CLS = "Spaces Closed successfully";
        #endregion

        #region Employee Mapping
        public const string EMP_ALLOC_SUC = "Employee Mapped successfully";
        public const string EMP_NO_REC = "No Employee Under selected BU&LOB";

        #endregion


        #region Admin Functions
        public const string AF_SUCCESS = "Privileges Added Successfully.";
        public const string AF_OK = "OK";
        #endregion

        #region Error Message
        public const string ErrorMessage = "Something Went Wrong. Try Later.";
        #endregion

        #region Conference Master Message
        public const string Facility_Success = "Data Inserted Successfully";
        public const string Facility_Modify = "Data Modified Successfully";
        public const string Facility_InsertError = "Code Already Exists";
        public const string Facility_UpdateError = "Failed To Update,Please Check Again!!";
        #endregion

        #region Map Messages
        public const string SelectCST = "Please Select ";
        #endregion

        #region Guest House Booking System
        public const string AddFacility_Exists = "Facility Name Already Exists";
        public const string AddFacility_Inserted = "Facility Added Successfully";
        public const string AddFacility_Updated = "Facility  Updated Successfully";
        public const string GH_BOOKED = "Transit Residence Booked Successfully";
        public const string RB_UPDATED = "Room Booking Updated Successfully";
        public const string RB_CANCELED = "Room Booking Canceled Successfully";
        public const string GH_WH_Added = "Transit Residence Withhold Successfully";
        public const string GH_WH_Modified = "Transit Residence Withhold Updated Successfully";
        public const string GH_WH_Cancelled = "Transit Residence Withhold Canceled Successfully";
        public const string GH_FAIL = "BOOKING FAILED, TIMESLOT NOT AVAILABLE.";
        #endregion

        #region Property Messages
        public const string PM_SER_TYPE_OK = "Service Type Saved Successfuly";
        public const string PM_EXISTS = "Service Type Name Already Exists";
        public const string PM_NO_REC = "No Records";
        #endregion

        #region Area Type Message
        public const string AR_Success = "Data Inserted Successfully";
        public const string AR_Modify = "Data Modified Successfully";
        public const string AR_UpdateError = "Failed To Update,Please Check Again!!";
        public const string Factsheet = "Sumitted Successfully";
        #endregion

        #region  UploadExcelPropertyandLeaseData 
        public const string ExcelValidations = "Please Enter All Required Fields in Excel";
        #endregion

        #region  Copilot 
        public const string Cop_Reg = "Please Buy CoPilot+ for this functionality";
        #endregion
    }
}