﻿using QuickFMS.API.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using UtiltiyVM;


public class UtilityController : ApiController
{
    UtilityService uservc = new UtilityService();

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetSysPreferences()
    {
        var obj = uservc.GetSysPreferences();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetCountries([FromUri] int id)
    {
        var obj = uservc.GetCountries(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetRoleAndReportingManger()
    {
        var obj = uservc.GetRoleAndReportingManger();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetEmployee()
    {
        var obj = uservc.GetEmployee();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetAllEmployees([FromUri] string  text)
    {
        var obj = uservc.GetAllEmployees(text);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage ValidateUserFirstLogin()
    {
        var obj = uservc.ValidateUserFirstLogin();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetLocationsByCostcenters([FromUri] int id, [FromBody] List<Costcenterlst> cost)
    {
        var obj = uservc.GetLocationsByCostcenters(cost, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetCities([FromUri] int id)
    {
        var obj = uservc.GetCities(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetLocations([FromUri] int id)
    {
        var obj = uservc.GetLocations(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetLocationsInventory([FromUri] int id)
    {
        var obj = uservc.GetLocationsInventory(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage getEmployeeType([FromUri] int id)
    {
        var obj = uservc.getEmployeeType(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getallzones()
    {
        var obj = uservc.Getallzones();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetLocationsall([FromUri] int id)
    {
        var obj = uservc.GetLocationsall(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getmaincat()
    {
        var obj = uservc.Getmaincat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getsubcat()
    {
        var obj = uservc.Getsubcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getchildcat()
    {
        var obj = uservc.Getchildcat();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Getstatus()
    {
        var obj = uservc.Getstatus();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetTowers([FromUri] int id)
    {
        var obj = uservc.GetTowers(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetFloors([FromUri] int id)
    {
        var obj = uservc.GetFloors(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [HttpPost]
    public HttpResponseMessage GetAssetPOid([FromUri] string Reqid)
    {
        var obj = uservc.GetAssetPOid(Reqid);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetVerticalsApi([FromUri] int id, String client, String userid)
    {
        HttpContext.Current.Session["TENANT"] = "[" + client + "]" + ".[dbo]";
        HttpContext.Current.Session["UID"] = userid;
        HttpContext.Current.Session["COMPANYID"] = "1";
        var obj = uservc.GetVerticals(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetVerticals([FromUri] int id)
    {
        var obj = uservc.GetVerticals(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage getAurNames()
    {
        var obj = uservc.getAurNames();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetVerticalsByFlrid([FromUri] string id)
    {
        var obj = uservc.GetVerticalsByFlrid(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetDepartments()
    {
        var obj = uservc.GetDepartments();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetDesignation()
    {
        var obj = uservc.GetDesignation();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetCostCenters([FromUri] int id)
    {
        var obj = uservc.GetCostCenters(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetCostCentersByFlrid([FromUri] string id)
    {
        var obj = uservc.GetCostCentersByFlrid(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCountriesById([FromBody] Countrylst cny)
    {
        var obj = uservc.GetCountriesById(cny);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCitiesBycountry([FromUri] int id, [FromBody] List<Countrylst> cny)
    {
        var obj = uservc.GetCitiesBycountry(cny, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getlocbyzone([FromBody] List<ZNlst> zone)
    {
        var obj = uservc.getlocbyzone(zone);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getsubbymain([FromBody] List<MAINCATlst> main)
    {
        var obj = uservc.getsubbymain(main);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage getchildbysub([FromBody] List<SUBCATlst> sub)
    {
        var obj = uservc.getchildbysub(sub);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetLocationsByCity([FromUri] int id, [FromBody] List<Citylst> city)
    {
        var obj = uservc.GetLocationsByCity(city, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetTowerByLocation([FromUri] int id, [FromBody] List<Locationlst> Location)
    {
        var obj = uservc.GetTowerByLocation(Location, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetFloorByTower([FromUri] int id, [FromBody] List<Towerlst> tower)
    {
        var obj = uservc.GetFloorByTower(tower, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetVerticalByChildEntity([FromUri] int id, [FromBody] List<ChildEntityLst> child)
    {
        var obj = uservc.GetVerticalByChildEntity(child, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCostcenterByVertical([FromUri] int id, [FromBody] List<Verticallst> vertical)
    {
        var obj = uservc.GetCostcenterByVertical(vertical, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetCostcenterByVerticalCode([FromUri] int id, [FromBody] Verticallst vertical)
    {
        var obj = uservc.GetCostcenterByVerticalCode(vertical, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetRoleByUserId()
    {
        var obj = uservc.GetRoleByUserId();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // Get role id by user id//
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetRoleID()
    {
        var obj = uservc.GetRoleID();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetSpacetypeByFloor([FromUri] int id, [FromBody] List<Floorlst> floors)
    {
        var obj = uservc.GetSpacetypeByFloor(floors, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetParentEntity([FromUri] int id)
    {
        var obj = uservc.GetParentEntity(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetChildEntity([FromUri] int id)
    {
        var obj = uservc.GetChildEntity(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetChildEntityByParent([FromUri] int id, [FromBody] List<ParentEntityLst> pel)
    {
        var obj = uservc.GetChildEntityByParent(pel, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetBussHeirarchy()
    {
        var obj = uservc.GetBussHeirarchy();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetRoles([FromUri] int id)
    {
        var obj = uservc.GetRoles(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetCompanies()
    {
        var obj = uservc.GetAllCompanies();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    // guest house booking
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetReservationTypes([FromUri] int id)
    {
        var obj = uservc.GetReservationTypes(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    // get employee name and id for autocomplete

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetEmployeeNameAndID(string q)
    {
        var obj = uservc.GetEmployeeNameAndID(q);

        var response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetEmployeeTypes()
    {
        var obj = uservc.GetEmployeeTypes();

        var response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetEmpEmailByID(string ID)
    {
        var obj = uservc.GetEmpEmailByID(ID);

        var response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;

    }

    //get all users
    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetAllUsers()
    {
        var user = uservc.GetAllUsers();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetTypes()
    {
        var user = uservc.GetTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [GzipCompression]
    public HttpResponseMessage GetSubTypes()
    {
        var user = uservc.GetSubTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [GzipCompression]
    public HttpResponseMessage GetVendors()
    {
        var user = uservc.GetVendors();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [GzipCompression]
    public HttpResponseMessage GetBrand()
    {
        var user = uservc.GetBrand();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [GzipCompression]
    public HttpResponseMessage GetModel()
    {
        var user = uservc.GetModel();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }
    // get facility names by types
    [GzipCompression]
    [HttpPost]

    public HttpResponseMessage GetFacilityNamesbyType([FromUri] int id, [FromBody] Nametype type)
    {
        var obj = uservc.GetFacilityNamesbyType(type, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
           return response;
    }
    //public HttpResponseMessage GetFacilityNamesbyType([FromUri] int id, [FromBody] List<ReservationType> rt)
    //{
    //    var obj = uservc.GetFacilityNamesbyType(rt, id);
    //    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
    //    return response;
    //}

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetPropTypes()
    {
        var user = uservc.GetPropTypes();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, user);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetProperties([FromBody] TenantReportModel objVal)
    {
        var obj = uservc.GetPropertyNames(objVal);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    public HttpResponseMessage GetCategories([FromUri] int id)
    {
        var obj = uservc.GetCategories(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    public HttpResponseMessage GetSubCategories([FromUri] int id)
    {
        var obj = uservc.GetSubCategories(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]

    public HttpResponseMessage GetBrands([FromUri] int id)
    {
        var obj = uservc.GetBrands(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]

    public HttpResponseMessage GetModels([FromUri] int id)
    {
        var obj = uservc.GetModels(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage Get_GH_Timings()
    {
        var obj = uservc.Get_GH_Timings();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetFacilityNamesbyLocation([FromUri] int id, [FromBody] List<Locationlst> lcm)
    {
        var obj = uservc.GetFacilityNamesbyLocation(lcm, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetReservationTypesByFacility([FromUri] int id, [FromBody] List<FacilityNamelst> fnamelst)
    {
        var obj = uservc.GetReservationTypesByFacility(id, fnamelst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetCountriesWithoutSession([FromUri] int id)
    {
        var obj = uservc.GetCountriesWithoutSession(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetCitiesWithoutSession([FromUri] int id)
    {
        var obj = uservc.GetCitiesWithoutSession(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpGet]
    public HttpResponseMessage GetLocationsWithoutSession([FromUri] int id)
    {
        var obj = uservc.GetLocationsWithoutSession(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }



    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetFacilityNamesbyLocationWithoutSession([FromUri] int id, [FromBody] List<Locationlst> lcm)
    {
        var obj = uservc.GetFacilityNamesbyLocationWithoutSession(lcm, id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage GetContractCategories([FromUri] int id)
    {
        var obj = uservc.GetContractCategories(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage GetContractSubcategories([FromUri] string id)
    {
        var obj = uservc.GetContractSubcategories(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    public HttpResponseMessage GetContractStatus()
    {
        var obj = uservc.GetContractStatus();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    [GzipCompression]
    [HttpPost]
    public HttpResponseMessage GetReservationTypesByFacilityWithoutSession([FromUri] int id, [FromBody] List<FacilityNamelst> fnamelst)
    {
        var obj = uservc.GetReservationTypesByFacilityWithoutSession(id, fnamelst);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }
    [GzipCompression]
    public HttpResponseMessage GetAssetTypes([FromUri] int id)
    {
        var obj = uservc.GetAssetTypes(id);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

}
