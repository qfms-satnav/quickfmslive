﻿using Microsoft.Reporting.WebForms;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using UtiltiyVM;
/// <summary>
/// Summary description for UtilityService
/// </summary>
public class UtilityService
{
    SubSonic.StoredProcedure sp;

    public object GetSysPreferences()
    {
        List<SysPreference> SysPrflst = new List<SysPreference>();
        SysPreference SysPrf;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SYSTEM_PREFERENCE_ADM");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SysPrf = new SysPreference();
                SysPrf.SYSP_CODE = sdr["SYSP_CODE"].ToString();
                SysPrf.SYSP_VAL1 = sdr["SYSP_VAL1"].ToString();
                SysPrf.SYSP_VAL2 = sdr["SYSP_VAL2"].ToString();
                SysPrf.SYSP_OPR = sdr["SYSP_OPR"].ToString();
                SysPrf.SYSP_STA_ID = Convert.ToInt32(sdr["SYSP_STA_ID"]);
                SysPrflst.Add(SysPrf);
            }
        }
        if (SysPrflst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SysPrflst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    public object GetLocationsByCostcenters(List<Costcenterlst> cost, int id)
    {
        List<Locationlst> loclst = new List<Locationlst>();
        Locationlst loc;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@COST_LIST", SqlDbType.Structured);
        if (cost == null)
        {
            param[1].Value = null;
        }
        else
        {
            param[1].Value = ConvertToDataTable(cost);
        }
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_LOCATIONS_BY_OCCUPIED_COSTCENTERS", param))
        {
            while (sdr.Read())
            {
                loc = new Locationlst();
                loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                loc.ticked = false;
                loclst.Add(loc);
            }
        }
        if (loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetRoleAndReportingManger()
    {
        List<GetRoleAndRM> RM_lst = new List<GetRoleAndRM>();
        GetRoleAndRM RM;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ROLE_AND_REPORTING_MNAGER_BY_USER_ID");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                RM = new GetRoleAndRM();
                RM.AUR_ROLE = sdr["URL_ROL_ID"].ToString();
                RM.AUR_REPOTING_TO = sdr["AUR_REPORTING_TO"].ToString();
                RM.AUR_ID = sdr["AUR_ID"].ToString();
                RM.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                RM.VER_CODE = sdr["VER_CODE"].ToString();
                RM.VER_NAME = sdr["VER_NAME"].ToString();
                RM.COST_CENTER_CODE = sdr["COST_CENTER_CODE"].ToString();
                RM.COST_CENTER_NAME = sdr["COST_CENTER_NAME"].ToString();
                RM_lst.Add(RM);
            }
        }
        if (RM_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = RM_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetEmployee()
    {
        List<GetEMPID> RM_lst = new List<GetEMPID>();
        GetEMPID RM;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_EMPLOYEE_ID");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                RM = new GetEMPID();
                RM.AUR_ID = sdr["id"].ToString();
                RM.AUR_NAME = sdr["name"].ToString();
                RM.AUR_PRJ_CODE = sdr["AUR_PRJ_CODE"].ToString();
                RM.AUR_VERT_CODE = sdr["AUR_VERT_CODE"].ToString();
                RM_lst.Add(RM);
            }
        }
        if (RM_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = RM_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetCountries(int id)
    {
        List<Countrylst> cnylst = new List<Countrylst>();
        Countrylst cny;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Countries_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cny = new Countrylst();
                cny.CNY_CODE = sdr["CNY_CODE"].ToString();
                cny.CNY_NAME = sdr["CNY_NAME"].ToString();
                cny.ticked = false;
                cnylst.Add(cny);
            }
        }
        if (cnylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = cnylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }
    public object GetAssetPOid(string Reqid)
    {
        //sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Po_update_finalise");
        //sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.AddParameter("@POID", Reqid, DbType.String);
        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@POID", SqlDbType.VarChar);
        param[1].Value = Reqid;
        Object o = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "Get_Po_update_finalise", param);

        return new { Message = MessagesVM.UM_OK, data = "Success" };
    }
    public object GetCities(int id)
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CITY_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.CNY_CODE = sdr["CNY_CODE"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    //public object GetLocations(int id)
    //{
        
    //    List<Locationlst> Loc_lst = new List<Locationlst>();
    //    Locationlst Loc;
    //    sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ADM_IN");
    //    //sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
    //    sp.Command.AddParameter("@AUR_ID", "1", DbType.String);
    //    //if (HttpContext.Current.Session["Zone"].ToString() == "All")
    //    //{
    //    //    id = 1;
    //    //}
    //    id = 1;
    //    sp.Command.AddParameter("@MODE", id, DbType.Int32);
    //    //sp.Command.AddParameter("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
    //    sp.Command.AddParameter("@ZONE", "South", DbType.String);
    //    //sp.Command.AddParameter("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
    //    sp.Command.AddParameter("@LCM_DIVISION", "", DbType.String);
    //    using (IDataReader sdr = sp.GetReader())
    //    {
    //        while (sdr.Read())
    //        {
    //            Loc = new Locationlst();
    //            Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
    //            Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
    //            Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
    //            Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
    //            Loc.ticked = false;
    //            Loc_lst.Add(Loc);
    //        }
    //    }
    //    if (Loc_lst.Count != 0)
    //        return new { Message = MessagesVM.UM_OK, data = Loc_lst };
    //    else
    //        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    //}
    public object GetLocations(int id)
    {

        List<Locationlst> Loc_lst = new List<Locationlst>();
        Locationlst Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);

        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new Locationlst();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetLocationsInventory(int id)
    {
        List<Locationlst> Loc_lst = new List<Locationlst>();
        Locationlst Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ADM_IN");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        if (HttpContext.Current.Session["Zone"].ToString() == "All")
        {
            id = 1;
        }
        id = 1;
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        sp.Command.AddParameter("@ZONE", HttpContext.Current.Session["Zone"], DbType.String);
        sp.Command.AddParameter("@LCM_DIVISION", HttpContext.Current.Session["LCM_DIVISION"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new Locationlst();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getEmployeeType(int id)
    {
        List<Aurgrade> EMP_TYPE = new List<Aurgrade>();
        Aurgrade Aurg;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_EMPLOYEE_GRADE");
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Aurg = new Aurgrade();
                Aurg.AUR_GRADE = sdr["AUR_GRADE"].ToString();
                EMP_TYPE.Add(Aurg);
            }
        }
        if (EMP_TYPE.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = EMP_TYPE };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getallzones()
    {
        List<ZNlst> zn_lst = new List<ZNlst>();
        ZNlst zn;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_ZONE_all]");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        //sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                zn = new ZNlst();
                zn.ZN_CODE = sdr["ZN_CODE"].ToString();
                zn.ZN_NAME = sdr["ZN_NAME"].ToString();
                zn.ticked = false;
                zn_lst.Add(zn);
            }
        }
        if (zn_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = zn_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object GetLocationsall(int id)
    {
        List<LCMlst> Loc_lst = new List<LCMlst>();
        LCMlst Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ADM_all");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new LCMlst();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
   
    public object Getmaincat()
    {
        List<MAINCATlst> MAIN_lst = new List<MAINCATlst>();
        MAINCATlst MAIN;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_MAIN_CAT");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                MAIN = new MAINCATlst();
                MAIN.MNC_CODE = sdr["MNC_CODE"].ToString();
                MAIN.MNC_NAME = sdr["MNC_NAME"].ToString();
                MAIN.ticked = false;
                MAIN_lst.Add(MAIN);
            }
        }
        if (MAIN_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = MAIN_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object Getsubcat()
    {
        List<SUBCATlst> SUB_lst = new List<SUBCATlst>();
        SUBCATlst SUB;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SUB_CAT");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SUB = new SUBCATlst();
                SUB.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                SUB.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                SUB.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                SUB.ticked = false;
                SUB_lst.Add(SUB);
            }
        }
        if (SUB_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SUB_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Getchildcat()
    {
        List<CHILDCATlst> CHILD_lst = new List<CHILDCATlst>();
        CHILDCATlst CHILD;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHILD_CAT");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CHILD = new CHILDCATlst();
                CHILD.CHC_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                CHILD.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                CHILD.CHC_TYPE_SUBC_CODE = sdr["CHC_TYPE_SUBC_CODE"].ToString();
                CHILD.CHC_TYPE_MNC_CODE = sdr["CHC_TYPE_MNC_CODE"].ToString();
                CHILD.ticked = false;
                CHILD_lst.Add(CHILD);
            }
        }
        if (CHILD_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = CHILD_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object Getstatus()
    {
        List<STATUSlst> STATUS_lst = new List<STATUSlst>();
        STATUSlst STATUS;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_STATUS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                STATUS = new STATUSlst();
                STATUS.STA_ID = sdr["STA_ID"].ToString();
                STATUS.STA_TITLE = sdr["STA_TITLE"].ToString();
                STATUS.ticked = false;
                STATUS_lst.Add(STATUS);
            }
        }
        if (STATUS_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = STATUS_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetTowers(int id)
    {
        List<Towerlst> Tow_lst = new List<Towerlst>();
        Towerlst Tow;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_TOWER_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Tow = new Towerlst();
                Tow.TWR_CODE = sdr["TWR_CODE"].ToString();
                Tow.TWR_NAME = sdr["TWR_NAME"].ToString();
                Tow.LCM_CODE = sdr["LCM_CODE"].ToString();
                Tow.CTY_CODE = sdr["CTY_CODE"].ToString();
                Tow.CNY_CODE = sdr["CNY_CODE"].ToString();
                Tow.ticked = false;
                Tow_lst.Add(Tow);
            }
        }
        if (Tow_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Tow_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetFloors(int id)
    {
        List<Floorlst> Flr_lst = new List<Floorlst>();
        Floorlst Flr;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_FLOOR_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Flr = new Floorlst();
                Flr.FLR_CODE = sdr["FLR_CODE"].ToString();
                Flr.FLR_NAME = sdr["FLR_NAME"].ToString();
                Flr.TWR_CODE = sdr["TWR_CODE"].ToString();
                Flr.LCM_CODE = sdr["LCM_CODE"].ToString();
                Flr.CTY_CODE = sdr["CTY_CODE"].ToString();
                Flr.CNY_CODE = sdr["CNY_CODE"].ToString();
                Flr.ticked = false;
                Flr_lst.Add(Flr);
            }
        }
        if (Flr_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Flr_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetVerticals(int id)
    {
        List<Verticallst> verlst = new List<Verticallst>();
        Verticallst ver;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Vertical_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                ver = new Verticallst();
                ver.VER_CODE = sdr["VER_CODE"].ToString();
                ver.VER_NAME = sdr["VER_NAME"].ToString();
                ver.ticked = false;
                verlst.Add(ver);
            }
        }
        if (verlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object getAurNames()
    {
        DataSet ds = new DataSet();
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Employees");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
        ds = sp.GetDataSet();

        if (ds.Tables.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ds.Tables[0] };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetVerticalsByFlrid(string id)
    {
        List<Verticallst> verlst = new List<Verticallst>();
        Verticallst ver;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_VERTICAL_BY_FLRID");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FLR_ID", id, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                ver = new Verticallst();
                ver.VER_CODE = sdr["VER_CODE"].ToString();
                ver.VER_NAME = sdr["VER_NAME"].ToString();
                ver.ticked = false;
                verlst.Add(ver);
            }
        }
        if (verlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetDepartments()
    {
        List<Department> verlst = new List<Department>();
        Department ver;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_DEPARTMENTS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                ver = new Department();
                ver.DEP_NAME = sdr["DEP_NAME"].ToString();
                ver.DEP_CODE = sdr["DEP_CODE"].ToString();
                ver.ticked = false;
                verlst.Add(ver);
            }
        }
        if (verlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetDesignation()
    {
        List<Designation> dsglst = new List<Designation>();
        Designation dsg;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_DESIGNATION");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                dsg = new Designation();
                dsg.DSG_CODE = sdr["DSN_CODE"].ToString();
                dsg.DSG_NAME = sdr["DSN_AMT_TITLE"].ToString();
                dsg.ticked = false;
                dsglst.Add(dsg);
            }
        }
        if (dsglst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = dsglst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetCostCenters(int id)
    {
        List<Costcenterlst> Cst_lst = new List<Costcenterlst>();
        Costcenterlst CST;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_COSTCENTERS_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CST = new Costcenterlst();
                CST.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                CST.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                CST.Vertical_Code = sdr["Vertical_Code"].ToString();
                CST.ticked = false;
                Cst_lst.Add(CST);
            }
        }
        if (Cst_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Cst_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCostCentersByFlrid(string id)
    {
        List<Costcenterlst> Cst_lst = new List<Costcenterlst>();
        Costcenterlst CST;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_COSTCENTERS_BY_FLRID");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@FLR_ID", id, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                CST = new Costcenterlst();
                CST.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                CST.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                CST.Vertical_Code = sdr["Vertical_Code"].ToString();
                CST.ticked = false;
                Cst_lst.Add(CST);
            }
        }
        if (Cst_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Cst_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCountriesById(UtiltiyVM.Countrylst cny)
    {
        return new { Message = "", data = (object)null };
    }

    public object GetCitiesBycountry(List<Countrylst> cny, int id)
    {
        List<Citylst> citylst = new List<Citylst>();
        Citylst city;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CNYLST", SqlDbType.Structured);
        if (cny == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(cny);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CITY_BY_CNYLST_ADM", param))
        {
            while (sdr.Read())
            {
                city = new Citylst();
                city.CTY_CODE = sdr["CTY_CODE"].ToString();
                city.CTY_NAME = sdr["CTY_NAME"].ToString();
                city.CNY_CODE = sdr["CNY_CODE"].ToString();
                city.ticked = false;
                citylst.Add(city);
            }
        }
        if (citylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = citylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object getlocbyzone(List<ZNlst> zone)
    {
        try
        {

            List<ZNBYLOClst> lst = new List<ZNBYLOClst>();
            ZNBYLOClst znbyloc;

            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@ZONELST", SqlDbType.Structured);
            if (zone == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = ConvertToDataTable(zone);
            }


            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "[GET_ZONE_BY_LOC]", param))
            {
                while (sdr.Read())
                {
                    znbyloc = new ZNBYLOClst();
                    znbyloc.LCM_CODE = sdr["LCM_CODE"].ToString();
                    znbyloc.LCM_NAME = sdr["LCM_NAME"].ToString();
                    znbyloc.ZN_CODE = sdr["LCM_ZONE"].ToString();
                    znbyloc.ticked = false;
                    lst.Add(znbyloc);
                }
            }


            if (lst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = lst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception ex)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }

    }

    public object getsubbymain(List<MAINCATlst> main)
    {
        List<SUBCATlst> SUBlst = new List<SUBCATlst>();
        SUBCATlst sub;

        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@MAINLST", SqlDbType.Structured);
        if (main == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(main);
        }
        
       
       using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_SUB_BY_MAIN", param))
        {
            while (sdr.Read())
            {
                sub = new SUBCATlst();
                sub.SUBC_CODE = sdr["SUBC_CODE"].ToString();
                sub.SUBC_NAME = sdr["SUBC_NAME"].ToString();
                sub.MNC_CODE = sdr["SUBC_MNC_CODE"].ToString();
                sub.ticked = false;
                SUBlst.Add(sub);
            }
        }
        if (SUBlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SUBlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object getchildbysub(List<SUBCATlst> sub)
    {
        List<CHILDCATlst> childlst = new List<CHILDCATlst>();
        CHILDCATlst child;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@SUBLST", SqlDbType.Structured);
        if (sub == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(sub);
        }
       
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CHILD_BY_SUB", param))
        {
            while (sdr.Read())
            {
                child = new CHILDCATlst();
                child.CHC_TYPE_CODE = sdr["CHC_TYPE_CODE"].ToString();
                child.CHC_TYPE_NAME = sdr["CHC_TYPE_NAME"].ToString();
                child.CHC_TYPE_SUBC_CODE = sdr["CHC_TYPE_SUBC_CODE"].ToString();
                child.CHC_TYPE_MNC_CODE = sdr["CHC_TYPE_MNC_CODE"].ToString();
                child.ticked = false;
                childlst.Add(child);
            }
        }
        if (childlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = childlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetLocationsByCity(List<Citylst> city, int id)
    {
        List<Locationlst> loclst = new List<Locationlst>();
        Locationlst loc;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CITYLST", SqlDbType.Structured);
        if (city == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(city);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_LOCATION_BY_CITYLST_ADM", param))
        {
            while (sdr.Read())
            {
                loc = new Locationlst();
                loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                loc.ticked = false;
                loclst.Add(loc);
            }
        }
        if (loclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = loclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetTowerByLocation(List<Locationlst> Loc, int id)
    {
        List<Towerlst> towerlst = new List<Towerlst>();
        Towerlst twr;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@LOCLST", SqlDbType.Structured);
        if (Loc == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(Loc);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_TOWER_BY_LOCATIONLST_ADM", param))
        {
            while (sdr.Read())
            {
                twr = new Towerlst();
                twr.TWR_CODE = sdr["TWR_CODE"].ToString();
                twr.TWR_NAME = sdr["TWR_NAME"].ToString();
                twr.LCM_CODE = sdr["LCM_CODE"].ToString();
                twr.CTY_CODE = sdr["CTY_CODE"].ToString();
                twr.CNY_CODE = sdr["CNY_CODE"].ToString();
                twr.ticked = false;
                towerlst.Add(twr);
            }
        }
        if (towerlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = towerlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetRoleByUserId()
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_MAP_GETROLENOFORUSER");
        sp.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            if (sdr.Read())
            {
                return new { Message = MessagesVM.UM_OK, data = new { Rol_id = sdr["NAME"] } };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    // Get Role id by User id/
    public object GetRoleID()
    {


        List<RoleId> Rollist = new List<RoleId>();
        RoleId Rllist;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ROLE_BY_USER_ID");
        sp.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Rllist = new RoleId();
                Rllist.ROL_ID = sdr["ROL_ID"].ToString();
                Rllist.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                Rllist.URL_USR_ID = sdr["URL_USR_ID"].ToString();

                Rollist.Add(Rllist);
            }
        }
        if (Rollist.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Rollist };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
       
    }

    public object GetFloorByTower(List<Towerlst> tower, int id)
    {
        List<Floorlst> floorlst = new List<Floorlst>();
        Floorlst flr;
        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@TWRLST", SqlDbType.Structured);

        if (tower == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(tower);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_FLOOR_BY_TOWERLST_ADM", param))
        {
            while (sdr.Read())
            {
                flr = new Floorlst();
                flr.FLR_CODE = sdr["FLR_CODE"].ToString();
                flr.FLR_NAME = sdr["FLR_NAME"].ToString();
                flr.TWR_CODE = sdr["TWR_CODE"].ToString();
                flr.LCM_CODE = sdr["LCM_CODE"].ToString();
                flr.CTY_CODE = sdr["CTY_CODE"].ToString();
                flr.CNY_CODE = sdr["CNY_CODE"].ToString();
                flr.ticked = false;
                floorlst.Add(flr);
            }
        }
        if (floorlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = floorlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetVerticalByChildEntity(List<ChildEntityLst> child, int id)
    {
        List<Verticallst> verlst = new List<Verticallst>();
        Verticallst vert;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@CHILD", SqlDbType.Structured);

        if (child == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(child);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_VERTICAL_BY_CHILD_ENTITY_ADM", param))
        {
            while (sdr.Read())
            {
                vert = new Verticallst();
                vert.VER_CODE = sdr["VER_CODE"].ToString();
                vert.VER_NAME = sdr["VER_NAME"].ToString();
                vert.ticked = false;
                verlst.Add(vert);
            }
        }
        if (verlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = verlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCostcenterByVertical(List<Verticallst> Vertcal, int id)
    {
        List<Costcenterlst> costlst = new List<Costcenterlst>();
        Costcenterlst cost;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@VERLST", SqlDbType.Structured);

        if (Vertcal == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(Vertcal);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_COSTCENTER_BY_VERTICAL_ADM", param))
        {
            while (sdr.Read())
            {
                cost = new Costcenterlst();
                cost.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                cost.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                cost.Vertical_Code = sdr["Vertical_Code"].ToString();
                cost.ticked = false;
                costlst.Add(cost);
            }
        }
        if (costlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = costlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCostcenterByVerticalCode(Verticallst Vertcal, int id)
    {
        List<Costcenterlst> costlst = new List<Costcenterlst>();
        Costcenterlst cost;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@VER_CODE", SqlDbType.NVarChar);
        param[0].Value = Vertcal.VER_CODE;
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_COSTCENTER_BY_VERTICALCODE", param))
        {
            while (sdr.Read())
            {
                cost = new Costcenterlst();
                cost.Cost_Center_Code = sdr["Cost_Center_Code"].ToString();
                cost.Cost_Center_Name = sdr["Cost_Center_Name"].ToString();
                cost.Vertical_Code = sdr["Vertical_Code"].ToString();
                cost.ticked = false;
                costlst.Add(cost);
            }
        }
        if (costlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = costlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSpacetypeByFloor(List<Floorlst> floors, int mode)
    {
        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetParentEntity(int id)
    {
        List<ParentEntityLst> pelst = new List<ParentEntityLst>();
        ParentEntityLst pe;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_PARENTENTITY_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                pe = new ParentEntityLst();
                pe.PE_CODE = sdr["PE_CODE"].ToString();
                pe.PE_NAME = sdr["PE_NAME"].ToString();
                pe.ticked = false;
                pelst.Add(pe);
            }
        }
        if (pelst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = pelst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetChildEntity(int id)
    {
        List<ChildEntityLst> chlst = new List<ChildEntityLst>();
        ChildEntityLst che;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CHILDENTITY_ADM");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                che = new ChildEntityLst();
                che.CHE_CODE = sdr["CHE_CODE"].ToString();
                che.CHE_NAME = sdr["CHE_NAME"].ToString();
                che.PE_CODE = sdr["CHE_PE_CODE"].ToString();
                che.ticked = false;
                chlst.Add(che);
            }
        }
        if (chlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = chlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetChildEntityByParent(List<ParentEntityLst> pel, int id)
    {
        List<ChildEntityLst> chdlst = new List<ChildEntityLst>();
        ChildEntityLst chd;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@PARENTLST", SqlDbType.Structured);
        if (pel == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(pel);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_CHILDENTITY_BY_PARENT_ADM", param))
        {
            while (sdr.Read())
            {
                chd = new ChildEntityLst();
                chd.CHE_CODE = sdr["CHE_CODE"].ToString();
                chd.CHE_NAME = sdr["CHE_NAME"].ToString();
                chd.PE_CODE = sdr["PE_CODE"].ToString();
                chd.ticked = false;
                chdlst.Add(chd);
            }
        }
        if (chdlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = chdlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetBussHeirarchy()
    {
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "AMT_BSM_GETALL"))
        {
            if (sdr.Read())
            {
                return new { Message = MessagesVM.UM_OK, data = new { Parent = sdr["AMT_BSM_PARENT"], Child = sdr["AMT_BSM_CHILD"],
                    BH1 = sdr["AMT_BH_ONE"],BH2 = sdr["AMT_BH_TWO"],PE= sdr["AMT_PARENT_ENTITY"],CE = sdr["AMT_CHILD_ENTITY"]} };
            }
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
    }

    public object GetRoles(int id)
    {
        List<Rolelst> Rollst = new List<Rolelst>();
        Rolelst role;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GETROLES");
        sp.Command.AddParameter("@dummy", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                role = new Rolelst();
                role.ROL_ID = sdr["ROL_ID"].ToString();
                role.ROL_ACRONYM = sdr["ROL_ACRONYM"].ToString();
                role.ROL_DESCRIPTION = sdr["ROL_DESCRIPTION"].ToString();
                role.isChecked = false;
                Rollst.Add(role);
            }
        }
        if (Rollst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Rollst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetAllCompanies()
    {
        try
        {
            List<GetCompanies> cmplst = new List<GetCompanies>();
            GetCompanies Cmpy;
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_COMPANIES");
            using (IDataReader sdr = sp.GetReader())
            {
                while (sdr.Read())
                {
                    Cmpy = new GetCompanies();
                    Cmpy.CNP_ID = Convert.ToInt32(sdr["CNP_ID"].ToString());
                    Cmpy.CNP_NAME = sdr["CNP_NAME"].ToString();
                    cmplst.Add(Cmpy);
                }
            }
            if (cmplst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = cmplst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetTypes()
    {
        List<Types> Typ_lst = new List<Types>();
        Types Typ;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_TYPES");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Typ = new Types();
                Typ.VT_CODE = sdr["VT_CODE"].ToString();
                Typ.VT_TYPE = sdr["VT_TYPE"].ToString();
                Typ.ticked = false;
                Typ_lst.Add(Typ);
            }
        }
        if (Typ_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Typ_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetSubTypes()
    {
        List<SubTypes> Typ_lst = new List<SubTypes>();
        SubTypes Typ;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_SUB_TYPES");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Typ = new SubTypes();
                Typ.AST_SUBCAT_CODE = sdr["AST_SUBCAT_CODE"].ToString();
                Typ.AST_SUBCAT_NAME = sdr["AST_SUBCAT_NAME"].ToString();
                Typ.CAT_CODE = sdr["AST_CAT_CODE"].ToString();
                Typ.ticked = false;
                Typ_lst.Add(Typ);
            }
        }
        if (Typ_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Typ_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetVendors()
    {
        List<Vendors> Ven_lst = new List<Vendors>();
        Vendors Ven;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_VENDORS_LIST");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Ven = new Vendors();
                Ven.AVR_CODE = sdr["AVR_CODE"].ToString();
                Ven.AVR_NAME = sdr["AVR_NAME"].ToString();
                Ven.AST_SUBCAT_CODE = sdr["AST_SUBCAT_CODE"].ToString();
                Ven.ticked = false;
                Ven_lst.Add(Ven);
            }
        }
        if (Ven_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Ven_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetBrand()
    {
        List<GetBrands> Typ_lst = new List<GetBrands>();
        GetBrands Typ;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ASSET_MANUFACTURER");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Typ = new GetBrands();
                Typ.MANUFACTUER_CODE = sdr["MANUFACTUER_CODE"].ToString();
                Typ.MANUFACTURER = sdr["MANUFACTURER"].ToString();
                Typ.ticked = false;
                Typ_lst.Add(Typ);
            }
        }
        if (Typ_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Typ_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetModel()
    {
        List<GetModel> Typ_lst = new List<GetModel>();
        GetModel Typ;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_MODELS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Typ = new GetModel();
                Typ.AST_MD_CODE = sdr["AST_MD_CODE"].ToString();
                Typ.AST_MD_NAME = sdr["AST_MD_NAME"].ToString();
                Typ.ticked = false;
                Typ_lst.Add(Typ);
            }
        }
        if (Typ_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Typ_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public static List<T> ConvertDataTableToList<T>(DataTable dt)
    {
        List<T> data = new List<T>();
        foreach (DataRow row in dt.Rows)
        {
            T item = GetItem<T>(row);
            data.Add(item);
        }
        return data;
    }


    public static T GetItem<T>(DataRow dr)
    {
        Type temp = typeof(T);
        T obj = Activator.CreateInstance<T>();

        foreach (DataColumn column in dr.Table.Columns)
        {
            foreach (PropertyInfo pro in temp.GetProperties())
            {
                if (pro.Name == column.ColumnName)
                    pro.SetValue(obj, dr[column.ColumnName], null);
                else
                    continue;
            }
        }
        return obj;
    }

    public static DataTable ConvertToDataTable<T>(List<T> data)
    {
        DataTable dataTable = new DataTable();
        PropertyDescriptorCollection propertyDescriptorCollection =
            TypeDescriptor.GetProperties(typeof(T));
        for (int i = 0; i < propertyDescriptorCollection.Count; i++)
        {
            PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
            Type type = propertyDescriptor.PropertyType;

            if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                type = Nullable.GetUnderlyingType(type);


            dataTable.Columns.Add(propertyDescriptor.Name, type);
        }
        object[] values = new object[propertyDescriptorCollection.Count];
        foreach (T iListItem in data)
        {
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
            }
            dataTable.Rows.Add(values);
        }
        return dataTable;
    }
    public static DataTable ConvertToDataTable<T>(List<T> list, string columnName)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(columnName, typeof(T));

        foreach (var item in list)
        {
            DataRow dr = dt.NewRow();
            dr[columnName] = item;
            dt.Rows.Add(dr);
        }

        return dt;
    }
    public DataTable GetInversedDataTable(DataTable table, string columnX,
                                             params string[] columnsToIgnore)
    {
        //Create a DataTable to Return
        DataTable returnTable = new DataTable();

        if (columnX == "")
            columnX = table.Columns[0].ColumnName;

        //Add a Column at the beginning of the table

        returnTable.Columns.Add(columnX);

        //Read all DISTINCT values from columnX Column in the provided DataTale
        List<string> columnXValues = new List<string>();

        //Creates list of columns to ignore
        List<string> listColumnsToIgnore = new List<string>();
        if (columnsToIgnore.Length > 0)
            listColumnsToIgnore.AddRange(columnsToIgnore);

        if (!listColumnsToIgnore.Contains(columnX))
            listColumnsToIgnore.Add(columnX);

        foreach (DataRow dr in table.Rows)
        {
            string columnXTemp = dr[columnX].ToString();
            //Verify if the value was already listed
            if (!columnXValues.Contains(columnXTemp))
            {
                //if the value id different from others provided, add to the list of 
                //values and creates a new Column with its value.
                columnXValues.Add(columnXTemp);
                returnTable.Columns.Add(columnXTemp);
            }
            else
            {
                //Throw exception for a repeated value
                throw new Exception("The inversion used must have " +
                                    "unique values for column " + columnX);
            }
        }

        //Add a line for each column of the DataTable

        foreach (DataColumn dc in table.Columns)
        {
            if (!columnXValues.Contains(dc.ColumnName) &&
                !listColumnsToIgnore.Contains(dc.ColumnName))
            {
                DataRow dr = returnTable.NewRow();
                dr[0] = dc.ColumnName;
                returnTable.Rows.Add(dr);
            }
        }

        //Complete the datatable with the values
        for (int i = 0; i < returnTable.Rows.Count; i++)
        {
            for (int j = 1; j < returnTable.Columns.Count; j++)
            {
                returnTable.Rows[i][j] =
                  table.Rows[j - 1][returnTable.Rows[i][0].ToString()].ToString();
            }
        }

        return returnTable;
    }

    // guest house booking

    public object GetReservationTypes(int id)
    {
        List<ReservationType> rt_lst = new List<ReservationType>();
        ReservationType RT;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_RESERVATION_TYPES");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                RT = new ReservationType();
                RT.RT_SNO = sdr["RT_SNO"].ToString();
                RT.RT_NAME = sdr["RT_NAME"].ToString();
                RT.ticked = false;
                rt_lst.Add(RT);
            }
        }
        if (rt_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = rt_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetEmployeeNameAndID(string name)
    {
        List<EmployeeNameID> emplst = new List<EmployeeNameID>();
        EmployeeNameID emp;
        SqlParameter[] param = new SqlParameter[3];

        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];

        param[1] = new SqlParameter("@NAME", SqlDbType.NVarChar);
        param[1].Value = name;

        param[2] = new SqlParameter("@COMPANY", SqlDbType.Int);
        param[2].Value = HttpContext.Current.Session["COMPANYID"].ToString();

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_EMP_NAMES_AUTO_COMPLETE", param))
        {
            while (sdr.Read())
            {
                emp = new EmployeeNameID();
                emp.AUR_ID = sdr["id"].ToString();
                emp.NAME = sdr["name"].ToString();
                emp.Costcenter = sdr["CostCenter"].ToString();
                emp.Vertical = sdr["Vertical"].ToString();
                emp.ticked = false;
                emplst.Add(emp);
            }
        }
        return new { items = emplst, total_count = emplst.Count, incomplete_results = false };
    }

    public object GetEmpEmailByID(string ID)
    {
        SqlParameter[] param = new SqlParameter[2];

        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];

        param[1] = new SqlParameter("@ID", SqlDbType.NVarChar);
        param[1].Value = ID;

        string Aur_Email = "";
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_EMP_EMAIL_BY_ID", param))
        {
            while (sdr.Read())
            {
                Aur_Email = sdr["AUR_EMAIL"].ToString();

            }
        }
        return new { data = Aur_Email };
    }

    public object GetEmployeeTypes()
    {
        List<EmployeeType> emplst = new List<EmployeeType>();
        EmployeeType emp;
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[0].Value = HttpContext.Current.Session["UID"];

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_EMPLOYEE_TYPE", param))
        {
            while (sdr.Read())
            {
                emp = new EmployeeType();
                emp.ET_SNO = sdr["ET_SNO"].ToString();
                emp.ET_NAME = sdr["ET_NAME"].ToString();
                emplst.Add(emp);
            }
        }
        return new { items = emplst, total_count = emplst.Count, incomplete_results = false };
    }
    public object GetAllUsers()
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_USER_REPORTING_TO");
            DataSet ds = sp.GetDataSet();
            return new { Message = MessagesVM.UM_OK, data = ds }; ;
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }


    public object GetFacilityNamesbyType(Nametype type, int id)
    {
        List<FacilityNamelst> fclst = new List<FacilityNamelst>();
        FacilityNamelst city;

        SqlParameter[] param = new SqlParameter[4];
        param[0] = new SqlParameter("@RT_LST", SqlDbType.Structured);
        if (type.rt == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(type.rt);
        }
        param[1] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
        if (type.lcm == null)
        {
            param[1].Value = null;
        }
        else
        {
            param[1].Value = ConvertToDataTable(type.lcm);
        }
        param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[2].Value = HttpContext.Current.Session["UID"];
        param[3] = new SqlParameter("@MODE", SqlDbType.Int);
        param[3].Value = id;

        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_FACILITY_NAMES_BY_TYPE_AND_LOCATION", param))
        {
            while (sdr.Read())
            {
                city = new FacilityNamelst();
                city.RF_NAME = sdr["RF_NAME"].ToString();
                city.RF_SNO = sdr["RF_SNO"].ToString();
                city.RT_SNO = sdr["RT_SNO"].ToString();
                city.ticked = false;
                fclst.Add(city);
            }
        }
        if (fclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = fclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetPropTypes()
    {
        try
        {
            List<GetPropertyType> prplst = new List<GetPropertyType>();
            GetPropertyType PrpType;
            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_GET_ACTPROPTYPE"))
            {
                while (sdr.Read())
                {
                    PrpType = new GetPropertyType();
                    PrpType.PN_PROPERTYTYPE = sdr["PN_PROPERTYTYPE"].ToString();
                    PrpType.PN_TYPEID = sdr["PN_TYPEID"].ToString();
                    prplst.Add(PrpType);
                }
            }
            if (prplst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = prplst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }

    public object GetPropertyNames(TenantReportModel obj)
    {
        try
        {
            List<GetPropertyName> fclst = new List<GetPropertyName>();
            GetPropertyName pn;

            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
            if (obj.selectedLoc == null)
            {
                param[0].Value = null;
            }
            else
            {
                param[0].Value = ConvertToDataTable(obj.selectedLoc);
            }
            param[1] = new SqlParameter("@PT_LST", SqlDbType.Structured);
            if (obj.selectedPrpType == null)
            {
                param[1].Value = null;
            }
            else
            {
                param[1].Value = ConvertToDataTable(obj.selectedPrpType);
            }
            //param[2] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
            //param[2].Value = HttpContext.Current.Session["UID"];


            using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_GET_TENANT_PROPERTIES", param))
            {
                while (sdr.Read())
                {
                    pn = new GetPropertyName();
                    pn.PM_PPT_NAME = sdr["PM_PPT_NAME"].ToString();
                    pn.PM_PPT_CODE = sdr["PM_PPT_CODE"].ToString();
                    pn.PM_PPT_TYPE = sdr["PM_PPT_SNO"].ToString();
                    pn.ticked = false;
                    fclst.Add(pn);
                }
            }
            if (fclst.Count != 0)
                return new { Message = MessagesVM.UM_OK, data = fclst };
            else
                return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

        }
        catch (Exception)
        {
            return new { Message = MessagesVM.ErrorMessage, data = (object)null };
        }
    }
    public object GetAssetTypes(int id)
    {
        List<AssetTypes> Asttypes = new List<AssetTypes>();
        AssetTypes Atyp;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ASSET_TYPE");
        sp.Command.AddParameter("@dummy", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Atyp = new AssetTypes();
                Atyp.ASSET_TYPE_CODE = sdr["ASSET_TYPE_CODE"].ToString();
                Atyp.ASSET_TYPE_NAME = sdr["ASSET_TYPE_NAME"].ToString();
                Atyp.TYPE_ID = sdr["TYPE_ID"].ToString();
                Atyp.isChecked = false;
                Asttypes.Add(Atyp);
            }
        }
        if (Asttypes.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Asttypes };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetCategories(int id)
    {
        List<Categorylst> catlst = new List<Categorylst>();
        Categorylst cat;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AM_USP_GET_ASSETCATEGORIESS_VIEWSTK");
        sp.Command.AddParameter("@dummy", id, DbType.Int32);
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cat = new Categorylst();
                cat.CAT_CODE = sdr["VT_CODE"].ToString();
                cat.CAT_NAME = sdr["VT_TYPE"].ToString();
                cat.CAT_STATUS = sdr["VT_CONS_STATUS"].ToString();
                cat.isChecked = false;
                catlst.Add(cat);
            }
        }
        if (catlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = catlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }


    public object GetAllEmployees(string text)
    {
        List<EmployeeList> catlst = new List<EmployeeList>();
        EmployeeList cat;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ALL_EMPLOYEE");
        sp.Command.AddParameter("@dummy", text, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cat = new EmployeeList();
                cat.AUR_ID = sdr["AUR_ID"].ToString();
                cat.AUR_KNOWN_AS = sdr["AUR_KNOWN_AS"].ToString();
                catlst.Add(cat);
            }
        }
        if (catlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = catlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object ValidateUserFirstLogin()
    {
        List<EmployeeList> usrlst = new List<EmployeeList>();
        EmployeeList usr;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "VALIDATE_USER_FIRST_LOGIN");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"], DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                usr = new EmployeeList();
                usr.AUR_ID = sdr["returnstatus"].ToString();

                usrlst.Add(usr);
            }
        }
        if (usrlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = usrlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetSubCategories(int id)
    {
        List<SubCategorylst> catlst = new List<SubCategorylst>();
        SubCategorylst cat;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_ASSETSUBCATEGORIES");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cat = new SubCategorylst();
                cat.AST_SUBCAT_CODE = sdr["AST_SUBCAT_CODE"].ToString();
                cat.AST_SUBCAT_NAME = sdr["AST_SUBCAT_NAME"].ToString();
                cat.CAT_CODE = sdr["CAT_CODE"].ToString();
                cat.isChecked = false;
                catlst.Add(cat);
            }
        }
        if (catlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = catlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetBrands(int id)
    {
        List<Brandlst> brndlst = new List<Brandlst>();
        Brandlst brnd;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ASSET_MANUFACTURER");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                brnd = new Brandlst();
                brnd.BRND_CODE = sdr["MANUFACTURERID"].ToString();
                brnd.BRND_NAME = sdr["MANUFACTURER"].ToString();
                brnd.CAT_CODE = sdr["MANUFACTURER_TYPE_CODE"].ToString();
                brnd.BRND_SUBCODE = sdr["MANUFACTURER_TYPE_SUBCODE"].ToString();
                brnd.isChecked = false;
                brndlst.Add(brnd);
            }
        }
        if (brndlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = brndlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetModels(int id)
    {
        List<Modellst> mdlst = new List<Modellst>();
        Modellst model;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "AM_GET_AST_MODEL");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                model = new Modellst();
                model.MD_CODE = sdr["AAT_MODEL_NAME"].ToString();
                model.MD_NAME = sdr["AST_MD_NAME"].ToString();
                model.BRND_CODE = sdr["AAT_AAB_CODE"].ToString();
                model.CAT_CODE = sdr["AAT_AAG_CODE"].ToString();
                model.AST_SUBCAT_CODE = sdr["AAT_SUB_CODE"].ToString();
                model.isChecked = false;
                mdlst.Add(model);
            }
        }
        if (mdlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = mdlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Get_GH_Timings()
    {
        List<SysPreference> SysPrflst = new List<SysPreference>();
        SysPreference SysPrf;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GH_GET_TIMINGS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                SysPrf = new SysPreference();
                SysPrf.SYSP_CODE = sdr["SYSP_CODE"].ToString();
                SysPrf.SYSP_VAL1 = sdr["SYSP_VAL1"].ToString();
                SysPrf.SYSP_VAL2 = sdr["SYSP_VAL2"].ToString();
                SysPrflst.Add(SysPrf);
            }
        }
        if (SysPrflst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = SysPrflst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetFacilityNamesbyLocation(List<Locationlst> lcm, int id)
    {
        List<FacilityNamelst> fclst = new List<FacilityNamelst>();
        FacilityNamelst city;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
        if (lcm == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(lcm);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;



        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GET_FACILITY_NAMES_BY_LOCATION", param))
        {
            while (sdr.Read())
            {
                city = new FacilityNamelst();
                city.RF_NAME = sdr["RF_NAME"].ToString();
                city.RF_SNO = sdr["RF_SNO"].ToString();
                city.RT_SNO = sdr["RT_SNO"].ToString();
                city.TVP_CNY_CODE = sdr["TVP_CNY_CODE"].ToString();
                city.TVP_CTY_CODE = sdr["TVP_CTY_CODE"].ToString();
                city.TVP_LCM_CODE = sdr["TVP_LCM_CODE"].ToString();
                city.ticked = false;
                fclst.Add(city);
            }
        }
        if (fclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = fclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetReservationTypesByFacility(int id, List<FacilityNamelst> fnamelst)
    {
        List<ReservationType> rt_lst = new List<ReservationType>();
        ReservationType RT;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@RF_LST", SqlDbType.Structured);
        if (fnamelst == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(fnamelst);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = HttpContext.Current.Session["UID"];
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;



        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_RESERVATION_TYPES", param))
        {
            while (sdr.Read())
            {
                RT = new ReservationType();
                RT.RT_SNO = sdr["RT_SNO"].ToString();
                RT.RT_NAME = sdr["RT_NAME"].ToString();
                RT.ticked = false;
                RT.ticked = false;
                rt_lst.Add(RT);
            }
        }
        if (rt_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = rt_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetCountriesWithoutSession(int id)
    {
        List<Countrylst> cnylst = new List<Countrylst>();
        Countrylst cny;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "Get_Countries_ADM");
        // sp = new SubSonic.StoredProcedure("Get_Countries_ADM");
        sp.Command.AddParameter("@AUR_ID", "", DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cny = new Countrylst();
                cny.CNY_CODE = sdr["CNY_CODE"].ToString();
                cny.CNY_NAME = sdr["CNY_NAME"].ToString();
                cny.ticked = false;
                cnylst.Add(cny);
            }
        }
        if (cnylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = cnylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetCitiesWithoutSession(int id)
    {
        List<Citylst> ctylst = new List<Citylst>();
        Citylst cty;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CITY_ADM");
        sp.Command.AddParameter("@AUR_ID", "", DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cty = new Citylst();
                cty.CTY_CODE = sdr["CTY_CODE"].ToString();
                cty.CTY_NAME = sdr["CTY_NAME"].ToString();
                cty.CNY_CODE = sdr["CNY_CODE"].ToString();
                cty.ticked = false;
                ctylst.Add(cty);
            }
        }
        if (ctylst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = ctylst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };

    }

    public object GetLocationsWithoutSession(int id)
    {
        List<Locationlst> Loc_lst = new List<Locationlst>();
        Locationlst Loc;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_LOCATION_ADM");
        sp.Command.AddParameter("@AUR_ID", "", DbType.String);
        sp.Command.AddParameter("@MODE", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                Loc = new Locationlst();
                Loc.LCM_CODE = sdr["LCM_CODE"].ToString();
                Loc.LCM_NAME = sdr["LCM_NAME"].ToString();
                Loc.CTY_CODE = sdr["CTY_CODE"].ToString();
                Loc.CNY_CODE = sdr["CNY_CODE"].ToString();
                Loc.ticked = false;
                Loc_lst.Add(Loc);
            }
        }
        if (Loc_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = Loc_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetFacilityNamesbyLocationWithoutSession(List<Locationlst> lcm, int id)
    {
        List<FacilityNamelst> fclst = new List<FacilityNamelst>();
        FacilityNamelst city;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@LCM_LST", SqlDbType.Structured);
        if (lcm == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(lcm);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = "";
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;



        using (SqlDataReader sdr = ConcurSQLHelper.ExecuteReader(CommandType.StoredProcedure, "GET_FACILITY_NAMES_BY_LOCATION", param))
        {
            while (sdr.Read())
            {
                city = new FacilityNamelst();
                city.RF_NAME = sdr["RF_NAME"].ToString();
                city.RF_SNO = sdr["RF_SNO"].ToString();
                city.RT_SNO = sdr["RT_SNO"].ToString();
                city.TVP_CNY_CODE = sdr["TVP_CNY_CODE"].ToString();
                city.TVP_CTY_CODE = sdr["TVP_CTY_CODE"].ToString();
                city.TVP_LCM_CODE = sdr["TVP_LCM_CODE"].ToString();
                city.ticked = false;
                fclst.Add(city);
            }
        }
        if (fclst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = fclst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object GetReservationTypesByFacilityWithoutSession(int id, List<FacilityNamelst> fnamelst)
    {
        List<ReservationType> rt_lst = new List<ReservationType>();
        ReservationType RT;

        SqlParameter[] param = new SqlParameter[3];
        param[0] = new SqlParameter("@RF_LST", SqlDbType.Structured);
        if (fnamelst == null)
        {
            param[0].Value = null;
        }
        else
        {
            param[0].Value = ConvertToDataTable(fnamelst);
        }
        param[1] = new SqlParameter("@AUR_ID", SqlDbType.NVarChar);
        param[1].Value = "";
        param[2] = new SqlParameter("@MODE", SqlDbType.Int);
        param[2].Value = id;



        using (SqlDataReader sdr = ConcurSQLHelper.ExecuteReader(CommandType.StoredProcedure, "GH_GET_RESERVATION_TYPES", param))
        {
            while (sdr.Read())
            {
                RT = new ReservationType();
                RT.RT_SNO = sdr["RT_SNO"].ToString();
                RT.RT_NAME = sdr["RT_NAME"].ToString();
                RT.ticked = false;
                RT.ticked = false;
                rt_lst.Add(RT);
            }
        }
        if (rt_lst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = rt_lst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetContractCategories(int id)
    {
        List<ContractCategorylst> catlst = new List<ContractCategorylst>();
        ContractCategorylst cat;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CONTRACT_CATEGORY");
        sp.Command.AddParameter("@dummy", id, DbType.Int32);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                cat = new ContractCategorylst();
                cat.DMC_CODE = sdr["DMC_CODE"].ToString();
                cat.DMC_NAME = sdr["DMC_NAME"].ToString();
                cat.isChecked = false;
                catlst.Add(cat);
            }
        }
        if (catlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = catlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetContractSubcategories(string id)
    {
        List<ContractSubcategorylst> subcatlst = new List<ContractSubcategorylst>();
        ContractSubcategorylst sub;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CONTRACT_SUBCATEGORIES");
        sp.Command.AddParameter("@CAT_CODE", id, DbType.String);
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                sub = new ContractSubcategorylst();
                sub.DBT_CODE = sdr["DBT_CODE"].ToString();
                sub.DBT_NAME = sdr["DBT_NAME"].ToString();
                sub.isChecked = false;
                subcatlst.Add(sub);
            }
        }
        if (subcatlst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = subcatlst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
    public object GetContractStatus()
    {
        List<ContractStatuslst> statuslst = new List<ContractStatuslst>();
        ContractStatuslst status;
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_CONTRACT_STATUS");
        using (IDataReader sdr = sp.GetReader())
        {
            while (sdr.Read())
            {
                status = new ContractStatuslst();
                status.DRS_CODE = sdr["DRS_CODE"].ToString();
                status.DRS_NAME = sdr["DRS_NAME"].ToString();
                status.isChecked = false;
                statuslst.Add(status);
            }
        }
        if (statuslst.Count != 0)
            return new { Message = MessagesVM.UM_OK, data = statuslst };
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }
}
public class ReportGenerator<T>
{
    public string ReportPath { get; set; }
    public string DataSetName { get; set; }
    public string ReportType { get; set; }
    public string Vertical { get; set; }
    public string Costcenter { get; set; }
    public string BH1 { get; set; }
    public string BH2 { get; set; }
    public string PE { get; set; }
    public string CE { get; set; }
    public string TotalSum { get; set; }

    //Report Format//
    public Task GenerateReport(List<T> datasource, string filePath, string Type)
    {
        return Task.Run(() =>
        {
            var viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = ReportPath;

            ReportDataSource rds = new ReportDataSource();
            rds.Name = DataSetName;
            rds.Value = datasource;
            
            Microsoft.Reporting.WebForms.Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            viewer.LocalReport.DataSources.Add(rds);

            if (!string.IsNullOrEmpty(ReportType))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("ReportType", ReportType));


            if (!string.IsNullOrEmpty(Vertical))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("Vertical", Vertical));

            if (!string.IsNullOrEmpty(Costcenter))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("Costcenter", Costcenter));

            if (!string.IsNullOrEmpty(BH1))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("BH1", BH1));

            if (!string.IsNullOrEmpty(BH2))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("BH2", BH2));

            if (!string.IsNullOrEmpty(PE))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("PE", PE));

            if (!string.IsNullOrEmpty(CE))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("CE", CE));

            if (!string.IsNullOrEmpty(TotalSum))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("TotalSum", TotalSum));

            viewer.LocalReport.Refresh();

            String apptype = GetApplicationName(Type);
            byte[] bytes = viewer.LocalReport.Render(
                apptype, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
                fs.Flush();
            }
        });
    }
    public Task GenerateReport(DataTable datasource, string filePath, string Type)
    {
        return Task.Run(() =>
        {

            var viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.ReportPath = ReportPath;

            ReportDataSource rds = new ReportDataSource();
            rds.Name = DataSetName;
            rds.Value = datasource;


            Microsoft.Reporting.WebForms.Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string filenameExtension;

            viewer.LocalReport.DataSources.Add(rds);

            if (!string.IsNullOrEmpty(ReportType))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("ReportType", ReportType));

            if (!string.IsNullOrEmpty(Vertical))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("Vertical", Vertical));

            if (!string.IsNullOrEmpty(Costcenter))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("Costcenter", Costcenter));

            if (!string.IsNullOrEmpty(BH1))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("BH1", BH1));

            if (!string.IsNullOrEmpty(BH2))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("BH2", BH2));

            if (!string.IsNullOrEmpty(PE))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("PE", PE));

            if (!string.IsNullOrEmpty(CE))
                viewer.LocalReport.SetParameters(new Microsoft.Reporting.WebForms.ReportParameter("CE", CE));


            viewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            viewer.LocalReport.Refresh();

            String apptype = GetApplicationName(Type);
            byte[] bytes = viewer.LocalReport.Render(
                apptype, null, out mimeType, out encoding, out filenameExtension,
                out streamids, out warnings);

            using (FileStream fs = new FileStream(filePath, FileMode.Create))
            {
                fs.Write(bytes, 0, bytes.Length);
            }
        });
    }
    public String GetApplicationName(String ext)
    {
        switch (ext)
        {
            case "xls": return "Excel";
            case "xlsx": return "EXCELOPENXML";
            case "doc": return "Word";
            case "pdf": return "PDF";
        }
        return "PDF";
    }


}




