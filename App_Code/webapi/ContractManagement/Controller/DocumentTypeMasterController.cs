﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class DocumentTypeMasterController : ApiController
{
     DocumentTypeMasterService DTMS = new DocumentTypeMasterService();

    [HttpGet]
     public HttpResponseMessage BindDocumentData()
    {
        IEnumerable<DocumentMst> DocumentDetails = DTMS.BindDocumentData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, DocumentDetails);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Insert(DocumentMst Dataobj)
    {
        if (DTMS.Insert(Dataobj) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Dataobj);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already exists");
    }

    [HttpPost]
    public HttpResponseMessage Update(DocumentMst Update)
    {
        if (DTMS.Update(Update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }
}
