﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SpendCategoryMasterController : ApiController
{
    SpendCategoryMasterService SCMS = new SpendCategoryMasterService();

    [HttpGet]
    public HttpResponseMessage BindCategoryData()
    {
        IEnumerable<CategoryMst> CategoryDetails = SCMS.BindCategoryData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, CategoryDetails);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Insert(CategoryMst Dataobj)
    {
        if (SCMS.Insert(Dataobj) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Dataobj);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already exists");
    }

    [HttpPost]
    public HttpResponseMessage Update(CategoryMst Update)
    {
        if (SCMS.Update(Update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }
}
