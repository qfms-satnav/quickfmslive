﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using UtiltiyVM;

public class ModifyFactSheetController : ApiController
{
    ModifyFactSheetService MFSS = new ModifyFactSheetService();
    SubSonic.StoredProcedure sp;

    public HttpResponseMessage GetFactSheetDetails()
    {
        IEnumerable<GetFactDetails> GetFactSheetDetails = MFSS.GetFactSheetDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, GetFactSheetDetails);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Get_Factsheet_Details(GetFactDetails data)
    {
        var list = MFSS.Get_Factsheet_Details(data);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, list);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Update(FactSheetVm FSVM)
    {
        var obj = MFSS.Update(FSVM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

    //[HttpPost]
    //public object GetHashCode(JObject jobj)
    //{
    //    Guid uniqueVal = Guid.NewGuid();
    //    ChkExportAuth.Add(uniqueVal.ToString(), jobj);
    //    return new { Message = MessagesVM.UM_OK, data = uniqueVal.ToString() };
    //}

    static Hashtable ChkExportAuth = new Hashtable();

    [HttpGet]
    public HttpResponseMessage DRMExcelDownload([FromUri]string id)
    {
        HttpResponseMessage result = Request.CreateResponse(HttpStatusCode.OK);

        try
        {
            sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_DOC_DETAILS");
            sp.Command.AddParameter("@ID", id, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (!Directory.Exists(HttpContext.Current.Server.MapPath("~/ContractFileRepository/")))
            {
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/ContractFileRepository/"));
            }
            string filePath = HttpContext.Current.Server.MapPath("~/ContractFileRepository/") + ds.Tables[0].Rows[0]["DFDC_NAME"].ToString();
            //string filePath = "C:\\ContractFileRepository\\" + ds.Tables[0].Rows[0]["DFDC_NAME"].ToString();
            byte[] bytes = System.IO.File.ReadAllBytes(filePath);
            result.Content = new StreamContent(new MemoryStream(bytes));
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = ds.Tables[0].Rows[0]["DFDC_ACT_NAME"].ToString();
            ChkExportAuth.Remove(id);
            return result;
        }
        catch (Exception ex)
        {
            if (ex.InnerException != null)
                result = Request.CreateResponse(HttpStatusCode.ExpectationFailed, MessagesVM.ErrorMessage);
            else
                result = Request.CreateResponse(HttpStatusCode.ExpectationFailed, MessagesVM.ErrorMessage);
            return result;
        }
    }
   
}
