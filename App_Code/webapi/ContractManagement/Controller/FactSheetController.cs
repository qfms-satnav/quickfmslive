﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UtiltiyVM;
using System.Web;
using Newtonsoft.Json;
using System.Text;
using System.Net.Http.Headers;
using System.Data;
using System.Globalization;
using System.IO;
using Microsoft.Reporting.WebForms;
using System.Threading.Tasks;
using System.Collections;

public class FactSheetController : ApiController
{
    FactSheetService FSS = new FactSheetService();

    public HttpResponseMessage GetReaminderDetails()
    {
        IEnumerable<RemainderDetails> GetReaminderDetails = FSS.GetReaminderDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, GetReaminderDetails);
        return response;
    }

    public HttpResponseMessage GetNotificationDetails()
    {
        IEnumerable<NotifcationDetails> GetNotificationDetails = FSS.GetNotificationDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, GetNotificationDetails);
        return response;
    }

    public HttpResponseMessage GetNotificDetails()
    {
        IEnumerable<NoticesDetails> GetNotificDetails = FSS.GetNotificDetails();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, GetNotificDetails);
        return response;
    }

    public object Getrefnum()
    {
        var obj = FSS.Getrefnum();
        return obj;
    }

    [HttpPost]
    public HttpResponseMessage Submit(FactSheetVm FSVM)
    {
        var obj = FSS.Submit(FSVM);
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, obj);
        return response;
    }

  
    [HttpPost]
    public HttpResponseMessage UploadTemplate()
    {

        var httpRequest = HttpContext.Current.Request;
        dynamic retobj = FSS.UploadTemplate(httpRequest);
        string retContent = "";
        if (retobj.data != null)
            retContent = JsonConvert.SerializeObject(retobj);
        else
            retContent = JsonConvert.SerializeObject(retobj);

        var response = Request.CreateResponse(HttpStatusCode.OK);
        response.Content = new StringContent(retContent, Encoding.UTF8, "text/plain");
        response.Content.Headers.ContentType = new MediaTypeWithQualityHeaderValue(@"text/html");
        return response;
    }
}
