﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class BusinessGroupMasterController : ApiController
{
    BusinessGroupMasterService BGMS = new BusinessGroupMasterService();
    
    [HttpGet]
    public HttpResponseMessage BindBusinessData()
    {
        IEnumerable<BusinessMst> BusinessGroupDetails = BGMS.BindBusinessData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, BusinessGroupDetails);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Insert(BusinessMst Dataobj)
    {
        if (BGMS.Insert(Dataobj) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Dataobj);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already exists");
    }

    [HttpPost]
    public HttpResponseMessage Update(BusinessMst Update)
    {
        if (BGMS.Update(Update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }
     
}
