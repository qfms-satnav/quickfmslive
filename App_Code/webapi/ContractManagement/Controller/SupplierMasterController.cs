﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

public class SupplierMasterController : ApiController
{
    SupplierMasterService SMS = new SupplierMasterService();

    [HttpGet]
    public HttpResponseMessage BindSupplierData()
    {
        IEnumerable<SupplierMst> DocumentDetails = SMS.BindSupplierData();
        HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, DocumentDetails);
        return response;
    }

    [HttpPost]
    public HttpResponseMessage Insert(SupplierMst Dataobj)
    {
        if (SMS.Insert(Dataobj) == 0)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Dataobj);
            return response;
        }
        else
            return Request.CreateResponse(HttpStatusCode.BadRequest, "Code already exists");
    }

    [HttpPost]
    public HttpResponseMessage Update(SupplierMst Update)
    {
        if (SMS.Update(Update))
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK, Update);
            return response;
        }
        return Request.CreateResponse(HttpStatusCode.BadRequest, "Update Failed");
    }
}
