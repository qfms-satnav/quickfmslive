﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for ModifyFactSheetService
/// </summary>
public class ModifyFactSheetService
{
    SubSonic.StoredProcedure sp;

    public IEnumerable<GetFactDetails> GetFactSheetDetails()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_FACT_SHEET_DETAILS]").GetReader();
        List<GetFactDetails> details = new List<GetFactDetails>();
        while (reader.Read())
        {
            details.Add(new GetFactDetails()
            {
                DFM_REQ_ID = reader.GetValue(0).ToString(),
                DCT_NAME = reader.GetValue(1).ToString(),
                DSD_NAME = reader.GetValue(2).ToString(),
                DST_NAME = reader.GetValue(3).ToString(),
                DBT_NAME = reader.GetValue(4).ToString(),
                DFM_REF_NO = reader.GetValue(5).ToString(),
                DFM_CREATED_BY = reader.GetValue(6).ToString(),

            });
        }
        reader.Close();
        return details;
    }

    public object Get_Factsheet_Details(GetFactDetails data)
    {
        FactSheetVm FS = new FactSheetVm();

        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_FACTSHEET_DETAILS");
        sp.Command.AddParameter("@REQ_ID", data.DFM_REQ_ID, DbType.String);
        DataSet ds = sp.GetDataSet();
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                FS.MainSheet = new MainSheet();
                FS.MainSheet.DFM_DCT_CODE = ds.Tables[0].Rows[0]["DFM_DCT_CODE"].ToString();
                FS.MainSheet.DFM_DST_CODE = ds.Tables[0].Rows[0]["DFM_DST_CODE"].ToString();
                FS.MainSheet.DFM_DBT_CODE = ds.Tables[0].Rows[0]["DFM_DBT_CODE"].ToString();
                FS.MainSheet.DFM_DSD_CODE = ds.Tables[0].Rows[0]["DFM_DSD_CODE"].ToString();
                FS.MainSheet.DFM_REF_NO = ds.Tables[0].Rows[0]["DFM_REF_NO"].ToString();
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                FS.AgreeMentSheet = new AgreeMentSheet();
                FS.AgreeMentSheet.DFDT_SIGN_DATE = Convert.ToDateTime(ds.Tables[1].Rows[0]["DFDT_SIGN_DATE"]);
                FS.AgreeMentSheet.DFDT_EFRM_DATE = Convert.ToDateTime(ds.Tables[1].Rows[0]["DFDT_EFRM_DATE"]);
                FS.AgreeMentSheet.DFDT_TRMN_DATE = Convert.ToDateTime(ds.Tables[1].Rows[0]["DFDT_TRMN_DATE"]);
                if (ds.Tables[1].Rows[0]["DFDT_REN_DATE"] != DBNull.Value)
                    FS.AgreeMentSheet.DFDT_REN_DATE = Convert.ToDateTime(ds.Tables[1].Rows[0]["DFDT_REN_DATE"]);
                if (ds.Tables[1].Rows[0]["DFDT_NOTF_PRD_WITH_CLAUSE"].ToString() != "")
                    FS.AgreeMentSheet.DFDT_NOTF_PRD_WITH_CLAUSE = ds.Tables[1].Rows[0]["DFDT_NOTF_PRD_WITH_CLAUSE"].ToString();
                if (ds.Tables[1].Rows[0]["DFDT_NOTF_PRD_WITHOUT_CLAUSE"].ToString() != "")
                    FS.AgreeMentSheet.DFDT_NOTF_PRD_WITHOUT_CLAUSE = ds.Tables[1].Rows[0]["DFDT_NOTF_PRD_WITHOUT_CLAUSE"].ToString();
            }

            if (ds.Tables[2].Rows.Count > 0)
            {
                FS.DRN_SNO = Convert.ToInt32(ds.Tables[2].Rows[0]["DFR_REN_RMDR"]);
                FS.DRD_SNO = Convert.ToInt32(ds.Tables[2].Rows[0]["DFR_RPT_NOTF"]);
                //FS.DRN_SNO = Convert.ToInt32(ds.Tables[2].Rows[0]["DFR_PO_REN_RMDR"]);
                if (ds.Tables[2].Rows[0]["DFR_PO_REN_RMDR"].ToString() != "")
                    FS.DRN_SNO = Convert.ToInt32(ds.Tables[2].Rows[0]["DFR_PO_REN_RMDR"]);
                //FS.DRD_SNO = Convert.ToInt32(ds.Tables[2].Rows[0]["DFR_PO_RPT_NOTF"]);
                if(ds.Tables[2].Rows[0]["DFR_PO_RPT_NOTF"].ToString() != "")
                    FS.DRD_SNO = Convert.ToInt32(ds.Tables[2].Rows[0]["DFR_PO_RPT_NOTF"]);
            }

            if (ds.Tables[3].Rows.Count > 0)
            {
                FS.ValuesSheet = new ValuesSheet();
                FS.ValuesSheet.DFV_CNRT_VAL = Convert.ToInt32(ds.Tables[3].Rows[0]["DFV_CNRT_VAL"]);
                FS.ValuesSheet.DFV_CNRT_SCP = ds.Tables[3].Rows[0]["DFV_CNRT_SCP"].ToString();
                if (ds.Tables[3].Rows[0]["DFV_ANNL_VAL"].ToString() != "")
                    FS.ValuesSheet.DFV_ANNL_VAL = Convert.ToInt32(ds.Tables[3].Rows[0]["DFV_ANNL_VAL"]);
            }

            if (ds.Tables[4].Rows.Count > 0)
            {
                FS.StakeholderSheet = new StakeholderSheet();
                FS.StakeholderSheet.DFD_CT = ds.Tables[4].Rows[0]["DFD_CT"].ToString();
                FS.StakeholderSheet.DFD_AC = ds.Tables[4].Rows[0]["DFD_AC"].ToString();
                FS.StakeholderSheet.DFD_PT = ds.Tables[4].Rows[0]["DFD_PT"].ToString();
                FS.StakeholderSheet.DFD_TC = ds.Tables[4].Rows[0]["DFD_TC"].ToString();
                FS.StakeholderSheet.DFD_DW = ds.Tables[4].Rows[0]["DFD_DW"].ToString();
                FS.StakeholderSheet.DFD_IPR = ds.Tables[4].Rows[0]["DFD_IPR"].ToString();
                FS.StakeholderSheet.DFD_CONFD = ds.Tables[4].Rows[0]["DFD_CONFD"].ToString();
                FS.StakeholderSheet.DFD_CD = ds.Tables[4].Rows[0]["DFD_CD"].ToString();
            }

            if (ds.Tables[5].Rows.Count > 0)
            {
                FS.BusinessStakehol = new List<BusinessStakeHolder>();

                foreach (DataRow dr in ds.Tables[5].Rows)
                {
                    FS.BusinessStakehol.Add(new BusinessStakeHolder { DFKD_NAME = dr["DFKD_NAME"].ToString() });
                }
            }

            if (ds.Tables[6].Rows.Count > 0)
            {
                FS.DocumentDetails = new List<GetDocumentDetailsVM>();

                foreach (DataRow dr in ds.Tables[6].Rows)
                {
                    FS.DocumentDetails.Add(new GetDocumentDetailsVM { DFDC_ACT_NAME = dr["DFDC_ACT_NAME"].ToString(), DFDC_DDT_CODE = dr["DFDC_DDT_CODE"].ToString(), DFDC_NAME = dr["DFDC_NAME"].ToString(), DFDC_ID = Convert.ToInt32(dr["DFDC_ID"]) });
                }
            }

            if (ds.Tables[7].Rows.Count > 0)
            {
                FS.POSheet = new POSheet();
                FS.POSheet.DFDT_PO_EFRM_DATE = Convert.ToDateTime(ds.Tables[7].Rows[0]["DFDT_PO_EFRM_DATE"]);
                FS.POSheet.DFDT_PO_TRMN_DATE = Convert.ToDateTime(ds.Tables[7].Rows[0]["DFDT_PO_TRMN_DATE"]);
                if (ds.Tables[7].Rows[0]["DFDT_PO_REN_DATE"] != DBNull.Value)
                    FS.POSheet.DFDT_PO_REN_DATE = Convert.ToDateTime(ds.Tables[7].Rows[0]["DFDT_PO_REN_DATE"]);
                if (ds.Tables[7].Rows[0]["DFDT_PO_NOTF_PRD_WITH_CLAUSE"].ToString() != "")
                    FS.POSheet.DFDT_PO_NOTF_PRD_WITH_CLAUSE = ds.Tables[7].Rows[0]["DFDT_PO_NOTF_PRD_WITH_CLAUSE"].ToString();
                if (ds.Tables[7].Rows[0]["DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE"].ToString() != "")
                    FS.POSheet.DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE = ds.Tables[7].Rows[0]["DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE"].ToString();
            }

            return FS;
        }
        else
            return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public object Update(FactSheetVm FSVM)
    {
        SqlParameter[] param = new SqlParameter[35];

        param[0] = new SqlParameter("@DFM_DCT_CODE", SqlDbType.NVarChar);
        param[0].Value = FSVM.MainSheet.DFM_DCT_CODE;
        param[1] = new SqlParameter("@DFM_DST_CODE", SqlDbType.NVarChar);
        param[1].Value = FSVM.MainSheet.DFM_DST_CODE;
        param[2] = new SqlParameter("@DFM_DBT_CODE", SqlDbType.NVarChar);
        param[2].Value = FSVM.MainSheet.DFM_DBT_CODE;
        param[3] = new SqlParameter("@DFM_DSD_CODE", SqlDbType.NVarChar);
        param[3].Value = FSVM.MainSheet.DFM_DSD_CODE;
        param[4] = new SqlParameter("@DFM_REF_NO", SqlDbType.NVarChar);
        param[4].Value = FSVM.MainSheet.DFM_REF_NO;
        param[5] = new SqlParameter("@USER_ID", SqlDbType.NVarChar);
        param[5].Value = HttpContext.Current.Session["UID"];


        param[6] = new SqlParameter("@DFDT_SIGN_DATE", SqlDbType.DateTime);
        param[6].Value = FSVM.AgreeMentSheet.DFDT_SIGN_DATE;
        param[7] = new SqlParameter("@DFDT_EFRM_DATE", SqlDbType.DateTime);
        param[7].Value = FSVM.AgreeMentSheet.DFDT_EFRM_DATE  ;
        param[8] = new SqlParameter("@DFDT_TRMN_DATE", SqlDbType.DateTime);
        param[8].Value = FSVM.AgreeMentSheet.DFDT_TRMN_DATE;

        param[9] = new SqlParameter("@DFDT_REN_DATE", SqlDbType.DateTime);
        param[9].Value = FSVM.AgreeMentSheet.DFDT_REN_DATE;
        param[10] = new SqlParameter("@DFDT_NOTF_PRD_WITH_CLAUSE", SqlDbType.NVarChar);
        param[10].Value = FSVM.AgreeMentSheet.DFDT_NOTF_PRD_WITH_CLAUSE;
        param[11] = new SqlParameter("@DFDT_NOTF_PRD_WITHOUT_CLAUSE", SqlDbType.NVarChar);
        param[11].Value = FSVM.AgreeMentSheet.DFDT_NOTF_PRD_WITHOUT_CLAUSE;

        param[12] = new SqlParameter("@DFR_REN_RMDR", SqlDbType.NVarChar);
        param[12].Value = FSVM.DRD_SNO;
        param[13] = new SqlParameter("@DFR_RPT_NOTF", SqlDbType.NVarChar);
        param[13].Value = FSVM.DRN_SNO;

        param[14] = new SqlParameter("@DFV_CNRT_VAL", SqlDbType.NVarChar);
        param[14].Value = FSVM.ValuesSheet.DFV_CNRT_VAL;
        param[15] = new SqlParameter("@DFV_CNRT_SCP", SqlDbType.NVarChar);
        param[15].Value = FSVM.ValuesSheet.DFV_CNRT_SCP;
        param[16] = new SqlParameter("@DFV_ANNL_VAL", SqlDbType.NVarChar);
        param[16].Value = FSVM.ValuesSheet.DFV_ANNL_VAL;


        param[17] = new SqlParameter("@KETLST", SqlDbType.Structured);
        param[17].Value = UtilityService.ConvertToDataTable(FSVM.BusinessStakehol);


        param[18] = new SqlParameter("@DFD_CT", SqlDbType.NVarChar);
        param[18].Value = FSVM.StakeholderSheet.DFD_CT;
        param[19] = new SqlParameter("@DFD_AC", SqlDbType.NVarChar);
        param[19].Value = FSVM.StakeholderSheet.DFD_AC;
        param[20] = new SqlParameter("@DFD_PT", SqlDbType.NVarChar);
        param[20].Value = FSVM.StakeholderSheet.DFD_PT;
        param[21] = new SqlParameter("@DFD_TC", SqlDbType.NVarChar);
        param[21].Value = FSVM.StakeholderSheet.DFD_TC;
        param[22] = new SqlParameter("@DFD_DW", SqlDbType.NVarChar);
        param[22].Value = FSVM.StakeholderSheet.DFD_DW;
        param[23] = new SqlParameter("@DFD_IPR", SqlDbType.NVarChar);
        param[23].Value = FSVM.StakeholderSheet.DFD_IPR;
        param[24] = new SqlParameter("@DFD_CONFD", SqlDbType.NVarChar);
        param[24].Value = FSVM.StakeholderSheet.DFD_CONFD;
        param[25] = new SqlParameter("@DFD_CD", SqlDbType.NVarChar);
        param[25].Value = FSVM.StakeholderSheet.DFD_CD;

        param[26] = new SqlParameter("@DOCUPLOAD", SqlDbType.Structured);
        param[26].Value = UtilityService.ConvertToDataTable(FSVM.DocumentDetails);

        param[27] = new SqlParameter("@DFM_REQ_ID", SqlDbType.NVarChar);
        param[27].Value = FSVM.MainSheet.DFM_REQ_ID;

        param[28] = new SqlParameter("@DFDT_PO_EFRM_DATE", SqlDbType.DateTime);
        param[28].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_EFRM_DATE;
        //param[28].Value = FSVM.POSheet.DFDT_PO_EFRM_DATE;

        param[29] = new SqlParameter("@DFDT_PO_TRMN_DATE", SqlDbType.DateTime);
        param[29].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_TRMN_DATE;
        //param[29].Value = FSVM.POSheet.DFDT_PO_TRMN_DATE;

        param[30] = new SqlParameter("@DFDT_PO_REN_DATE", SqlDbType.DateTime);
        param[30].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_REN_DATE;
        //param[30].Value = FSVM.POSheet.DFDT_PO_REN_DATE;
        param[31] = new SqlParameter("@DFDT_PO_NOTF_PRD_WITH_CLAUSE", SqlDbType.NVarChar);
        param[31].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_NOTF_PRD_WITH_CLAUSE;
        //param[31].Value = FSVM.POSheet.DFDT_PO_NOTF_PRD_WITH_CLAUSE;
        param[32] = new SqlParameter("@DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE", SqlDbType.NVarChar);
        param[32].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE;
        //param[32].Value = FSVM.POSheet.DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE;

        param[33] = new SqlParameter("@DFR_PO_REN_RMDR", SqlDbType.NVarChar);
        param[33].Value = FSVM.DRD_SNO;
        param[34] = new SqlParameter("@DFR_PO_RPT_NOTF", SqlDbType.NVarChar);
        param[34].Value = FSVM.DRN_SNO;


        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "UPDATE_FACTSHYEET_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                    return new { Message = MessagesVM.Factsheet, data = dr["MSG"].ToString() };
                else
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
            }
        }
        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }



}