﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DocumentTypeMasterService
/// </summary>
public class DocumentTypeMasterService
{
    SubSonic.StoredProcedure sp;
    public IEnumerable<DocumentMst> BindDocumentData()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_DOCUMENT_TYPE_DETAILS]").GetReader();
        List<DocumentMst> BussMst = new List<DocumentMst>();
        while (reader.Read())
        {
            BussMst.Add(new DocumentMst()
            {
                DDT_CODE = reader.GetValue(0).ToString(),
                DDT_NAME = reader.GetValue(1).ToString(),
                DDT_STA_ID = reader.GetValue(2).ToString(),
                DDT_REM = reader.GetValue(3).ToString(),
            });
        }
        reader.Close();
        return BussMst;
    }

    public int Insert(DocumentMst Dataobj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_DOCUMENT_TYPE_DETAILS");
        sp.Command.AddParameter("@DDT_CODE", Dataobj.DDT_CODE, DbType.String);
        sp.Command.AddParameter("@DDT_NAME", Dataobj.DDT_NAME, DbType.String);
        sp.Command.AddParameter("@DDT_STA_ID", Dataobj.DDT_STA_ID, DbType.String);
        sp.Command.AddParameter("@DDT_REM", Dataobj.DDT_REM, DbType.String);
        sp.Command.AddParameter("@DDT_CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }

    //Update
    public Boolean Update(DocumentMst Update)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_DOCUMENT_TYPE_DETAILS");
        sp.Command.AddParameter("@DDT_CODE", Update.DDT_CODE, DbType.String);
        sp.Command.AddParameter("@DDT_NAME", Update.DDT_NAME, DbType.String);
        sp.Command.AddParameter("@DDT_STA_ID", Update.DDT_STA_ID, DbType.Int32);
        sp.Command.AddParameter("@DDT_REM", Update.DDT_REM, DbType.String);
        sp.Command.Parameters.Add("@DDT_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        sp.Execute();
        return true;
    }
}