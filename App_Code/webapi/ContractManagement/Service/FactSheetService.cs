﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using UtiltiyVM;

/// <summary>
/// Summary description for FactSheetService
/// </summary>
public class FactSheetService
{
    SubSonic.StoredProcedure sp;
    DataSet ds;

    public IEnumerable<RemainderDetails> GetReaminderDetails()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_REMAINDER_DETAILS]").GetReader();
        List<RemainderDetails> Reaminder = new List<RemainderDetails>();
        while (reader.Read())
        {
            Reaminder.Add(new RemainderDetails()
            {
                DRD_SNO = reader.GetValue(0).ToString(),
                DRD_NAME = reader.GetValue(1).ToString(),
                DRD_STA_ID = reader.GetValue(2).ToString(),
            });
        }
        reader.Close();
        return Reaminder;
    }

    public IEnumerable<NotifcationDetails> GetNotificationDetails()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_NOTIFICATION_DETAILS]").GetReader();
        List<NotifcationDetails> Notofication = new List<NotifcationDetails>();
        while (reader.Read())
        {
            Notofication.Add(new NotifcationDetails()
            {
                DRN_SNO = reader.GetValue(0).ToString(),
                DRN_NAME = reader.GetValue(1).ToString(),
                DRN_STA_ID = reader.GetValue(2).ToString(),
            });
        }
        reader.Close();
        return Notofication;
    }

    public IEnumerable<NoticesDetails> GetNotificDetails()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_NOTICE_PERIOD_DETAILS]").GetReader();
        List<NoticesDetails> Notofic = new List<NoticesDetails>();
        while (reader.Read())
        {
            Notofic.Add(new NoticesDetails()
            {
                DNP_CODE = reader.GetValue(0).ToString(),
                DNP_NAME = reader.GetValue(1).ToString(),
                DNP_STA_ID = reader.GetValue(2).ToString(),
            });
        }
        reader.Close();
        return Notofic;
    }

    public object Getrefnum()
    {
        sp = new SubSonic.StoredProcedure(Convert.ToString(HttpContext.Current.Session["TENANT"]) + "." + "GET_REF_NUMBER");
        sp.Command.Parameters.Add("@USER_ID", HttpContext.Current.Session["UID"], DbType.String);
        ds = sp.GetDataSet();

        return ds.Tables[0].Rows[0]["RefNum"];
    }

    public object Submit(FactSheetVm FSVM)
    {
        SqlParameter[] param = new SqlParameter[34];

        param[0] = new SqlParameter("@DFM_DCT_CODE", SqlDbType.NVarChar);
        param[0].Value = FSVM.MainSheet.DFM_DCT_CODE;
        param[1] = new SqlParameter("@DFM_DST_CODE", SqlDbType.NVarChar);
        param[1].Value = FSVM.MainSheet.DFM_DST_CODE;
        param[2] = new SqlParameter("@DFM_DBT_CODE", SqlDbType.NVarChar);
        param[2].Value = FSVM.MainSheet.DFM_DBT_CODE;
        param[3] = new SqlParameter("@DFM_DSD_CODE", SqlDbType.NVarChar);
        param[3].Value = FSVM.MainSheet.DFM_DSD_CODE;
        param[4] = new SqlParameter("@DFM_REF_NO", SqlDbType.NVarChar);
        param[4].Value = FSVM.MainSheet.DFM_REF_NO;
        param[5] = new SqlParameter("@USER_ID", SqlDbType.NVarChar);
        param[5].Value = HttpContext.Current.Session["UID"];


        param[6] = new SqlParameter("@DFDT_SIGN_DATE", SqlDbType.DateTime);
        param[6].Value = FSVM.AgreeMentSheet.DFDT_SIGN_DATE;
        param[7] = new SqlParameter("@DFDT_EFRM_DATE", SqlDbType.DateTime);
        param[7].Value = FSVM.AgreeMentSheet.DFDT_EFRM_DATE;
        param[8] = new SqlParameter("@DFDT_TRMN_DATE", SqlDbType.DateTime);
        param[8].Value = FSVM.AgreeMentSheet.DFDT_TRMN_DATE;

        param[9] = new SqlParameter("@DFDT_REN_DATE", SqlDbType.DateTime);
        param[9].Value = FSVM.AgreeMentSheet.DFDT_REN_DATE;
        param[10] = new SqlParameter("@DFDT_NOTF_PRD_WITH_CLAUSE", SqlDbType.NVarChar);
        param[10].Value = FSVM.AgreeMentSheet.DFDT_NOTF_PRD_WITH_CLAUSE;
        param[11] = new SqlParameter("@DFDT_NOTF_PRD_WITHOUT_CLAUSE", SqlDbType.NVarChar);
        param[11].Value = FSVM.AgreeMentSheet.DFDT_NOTF_PRD_WITHOUT_CLAUSE;

        param[12] = new SqlParameter("@DFR_REN_RMDR", SqlDbType.NVarChar);
        param[12].Value = FSVM.DRD_SNO;
        param[13] = new SqlParameter("@DFR_RPT_NOTF", SqlDbType.NVarChar);
        param[13].Value = FSVM.DRN_SNO;

        param[14] = new SqlParameter("@DFV_CNRT_VAL", SqlDbType.NVarChar);
        param[14].Value = FSVM.ValuesSheet.DFV_CNRT_VAL;
        param[15] = new SqlParameter("@DFV_CNRT_SCP", SqlDbType.NVarChar);
        param[15].Value = FSVM.ValuesSheet.DFV_CNRT_SCP;
        param[16] = new SqlParameter("@DFV_ANNL_VAL", SqlDbType.NVarChar);
        param[16].Value = FSVM.ValuesSheet.DFV_ANNL_VAL;


        param[17] = new SqlParameter("@KETLST", SqlDbType.Structured);
        param[17].Value = UtilityService.ConvertToDataTable(FSVM.BusinessStakehol);


        param[18] = new SqlParameter("@DFD_CT", SqlDbType.NVarChar);
        param[18].Value = FSVM.StakeholderSheet.DFD_CT;
        param[19] = new SqlParameter("@DFD_AC", SqlDbType.NVarChar);
        param[19].Value = FSVM.StakeholderSheet.DFD_AC;
        param[20] = new SqlParameter("@DFD_PT", SqlDbType.NVarChar);
        param[20].Value = FSVM.StakeholderSheet.DFD_PT;
        param[21] = new SqlParameter("@DFD_TC", SqlDbType.NVarChar);
        param[21].Value = FSVM.StakeholderSheet.DFD_TC;
        param[22] = new SqlParameter("@DFD_DW", SqlDbType.NVarChar);
        param[22].Value = FSVM.StakeholderSheet.DFD_DW;
        param[23] = new SqlParameter("@DFD_IPR", SqlDbType.NVarChar);
        param[23].Value = FSVM.StakeholderSheet.DFD_IPR;
        param[24] = new SqlParameter("@DFD_CONFD", SqlDbType.NVarChar);
        param[24].Value = FSVM.StakeholderSheet.DFD_CONFD;
        param[25] = new SqlParameter("@DFD_CD", SqlDbType.NVarChar);
        param[25].Value = FSVM.StakeholderSheet.DFD_CD;

        param[26] = new SqlParameter("@DOCUPLOAD", SqlDbType.Structured);
        param[26].Value = UtilityService.ConvertToDataTable(FSVM.DocumentDetails);

        param[27] = new SqlParameter("@DFDT_PO_EFRM_DATE", SqlDbType.DateTime);
        param[27].Value = (FSVM.POSheet == null)? null :FSVM.POSheet.DFDT_PO_EFRM_DATE;
       // param[27].Value =FSVM.POSheet.DFDT_PO_EFRM_DATE;

        param[28] = new SqlParameter("@DFDT_PO_TRMN_DATE", SqlDbType.DateTime);
        param[28].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_TRMN_DATE;
        //param[28].Value = FSVM.POSheet.DFDT_PO_TRMN_DATE;

        param[29] = new SqlParameter("@DFDT_PO_REN_DATE", SqlDbType.DateTime);
        param[29].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_REN_DATE;
        //param[29].Value = FSVM.POSheet.DFDT_PO_REN_DATE;
        param[30] = new SqlParameter("@DFDT_PO_NOTF_PRD_WITH_CLAUSE", SqlDbType.NVarChar);
        param[30].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_NOTF_PRD_WITH_CLAUSE;
        //param[30].Value = FSVM.POSheet.DFDT_PO_NOTF_PRD_WITH_CLAUSE;
        param[31] = new SqlParameter("@DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE", SqlDbType.NVarChar);
        param[31].Value = (FSVM.POSheet == null) ? null : FSVM.POSheet.DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE;
        //param[31].Value = FSVM.POSheet.DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE;

        param[32] = new SqlParameter("@DFR_PO_REN_RMDR", SqlDbType.NVarChar);
        param[32].Value = FSVM.DRD_SNO;
        param[33] = new SqlParameter("@DFR_PO_RPT_NOTF", SqlDbType.NVarChar);
        param[33].Value = FSVM.DRN_SNO;

        DataSet ds = new DataSet();
        //ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SUBMIT_CONTRACT_DETAILS", param);

        using (SqlDataReader dr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "SUBMIT_CONTRACT_DETAILS", param))
        {
            if (dr.Read())
            {
                if ((int)dr["FLAG"] == 1)
                    return new { Message = MessagesVM.Factsheet, data = dr["MSG"].ToString() };
                else
                    return new { Message = dr["MSG"].ToString(), data = (object)null };
            }
        }
        return new { Message = MessagesVM.UM_NO_REC, data = (object)null };
    }

    public dynamic UploadTemplate(HttpRequest httpRequest)
    {
        GetDocumentDetailsVM FDT;
        List<GetDocumentDetailsVM> FD = new List<GetDocumentDetailsVM>();
        if (httpRequest.Files.Count > 0)
        {
            String login = httpRequest.Params["login"];
            String typdoc = httpRequest.Params["CurrObj"];
            foreach (string file in httpRequest.Files)
            {
                var postedFile = httpRequest.Files[file];
                var extension = Path.GetExtension(postedFile.FileName);
                String filename = Guid.NewGuid() + extension;

                //var filePath = "C:\\ContractFileRepository\\" + filename;
                var filePath = HttpContext.Current.Server.MapPath("~/ContractFileRepository/"+ filename);
                if (!FileInUse(filePath))
                {
                    postedFile.SaveAs(filePath);
                }

                FDT = new GetDocumentDetailsVM();
                FDT.DFDC_DDT_CODE = typdoc;
                FDT.DFDC_ACT_NAME = postedFile.FileName;
                FDT.DFDC_NAME = filename;               
                FD.Add(FDT);
            }
            return new { Message = MessagesVM.UAD_UPLNOREC, data = FD };
        }
        else
            return new { Message = MessagesVM.UAD_UPLNOREC, data = (object)null };
    }

    public static bool FileInUse(string path)
    {
        try
        {
            using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
            {
                return false;
            }
            //return false;
        }
        catch (IOException ex)
        {
            return true;
        }
    }
}