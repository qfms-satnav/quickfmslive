﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ContractMasterService
/// </summary>
public class ContractMasterService
{
    SubSonic.StoredProcedure sp;
    public IEnumerable<ContractMst> BindContractData()
    {
        IDataReader reader = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "[GET_CONTRACT_DETAILS]").GetReader();
        List<ContractMst> ConMst = new List<ContractMst>();
        while (reader.Read())
        {
            ConMst.Add(new ContractMst()
            {
                DCT_CODE = reader.GetValue(0).ToString(),
                DCT_NAME = reader.GetValue(1).ToString(),
                DCT_STA_ID = reader.GetValue(2).ToString(),
                DCT_REM = reader.GetValue(3).ToString(),
            });
        }
        reader.Close();
        return ConMst;
    }

    public int Insert(ContractMst Dataobj)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "INSERT_CONTRACT_MASTER_DATA");
        sp.Command.AddParameter("@CODE", Dataobj.DCT_CODE, DbType.String);
        sp.Command.AddParameter("@NAME", Dataobj.DCT_NAME, DbType.String);
        sp.Command.AddParameter("@STA", Dataobj.DCT_STA_ID, DbType.String);
        sp.Command.AddParameter("@REMARKS", Dataobj.DCT_REM, DbType.String);
        sp.Command.AddParameter("@CREATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        int flag = (int)sp.ExecuteScalar();
        return flag;
    }

    //Update
    public Boolean Update(ContractMst Update)
    {
        sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "UPDATE_CONTRACT_MASTER_DETAILS");
        sp.Command.AddParameter("@DCT_CODE", Update.DCT_CODE, DbType.String);
        sp.Command.AddParameter("@DCT_NAME", Update.DCT_NAME, DbType.String);
        sp.Command.AddParameter("@DCT_STA_ID", Update.DCT_STA_ID, DbType.Int32);
        sp.Command.AddParameter("@DCT_REM", Update.DCT_REM, DbType.String);
        sp.Command.Parameters.Add("@DCT_UPDATED_BY", HttpContext.Current.Session["UID"], DbType.String);
        sp.Execute();
        return true;
    }

}