﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;



public class AgreementReportService
{
    SubSonic.StoredProcedure sp;

    public object GetAgreementReportData(AgreementReportDetails ContractData)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@MAIN_CAT", SqlDbType.VarChar);
            param[0].Value = ContractData.DMC_CODE;
            param[1] = new SqlParameter("@SUB_CAT", SqlDbType.VarChar);
            param[1].Value = ContractData.DBT_CODE;
            param[2] = new SqlParameter("@STATUS", SqlDbType.VarChar);
            param[2].Value = ContractData.DRS_CODE;           
            param[3] = new SqlParameter("@FROM_DATE", SqlDbType.DateTime);
            param[3].Value = ContractData.FROM_DATE;
            param[4] = new SqlParameter("@TO_DATE", SqlDbType.DateTime);
            param[4].Value = ContractData.TO_DATE;          
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_AGREEMENT_DETAILS_RPT", param);
            return new { data = ds.Tables[0] };
        }
        catch (Exception Ex)
        {

            return new { Message = "Invalid Input! Please try again", data = (object)null, ErrorMessage = Ex.InnerException };
        }
    }
}