﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SupplierWiseAgreementReportModel
/// </summary>
public class SupplierData
{
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
    public string SearchValue { get; set; }
}