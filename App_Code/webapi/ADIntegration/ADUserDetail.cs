﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Configuration;


public class ADUserDetail
{
    private string _username;
    private string _logintime;
    private string _logofftime;

    public string username
    {
        get { return _username; }
    }

    public string logintime
    {
        get { return _logintime; }
    }

    public string logofftime
    {
        get { return _logofftime; }
    }

    private static object GetProperty(SearchResult userDetail, String propertyName)
    {
        if (userDetail.Properties.Contains(propertyName))
        {
            return userDetail.Properties[propertyName][0];
        }
        else
        {
            return string.Empty;
        }
    }

    public static ADUserDetail GetUser(SearchResult directoryUser)
    {
        return new ADUserDetail(directoryUser);
    }

    private ADUserDetail(SearchResult directoryUser)
    {
        _username = GetProperty(directoryUser, ADProperties.LOGINNAME).ToString();
        object OBJ = GetProperty(directoryUser, ADProperties.LASTLOGON);

        _logintime = DateTime.FromFileTime(Convert.ToInt64(GetProperty(directoryUser, ADProperties.LASTLOGON))).ToString("dddd dd MMMM yyyy HH:mm:ss");
        _logofftime = DateTime.FromFileTime(Convert.ToInt64(GetProperty(directoryUser, ADProperties.LASTLOGOFF))).ToString("dddd dd MMMM yyyy HH:mm:ss");
    }


}

