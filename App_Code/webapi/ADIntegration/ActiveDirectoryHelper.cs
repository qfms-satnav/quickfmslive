﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Configuration;


public class ActiveDirectoryHelper
{
    private DirectoryEntry _directoryEntry = null;

    private DirectoryEntry SearchRoot
    {
        get
        {
            if (_directoryEntry == null)
            {
                _directoryEntry = new DirectoryEntry(LDAPPath, LDAPUser, LDAPPassword, AuthenticationTypes.Secure);
            }
            return _directoryEntry;
        }
    }

    private string LDAPPath
    {
        get { return ConfigurationManager.AppSettings["LDAPPath"]; }
    }

    private string LDAPUser
    {
        get { return ConfigurationManager.AppSettings["LDAPUser"]; }
    }

    private string LDAPPassword
    {
        get { return ConfigurationManager.AppSettings["LDAPPassword"]; }
    }

    public List<ADUserDetail> GetUserslist()
    {
        List<ADUserDetail> userlist = new List<ADUserDetail>();
        DirectorySearcher searcher = new DirectorySearcher(SearchRoot);
        string filter = "(&(objectCategory=person)(objectClass=user)(!userAccountControl:1.2.840.113556.1.4.803:=2))";
        searcher.Filter = filter;
        SearchResultCollection collection = searcher.FindAll();
        if (collection != null)
        {
            foreach (SearchResult result in collection)
            {
                //DirectoryEntry deobj = new DirectoryEntry(result.Path, LDAPUser, LDAPPassword);
                ADUserDetail userobj = ADUserDetail.GetUser(result);
                userlist.Add(userobj);
            }
        }
        return userlist;
    }
}
