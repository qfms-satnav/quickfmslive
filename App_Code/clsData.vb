Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web
Imports Microsoft.VisualBasic

Public Class clsData

#Region "Constructor and Connection"

    Public Sub New()

    End Sub

#End Region

    'This region block main purpose is to retrieve the data from the database depending upon the parameters.
    'It consists of different functions.
#Region "Select Command"

    Public Overloads Function fnSelectAllData(ByVal strTable As String) As DataSet
        Try
            Dim dtsDataSet As DataSet
            Dim sqlcmdSelect As String = "SELECT * FROM " & HttpContext.Current.Session("TENANT") & "." & strTable
            dtsDataSet = SqlHelper.ExecuteDataset(CommandType.Text, sqlcmdSelect)
            Return dtsDataSet

        Catch er As SqlException

        End Try
    End Function

    Public Overloads Function fnSelectData(ByVal strTable As String, ByVal strWhereClause As String) As DataSet
        Try
            Dim dtsDataSet As DataSet
            Dim sqlcmdSelect As String = "SELECT * FROM " & HttpContext.Current.Session("TENANT") & "." & strTable & " where " & strWhereClause
            dtsDataSet = SqlHelper.ExecuteDataset(CommandType.Text, sqlcmdSelect)
            Return dtsDataSet

        Catch er As SqlException

        End Try
    End Function

    Public Overloads Function fnSelectData(ByVal strQuery As String) As DataTable
        Try
            Dim dtblTable As DataTable
            dtblTable = SqlHelper.ExecuteDatatable(CommandType.Text, strQuery)
            Return dtblTable
        Catch er As SqlException

        End Try
    End Function

    Public Overloads Function fnSelectData(ByVal strQuery As StringBuilder) As DataTable
        Try
            Dim dtblTable As DataTable
            dtblTable = SqlHelper.ExecuteDatatable(CommandType.Text, strQuery.ToString)
            Return dtblTable

        Catch er As SqlException

        End Try
    End Function

    Public Function fnSelectScalar(ByVal strQuery As String) As Object
        Try
            Dim strValue As Object

            strValue = SqlHelper.ExecuteScalar(CommandType.Text, strQuery)
            Return strValue

        Catch er As SqlException

        End Try
    End Function

    Public Overloads Function fnSelectSchema(ByVal strTable As String) As DataTable
        Try
            Dim dtTable As New DataTable(strTable)
            Dim strQuery As String = "SELECT * FROM " & HttpContext.Current.Session("TENANT") & "." & strTable
            dtTable = SqlHelper.ExecutetableSchema(strQuery)
            dtTable.TableName = strTable

            Return dtTable
        Catch er As SqlException

        End Try
    End Function

#End Region

    'This region block main purpose is to insert the data into the database.
    'It consists of different functions. Each function will have different parameters.
#Region "Insert Command"

    Public Overloads Function fnInsertData(ByVal dtsDataSet As DataSet, ByVal strTable As String) As Boolean

        Try
            Dim strColumnName As String
            Dim strColumnValues As String
            Dim intColCount As Integer
            Dim intRowCount As Integer
            Dim intColumnCount As Integer = dtsDataSet.Tables(strTable).Columns.Count - 1
            For intRowCount = 0 To dtsDataSet.Tables(strTable).Rows.Count - 1
                For intColCount = 0 To intColumnCount
                    If (dtsDataSet.Tables(strTable).Columns(intColCount).AutoIncrement = False) Then
                        If Not IsDBNull(dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Then
                            If TypeOf (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Is String Then
                                Dim strValue As String = (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount))
                                strColumnValues += "'" & strValue & "',"
                                strColumnName += (dtsDataSet.Tables(strTable).Columns(intColCount).Caption & ",")
                            ElseIf TypeOf (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Is DateTime Then
                                Dim dtpDate As DateTime = (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount))
                                strColumnValues += "'" & dtpDate.ToString & "',"
                                strColumnName += (dtsDataSet.Tables(strTable).Columns(intColCount).Caption & ",")
                            Else
                                strColumnName += (dtsDataSet.Tables(strTable).Columns(intColCount).Caption & ",")
                                strColumnValues += (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount) & ",")
                            End If
                        End If
                    End If
                Next

                Dim strInsertCommand As String = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & strTable & "(" & strColumnName.Substring(0, strColumnName.Length - 1) & ") VALUES ( " & strColumnValues.Substring(0, strColumnValues.Length - 1) & ")"
                SqlHelper.ExecuteNonQuery(CommandType.Text, strInsertCommand)
                strColumnValues = ""
                strColumnName = ""
            Next
            fnInsertData = True

        Catch er As SqlException

            fnInsertData = False

        End Try
    End Function

    Public Overloads Function fnInsertData(ByVal dtsDataSet As DataSet) As Boolean

        Try
            Dim strColumnName As String
            Dim strColumnValues As String
            Dim intColCount As Integer
            Dim intRowCount As Integer
            Dim intColumnCount As Integer
            Dim strTable As String
            Dim dsDSet As New DataSet
            Dim intTableCount As Integer
            For intTableCount = 0 To dtsDataSet.GetChanges(DataRowState.Added).Tables.Count - 1
                strTable = dtsDataSet.Tables(intTableCount).TableName

                intColumnCount = dtsDataSet.Tables(intTableCount).Columns.Count - 1
                For intRowCount = 0 To dtsDataSet.Tables(intTableCount).Rows.Count - 1
                    For intColCount = 0 To intColumnCount
                        If (dtsDataSet.Tables(intTableCount).Columns(intColCount).AutoIncrement = False) Then
                            If Not IsDBNull(dtsDataSet.Tables(intTableCount).Rows(intRowCount).Item(intColCount)) Then
                                If TypeOf (dtsDataSet.Tables(intTableCount).Rows(intRowCount).Item(intColCount)) Is String Then
                                    Dim strValue As String = (dtsDataSet.Tables(intTableCount).Rows(intRowCount).Item(intColCount))
                                    strColumnValues += "'" & strValue & "',"
                                    strColumnName += (dtsDataSet.Tables(intTableCount).Columns(intColCount).Caption & ",")
                                ElseIf TypeOf (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Is DateTime Then
                                    Dim dtpDate As DateTime = (dtsDataSet.Tables(intTableCount).Rows(intRowCount).Item(intColCount))
                                    strColumnValues += "'" & dtpDate.ToString & "',"
                                    strColumnName += (dtsDataSet.Tables(intTableCount).Columns(intColCount).Caption & ",")
                                Else

                                    strColumnName += (dtsDataSet.Tables(intTableCount).Columns(intColCount).Caption & ",")
                                    strColumnValues += (dtsDataSet.Tables(intTableCount).Rows(intRowCount).Item(intColCount) & ",")
                                End If
                            End If
                        End If
                    Next

                    Dim strInsertCommand As String = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & strTable & "(" & strColumnName.Substring(0, strColumnName.Length - 1) & ") VALUES ( " & strColumnValues.Substring(0, strColumnValues.Length - 1) & ")"
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strInsertCommand)

                    strColumnValues = ""
                    strColumnName = ""
                Next
            Next
            fnInsertData = True
        Catch er As SqlException

            fnInsertData = False

        End Try
    End Function

    Public Overloads Function fnInsertData(ByVal dtblTable As DataTable) As Boolean

        Try
            Dim strColumnName As String
            Dim strColumnValues As String
            Dim intColCount As Integer
            Dim intRowCount As Integer
            Dim intColumnCount As Integer = dtblTable.Columns.Count - 1
            Dim strTable As String

            strTable = dtblTable.TableName
            For intRowCount = 0 To dtblTable.Rows.Count - 1

                For intColCount = 0 To intColumnCount
                    If (dtblTable.Columns(intColCount).AutoIncrement = False) Then
                        If Not IsDBNull(dtblTable.Rows(intRowCount).Item(intColCount)) Then
                            If TypeOf (dtblTable.Rows(intRowCount).Item(intColCount)) Is String Then
                                Dim strValue As String = (dtblTable.Rows(intRowCount).Item(intColCount))
                                strColumnValues += "'" & strValue & "',"
                                strColumnName += (dtblTable.Columns(intColCount).Caption & ",")
                            ElseIf TypeOf (dtblTable.Rows(intRowCount).Item(intColCount)) Is DateTime Then
                                Dim dtpDate As DateTime
                                dtpDate = dtblTable.Rows(intRowCount).Item(intColCount)
                                strColumnValues += "'" & dtpDate.ToString & "',"     '"'" & dtpDate.ToString("MM/dd/yyyy") & "',"
                                strColumnName += (dtblTable.Columns(intColCount).Caption & ",")
                            Else
                                strColumnName += (dtblTable.Columns(intColCount).Caption & ",")
                                strColumnValues += (dtblTable.Rows(intRowCount).Item(intColCount) & ",")
                            End If
                        End If
                    End If
                Next

                Dim strInsertCommand As String = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & strTable & " (" & strColumnName.Substring(0, strColumnName.Length - 1) & ") VALUES ( " & strColumnValues.Substring(0, strColumnValues.Length - 1) & ")"
                SqlHelper.ExecuteNonQuery(CommandType.Text, strInsertCommand)

                strColumnValues = ""
                strColumnName = ""
            Next
            fnInsertData = True
        Catch er As SqlException

            fnInsertData = False

        End Try
    End Function

    Public Overloads Function fnInsertIdentityData(ByVal dtblTable As DataTable) As Int64

        Try
            Dim strColumnName As String
            Dim strColumnValues As String
            Dim intColCount As Integer
            Dim intRowCount As Integer
            Dim intColumnCount As Integer = dtblTable.Columns.Count - 1
            Dim strTable As String
            Dim strTbl As String
            Dim objDr As SqlDataReader

            strTable = dtblTable.TableName
            For intRowCount = 0 To dtblTable.Rows.Count - 1

                For intColCount = 0 To intColumnCount
                    If (dtblTable.Columns(intColCount).AutoIncrement = False) Then
                        If Not IsDBNull(dtblTable.Rows(intRowCount).Item(intColCount)) Then
                            If TypeOf (dtblTable.Rows(intRowCount).Item(intColCount)) Is String Then
                                Dim strValue As String = (dtblTable.Rows(intRowCount).Item(intColCount))
                                strColumnValues += "'" & strValue & "',"
                                strColumnName += (dtblTable.Columns(intColCount).Caption & ",")
                            ElseIf TypeOf (dtblTable.Rows(intRowCount).Item(intColCount)) Is DateTime Then
                                Dim dtpDate As DateTime
                                dtpDate = dtblTable.Rows(intRowCount).Item(intColCount)
                                strColumnValues += "'" & dtpDate.ToString & "',"
                                strColumnName += (dtblTable.Columns(intColCount).Caption & ",")
                            Else
                                strColumnName += (dtblTable.Columns(intColCount).Caption & ",")
                                strColumnValues += (dtblTable.Rows(intRowCount).Item(intColCount) & ",")
                            End If
                        End If
                    End If
                Next

                Dim strInsertCommand As String = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & strTable & "(" & strColumnName.Substring(0, strColumnName.Length - 1) & ") VALUES ( " & strColumnValues.Substring(0, strColumnValues.Length - 1) & ") SELECT IDENT_CURRENT('" & strTable & "')"
                objDr = SqlHelper.ExecuteReader(CommandType.Text, strInsertCommand)
                While objDr.Read
                    fnInsertIdentityData = objDr.GetValue(0)
                End While
                objDr.Close()

                strColumnValues = ""
                strColumnName = ""
            Next

            Return fnInsertIdentityData
        Catch er As SqlException

        End Try
    End Function

#End Region

    'This region block main purpose is to Update the data in the database.
    'It consists of different functions. Each function will have different parameters.
#Region "Update Command"

    Public Function fnUpdateData(ByVal dtsDataSet As DataSet, ByVal strTable As String) As Boolean
        Try
            Dim strColumnName As String
            Dim strColumnValues As String
            Dim strUpdateCommand As String
            Dim intColCount As Integer
            Dim intRowCount As Integer
            Dim intColumnCount As Integer
            Dim intTableCount As Integer
            Dim dtrPrimaryColumns() As DataColumn
            Dim dtrColumn As DataColumn
            For intTableCount = 0 To dtsDataSet.GetChanges(DataRowState.Modified).Tables.Count - 1
                dtrPrimaryColumns = dtsDataSet.GetChanges(DataRowState.Modified).Tables(intTableCount).PrimaryKey
                For intRowCount = 0 To dtsDataSet.Tables(strTable).Rows.Count - 1
                    strTable = dtsDataSet.Tables(intTableCount).TableName
                    intColumnCount = dtsDataSet.Tables(strTable).Columns.Count - 1
                    For intColCount = 0 To intColumnCount
                        If (dtsDataSet.Tables(0).Columns(intColCount).AutoIncrement = False) Then
                            If Not IsDBNull(dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Then
                                If TypeOf (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Is String Then
                                    Dim strValue As String = (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount))
                                    strColumnValues = "'" & strValue & "',"
                                    strColumnName = (dtsDataSet.Tables(strTable).Columns(intColCount).Caption & "=")
                                    strUpdateCommand += strColumnName + strColumnValues
                                ElseIf TypeOf (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Is DateTime Then
                                    Dim dtpDate As DateTime
                                    dtpDate = dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)
                                    strColumnValues = "'" & dtpDate.ToString & "',"          '"'" & dtpDate.ToString("MM/dd/yyyy") & "',"
                                    strColumnName = (dtsDataSet.Tables(strTable).Columns(intColCount).Caption & "=")
                                    strUpdateCommand += strColumnName + strColumnValues
                                Else
                                    strColumnName = (dtsDataSet.Tables(strTable).Columns(intColCount).Caption & "=")
                                    strColumnValues = (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount) & ",")
                                    strUpdateCommand += strColumnName + strColumnValues
                                End If
                            End If
                        End If

                    Next

                    Dim strCondition As String
                    Dim intPrimaryColCount As Integer
                    For intPrimaryColCount = 0 To UBound(dtrPrimaryColumns)
                        If intPrimaryColCount = UBound(dtrPrimaryColumns) Then
                            strCondition += dtrPrimaryColumns(intPrimaryColCount).ColumnName & " = " & fnDataType(dtsDataSet.Tables(strTable).Rows(intRowCount).Item(dtrPrimaryColumns(intPrimaryColCount).ColumnName))
                        Else
                            strCondition += dtrPrimaryColumns(intPrimaryColCount).ColumnName & " = " & fnDataType(dtsDataSet.Tables(strTable).Rows(intRowCount).Item(dtrPrimaryColumns(intPrimaryColCount).ColumnName)) & " AND "
                        End If
                    Next

                    Dim strUpdate As String = "UPDATE " & HttpContext.Current.Session("TENANT") & "." & strTable & " SET " & strUpdateCommand.Substring(0, strUpdateCommand.Length - 1) & " WHERE " & strCondition
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strUpdate)

                    strColumnValues = ""
                    strColumnName = ""
                    strUpdateCommand = ""
                    strCondition = ""
                Next
            Next
            fnUpdateData = True
        Catch er As SqlException
            fnUpdateData = False

        End Try
    End Function

    Private Function fnDataType(ByVal objItem As Object) As String
        Try
            If TypeOf (objItem) Is String Then
                objItem = "'" & objItem & "'"
            End If
            Return objItem
        Catch er As Exception
            Console.Write(er.Message)
        End Try
    End Function

    Public Function fnUpdateData(ByVal dtblTable As DataTable) As Boolean
        Try
            Dim strColumnName As String
            Dim strColumnValues As String
            Dim strUpdateCommand As String
            Dim intColCount As Integer
            Dim intRowCount As Integer
            Dim intColumnCount As Integer
            Dim strTable As String
            Dim dtrPrimaryColumns() As DataColumn
            For intRowCount = 0 To dtblTable.Rows.Count - 1
                strTable = dtblTable.TableName
                dtrPrimaryColumns = dtblTable.PrimaryKey
                intColumnCount = dtblTable.Columns.Count - 1
                For intColCount = 0 To intColumnCount

                    If (dtblTable.Columns(intColCount).AutoIncrement = False) Then
                        If Not IsDBNull(dtblTable.Rows(intRowCount).Item(intColCount)) Then
                            If TypeOf (dtblTable.Rows(intRowCount).Item(intColCount)) Is String Then
                                Dim strValue As String = (dtblTable.Rows(intRowCount).Item(intColCount))
                                strColumnValues = "'" & strValue & "',"
                                strColumnName = (dtblTable.Columns(intColCount).Caption & "=")
                                strUpdateCommand += strColumnName + strColumnValues
                            ElseIf TypeOf (dtblTable.Rows(intRowCount).Item(intColCount)) Is DateTime Then
                                Dim dtpDate As DateTime
                                dtpDate = dtblTable.Rows(intRowCount).Item(intColCount)
                                strColumnValues = "'" & dtpDate.ToString & "',"
                                strColumnName = (dtblTable.Columns(intColCount).Caption & "=")
                                strUpdateCommand += strColumnName + strColumnValues
                            Else
                                strColumnName = (dtblTable.Columns(intColCount).Caption & "=")
                                strColumnValues = (dtblTable.Rows(intRowCount).Item(intColCount) & ",")
                                strUpdateCommand += strColumnName + strColumnValues
                            End If
                        End If
                    End If

                Next

                Dim strCondition As String
                Dim intPrimaryColCount As Integer
                For intPrimaryColCount = 0 To UBound(dtrPrimaryColumns)
                    If intPrimaryColCount = UBound(dtrPrimaryColumns) Then
                        strCondition += dtrPrimaryColumns(intPrimaryColCount).ColumnName & " = " & fnDataType(dtblTable.Rows(intRowCount).Item(dtrPrimaryColumns(intPrimaryColCount).ColumnName))
                    Else
                        strCondition += dtrPrimaryColumns(intPrimaryColCount).ColumnName & " = " & fnDataType(dtblTable.Rows(intRowCount).Item(dtrPrimaryColumns(intPrimaryColCount).ColumnName)) & " AND "
                    End If
                Next

                strUpdateCommand = "UPDATE " & HttpContext.Current.Session("TENANT") & "." & strTable & " SET " & strUpdateCommand.Substring(0, strUpdateCommand.Length - 1) & " WHERE " & strCondition
                SqlHelper.ExecuteNonQuery(CommandType.Text, strUpdateCommand)

                strColumnValues = ""
                strColumnName = ""
                strUpdateCommand = ""
                strCondition = ""
            Next
            fnUpdateData = True

        Catch er As SqlException

            fnUpdateData = False

        End Try
    End Function

    Public Function fnUpdateRow(ByVal strTable As String, ByVal strColumn As String, ByVal strValues As String, ByVal strWhereClause As String) As Boolean
        Try
            Dim strColumns() As String
            Dim strColumnValues() As String
            Dim intRetCol As Int16
            Dim intRetVal As Int16
            Dim i As Int16
            Dim strUpdateCommand As String

            strUpdateCommand = "update " & HttpContext.Current.Session("TENANT") & "." & strTable & " set "

            intRetCol = InStr(1, strColumn, ",")

            If Not intRetCol = 0 Then

                strColumns = Split(strColumn, ",") 'Splitting colname which is passed as parameter

                strColumnValues = Split(strValues, ",") 'Splitting values which is passed as parameter

                For i = 0 To UBound(strColumns)

                    strUpdateCommand = strUpdateCommand & strColumns(i) & "=" & strColumnValues(i).ToString

                    If Not i = UBound(strColumns) Then
                        strUpdateCommand = strUpdateCommand & ","
                    End If
                Next
            Else

                strUpdateCommand = strUpdateCommand & strColumn & "=" & strValues
            End If

            If Not strWhereClause = "" Then
                strUpdateCommand = strUpdateCommand & " where " & strWhereClause
            End If

            SqlHelper.ExecuteNonQuery(CommandType.Text, strUpdateCommand)

            fnUpdateRow = True

        Catch er As Exception
            Console.Write(er.Message)
            fnUpdateRow = False

        End Try

    End Function

    'to be deleted
    Public Function fnUpdateData(ByVal dtsDataSet As DataSet, ByVal strTable As String, ByVal strCondition As String) As Boolean
        Try
            Dim strColumnName As String
            Dim strColumnValues As String
            Dim strUpdateCommand As String
            Dim intColCount As Integer
            Dim intRowCount As Integer
            Dim intColumnCount As Integer
            Dim intTableCount As Integer
            For intTableCount = 0 To dtsDataSet.GetChanges(DataRowState.Modified).Tables.Count - 1
                For intRowCount = 0 To dtsDataSet.Tables(strTable).Rows.Count - 1
                    strTable = dtsDataSet.Tables(intTableCount).TableName
                    intColumnCount = dtsDataSet.Tables(strTable).Columns.Count - 1
                    For intColCount = 0 To intColumnCount
                        If (dtsDataSet.Tables(0).Columns(intColCount).AutoIncrement = False) Then
                            If Not IsDBNull(dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Then
                                If TypeOf (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount)) Is String Then
                                    Dim strValue As String = (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount))
                                    strColumnValues = "'" & strValue & "',"
                                    strColumnName = (dtsDataSet.Tables(strTable).Columns(intColCount).Caption & "=")
                                    strUpdateCommand += strColumnName + strColumnValues
                                Else

                                    strColumnName = (dtsDataSet.Tables(strTable).Columns(intColCount).Caption & "=")
                                    strColumnValues = (dtsDataSet.Tables(strTable).Rows(intRowCount).Item(intColCount) & ",")
                                    strUpdateCommand += strColumnName + strColumnValues
                                End If
                            End If
                        End If

                    Next


                    strUpdateCommand = "UPDATE " & HttpContext.Current.Session("TENANT") & "." & strTable & " SET " & strUpdateCommand.Substring(0, strUpdateCommand.Length - 1) & " WHERE " & strCondition
                    SqlHelper.ExecuteNonQuery(CommandType.Text, strUpdateCommand)

                    strColumnValues = ""
                    strColumnName = ""
                    strUpdateCommand = ""
                Next
            Next
            fnUpdateData = True
        Catch er As SqlException

            fnUpdateData = False

        End Try
    End Function

#End Region

    'This region block main purpose is to Delete the data from the database.
    'It consists of different functions. Each function will have different parameters.
#Region "Delete Command"

    Public Overloads Function fnDeleteRow(ByVal strTable As String, ByVal strWhereClause As String) As Boolean
        Try
            Dim strDelete As String = "DELETE FROM " & HttpContext.Current.Session("TENANT") & "." & strTable & " WHERE  " & strWhereClause
            SqlHelper.ExecuteNonQuery(CommandType.Text, strDelete)

            fnDeleteRow = True

        Catch er As Exception

            fnDeleteRow = False
        End Try
    End Function

#End Region

End Class
