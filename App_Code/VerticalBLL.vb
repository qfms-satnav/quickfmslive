Imports System.Configuration.ConfigurationManager
Imports Microsoft.VisualBasic
Imports Amantra.VerticalDALN
Imports Amantra.VerticalDTON
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Web
Imports System

Namespace Amantra.VerticalBLLN

    Public Class VerticalBLL
#Region "[ BLL and DTO INSTANCE CREATION ]"
        '' ----------------------------------------------------------------------------------------------- 
        ''' <summary> 
        ''' <Author>Srinivasulu M</Author> 
        ''' </summary> 
        Dim verdal As New VerticalDAL()
        Dim verDTO As IList(Of VerticalDTO) = New List(Of VerticalDTO)()
#End Region

#Region "[Get Requisition Details]"
        Public Function GetRequIds(ByVal Sta_id As Integer) As IList(Of VerticalDTO)
            verDTO = verdal.GetRequisitionIDs(Sta_id)
            Return verDTO
        End Function
#End Region
#Region "[Get Requisition Details For Vertical]"
        Public Function GetRequIdsForVertical() As Data.DataTable
            Dim dt As Data.DataTable = verdal.GetRequisitionIDsForVertical()
            Return dt
        End Function
#End Region
#Region "[Get Month and year for ReqId]"
        Public Function GetDatesforreqID(ByVal req_id As String) As IList(Of VerticalDTO)
            verDTO = verdal.GetdatesforReqIDs(req_id)
            Return verDTO
        End Function
#End Region

#Region "[Get Month and year for ReqId]"
        Public Function GetSelectedVerticalReqIDs(ByVal vert_id As String) As IList(Of VerticalDTO)
            verDTO = verdal.GetSelectedVerticalReqIDs(vert_id)
            Return verDTO
        End Function
#End Region

#Region "[Get data for ReqId]"
        Public Function GetDataforreqID(ByVal req_id As String, ByVal month As DateTime, ByVal year As DateTime) As IList(Of VerticalDTO)
            verDTO = verdal.GetDataforReqIDs(req_id, month, year)
            Return verDTO
        End Function
#End Region

#Region "[Get data for Towers]"
        Public Function GetDataforTowers(ByVal loc_id As String) As IList(Of VerticalDTO)
            verDTO = verdal.GetDataforTowers(loc_id)
            Return verDTO
        End Function
#End Region

#Region "[Get data for Floors]"
        Public Function GetDataforFloors(ByVal twr_id As String, ByVal loc_id As String) As IList(Of VerticalDTO)
            verDTO = verdal.GetDataforFloors(twr_id, loc_id)
            Return verDTO
        End Function
#End Region

#Region "[Get data for Wings]"
        Public Function GetDataforWings(ByVal flr_id As String, ByVal twr_id As String, ByVal loc_id As String) As IList(Of VerticalDTO)
            verDTO = verdal.GetDataforWings(flr_id, twr_id, loc_id)
            Return verDTO
        End Function
#End Region

#Region "[Get data for Space]"
        Public Function GetDataforSpace(ByVal spc_type As String, ByVal frmdate As DateTime, ByVal toDate As DateTime, ByVal twr_id As String, ByVal flr_id As String, ByVal wng_id As String, ByVal loc_id As String) As IList(Of VerticalDTO)
            verDTO = verdal.GetDataforSpaceIDs(frmdate, toDate, spc_type, twr_id, flr_id, wng_id, loc_id)
            Return verDTO
        End Function
#End Region

#Region "[ InsertSpace ]"
        '' ----------------------------------------------------------------------------------------------- 

        Public Function insertSpaceDetails(ByVal vdto As VerticalDTO, ByVal strVertical As String) As Integer
            Dim iCountryID As Integer = 0
            ' vdto.CountryId = Convert.ToString(iCountryID)
            iCountryID = verdal.insertSpaceID(vdto, strVertical)
            Return iCountryID
        End Function

        '' ----------------------------------------------------------------------------------------------- 
#End Region


#Region "[ Insert Vertical Allocation Data ]"
        '' ----------------------------------------------------------------------------------------------- 

        Public Function insertVerAllocDetails(ByVal vdto As VerticalDTO) As Integer
            Dim iCountryID As Integer = 0
            ' vdto.CountryId = Convert.ToString(iCountryID)
            iCountryID = verdal.insertVerticalAlloc(vdto)
            Return iCountryID
        End Function

        '' ----------------------------------------------------------------------------------------------- 
#End Region

#Region "[ Insert Child Vertical Allocation Data ]"
        '' ----------------------------------------------------------------------------------------------- 

        Public Function insertVerAllocChildDetails(ByVal vdto As VerticalDTO) As Integer
            Dim iCountryID As Integer = 0
            ' vdto.CountryId = Convert.ToString(iCountryID)
            iCountryID = verdal.insertVerticalAllocDtls(vdto)
            Return iCountryID
        End Function

        '' ----------------------------------------------------------------------------------------------- 
#End Region

#Region "[ Update Vertical Status ids ]"
        '' ----------------------------------------------------------------------------------------------- 

        Public Function updateVertStatusID(ByVal req_id As String, ByVal sta_id As String) As Integer
            Dim iCountryID As Integer = 0
            ' vdto.CountryId = Convert.ToString(iCountryID)
            iCountryID = verdal.UpdateVertStatus(req_id, sta_id)
            Return iCountryID
        End Function

        '' ----------------------------------------------------------------------------------------------- 
#End Region

#Region "[Get data for ReqId]"
        Public Function GetPartialAllocVerticalData(ByVal req_id As String, ByVal month As DateTime, ByVal year As DateTime) As IList(Of VerticalDTO)
            verDTO = verdal.GetPartialAllocverticalsbyReqIDs(req_id, month, year)
            Return verDTO
        End Function
#End Region
        Public Function getVerticalName(ByVal strVerticalCode As String) As String
            'strSQL = "SELECT isnull(VER_NAME,'NA')as VerticalName FROM " & HttpContext.Current.Session("TENANT") & "."  & "VERTICAL WHERE VER_CODE = '" & strVerticalCode & "'"
            strSQL = "SELECT isnull(COST_CENTER_NAME,'NA')as VerticalName FROM " & HttpContext.Current.Session("TENANT") & "." & "COSTCENTER WHERE COST_CENTER_CODE = '" & strVerticalCode & "'"
            Return SqlHelper.ExecuteScalar(Data.CommandType.Text, strSQL).ToString()
        End Function

        Private Function Data() As Object
            Throw New System.NotImplementedException
        End Function

    End Class

End Namespace
