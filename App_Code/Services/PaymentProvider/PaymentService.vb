#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Configuration
Imports System.Configuration.Provider
Imports System.Web.Configuration
Imports System.Web
Imports System.Web.Caching
Imports System.Data
Imports Commerce.Providers
Imports System.Text.RegularExpressions
Imports Commerce.Common

Namespace Commerce.Providers
	Public Class PaymentService
		#Region "Provider-specific bits"
		Private Shared _provider As PaymentProvider = Nothing
		Private Shared _lock As Object = New Object()

		Public ReadOnly Property Provider() As PaymentProvider
			Get
				Return _provider
			End Get
		End Property

		Private Shared ReadOnly Property Instance() As PaymentProvider
			Get
				LoadProvider()
				Return _provider
			End Get
		End Property
		Private Shared Sub LoadProvider()
			' Avoid claiming lock if providers are already loaded
			If _provider Is Nothing Then
				SyncLock _lock
					' Do this again to make sure _provider is still null
					If _provider Is Nothing Then
						' Get a reference to the <PaymentService> section
						Dim section As PaymentServiceSection = CType(WebConfigurationManager.GetSection ("PaymentService"), PaymentServiceSection)

						' Load registered providers and point _provider
						' to the default provider

						'since this is a CC provider, we only want one
						'so no collections for providers.
						'however feel free to change this as needed.
						_provider = CType(ProvidersHelper.InstantiateProvider(section.Providers(0), GetType(PaymentProvider)), PaymentProvider)

						If _provider Is Nothing Then
							Throw New ProviderException ("Unable to load default PaymentProvider")
						End If
					End If
				End SyncLock
			End If
		End Sub
		#End Region

		#Region "Methods"

			#Region "Gateway Charger"

			Public Shared Function RunCharge(ByVal order As Commerce.Common.Order) As Commerce.Common.Transaction

				'validations
				'CCNumber
				TestCondition.IsTrue(IsValidCardType(order.CreditCardNumber, order.CreditCardType),"Invalid Credit Card Number")

				'current expiration
				Dim expDate As DateTime = New DateTime(order.CreditCardExpireYear,order.CreditCardExpireMonth, 28)
				TestCondition.IsTrue(expDate >= getoffsetdate(Date.Today), "This credit card appears to be expired")

				'amount>0
				TestCondition.IsGreaterThanZero(order.OrderTotal, "Charge amount cannot be 0 or less")

				Dim result As Commerce.Common.Transaction=Instance.Charge(order)


				result.TransactionDate = DateTime.UtcNow
				result.Amount = order.OrderTotal

				Return result
			End Function

			'Many thanks to Paul Ingles for this Code
			'http://www.codeproject.com/aspnet/creditcardvalidator.asp
			'Modified by Spook, 3/2006
			Public Shared Function IsValidCardType(ByVal cardNumber As String, ByVal CardType As Commerce.Common.CreditCardType) As Boolean
				Dim bOut As Boolean = False


				' AMEX -- 34 or 37 -- 15 length
				If (Regex.IsMatch(cardNumber, "^(34|37)")) AndAlso ((CardType=Commerce.Common.CreditCardType.Amex)) Then
					bOut= 15 = cardNumber.Length


				' MasterCard -- 51 through 55 -- 16 length
				ElseIf (Regex.IsMatch(cardNumber, "^(51|52|53|54|55)")) AndAlso ((CardType=Commerce.Common.CreditCardType.MasterCard)) Then
					bOut= 16 = cardNumber.Length

				' VISA -- 4 -- 13 and 16 length
				ElseIf (Regex.IsMatch(cardNumber, "^(4)")) AndAlso ((CardType=Commerce.Common.CreditCardType.VISA)) Then
					bOut= 13 = cardNumber.Length OrElse 16 = cardNumber.Length

				' Discover -- 6011 -- 16 length
				ElseIf (Regex.IsMatch(cardNumber, "^(6011)")) AndAlso ((CardType=Commerce.Common.CreditCardType.Discover)) Then
					bOut= 16 = cardNumber.Length
				End If

				Return bOut
			End Function

			''' <summary>
			''' Runs a refund for the passed in order, and returns a verification 
			''' string. An exception will be thrown if there is an error.
			''' </summary>
			''' <param name="order"></param>
			''' <returns></returns>
			Public Shared Function Refund(ByVal order As Commerce.Common.Order) As String
				'validations

				'there has to be an initial payment
				If order.Transactions.Count = 0 Then
					Throw New Exception("This order has no existing transactions; please be sure the transactions are loaded for this order. Cannot refund")
				End If


				Dim sOut As String =Instance.Refund(order)

				Return sOut
			End Function

			#End Region

		#End Region
	End Class


End Namespace
