﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.IO

Namespace Commerce.Providers
    Public Class ICICIPaymentProvider
        Inherits PaymentProvider
        'properties
#Region "Passed-in Props"
        Private MerchantId As String = ""
        Private MerchantKey As String = ""
        'Private transactionKey As String = ""
        Private serverURL As String = ""
        Private currencyCode As String = "INR"
#End Region

#Region "Provider specific behaviors"

        Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)
            ' Verify that config isn't null
            If config Is Nothing Then
                Throw New ArgumentNullException("config")
            End If

            ' Assign the provider a default name if it doesn't have one
            If String.IsNullOrEmpty(name) Then
                config.Remove("name")
                config.Add("name", "ICICIPaymentProvider")
            End If
            ' Add a default "description" attribute to config if the
            ' attribute doesn't exist or is empty
            If String.IsNullOrEmpty(config("description")) Then
                config.Remove("description")
                config.Add("description", "ICICI Payment Provider")
            End If
            MyBase.Initialize(name, config)

            MerchantId = config("MerchantId").ToString()
            If String.IsNullOrEmpty(MerchantId) Then
                Throw New ProviderException("Empty ICICI Merchant Id value")
            End If
            config.Remove("MerchantId")

            MerchantKey = config("MerchantKey").ToString()
            If String.IsNullOrEmpty(MerchantKey) Then
                Throw New ProviderException("Empty ICICI Merchant Key value")
            End If
            config.Remove("MerchantKey")

            'transactionKey = config("transactionKey").ToString()
            'If String.IsNullOrEmpty(transactionKey) Then
            '    Throw New ProviderException("Empty Transaction Key value")
            'End If
            'config.Remove("transactionKey")

            serverURL = config("serverURL").ToString()
            If String.IsNullOrEmpty(serverURL) Then
                Throw New ProviderException("Empty Server URL value")
            End If
            config.Remove("serverURL")

            currencyCode = config("currencyCode").ToString()
            If String.IsNullOrEmpty(currencyCode) Then
                Throw New ProviderException("Empty Currency Code value")
            End If
            config.Remove("currencyCode")
        End Sub

        Public Overrides ReadOnly Property Name() As String
            Get
                Return Nothing
            End Get
        End Property
#End Region

        Public Overrides Function Charge(ByVal order As Commerce.Common.Order) As Commerce.Common.Transaction
            'string sOut = "";
            Dim transactionid As String
            Dim reason As String
            Dim trans As Commerce.Common.Transaction = New Commerce.Common.Transaction()

            Dim dTotal As Decimal = order.OrderTotal
            Dim roundTo As Integer = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits
            dTotal = Math.Round(dTotal, roundTo)

            'make sure that the ExpMonth has a leading 0
            Dim sExpMonth As String = order.CreditCardExpireMonth.ToString()
            If order.CreditCardExpireMonth < 10 Then
                sExpMonth = "0" & sExpMonth
            End If

            Dim result As String = ""
            Dim request As String
            request = "M_id=" & MerchantId
            request = request & "&" & "M_key=" & MerchantKey
            request = request & "&" & "C_name=" & order.BillingAddress.FirstName
            request = request & "&" & "C_address=" & order.BillingAddress.Address1
            request = request & "&" & "C_city=" & order.BillingAddress.City
            request = request & "&" & "C_state=" & order.BillingAddress.StateOrRegion
            request = request & "&" & "C_zip=" & order.BillingAddress.Zip
            request = request & "&" & "C_country=" & order.BillingAddress.Country
            request = request & "&" & "C_email=" & order.BillingAddress.Email
            request = request & "&" & "C_cardnumber=" & order.CreditCardNumber
            request = request & "&" & "C_exp=" & sExpMonth & order.CreditCardExpireYear.ToString()
            request = request & "&" & "T_code=" & "01"
            request = request & "&" & "T_amt=" & "1.00" 'dTotal.ToString()

            Dim myWriter As StreamWriter = Nothing
            Dim objRequest As HttpWebRequest = CType(WebRequest.Create(serverURL), HttpWebRequest)
            objRequest.Method = "POST"
            objRequest.ContentLength = request.Length
            objRequest.ContentType = "application/x-www-form-urlencoded"
            Dim outByte() As Byte = System.Text.Encoding.ASCII.GetBytes(request)
            objRequest.ContentLength = outByte.Length
            Dim outputStream As Stream = objRequest.GetRequestStream()
            outputStream.Write(outByte, 0, outByte.Length)
            Dim webResponse As HttpWebResponse = objRequest.GetResponse()
            Dim statusCode As Integer = webResponse.StatusCode
            Dim sr As New StreamReader(webResponse.GetResponseStream(), System.Text.Encoding.ASCII)

            Dim response1 As String = sr.ReadToEnd()

            transactionid = response1.Substring(46, 10)

            If response1.Chars(1).ToString = "A" Then
                trans.GatewayResponse = "Transaction Approved"
                trans.AuthorizationCode = "A"
                trans.GatewayResponse = response1
            ElseIf response1.Chars(1).ToString = "E" Or response1.Chars(1).ToString = "X" Then
                trans.GatewayResponse = "Transaction Declined"
                trans.AuthorizationCode = "E"
                Throw New Exception("Declined: Transaction Declined")
            End If

            If response1.Chars(43).ToString = "X" Then
                trans.GatewayResponse = "Exact; Match on Address and 9 Digit Zip Code"
                reason = "Exact; Match on Address and 9 Digit Zip Code"
                Throw New Exception("Declined: " & reason)
            ElseIf response1.Chars(43).ToString = "Y" Then
                trans.GatewayResponse = "Yes; Match on Address and 5 Digit Zip Code"
                reason = "Yes; Match on Address and 5 Digit Zip Code"
                Throw New Exception("Declined: " & reason)
            ElseIf response1.Chars(43).ToString = "A" Then
                trans.GatewayResponse = "Address matches, Zip does not"
                reason = "Address matches, Zip does not"
                Throw New Exception("Declined: " & reason)
            ElseIf response1.Chars(43).ToString = "W" Then
                trans.GatewayResponse = "9 Digit Zip matches,Address does not"
                reason = "9 Digit Zip matches,Address does not"
                Throw New Exception("Declined: " & reason)
            ElseIf response1.Chars(43).ToString = "Z" Then
                trans.GatewayResponse = "5 Digit Zip matches,address dores not"
                reason = "5 Digit Zip matches,address dores not"
                Throw New Exception("Declined: " & reason)
            ElseIf response1.Chars(43).ToString = "N" Then
                trans.GatewayResponse = "No; Neither Zip Nor Address matches"
                reason = "No; Neither Zip Nor Address matches"
                Throw New Exception("Declined: " & reason)
            ElseIf response1.Chars(43).ToString = "U" Then
                trans.GatewayResponse = "Unavailable"
                reason = "Unavailable"
                Throw New Exception("Declined: " & reason)
            ElseIf response1.Chars(43).ToString = "R" Then
                trans.GatewayResponse = "Retry"
                reason = "Retry"
                Throw New Exception("Declined: " & reason)
            End If

            sr.Close()

            Return trans
        End Function

        Public Overrides Function Refund(ByVal order As Commerce.Common.Order) As String
            Throw New Exception("This method not enabled for ICICI")
        End Function
    End Class
End Namespace
