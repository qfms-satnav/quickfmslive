#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.IO

Namespace Commerce.Providers
	Public Class AuthorizeNetPaymentProvider
		Inherits PaymentProvider

		'properties
		#Region "Passed-in Props"
		Private serviceUserName As String = ""
		Private servicePassword As String = ""
		Private transactionKey As String = ""
		Private serverURL As String = ""
		Private currencyCode As String = "USD"
		#End Region

		#Region "Provider specific behaviors"

		Public Overrides Sub Initialize(ByVal name As String, ByVal config As NameValueCollection)

			' Verify that config isn't null
			If config Is Nothing Then
				Throw New ArgumentNullException("config")
			End If

			' Assign the provider a default name if it doesn't have one
			If String.IsNullOrEmpty(name) Then
				config.Remove("name")
				config.Add("name", "AuthorizeNetPaymentProvider")
			End If
			' Add a default "description" attribute to config if the
			' attribute doesn't exist or is empty
			If String.IsNullOrEmpty(config("description")) Then
				config.Remove("description")
				config.Add("description", "Authorize.Net Payment Provider")
			End If
			MyBase.Initialize(name, config)

			serviceUserName = config("serviceUserName").ToString()
			If String.IsNullOrEmpty(serviceUserName) Then
				Throw New ProviderException("Empty Authorize.Net Username value")
			End If
			config.Remove("serviceUserName")

			servicePassword = config("servicePassword").ToString()
			If String.IsNullOrEmpty(servicePassword) Then
				Throw New ProviderException("Empty Authorize.Net Password value")
			End If
			config.Remove("servicePassword")

			transactionKey = config("transactionKey").ToString()
			If String.IsNullOrEmpty(transactionKey) Then
				Throw New ProviderException("Empty Transaction Key value")
			End If
			config.Remove("transactionKey")

			serverURL = config("serverURL").ToString()
			If String.IsNullOrEmpty(serverURL) Then
				Throw New ProviderException("Empty Server URL value")
			End If
			config.Remove("serverURL")

			currencyCode = config("currencyCode").ToString()
			If String.IsNullOrEmpty(currencyCode) Then
				Throw New ProviderException("Empty Currency Code value")
			End If
			config.Remove("currencyCode")


		End Sub

		Public Overrides ReadOnly Property Name() As String
			Get
				Return Nothing
			End Get
		End Property
		#End Region

		Public Overrides Function Charge(ByVal order As Commerce.Common.Order) As Commerce.Common.Transaction
			'string sOut = "";

			Dim dTotal As Decimal = order.OrderTotal
			Dim roundTo As Integer = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalDigits
			dTotal = Math.Round(dTotal, roundTo)

			'make sure that the ExpMonth has a leading 0
			Dim sExpMonth As String = order.CreditCardExpireMonth.ToString()
			If order.CreditCardExpireMonth < 10 Then
				sExpMonth = "0" & sExpMonth
			End If

			Dim useTestServer As Boolean = serverURL.IndexOf("test") >= 0


			Dim result As String = ""
				'the following is optional however it's good to have for records
			Dim strPost As String = "x_login=" & serviceUserName & "&x_tran_key=" & transactionKey & "&x_method=CC" & "&x_type=AUTH_CAPTURE" & "&x_amount=" & dTotal.ToString() & "&x_delim_data=TRUE" & "&x_delim_char=|" & "&x_relay_response=FALSE" & "&x_card_num=" & order.CreditCardNumber & "&x_exp_date=" & sExpMonth & order.CreditCardExpireYear.ToString() & "&x_version=3.1" & "&x_first_name=" & order.BillingAddress.FirstName & "&x_last_name=" & order.BillingAddress.LastName & "&x_address=" & order.BillingAddress.Address1 & "&x_city=" & order.BillingAddress.City & "&x_state=" & order.BillingAddress.StateOrRegion & "&x_zip=" & order.BillingAddress.Zip & "&x_currency_code=" & currencyCode & "&x_country=" & order.BillingAddress.Country & "&x_card_code=" & order.CreditCardSecurityNumber

			'you can set this up to send you an email if you like
			'by adding this: 
			'&x_merchant_email=me@email.com

			'you can also have them send you customer an email
			'&x_email=mycustomer@email.com&x_email_customer=TRUE


			If useTestServer Then
				strPost &= "&x_test_request=TRUE"
			End If

			Dim myWriter As StreamWriter = Nothing

			Dim objRequest As HttpWebRequest = CType(WebRequest.Create(serverURL), HttpWebRequest)
			objRequest.Method = "POST"
			objRequest.ContentLength = strPost.Length
			objRequest.ContentType = "application/x-www-form-urlencoded"

			Try
				myWriter = New StreamWriter(objRequest.GetRequestStream())
				myWriter.Write(strPost)
			Catch e As Exception
				Throw e
			Finally
				myWriter.Close()
			End Try
			Dim trans As Commerce.Common.Transaction = New Commerce.Common.Transaction()

			Dim objResponse As HttpWebResponse = CType(objRequest.GetResponse(), HttpWebResponse)
			Using sr As StreamReader = New StreamReader(objResponse.GetResponseStream())
				result = sr.ReadToEnd()

				'the result is a pipe-delimited set
				'parse this out so we know what happened
				Dim lines As String() = result.Split("|"c)

				'the response flag is the first item
				'1=success, 2=declined, 3=error
				Dim sFlag As String = lines(0)
				'string transactionID = "";
				'string authCode = "";
				Dim reason As String = ""

				If sFlag = "1" Then
					trans.GatewayResponse = lines(37)
					trans.AuthorizationCode = lines(4)

					'return the transactionID
					trans.GatewayResponse=result
				ElseIf sFlag = "2" Then
					trans.GatewayResponse = lines(3)
					Throw New Exception("Declined: " & reason)
				Else
					trans.GatewayResponse = lines(3)
					Throw New Exception("Error: " & result)

				End If


				' Close and clean up the StreamReader
				sr.Close()
			End Using

			Return trans
		End Function
		Public Overrides Function Refund(ByVal order As Commerce.Common.Order) As String
			Throw New Exception("This method not enabled for Authorize.NET")
		End Function
	End Class
End Namespace
