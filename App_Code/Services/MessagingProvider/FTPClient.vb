#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Net
Imports System.Threading

Imports System.IO
Namespace Commerce.Messaging
	Public Class FtpState
		Private wait As ManualResetEvent
		Private request_Renamed As FtpWebRequest
		Private fileName_Renamed As String
		Private operationException_Renamed As Exception = Nothing
		Private status As String

		Public Sub New()
			wait = New ManualResetEvent(False)
		End Sub

		Public ReadOnly Property OperationComplete() As ManualResetEvent
			Get
				Return wait
			End Get
		End Property

		Public Property Request() As FtpWebRequest
			Get
				Return request_Renamed
			End Get
			Set
				request_Renamed = Value
			End Set
		End Property

		Public Property FileName() As String
			Get
				Return fileName_Renamed
			End Get
			Set
				fileName_Renamed = Value
			End Set
		End Property
		Public Property OperationException() As Exception
			Get
				Return operationException_Renamed
			End Get
			Set
				operationException_Renamed = Value
			End Set
		End Property
		Public Property StatusDescription() As String
			Get
				Return status
			End Get
			Set
				status = Value
			End Set
		End Property
	End Class
	Public Class FTPClient

		Private Sub New()
		End Sub
		Public Shared Function PutFile(ByVal fileName As String, ByVal destinationServer As String, ByVal userName As String, ByVal password As String) As Boolean
			' Create a Uri instance with the specified URI string.
			' If the URI is not correctly formed, the Uri constructor
			' will throw an exception.
			Dim waitObject As ManualResetEvent
			Dim bOut As Boolean = False
			Dim target As Uri = New Uri(destinationServer)
			Dim state As FtpState = New FtpState()
			Dim request As FtpWebRequest = CType(WebRequest.Create(target), FtpWebRequest)
			request.Method = WebRequestMethods.Ftp.UploadFile

			' The request is anonymous by default; the credential does not have to be specified. 
			' The example specifies the credential only to
			' control how actions are logged on the server.

			request.Credentials = New NetworkCredential(userName, password)

			' Store the request in the object that we pass into the
			' asynchronous operations.
			state.Request = request
			state.FileName = fileName

			' Get the event to wait on.
			waitObject = state.OperationComplete

			' Asynchronously get the stream for the file contents.
			request.BeginGetRequestStream(New AsyncCallback(AddressOf EndGetStreamCallback), state)

			' Block the current thread until all operations are complete.
			waitObject.WaitOne()

			' The operations either completed or threw an exception.
			If Not state.OperationException Is Nothing Then
				Throw state.OperationException
			Else
			   'log this ("The operation completed - {0}", state.StatusDescription);
				bOut = True
			End If
			Return bOut
		End Function
		Private Shared Sub EndGetStreamCallback(ByVal ar As IAsyncResult)
			Dim state As FtpState = CType(ar.AsyncState, FtpState)

			Dim requestStream As Stream = Nothing
			' End the asynchronous call to get the request stream.
			Try
				requestStream = state.Request.EndGetRequestStream(ar)
				' Copy the file contents to the request stream.
				Const bufferLength As Integer = 2048
				Dim buffer As Byte() = New Byte(bufferLength - 1){}
				Dim count As Integer = 0
				Dim readBytes As Integer = 0
				Dim stream As FileStream = File.OpenRead(state.FileName)
				Do
					readBytes = stream.Read(buffer, 0, bufferLength)
					requestStream.Write(buffer, 0, readBytes)
					count += readBytes
				Loop While readBytes <> 0
				Console.WriteLine("Writing {0} bytes to the stream.", count)
				' IMPORTANT: Close the request stream before sending the request.
				requestStream.Close()
				' Asynchronously get the response to the upload request.
				state.Request.BeginGetResponse(New AsyncCallback(AddressOf EndGetResponseCallback), state)
				' Return exceptions to the main application thread.
			Catch e As Exception
				Console.WriteLine("Could not get the request stream.")
				state.OperationException = e
				state.OperationComplete.Set()
				Return
			End Try

		End Sub

		' The EndGetResponseCallback method  
		' completes a call to BeginGetResponse.
		Private Shared Sub EndGetResponseCallback(ByVal ar As IAsyncResult)
			Dim state As FtpState = CType(ar.AsyncState, FtpState)
			Dim response As FtpWebResponse = Nothing
			Try
				response = CType(state.Request.EndGetResponse(ar), FtpWebResponse)
				response.Close()
				state.StatusDescription = response.StatusDescription
				' Signal the main application thread that 
				' the operation is complete.
				state.OperationComplete.Set()
				' Return exceptions to the main application thread.
			Catch e As Exception
				Console.WriteLine("Error getting response.")
				state.OperationException = e
				state.OperationComplete.Set()
			End Try
		End Sub
	End Class
End Namespace