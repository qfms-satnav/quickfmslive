#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Configuration
Imports System.Xml
Imports System.Xml.Serialization
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Net.Mail
Imports SubSonic

Namespace Commerce.Messaging

  ''' <summary>
  ''' A Persistable class that uses Generics to store it's state
  ''' in the database. This class maps to the CSK_Messaging_Mailer table.
  ''' </summary>
  Public Class Email
	  Inherits ActiveRecord(Of Email)

	#Region ".ctors"
	''' <summary>
	''' Sets the static Table property from our Base class. This property tells
	''' the base class how to create the CRUD queries, etc.
	''' </summary>
	Private Sub SetSQLProps()

	  If Schema Is Nothing Then
		Schema = Query.BuildTableSchema("CSK_Messaging_Mailer")

	  End If

	End Sub

	Public Shared Function GetTableSchema() As TableSchema.Table
	  'instance an object to make sure
	  'the table schema has been created
	  Dim item As Email = New Email()
	  Return Email.Schema
	End Function

	Public Sub New()
	  SetSQLProps()
	  Me.MarkNew()
	End Sub
	Public Sub New(ByVal mailerID As Integer)
	  SetSQLProps()
	  MyBase.LoadByKey(mailerID)

	End Sub

	#End Region

	#Region "Public Props"
	<XmlAttribute("MailerID")> _
	Public Property MailerID() As Integer
	  Get
		Return Integer.Parse(Me.GetColumnValue("mailerID").ToString())
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("mailerID", Value)

	  End Set
	End Property
	<XmlAttribute("MailerName")> _
	Public Property MailerName() As String
	  Get
		Return Me.GetColumnValue("mailerName").ToString()
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("mailerName", Value)

	  End Set
	End Property
	<XmlAttribute("ToList")> _
	Public Property ToList() As String
	  Get
		Return Me.GetColumnValue("toList").ToString()
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("toList", Value)

	  End Set
	End Property
	<XmlAttribute("CcList")> _
	Public Property CcList() As String
	  Get
		Return Me.GetColumnValue("ccList").ToString()
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("ccList", Value)

	  End Set
	End Property
	<XmlAttribute("FromName")> _
	Public Property FromName() As String
	  Get
		Return Me.GetColumnValue("fromName").ToString()
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("fromName", Value)

	  End Set
	End Property
	<XmlAttribute("FromEmail")> _
	Public Property FromEmail() As String
	  Get
		Return Me.GetColumnValue("fromEmail").ToString()
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("fromEmail", Value)

	  End Set
	End Property
	<XmlAttribute("Subject")> _
	Public Property Subject() As String
	  Get
		Return Me.GetColumnValue("subject").ToString()
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("subject", Value)

	  End Set
	End Property
	<XmlAttribute("MessageBody")> _
	Public Property MessageBody() As String
	  Get
		Return Me.GetColumnValue("messageBody").ToString()
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("messageBody", Value)

	  End Set
	End Property
	<XmlAttribute("IsHTML")> _
	Public Property IsHTML() As Boolean
	  Get
		Return Boolean.Parse(Me.GetColumnValue("isHTML").ToString())
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("isHTML", Value)

	  End Set
	End Property

	<XmlAttribute("IsSystemMailer")> _
	Public Property IsSystemMailer() As Boolean
	  Get
		Return Boolean.Parse(Me.GetColumnValue("isSystemMailer").ToString())
	  End Get
	  Set
		Me.MarkDirty()
		Me.SetColumnValue("isSystemMailer", Value)

	  End Set
	End Property


	#End Region

	#Region "Mail Service Bits"

	Public Shared Function SendEmail(ByVal mailerID As Integer, ByVal appendText As String) As Boolean
	  Dim mailer As Email = New Email(mailerID)
	  mailer.MessageBody += appendText
	  Return SendEmail(mailer)
	End Function
	Public Shared Function SendEmail(ByVal sTo As String, ByVal mailerID As Integer) As Boolean
	  Dim mailer As Email = New Email(mailerID)

	  If sTo <> String.Empty Then
		mailer.ToList = sTo
	  End If

	  Return SendEmail(mailer)
	End Function
	Public Shared Function SendEmail(ByVal mailerID As Integer) As Boolean
	  Return SendEmail("", mailerID)
	End Function
	Public Shared Function SendEmail(ByVal mailer As Email) As Boolean
	  Dim client As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()
	  Using message As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
		Dim bOut As Boolean = False
		If (Not String.IsNullOrEmpty(mailer.ToList)) Then
		  Dim toList As String() = mailer.ToList.Split(";"c)
		  For Each [to] As String In toList
			message.To.Add(New MailAddress([to]))
		  Next [to]
		End If
		If (Not String.IsNullOrEmpty(mailer.CcList)) Then
		  Dim ccList As String() = mailer.CcList.Split(";"c)
		  For Each cc As String In ccList
			message.CC.Add(New MailAddress(cc))
		  Next cc
		End If

		message.Subject = mailer.Subject
		message.IsBodyHtml = mailer.IsHTML
		message.Body = mailer.MessageBody
		Try
		  client.Send(message)
		  'TODO: CMC - log this
		  bOut = True
		Catch x As System.Net.Mail.SmtpException
		  Throw x
		  'TODO: CMC - Log Exception, Do we really want to throw this back up the stack?
		  'throw new Exception("Email not sent: " + x.Message + "; message details: " + message.ToString());
		  'log this
		End Try
		Return bOut
	  End Using
	End Function

	#End Region

  End Class
End Namespace
