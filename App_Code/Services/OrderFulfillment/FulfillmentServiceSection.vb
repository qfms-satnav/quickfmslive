#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Configuration


Namespace Commerce.Providers
	Public Class FulfillmentServiceSection
		Inherits ConfigurationSection
		<ConfigurationProperty("providers")> _
		Public ReadOnly Property Providers() As ProviderSettingsCollection
			Get
				Return CType(MyBase.Item("providers"), ProviderSettingsCollection)
			End Get
		End Property

		<StringValidator(MinLength := 1), ConfigurationProperty("defaultProvider", DefaultValue := "SqlShippingProvider")> _
		Public Property DefaultProvider() As String
			Get
				Return CStr(MyBase.Item("defaultProvider"))
			End Get
			Set
				MyBase.Item("defaultProvider") = Value
			End Set
		End Property
		<ConfigurationProperty("useShipping", DefaultValue := True)> _
		Public Property UseShipping() As Boolean
			Get
				Return CBool(MyBase.Item("useShipping"))
			End Get
			Set
				MyBase.Item("useShipping") = Value
			End Set
		End Property
		<ConfigurationProperty("dimensionUnit", DefaultValue := "inches")> _
		Public Property DimensionUnit() As String
			Get
				Return CStr(MyBase.Item("dimensionUnit"))
			End Get
			Set
				MyBase.Item("dimensionUnit") = Value
			End Set
		End Property
		<ConfigurationProperty("shipFromZip", DefaultValue := "00000")> _
		Public Property ShipFromZip() As String
			Get
				Return CStr(MyBase.Item("shipFromZip"))
			End Get
			Set
				MyBase.Item("shipFromZip") = Value
			End Set
		End Property
		<ConfigurationProperty("shipFromCountryCode", DefaultValue := "US")> _
		Public Property ShipFromCountryCode() As String
			Get
				Return CStr(MyBase.Item("shipFromCountryCode"))
			End Get
			Set
				MyBase.Item("shipFromCountryCode") = Value
			End Set
		End Property
		<ConfigurationProperty("shipPackagingBuffer", DefaultValue := "0")> _
		Public Property ShipPackagingBuffer() As Decimal
			Get
				Return CDec(MyBase.Item("shipPackagingBuffer"))
			End Get
			Set
				MyBase.Item("shipPackagingBuffer") = Value
			End Set
		End Property



	End Class
End Namespace