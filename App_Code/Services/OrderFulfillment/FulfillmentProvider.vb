#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Configuration.Provider
Imports System.Configuration

Namespace Commerce.Providers

	Public Class FulfillmentProviderCollection
		Inherits ProviderCollection

	End Class
	Public MustInherit Class FulfillmentProvider
		Inherits ProviderBase
		Public MustOverride Function GetDeliveryOptions(ByVal package As PackageInfo) As DeliveryOptionCollection
		Public MustOverride Function GetDeliveryOptions(ByVal package As PackageInfo, ByVal restrictions As DeliveryRestrictions) As DeliveryOptionCollection

	End Class
End Namespace
