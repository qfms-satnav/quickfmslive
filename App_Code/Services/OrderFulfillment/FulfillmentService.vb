#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Configuration.Provider
Imports System.Configuration
Imports System.Web.Configuration

Namespace Commerce.Providers
	Public Enum DeliveryRestrictions
		Ground
		Air
		Freight
		Download
		None

	End Enum

	Public Class FulfillmentService

		#Region "Provider-specific bits"
		Private Shared _provider As FulfillmentProvider = Nothing
		Private Shared _providers As FulfillmentProviderCollection = Nothing
		Private Shared _lock As Object = New Object()

		Public ReadOnly Property Provider() As FulfillmentProvider
			Get
				Return _provider
			End Get
		End Property

		Public ReadOnly Property Providers() As FulfillmentProviderCollection
			Get
				Return _providers
			End Get
		End Property
		Public Shared ReadOnly Property Instance() As FulfillmentProvider
			Get
				LoadProviders()
				Return _provider
			End Get
		End Property

		Private Shared ThisConfig As Commerce.Providers.FulfillmentServiceSection
		Private Shared Sub LoadProviders()
			' Avoid claiming lock if providers are already loaded
			If _provider Is Nothing Then
				SyncLock _lock
					' Do this again to make sure _provider is still null
					If _provider Is Nothing Then
						' Get a reference to the <imageService> section
						ThisConfig = CType(WebConfigurationManager.GetSection ("FulfillmentService"), Commerce.Providers.FulfillmentServiceSection)

						' Load registered providers and point _provider
						' to the default provider
						_providers = New FulfillmentProviderCollection()
						ProvidersHelper.InstantiateProviders(ThisConfig.Providers, _providers, GetType(FulfillmentProvider))
						_provider = CType(_providers(ThisConfig.DefaultProvider), FulfillmentProvider)

						If _provider Is Nothing Then
							Throw New ProviderException ("Unable to load default FulfillmentProvider")
						End If
					End If
				End SyncLock
			End If
		End Sub
		#End Region





		Public Shared Function GetOptions(ByVal package As PackageInfo) As DeliveryOptionCollection
			LoadProviders()
			Dim options As DeliveryOptionCollection = New DeliveryOptionCollection()

			'if there are no restrictions, hit every provider and return the collection
			For Each provider As FulfillmentProvider In _providers
				options.Combine(provider.GetDeliveryOptions(package))
			Next provider
			Return options
		End Function

		Public Shared Function GetOptions(ByVal package As PackageInfo, ByVal restrictions As DeliveryRestrictions) As DeliveryOptionCollection
			LoadProviders()
			Dim options As DeliveryOptionCollection = New DeliveryOptionCollection()

			'if there are no restrictions, hit every provider and return the collection
			For Each provider As FulfillmentProvider In _providers
				options.Combine(provider.GetDeliveryOptions(package,restrictions))
			Next provider
			Return options
		End Function

	End Class
End Namespace
