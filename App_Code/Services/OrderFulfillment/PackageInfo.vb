#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text

Namespace Commerce.Providers
	Public Class PackageInfo
		Private _fromZip As String

		Public Property FromZip() As String
			Get
				Return _fromZip
			End Get
			Set
				_fromZip = Value
			End Set
		End Property

		Private _fromCountryCode As String

		Public Property FromCountryCode() As String
			Get
				Return _fromCountryCode
			End Get
			Set
				_fromCountryCode = Value
			End Set
		End Property

		Private _toZip As String

		Public Property ToZip() As String
			Get
				Return _toZip
			End Get
			Set
				_toZip = Value
			End Set
		End Property

		Private _toCountryCode As String

		Public Property ToCountryCode() As String
			Get
				Return _toCountryCode
			End Get
			Set
				_toCountryCode = Value
			End Set
		End Property

		Private _weight As Decimal

		Public Property Weight() As Decimal
			Get
				Return _weight
			End Get
			Set
				_weight = Value + (Value * _packagingBuffer)
			End Set
		End Property

		Private _width As Integer

		Public Property Width() As Integer
			Get
				Return _width
			End Get
			Set
				_width = Value + CInt(Fix(Value * _packagingBuffer))
			End Set
		End Property

		Private _height As Integer

		Public Property Height() As Integer
			Get
				Return _height
			End Get
			Set
				_height = Value + CInt(Fix(Value * _packagingBuffer))
			End Set
		End Property

		Private _length As Integer

		Public Property Length() As Integer
			Get
				Return _length
			End Get
			Set
				_length = Value + CInt(Fix(Value * _packagingBuffer))
			End Set
		End Property

		Private _dimensionUnit As String

		Public Property DimensionUnit() As String
			Get
				Return _dimensionUnit
			End Get
			Set
				_dimensionUnit = Value
			End Set
		End Property

		Private _packagingBuffer As Decimal

		Public Property PackagingBuffer() As Decimal
			Get
				Return _packagingBuffer
			End Get
			Set
				_packagingBuffer = Value
			End Set
		End Property

		Private _args As Dictionary(Of String, String)

		Public Property Args() As Dictionary(Of String, String)
			Get
				Return _args
			End Get
			Set
				_args = Value
			End Set
		End Property

	End Class
End Namespace
