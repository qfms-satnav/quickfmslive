Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports System.Web
Imports System.Web.UI.WebControls

Public Class clsReports

    Public Sub bindTower(ByVal ddl As DropDownList)
        strSQL = "usp_getAllTowers"
        BindCombo(strSQL, ddl, "twr_name", "twr_code")
    End Sub
    Public Sub binddata(ByVal gv As GridView, ByVal gvExcel As GridView, ByVal checkID As Integer, ByVal vc_TowerName As String, ByVal vc_StatusID As Integer, ByVal fd As Date, ByVal td As Date)

        Dim sp1 As New SqlParameter("@checkID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp1.Value = checkID
        Dim sp2 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = vc_TowerName
        Dim sp3 As New SqlParameter("@vc_StatusID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp3.Value = vc_StatusID
        Dim sp4 As New SqlParameter("@fd", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp4.Value = fd
        Dim sp5 As New SqlParameter("@td", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp5.Value = td
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_TowerWise", sp1, sp2, sp3, sp4, sp5)
        gv.DataSource = dt
        gv.DataBind()
        gvExcel.DataSource = dt
        gvExcel.DataBind()

    End Sub
    Public Sub bindfloor(ByVal ddl As DropDownList, ByVal strTower As String)

        Dim sp1 As New SqlParameter("@vc_code", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = strTower
        strSQL = "usp_getFloorsforTower"
        BindCombo(strSQL, ddl, "flr_name", "flr_code", sp1)
    End Sub
    Public Sub binddata_floorwise(ByVal gv As GridView, ByVal gvExcel As GridView, ByVal checkID As Integer, ByVal vc_TowerName As String, ByVal vc_FloorName As String, ByVal vc_StatusID As Integer, ByVal fd As Date, ByVal td As Date)

        Dim sp1 As New SqlParameter("@checkID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp1.Value = checkID
        Dim sp2 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = vc_TowerName
        Dim sp3 As New SqlParameter("@vc_FloorName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = vc_FloorName
        Dim sp4 As New SqlParameter("@vc_StatusID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp4.Value = vc_StatusID
        Dim sp5 As New SqlParameter("@fd", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp5.Value = fd
        Dim sp6 As New SqlParameter("@td", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp6.Value = td
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_TowerWise_FloorWise", sp1, sp2, sp3, sp4, sp5, sp6)
        gv.DataSource = dt
        gv.DataBind()
        gvExcel.DataSource = dt
        gvExcel.DataBind()

    End Sub
    Public Sub bindwing(ByVal ddl As DropDownList, ByVal strFlr As String)
        strSQL = "select distinct wng_code,(wng_code + '/' + wng_name )wng_name from " & HttpContext.Current.Session("TENANT") & "." & "wing where wng_flr_id='" & strFlr & "' and wng_sta_id=1 order by wng_name"
        BindCombo(strSQL, ddl, "wng_name", "wng_code")
    End Sub


    Public Sub bindwing(ByVal ddl As DropDownList, ByVal strFlr As String, ByVal STRLOCATION As String, ByVal strTOWER As String)
        Dim sp1 As New SqlParameter("@LCN_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = STRLOCATION
        Dim sp2 As New SqlParameter("@TWR_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = strTOWER
        Dim sp3 As New SqlParameter("@FLR_ID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = strFlr

        ddl.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_WINGBYFLOOR", sp1, sp2, sp3)
        ddl.DataTextField = "wng_name"
        ddl.DataValueField = "wng_code"
        ddl.DataBind()
        ddl.Items.Insert(0, "--Select--")
    End Sub
    Public Sub binddata_wingwise(ByVal gv As GridView, ByVal gvExcel As GridView, ByVal checkID As Integer, ByVal vc_TowerName As String, ByVal vc_FloorName As String, ByVal vc_WingName As String, ByVal vc_StatusID As Integer, ByVal fd As Date, ByVal td As Date)

        Dim sp1 As New SqlParameter("@checkID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp1.Value = checkID
        Dim sp2 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = vc_TowerName
        Dim sp3 As New SqlParameter("@vc_FloorName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = vc_FloorName
        Dim sp4 As New SqlParameter("@vc_WingName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp4.Value = vc_WingName
        Dim sp5 As New SqlParameter("@vc_StatusID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp5.Value = vc_StatusID
        Dim sp6 As New SqlParameter("@fd", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp6.Value = fd
        Dim sp7 As New SqlParameter("@td", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp7.Value = td
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_TowerWise_FloorWise_WingWise", sp1, sp2, sp3, sp4, sp5, sp6, sp7)
        gv.DataSource = dt
        gv.DataBind()
        gvExcel.DataSource = dt
        gvExcel.DataBind()

    End Sub

    '****************** Location wise Occupancy Report ***********************************
    Public Sub bindLocation(ByVal ddl As DropDownList)
        strSQL = "select distinct lcm_code,lcm_name from " & HttpContext.Current.Session("TENANT") & "." & "location where lcm_sta_id=1"
        BindCombo(strSQL, ddl, "lcm_name", "lcm_code")
    End Sub
    Public Sub binddata_Locationwise(ByVal gv As GridView, ByVal checkID As Integer, ByVal vc_LocationName As String, ByVal vc_StatusID As Integer, ByVal fd As Date, ByVal td As Date)

        Dim sp1 As New SqlParameter("@checkID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp1.Value = checkID
        Dim sp2 As New SqlParameter("@vc_LocationName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = vc_LocationName
        Dim sp3 As New SqlParameter("@vc_StatusID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp3.Value = vc_StatusID
        Dim sp4 As New SqlParameter("@fd", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp4.Value = fd
        Dim sp5 As New SqlParameter("@td", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp5.Value = td
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_LocationWise", sp1, sp2, sp3, sp4, sp5)
        gv.DataSource = dt
        gv.DataBind()

    End Sub
    '********************* Vertical wise Occupancy Report **************************
    Public Sub bindVertical(ByVal ddl As DropDownList)
        strSQL = "SELECT VER_CODE AS  COST_CENTER_CODE, VER_NAME AS COST_CENTER_NAME FROM  " & HttpContext.Current.Session("TENANT") & "." & "VERTICAL WHERE VER_STA_ID=1 ORDER BY VER_NAME"
        BindCombo(strSQL, ddl, "COST_CENTER_NAME", "COST_CENTER_CODE")
    End Sub


    Public Sub binddata_verticalwise(ByVal gv As GridView, ByVal gvexcel As GridView, ByVal checkID As Integer, ByVal vc_VerticalName As String, ByVal vc_StatusID As Integer, ByVal fd As Date, ByVal td As Date)

        Dim sp1 As New SqlParameter("@checkID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp1.Value = checkID
        Dim sp2 As New SqlParameter("@vc_VerticalName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = vc_VerticalName
        Dim sp3 As New SqlParameter("@vc_StatusID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp3.Value = vc_StatusID
        Dim sp4 As New SqlParameter("@fd", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp4.Value = fd
        Dim sp5 As New SqlParameter("@td", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp5.Value = td
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Occupancy_VerticalWise", sp1, sp2, sp3, sp4, sp5)
        gv.DataSource = dt
        gv.DataBind()
        gvexcel.DataSource = dt
        gvexcel.DataBind()

    End Sub
    '****************** Requisition Tower wise Report ****************
    Public Sub binddata_requistions(ByVal gv As GridView, ByVal gvExport As GridView, ByVal checkID As Integer, ByVal vc_TowerName As String, ByVal fd As Date, ByVal td As Date)

        Dim sp1 As New SqlParameter("@checkID", SqlDbType.Int, 4, ParameterDirection.Input)
        sp1.Value = checkID
        Dim sp2 As New SqlParameter("@vc_TowerName", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp2.Value = vc_TowerName
        Dim sp3 As New SqlParameter("@fd", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp3.Value = fd
        Dim sp4 As New SqlParameter("@td", SqlDbType.DateTime, 8, ParameterDirection.Input)
        sp4.Value = td
        Dim dt As DataTable = SqlHelper.ExecuteDatatable(CommandType.StoredProcedure, "Requests_TowerWise", sp1, sp2, sp3, sp4)
        gv.DataSource = dt
        gv.DataBind()
        gvExport.DataSource = dt
        gvExport.DataBind()

    End Sub
    '******************** Requisition Location wise Report *************
    Public Sub bindTower_Locationwise(ByVal ddl As DropDownList, ByVal loc As String)
        strSQL = "select distinct twr_code,twr_name from " & HttpContext.Current.Session("TENANT") & "." & "tower where twr_loc_id='" & loc & "' and twr_sta_id=1"
        BindCombo(strSQL, ddl, "twr_name", "twr_code")
    End Sub
End Class
