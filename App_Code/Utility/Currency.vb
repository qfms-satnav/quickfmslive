#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Globalization
''' <summary>
''' Summary description for Currency
''' </summary>
Public Partial Class Currency

  Private currencyDecimals_Renamed As Integer
  Private cultureInfo As CultureInfo = Nothing

  Public Sub New()
	cultureInfo = CultureInfo.CurrentCulture
	currencyDecimals_Renamed = cultureInfo.NumberFormat.CurrencyDecimalDigits
  End Sub

  Public Sub New(ByVal name As String)
	'TODO: CMC - Fix this up.
	'Validator.ValidateStringArgumentIsNotNullOrEmptyString(name, "name");
	cultureInfo = New CultureInfo(name)
	currencyDecimals_Renamed = cultureInfo.NumberFormat.CurrencyDecimalDigits
  End Sub

  Public Sub New(ByVal culture As CultureInfo)
	'Validator.ValidateObjectType(culture, typeof(CultureInfo));
	cultureInfo = culture
	currencyDecimals_Renamed = culture.NumberFormat.CurrencyDecimalDigits
  End Sub

  Public ReadOnly Property CurrencyDecimals() As Integer
	Get
	  Return currencyDecimals_Renamed
	End Get
  End Property
End Class
