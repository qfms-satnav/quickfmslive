Imports Microsoft.VisualBasic

Public Class AssetGroupMaster
    Private mAAG_ID As Integer
    Private mAAG_CODE As String
    Private mAAG_NAME As String
    Private mAAG_STA_ID As String
    Private mAAG_UPT_BY As String
    Private mAAG_UPT_DT As String
    Private mAAG_REM As String
    Private mAAG_EXH_CODE As String
    Private mAAG_MFTYPE As String
    Private mAAG_STYPE As String
    Private mAsset_Group_Active_Inactive_link As String

    Public Property AAG_ID() As Integer
        Get
            Return mAAG_ID
        End Get
        Set(ByVal value As Integer)
            mAAG_ID = value
        End Set
    End Property


    Public Property AAG_CODE() As String
        Get
            Return mAAG_CODE
        End Get
        Set(ByVal value As String)
            mAAG_CODE = value
        End Set
    End Property

    Public Property AAG_NAME() As String
        Get
            Return mAAG_NAME
        End Get
        Set(ByVal value As String)
            mAAG_NAME = value
        End Set
    End Property

    Public Property AAG_STA_ID() As String
        Get
            Return mAAG_STA_ID
        End Get
        Set(ByVal value As String)
            mAAG_STA_ID = value
        End Set
    End Property

    Public Property AAG_UPT_BY() As String
        Get
            Return mAAG_UPT_BY
        End Get
        Set(ByVal value As String)
            mAAG_UPT_BY = value
        End Set
    End Property

    Public Property AAG_UPT_DT() As String
        Get
            Return mAAG_UPT_DT
        End Get
        Set(ByVal value As String)
            mAAG_UPT_DT = value
        End Set
    End Property

    Public Property AAG_REM() As String
        Get
            Return mAAG_REM
        End Get
        Set(ByVal value As String)
            mAAG_REM = value
        End Set
    End Property

    Public Property AAG_EXH_CODE() As String
        Get
            Return mAAG_EXH_CODE
        End Get
        Set(ByVal value As String)
            mAAG_EXH_CODE = value
        End Set
    End Property

    Public Property AAG_MFTYPE() As String
        Get
            Return mAAG_MFTYPE
        End Get
        Set(ByVal value As String)
            mAAG_MFTYPE = value
        End Set
    End Property

    Public Property AAG_STYPE() As String
        Get
            Return mAAG_STYPE
        End Get
        Set(ByVal value As String)
            mAAG_STYPE = value
        End Set
    End Property

    Public Property Asset_Group_Active_Inactive_link() As String
        Get
            Return mAsset_Group_Active_Inactive_link
        End Get
        Set(ByVal value As String)
            mAsset_Group_Active_Inactive_link = value
        End Set
    End Property


End Class
