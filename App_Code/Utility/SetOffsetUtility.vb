﻿Imports Microsoft.VisualBasic
Imports System.Collections.ObjectModel
Imports System
Imports System.Web

Public Module SetDatetimeOffset

    Public Function getoffsetdatetime([date] As DateTime) As DateTime
        Dim O As [Object] = HttpContext.Current.Session("useroffset")
        If O IsNot Nothing Then
            Dim Temp As String = O.ToString().Trim()
            If Not Temp.Contains("+") AndAlso Not Temp.Contains("-") Then
                Temp = Temp.Insert(0, "+")
            End If
            Dim timeZones As ReadOnlyCollection(Of TimeZoneInfo) = TimeZoneInfo.GetSystemTimeZones()
            Dim startTime As DateTime = DateTime.Parse(DateTime.UtcNow.ToString())
            Dim _now As DateTime = DateTime.Parse(DateTime.UtcNow.ToString())
            For Each timeZoneInfo__1 As TimeZoneInfo In timeZones
                If timeZoneInfo__1.ToString().Contains(Temp) Then
                    'Response.Write(timeZoneInfo.ToString());
                    'string _timeZoneId = "Pacific Standard Time";
                    Dim tst As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id)
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst)
                    Exit For
                End If
            Next
            Return _now
        Else
            Return DateTime.Now
        End If
    End Function
    Public Function getoffsetdate([date] As Date) As Date
        Dim O As [Object] = HttpContext.Current.Session("useroffset")
        If O IsNot Nothing Then
            Dim Temp As String = O.ToString().Trim()
            If Not Temp.Contains("+") AndAlso Not Temp.Contains("-") Then
                Temp = Temp.Insert(0, "+")
            End If
            Dim timeZones As ReadOnlyCollection(Of TimeZoneInfo) = TimeZoneInfo.GetSystemTimeZones()
            Dim startTime As DateTime = DateTime.Parse(DateTime.UtcNow.ToString())
            Dim _now As Date = DateTime.Parse(DateTime.UtcNow.ToString())
            For Each timeZoneInfo__1 As TimeZoneInfo In timeZones
                If timeZoneInfo__1.ToString().Contains(Temp) Then
                    'Response.Write(timeZoneInfo.ToString());
                    'string _timeZoneId = "Pacific Standard Time";
                    Dim tst As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id)
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst)
                    Exit For
                End If
            Next
            Return _now.Date
        Else
            Return DateTime.Today

        End If
    End Function
End Module
'public static DateTime Now (this DateTime date)
'{
'    Object O = HttpContext.Current.Session["useroffset"];
'    if (O != null)
'    {
'        string Temp = O.ToString().Trim();
'        if (!Temp.Contains("+") && !Temp.Contains("-"))
'        {
'            Temp = Temp.Insert(0, "+");
'        }
'        ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
'        DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
'        DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
'        foreach (TimeZoneInfo timeZoneInfo in timeZones)
'        {
'            if (timeZoneInfo.ToString().Contains(Temp))
'            {
'                //Response.Write(timeZoneInfo.ToString());
'                //string _timeZoneId = "Pacific Standard Time";
'                TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo.Id);
'                _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
'                break;
'            }
'        }
'        return _now;
'    }
'    else
'        return DateTime.Parse(getoffsetdatetime(DateTime.Now).ToString());

