Imports Microsoft.VisualBasic

Public Class clsSearchAssetStatus
    Private mAUR_FIRST_NAME As String

    Public Property AUR_FIRST_NAME() As String
        Get
            Return mAUR_FIRST_NAME
        End Get
        Set(ByVal value As String)
            mAUR_FIRST_NAME = value
        End Set
    End Property
    Private mAUR_EMAIL As String

    Public Property AUR_EMAIL() As String
        Get
            Return mAUR_EMAIL
        End Get
        Set(ByVal value As String)
            mAUR_EMAIL = value
        End Set
    End Property
    Private mRMNAME As String

    Public Property RMNAME() As String
        Get
            Return mRMNAME
        End Get
        Set(ByVal value As String)
            mRMNAME = value
        End Set
    End Property
    Private mRMEMAIL As String

    Public Property RMEMAIL() As String
        Get
            Return mRMEMAIL
        End Get
        Set(ByVal value As String)
            mRMEMAIL = value
        End Set
    End Property
    Private mLCM_NAME As String

    Public Property LCM_NAME() As String
        Get
            Return mLCM_NAME
        End Get
        Set(ByVal value As String)
            mLCM_NAME = value
        End Set
    End Property
    Private mTWR_NAME As String

    Public Property TWR_NAME() As String
        Get
            Return mTWR_NAME
        End Get
        Set(ByVal value As String)
            mTWR_NAME = value
        End Set
    End Property
    Private mFLR_NAME As String

    Public Property FLR_NAME() As String
        Get
            Return mFLR_NAME
        End Get
        Set(ByVal value As String)
            mFLR_NAME = value
        End Set
    End Property
    Private mAUR_DEP_ID As String

    Public Property AUR_DEP_ID() As String
        Get
            Return mAUR_DEP_ID
        End Get
        Set(ByVal value As String)
            mAUR_DEP_ID = value
        End Set
    End Property
    Private mAUR_LAST_NAME As String

    Public Property AUR_LAST_NAME() As String
        Get
            Return mAUR_LAST_NAME
        End Get
        Set(ByVal value As String)
            mAUR_LAST_NAME = value
        End Set
    End Property
    Private mPRODUCTNAME As String

    Public Property PRODUCTNAME() As String
        Get
            Return mPRODUCTNAME
        End Get
        Set(ByVal value As String)
            mPRODUCTNAME = value
        End Set
    End Property
    Private mAAS_AAT_CODE As String

    Public Property AAS_AAT_CODE() As String
        Get
            Return mAAS_AAT_CODE
        End Get
        Set(ByVal value As String)
            mAAS_AAT_CODE = value
        End Set
    End Property
End Class
