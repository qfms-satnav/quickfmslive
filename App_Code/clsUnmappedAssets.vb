Imports Microsoft.VisualBasic

Public Class clsUnmappedAssets
    Private mAAS_AAT_CODE As String

    Public Property AAS_AAT_CODE() As String
        Get
            Return mAAS_AAT_CODE
        End Get
        Set(ByVal value As String)
            mAAS_AAT_CODE = value
        End Set
    End Property
    Private mPRODUCTNAME As String

    Public Property PRODUCTNAME() As String
        Get
            Return mPRODUCTNAME
        End Get
        Set(ByVal value As String)
            mPRODUCTNAME = value
        End Set
    End Property
End Class
