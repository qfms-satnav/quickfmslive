Imports System
Imports System.Text
Imports System.Data.SqlClient
Imports System.Data
Imports SqlHelper
Imports System.Configuration.ConfigurationManager
Imports System.Web
Imports System.Web.UI.WebControls


Public Class clsQueries
    Public Shared Function Insert_Query(ByVal fields As String, ByVal values As String, ByVal table As String) As Boolean
        Try
            strSQL = "INSERT INTO " & HttpContext.Current.Session("TENANT") & "." & table & " (" & fields & ") VALUES (" & values & ")"
            If (SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL) > 0) Then
                Return True
            Else : Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function
    Public Sub getcountry(ByVal ddl As DropDownList)
        strSQL = "SELECT distinct cny_code,cny_name from " & HttpContext.Current.Session("TENANT") & "." & "country"
        ddl.Items.Clear()
        BindCombo(strSQL, ddl, "CNY_NAME", "CNY_CODE")
    End Sub

    Public Shared Function Update_Query(ByVal table As String, ByVal fields As String, ByVal condition As String) As Boolean
        Try
            strSQL = "UPDATE " & HttpContext.Current.Session("TENANT") & "." & table & " SET " & fields & " WHERE " & condition
            If (SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL) > 0) Then
                Return True
            Else : Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function DeleteQuery(ByVal Conditions As String, ByVal table As String) As Boolean
        Try
            strSQL = "delete from " & HttpContext.Current.Session("TENANT") & "." & table & " where " & Conditions
            If (SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL) > 0) Then
                Return True
            Else : Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function CheckDuplicate(ByVal table As String, ByVal Field As String, ByVal Condition As String) As Integer
        Dim cnt As Integer = 0
        Try
            strSQL = "SELECT COUNT(" & Field & ") FROM " & HttpContext.Current.Session("TENANT") & "." & table & " WHERE " & Condition
            cnt = SqlHelper.ExecuteScalar(CommandType.Text, strSQL)
            Return cnt
        Catch ex As Exception
            Return 9999
        End Try
    End Function

    Public Shared Function SelectData(ByVal table As String, ByVal Field As String, ByVal Condition As String) As DataSet
        Dim ds As New DataSet
        Try
            strSQL = "SELECT DISTINCT " & Field & " FROM " & HttpContext.Current.Session("TENANT") & "." & table & " WHERE " & Condition
            ds = SqlHelper.ExecuteDataset(CommandType.Text, strSQL)
            Return ds
        Catch ex As Exception
            ds = Nothing
            Return ds
        End Try
    End Function
End Class