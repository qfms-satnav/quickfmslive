Imports System
Imports System.Collections.Generic
Imports System.Text

Namespace Amantra.Exception
    Public Class BaseException
        Inherits System.Exception

        '''** Variable to hold the error message initialized to empty string */ 
        Private m_ErrorMessage As String = String.Empty

        '* Variable to hold another exception code 

        Private m_ObjException As System.Exception

        Private m_UserId As String

        '* Variable to hold the name of the method from which the Exception originated 

        Private m_MethodName As String

        '* Variable to hold the name of the program from which the Exception originated 

        Private m_ClassName As String
        '* Variable to hold the Formname of the program from which the Exception originated 

        Private m_FormName As String



        Private m_PrintStack As StringBuilder = New StringBuilder


        Public Sub New()
        End Sub

        Public Sub New(ByVal message As String, ByVal className As String, ByVal methodName As String, ByVal objException As System.Exception)
            MyBase.New(message)
            m_ErrorMessage = message
            m_ClassName = className
            m_MethodName = methodName
            m_ObjException = objException

            If objException IsNot Nothing Then
                m_PrintStack.Append(objException.StackTrace.ToString())
            Else
                m_PrintStack.Append("")
            End If

            Me.logException(objException.StackTrace.ToString())
        End Sub

        Public Sub New(ByVal message As String, ByVal className As String, ByVal formName As String, ByVal methodName As String, ByVal objException As System.Exception)
            MyBase.New(message)
            m_ErrorMessage = message
            m_ClassName = className
            m_FormName = formName
            m_MethodName = methodName
            m_ObjException = objException

            If objException IsNot Nothing Then
                m_PrintStack.Append(objException.StackTrace.ToString())
            Else
                m_PrintStack.Append("")
            End If

            Me.logException(objException.StackTrace.ToString())
        End Sub


        Public Sub New(ByVal message As String, ByVal className As String, ByVal formName As String, ByVal methodName As String, ByVal UserId As String)
            MyBase.New(message)
            m_ErrorMessage = message
            m_ClassName = className
            m_FormName = formName
            m_MethodName = methodName
            m_UserId = UserId

            'if (UserId != null) 
            ' m_PrintStack.Append(UserId); 
            'else 
            ' m_PrintStack.Append(""); 

            Me.logException(UserId)
        End Sub
        Private Sub logException(ByVal msg As String)
            '''NEED TO IMPLIMENT LOG4NET 

        End Sub

        Public Property ErrorMessage() As String
            Get
                Return m_ErrorMessage
            End Get
            Set(ByVal value As String)
                m_ErrorMessage = value
            End Set
        End Property

        Public Property ClassName() As String
            Get
                Return m_ClassName
            End Get
            Set(ByVal value As String)
                m_ClassName = value
            End Set
        End Property

        Public Property MethodName() As String
            Get
                Return m_MethodName
            End Get
            Set(ByVal value As String)
                m_MethodName = value
            End Set
        End Property

        Public Property FormName() As String
            Get
                Return m_FormName
            End Get
            Set(ByVal value As String)
                m_FormName = value
            End Set
        End Property

        Public Property objException() As System.Exception
            Get
                Return m_ObjException
            End Get
            Set(ByVal value As System.Exception)
                m_ObjException = value
            End Set
        End Property

        Public ReadOnly Property TimeStamp() As DateTime
            Get
                Return getoffsetdatetime(DateTime.Now)
            End Get
        End Property

        Public Property PrintStack() As StringBuilder
            Get
                Return m_PrintStack
            End Get
            Set(ByVal value As StringBuilder)
                m_PrintStack = value
            End Set
        End Property
    End Class

End Namespace