Imports Microsoft.VisualBasic

Public Structure JQGridResults
    Public page As Integer
    Public total As Integer
    Public records As Integer
    Public total_rows As Integer
    Public rows As JQGridRow()

End Structure
Public Structure JQGridRow
    Public id As String

    Public cell As String()
End Structure



Public Structure JQGridResultsDrop

    Public total_rows As String

    Public rows As JQGridRowDrop()

End Structure
Public Structure JQGridRowDrop
    Public row As String()
End Structure
Public Class clsJson



    Public Property strspclayer() As String
        Get
            Return m_spclayer
        End Get
        Set(ByVal value As String)
            m_spclayer = value
        End Set
    End Property
    Private m_spclayer As String

    Public Property strspctype() As String
        Get
            Return m_spctype
        End Get
        Set(ByVal value As String)
            m_spctype = value
        End Set
    End Property
    Private m_spctype As String

    Public Property strspccarved() As String
        Get
            Return m_spccarved
        End Get
        Set(ByVal value As String)
            m_spccarved = value
        End Set
    End Property
    Private m_spccarved As String

    Public Property strspcportstatus() As String
        Get
            Return m_spcportstatus
        End Get
        Set(ByVal value As String)
            m_spcportstatus = value
        End Set
    End Property
    Private m_spcportstatus As String

    Public Property strspcporttype() As String
        Get
            Return m_spcporttype
        End Get
        Set(ByVal value As String)
            m_spcporttype = value
        End Set
    End Property
    Private m_spcporttype As String

    Public Property strbcpseattype() As String
        Get
            Return m_bcpseattype
        End Get
        Set(ByVal value As String)
            m_bcpseattype = value
        End Set
    End Property
    Private m_bcpseattype As String

    Public Property strmcpseat() As String
        Get
            Return m_mcpseat
        End Get
        Set(ByVal value As String)
            m_mcpseat = value
        End Set
    End Property
    Private m_mcpseat As String


    Public Property strportnumber() As String
        Get
            Return m_portnumber
        End Get
        Set(ByVal value As String)
            m_portnumber = value
        End Set
    End Property
    Private m_portnumber As String


    Public Property strAreatype() As String
        Get
            Return m_Areatype
        End Get
        Set(ByVal value As String)
            m_Areatype = value
        End Set
    End Property
    Private m_Areatype As String



    Public Property strVert() As String
        Get
            Return m_vert
        End Get
        Set(ByVal value As String)
            m_vert = value
        End Set
    End Property
    Private m_vert As String

    Public Property strVertAlloc() As String
        Get
            Return m_vertAlloc
        End Get
        Set(ByVal value As String)
            m_vertAlloc = value
        End Set
    End Property
    Private m_vertAlloc As String

    Public Property strEmpid() As String
        Get
            Return m_Empid
        End Get
        Set(ByVal value As String)
            m_Empid = value
        End Set
    End Property
    Private m_Empid As String
    Public Property strName() As String
        Get
            Return m_Name
        End Get
        Set(ByVal value As String)
            m_Name = value
        End Set
    End Property
    Private m_Name As String
    Public Property strSpaceID() As String
        Get
            Return m_SpaceID
        End Get
        Set(ByVal value As String)
            m_SpaceID = value
        End Set
    End Property
    Private m_SpaceID As String
    Public Property strDep() As String
        Get
            Return m_DEP
        End Get
        Set(ByVal value As String)
            m_DEP = value
        End Set
    End Property
    Private m_DEP As String

    Public Property strfloor() As String
        Get
            Return m_Floor
        End Get
        Set(ByVal value As String)
            m_Floor = value
        End Set
    End Property
    Private m_Floor As String



    Public Property strWing() As String
        Get
            Return m_Wing
        End Get
        Set(ByVal value As String)
            m_Wing = value
        End Set
    End Property
    Private m_Wing As String



    Public Property strWST() As String
        Get
            Return m_WST
        End Get
        Set(ByVal value As String)
            m_WST = value
        End Set
    End Property
    Private m_WST As String

    Public Property strFCB() As String
        Get
            Return m_FCB
        End Get
        Set(ByVal value As String)
            m_FCB = value
        End Set
    End Property
    Private m_FCB As String


    Public Property strHCB() As String
        Get
            Return m_HCB
        End Get
        Set(ByVal value As String)
            m_HCB = value
        End Set
    End Property
    Private m_HCB As String


    Public Property strTOTAL() As String
        Get
            Return m_TOTAL
        End Get
        Set(ByVal value As String)
            m_TOTAL = value
        End Set
    End Property
    Private m_TOTAL As String



    Public Property strCostCenter() As String
        Get
            Return m_CostCenter
        End Get
        Set(ByVal value As String)
            m_CostCenter = value
        End Set
    End Property
    Private m_CostCenter As String






    Public Property strlcm_name() As String
        Get
            Return m_LCM_NAME
        End Get
        Set(ByVal value As String)
            m_LCM_NAME = value
        End Set
    End Property
    Private m_LCM_NAME As String
    Public Property strTwr_Name() As String
        Get
            Return m_TwrName
        End Get
        Set(ByVal value As String)
            m_TwrName = value
        End Set
    End Property
    Private m_TwrName As String


    Public Property strProject() As String
        Get
            Return m_Project
        End Get
        Set(ByVal value As String)
            m_Project = value
        End Set
    End Property
    Private m_Project As String

    Public Property strCity() As String
        Get
            Return m_City
        End Get
        Set(ByVal value As String)
            m_City = value
        End Set
    End Property
    Private m_City As String


    Private m_CTY_NAME As String

    Public Property strCTY_NAME() As String
        Get
            Return m_CTY_NAME
        End Get
        Set(ByVal value As String)
            m_CTY_NAME = value
        End Set
    End Property

    Private m_CTY_CODE As String
    Public Property strCTY_CODE() As String
        Get
            Return m_CTY_CODE
        End Get
        Set(ByVal value As String)
            m_CTY_CODE = value
        End Set
    End Property

    Private m_totalseats As String
    Public Property strTotalseats() As String
        Get
            Return m_totalseats
        End Get
        Set(ByVal value As String)
            m_totalseats = value
        End Set
    End Property


    Private m_alloted As String
    Public Property strAlloted() As String
        Get
            Return m_alloted
        End Get
        Set(ByVal value As String)
            m_alloted = value
        End Set
    End Property


    Private m_Available As String
    Public Property strAvailable() As String
        Get
            Return m_Available
        End Get
        Set(ByVal value As String)
            m_Available = value
        End Set
    End Property

    '---------- parameters -----------
    Private m_AVAILABLE_SEATS As String
    Public Property strAVAILABLE_SEATS() As String
        Get
            Return m_AVAILABLE_SEATS
        End Get
        Set(ByVal value As String)
            m_AVAILABLE_SEATS = value
        End Set
    End Property

    Private m_ALLOTED_SEATS As String
    Public Property strALLOTED_SEATS() As String
        Get
            Return m_ALLOTED_SEATS
        End Get
        Set(ByVal value As String)
            m_ALLOTED_SEATS = value
        End Set
    End Property

    Private m_OCCUPIED_SEATS As String
    Public Property strOCCUPIED_SEATS() As String
        Get
            Return m_OCCUPIED_SEATS
        End Get
        Set(ByVal value As String)
            m_OCCUPIED_SEATS = value
        End Set
    End Property

    Private m_ALLOTTED_BUT_NOT_OCCUPIED_SEATS As String
    Public Property strALLOTTED_BUT_NOT_OCCUPIED_SEATS() As String
        Get
            Return m_ALLOTTED_BUT_NOT_OCCUPIED_SEATS
        End Get
        Set(ByVal value As String)
            m_ALLOTTED_BUT_NOT_OCCUPIED_SEATS = value
        End Set
    End Property

    Private m_SEATS_AVAILABLE_FOR_ALLOCATION As String
    Public Property strSEATS_AVAILABLE_FOR_ALLOCATION() As String
        Get
            Return m_SEATS_AVAILABLE_FOR_ALLOCATION
        End Get
        Set(ByVal value As String)
            m_SEATS_AVAILABLE_FOR_ALLOCATION = value
        End Set
    End Property

    Private m_LCM_TYPE_ID As String
    Public Property strLCM_TYPE_ID() As String
        Get
            Return m_LCM_TYPE_ID
        End Get
        Set(ByVal value As String)
            m_LCM_TYPE_ID = value
        End Set
    End Property

End Class
