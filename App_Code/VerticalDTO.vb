Imports Microsoft.VisualBasic
Imports System

Namespace Amantra.VerticalDTON

    Public Class VerticalDTO
#Region "'Private variable declaration'"
        Private m_req_id As String = String.Empty

        Private m_loc_id As String = String.Empty
        Private m_bdg_id As String = String.Empty
        Private m_cty As String = String.Empty
        Private m_wst_num As Integer = 0
        Private m_full_cabins As Integer = 0
        Private m_half_cabins As String = 0
        Private m_wst_alloc As String = String.Empty
        Private m_wst_to As String = String.Empty
        Private m_fullcabins_alloc As String = String.Empty
        Private m_fullcabins_to As String = String.Empty
        Private m_halfcabins_alloc As String = String.Empty
        Private m_halfcabins_to As String = String.Empty
        Private m_flr_id As String = String.Empty
        Private m_twr_id As String = String.Empty
        Private m_wng_id As String = String.Empty
        Private m_remarks As String = String.Empty
        Private m_mon_name As String = String.Empty
        Private m_mon_id As String = String.Empty
        Private m_year As String = String.Empty
        Private m_req_name As String = String.Empty
        Private m_flr_name As String = String.Empty
        Private m_twr_name As String = String.Empty
        Private m_wng_name As String = String.Empty
        Private m_spc_id As String = String.Empty
        Private m_usr_id As String = String.Empty
        Private m_sta_id As Integer = 0
        Private m_frm_date As String = String.Empty
        Private m_to_date As String = String.Empty
        Private m_spc_type As String = String.Empty
        Private m_loc_name As String = String.Empty
        Private m_month As String = String.Empty
        Private m_req_date As String = String.Empty
        Private m_Lab_Space As String = String.Empty
        Private m_svr_vertical As String = String.Empty
        Private m_svr_from As DateTime
        Private m_svr_to As DateTime
#End Region

#Region "Constructors"

        Public Sub New()

        End Sub
        Public Sub New(ByVal p_srn_req_id As String)
            Me.m_req_id = p_srn_req_id
        End Sub
        Public Sub New(ByVal p_spc_id As String, ByVal state As Integer)
            If state = 1 Then
                m_spc_id = p_spc_id
            End If
        End Sub
        Public Sub New(ByVal p_name As String, ByVal p_id As String, ByVal state As Integer)
            If state = 1 Then
                Me.m_twr_id = p_id
                Me.m_twr_name = p_name
            ElseIf state = 2 Then
                Me.m_flr_id = p_id
                Me.m_flr_name = p_name
            ElseIf state = 3 Then
                Me.m_wng_id = p_id
                Me.m_wng_name = p_name
            End If

        End Sub

        Public Sub New(ByVal p_mon_name As String, ByVal p_mon_id As String, ByVal p_year As String)
            Me.m_mon_id = p_mon_id
            Me.m_mon_name = p_mon_name
            Me.m_year = p_year
        End Sub
        Public Sub New(ByVal p_wst_alloc As String, ByVal p_full_alloc As String, ByVal p_half_alloc As String, ByVal status As Integer)
            If status = 1 Then
                Me.m_wst_alloc = p_wst_alloc
                Me.m_fullcabins_alloc = p_full_alloc
                Me.m_halfcabins_alloc = p_half_alloc
            End If
        End Sub

        Public Sub New(ByVal p_remarks As String, ByVal p_req_name As String, ByVal p_wst_num As Integer, ByVal p_hc_num As Integer, ByVal p_fc_num As Integer, ByVal strLocID As String, ByVal p_loc_name As String, ByVal str_SVR_VERTICAL As String)
            Me.m_req_name = p_req_name
            Me.m_remarks = p_remarks
            Me.m_wst_num = p_wst_num
            Me.m_half_cabins = p_hc_num
            Me.m_full_cabins = p_fc_num
            Me.m_loc_id = strLocID
            Me.m_loc_name = p_loc_name
            Me.m_svr_vertical = str_SVR_VERTICAL
        End Sub
        Public Sub New(ByVal p_remarks As String, ByVal p_req_name As String, ByVal p_wst_num As Integer, ByVal p_hc_num As Integer, ByVal p_fc_num As Integer, ByVal strLocID As String, ByVal p_loc_name As String, ByVal str_SVR_VERTICAL As String, ByVal p_req_date As String, ByVal P_SVR_LABSPACE As String, ByVal P_SVR_Cty As String)
            Me.m_req_name = p_req_name
            Me.m_remarks = p_remarks
            Me.m_wst_num = p_wst_num
            Me.m_half_cabins = p_hc_num
            Me.m_full_cabins = p_fc_num
            Me.m_loc_id = strLocID
            Me.m_loc_name = p_loc_name
            Me.m_svr_vertical = str_SVR_VERTICAL
            Me.m_req_date = p_req_date
            Me.m_Lab_Space = P_SVR_LABSPACE
            Me.m_cty = P_SVR_Cty
        End Sub

        Public Sub New(ByVal p_remarks As String, ByVal p_req_name As String, ByVal p_wst_num As Integer, ByVal p_hc_num As Integer, ByVal p_fc_num As Integer, ByVal strLocID As String, ByVal p_loc_name As String, ByVal str_SVR_VERTICAL As String, ByVal p_req_date As String, ByVal P_SVR_LABSPACE As String, ByVal P_SVR_Cty As String, ByVal P_SVR_From As String, ByVal P_SVR_To As String)
            Me.m_req_name = p_req_name
            Me.m_remarks = p_remarks
            Me.m_wst_num = p_wst_num
            Me.m_half_cabins = p_hc_num
            Me.m_full_cabins = p_fc_num
            Me.m_loc_id = strLocID
            Me.m_loc_name = p_loc_name
            Me.m_svr_vertical = str_SVR_VERTICAL
            Me.m_req_date = p_req_date
            Me.m_Lab_Space = P_SVR_LABSPACE
            Me.m_cty = P_SVR_Cty
            Me.m_frm_date = P_SVR_From
            Me.m_to_date = P_SVR_To
        End Sub


        Public Sub New(ByVal p_srn_req_id As String, ByVal p_cabins As Integer, ByVal p_cubicals As Integer, ByVal p_work_stations As Integer, ByVal p_srn_bdg_one As String, ByVal p_srn_bdg_two As String, ByVal p_srn_frm_dt As String, ByVal p_srn_to_dt As String, ByVal p_usr_name As String, ByVal p_RptMgr_Name As String, ByVal p_srn_sta_id As String, ByVal p_srn_dept As String, ByVal p_srn_rem As String, ByVal p_rm_remarks As String)
            'Me.m_srn_req_id = p_srn_req_id
            'Me.m_cabins = p_cabins
            'Me.m_cubicals = p_cubicals
            'Me.m_Work_Stations = p_work_stations
            'Me.m_srn_bdg_one = p_srn_bdg_one
            'Me.m_srn_bdg_two = p_srn_bdg_two
            'Me.m_srn_frm_dt = p_srn_frm_dt
            'Me.m_srn_to_dt = p_srn_to_dt
            'Me.m_usr_name = p_usr_name
            'Me.m_RptMgr_Name = p_RptMgr_Name
            'Me.m_srn_sta_id = p_srn_sta_id
            'Me.m_srn_dept = p_srn_dept
            'Me.m_srn_rem = p_srn_rem
            'Me.m_srn_rm_remarks = p_rm_remarks
        End Sub
#End Region

#Region "Properties"

        Public Property CityCode() As String
            Get
                Return m_cty
            End Get
            Set(ByVal value As String)
                m_cty = value
            End Set
        End Property

        Public Property SvrFromDate() As DateTime
            Get
                Return m_svr_from
            End Get
            Set(ByVal value As DateTime)
                m_svr_from = value
            End Set
        End Property

        Public Property SvrToDate() As DateTime
            Get
                Return m_svr_to
            End Get
            Set(ByVal value As DateTime)
                m_svr_to = value
            End Set
        End Property


        Public Property RequestID() As String
            Get
                Return m_req_id
            End Get
            Set(ByVal value As String)
                m_req_id = value
            End Set
        End Property
        Public Property getvertical() As String
            Get
                Return m_svr_vertical
            End Get
            Set(ByVal value As String)
                m_svr_vertical = value
            End Set
        End Property
        Public Property LocationName() As String
            Get
                Return m_loc_name
            End Get
            Set(ByVal value As String)
                m_loc_name = value
            End Set
        End Property
        Public Property SpaceType() As String
            Get
                Return m_spc_type
            End Get
            Set(ByVal value As String)
                m_spc_type = value
            End Set
        End Property
        Public Property Status() As String
            Get
                Return m_sta_id
            End Get
            Set(ByVal value As String)
                m_sta_id = value
            End Set
        End Property
        Public Property FromDate() As String
            Get
                Return m_frm_date
            End Get
            Set(ByVal value As String)
                m_frm_date = value
            End Set
        End Property
        Public Property ToDate() As String
            Get
                Return m_to_date
            End Get
            Set(ByVal value As String)
                m_to_date = value
            End Set
        End Property

        Public Property UserID() As String
            Get
                Return m_usr_id
            End Get
            Set(ByVal value As String)
                m_usr_id = value
            End Set
        End Property

        Public Property MonthID() As String
            Get
                Return m_mon_id
            End Get
            Set(ByVal value As String)
                m_mon_id = value
            End Set
        End Property
        Public Property MonthName() As String
            Get
                Return m_mon_name
            End Get
            Set(ByVal value As String)
                m_mon_name = value
            End Set
        End Property

        Public Property Year() As String
            Get
                Return m_year
            End Get
            Set(ByVal value As String)
                m_year = value
            End Set
        End Property

        Public Property Remarks() As String
            Get
                Return m_remarks
            End Get
            Set(ByVal value As String)
                m_remarks = value
            End Set
        End Property

        Public Property RequestorName() As String
            Get
                Return m_req_name
            End Get
            Set(ByVal value As String)
                m_req_name = value
            End Set
        End Property
        Public Property WorkStations() As Integer
            Get
                Return m_wst_num
            End Get
            Set(ByVal value As Integer)
                m_wst_num = value
            End Set
        End Property
        Public Property FullCabins() As Integer
            Get
                Return m_full_cabins
            End Get
            Set(ByVal value As Integer)
                m_full_cabins = value
            End Set
        End Property
        Public Property HalfCabins() As Integer
            Get
                Return m_half_cabins
            End Get
            Set(ByVal value As Integer)
                m_half_cabins = value
            End Set
        End Property

        Public Property Towername() As String
            Get
                Return m_twr_name
            End Get
            Set(ByVal value As String)
                m_twr_name = value
            End Set
        End Property
        Public Property Towercode() As String
            Get
                Return m_twr_id
            End Get
            Set(ByVal value As String)
                m_twr_id = value
            End Set
        End Property
        Public Property FloorName() As String
            Get
                Return m_flr_name
            End Get
            Set(ByVal value As String)
                m_flr_name = value
            End Set
        End Property

        Public Property LocationCode() As String
            Get
                Return m_loc_id
            End Get
            Set(ByVal value As String)
                m_loc_id = value
            End Set
        End Property
        Public Property FloorCode() As String
            Get
                Return m_flr_id
            End Get
            Set(ByVal value As String)
                m_flr_id = value
            End Set
        End Property
        Public Property WingName() As String
            Get
                Return m_wng_name
            End Get
            Set(ByVal value As String)
                m_wng_name = value
            End Set
        End Property
        Public Property WingCode() As String
            Get
                Return m_wng_id
            End Get
            Set(ByVal value As String)
                m_wng_id = value
            End Set
        End Property
        Public Property SpaceID() As String
            Get
                Return m_spc_id
            End Get
            Set(ByVal value As String)
                m_spc_id = value
            End Set
        End Property
        Public Property WorkStationsAllocated() As String
            Get
                Return m_wst_alloc
            End Get
            Set(ByVal value As String)
                m_wst_alloc = value
            End Set
        End Property
        Public Property FullCabinsAllocated() As String
            Get
                Return m_fullcabins_alloc
            End Get
            Set(ByVal value As String)
                m_fullcabins_alloc = value
            End Set
        End Property
        Public Property HalfCabinsAllocated() As String
            Get
                Return m_halfcabins_alloc
            End Get
            Set(ByVal value As String)
                m_halfcabins_alloc = value
            End Set
        End Property
        Public Property LabSapce() As String
            Get
                Return m_Lab_Space
            End Get
            Set(ByVal value As String)
                m_Lab_Space = value
            End Set
        End Property
        Public Property RequestDate() As String
            Get
                Return m_req_date
            End Get
            Set(ByVal value As String)
                m_req_date = value
            End Set
        End Property

#End Region

    End Class

End Namespace
