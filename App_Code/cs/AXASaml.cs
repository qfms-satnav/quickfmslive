﻿using System;
using System.IO;
using System.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.IO.Compression;
using System.Web;

/// <summary>
/// Summary description for Saml
/// </summary>
namespace OneLogin
{
    namespace AXASaml
    {
        public class Certificate
        {
            public X509Certificate2 cert;

            public void LoadCertificate(string certificate)
            {
                cert = new X509Certificate2();
                cert.Import(StringToByteArray(certificate));
            }

            public void LoadCertificate(byte[] certificate)
            {
                cert = new X509Certificate2();
                cert.Import(certificate);
            }

            private byte[] StringToByteArray(string st)
            {
                byte[] bytes = new byte[st.Length];
                for (int i = 0; i < st.Length; i++)
                {
                    bytes[i] = (byte)st[i];
                }
                return bytes;
            }
        }

        public class Response
        {
            private XmlDocument xmlDoc;
            private AXASettings accountSettings;
            private Certificate certificate;
            public static bool CreateLogFile(string message)
            {
                try
                {
                    // string location = @"C://IRPC//myfile1.txt";
                    string filename = "NewLog_AXA" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                    string location = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") + filename); // ''(System.Environment.CurrentDirectory + ("\log.txt"))
                    if (!File.Exists(location))
                    {
                        FileStream fs;
                        fs = new FileStream(location, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                        fs.Close();
                    }

                    Console.WriteLine(message);
                    // Release the File that is created
                    StreamWriter sw = new StreamWriter(location, true);
                    sw.Write((message + Environment.NewLine));
                    sw.Close();
                    sw = null;
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
            public Response(AXASettings accountSettings)
            {
                this.accountSettings = accountSettings;
                certificate = new Certificate();
                certificate.LoadCertificate(accountSettings.certificate);
            }

            public void LoadXml(string xml)
            {
                xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.XmlResolver = null;
                xmlDoc.LoadXml(xml);
            }

            public void LoadXmlFromBase64(string response)
            {
                CreateLogFile("Reading Response");
                CreateLogFile(response);
                System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
                LoadXml(enc.GetString(Convert.FromBase64String(response)));
            }

            public bool IsValid()
            {
                bool status = true;

                status &= VerifyXml(xmlDoc);

                var notBefore = NotBefore();
                status &= !notBefore.HasValue || (notBefore <= DateTime.Now);

                var notOnOrAfter = NotOnOrAfter();
                status &= !notOnOrAfter.HasValue || (notOnOrAfter > DateTime.Now);
                CreateLogFile(status.ToString());
                return status;
            }
            private static bool VerifyXml(XmlDocument Doc)
            {
                if (Doc == null)
                    throw new ArgumentException("Doc");
                SignedXml signedXml = new SignedXml(Doc);
                var nsManager = new XmlNamespaceManager(Doc.NameTable);
                nsManager.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
                var node = Doc.SelectSingleNode("//ds:Signature", nsManager);
                // find signature node
                var certElement = Doc.SelectSingleNode("//ds:X509Certificate", nsManager);
                // find certificate node
                var cert = new X509Certificate2(Convert.FromBase64String(certElement.InnerText));
                signedXml.LoadXml((XmlElement)node);
                return signedXml.CheckSignature(cert, true);
            }
            public DateTime? NotBefore()
            {
                XmlNamespaceManager manager = new XmlNamespaceManager(xmlDoc.NameTable);
                manager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
                manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");

                var nodes = xmlDoc.SelectNodes("/samlp:Response/saml:Assertion/saml:Conditions", manager);
                string value = null;
                if (nodes != null && nodes.Count > 0 && nodes[0] != null && nodes[0].Attributes != null && nodes[0].Attributes["NotBefore"] != null)
                {
                    value = nodes[0].Attributes["NotBefore"].Value;
                }
                return value != null ? DateTime.Parse(value) : (DateTime?)null;
            }

            public DateTime? NotOnOrAfter()
            {
                XmlNamespaceManager manager = new XmlNamespaceManager(xmlDoc.NameTable);
                manager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
                manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");

                var nodes = xmlDoc.SelectNodes("/samlp:Response/saml:Assertion/saml:Conditions", manager);
                string value = null;
                if (nodes != null && nodes.Count > 0 && nodes[0] != null && nodes[0].Attributes != null && nodes[0].Attributes["NotOnOrAfter"] != null)
                {
                    value = nodes[0].Attributes["NotOnOrAfter"].Value;
                }
                return value != null ? DateTime.Parse(value) : (DateTime?)null;
            }

            public string GetNameID()
            {
                XmlNamespaceManager manager = new XmlNamespaceManager(xmlDoc.NameTable);
                manager.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);
                manager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
                manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");
                CreateLogFile("Getting mailid");
                XmlNode node = xmlDoc.SelectSingleNode("/samlp:Response/saml:Assertion/saml:Subject/saml:NameID", manager);
                CreateLogFile(node.InnerText);
                return node.InnerText;
            }

            public string GetEmpID()
            {
                XmlNamespaceManager manager = new XmlNamespaceManager(xmlDoc.NameTable);
                manager.AddNamespace("ds", SignedXml.XmlDsigNamespaceUrl);
                manager.AddNamespace("saml", "urn:oasis:names:tc:SAML:2.0:assertion");
                manager.AddNamespace("samlp", "urn:oasis:names:tc:SAML:2.0:protocol");
                XmlNodeList xnList = xmlDoc.SelectNodes("/samlp:Response/saml:Assertion/saml:AttributeStatement/saml:Attribute", manager);
                string emp_Id = "";
                foreach (XmlNode xn in xnList)
                {
                    if (xn.Attributes[0].Value == "Emp ID")
                    {
                        emp_Id = xn.ChildNodes[0].InnerText;
                    }
                }
                return emp_Id;
            }
        }

        public class AuthRequest
        {
            public string id;
            private string issue_instant;
            private AxaSettings2 appSettings;
            private AXASettings accountSettings;

            public enum AuthRequestFormat
            {
                Base64 = 1
            }

            public AuthRequest(AxaSettings2 appSettings, AXASettings accountSettings)
            {
                this.appSettings = appSettings;
                this.accountSettings = accountSettings;

                id = "_" + System.Guid.NewGuid().ToString();
                issue_instant = DateTime.Now.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
            }
            
            public string GetRequest(AuthRequestFormat format)
            {
                using (StringWriter sw = new StringWriter())
                {
                    XmlWriterSettings xws = new XmlWriterSettings();
                    xws.OmitXmlDeclaration = true;

                    using (XmlWriter xw = XmlWriter.Create(sw, xws))
                    {
                        xw.WriteStartElement("samlp", "AuthnRequest", "urn:oasis:names:tc:SAML:2.0:protocol");
                        xw.WriteAttributeString("ID", id);
                        xw.WriteAttributeString("Version", "2.0");
                        xw.WriteAttributeString("IssueInstant", issue_instant);
                        xw.WriteAttributeString("ForceAuthn", "false");
                        xw.WriteAttributeString("IsPassive", "false");

                        xw.WriteAttributeString("AssertionConsumerServiceURL", appSettings.assertionConsumerServiceUrl);

                        xw.WriteStartElement("saml", "Issuer", "urn:oasis:names:tc:SAML:2.0:assertion");
                        xw.WriteString("https://live.quickfms.com/AxaSSO.aspx");
                        xw.WriteEndElement();

                        xw.WriteStartElement("samlp", "NameIDPolicy", "urn:oasis:names:tc:SAML:2.0:protocol");
                        xw.WriteAttributeString("Format", "urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified");                        
                        xw.WriteAttributeString("AllowCreate", "true");
                        xw.WriteEndElement();

                        xw.WriteStartElement("samlp", "RequestedAuthnContext", "urn:oasis:names:tc:SAML:2.0:protocol");
                        xw.WriteAttributeString("Comparison", "exact");

                        xw.WriteStartElement("saml", "AuthnContextClassRef", "urn:oasis:names:tc:SAML:2.0:assertion");
                        xw.WriteString("urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport");
                        xw.WriteEndElement();

                        //xw.WriteEndElement(); // RequestedAuthnContext

                        //xw.WriteEndElement();
                    }

                    //if (format == AuthRequestFormat.Base64)
                    //{
                    //    byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sw.ToString());
                    //    return System.Convert.ToBase64String(toEncodeAsBytes);
                    //}

                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.LoadXml(sw.ToString());

                    byte[] toEncodeAsBytes = ASCIIEncoding.ASCII.GetBytes(xmlDocument.OuterXml);

                    using (var output = new MemoryStream())
                    {
                        using (var zip = new DeflateStream(output, CompressionMode.Compress))
                        {
                            zip.Write(toEncodeAsBytes, 0, toEncodeAsBytes.Length);
                        }
                        return Convert.ToBase64String(output.ToArray());
                    }
                    return null;
                }
            }
        }
    }
}