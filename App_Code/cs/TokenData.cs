﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class TokenData
{
    public string NameIdentifier { get; set; }
    public string Upn { get; set; }
    public string Name { get; set; }
    public string Surname { get; set; }
    public string Role { get; set; }
    public string Email { get; set; }
    public string PetName { get; set; }
    public bool IsAuthenticated { get; set; }
}
