﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace BasicAuthentication
{
    public class BasicAuthenticationAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.Headers.Authorization != null)
            {
                var authToken = actionContext.Request.Headers
                    .Authorization.Parameter;

                var decodeauthToken = System.Text.Encoding.UTF8.GetString(
                    Convert.FromBase64String(authToken));

                var arrUserNameandPassword = decodeauthToken.Split(':');

                if (IsAuthorizedUser(arrUserNameandPassword[0], arrUserNameandPassword[1]))
                {
                    Thread.CurrentPrincipal = new GenericPrincipal(
                    new GenericIdentity(arrUserNameandPassword[0]), null);
                }
                else
                {
                    actionContext.Response = actionContext.Request
                    .CreateResponse(HttpStatusCode.Unauthorized);
                }
            }
            else
            {
                actionContext.Response = actionContext.Request
                 .CreateResponse(HttpStatusCode.Unauthorized);
            }
        }

        public static bool IsAuthorizedUser(string Username, string Password)
        {
            // In this method we can handle our database logic here...
            if (Username == "BagicFMS")
            {
                return Username == "BagicFMS" && Password == "Bagic@123$";
            }
            else if (Username == "LibertyFMS")
            {
                return Username == "LibertyFMS" && Password == "Liberty@123$";
            }
            else if (Username == "HondaFMS")
            {
                return Username == "HondaFMS" && Password == "Honda@123$";
            }
            else if (Username == "JohndeereFMS")
            {
                return Username == "JohndeereFMS" && Password == "JD@123$";
            }
            else if (Username == "ApexonFMS")
            {
                return Username == "ApexonFMS" && Password == "Apexon@123$";
            }
            else if (Username == "BhartiAXAFMS")
            {
                return Username == "BhartiAXAFMS" && Password == "BhartiAXA@123$";
            }
            else if (Username == "QuickFMS_HRMS")
            {
                return Username == "QuickFMS_HRMS" && Password == "QuickFMS_HRMS@123$";
            }
            else if (Username == "QuickFMSLTTS")
            {
                return Username == "QuickFMSLTTS" && Password == "QuickFMSLTTS@123$";
            }
            else
            {
                return false;
            }


        }
    }
}