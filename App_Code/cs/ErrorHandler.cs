﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ErrorHandler
/// </summary>
public class ErrorHandler
{
    #region Error Log Logic
    public void _WriteErrorLog(Exception ex)
    {
        string errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string path = HttpContext.Current.Server.MapPath("~/ErrorLogFiles/" + errorLogFilename);
        if (File.Exists(path))
        {
            using (StreamWriter stwriter = new StreamWriter(path, true))
            {
                stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
                stwriter.WriteLine("Message:" + ex.ToString());
                stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            }
        }
        else
        {
            StreamWriter stwriter = File.CreateText(path);
            stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.WriteLine("Message: " + ex.ToString());
            stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.Close();
        }
    }
    #endregion
    #region Error Log Logic
    public void _WriteErrorLog(String str)
    {
        string errorLogFilename = "ErrorLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
        string path = HttpContext.Current.Server.MapPath("~/ErrorLogFiles/" + errorLogFilename);
        if (File.Exists(path))
        {
            using (StreamWriter stwriter = new StreamWriter(path, true))
            {
                stwriter.WriteLine("Information Start as on " + DateTime.Now.ToString("hh:mm tt"));
                stwriter.WriteLine("Message:" + str);
                stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            }
        }
        else
        {
            StreamWriter stwriter = File.CreateText(path);
            stwriter.WriteLine("Error Log Start as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.WriteLine("Message: " + str);
            stwriter.WriteLine("End as on " + DateTime.Now.ToString("hh:mm tt"));
            stwriter.Close();
        }
    }
    #endregion

    #region Inserting Other Error's In DB Error Log
    public void _OtherExceptionsInDBErrorLog(Exception ex)
    {
        try
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(System.Configuration.ConfigurationManager.AppSettings["FRMDB"] + "." + "ERROR_LOG");
            sp.Command.AddParameter("@ERROR", ex.ToString(), DbType.String);
            sp.ExecuteScalar();
        }
        catch (SqlException e)
        {
            _WriteErrorLog(e);
        }
    }
    #endregion
}