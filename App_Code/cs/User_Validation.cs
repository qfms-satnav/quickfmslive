﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for User_Validation
/// </summary>
/// 

public static class User_Validation
{
    public static CLS_VALIDATIONS GetValidationExpressionForCode()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[A-Za-z0-9-/_:.() ]+";
        obj.VAL_MSG = "Please Enter Code in Alphanumeric, Special Character (Space-/_():) Allowed";
        return obj;
    }
    public static CLS_VALIDATIONS GetValidationExpressionForName()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[0-9a-zA-Z-_/:&.() ]+";
        obj.VAL_MSG = "Please Enter Name in Alphanumeric, Special Character (Space-/_&():) Allowed";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForRemarks()
    {

        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        //obj.VAL_EXPR = @"[a-zA-Z0-9@+'.!#$'&quot;,:;=/\(\),\-\s]{1,255}$";
        obj.VAL_EXPR = @"^[a-zA-Z0-9@#$%&*+\-_(){}<>|^~`""=,+':;?.,![\]\s\\/]+";
        obj.VAL_MSG = "Please Enter Remarks upto 500 Characters, special characters (-/_@$)(&*,space) are allowed";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForEmail()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^([0-9a-zA-Z]([-\.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$";
        obj.VAL_MSG = "Please Enter a valid email address, example: you@yourdomain.com";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForPhone()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[0-9-]+";
        obj.VAL_MSG = "Please Enter Numbers Only";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForPassword()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[a-zA-Z0-9-/_@&$%*^ ]+";
        obj.VAL_MSG = "Please Enter password in alphanumeric, special characters (-/_@&$%*^space) are allowed";
        return obj;

    }      
    public static CLS_VALIDATIONS GetValidationExpressionForNormalText()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[0-9a-zA-Z-_/:&.() ]+";
        obj.VAL_MSG = "Please Enter Text in Alphanumeric, Special Character (Space-/_&():) Allowed";
        return obj;

    }   
    public static CLS_VALIDATIONS GetValidationExpressionForLat()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[0-9.]+";
        obj.VAL_MSG = "Please Enter Valid Latitude";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForLong()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[0-9.]+";
        obj.VAL_MSG = "Please Enter Valid Longitude";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForAddress()
    {

        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[a-zA-Z0-9@#$%&*+\-_(){}<>|^~`""=,+':;?.,![\]\s\\/]+";
        obj.VAL_MSG = "Please Enter address in alphanumeric, special characters (-/_@$&*)(,#space) are allowed";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForFax()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[0-9-]+";
        obj.VAL_MSG = "Please Enter Number Only";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionFoMobile()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[0-9-]+";
        obj.VAL_MSG = "Please Enter Number Only";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForNumber()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[0-9-]+";
        obj.VAL_MSG = "Please Enter Number Only";
        return obj;

    }
    public static CLS_VALIDATIONS GetValidationExpressionForDecimalValues()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"((\d+)((\.\d{1,2})?))$";
        obj.VAL_MSG = "Please Enter Valid Numbers or Decimal Number with 2 Decimal Places.";
        return obj;
    }
    public static CLS_VALIDATIONS GetValidationExpressionForCheque()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"[0-9]{3,8}";
        obj.VAL_MSG = "Enter Numbers and No Special Characters allowed.";
        return obj;
    }
    public static CLS_VALIDATIONS GetValidationExpressionForOnlyName()
    {
        CLS_VALIDATIONS obj = new CLS_VALIDATIONS();
        obj.VAL_EXPR = @"^[A-Za-z0-9 ]*$";
        obj.VAL_MSG = "Enter alphabets and Numbers only and No Special Characters allowed.";
        return obj;
    }
   
}

public class CLS_VALIDATIONS
{
    public string VAL_EXPR { get; set; }
    public string VAL_MSG { get; set; }


}
