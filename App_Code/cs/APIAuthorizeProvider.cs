﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using System.Threading.Tasks;

/// <summary>
/// Summary description for APIAuthorizeProvider
/// </summary>
public class APIAuthorizeProvider : OAuthAuthorizationServerProvider
{
    public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
    {
        await Task.Run(() => context.Validated());
    }

    public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
    {
        try
        {
            //int number = CheckUserSSO(context.UserName, context.Password);
            //if (number == 1)
            //{
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, context.UserName.ToString()));
                identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                await Task.Run(() => context.Validated(identity));
            //}
            //else if (number == -1)
            //{
            //    context.SetError("invalid_grant", "User Blocked Try again after 30 min");
            //    return;
            //}
            //else
            //{
            //    context.SetError("invalid_grant", "Provided username or password is incorrect");
            //    return;
            //}
        }
        catch(Exception ex)
        {
            //int error = CheckUserDecryptionerror(context.UserName, context.Password);
            //if (error == -1)
            //{
            //    context.SetError("invalid_grant", "User Blocked Try again after 30 min");
            //    return;
            //}
            //else
            //{
            //    context.SetError("invalid_grant", "Provided username or password is incorrect");
            //    return;
            //}
        }


    }

    private int CheckUser(string userName, string password)
    {
            using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["csamantraFAM"].ConnectionString))
            {
                sqlCon.Open();
                SqlCommand Cmnd = new SqlCommand("GN_VALIDATE_USER", sqlCon);
                Cmnd.CommandType = CommandType.StoredProcedure;
                Cmnd.Parameters.AddWithValue("@COMPANY", "1");
                Cmnd.Parameters.AddWithValue("@USR_ID", userName);
                //Cmnd.Parameters.AddWithValue("@USR_LOGIN_PASSWORD", password);
                //Cmnd.Parameters.AddWithValue("@USR_LOGIN_PASSWORD", Encrypt_Class.DecryptCipherTextToPlainText(password));
            return (int)Cmnd.ExecuteScalar();
            }
    }

    private int CheckUserSSO(string userName, string password)
    {
        using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["csamantraFAM"].ConnectionString))
        {
            sqlCon.Open();
            SqlCommand Cmnd = new SqlCommand("[GET_USERID_EXIST_SSO]", sqlCon);
            Cmnd.CommandType = CommandType.StoredProcedure;
            //Cmnd.Parameters.AddWithValue("@COMPANY", "1");
            Cmnd.Parameters.AddWithValue("@EMPID", userName);
            //Cmnd.Parameters.AddWithValue("@USR_LOGIN_PASSWORD", password);
            //Cmnd.Parameters.AddWithValue("@USR_LOGIN_PASSWORD", Encrypt_Class.DecryptCipherTextToPlainText(password));
            return (int)Cmnd.ExecuteScalar();
        }
    }

    private int CheckUserDecryptionerror(string userName, string password)
    {
        using (SqlConnection sqlCon = new SqlConnection(ConfigurationManager.ConnectionStrings["csamantraFAM"].ConnectionString))
        {
            sqlCon.Open();
            SqlCommand Cmnd = new SqlCommand("GN_VALIDATE_USER", sqlCon);
            Cmnd.CommandType = CommandType.StoredProcedure;
            Cmnd.Parameters.AddWithValue("@COMPANY", "1");
            Cmnd.Parameters.AddWithValue("@USR_ID", userName);
            Cmnd.Parameters.AddWithValue("@USR_LOGIN_PASSWORD", "@1@");
            //Cmnd.Parameters.AddWithValue("@USR_LOGIN_PASSWORD", Encrypt_Class.DecryptCipherTextToPlainText(password));
            return (int)Cmnd.ExecuteScalar();
        }
    }


}