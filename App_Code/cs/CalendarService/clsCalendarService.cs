﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Calendar.v3;
using Google.Apis.Services;
using Google.Apis.Calendar.v3.Data;
using Google_Clndr_Api_dotnet;

/// <summary>
/// Summary description for frmCalendar
/// </summary>
public class clsCalendarService
{

    public String Summary { get; set; }
    public String Location { get; set; }
    public String Description { get; set; }
    public EventDateTime Start { get; set; }
    public EventDateTime End { get; set; }
    public String[] Recurrence { get; set; }
    public EventAttendee[] Attendees { get; set; }
    public Event.RemindersData Reminders { get; set; }
    public String CalendarId { get; set; }
    public String DelEventID { get; set; }

    public String SERVICE_ACCOUNT_EMAIL { get; set; }

    public String SERVICE_ACCOUNT_KEYFILE { get; set; }

    private CalendarService service;

    public clsCalendarService()
    {
        try
        {
            SERVICE_ACCOUNT_EMAIL = System.Configuration.ConfigurationManager.AppSettings["GoogleCalendarKey"];
            SERVICE_ACCOUNT_KEYFILE = System.Configuration.ConfigurationManager.AppSettings["GoogleCalendarKeyFile"];
            service = Authentication.AuthenticateServiceAccount(SERVICE_ACCOUNT_EMAIL, SERVICE_ACCOUNT_KEYFILE);
            CalendarId = "primary";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public string PushEvent()
    {
        try
        {

            Event newEvent = new Event()
            {
                Summary = Summary,
                Location = Location,
                Description = Description,
                Start = Start,
                End = End,
                Recurrence = Recurrence,
                Attendees = Attendees,
                Reminders = Reminders
            };
            EventsResource.InsertRequest insertrequest = service.Events.Insert(newEvent, CalendarId);
            Event createdEvent = insertrequest.Execute();
            String EventID = createdEvent.Id;
            return EventID;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return "0";
    }

    public Events GetEvents()
    {
        EventsResource.ListRequest request = service.Events.List("primary");

        //request.TimeMin = DateTime.Now;
        //request.ShowDeleted = false;
        //request.SingleEvents = true;
        //request.MaxResults = 10;
        //request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

        // List events.
        Events events = request.Execute();
        return events;

        //if (events.Items != null && events.Items.Count > 0)
        //{
        //    foreach (var eventItem in events.Items)
        //    {
        //        string when = eventItem.Start.DateTime.ToString();
        //        if (String.IsNullOrEmpty(when))
        //        {
        //            when = eventItem.Start.Date;
        //        }
        //        Console.WriteLine("{0} ({1})", eventItem.Summary, when);
        //        Console.ReadKey();
        //    }
        //}
    }

    public Boolean DeleteEvents()
    {
        try
        {
            service.Events.Delete("primary", DelEventID).Execute(); // service.Events.Delete("primary", "eventId").Execute();
            return true;
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return false;
    }
}