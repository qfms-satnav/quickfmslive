﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.Apis.Auth.OAuth2;
using System.Threading;
using Google.Apis.Util.Store;
using Google.Apis.Calendar.v3;
using Google.Apis.Services;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;


namespace Google_Clndr_Api_dotnet
{
    class Authentication
    {


        /// <summary>
        /// Authenticate to Google Using Oauth2
        /// Documentation https://developers.google.com/accounts/docs/OAuth2
        /// </summary>
        /// <param name="clientId">From Google Developer console https://console.developers.google.com</param>
        /// <param name="clientSecret">From Google Developer console https://console.developers.google.com</param>
        /// <param name="userName">A string used to identify a user.</param>
        /// <returns></returns>
        public static CalendarService AuthenticateOauth(string clientId, string clientSecret, string userName)
        {

            string[] scopes = new string[] {
        CalendarService.Scope.Calendar  ,  // Manage your calendars
        CalendarService.Scope.CalendarReadonly    // View your Calendars
            };

            try
            {
                // here is where we Request the user to give us access, or use the Refresh Token that was previously stored in %AppData%
                UserCredential credential = GoogleWebAuthorizationBroker.AuthorizeAsync(new ClientSecrets { ClientId = clientId, ClientSecret = clientSecret }
                                                                    , scopes
                                                                    , userName
                                                                    , CancellationToken.None
                                                                    , new FileDataStore("Daimto.GoogleCalendar.Auth.Store")).Result;



                CalendarService service = new CalendarService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Calendar API Sample",
                });
                return service;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.InnerException);
                return null;

            }

        }

        /// <summary>
        /// Authenticating to Google using a Service account
        /// Documentation: https://developers.google.com/accounts/docs/OAuth2#serviceaccount
        /// </summary>
        /// <param name="serviceAccountEmail">From Google Developer console https://console.developers.google.com</param>
        /// <param name="keyFilePath">Location of the Service account key file downloaded from Google Developer console https://console.developers.google.com</param>
        /// <returns></returns>
        public static CalendarService AuthenticateServiceAccount(string serviceAccountEmail, string keyFilePath)
        {

            // check the file exists
            //if (!File.Exists(keyFilePath))
            //{
            //    Console.WriteLine("An Error occurred - Key file does not exist");
            //    return null;
            //}

            string[] scopes = new string[] {
        CalendarService.Scope.Calendar  ,  // Manage your calendars
        CalendarService.Scope.CalendarReadonly    // View your Calendars
            };

           // var certificate = new X509Certificate2("<RSAKeyValue><Modulus>o+3euT+OU5aI3hAkSvdkpsSZnC911yWsqeMBNDQJ3qTFJoDzzTX6Icrjg/qEtODh64UTEQh2BvwHA2C88+INA6rFE/SEYMaoRRZx+mSz9fuVXbKGuhtwDE3e11IsCEapx/mew9b4fSSqgrg7CZM2VIFgT16q8kK52BPNZ3f+CfIzc3wEZPjMW5QUXp60pCuwN4FYjmsEf8NcukUeSElGdmpocnkjZL/MJKf4W3Xg3NhjYEwbBYhryaBIvIb515n6+UhUbcMaE+Oe04eEpbFs0L0LqUxfIzHM48tkmbiEM2ucjcPveO8L7vMnJ6+NO3XROOSSQjv07hy3AMi621dQcw==</Modulus><Exponent>AQAB</Exponent><P>0mna0WB9Pqt6GAFADJXXOxGDh/tbps1VGzhGG12+cnEeACKOMV7w4h8sX5VU4NVWxCPTcZMfL7r5c3AEq1k4tUjd5ketIZN5H+N+kyyU38eFVFkJ9x3JMzcgIVSCTasUHOq6npgsGNHinDJ0w0gZXu+E6qvpOKqp7UoKQoTNEFE=</P><Q>x3HcGECtVOv3FL8HLRCuYZyobzYWpLkBZ4R+kzqGPbi2XWnMc8BnPnYiN68ezvaM0eesNAnCvE6A4WnG7Jx3pM233B8mZ5SzzSfyZbT0JxhI7tNlwSpgSrYfYBX62aFuI0A/Ac286pQcZTYXvsPiIaNCDm0PuqXKOzi/CxXpx4M=</Q><DP>LZWNOEsFFyyjGMw8if10UXGr4yvLYUmepHT+izM17SkgbLsPvxZobTR/sn1xk/vj/j4cssBCw15oaF/eQxjjNjMBD8B7e4itdwEcDNpdrlVVRz667/ReAGBy74WVbSohA1q+71jcRMxDob80mJHoHS8axpRTxFkE00l1C5AltOE=</DP><DQ>NFyqQhvI5gDFb8bvZV95yNrORdRXgYuo2LqqWJEG6XFzP3clXEZpVLbDKQdq+T81nW5LgaYGYFXj9EpHFZvGtZfk63+qH4uk4PnZ4+XsK89uZnVNw7cSbMr/1cQvha+iMNMPN7QCIzwbcF8RQN5AokAweYUcmg4o1/9/+LMaG20=</DQ><InverseQ>FDQ/TYBFyGBADTrqLtbQdLOFZXDJ+73bakbalbmtM8Ol2vaORgxzPGQJNGI4undDkXY4RpXuwml4+c7/SB3JvHqONFJvKAhqpAFTP/LJq2WPKfeOnn1KRTJHxMiLtsmhB/8vULCpiXoClIvWx2PE1cdtZVxAIUxjKrQDPwj2Hqw=</InverseQ><D>TelELe9TCc9GBaTOWwLxx4ExMNYtv6+cPLIzZEhHWJYfTYmEVkgNNfpj39o1MHKhbXq18SaAUhl+i9Byfcc2QEPtnupb8FL6rrtYOJWcIpEMEJhFCDb6Lh7khpn7a4OKNg0htBp7x56hm7dL9+ULsWY8C/0GalXkVhozSbXx+29q0INRkTZ2LGIai1swjVVde6wktjdQUtu1T5K3goTrmkLFs1FS2lHHbD4isvjLsS9oV6FFPBPGK+Dd5SkCAIk1egk89ibuTdlq7Zopm3VH3hbfGtkURI3SWcNqmowwP18yFZHpuzTm4ZCIUCurqUkNA6aVoNLD9daiwqefmVOrgQ==</D></RSAKeyValue>", "notasecret", X509KeyStorageFlags.Exportable);

            //--===================================================
            //var xml = certificate.PrivateKey.ToXmlString(true);

            var rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString("<RSAKeyValue><Modulus>o+3euT+OU5aI3hAkSvdkpsSZnC911yWsqeMBNDQJ3qTFJoDzzTX6Icrjg/qEtODh64UTEQh2BvwHA2C88+INA6rFE/SEYMaoRRZx+mSz9fuVXbKGuhtwDE3e11IsCEapx/mew9b4fSSqgrg7CZM2VIFgT16q8kK52BPNZ3f+CfIzc3wEZPjMW5QUXp60pCuwN4FYjmsEf8NcukUeSElGdmpocnkjZL/MJKf4W3Xg3NhjYEwbBYhryaBIvIb515n6+UhUbcMaE+Oe04eEpbFs0L0LqUxfIzHM48tkmbiEM2ucjcPveO8L7vMnJ6+NO3XROOSSQjv07hy3AMi621dQcw==</Modulus><Exponent>AQAB</Exponent><P>0mna0WB9Pqt6GAFADJXXOxGDh/tbps1VGzhGG12+cnEeACKOMV7w4h8sX5VU4NVWxCPTcZMfL7r5c3AEq1k4tUjd5ketIZN5H+N+kyyU38eFVFkJ9x3JMzcgIVSCTasUHOq6npgsGNHinDJ0w0gZXu+E6qvpOKqp7UoKQoTNEFE=</P><Q>x3HcGECtVOv3FL8HLRCuYZyobzYWpLkBZ4R+kzqGPbi2XWnMc8BnPnYiN68ezvaM0eesNAnCvE6A4WnG7Jx3pM233B8mZ5SzzSfyZbT0JxhI7tNlwSpgSrYfYBX62aFuI0A/Ac286pQcZTYXvsPiIaNCDm0PuqXKOzi/CxXpx4M=</Q><DP>LZWNOEsFFyyjGMw8if10UXGr4yvLYUmepHT+izM17SkgbLsPvxZobTR/sn1xk/vj/j4cssBCw15oaF/eQxjjNjMBD8B7e4itdwEcDNpdrlVVRz667/ReAGBy74WVbSohA1q+71jcRMxDob80mJHoHS8axpRTxFkE00l1C5AltOE=</DP><DQ>NFyqQhvI5gDFb8bvZV95yNrORdRXgYuo2LqqWJEG6XFzP3clXEZpVLbDKQdq+T81nW5LgaYGYFXj9EpHFZvGtZfk63+qH4uk4PnZ4+XsK89uZnVNw7cSbMr/1cQvha+iMNMPN7QCIzwbcF8RQN5AokAweYUcmg4o1/9/+LMaG20=</DQ><InverseQ>FDQ/TYBFyGBADTrqLtbQdLOFZXDJ+73bakbalbmtM8Ol2vaORgxzPGQJNGI4undDkXY4RpXuwml4+c7/SB3JvHqONFJvKAhqpAFTP/LJq2WPKfeOnn1KRTJHxMiLtsmhB/8vULCpiXoClIvWx2PE1cdtZVxAIUxjKrQDPwj2Hqw=</InverseQ><D>TelELe9TCc9GBaTOWwLxx4ExMNYtv6+cPLIzZEhHWJYfTYmEVkgNNfpj39o1MHKhbXq18SaAUhl+i9Byfcc2QEPtnupb8FL6rrtYOJWcIpEMEJhFCDb6Lh7khpn7a4OKNg0htBp7x56hm7dL9+ULsWY8C/0GalXkVhozSbXx+29q0INRkTZ2LGIai1swjVVde6wktjdQUtu1T5K3goTrmkLFs1FS2lHHbD4isvjLsS9oV6FFPBPGK+Dd5SkCAIk1egk89ibuTdlq7Zopm3VH3hbfGtkURI3SWcNqmowwP18yFZHpuzTm4ZCIUCurqUkNA6aVoNLD9daiwqefmVOrgQ==</D></RSAKeyValue>");
            //==================================================
            try
            {
                ServiceAccountCredential credential = new ServiceAccountCredential(
                    new ServiceAccountCredential.Initializer(serviceAccountEmail)
                    {
                        Scopes = scopes,
                        Key = rsa
                    });
                    //.FromCertificate(certificate));

                // Create the service.
                CalendarService service = new CalendarService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Calendar API Sample",
                });
                return service;
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.InnerException);
                return null;

            }
        }

    }
}
