using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AppSettings
/// </summary>
public class AppSettings2
{
    public string assertionConsumerServiceUrl = "https://live.quickfms.com/JDSSO.aspx";
    public string apexonassertionConsumerServiceUrl = "https://live.quickfms.com/apexonsso.aspx";
    public string issuer = "test-app";
    public string LTTSassertionConsumerServiceUrl = "https://live.quickfms.com/LTTSSSO.aspx";
    public string EmidsassertionConsumerServiceUrl = "https://live.quickfms.com/emids.aspx";
    public string TataCapitalassertionConsumerServiceUrl = "https://live.quickfms.com/tatacapital.aspx";
    public string TataTechassertionConsumerServiceUrl = "https://live.quickfms.com/tatatech.aspx";
    public string AGSassertionConsumerServiceUrl = "https://live.quickfms.com/AGSSSO.aspx";
    public string ZelisassertionConsumerServiceUrl = "https://live.quickfms.com/ZelisSSO.aspx";
}
