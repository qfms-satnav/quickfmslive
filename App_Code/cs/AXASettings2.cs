﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for AppSettings2
/// </summary>
public class AxaSettings2
{
    public string assertionConsumerServiceUrl = "https://live.quickfms.com/AxaSSO.aspx";
    public string issuer = "Test-app";

}