﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

public class SetDatetimeOffset
{

    public static DateTime GetOffSetDateTime(DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
            {
                Temp = Temp.Insert(0, "+");
            }
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    //Response.Write(timeZoneInfo.ToString());
                    //string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return _now;
        }
        else
        {
            return DateTime.Now;
        }
    }
    public static System.DateTime GetOffSetDate(System.DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
            {
                Temp = Temp.Insert(0, "+");
            }
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            System.DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    //Response.Write(timeZoneInfo.ToString());
                    //string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return _now.Date;
        }
        else
        {
            return DateTime.Today;

        }
    }
}