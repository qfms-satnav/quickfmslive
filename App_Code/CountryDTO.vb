Imports Microsoft.VisualBasic


Namespace Amantra.CountryDTON

    Public Class CountryDTO
        Public Sub New()

        End Sub
#Region "'Private variable declaration'"

        Private m_countryid As String = String.Empty
        Private m_countryname As String = String.Empty
        Private m_countryrem As String = String.Empty
        Private m_countrycode As String = String.Empty
        Private m_countrystatus As String = String.Empty
        Private m_countryuser As String = String.Empty


#End Region


        Public Sub New(ByVal p_countrycode As String, ByVal p_countryname As String, ByVal p_countryrem As String)
            Me.m_countrycode = p_countrycode
            Me.m_countryname = p_countryname
            Me.m_countryrem = p_countryrem
        End Sub

#Region "Private variable declaration"

        Public Property Countryid() As String
            Get
                Return m_countryid
            End Get
            Set(ByVal value As String)
                m_countryid = value
            End Set
        End Property
        Public Property CountryName() As String
            Get
                Return m_countryname
            End Get
            Set(ByVal value As String)
                m_countryname = value
            End Set
        End Property
        Public Property CountryCode() As String

            Get
                Return m_countrycode
            End Get
            Set(ByVal value As String)
                m_countrycode = value
            End Set
        End Property
        Public Property CountryRemarks() As String
            Get
                Return m_countryrem
            End Get
            Set(ByVal value As String)
                m_countryrem = value
            End Set
        End Property
        Public Property CountryStatus() As Integer
            Get
                Return m_countrystatus
            End Get
            Set(ByVal value As Integer)
                m_countrystatus = value
            End Set
        End Property
        Public Property CountryUser() As String
            Get
                Return m_countryuser
            End Get
            Set(ByVal value As String)
                m_countryuser = value
            End Set
        End Property


#End Region
    End Class

End Namespace


