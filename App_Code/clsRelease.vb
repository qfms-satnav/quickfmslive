Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Collections.Generic
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Web
Imports System.Web.UI.WebControls
Imports System

Public Class clsRelease
#Region "Bind the Requestids to release"
    Public Sub release_requests(ByVal ddl As DropDownList, ByVal sta As Integer)
        Dim parms As SqlParameter() = {New SqlParameter("@staid", SqlDbType.Int, 50)}
        parms(0).Value = sta
        Try
            Dim dr As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_release_req_sp", parms)
            ddl.DataSource = dr
            ddl.DataTextField = "ssa_srnreq_id"
            ddl.DataValueField = "ssa_srnreq_id"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select--")

        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Update the Status of the Space "
    Public Sub updateSpace1(ByVal strReqID As String, ByVal intEmpId As Int16, ByVal strSpaceID As String, ByVal intStatusID As Integer)
        Dim sp1 As New SqlParameter("@strReqID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp1.Value = strReqID
        Dim sp2 As New SqlParameter("@intEmpID", SqlDbType.Int)
        sp2.Value = intEmpId
        Dim sp3 As New SqlParameter("@strSpaceID", SqlDbType.NVarChar, 50, ParameterDirection.Input)
        sp3.Value = strSpaceID
        Dim sp4 As New SqlParameter("@intStatusID", SqlDbType.Int)
        sp4.Value = intStatusID
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "UPDATE_SPACE_release", sp1, sp2, sp3, sp4)
    End Sub
#End Region

#Region "Update the Status of the Project wise Space "

    Public Function updateSpace(ByVal strSpaceID As String, ByVal strReqID As String) As String
        Dim spSpaceID As New SqlParameter("@VC_SPACEID", SqlDbType.NVarChar, 100)
        spSpaceID.Value = strSpaceID
        Dim spReqID As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
        spReqID.Value = strReqID
        Dim spAurID As New SqlParameter("@RM_ID", SqlDbType.NVarChar, 100)
        spAurID.Value = HttpContext.Current.Session("Uid").ToString()
        Dim retVal = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "NP_USP_UPDATE_SPACE_TO_RELEASE", spSpaceID, spReqID, spAurID)
        Return retVal
    End Function
#End Region

#Region "Update the Status of the Vertical wise Space when release "
    Public Sub updateSpace_vertical(ByVal strSpaceID As String, ByVal strReqID As String)
        Dim spReqID As New SqlParameter("@VC_REQID", SqlDbType.NVarChar, 100)
        Dim spSpaceID As New SqlParameter("@VC_SPACEID", SqlDbType.NVarChar, 100)
        spReqID.Value = strReqID
        spSpaceID.Value = strSpaceID

        strSQL = "UPDATE " & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACE_ALLOCATION SET SSA_STA_ID =165, SSA_RELREQ_DT='" & getoffsetdatetime(DateTime.Now) & "'  where  SSA_SRNREQ_ID ='" & strReqID & "' AND SSA_SPC_ID='" & strSpaceID & "'"
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)
        'SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, HttpContext.Current.Session("TENANT") & "."  & "USP_RELEASE_VERTICAL", strReqID, strSpaceID)
        strSQL = "update " & HttpContext.Current.Session("TENANT") & "." & "SMS_SPACE_ALLOCATION_FOR_PROJECT set SSA_STA_ID=165 where SSA_SPC_ID='" & strSpaceID & "'"
        SqlHelper.ExecuteNonQuery(CommandType.Text, strSQL)

    End Sub
#End Region
#Region "Displaying the details of a Request to a grid"
    Public Function binddata_grid(ByVal ddlitem As String) As DataSet
        Dim parms As SqlParameter() = {New SqlParameter("@reqid", SqlDbType.NVarChar, 50)}
        parms(0).Value = ddlitem
        Try
            Dim dr As DataSet = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_release_details_sp", parms)
            Return dr
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
#End Region

End Class
