#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Configuration
Imports SubSonic
Imports Commerce.Common
Imports Commerce.Providers
Imports System.Data

Namespace Commerce.Promotions
	Public Class PromotionService
		Private Shared discounts_Renamed As List(Of ProductDiscount)=Nothing

		''' <summary>
		''' A Collection of all the discounts, per product, in the store. This is loaded when the app starts.
		''' </summary>
		Public Shared ReadOnly Property Discounts() As List(Of ProductDiscount)
			Get
				If discounts_Renamed Is Nothing Then
					discounts_Renamed = ProductDiscount.GetDiscounts()
				End If
				Return discounts_Renamed
			End Get
		End Property

		''' <summary>
		''' Resets the static Discounts property. Use when you update
		''' the promos from the admin section.
		''' </summary>
		Public Shared Sub ResetPromos()
			discounts_Renamed = ProductDiscount.GetDiscounts()

		End Sub
		''' <summary>
		''' Returns discount information by SKU
		''' </summary>
		''' <param name="sku">Product Sku</param>
		''' <returns>ProductDiscount</returns>
		Public Shared Function GetProductDiscount(ByVal sku As String) As ProductDiscount
			Dim pOut As ProductDiscount = Nothing
			For Each disc As ProductDiscount In Discounts

				If disc.Sku.Trim().ToLower().Equals(sku.Trim().ToLower()) Then
					pOut = disc
					Exit For
				End If

			Next disc
			Return pOut
		End Function
		''' <summary>
		''' Returns discount information by ProductID
		''' </summary>
		''' <param name="productID">ProductID</param>
		''' <returns>ProductDiscount</returns>
		Public Shared Function GetProductDiscount(ByVal productID As Integer) As ProductDiscount
			Dim pOut As ProductDiscount = Nothing
			For Each disc As ProductDiscount In Discounts
				If disc.ProductID=productID Then
					pOut = disc
					Exit For
				End If

			Next disc
			Return pOut

		End Function

		''' <summary>
		''' Sets the pricing of a product according to the discounts found, and 
		''' returns the discount information (title, etc).
		''' </summary>
		''' <param name="product"></param>
		''' <returns>ProductDiscount</returns>
		Public Shared Function SetProductPricing(ByVal product As Commerce.Common.Product) As ProductDiscount
			Dim discount As ProductDiscount = GetProductDiscount(product.ProductID)
			If discount Is Nothing Then
				'this is a normal pricing scheme
				'meaning that the "everyday discount" off of retail
				'is in effect
				discount = New ProductDiscount()
								discount.Title = ConfigurationManager.AppSettings("discountTitle")
				discount.Description = ConfigurationManager.AppSettings("discountDescription")
				'the price has already been set by the merchant 
				'in the product's RetailPrice. So back-calculate it
				If product.RetailPrice>0 Then
					discount.Discount = product.OurPrice / product.RetailPrice * 100
				End If
				discount.DiscountedPrice = product.OurPrice
				discount.DiscountAmount = product.RetailPrice - product.OurPrice

			End If
			product.OurPrice = discount.DiscountedPrice
			product.YouSavePrice = discount.DiscountAmount

			'calc the percent saved
			'this is calculated as the overall savings between the
			'retail price, and the final discounted price
						Dim dSavingsRate As Decimal = 1
						If product.RetailPrice <> 0 Then
							dSavingsRate = discount.DiscountedPrice / product.RetailPrice
						End If
			Dim savingsPercent As Decimal = 1 - (dSavingsRate)

			product.YouSavePercent = Math.Round(savingsPercent, 2)


			Return discount
		End Function

		Public Shared Function GetBundleByProduct(ByVal productID As Integer) As List(Of BundleItem)
			Return BundleItem.GetByProductID(productID)
		End Function

		Public Function ProductHasDiscount(ByVal productID As Integer) As Boolean
			Return Not GetProductDiscount(productID) Is Nothing
		End Function
		Public Function ProductHasDiscount(ByVal sku As String) As Boolean
			Return Not GetProductDiscount(sku) Is Nothing
		End Function

		Public Shared Function GetCrossSells(ByVal productID As Integer) As ProductCollection


			Dim crossList As ProductCollection = New ProductCollection()
			Dim rdr As IDataReader = ProductCrossSells.CreateQuery().AddWhere("fromProductID", productID).ExecuteReader()
			crossList.Load(rdr)
			rdr.Close()
			Return crossList
		End Function

		Public Shared Sub DeleteBundle(ByVal bundleID As Integer)
			'delete the items first
			Dim q As Query = New Query("CSK_Promo_Product_Bundle_Map")
			q.QueryType = QueryType.Delete
			q.AddWhere("bundleID", bundleID)
			q.Execute()

			'then the bundle
			q = New Query(Bundle.GetTableSchema())
			q.QueryType = QueryType.Delete
			q.AddWhere("bundleID", bundleID)
			q.Execute()


		End Sub
	End Class


End Namespace
