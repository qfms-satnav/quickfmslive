Imports System
Imports System.IO
Imports System.Xml
Imports System.Text
Imports System.Web.UI
Imports System.Security.Cryptography
Imports Microsoft.VisualBasic.Strings
Imports Microsoft.VisualBasic

Public Module Encryption

    Public Function EncryptText(ByVal strText As String) As String
        Return Encrypt(strText, "&%#@?,:*")
    End Function

    'Decrypt the text 
    Public Function DecryptText(ByVal strText As String) As String
        Return Decrypt(strText.Replace("_", "+"), "&%#@?,:*")
    End Function

    'The function used to encrypt the text
    Public Function Encrypt(ByVal strText As String, ByVal strEncrKey As String) As String
        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}

        Try
            byKey = System.Text.Encoding.UTF8.GetBytes(Strings.Left(strEncrKey, 8))

            Dim des As New DESCryptoServiceProvider
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes(strText)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return (Convert.ToBase64String(ms.ToArray).Replace("+", "_"))

        Catch ex As Exception
            Return ex.Message
        End Try

    End Function

    'The function used to decrypt the text
    Public Function Decrypt(ByVal strText As String, ByVal sDecrKey _
               As String) As String
        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim inputByteArray(strText.Length) As Byte

        Try
            byKey = System.Text.Encoding.UTF8.GetBytes(Strings.Left(sDecrKey, 8))
            Dim des As New DESCryptoServiceProvider
            inputByteArray = Convert.FromBase64String(strText)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)

            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8

            Return encoding.GetString(ms.ToArray())

        Catch ex As Exception
            Return ex.Message
        End Try

    End Function


    Public Function EncryptString(ByVal stringToEncrypt As String) As String
        Dim sEncryptionKey As String = "!#$a54?3"
        Dim key As Byte() = {}
        Dim IV As Byte() = {10, 20, 30, 40, 50, 60, _
        70, 80}
        Dim inputByteArray As Byte()
        'Convert.ToByte(stringToEncrypt.Length) 
        Try
            key = Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8))
            Dim des As New DESCryptoServiceProvider
            inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch ex As System.Exception
            Return ex.Message
        End Try
    End Function

    Public Function DecryptString(ByVal stringToDecrypt As String) As String
        Dim sEncryptionKey As String = "!#$a54?3"
        Dim key As Byte() = {}
        Dim IV As Byte() = {10, 20, 30, 40, 50, 60, _
        70, 80}
        'Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF} 
        stringToDecrypt = stringToDecrypt.Replace(" "c, "+"c)
        Dim inputByteArray As Byte() = New Byte(stringToDecrypt.Length - 1) {}
        Try
            key = encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8))
            Dim des As New DESCryptoServiceProvider
            inputByteArray = Convert.FromBase64String(stringToDecrypt)

            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()

            Dim encoding1 As Encoding = encoding1.UTF8
            Return encoding1.GetString(ms.ToArray())
        Catch ex As System.Exception
            Return ex.Message
        End Try
    End Function
End Module
