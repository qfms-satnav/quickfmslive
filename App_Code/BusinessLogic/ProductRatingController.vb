#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Common
Imports System.Collections.Generic
Imports SubSonic

''' <summary>
''' Controller for business logic relating to Products
''' </summary>
Public Class ProductRatingController
	''' <summary>
	''' Adds a rating to a product for a given user and resets the product's rating stats
	''' </summary>
	''' <param name="userName"></param>
	''' <param name="productID"></param>
	''' <param name="rating"></param>
	Private Sub New()
	End Sub
	Public Shared Sub AddUserRating(ByVal userName As String, ByVal productID As Integer, ByVal rating As Integer)

		SPs.StoreProductAddRating(productID, rating, userName).Execute()

	End Sub

	''' <summary>
	''' Gets the user's rating for a products
	''' </summary>
	''' <param name="userName"></param>
	''' <param name="productID"></param>
	''' <returns></returns>
	Public Shared Function GetUserRating(ByVal userName As String, ByVal productID As Integer) As Integer
		Dim iOut As Integer = -1
		Dim rating As ProductRating = New ProductRating()
		rating.UserName = Utility.GetUserName()
		rating.ProductID = productID
		Dim rdr As IDataReader = ProductRating.Find(rating)
		Dim coll As ProductRatingCollection = New ProductRatingCollection()
		coll.Load(rdr)
		rdr.Close()
		If coll.Count > 0 Then
			iOut = coll(0).Rating
		End If
		Return iOut

	End Function

End Class
