#Region "dCPL Version 1.1.1"
'
'The contents of this file are subject to the dashCommerce Public License
'Version 1.1.1 (the "License"); you may not use this file except in
'compliance with the License. You may obtain a copy of the License at
'http://www.dashcommerce.org

'Software distributed under the License is distributed on an "AS IS"
'basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
'License for the specific language governing rights and limitations
'under the License.

'The Original Code is dashCommerce.

'The Initial Developer of the Original Code is Mettle Systems LLC.
'Portions created by Mettle Systems LLC are Copyright (C) 2007. All Rights Reserved.
'
#End Region


Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Commerce.Promotions
Imports System.Data.Common
Imports Commerce.Common
Imports SubSonic
Imports System.Collections

''' <summary>
''' Business logic for products
''' </summary>
Public Class ProductController

    ''' <summary>
    ''' Returns all products
    ''' </summary>
    ''' <returns></returns>
    Private Sub New()
    End Sub
    Public Shared Function GetAll() As ProductCollection
        Dim list As ProductCollection = New ProductCollection().Load()
        Return list
    End Function
    
    ''' <summary>
    ''' Sets a default image for a product
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <param name="imageFile"></param>
    Public Shared Sub SetProductDefaultImage(ByVal productID As Integer, ByVal imageFile As String)
        Dim q As Query = New Query(Product.GetTableSchema())
        q.AddWhere("productID", productID)
        q.AddUpdateSetting("defaultImage", imageFile)
        q.Execute()

    End Sub

    ''' <summary>
    ''' Returns products for a category
    ''' </summary>
    ''' <param name="categoryID">Value for the @categoryID parameter</param>
    ''' <returns>ProductCollection</returns>	
    Public Shared Function GetByCategoryID(ByVal categoryID As Integer) As IDataReader
        Return SPs.StoreProductGetByCategoryID(categoryID).GetReader()
    End Function
    ''' <summary>
    ''' Returns products for a category
    ''' </summary>
    ''' <param name="categoryID">Value for the @categoryID parameter</param>
    ''' <returns>ProductCollection</returns>	
    Public Shared Function GetByCategoryID_DataSet(ByVal categoryID As Integer) As DataSet
        Return SPs.StoreProductGetByCategoryID(categoryID).GetDataSet()
    End Function
    ''' <summary>
    ''' Returns products for a category
    ''' </summary>
    ''' <param name="categoryID">Value for the @categoryID parameter</param>
    ''' <returns>ProductCollection</returns>	
    Public Shared Function GetTop8ByCategoryID(ByVal categoryID As Integer) As IDataReader
        Return SPs.StoreProductGetTop8ByCategoryID(categoryID).GetReader()
    End Function

    ''' <summary>
    ''' Returns products for a category
    ''' </summary>
    ''' <param name="categoryID">Value for the @categoryID parameter</param>
    ''' <returns>ProductCollection</returns>	
    Public Shared Function GetByTypeID(ByVal typeID As Integer) As IDataReader
        Return SPs.StoreProductGetByTypeID(typeID).GetReader()
    End Function

    ''' <summary>
    ''' Returns all products for a manufacturer
    ''' </summary>
    ''' <param name="categoryID"></param>
    ''' <param name="manufacturerID"></param>
    ''' <returns></returns>
    Public Shared Function GetByManufacturerID(ByVal categoryID As Integer, ByVal manufacturerID As Integer) As IDataReader

        Return SPs.StoreProductGetByManufacturerID(manufacturerID, categoryID).GetReader()

    End Function

    ''' <summary>
    ''' Returns products by price range/category
    ''' </summary>
    ''' <param name="categoryID"></param>
    ''' <param name="priceLow"></param>
    ''' <param name="priceHigh"></param>
    ''' <returns></returns>
    Public Shared Function GetByPriceRange(ByVal categoryID As Integer, ByVal priceLow As Decimal, ByVal priceHigh As Decimal) As IDataReader
        Return SPs.StoreProductGetByPriceRange(categoryID, priceLow, priceHigh).GetReader()
    End Function

    ''' <summary>
    ''' Returns reviews by product author
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <param name="author"></param>
    ''' <returns></returns>
    Public Shared Function GetByProductAndAuthor(ByVal productID As Integer, ByVal author As String) As ProductReviewCollection

        Return New ProductReviewCollection().Where("productID", productID).Where("Authorname", author).Load()
    End Function

    ''' <summary>
    ''' Returns text descriptors for a given product
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <returns></returns>
    Public Shared Function GetDescriptors(ByVal productID As Integer) As ProductDescriptorCollection
        Dim list As ProductDescriptorCollection = New ProductDescriptorCollection().Where("productID", productID).OrderByAsc("listOrder").Load()
        Return list
    End Function
    ''' <summary>
    ''' Updates the XML attributes for a given product
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <param name="atts"></param>
    Public Shared Sub UpdateProductAttributes(ByVal productID As Integer, ByVal atts As Commerce.Common.Attributes)
        Dim prod As Commerce.Common.Product = New Commerce.Common.Product(productID)
        prod.Attributes = atts
        prod.Save(Utility.GetUserName())
    End Sub

    ''' <summary>
    ''' Removes a product from a category
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <param name="categoryID"></param>
    Public Shared Sub RemoveFromCategory(ByVal productID As Integer, ByVal categoryID As Integer)
        Utility.DeleteManyToManyRecord("CSK_Store_Product_Category_Map", "productID", "categoryID", productID, categoryID)
    End Sub

    ''' <summary>
    ''' Adds a product to a category
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <param name="categoryID"></param>
    Public Shared Sub AddToCategory(ByVal productID As Integer, ByVal categoryID As Integer)
        Utility.SaveManyToManyRecord("CSK_Store_Product_Category_Map", "productID", "categoryID", productID, categoryID)
    End Sub

    ''' <summary>
    ''' Retrieves all images for a given product
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <returns></returns>
    Public Shared Function GetImages(ByVal productID As Integer) As ImageCollection
        Dim list As ImageCollection = New ImageCollection().Where("productID", productID).OrderByAsc("listOrder").Load()
        Return list
    End Function

    ''' <summary>
    ''' Gets a product based on ID
    ''' </summary>
    ''' <param name="productID"></param>
    ''' <returns></returns>
    Public Shared Function GetProduct(ByVal productID As Integer) As Product

        'load up the product
        Dim product As Product = product.FetchByID(productID)

        'set the image
        'Dim q As Query = New Query(Commerce.Common.Image.GetTableSchema())
        'q.AddWhere("productID", productID)
        'q.Top = "1"
        'q.SelectList = "imageFile"

        'Dim imgFile As String = q.ExecuteScalar().ToString()
        product.ImageFile = "" 'imgFile
        Return product

    End Function

    ''' <summary>
    ''' Loads a product using a passed-in dataset
    ''' </summary>
    ''' <param name="product"></param>
    ''' <param name="ds"></param>
    Private Shared Sub LoadByDataSet(ByVal product As Product, ByVal ds As DataSet)

        'load up the product
        If ds.Tables(0).Rows.Count > 0 Then
            product.Load(ds.Tables(0).Rows(0))
            product.ShippingEstimate = ds.Tables(0).Rows(0)("shippingEstimate").ToString()
        End If

        ' and each of the child collections
        'load the collections
        product.Images = New ImageCollection()
        product.Images.Load(ds.Tables(1))

        If product.Images.Count > 0 Then
            product.ImageFile = product.Images(0).ImageFile
        End If

        'reviews
        product.Reviews = New ProductReviewCollection()
        product.Reviews.Load(ds.Tables(2))


        'descriptors
        product.Descriptors = New ProductDescriptorCollection()
        product.Descriptors.Load(ds.Tables(3))


    End Sub

    ''' <summary>
    ''' Gets a product and all of it's subcollections (images, promos, etc)
    ''' </summary>
    ''' <param name="sku"></param>
    ''' <returns></returns>
    Public Shared Function GetProductDeep(ByVal sku As String) As Commerce.Common.Product
        'load up the product using a multi-return DataSte
        Dim productID As Integer = 0
        Dim q As Query = New Query(Product.GetTableSchema())
        q.AddWhere("sku", sku)
        q.SelectList = "productID"
        productID = CInt(Fix(q.ExecuteScalar()))

        Return GetProductDeep(productID)
    End Function
    ''' <summary>
    ''' Gets a product and all of it's subcollections (images, promos, etc)
    ''' </summary>
    Public Shared Function GetProductDeepByGUID(ByVal productGUID As Guid) As Commerce.Common.Product
        'load up the product using a multi-return DataSte
        Dim productID As Integer = 0
        Dim q As Query = New Query(Product.GetTableSchema())

        q.AddWhere("productGUID", productGUID)
        q.SelectList = "productID"

        productID = CInt(Fix(q.ExecuteScalar()))
        Return GetProductDeep(productID)
    End Function

    ''' <summary>
    ''' Returns the data needed to popluated the ItemJustAdded page
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetPostAddPage() As DataSet
        Return SPs.StoreProductGetPostAddMulti(Utility.GetUserName()).GetDataSet()

    End Function

    ''' <summary>
    ''' Gets a product and all of it's subcollections (images, promos, etc)
    ''' </summary>
    Public Shared Function GetProductDeep(ByVal productID As Integer) As Commerce.Common.Product
        'load up the product using a multi-return DataSet
        'for performance, queue up the 4 SQL calls into one
        Dim sql As String = ""

        'Product Main Info
        Dim q As Query = New Query("vwProduct")
        q.AddWhere("productID", productID)

        'append
        sql &= q.GetSql() + Constants.vbCrLf

        'Images
        q = New Query(Commerce.Common.Image.GetTableSchema())
        q.AddWhere("productID", productID)
        'q.AddBetweenAnd()


        q.OrderBy = OrderBy.Asc("listOrder")

        'append
        sql &= q.GetSql() & Constants.vbCrLf

        'Reviews
        q = New Query(ProductReview.GetTableSchema())
        q.AddWhere("productID", productID)
        q.AddWhere("isApproved", 1)

        'append
        sql &= q.GetSql() & Constants.vbCrLf


        'Descriptors
        q = New Query(ProductDescriptor.GetTableSchema())
        q.AddWhere("productID", productID)
        q.OrderBy = OrderBy.Asc("listOrder")


        'append
        sql &= q.GetSql() & Constants.vbCrLf

        Dim cmd As QueryCommand = New QueryCommand(sql)
        cmd.AddParameter("@productID", productID, DbType.Int32)
        cmd.AddParameter("@isApproved", True, DbType.Boolean)
        Dim ds As DataSet = DataService.GetDataSet(cmd)


        Dim product As Product = New Product()

        LoadByDataSet(product, ds)
        Return product
    End Function
    ''' <summary>
    ''' This is a semi-smart query that will sort your returns based on word count 
    ''' </summary>
    ''' <param name="descriptionLength">How long the resulting product description will be</param>
    ''' <param name="AllWords">Whether to use all the words in the query, or just the blob</param>

    ''' <returns>System.Data.IDataReader</returns>	
    Public Shared Function RunSmartSearch(ByVal descriptionLength As Integer, ByVal query As String) As IDataReader

        Dim AllWords As Boolean = True
        'thanks to Moody for this routine
        'Define the parameters
        Dim howManyWords As Integer = 5 'words which is allowed to take from search text box you can enter to 5 words
        Dim queryparam As String() = New String(howManyWords - 1) {} 'array to containe the words entered in search text box
        Dim wordSeparators As Char() = New Char() {","c, ";"c, ","c, "."c, "!"c, ","c, "?"c, "-"c, " "c} 'char elemnets is used when to seperat the entered query
        Dim words As String() = query.Split(wordSeparators, StringSplitOptions.RemoveEmptyEntries)
        Dim index As Integer = 1
        Dim i As Integer = 0
        Do While i <= words.GetUpperBound(0) AndAlso index <= howManyWords

            'check to see if you entered more than one words
            If words(i).Length > 2 Then
                queryparam(i) = words(i)
            End If
            index += 1
            i += 1
        Loop
        Dim bitNum As Integer
        If AllWords = True Then
            bitNum = 1
        Else
            bitNum = 0
        End If

        Return SPs.StoreProductSmartSearch(descriptionLength, AllWords, queryparam(0), queryparam(1), queryparam(2), queryparam(3), queryparam(4)).GetReader()


    End Function
    ''' <summary>
    ''' Removes all information from the DB about this product
    ''' including all reviews, review feedback, cross-sells, etc.
    ''' Does NOT remove any products from checked-out orders, however
    ''' all deleted products will be removed from non-checked out carts
    ''' </summary>
    ''' <param name="productID"></param>
    Public Shared Shadows Sub DeletePermanent(ByVal productID As Integer)

        'change as needed
        'currently just removes the product from the DB
        'load the default db from the base class

        SPs.StoreProductDeletePermanent(productID).Execute()

    End Sub

    ''' <summary>
    ''' Gets the most popular products for use on the 404 and basket pages
    ''' </summary>
    ''' <returns></returns>
    Public Shared Function GetMostPopular() As IDataReader

        Return SPs.StoreProductGetMostPopular().GetReader()

    End Function

    Public Shared Function GetProductsByManufacturer(ByVal manufacturerid As Integer) As IDataReader
        Return SPs.GetProductsByManufacturer(manufacturerid).GetReader
    End Function

    Public Shared Function GetProductsByAdvancedSearch(ByVal sku As String, ByVal productname As String, ByVal keywords As String, ByVal LowerPrice As Decimal, ByVal UpperPrice As Decimal) As DataSet
        Return SPs.GetProductsByAdvancedSearch(sku, productname, keywords, LowerPrice, UpperPrice).GetDataSet()
    End Function

    Public Shared Function GerDealer_LocatorByCity() As DataSet
        Return SPs.GerDealer_LocatorByCity().GetDataSet
    End Function

    Public Shared Function GetStore_LocatorByCityasReader() As IDataReader
        Return SPs.GerStore_LocatorByCity().GetReader
    End Function

    Public Shared Function GerStore_LocatorByCity() As DataSet
        Return SPs.GerStore_LocatorByCity().GetDataSet
    End Function
    Public Shared Function GetStore_LocatorCitiesByStateasReader(ByVal StateName As String) As IDataReader
        Return SPs.GerStore_LocatorCitiesByState(StateName).GetReader
    End Function
    Public Shared Function GerStore_LocatorCitiesByState(ByVal StateName As String) As DataSet
        Return SPs.GerStore_LocatorCitiesByState(StateName).GetDataSet
    End Function
    Public Shared Function GetAllDealers() As IDataReader
        Return SPs.GMR_GetAllDealers().GetReader
    End Function
    Public Shared Function GetDealer_LocatorByState(ByVal State_Name As String) As DataTable
        Return SPs.GMR_GetDealer_LocatorByState(State_Name).GetDataSet.Tables(0)
    End Function
    Public Shared Function GetDealer_LocatorByCity(ByVal CityName As String) As DataSet
        Return SPs.GMR_GetDealer_LocatorByCity(CityName).GetDataSet
    End Function
    Public Shared Function GetDealer_LocatorByStateCity(ByVal StateName As String, ByVal CityName As String) As DataSet
        Return SPs.GMR_GetDealer_LocatorByStateCity(StateName, CityName).GetDataSet
    End Function

    Public Shared Function Getstore_LocatorByState(ByVal State_Name As String) As DataTable
        Return SPs.GMR_GetStore_LocatorByState(State_Name).GetDataSet().Tables(0)
    End Function
    Public Shared Function Getstore_LocatorByCity(ByVal CityName As String) As DataSet
        Return SPs.GMR_GetStore_LocatorByCity(CityName).GetDataSet
    End Function
    Public Shared Function Getstore_LocatorByStateCity(ByVal StateName As String, ByVal CityName As String) As DataSet
        Return SPs.GMR_GetStore_LocatorByStateCity(StateName, CityName).GetDataSet
    End Function

    Public Shared Function GetAllProducts() As IDataReader
        Return SPs.GetAllProducts().GetReader
    End Function
    'GMR_GetDealer_LocatorByState

    'GetProductDeepProductName
    Public Shared Function GetProductDeepProductName(ByVal ProductName As String) As DataSet
        ProductName = ProductName.Replace("_", " ")
        Return SPs.spGetProductDeepProductName(ProductName).GetDataSet

    End Function
    Public Shared Function GetProductDeepProductId(ByVal ProductId As Integer) As DataSet
        Return SPs.spGetProductDeepProductId(ProductId).GetDataSet
    End Function

    Public Shared Function GetProductGetCrossSellingProductsByProductId(ByVal ProductId As Int64) As ProductCollection
        Dim rdr As IDataReader = SPs.StoreProductGetRelatedProductsByProductId(ProductId).GetReader()
        Dim list As ProductCollection = New ProductCollection()
        list.Load(rdr)
        rdr.Close()
        Return list
    End Function

    Public Shared Function GetAllPartners() As IDataReader

        Return SPs.spGetPartners().GetReader()

    End Function
    Public Shared Function GetAllStores() As IDataReader
        Return SPs.spGetStores().GetReader()
    End Function
    Public Shared Function GetAllStoreLocators() As IDataReader
        Return SPs.spGetAllStoreLocators().GetReader()
    End Function

    Public Shared Function GetProductSEOTagsById(ByVal ProductId As String) As ProductCollection
        Dim rdr As IDataReader = SPs.GetProductSEOTags(ProductId).GetReader()
        Dim list As ProductCollection = New ProductCollection()
        list.Load(rdr)
        rdr.Close()
        Return list
    End Function
    Public Shared Function GetAllProductTypes() As IDataReader
        Return SPs.GetAllProductTypes().GetReader
    End Function
End Class


