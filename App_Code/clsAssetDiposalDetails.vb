Imports Microsoft.VisualBasic

Public Class clsAssetDiposalDetails
    Private mAUR_FIRST_NAME As String

    Public Property AUR_FIRST_NAME() As String
        Get
            Return mAUR_FIRST_NAME
        End Get
        Set(ByVal value As String)
            mAUR_FIRST_NAME = value
        End Set
    End Property
    Private mAAT_NAME As String

    Public Property AAT_NAME() As String
        Get
            Return mAAT_NAME
        End Get
        Set(ByVal value As String)
            mAAT_NAME = value
        End Set
    End Property
    Private mDREQ_ADMIN_REMARKS As String

    Public Property DREQ_ADMIN_REMARKS() As String
        Get
            Return mDREQ_ADMIN_REMARKS
        End Get
        Set(ByVal value As String)
            mDREQ_ADMIN_REMARKS = value
        End Set
    End Property
    Private mDREQ_ADMIN_DT As String

    Public Property DREQ_ADMIN_DT() As String
        Get
            Return mDREQ_ADMIN_DT
        End Get
        Set(ByVal value As String)
            mDREQ_ADMIN_DT = value
        End Set
    End Property
    Private mAST_DISPOSE As String

    Public Property AST_DISPOSE() As String
        Get
            Return mAST_DISPOSE
        End Get
        Set(ByVal value As String)
            mAST_DISPOSE = value
        End Set
    End Property
    Private mAST_SALAVAGE_VALUE As String

    Public Property AST_SALAVAGE_VALUE() As String
        Get
            Return mAST_SALAVAGE_VALUE
        End Get
        Set(ByVal value As String)
            mAST_SALAVAGE_VALUE = value
        End Set
    End Property
    Private mDREQ_STATUS As String

    Public Property DREQ_STATUS() As String
        Get
            Return mDREQ_STATUS
        End Get
        Set(ByVal value As String)
            mDREQ_STATUS = value
        End Set
    End Property
    Private mDREQ_REQUESTED_REMARKS As String

    Public Property DREQ_REQUESTED_REMARKS() As String
        Get
            Return mDREQ_REQUESTED_REMARKS
        End Get
        Set(ByVal value As String)
            mDREQ_REQUESTED_REMARKS = value
        End Set
    End Property
    Private mDREQ_REQUESITION_DT As String

    Public Property DREQ_REQUESITION_DT() As String
        Get
            Return mDREQ_REQUESITION_DT
        End Get
        Set(ByVal value As String)
            mDREQ_REQUESITION_DT = value
        End Set
    End Property
End Class
