Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Data.SqlClient
Imports System.Security.Cryptography
Imports System.Web.HttpApplicationState
Imports System.Web.UI.WebControls

Imports Microsoft.VisualBasic
Imports System
Imports System.Web.UI

Public Module CommonModules

#Region " Common Datatable objects "

    Public dtCountry As DataTable
    Public dtregion As DataTable
    Public dtState As DataTable
    Public dtCity As DataTable
    Public dtPremise As DataTable
    Public dtLocation As DataTable
    Public dtGSC As DataTable
    Public dtFloor As DataTable
    Public dtTower As DataTable
    Public dtWing As DataTable
    Public dtProcess As DataTable
    Public dtNOProcess As DataTable
    Public dtVDProcess As DataTable
    Public dtSites As DataTable
    Public dtAssets As DataTable
    Public dtInv As DataTable

    Public Sub ClearCommonDatatables()
        dtCountry = Nothing
        dtState = Nothing
        dtregion = Nothing
        dtCity = Nothing
        dtPremise = Nothing
        dtLocation = Nothing
        dtGSC = Nothing
        dtProcess = Nothing
        dtAssets = Nothing
    End Sub
#End Region

#Region " Binding Combos "
    Public Sub BindComboall(ByVal strqry As String, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String)
        If (strqry.Contains("select")) Then
            cboCombo.DataSource = SqlHelper.ExecuteReader(CommandType.Text, strqry)
        Else
            cboCombo.DataSource = SqlHelper.ExecuteReader(CommandType.StoredProcedure, strqry)
        End If

        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--All--")
    End Sub

    Public Sub BindCombo(ByVal strqry As String, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String, ByVal ParamArray commandParameters() As SqlParameter)
        If (strqry.Contains("select")) Then
            cboCombo.DataSource = SqlHelper.ExecuteDataset(CommandType.Text, strqry)
        Else
            cboCombo.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, strqry, commandParameters)
        End If

        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
        'cboCombo.SelectedIndex = 0
    End Sub

    Public Sub BindCombo(ByVal strqry As String, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String)
        If (strqry.Contains("select") Or strqry.Contains("SELECT")) Then
            cboCombo.DataSource = SqlHelper.ExecuteDataset(CommandType.Text, strqry)
        Else
            cboCombo.DataSource = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, strqry)
        End If

        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
        'cboCombo.SelectedIndex = 0
    End Sub

    Public Sub BindCombo(ByVal ds As DataSet, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String)
        cboCombo.Items.Clear()
        cboCombo.DataSource = ds
        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
        cboCombo.SelectedIndex = 0
    End Sub

    Public Sub BindCombo(ByVal dv As DataView, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String)
        cboCombo.Items.Clear()
        cboCombo.DataSource = dv
        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
        cboCombo.SelectedIndex = 0
    End Sub

    Public Sub BindCombo(ByVal dt As DataTable, ByRef cboCombo As DropDownList, ByVal txtField As String, ByVal valField As String)
        cboCombo.Items.Clear()
        cboCombo.DataSource = dt
        cboCombo.DataTextField = txtField
        cboCombo.DataValueField = valField
        cboCombo.DataBind()
        cboCombo.Items.Insert(0, "--Select--")
        cboCombo.SelectedIndex = 0
    End Sub
#End Region

#Region " Binding ListBox  "

    Public Sub BindList(ByVal strqry As String, ByRef lstBox As ListBox, ByVal txtField As String, ByVal valField As String, ByVal ParamArray params() As SqlParameter)
        lstBox.Items.Clear()
        If (strqry.Contains("select")) Then
            lstBox.DataSource = SqlHelper.ExecuteReader(CommandType.Text, strqry)
        Else
            lstBox.DataSource = SqlHelper.ExecuteReader(CommandType.StoredProcedure, strqry, params)
        End If

        lstBox.DataTextField = txtField
        lstBox.DataValueField = valField
        lstBox.DataBind()
    End Sub

    Public Sub BindList(ByVal strqry As String, ByRef lstBox As ListBox, ByVal txtField As String, ByVal valField As String)
        lstBox.Items.Clear()

        lstBox.DataSource = SqlHelper.ExecuteReader(CommandType.Text, strqry)
        lstBox.DataTextField = txtField
        lstBox.DataValueField = valField
        lstBox.DataBind()
    End Sub

    Public Sub BindList(ByVal ds As DataSet, ByRef lstBox As ListBox, ByVal txtField As String, ByVal valField As String)
        lstBox.Items.Clear()

        lstBox.DataSource = ds
        lstBox.DataTextField = txtField
        lstBox.DataValueField = valField
        lstBox.DataBind()
    End Sub

    Public Sub BindList(ByVal dt As DataTable, ByRef lstBox As ListBox, ByVal txtField As String, ByVal valField As String)
        lstBox.Items.Clear()

        lstBox.DataSource = dt
        lstBox.DataTextField = txtField
        lstBox.DataValueField = valField
        lstBox.DataBind()
    End Sub

#End Region

#Region " Clearing Controls "

    Public Sub ClearCombo(ByVal ParamArray cboSrc() As DropDownList)
        Dim iLoop As DropDownList
        For Each iLoop In cboSrc
            iLoop.Items.Clear()
            iLoop.Items.Insert(0, "--Select--")
            iLoop.SelectedIndex = 0
        Next
    End Sub

    Public Sub ClearText(ByVal ParamArray txtSrc() As TextBox)
        Dim iLoop As TextBox
        For Each iLoop In txtSrc
            iLoop.Text = ""
        Next
    End Sub
#End Region

#Region "  Encrypt and Decrypt "
    Public Function EncryptText(ByVal strText As String) As String
        Return Encrypt(strText, "&%#@?,:*")
    End Function

    'Decrypt the text 
    Public Function DecryptText(ByVal strText As String) As String
        Return Decrypt(strText, "&%#@?,:*")
    End Function

    'The function used to encrypt the text
    Private Function Encrypt(ByVal strText As String, ByVal strEncrKey As String) As String
        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}

        Try
            byKey = System.Text.Encoding.UTF8.GetBytes(Left(strEncrKey, 8))

            Dim des As New DESCryptoServiceProvider
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes(strText)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write)

            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())

        Catch ex As Exception
            Return ex.Message

        End Try
    End Function

    'The function used to decrypt the text
    Private Function Decrypt(ByVal strText As String, ByVal sDecrKey As String) As String

        Dim byKey() As Byte = {}
        Dim IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
        Dim inputByteArray(strText.Length) As Byte

        Try

            byKey = System.Text.Encoding.UTF8.GetBytes(Left(sDecrKey, 8))

            Dim des As New DESCryptoServiceProvider
            inputByteArray = Convert.FromBase64String(strText)

            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write)

            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()

            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())

        Catch ex As Exception

            Return ex.Message

        End Try
    End Function
#End Region
    Public Sub PopUpMessageNormal(ByVal strText As String, ByVal Pg As Page)
        Dim strScript As String
        strScript = "<script language='javascript'>alert('" & strText & "')</script>"
        Pg.ClientScript.RegisterStartupScript(Pg.GetType(), "strScript", strScript)
        'Return
    End Sub

    Public Sub PopUpMessage(ByVal strText As String, ByVal Pg As Page)
        ScriptManager.RegisterStartupScript(Pg, Pg.GetType(), "ajaxscript", "alert('" & strText & "')", True)
        'Return
    End Sub
End Module