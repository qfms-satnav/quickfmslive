Imports Microsoft.VisualBasic
Imports Amantra.RequisitionDALN
Imports Amantra.RequisitionDTON
Imports System.Collections.Generic
Imports System.Configuration.ConfigurationManager
Imports System.Data.SqlClient
Imports System.data
Namespace Amantra.RequisitionBLLN

    Public Class RequisitionBLL

#Region "[ BLL and DTO INSTANCE CREATION ]"
        '' ----------------------------------------------------------------------------------------------- 
        ''' <summary> 
        ''' <Author>Srinivasulu M</Author> 
        ''' </summary> 
        Dim reqdal As New RequsitionDAL()
        Dim reqDTO As IList(Of RequisitionDTO) = New List(Of RequisitionDTO)()
#End Region

#Region "[Get Requisition Details]"
        Public Function GetRequisitionData(ByVal Req_ID As String, ByVal Sta_id As Integer, ByVal usr_id As String) As IList(Of RequisitionDTO)
            reqDTO = reqdal.GetRequisitionData(Req_ID, Sta_id, usr_id)
            Return reqDTO
        End Function
#End Region

#Region "[Get Requisition Details]"
        Public Function GetRequIds(ByVal Sta_id As Integer) As IList(Of RequisitionDTO)
            reqDTO = reqdal.GetRequisitionIDs(Sta_id)
            Return reqDTO
        End Function
#End Region

#Region "[Update Request Status]"
        Public Function updateRequestIdStatus(ByVal rDto As RequisitionDTO) As Integer
            Dim iStatus = reqdal.updateRequestStatus(rDto)
            Return iStatus
        End Function
        Public Function updateBFMRequestIdStatus(ByVal rDto As RequisitionDTO) As Integer
            Dim iStatus = reqdal.updateBFMRequestStatus(rDto)
            Return iStatus
        End Function
#End Region

#Region "[Get BFM Requisition Details]"
        Public Function GetBFMRequisitionData(ByVal Req_ID As String, ByVal Sta_id As Integer, ByVal usr_id As String) As IList(Of RequisitionDTO)
            reqDTO = reqdal.GetBFMRequisitionData(Req_ID, Sta_id, usr_id)
            Return reqDTO
        End Function
#End Region

#Region "[Get RM Approval Details]"
        Public Function insertRMReqDetails(ByVal strReqID As String, ByVal intWST_NUM As Integer, ByVal intCAB_NUM As Integer, ByVal intCUB_NUM As Integer, ByVal intWST_ALLOC As Integer, ByVal intCAB_ALLOC As Integer, ByVal intCUB_ALLOC As Integer, ByVal dtFROM_DATE As Date, ByVal dtTO_DATE As Date, ByVal intUPDATED_BY As String, ByVal strSMS_REM As String, ByVal strApprovedby As String) As Boolean
           
            Dim sp1 As New SqlParameter("@vc_reqId", SqlDbType.NVarChar, 50)
            sp1.Value = strReqID
            Dim sp2 As New SqlParameter("@vc_wst", SqlDbType.Int)
            sp2.Value = intWST_NUM
            Dim sp3 As New SqlParameter("@vc_cab", SqlDbType.Int)
            sp3.Value = intCAB_NUM
            Dim sp4 As New SqlParameter("@vc_cub", SqlDbType.Int)
            sp4.Value = intCUB_NUM
            Dim sp5 As New SqlParameter("@vc_wst_alloc", SqlDbType.Int)
            sp5.Value = intWST_ALLOC
            Dim sp6 As New SqlParameter("@vc_cab_alloc", SqlDbType.Int)
            sp6.Value = intCAB_ALLOC
            Dim sp7 As New SqlParameter("@vc_cub_alloc", SqlDbType.Int)
            sp7.Value = intCUB_ALLOC
            Dim sp8 As New SqlParameter("@vc_fromdate", SqlDbType.DateTime)
            sp8.Value = dtFROM_DATE
            Dim sp9 As New SqlParameter("@vc_todate", SqlDbType.DateTime)
            sp9.Value = dtTO_DATE
            Dim sp10 As New SqlParameter("@vc_user", SqlDbType.NVarChar, 50)
            sp10.Value = intUPDATED_BY
            Dim sp11 As New SqlParameter("@vc_rem", SqlDbType.NVarChar, 500)
            sp11.Value = strSMS_REM
            Dim sp12 As New SqlParameter("@vc_apporved_by", SqlDbType.NVarChar, 50)
            sp12.Value = strApprovedby
            'SqlHelper.ExecuteNonQuery(Data.CommandType.StoredProcedure, Session("TENANT") & "."  & "usp_insertSpaceApporval", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11, sp12)
            '   Dim intCount As Integer = SqlHelper.ExecuteNonQuery(Data.CommandType.StoredProcedure, Session("TENANT") & "."  & "usp_insertSpaceApporval", sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11, sp12)

            Dim parms As SqlParameter() = {sp1, sp2, sp3, sp4, sp5, sp6, sp7, sp8, sp9, sp10, sp11, sp12}
            Dim intCount As Integer = SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "usp_insertSpaceApporval", parms)


            If intCount = 1 Then
                Return True
            Else
                Return False
            End If
            'Return True
        End Function
#End Region

        Private Function Data() As Object
            Throw New System.NotImplementedException
        End Function

    End Class
End Namespace
