Imports Microsoft.VisualBasic

Public Class clsAssetsLocationReport
    Private mPRODUCTid As String

    Public Property PRODUCTid() As String
        Get
            Return mPRODUCTid
        End Get
        Set(ByVal value As String)
            mPRODUCTid = value
        End Set
    End Property
    Private mPRODUCTNAME As String

    Public Property PRODUCTNAME() As String
        Get
            Return mPRODUCTNAME
        End Get
        Set(ByVal value As String)
            mPRODUCTNAME = value
        End Set
    End Property
    Private mLCM_NAME As String

    Public Property LCM_NAME() As String
        Get
            Return mLCM_NAME
        End Get
        Set(ByVal value As String)
            mLCM_NAME = value
        End Set
    End Property
    Private mAssetsCount As String

    Public Property AssetsCount() As String
        Get
            Return mAssetsCount
        End Get
        Set(ByVal value As String)
            mAssetsCount = value
        End Set
    End Property
    Private mLCM_CODE As String

    Public Property LCM_CODE() As String
        Get
            Return mLCM_CODE
        End Get
        Set(ByVal value As String)
            mLCM_CODE = value
        End Set
    End Property
End Class
