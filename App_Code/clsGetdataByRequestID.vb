Imports Microsoft.VisualBasic

Public Class clsGetdataByRequestID
    Private mAID_REQ_ID As String

    Public Property AID_REQ_ID() As String
        Get
            Return mAID_REQ_ID
        End Get
        Set(ByVal value As String)
            mAID_REQ_ID = value
        End Set
    End Property
    Private mAID_AVLBL_QTY As String

    Public Property AID_AVLBL_QTY() As String
        Get
            Return mAID_AVLBL_QTY
        End Get
        Set(ByVal value As String)
            mAID_AVLBL_QTY = value
        End Set
    End Property
    Private mAID_REQ_QTY As String

    Public Property AID_REQ_QTY() As String
        Get
            Return mAID_REQ_QTY
        End Get
        Set(ByVal value As String)
            mAID_REQ_QTY = value
        End Set
    End Property
    Private mAID_REQ_DT As String

    Public Property AID_REQ_DT() As String
        Get
            Return mAID_REQ_DT
        End Get
        Set(ByVal value As String)
            mAID_REQ_DT = value
        End Set
    End Property
    Private mCRNTQTY As String

    Public Property CRNTQTY() As String
        Get
            Return mCRNTQTY
        End Get
        Set(ByVal value As String)
            mCRNTQTY = value
        End Set
    End Property

End Class
