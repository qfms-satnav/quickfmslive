﻿Imports Microsoft.VisualBasic
Imports System.Web
Imports System.Globalization

Public Class SetOffSetModule
    Implements IHttpModule
    '
    ' TODO: Add constructor logic here
    '
    Public Sub New()
    End Sub

    Public Sub Dispose() Implements IHttpModule.Dispose


    End Sub

    Public Sub Init(context As HttpApplication) Implements IHttpModule.Init
        AddHandler context.BeginRequest, AddressOf context_BeginRequest
        AddHandler context.PostAcquireRequestState, AddressOf context_PostAcquireRequestState
        AddHandler context.PostMapRequestHandler, AddressOf context_PostMapRequestHandler
    End Sub

    Private Sub context_PostMapRequestHandler(sender As Object, e As EventArgs)
        Dim app As HttpApplication = DirectCast(sender, HttpApplication)
        If TypeOf app.Context.Handler Is IReadOnlySessionState OrElse TypeOf app.Context.Handler Is IRequiresSessionState Then
            ' no need to replace the current handler
            Return
        End If
        ' swap the current handler
        app.Context.Handler = New MyHttpHandler(app.Context.Handler)
    End Sub

    Private Sub context_PostAcquireRequestState(sender As Object, e As EventArgs)
        Dim app As HttpApplication = DirectCast(sender, HttpApplication)
        Dim resourceHttpHandler As MyHttpHandler = TryCast(HttpContext.Current.Handler, MyHttpHandler)
        If resourceHttpHandler IsNot Nothing Then
            ' set the original handler back
            HttpContext.Current.Handler = resourceHttpHandler.OriginalHandler
        End If
        'app.Session.Add("hi", "how are u"); session is available
        Dim currentCulture As CultureInfo
        If Not app.Session Is Nothing And Not app.Session("userculture") Is Nothing Then
            currentCulture = New System.Globalization.CultureInfo(app.Session("userculture").ToString())
        Else
            currentCulture = New System.Globalization.CultureInfo("en-IN")
            app.Session("useroffset") = "+05:30"
        End If
        System.Threading.Thread.CurrentThread.CurrentCulture = currentCulture

        System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat = System.Globalization.DateTimeFormatInfo.InvariantInfo
        System.Threading.Thread.CurrentThread.CurrentUICulture = currentCulture
    End Sub

    Private Sub context_BeginRequest(sender As Object, e As EventArgs)
        'We recived a request, so we save the original URL here
        Dim context As HttpContext = DirectCast(sender, HttpApplication).Context
    End Sub
End Class

Public Class MyHttpHandler
    Implements IHttpHandler
    Implements IRequiresSessionState
    Friend ReadOnly OriginalHandler As IHttpHandler

    Public Sub New(originalHandler__1 As IHttpHandler)
        OriginalHandler = originalHandler__1
    End Sub

    Public Sub ProcessRequest(context As HttpContext) Implements IHttpHandler.ProcessRequest
        ' do not worry, ProcessRequest() will not be called, but let's be safe
        Throw New InvalidOperationException("MyHttpHandler cannot process requests.")
    End Sub

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        ' IsReusable must be set to false since class has a member!
        Get
            Return False
        End Get
    End Property
End Class
