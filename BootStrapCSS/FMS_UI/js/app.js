function Nav_toggle(){

    $(".nav_toggle_icon").click(function(){
      $("header").toggleClass("compressed");
      if($("header").hasClass('compressed')==true){
        $(".u-vmenu > ul > li >a[data-option='on']+ul").hide();
        $(".u-vmenu > ul > li >a[data-option='on']").attr("data-option","off");
                
      }
    });

    $(".u-vmenu > ul > li >a[data-option]").click(function(){
      if($("header").hasClass('compressed')==true){
        $("header").removeClass('compressed');
      }
    });
}

 /* cookie initialization */
var cookie_name = "selected_theme";
var cookie_options = { path: '/', expires: 7 };

$(document).ready(function(){

/*>>>THEME PERSONALIZATION<<<*/
  /* Get Cookie */ 
var get_cookie = $.cookie(cookie_name);

if(get_cookie != null) { 
  $("#active-theme").attr({ href: "css/" + get_cookie + ".css"});
  $(".theme_swatches i[rel='"+get_cookie+"']").addClass('active_theme');
}
else{
  $(".theme_swatches i.blue").not($('i.active_theme')).addClass("active_theme");
}

/* theme switcher */
$(".theme_swatches i").click(function() {
var themename = $(this).attr("rel");
$(".theme_swatches i").removeClass("active_theme");
$(".theme_swatches i[rel='"+themename+"']").addClass('active_theme');
$("#active-theme").attr({ href: "css/" + themename +".css"});
$.cookie(cookie_name, themename, cookie_options);
return false;
});
/*>>>THEME PERSONALIZATION ENDS<<<*/

	$('.custom-upload input[type=file]').change(function(){
		$(this).next().find('input').val($(this).val());
	});
     
  

 $(".u-vmenu").vmenuModule({
      Speed: 200,
      autostart: false,
      autohide: true
  });
  Nav_toggle();
  $('.theme_toggle_icon').click(function(){
       $('.theme_swatches').toggleClass('showthis');
  });
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();
     
});



