﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.IO;

public partial class managerApproval : System.Web.UI.Page
{
    SubSonic.StoredProcedure sp;
    private string DBNAME;
    protected void Page_Load(object sender, EventArgs e)
    {
        string SRC_REQST_ID = "";
        int ID = 0;
        DBNAME = "";
        SRC_REQST_ID = Request.QueryString["SRC_REQST_ID"];
        int.TryParse(Request.QueryString["ID"], out ID);
        DBNAME = Request.QueryString["DB"];

        if (DBNAME != null && DBNAME.Length > 0)
        {
            if (SRC_REQST_ID != null && SRC_REQST_ID.Length > 0)
                Getmanagerapproval(DBNAME, SRC_REQST_ID);

            if (ID > 0)
                Rejectupdatedata(DBNAME, ID);
        }
    }
    public void Getmanagerapproval(string DBNAME, string SRC_REQST_ID)
    {
        try
        {
            DataSet ds = new DataSet();
            sp = new SubSonic.StoredProcedure(DBNAME + "." + "MANAGER_APPROVAL");
            sp.Command.Parameters.Add("@SRC_REQST_ID", SRC_REQST_ID, DbType.String);
            ds = sp.GetDataSet();
            string SSA_SRNREQ_ID = "";
            string AUR_ID = "";
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataTable table in ds.Tables)
                    {
                        foreach (DataRow dr in table.Rows)
                        {
                            DataSet ds1 = new DataSet();
                            SSA_SRNREQ_ID = dr["SSA_SRNREQ_ID"].ToString();
                            AUR_ID = dr["emp"].ToString();
                            sp = new SubSonic.StoredProcedure(DBNAME + "." + "SEND_MAIL_EMPLOYEE_ALLOCATION_ACTION_MANAGER");
                            sp.Command.Parameters.Add("@REQID", SSA_SRNREQ_ID, DbType.String);
                            sp.Command.Parameters.Add("@CREATED_BY", AUR_ID, DbType.String);
                            sp.Command.Parameters.Add("@COMPANY_ID", 1, DbType.String);
                            ds1 = sp.GetDataSet();
                            if (ds1.Tables.Count > 0)
                            {
                                Success.Visible = true;
                               
                            }
                        }
                    }
                }
                else
                {
                    Successwarning.Visible = true;
                    
                }
            }
        }
        catch (SqlException)
        {
            Fail.Visible = true;
            throw;
        }
    }

    public void Rejectupdatedata(string DBNAME, int id)
    {
        try
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@SSMA_ID", DbType.Int32);
            param[0].Value = id;

            int SSMA_ID = (int)SqlHelper.ExecuteScalar(CommandType.StoredProcedure, DBNAME + "." + "UPD_SMS_SPACE_MANAGER_APPROVAL_MAP", param);
            if (SSMA_ID == 1)
            {
                Reject.Visible = true;
                
            }
            else
            {
                Rejectwarning.Visible = true;
               
            }
        }
        catch (SqlException)
        {
            Fail.Visible = true;
            throw;
        }

    }
}