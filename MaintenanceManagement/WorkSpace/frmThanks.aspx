<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmThanks.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmThanks" Title="Building Management System" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table id="table2" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="Label4" runat="server" Font-Bold="True" CssClass="clsHead" Width="95%">Status<hr width="70%" align="center" /></asp:Label></td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                align="center" border="0">
                <tr>
                    <td style="width: 11px">
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Status</strong></td>
                    <td style="width: 17px">
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <%-- <tr>
                    <td colspan="3" align="left">
                        &nbsp;</td>
                </tr>--%>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif" style="width: 11px">
                        &nbsp;</td>
                    <td align="left">
                        <asp:Panel ID="pnlmain" runat="server" Width="95%">
                            <br />
                            <table id="table6" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
                                <tr>
                                    <td align="Center">
                                        <asp:Label ID="lbl1" runat="server" CssClass="clslbl1" Font-Bold="True" Font-Names="Verdana" ForeColor="Red" Font-Size="Larger"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 17px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 11px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px; width: 17px;">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
