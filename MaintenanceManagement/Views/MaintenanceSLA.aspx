﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
   <%-- <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>--%>
</head>

<body data-ng-controller="MaintSLAcontroller" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Warranty Expired Assets Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Maintenance SLA</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                             <h3 class="panel-title">Maintenance SLA</h3>
                                   </div>
                                <div class="card">
                            <form role="form" id="form2" name="frmSLA" data-valid-submit="SaveDetails()" novalidate>
                                <div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Country<span style="color: red;">*</span></label>
                                                <div class="col-md-12">
                                                    <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.CNY_NAME.$invalid}">
                                                        <div isteven-multi-select data-input-model="Countrylist" data-output-model="SLAMaster.Country" data-button-label="icon CNY_NAME"
                                                            data-item-label="icon CNY_NAME maker" data-on-item-click="CountryChanged()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                        </div>
                                                        <input type="text" data-ng-model="SLAMaster.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                                        <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">City<span style="color: red;">*</span></label>
                                                <div class="col-md-12">
                                                    <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.CTY_NAME.$invalid}">
                                                        <div isteven-multi-select data-input-model="Citylist" data-output-model="SLAMaster.City" data-button-label="icon CTY_NAME"
                                                            data-item-label="icon CTY_NAME maker" data-on-item-click="CityChanged()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                        </div>
                                                        <input type="text" data-ng-model="SLAMaster.City[0]" name="CTY_NAME" style="display: none" required="" />
                                                        <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.CTY_NAME.$invalid" style="color: red">Please Select City</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Location<span style="color: red;">*</span></label>
                                                <div class="col-md-12">
                                                    <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.LCM_NAME.$invalid}">
                                                        <div isteven-multi-select data-input-model="locationlist" data-output-model="SLAMaster.Location" data-button-label="icon LCM_NAME"
                                                            data-item-label="icon LCM_NAME maker" data-on-item-click="GetSLA()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                        </div>
                                                        <input type="text" data-ng-model="SLAMaster.Location[0]" name="LCM_NAME" style="display: none" required="" />
                                                        <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.LCM_NAME.$invalid" style="color: red">Please Select Location</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12" data-ng-show="SLADetails" style="padding-top: 10px;">
                                        <div class="box">
                                            <div class="box-body">
                                                <table data-ng-table="tableParams" class="table GridStyle ng-table-responsive">
                                                    <tr>
                                                        <th>Status </th>
                                                        <th>Email Escalation</th>
                                                        <th data-ng-repeat="rol in Roles">{{rol.ROL_DESCRIPTION}}</th>
                                                    </tr>
                                                    <tr data-ng-repeat="SLA in SLADetails">
                                                        <td style="vertical-align: central !important;">
                                                            <label>{{SLA.Key.SLAD_DESC}}</label>
                                                        </td>
                                                        <td>
                                                            <input type="checkbox" id="chk" data-ng-model="SLA.Key.selected" value="{{SLA.Key.selected}}" />
                                                        </td>
                                                        <td data-ng-repeat="SLARoles in SLA.Value" data-ng-form="innerForm">
                                                            <div class="input-group margin">
                                                                <span class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-time"></span>
                                                                </span>
                                                                <div data-ng-class="{'has-error': frmSLA.$submitted && innerForm.SLAD_ESC_TIME.$invalid}">
                                                                    <input class="form-control input-sm" placeholder="" data-ng-model="SLARoles.Value.SLAD_ESC_TIME" data-ng-pattern="/^[0-9]{1,5}$/" name="SLAD_ESC_TIME" required="" type="text">
                                                                </div>
                                                                <div data-ng-class="{'has-error': frmSLA.$submitted && innerForm.SLAD_ESC_TIME_TYPE.$invalid}">
                                                                    <select class="form-control input-sm" data-ng-model="SLARoles.Value.SLAD_ESC_TIME_TYPE" name="SLAD_ESC_TIME_TYPE" required="">
                                                                        <option data-ng-repeat="type in timetype" data-ng-selected="type.value==SLARoles.Value.SLAD_ESC_TIME_TYPE" value="{{type.value}}">{{type.Name}}</option>
                                                                    </select>
                                                                </div>
                                                                <span class="error" data-ng-show="innerForm.SLAD_ESC_TIME.$error.pattern">Enter Valid Number</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                            <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                            <input id="btnClear" type="button" class="btn btn-primary custom-button-color" value="Clear" data-ng-click="clear()" />
                                            <%--<input id="btnBack" type="button" class='btn btn-primary' value="Back"  data-ng-click="goBack()" >--%>
                                            <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                                        </div>
                                    </div>
                                </div>

                                <div style="height: 320px">
                                    <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                    <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
           <%-- </div>
        </div>
    </div>--%>
</body>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

<script defer>
    var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
</script>
<%--<script src="../../HDM/HDM_Webfiles/js/SLA.js"></script>--%>
<script src="../../HDM/HDM_Webfiles/js/SLA.min.js" defer></script>
<script src="../../Scripts/Lodash/lodash.min.js" defer></script>
<script src="../JS/MaintenanceSLA.min.js" defer></script>
    <script src="../../SMViews/Utility.min.js" defer></script>
<%--<script src="../JS/MaintenanceSLA.js"></script>--%>
<%--<script src="../../SMViews/Utility.js"></script>--%>
<script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
</html>
