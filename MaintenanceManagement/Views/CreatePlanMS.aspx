﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreatePlanMS.aspx.cs" Inherits="MaintenanceManagement_Views_CreatePlanMS" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };
    </script>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
    <style>
        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .panel-title {
            color: black;
            font-weight: bold;
        }

        .ag-blue .ag-row {
            height: 45px !important;
        }

        .editable-grid-cell::after {
            content: "\F303";
            font-family: "Font Awesome 5 Free";
            position: absolute;
            top: 0;
            right: 0;
            font-size: 15px;
            font-weight: 900;
            z-index: -1; /* this will set it in background so when user click onit you will get cell-click event as if user would normally click on cell */
        }
    </style>
</head>
<body data-ng-controller="CreatePlanMSController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Create Plan</h3>
            </div>
            <div class="card">
                <form id="form1" name="Create Plan" data-valid-submit="loadFn()" novalidate>
                    <div class="row">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Location <span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="CreatePlan.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="CreatePlan.Locations" name="LCM_NAME" style="display: none" required="" />
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Equipment <span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="Equipments" data-output-model="CreatePlan.Equipments" data-button-label="icon AST_SUBCAT_NAME"
                                    data-item-label="icon AST_SUBCAT_NAME maker" data-on-select-all="EquipmentSelectAll()" data-on-select-none="EquipmentSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="CreatePlan.Equipments" name="AST_SUBCAT_NAME" style="display: none" required="" />
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Vendor </label>
                                <div isteven-multi-select data-input-model="Vendors" data-output-model="CreatePlan.Vendors" data-button-label="icon AVR_NAME"
                                    data-item-label="icon AVR_NAME maker" data-on-select-all="VendorSelectAll()" data-on-select-none="VendorSelectAll()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="CreatePlan.Vendors" name="AVR_NAME" style="display: none" required="" />
                            </div>
                        </div>


                    </div>
                    <div class="row form-inline">
                        <div class="col-md-8 text-right" style="padding-top: 17px">
                            <input type="submit" value="Search" ng-click="GetSearchData()" class="btn btn-primary custom-button-color" />

                        </div>
                    </div>

                    <a data-ng-click="GenReport(SpaceAllocation,'xls')"><i id="excel" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 500px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../JS/CreatePlanMS.js"></script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
</body>
</html>

