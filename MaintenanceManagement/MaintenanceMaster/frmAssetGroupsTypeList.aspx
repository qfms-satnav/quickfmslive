<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmAssetGroupsTypeList.aspx.vb" Inherits="MaintenanceManagement_MaintenanceMaster_frmAssetGroupsTypeList"   %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <script src="../../MaintenanceManagement/MaintenanceMaster/GenericJS/MaintenanceMaster.js"
        type="text/javascript" defer></script>

    <script type="text/javascript" defer>
  function CallFunc(id,cstatus){
          $("#UsersGrid").setGridParam({url:'../../MaintenanceManagement/MaintenanceMaster/Generic_Handlers/AssetGroupType.ashx?id=' + id + '&C_Status='+ cstatus}); 
		  $("#UsersGrid").trigger("reloadGrid");
		  GetAssetGroupType();
		  $("#UsersGrid").trigger("reloadGrid");
	}
   
    
 $(function() {
             $("#UsersGrid").setGridParam({url:'../../MaintenanceManagement/MaintenanceMaster/Generic_Handlers/AssetGroupType.ashx'});
             $("#UsersGrid").trigger("reloadGrid");
             GetAssetGroupType();
		     $("#UsersGrid").trigger("reloadGrid");
	        });
 
    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript" defer></script>

    <table id="table2" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
        <tr>
            <td align="left" width="100%" colspan="3">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="88%" Font-Underline="False"
                    ForeColor="Black">Asset Group Type
</asp:Label>
                <hr align="center" width="60%" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="PNLCONTAINER" runat="server" Width="100%" Height="101%">
        <table id="table3" cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
            <tr>
                <td align="left" width="100%" colspan="3">
                    <asp:Label ID="Label2" runat="server" ForeColor="Red" Text="(*) Mandatory Fields"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"  width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                        <tr>
                            <td class="tableHEADER">
                                <strong>&nbsp; Asset Group Type Details </strong>
                            </td>
                            <td class="clsHeaderLink" align="right">
                                <a href="frmAssetGroupsType.aspx" class="clsHeaderLink">Add/Modify Asset Group Type</a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"   width="16" /></td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                    &nbsp;</td>
                <td align="center" style="width: 100%">
                    <table id="UsersGrid" cellpadding="0" cellspacing="0" border="1" width="100%">
                    </table>
                    <div id="UsersGridPager">
                    </div>
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                    height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                        width="9" /></td>
                <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                        width="25" /></td>
                <td style="height: 17px">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" /></td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>


