Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_MaintenanceMaster_frmAssetBrands
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    'Dim iStatus As Integer = 0     'holds request Status.
    Dim iLoop As Integer            'holds Loop value.
    Dim strIntID As String          'holds Asset Brand Id.
    Dim strName As String           'holds Building Name
    Dim strDesc As String
    Dim obj As clsMasters = New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            bindgrid()
            binddata()
            rbActions.Checked = True
            Cleardata()
            LblPageStatus.Text = "Add"
        End If
    End Sub

   Private Sub Cleardata()
        txtcode.Text = ""
        txtName.Text = ""
        txtRem.Text = ""
        ddlStatus.SelectedIndex = 0
    End Sub

    Private Sub binddata()
        ' StrSQL = "select distinct AAB_ID,upper(AAB_NAME)AAB_NAME from AMG_ASSET_BRAND where AAB_STA_ID=1 order by AAB_NAME"
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_GET_BRND")
        ObjDR = sp.GetReader()
        cmbPKeys.Items.Clear()
        cmbPKeys.DataSource = ObjDR
        cmbPKeys.DataValueField = "AAB_CODE"
        cmbPKeys.DataTextField = "AAB_NAME"
        cmbPKeys.DataBind()
        cmbPKeys.Items.Insert(0, "--Select--")
        ObjDR.Close()
        Try
            cmbPKeys.SelectedIndex = 0
        Catch ex As Exception

        End Try

        strSQL = "select Distinct AGT_ID,upper(AGT_NAME)AGT_NAME from  " & HttpContext.Current.Session("TENANT") & "." & "AMG_ASSET_GROUPTYPE where AGT_STA_ID=1 Order by AGT_NAME"
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_GET_GRPTYPES")
        ObjDR = sp1.GetReader()
        cmbFKeys.Items.Clear()
        cmbFKeys.DataSource = ObjDR
        cmbFKeys.DataValueField = "AGT_CODE"
        cmbFKeys.DataTextField = "AGT_NAME"
        cmbFKeys.DataBind()
        cmbFKeys.Items.Insert(0, "--Select--")
        ObjDR.Close()
        ' ObjSubSonic.Binddropdown(cmbAstGroups, "MAS_GET_GRP", "AAG_NAME", "AAG_CODE")
        Try
            cmbFKeys.SelectedIndex = 0
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Insertdata()
        Dim iVar1, iVar2, idcount As Integer
        'chk for length of remarks  > 500
        'If ((Len(Trim(txtRem.Text)) >= 500) And (Len(Trim(txtName.Text)) >= 500)) Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Name and Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtRem.Text)) >= 500 Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtName.Text)) >= 500 Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Name in not more than 500 Characters!"
        'Else
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMG_ASSET_BRAND_MAX")

        'strSQL = "select max(AAB_ID) from AMG_ASSET_BRAND"
        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, StrSQL)
        ObjDR = sp.GetReader()
        If IsDBNull(ObjDR.Read) Then
            idcount = ObjDR(0)
            txtcode.Text = txtcode.Text & idcount
        End If

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMG_ASSET_VALIDATE_CODE")
        sp1.Command.AddParameter("@AMG_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)
        'strSQL = "select count(*) from AMG_ASSET_BRAND where AAB_CODE='" & Replace(Trim(txtcode.Text), "'", "''") & "'"
        ' strSQL1 = "select count(*) from AMG_ASSET_BRAND where AAB_NAME='" & Replace(Trim(txtName.Text), "'", "''") & "' and AAB_AGTM_ID='" & Trim(cmbFKeys.SelectedItem.Value) & "'"

        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, StrSQL)
        ObjDR = sp1.GetReader()
        ObjDR.Read()
        iVar1 = ObjDR(0)
        ObjDR.Close()
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMG_ASSET_VALIDATE_CODE1")
        sp2.Command.AddParameter("@AAB_NAME", Replace(Trim(txtName.Text), "'", "''"), DbType.String)
        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
        ObjDR = sp2.GetReader()
        ObjDR.Read()
        iVar2 = ObjDR(0)
        ObjDR.Close()
        If iVar1 > 0 Then
            LblFinalStatus.Visible = True
            LblFinalStatus.Text = "Asset Brand With The Same Code Already Exists..."
            'ElseIf iVar2 > 0 Then
            '    LblFinalStatus.Visible = True
            '    LblFinalStatus.Text = "Asset Brand With The Same Name Already Exists. Please Modify!"
        Else
            'iStatus = 1
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_insrt_BRND_DTLS")
            sp3.Command.AddParameter("@AAB_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)
            sp3.Command.AddParameter("@AAB_NAME", Replace(Trim(txtName.Text), "'", "''"), DbType.String)
            sp3.Command.AddParameter("@AAB_AGTM_ID", Trim(cmbFKeys.SelectedItem.Value), DbType.String)
            sp3.Command.AddParameter("@AAB_STA_ID", ddlStatus.SelectedValue, DbType.Int32)
            sp3.Command.AddParameter("@AAB_UPT_BY", Session("UID"), DbType.String)
            sp3.Command.AddParameter("@AAB_UPT_DT", getoffsetdatetime(DateTime.Now).Date, DbType.DateTime)
            sp3.Command.AddParameter("@AAB_REM", Replace(Trim(txtRem.Text), "'", "''"), DbType.String)
            sp3.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp3.ExecuteScalar()

            LblFinalStatus.Visible = True
            LblFinalStatus.Text = "New Asset Brand Added Successfully..."
            ' Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=27")
            Cleardata()
            binddata()
            bindgrid()
            'LoadGrid()
            'End If
        End If
    End Sub

    Private Sub modifydata()
        'chk for length of remarks  > 500
        'If ((Len(Trim(txtRem.Text)) >= 500) And (Len(Trim(txtName.Text)) >= 500)) Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Name and Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtRem.Text)) >= 500 Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtName.Text)) >= 500 Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Name in not more than 500 Characters!"
        'Else
        'iStatus = 1
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_UPDT_BRND_DTLS")
        sp3.Command.AddParameter("@AAB_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)
        sp3.Command.AddParameter("@AAB_NAME", Replace(Trim(txtName.Text), "'", "''"), DbType.String)
        sp3.Command.AddParameter("@AAB_STA_ID", ddlStatus.SelectedValue, DbType.Int32)
        sp3.Command.AddParameter("@AAB_AGTM_ID", Trim(cmbFKeys.SelectedItem.Value), DbType.String)
        sp3.Command.AddParameter("@AAB_REM", Replace(Trim(txtRem.Text), "'", "''"), DbType.String)
        sp3.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp3.ExecuteScalar()
        LblFinalStatus.Visible = True
        LblFinalStatus.Text = "Asset Brand Modified Successfully..."
        'Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=28")
        Cleardata()
        binddata()
        bindgrid()
        'LoadGrid()
        'End If
    End Sub

    Private Sub cmbPKeys_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPKeys.SelectedIndexChanged

        If cmbPKeys.SelectedItem.Value <> "--Select--" Then
            LblFinalStatus.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ASSET_BRAND_GET_DETAILS")
            sp.Command.AddParameter("@AMG_CODE", cmbPKeys.SelectedItem.Value, DbType.String)
            'strSQL = "select * from AMG_ASSET_BRAND where AAB_ID=" & cmbPKeys.SelectedItem.Value
            ObjDR = sp.GetReader()
            'ObjDR = DataReader(StrSQL)
            Do While ObjDR.Read
                strIntID = ObjDR("AAB_ID").ToString
                txtcode.Text = ObjDR("AAB_CODE").ToString
                'txtcd.Text = ObjDR("AAB_CODE").ToString
                txtName.Text = ObjDR("AAB_NAME").ToString
                txtRem.Text = ObjDR("AAB_REM").ToString
                cmbFKeys.ClearSelection()
                cmbFKeys.Items.FindByValue(ObjDR("AAB_AGTM_ID")).Selected = True
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ObjDR("AAB_STA_ID")).Selected = True
                'For iLoop = 0 To cmbFKeys.Items.Count - 1
                '    If cmbFKeys.Items(iLoop).Value = ObjDR("AAB_AGTM_ID").ToString Then
                '        cmbFKeys.SelectedIndex = iLoop
                '    End If
                'Next
                'iStatus = ObjDR("AAB_STA_ID")
            Loop
            ObjDR.Close()
        Else
            Cleardata()
            binddata()
        End If

    End Sub

    Private Sub rbActions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged


        If rbActions.Checked = True Then
            btnSubmit.Text = "Add"
            txtcode.Enabled = True
            txtName.Enabled = True
            LblPageStatus.Text = "Add"
            cmbFKeys.Items.Clear()
            'lblabrand.Visible = False
            'cmbPKeys.Visible = False
            trgroup.Visible = False
            '   txtcode.Enabled = True
            Cleardata()
            binddata()
            LblFinalStatus.Text = ""
        Else
            LblFinalStatus.Text = ""
            btnSubmit.Text = "Modify"
            If cmbPKeys.Items.Count <= 1 Then
                LblFinalStatus.Visible = True
                LblFinalStatus.Text = "No Data Found!"
            Else
                LblFinalStatus.Text = ""
            End If
            LblPageStatus.Text = "Modify"
            Cleardata()
            'lblabrand.Visible = True
            'cmbPKeys.Visible = True
            trgroup.Visible = True
            txtcode.Enabled = False
            txtName.Enabled = True
            binddata()
        End If
           

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Response.Write(LblPageStatus.Text)
        If sender.Text = "Add" Then
            Insertdata()
        ElseIf sender.Text = "Modify" Then
            modifydata()
        End If
    End Sub

    Private Sub bindgrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_BRANDS_NEW")
            Dim dummy As String = sp.Command.CommandSql
            gvItem.DataSource = sp.GetDataSet()
            gvItem.DataBind()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub

    'Protected Sub cmbAstGroups_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbAstGroups.SelectedIndexChanged
    '    If cmbAstGroups.SelectedIndex <> 0 Then
    '        Dim param() As SqlParameter = New SqlParameter(0) {}
    '        param(0) = New SqlParameter("@AGT_AGPM_ID", SqlDbType.NVarChar)
    '        param(0).Value = cmbAstGroups.SelectedItem.Value
    '        ObjSubSonic.Binddropdown(cmbFKeys, "MAS_GET_GRPTYPE_ASSET", "AGT_NAME", "AGT_CODE", param)
    '    End If
    'End Sub

    Protected Sub txtcode_TextChanged(sender As Object, e As EventArgs) Handles txtcode.TextChanged
        LblFinalStatus.Text = ""
    End Sub

End Class
