Imports System.Data
Imports System.Data.SqlClient

Partial Class frmassetgroups
    Inherits System.Web.UI.Page

    Protected strSQL, strSQL1 As String
    Protected ObjDR As SqlDataReader

    Dim intid, intlid As String
    'Dim strname As String
    'Dim strdesc As String
    Dim intstatus, i As Integer
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Public Sub cleardata()
        Try
            txtcode.Text = ""
            txtName.Text = ""
            txtRem.Text = ""
            ddlAssetGroup.SelectedIndex = 0
            cmbastype.SelectedIndex = 0
            cmbspaces.SelectedIndex = 0
            ddlStatus.SelectedIndex = 0
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindAssetGroup()
        ObjSubSonic.Binddropdown(ddlAssetGroup, "MAS_GET_ALLGRP_DTLS", "AAG_NAME", "AAG_ID")
        Try
            ddlAssetGroup.SelectedIndex = 0
        Catch ex As Exception
        End Try
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_ASSET_GROUPS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvAssetGroup.DataSource = ds
        gvAssetGroup.DataBind()
        For i As Integer = 0 To gvAssetGroup.Rows.Count - 1
            Dim lblstatus As Label = CType(gvAssetGroup.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "Inactive"
            End If
        Next
    End Sub

    Private Sub Insertdata()
        Try
            ''chk for length of remarks  > 500
            'If ((Len(Trim(txtRem.Text)) >= 500) And (Len(Trim(txtName.Text)) >= 500)) Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Please Enter Name and Remarks in not more than 500 Characters!"
            'ElseIf Len(Trim(txtRem.Text)) >= 500 Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Please Enter Remarks in not more than 500 Characters!"
            'ElseIf Len(Trim(txtName.Text)) >= 500 Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Please Enter Name in not more than 500 Characters!"
            'Else

            '    lblMsg.Visible = False

            Dim param(8) As SqlParameter
            param(0) = New SqlParameter("@AAG_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = Trim(txtcode.Text)
            param(1) = New SqlParameter("@AAG_NAME", SqlDbType.NVarChar, 200)
            param(1).Value = UCase(Trim(txtName.Text))
            param(2) = New SqlParameter("@AAG_STA_ID", SqlDbType.Int)
            param(2).Value = ddlStatus.SelectedItem.Value
            param(3) = New SqlParameter("@AAG_UPT_BY", SqlDbType.NVarChar, 200)
            param(3).Value = Session("UID")
            param(4) = New SqlParameter("@AAG_UPT_DT", SqlDbType.DateTime)
            param(4).Value = getoffsetdatetime(DateTime.Now)
            param(5) = New SqlParameter("@AAG_REM", SqlDbType.NVarChar, 200)
            param(5).Value = Trim(txtRem.Text)
            param(6) = New SqlParameter("@AAG_MFTYPE", SqlDbType.NVarChar, 200)
            param(6).Value = cmbastype.SelectedItem.Value
            param(7) = New SqlParameter("@AAG_STYPE", SqlDbType.NVarChar, 200)
            param(7).Value = cmbspaces.SelectedItem.Value
            param(8) = New SqlParameter("@COMPANY", SqlDbType.Int)
            param(8).Value = HttpContext.Current.Session("COMPANYID")

            Dim status As Integer = ObjSubSonic.GetSubSonicExecuteScalar("MAS_INSRT_GRP_DTLS", param)
            If status = "1" Then
                lblMsg.Visible = True
                lblMsg.Text = "New Asset Group Added Successfully..."
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Asset Group Already Exists..."
            End If

            cleardata()
            BindAssetGroup()
            BindGrid()
            'End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub

    Private Sub modifydata()
        Try
            'chk for length of remarks  > 500
            'If ((Len(Trim(txtRem.Text)) >= 500) And (Len(Trim(txtName.Text)) >= 500)) Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Please Enter Name And Remarks In Not More Than 500 Characters."
            '    Exit Sub
            'ElseIf Len(Trim(txtRem.Text)) >= 500 Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Please Enter Remarks in not more than 500 Characters."
            '    Exit Sub
            'ElseIf Len(Trim(txtName.Text)) >= 500 Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Please Enter Name in not more than 500 Characters."
            '    Exit Sub
            'Else

            'intstatus = 1
            Dim param(7) As SqlParameter
            param(0) = New SqlParameter("@AAG_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = Trim(txtcode.Text)
            param(1) = New SqlParameter("@AAG_NAME", SqlDbType.NVarChar, 200)
            param(1).Value = UCase(Trim(txtName.Text))
            param(2) = New SqlParameter("@AAG_STA_ID", SqlDbType.Int)
            param(2).Value = ddlStatus.SelectedItem.Value
            param(3) = New SqlParameter("@AAG_REM", SqlDbType.NVarChar, 200)
            param(3).Value = Trim(txtRem.Text)
            param(4) = New SqlParameter("@AAG_MFTYPE", SqlDbType.DateTime)
            param(4).Value = cmbastype.SelectedItem.Value
            param(5) = New SqlParameter("@AAG_STYPE", SqlDbType.NVarChar, 200)
            param(5).Value = cmbspaces.SelectedItem.Value
            param(6) = New SqlParameter("@AAG_ID", SqlDbType.NVarChar, 200)
            param(6).Value = ddlAssetGroup.SelectedItem.Value()
            param(7) = New SqlParameter("@COMPANY", SqlDbType.Int)
            param(7).Value = HttpContext.Current.Session("COMPANYID")
            ObjSubSonic.GetSubSonicExecute("MAS_UPDT_GRP_DTLS", param)
            lblMsg.Visible = True
            lblMsg.Text = "Asset Group Modified Successfully..."
            cleardata()
            BindAssetGroup()
            BindGrid()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
        'End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Put user code to initialize the page here
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        lblMsg.Visible = False
        If Not IsPostBack Then
            BindAssetGroup()
            rbActions.Checked = True
            cleardata()
            'LblPageStatus.Text = "Add"
            BindGrid()
        End If
    End Sub

    Private Sub rbActions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged


        If rbActions.Checked = True Then
            'LblPageStatus.Text = rbActions.Items(i).Value
            'cmbPKeys.Visible = False
            'lblabrand.Visible = False
            trgroup.Visible = False
            txtcode.Enabled = True
            txtName.Enabled = True
            btnSubmit.Text = "Add"
            cleardata()
            BindAssetGroup()
            BindGrid()
            lblMsg.Text = ""
        Else
            'If cmbPKeys.Items.Count <= 1 Then
            '    lblMsg.Text = "No Data Found!"
            'Else
            '    lblMsg.Text = ""
            'End If
            '' LblPageStatus.Text = rbActions.Items(i).Value
            ddlAssetGroup.Items.Clear()
            'cmbPKeys.Visible = True
            'lblabrand.Visible = True
            trgroup.Visible = True
            txtcode.Enabled = False
            txtName.Enabled = True
            btnSubmit.Text = "Modify"
            cleardata()
            BindAssetGroup()
            BindGrid()
        End If


    End Sub

    Private Sub ddlAssetGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlAssetGroup.SelectedIndexChanged

        If ddlAssetGroup.SelectedItem.Value <> "--Select--" Then

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@AAG_CODE", SqlDbType.NVarChar, 200)
            param(0).Value = ddlAssetGroup.SelectedItem.Value
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("MAS_GET_GRP_DETAILS", param)
            If ds.Tables(0).Rows.Count > 0 Then
                intid = ds.Tables(0).Rows(0).Item("AAG_ID").ToString
                txtcode.Text = ds.Tables(0).Rows(0).Item("AAG_CODE").ToString
                txtcd.Text = ds.Tables(0).Rows(0).Item("AAG_CODE").ToString
                txtName.Text = ds.Tables(0).Rows(0).Item("AAG_NAME").ToString
                txtRem.Text = ds.Tables(0).Rows(0).Item("AAG_REM").ToString
                ' intstatus = ds.Tables(0).Rows(0).Item("AAG_STA_ID")
                cmbastype.ClearSelection()
                cmbspaces.ClearSelection()
                cmbastype.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAG_MFTYPE")).Selected = True
                cmbspaces.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAG_STYPE")).Selected = True
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("AAG_STA_ID")).Selected = True
            End If
        Else
            cleardata()
            BindAssetGroup()
        End If

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not IsValid Then
            Exit Sub
        End If
        If btnSubmit.Text = "Add" Then
            Insertdata()
        ElseIf btnSubmit.Text = "Modify" Then
            'chk for duplicate entries
            If txtcode.Text <> txtcd.Text Then
            Else
                modifydata()
            End If

        End If

    End Sub

    Protected Sub gvAssetGroup_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvAssetGroup.PageIndexChanging
        gvAssetGroup.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs)
        Response.Redirect("~/MaintenanceManagement/Masters/Mas_Webfiles/frmMaintenanceMasters.aspx")
    End Sub

End Class
