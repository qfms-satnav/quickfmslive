Imports System.Data

Partial Class MaintenanceManagement_MaintenanceMaster_frmAssetModels
    Inherits System.Web.UI.Page

    Dim intstatus As Integer
    Dim strIntID As String
    Dim strName As String
    Dim strDesc As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            bindgrid()
            binddata()
            rbActions.Checked = True
            LblPageStatus.Text = "Add"
            Cleardata()

        End If
    End Sub

    Private Sub Cleardata()
        txtcode.Text = ""
        txtName.Text = ""
        txtRem.Text = ""
        ddlStatus.SelectedIndex = 0
        'cmbPKeys.SelectedIndex = 0
        cmbFKeys.SelectedIndex = 0
    End Sub

    Private Sub binddata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_GET_MDL")
        ObjDR = sp.GetReader()
        cmbPKeys.DataSource = ObjDR
        cmbPKeys.DataValueField = "AAM_CODE"
        cmbPKeys.DataTextField = "AAM_NAME"
        cmbPKeys.DataBind()
        cmbPKeys.Items.Insert(0, "--Select--")
        Try
            cmbPKeys.SelectedIndex = 0
        Catch ex As Exception
        End Try
        ObjDR.Close()

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_GET_BRND")

        ObjDR = sp1.GetReader()
        cmbFKeys.DataSource = ObjDR
        cmbFKeys.DataValueField = "AAB_CODE"
        cmbFKeys.DataTextField = "AAB_NAME"
        cmbFKeys.DataBind()
        cmbFKeys.Items.Insert(0, "--Select--")
        Try
            cmbFKeys.SelectedIndex = 0
        Catch ex As Exception
        End Try
        ObjDR.Close()
    End Sub

    Private Sub Insertdata()
        'chk for length of remarks  > 500
        'If ((Len(Trim(txtRem.Text)) >= 500) And (Len(Trim(txtName.Text)) >= 500)) Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Name and Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtRem.Text)) >= 500 Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtName.Text)) >= 500 Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Name in not more than 500 Characters!"
        'Else
        'chk for duplicate entries
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_MODEL_VALIDATE")
        sp.Command.AddParameter("@MODEL_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)

        'StrSQL = "select count(*) from  AMG_ASSET_MODEL where AAM_CODE='" & Replace(Trim(txtcode.Text), "'", "''") & "'"
        'strSQL1 = "select count(*) from  AMG_ASSET_MODEL where AAM_NAME='" & Replace(Trim(txtName.Text), "'", "''") & "'"
        Dim iVar1, iVar2 As Integer
        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, StrSQL)
        ObjDR = sp.GetReader()
        ObjDR.Read()
        iVar1 = ObjDR(0)
        ObjDR.Close()


        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_MODEL_VALIDATE1")
        sp1.Command.AddParameter("@MODEL_NAME", Replace(Trim(txtName.Text), "'", "''"), DbType.String)
        ObjDR = sp1.GetReader()
        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, strSQL1)
        ObjDR.Read()
        iVar2 = ObjDR(0)
        ObjDR.Close()
        If iVar1 > 0 Then
            LblFinalStatus.Visible = True
            LblFinalStatus.Text = "Asset Model With The Same Code Already Exists..."
        ElseIf iVar2 > 0 Then
            LblFinalStatus.Visible = True
            LblFinalStatus.Text = "Asset Model With The Same Name Already Exists..."
        Else
            intstatus = 1
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_INSRT_MDL")
            sp2.Command.AddParameter("@AAM_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)
            sp2.Command.AddParameter("@AAM_NAME", Replace(Trim(txtName.Text), "'", "''"), DbType.String)
            sp2.Command.AddParameter("@AAM_STA_ID", ddlStatus.SelectedValue, DbType.Int32)
            sp2.Command.AddParameter("@AAM_UPT_BY", Session("UID"), DbType.String)
            sp2.Command.AddParameter("@AAM_UPT_DT", getoffsetdate(Date.Today), DbType.DateTime)
            sp2.Command.AddParameter("@AAM_REM", Replace(Trim(txtRem.Text), "'", "''"), DbType.String)
            sp2.Command.AddParameter("@AAM_ABM_ID", Trim(cmbFKeys.SelectedItem.Value), DbType.String)
            sp2.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp2.ExecuteScalar()
            LblFinalStatus.Visible = True
            LblFinalStatus.Text = "New Asset Model Added Successfully..."
            ' Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=25")
            Cleardata()
            binddata()
            bindgrid()
        End If
        ' End If
    End Sub

    Private Sub modifydata()
        'chk for length of remarks  > 500
        'If ((Len(Trim(txtRem.Text)) >= 500) And (Len(Trim(txtName.Text)) >= 500)) Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Name and Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtRem.Text)) >= 500 Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Remarks in not more than 500 Characters!"
        'ElseIf Len(Trim(txtName.Text)) >= 500 Then
        '    LblFinalStatus.Visible = True
        '    LblFinalStatus.Text = "Please Enter Name in not more than 500 Characters!"
        'Else
        '    intstatus = 1
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MAS_UPDATE_MDL")
        sp2.Command.AddParameter("@AAM_CODE", Replace(Trim(txtcode.Text), "'", "''"), DbType.String)
        sp2.Command.AddParameter("@AAM_NAME", Replace(Trim(txtName.Text), "'", "''"), DbType.String)
        sp2.Command.AddParameter("@AAM_STA_ID", ddlStatus.SelectedValue, DbType.Int32)
        sp2.Command.AddParameter("@AAM_REM", Replace(Trim(txtRem.Text), "'", "''"), DbType.String)
        sp2.Command.AddParameter("@AAM_ABM_ID", Trim(cmbFKeys.SelectedItem.Value), DbType.String)
        sp2.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp2.ExecuteScalar()

        LblFinalStatus.Visible = True
        LblFinalStatus.Text = "Asset Model Modified Successfully..."
        'Response.Redirect("../../WorkSpace/SMS_Webfiles/frmThanks.aspx?id=26")
        Cleardata()
        binddata()
        bindgrid()
        'End If
    End Sub

    Private Sub cmbPKeys_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbPKeys.SelectedIndexChanged

        Dim i As Integer
        If cmbPKeys.SelectedItem.Value <> "--Select--" Then
            LblFinalStatus.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMG_ASSET_MODEL_GET_DETAILS")
            sp.Command.AddParameter("@AAM_CODE", cmbPKeys.SelectedItem.Value, DbType.String)
            'strSQL = "select * from AMG_ASSET_MODEL where AAM_ID =" & cmbPKeys.SelectedItem.Value
            ObjDR = sp.GetReader()
            'DataReader(strSQL)
            Do While ObjDR.Read
                strIntID = ObjDR("AAM_ID").ToString
                txtcode.Text = ObjDR("AAM_CODE").ToString
                txtcd.Text = ObjDR("AAM_CODE").ToString
                txtName.Text = ObjDR("AAM_NAME").ToString
                txtRem.Text = ObjDR("AAM_REM").ToString
                cmbFKeys.ClearSelection()
                cmbFKeys.Items.FindByValue(ObjDR("AAM_ABM_ID")).Selected = True
                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ObjDR("AAM_STA_ID")).Selected = True
                'For i = 0 To cmbFKeys.Items.Count - 1
                '    If cmbFKeys.Items(i).Value = ObjDR("AAM_ABM_ID").ToString Then
                '        cmbFKeys.SelectedIndex = i
                '        Exit For
                '    End If
                'Next
                intstatus = ObjDR("AAM_STA_ID").ToString
            Loop
            ObjDR.Close()
        Else
            Cleardata()
            binddata()
        End If

    End Sub

    Private Sub rbActions_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged


        If rbActions.Checked = True Then
            btnSubmit.Text = "Add"
            LblPageStatus.Text = rbActions.Checked
            'cmbPKeys.Visible = False
            'lblmodel.Visible = False
            trgroup.Visible = False
            txtcode.Enabled = True
            txtName.Enabled = True
            Cleardata()
            binddata()
            LblFinalStatus.Text = ""
        Else
            btnSubmit.Text = "Modify"
            LblFinalStatus.Text = ""
            'If cmbPKeys.Items.Count <= 1 Then
            '    LblFinalStatus.Visible = True
            '    LblFinalStatus.Text = "No Data Found!"
            'Else
            '    LblFinalStatus.Text = ""
            'End If
            LblPageStatus.Text = rbActions.Checked
            cmbPKeys.Items.Clear()
            'cmbPKeys.Visible = True
            'lblmodel.Visible = True
            trgroup.Visible = True
            txtcode.Enabled = False
            txtName.Enabled = True
            Cleardata()
            binddata()
        End If
           

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        If sender.Text = "Add" Then
            Insertdata()
        ElseIf sender.Text = "Modify" Then
            modifydata()
        End If

    End Sub

    Private Sub bindgrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSET_MODELS_NEW")
            Dim dummy As String = sp.Command.CommandSql
            gvItem.DataSource = sp.GetDataSet()
            gvItem.DataBind()

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItem.PageIndexChanging
        gvItem.PageIndex = e.NewPageIndex
        bindgrid()
    End Sub

    Protected Sub txtcode_TextChanged(sender As Object, e As EventArgs) Handles txtcode.TextChanged
        LblFinalStatus.Text = ""
    End Sub

End Class
