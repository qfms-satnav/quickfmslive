<%@ WebHandler Language="VB" Class="AssetGroup" %>

Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
 
Imports clsSubSonicCommonFunctions

Public Class AssetGroup : Implements IHttpHandler, IRequiresSessionState 
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        
             
        Dim request As HttpRequest = context.Request
        Dim response As HttpResponse = context.Response

        Dim _search As String = request("_search")
        Dim numberOfRows As String = request("rows")
        Dim pageIndex As String = request("page")
        Dim sortColumnName As String = request("sidx")
        Dim sortOrderBy As String = request("sord")
        'Dim strCity As String = request("Mcity")
        Dim totalRecords As Integer
        Dim mcityId As Integer = request("id")
        Dim mcitystatus As Integer = request("C_Status")
        GetAssetGroupMasters(mcityId, mcitystatus)
         
        Dim AssetGroupMaster As Collection(Of AssetGroupMaster) = GetAssetGroupMasters(numberOfRows, pageIndex, sortColumnName, sortOrderBy, totalRecords)
        Dim output As String = BuildJQGridResults(AssetGroupMaster, Convert.ToInt32(numberOfRows), Convert.ToInt32(pageIndex), Convert.ToInt32(totalRecords))
        response.Write(output)
    End Sub
 
    Private Sub GetAssetGroupMasters(ByVal AAG_ID As Integer, ByVal AAG_Status As Integer)
               
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AAG_ID", SqlDbType.Int)
        param(0).Value = AAG_ID
        param(1) = New SqlParameter("@STATUS_ID", SqlDbType.Int)
        param(1).Value = AAG_Status
        ObjSubSonic.GetSubSonicDataSet("UPDATE_ASSETGROUP_STATUS", param)
         
       
    End Sub
    
    Private Function BuildJQGridResults(ByVal AssetGroupMaster As Collection(Of AssetGroupMaster), ByVal numberOfRows As Integer, ByVal pageIndex As Integer, ByVal totalRecords As Integer) As String

        Dim result As New JQGridResults()
        Dim rows As New List(Of JQGridRow)()
        For Each AssetGroup As AssetGroupMaster In AssetGroupMaster
            Dim row As New JQGridRow()
            row.cell = New String(1) {}
            'row.cell(0) = AssetGroup.AAG_CODE.ToString()
            'row.cell(1) = AssetGroup.AAG_NAME.ToString()
            'row.cell(2) = AssetGroup.Asset_Group_Active_Inactive_link.ToString()
             
            row.cell(0) = AssetGroup.AAG_NAME.ToString()
            row.cell(1) = AssetGroup.Asset_Group_Active_Inactive_link.ToString()
            rows.Add(row)
        Next
               
        result.rows = rows.ToArray()
        result.page = pageIndex
        result.total = totalRecords / numberOfRows
        result.records = totalRecords
               
        Return New JavaScriptSerializer().Serialize(result)
    End Function

    Private Function GetAssetGroupMasters(ByVal numberOfRows As String, ByVal pageIndex As String, ByVal sortColumnName As String, ByVal sortOrderBy As String, ByRef totalRecords As Integer) As Collection(Of AssetGroupMaster)
        Dim AssetGroups As New Collection(Of AssetGroupMaster)()
              
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@PageIndex", SqlDbType.Int)
        param(0).Value = pageIndex
        param(1) = New SqlParameter("@SortColumnName", SqlDbType.NVarChar, 200)
        param(1).Value = sortColumnName
        param(2) = New SqlParameter("@SortOrderBy", SqlDbType.NVarChar, 200)
        param(2).Value = sortOrderBy
        param(3) = New SqlParameter("@NumberOfRows", SqlDbType.Int)
        param(3).Value = Convert.ToInt32(numberOfRows)
        param(4) = New SqlParameter("@TotalRecords", SqlDbType.Int)
        param(4).Value = 0
        param(4).Direction = ParameterDirection.Output
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GMAS_GETAssetGroupDETAILS", param)
       
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim AssetGroup As AssetGroupMaster
            AssetGroup = New AssetGroupMaster()
            AssetGroup.AAG_ID = Convert.ToString(ds.Tables(0).Rows(i).Item("AAG_ID"))
            AssetGroup.AAG_CODE = Convert.ToString(ds.Tables(0).Rows(i).Item("AAG_CODE"))
            AssetGroup.AAG_NAME = Convert.ToString(ds.Tables(0).Rows(i).Item("AAG_NAME"))
            Dim status As Integer
            If Convert.ToString(ds.Tables(0).Rows(i).Item("AAG_STA_ID")) = "1" Then
                status = CInt(ds.Tables(0).Rows(i).Item("AAG_STA_ID")) - 1
                AssetGroup.Asset_Group_Active_Inactive_link = "<a href='#' onclick='CallFunc(" & AssetGroup.AAG_ID & "," & status & ")' > Active </a>"
            Else
                status = CInt(ds.Tables(0).Rows(i).Item("AAG_STA_ID")) + 1
            
                AssetGroup.Asset_Group_Active_Inactive_link = "<a href='#' onclick='CallFunc(" & AssetGroup.AAG_ID & "," & status & ")' > InActive </a>"
            End If
            AssetGroups.Add(AssetGroup)
            totalRecords = CInt(ds.Tables(1).Rows(0).Item("TotalRecords"))
        Next
        Return AssetGroups
    End Function
    
  

    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class