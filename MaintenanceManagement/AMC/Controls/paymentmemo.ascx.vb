Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports System.IO
Imports System.Threading
Imports System.Data.OleDb

Partial Class MaintenanceManagement_AMC_Controls_paymentmemo
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim rid, uid As String

    Public Sub clearitems()

        ddlbrand.Items.Clear()
        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        cmbVen.Items.Clear()
        cmbWork.Items.Clear()
        lstasset.Items.Clear()
        txtWOGenDate.Text = ""
        txtWOAmt.Text = ""
        txtPMGenFor.Text = ""
        txtPay.Text = ""
        txtBillNo.Text = ""
        txtBillDate.Text = ""
        txtConAdd.Text = ""
        txtConPh.Text = ""
        txtConPer.Text = ""
        txtBillAmt.Text = ""
        lblMessage.Text = ""
        txtTot.Text = ""
        lblMsg.Text = ""


    End Sub

    Public Sub clearitems1()

        ddlbrand.Items.Clear()
        ddlgrouptype.Items.Clear()
        cmbVen.Items.Clear()
        cmbWork.Items.Clear()
        lstasset.Items.Clear()
        txtWOGenDate.Text = ""
        txtWOAmt.Text = ""
        txtPMGenFor.Text = ""
        txtPay.Text = ""
        txtBillNo.Text = ""
        txtBillDate.Text = ""
        txtConAdd.Text = ""
        txtConPh.Text = ""
        txtConPer.Text = ""
        txtBillAmt.Text = ""
        lblMessage.Text = ""
        txtTot.Text = ""
        lblMsg.Text = ""


    End Sub

    Public Sub clearitems2()

        ddlbrand.Items.Clear()
        cmbVen.Items.Clear()
        cmbWork.Items.Clear()
        lstasset.Items.Clear()
        txtWOGenDate.Text = ""
        txtWOAmt.Text = ""
        txtPMGenFor.Text = ""
        txtPay.Text = ""
        txtBillNo.Text = ""
        txtBillDate.Text = ""
        txtConAdd.Text = ""
        txtConPh.Text = ""
        txtConPer.Text = ""
        txtBillAmt.Text = ""
        lblMessage.Text = ""
        txtTot.Text = ""
        lblMsg.Text = ""


    End Sub

    Public Sub clearitems3()

        cmbVen.Items.Clear()
        cmbWork.Items.Clear()
        lstasset.Items.Clear()
        txtWOGenDate.Text = ""
        txtWOAmt.Text = ""
        txtPMGenFor.Text = ""
        txtPay.Text = ""
        txtBillNo.Text = ""
        txtBillDate.Text = ""
        txtConAdd.Text = ""
        txtConPh.Text = ""
        txtConPer.Text = ""
        txtBillAmt.Text = ""
        lblMessage.Text = ""
        txtTot.Text = ""
        lblMsg.Text = ""


    End Sub

    Public Sub clearitems4()

        cmbWork.Items.Clear()
        lstasset.Items.Clear()
        txtWOGenDate.Text = ""
        txtWOAmt.Text = ""
        txtPMGenFor.Text = ""
        txtPay.Text = ""
        txtBillNo.Text = ""
        txtBillDate.Text = ""
        txtConAdd.Text = ""
        txtConPh.Text = ""
        txtConPer.Text = ""
        txtBillAmt.Text = ""
        lblMessage.Text = ""
        txtTot.Text = ""
        lblMsg.Text = ""


    End Sub

    Public Sub clearitems5()

        lstasset.Items.Clear()
        txtWOGenDate.Text = ""
        txtWOAmt.Text = ""
        txtPMGenFor.Text = ""
        txtPay.Text = ""
        txtBillNo.Text = ""
        txtBillDate.Text = ""
        'txtConAdd.Text = ""
        'txtConPh.Text = ""
        'txtConPer.Text = ""
        txtBillAmt.Text = ""
        lblMessage.Text = ""
        txtTot.Text = ""
        lblMsg.Text = ""


    End Sub

    Private Sub BindBuilding()

        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID").ToString
        ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)

    End Sub

    Private Sub PopUpMessage(ByVal strText As String)

        Dim strScript As String
        strScript = "<script language='javascript'>alert('" & strText & "')</script>"
        Page.RegisterStartupScript("strScript", strScript)
        Return

    End Sub

    Public Sub Calculate()

        Dim BillAmt, TaxAmt, TaxOth As Integer
        'BillAmt = CInt(txtBillAmt.Text) + CInt(txtTaxFFIP.Text) + CInt(txtTaxCST.Text) + CInt(txtTaxWST.Text) + CInt(txtTaxOct.Text) + CInt(txtTaxRet.Text) + CInt(txtTaxOth1.Text) + CInt(txtTaxOth2.Text) + CInt(txtpenalty.Text)
        BillAmt = CInt(txtBillAmt.Text) + CInt(txtTaxCST.Text) + CInt(txtTaxOth1.Text) + CInt(txtTaxOth2.Text) + CInt(txtpenalty.Text)
        txtTot.Text = BillAmt
        'If txtTot.Text = 0 Then
        '    PopUpMessage("Bill Amount cannot be zero!")
        '    Exit Sub
        'End If
        If txtPaymentAmount.Text = "" Or txtPaymentAmount.Text Is Nothing Then
            txtPaymentAmount.Text = 0
        End If
        txtPaymentAmount.Text = CInt(txtPaymentAmount.Text) + CInt(txtTot.Text)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        'txtBillDate.Attributes.Add("onClick", "displayDatePicker('" + txtBillDate.ClientID + "')")
        'txtBillDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtBillDate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack Then
            BindBuilding()
            ' ObjSubSonic.Binddropdown(cmbVen, "AMC_GET_VENDOR_PAYMEMO", "AVR_NAME", "AVR_code")
            'cmbWork.Items.Insert(0, "--Select--")
            lblError.Visible = False
            If cmbVen.Items.Count = 1 Then
                lblError.Visible = True
                lblError.Text = "There are no open work orders that can generate a payment memo...."
                pnlContainer.Visible = False
            End If
        End If

    End Sub

    Protected Sub cboBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged

        If cboBuilding.SelectedItem.Value <> "--Select--" Then
            If cboBuilding.SelectedItem.Value <> "--All--" Then
                Dim param(0) As SqlParameter
                param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
                param(0).Value = cboBuilding.SelectedItem.Value
                clearitems()
                ' lblMsg.Text = ""
                'ObjSubSonic.Binddropdown(ddlGroup, "GET_ASSET_GROUP_BLDG", "GROUP_NAME", "GROUP_ID", param)
                'ddlGroup.Items.Insert(0, "--Select--")
                ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
            End If
        Else
            clearitems()
            'lblMsg.Visible = False
        End If

    End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged

        If ddlGroup.SelectedIndex <> 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            'lblMsg.Visible = False
            clearitems1()
            'ddlbrand.Items.Clear()
            'ddlgrouptype.Items.Clear()
            'cmbVen.Items.Clear()
            'ObjSubSonic.Binddropdown(ddlgrouptype, "GET_ASSET_GROUPTYPE_BLDGGROOUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
        Else
            clearitems1()
            'cmbVen.Items.Clear()
            'ddlgrouptype.Items.Clear()
            'ddlbrand.Items.Clear()
        End If

    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged

        If ddlgrouptype.SelectedIndex <> 0 Then
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            clearitems2()
            'lblMsg.Visible = False
            'ddlbrand.Items.Clear()
            'cmbVen.Items.Clear()
            'ObjSubSonic.Binddropdown(ddlbrand, "GET_ASSET_BRAND_BLDGGROOUP", "BRAND_NAME", "BRAND_ID", param)
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            clearitems2()
            'ddlbrand.ClearSelection()
            'cmbVen.ClearSelection()
        End If

    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged

        If ddlbrand.SelectedIndex <> 0 Then
            Dim param(3) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            lblMsg.Visible = False
            clearitems3()
            'cmbVen.Items.Clear()
            'ObjSubSonic.Binddropdown(cmbVen, "GET_ASSET_VENDOR_BLDGGROOUPBR", "VENDOR_NAME", "VENDOR_ID", param)
            ObjSubSonic.Binddropdown(cmbVen, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
        Else
            clearitems3()
            'cmbVen.ClearSelection()
        End If

    End Sub

    Protected Sub cmbVen_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbVen.SelectedIndexChanged

        If cmbVen.SelectedItem.Value <> "--Select--" Then
            clearitems4()
            Dim param1(5) As SqlParameter
            param1(0) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 200)
            param1(0).Value = cmbVen.SelectedItem.Value
            param1(1) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
            param1(1).Value = cboBuilding.SelectedItem.Value
            param1(2) = New SqlParameter("@GROUP_ID", SqlDbType.NVarChar, 200)
            param1(2).Value = ddlGroup.SelectedItem.Value
            param1(3) = New SqlParameter("@GROUP_TYPE_ID", SqlDbType.NVarChar, 200)
            param1(3).Value = ddlgrouptype.SelectedItem.Value
            param1(4) = New SqlParameter("@BRAND", SqlDbType.NVarChar, 200)
            param1(4).Value = ddlbrand.SelectedItem.Value
            param1(5) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param1(5).Value = Session("COMPANYID")
            ObjSubSonic.Binddropdown(cmbWork, "MN_AMC_GET_WORKORDER_BY_VENDOR", "AMN_PLAN_ID", "AMN_PLAN_ID", param1)

            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@AVR_ID", SqlDbType.NVarChar, 200)
            param(0).Value = cmbVen.SelectedItem.Value
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("MN_GET_VENDORDETAILS_BY_CODE", param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtConAdd.Text = ds.Tables(0).Rows(0).Item("AVR_ADDR")
                txtConPh.Text = ds.Tables(0).Rows(0).Item("AVR_MOBILE_PHNO")
                txtConPer.Text = ds.Tables(0).Rows(0).Item("AVR_EMAIL")
            Else
                clearitems4()
            End If
        Else
            clearitems4()

        End If

    End Sub

    Protected Sub cmbWork_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWork.SelectedIndexChanged

        If cmbWork.SelectedItem.Value <> "--Select--" Then
            clearitems5()
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = cmbWork.SelectedItem.Value
            ObjSubSonic.BindListBox(lstasset, "GET_AMC_DETAILS", "ASSETNAME", "AMN_PLAN_ID", param)
            Dim par(0) As SqlParameter
            par(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
            par(0).Value = cmbWork.SelectedItem.Value
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("GET_AMC_DETAILS_CITY", par)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDesc.Text = ds.Tables(0).Rows(0).Item("AMN_PLAN_FOR")
                txtPrem.Text = ds.Tables(0).Rows(0).Item("CTY_NAME")
            End If
            txtWorkOrderAmount.Text = 0
            txtPaymentAmount.Text = 0
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@AWO_MAMPLAN_ID", SqlDbType.NVarChar, 50)
            param(0).Value = cmbWork.SelectedItem.Value
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_WO_AMC_COST", param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtWorkOrderAmount.Text = ds.Tables(0).Rows(0).Item(0)
                txthiddenDate.Text = ds.Tables(0).Rows(0).Item(1)
                txtWOGenDate.Text = ds.Tables(0).Rows(0).Item(1)
                txtWOAmt.Text = ds.Tables(0).Rows(0).Item(2)
                lblMsg.Visible = False
                btnCalc.Visible = True
            End If
            If txtWOGenDate.Text = String.Empty And txtWOAmt.Text = String.Empty Then
                lblMsg.Visible = True
                lblMsg.Text = "Create Work Order and then Proceed..."
                btnCalc.Visible = False
                Exit Sub
            End If
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@APM_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
            param(0).Value = cmbWork.SelectedItem.Value
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_AMC_NET_PAYBLE", param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtPaymentAmount.Text = ds.Tables(0).Rows(0).Item(0)
                txtPMGenFor.Text = ds.Tables(0).Rows(0).Item(0)
            End If
            If txtWOAmt.Text = txtPMGenFor.Text Then
                panel.Visible = False
                lblMsg.Visible = True
                lblMsg.Text = "Payment Memo For Selected Work Order Has Been Completed"
            Else
                panel.Visible = True
            End If
            Dim ctr = 1
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@APM_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
            param(0).Value = cmbWork.SelectedItem.Value
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_COUNT_AMC_WO", param)
            If ds.Tables(0).Rows.Count > 0 Then
                ctr = ds.Tables(0).Rows(0).Item(0)
            End If
            txtPay.Text = cmbWork.SelectedItem.Value & "_" & ctr
        Else
            clearitems5()
        End If

    End Sub

    Protected Sub btnCalc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalc.Click

        Call Calculate()
        If CInt(txtTot.Text) > (CInt(txtWOAmt.Text) - CInt(txtPMGenFor.Text)) Then
            lblMessage.Visible = True
            lblMessage.Text = "Total Payable Cannot Exceed the Cost of the Work Order"
            '  PopUpMessage("Bill Amount Exceeded AMC Cost!")
        ElseIf CInt(txtTot.Text) <= 0 Then
            lblMessage.Visible = True
            lblMessage.Text = "Enter Bill / Invoice Amount..."
            ' PopUpMessage("Enter Bill / Invoice Amount!")
        Else
            lblMessage.Visible = False
            'txtBillAmt.ReadOnly = True
            'txtTaxFFIP.ReadOnly = True
            'txtTaxCST.ReadOnly = True
            'txtTaxWST.ReadOnly = True
            'txtTaxOct.ReadOnly = True
            'txtTaxRet.ReadOnly = True
            'txtTaxOth1.ReadOnly = True
            'txtTaxOth2.ReadOnly = True
            'txtpenalty.ReadOnly = True
            'txtPAN.ReadOnly = True
            Panel1.Visible = True
            'btnCalc.Visible = False
            txtRem.Text = ""
        End If

    End Sub
   
    Protected Sub btnSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSub.Click


        Dim orgfilenameAMC As String = Replace(Replace(fpBrowseDoc.FileName, " ", "_"), "&", "_")
        Dim repdocdatetimeAMC As String = ""
        Try
            If (fpBrowseDoc.HasFile) Then
                Dim fileExt As String
                fileExt = System.IO.Path.GetExtension(fpBrowseDoc.FileName)
                repdocdatetimeAMC = getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss_") & orgfilenameAMC
                Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & repdocdatetimeAMC
                fpBrowseDoc.PostedFile.SaveAs(filePath)
                'lblMsg.Visible = True
                'lblMsg.Text = "File upload successfully"

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

        Try
            Dim param = New SqlParameter() {}
            Dim varsum, varwo As Double
            Dim flag = 0
            Dim uid
            uid = Session("uid")
            Dim par(0) As SqlParameter
            par(0) = New SqlParameter("@PAYMEMO_ID", SqlDbType.NVarChar, 200)
            par(0).Value = txtPay.Text
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("GET_PAYMEMO_ID", par)
            If ds.Tables(0).Rows.Count > 1 Then
                flag = 3005
            End If
            If flag = 0 Then
                Dim flag1 = 0

                Dim par1(1) As SqlParameter
                par1(0) = New SqlParameter("@APM_AAT_ID", SqlDbType.NVarChar, 200)
                par1(0).Value = cmbVen.SelectedItem.Value
                par1(1) = New SqlParameter("@APM_BILL_NO", SqlDbType.Int)
                par1(1).Value = txtBillNo.Text
                ds = New DataSet
                ds = ObjSubSonic.GetSubSonicDataSet("CHK_BILL_INVOCE", par1)
                If ds.Tables(0).Rows.Count > 0 Then
                    If ds.Tables(0).Rows(0).Item("CNT") > 0 Then
                        flag1 = 3005
                    End If
                End If
                If txtBillDate.Text = "NA" Then
                    txtBillDate.Text = 0
                End If
                If flag1 = 0 Then
                    If txtRem.Text.Length > 500 Then
                        lblError.Text = "500 characters is the maximum length permitted for the Remarks field."
                        lblError.Visible = True
                    Else
                        '------REPLACED By SP--------------
                        param = New SqlParameter(23) {}
                        param(0) = New SqlParameter("@APM_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
                        param(0).Value = cmbWork.SelectedItem.Value
                        param(1) = New SqlParameter("@APM_PAYMEMO_ID", SqlDbType.NVarChar, 50)
                        param(1).Value = Replace(Trim(txtPay.Text), "'", "''")
                        param(2) = New SqlParameter("@APM_TYPE", SqlDbType.NVarChar, 50)
                        param(2).Value = "Asset"
                        param(3) = New SqlParameter("@APM_AAT_ID", SqlDbType.NVarChar, 50)
                        param(3).Value = cmbVen.SelectedItem.Value
                        param(4) = New SqlParameter("@APM_BILL_NO", SqlDbType.NVarChar, 50)
                        param(4).Value = Replace(Trim(txtBillNo.Text), "'", "''")
                        param(5) = New SqlParameter("@APM_BILL_DATE ", SqlDbType.DateTime)
                        param(5).Value = Replace(Trim(txtBillDate.Text), "'", "''")
                        param(6) = New SqlParameter("@APM_TAX_FFIP", SqlDbType.Float)
                        param(6).Value = 0 'Replace(Trim(txtTaxFFIP.Text), "'", "''")
                        param(7) = New SqlParameter("@APM_TAX_CST", SqlDbType.Float)
                        param(7).Value = Replace(Trim(txtTaxCST.Text), "'", "''")
                        param(8) = New SqlParameter("@APM_TAX_WST", SqlDbType.Float)
                        param(8).Value = 0 'Replace(Trim(txtTaxWST.Text), "'", "''")
                        param(9) = New SqlParameter("@APM_TAX_OCTROI", SqlDbType.Float)
                        param(9).Value = 0 'Replace(Trim(txtTaxOct.Text), "'", "''")
                        param(10) = New SqlParameter("@APM_TAX_RETENTION", SqlDbType.Float)
                        param(10).Value = 0 'Replace(Trim(txtTaxRet.Text), "'", "''")
                        param(11) = New SqlParameter("@APM_TAX_OTHERS1", SqlDbType.Float)
                        param(11).Value = Replace(Trim(txtTaxOth1.Text), "'", "''")
                        param(12) = New SqlParameter("@APM_TAX_OTHERS2", SqlDbType.Float)
                        param(12).Value = Replace(Trim(txtTaxOth2.Text), "'", "''")
                        param(13) = New SqlParameter("@APM_BILL_AMT", SqlDbType.Float)
                        param(13).Value = Replace(Trim(txtBillAmt.Text), "'", "''")
                        param(14) = New SqlParameter("@APM_NET_PAYABLE", SqlDbType.Float)
                        param(14).Value = Replace(Trim(txtTot.Text), "'", "''")
                        param(15) = New SqlParameter("@APM_MPAPAYADVICE_NO", SqlDbType.NVarChar, 55)
                        param(15).Value = " "
                        param(16) = New SqlParameter("@APM_PAY_MODE", SqlDbType.NVarChar, 50)
                        param(16).Value = " "
                        param(17) = New SqlParameter("@APM_POGEN_DT", SqlDbType.DateTime)
                        param(17).Value = getoffsetdatetime(DateTime.Now)
                        param(18) = New SqlParameter("@APM_POGEN_BY", SqlDbType.NVarChar, 10)
                        param(18).Value = uid
                        param(19) = New SqlParameter("@APM_PENALTY", SqlDbType.Float)
                        param(19).Value = Replace(Trim(txtpenalty.Text), "'", "''")
                        param(20) = New SqlParameter("@APM_REMARKS", SqlDbType.NVarChar, 500)
                        param(20).Value = txtRem.Text
                        param(21) = New SqlParameter("@APM_PAN_NO", SqlDbType.NVarChar, 50)
                        param(21).Value = 0 'Replace(Trim(txtPAN.Text), "'", "''")
                        param(22) = New SqlParameter("@APM_DOC_PATH", SqlDbType.NVarChar, 500)
                        param(22).Value = repdocdatetimeAMC
                        param(23) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                        param(23).Value = HttpContext.Current.Session("COMPANYID")
                        ObjSubSonic.GetSubSonicExecute("AMC_INSRT_AMC_PAYMEMO", param)
                        lblMsg.Visible = True
                        lblMsg.Text = "Data Inserted Successfully"
                        Dim par_payable(0) As SqlParameter
                        par_payable(0) = New SqlParameter("@APM_MPWWRKORD_ID", SqlDbType.NVarChar, 200)
                        par_payable(0).Value = cmbWork.SelectedItem.Text
                        ds = New DataSet
                        ds = ObjSubSonic.GetSubSonicDataSet("GET_PAYMENT_PAYABLE", par_payable)
                        If ds.Tables(0).Rows.Count > 1 Then
                            varsum = ds.Tables(0).Rows(0).Item("GET_PAYMENT_PAYABLE")
                        End If
                        Dim par_cost(0) As SqlParameter
                        par_cost(0) = New SqlParameter("@AWO_MAMPLAN_ID", SqlDbType.NVarChar, 200)
                        par_cost(0).Value = cmbWork.SelectedItem.Text
                        ds = New DataSet
                        ds = ObjSubSonic.GetSubSonicDataSet("GET_AMC_COST", par_cost)
                        If ds.Tables(0).Rows.Count > 0 Then
                            varwo = ds.Tables(0).Rows(0).Item("AWO_AMC_COST")
                        End If
                        lblError.Visible = True
                        lblError.Text = "Record Inserted Successfully"
                        If varsum = varwo Then
                            '------REPLACED By SP--------------
                            param = New SqlParameter(0) {}
                            param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
                            param(0).Value = cmbWork.SelectedItem.Text
                            ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_FLAG", param)
                        End If
                        Response.Redirect("frmAMCfinalpage.aspx?staid=PaymentMemo&rid=" & cmbWork.SelectedItem.Value)
                    End If
                Else
                    lblError.Visible = True
                    lblError.Text = "Existing Bill with the Same Number"
                End If
            Else
                Response.Redirect("frmbackpage.aspx?flag=5")
            End If
        Catch ex As Exception

        End Try

    End Sub
   
End Class
