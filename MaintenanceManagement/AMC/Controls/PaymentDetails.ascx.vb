Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_AMC_Controls_PaymentDetails
    Inherits System.Web.UI.UserControl

    Dim rid, uid As String
    Public param(), param1() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Public Sub clearitems()
        lstasset.Items.Clear()
        cmbPayment.Items.Clear()
        txtcheckdate.Text = ""
        txtChqNo.Text = ""
        txtChqDate.Text = ""
        txtChqAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems1()

      
        lstasset.Items.Clear()
        cmbPayment.Items.Clear()
        txtcheckdate.Text = ""
        txtChqNo.Text = ""
        txtChqDate.Text = ""
        txtChqAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems2()

       
        lstasset.Items.Clear()
        cmbPayment.Items.Clear()
        txtcheckdate.Text = ""
        txtChqNo.Text = ""
        txtChqDate.Text = ""
        txtChqAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems3()

       
        lstasset.Items.Clear()
        cmbPayment.Items.Clear()
        txtcheckdate.Text = ""
        txtChqNo.Text = ""
        txtChqDate.Text = ""
        txtChqAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems4()


        lstasset.Items.Clear()
        cmbPayment.Items.Clear()
        txtcheckdate.Text = ""
        txtChqNo.Text = ""
        txtChqDate.Text = ""
        txtChqAmt.Text = ""
        lblMsg.Text = ""

    End Sub

    Public Sub clearitems5()

        lstasset.Items.Clear()
        cmbPayment.Items.Clear()
        txtcheckdate.Text = ""
        txtChqNo.Text = ""
        txtChqDate.Text = ""
        txtChqAmt.Text = ""
        lblMsg.Text = ""

    End Sub
    Private Sub dispdata()
        Dim ReqId As String = Request("RID")
        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CREATE_PAYMENT_ADVICE_BY_WO_ID")
        SP.Command.AddParameter("@AMN_PLAN_ID", Request.QueryString("RID"), DbType.String)
        SP.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim dr As SqlDataReader = SP.GetReader()
        If dr.Read() Then
            lblReqId.Text = ReqId
            cboBuilding.Text = dr("LCM_NAME")
            ddlGroup.Text = dr("VT_TYPE")
            ddlgrouptype.Text = dr("AST_SUBCAT_NAME")
            ddlbrand.Text = dr("manufacturer")
            cmbVen.Text = dr("AVR_NAME")

            fillgrid()

        End If
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        '     ObjSubSonic.BindGridView(gvItems, "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR", param)
    End Sub
    'Private Sub BindBuilding()
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
    '    param(0).Value = Session("UID").ToString
    '    ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.QueryString("rid") <> "" Then
            Session("rid") = Request.QueryString("rid")
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        'txtChqDate.Attributes.Add("onClick", "displayDatePicker('" + txtChqDate.ClientID + "')")
        'txtChqDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        txtChqDate.Attributes.Add("readonly", "readonly")
        Try

            rid = Session("rid")
            uid = Session("uid")
            If Not IsPostBack Then
                'BindBuilding()
                Panel1.Visible = True
                dispdata()
            End If
        Catch ex As Exception

        End Try

    End Sub

    'Protected Sub cboBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged
    '    If cboBuilding.SelectedItem.Value <> "--Select--" Then
    '        If cboBuilding.SelectedItem.Value <> "--All--" Then
    '            Dim param(0) As SqlParameter
    '            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
    '            param(0).Value = cboBuilding.SelectedItem.Value
    '            clearitems()
    '            'ddlGroup.Items.Insert(0, "--Select--")
    '            'ObjSubSonic.Binddropdown(ddlGroup, "GET_ASSET_GROUP_BLDG", "GROUP_NAME", "GROUP_ID", param)
    '            ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", param)
    '        End If
    '    Else
    '        clearitems()
    '        'cmbVendor.Items.Clear()
    '        'ddlGroup.Items.Clear()
    '        'ddlgrouptype.Items.Clear()
    '        'ddlbrand.Items.Clear()
    '        'cmbVendor.Items.Insert("0", "--Select--")
    '        'lblMsg.Visible = False
    '    End If
    'End Sub

    'Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
    '    If ddlGroup.SelectedIndex <> 0 Then
    '        Dim param(1) As SqlParameter
    '        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
    '        param(0).Value = cboBuilding.SelectedItem.Value
    '        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
    '        param(1).Value = ddlGroup.SelectedItem.Value
    '        clearitems1()
    '        'lblMsg.Visible = False
    '        'ddlbrand.Items.Clear()
    '        'ddlgrouptype.Items.Clear()
    '        'cmbVendor.Items.Clear()
    '        'ObjSubSonic.Binddropdown(ddlgrouptype, "GET_ASSET_GROUPTYPE_BLDGGROOUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
    '        ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
    '    Else
    '        clearitems1()
    '        'cmbVendor.Items.Clear()
    '        'ddlgrouptype.Items.Clear()
    '        'ddlbrand.Items.Clear()
    '    End If
    'End Sub

    'Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged
    '    If ddlgrouptype.SelectedIndex <> 0 Then
    '        Dim param(2) As SqlParameter
    '        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
    '        param(0).Value = cboBuilding.SelectedItem.Value
    '        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
    '        param(1).Value = ddlGroup.SelectedItem.Value
    '        param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
    '        param(2).Value = ddlgrouptype.SelectedItem.Value
    '        clearitems2()
    '        'lblMsg.Visible = False
    '        'ddlbrand.Items.Clear()
    '        'cmbVendor.Items.Clear()
    '        'ObjSubSonic.Binddropdown(ddlbrand, "GET_ASSET_BRAND_BLDGGROOUP", "BRAND_NAME", "BRAND_ID", param)
    '        ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
    '    Else
    '        clearitems2()
    '        'ddlbrand.ClearSelection()
    '        'cmbVendor.ClearSelection()
    '    End If
    'End Sub

    'Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged
    '    If ddlbrand.SelectedIndex <> 0 Then
    '        Dim param(3) As SqlParameter
    '        param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
    '        param(0).Value = cboBuilding.SelectedItem.Value
    '        param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
    '        param(1).Value = ddlGroup.SelectedItem.Value
    '        param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
    '        param(2).Value = ddlgrouptype.SelectedItem.Value
    '        param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
    '        param(3).Value = ddlbrand.SelectedItem.Value
    '        clearitems3()
    '        ' lblMsg.Visible = False
    '        'cmbVendor.Items.Clear()
    '        'ObjSubSonic.Binddropdown(cmbVendor, "GET_ASSET_VENDOR_BLDGGROOUPBR", "VENDOR_NAME", "VENDOR_ID", param)
    '        ObjSubSonic.Binddropdown(cmbVendor, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
    '    Else
    '        clearitems3()
    '        'cmbVendor.ClearSelection()
    '    End If
    'End Sub

    'Protected Sub cmbVendor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbVendor.SelectedIndexChanged
    '    txtChqAmt.Text = ""
    '    txtcheckdate.Text = ""
    '    txtChqDate.Text = ""
    '    txtChqNo.Text = ""

    '    If cmbVendor.SelectedItem.Value <> "--Select--" Then
    '        cmbWorkOrder.Items.Clear()
    '        Dim uid As String
    '        uid = Session("uid")
    '        clearitems4()
    '        'param = New SqlParameter(0) {}
    '        'param(0) = New SqlParameter("@AMN_CTM_ID", SqlDbType.Int)
    '        'param(0).Value = cmbVendor.SelectedItem.Value
    '        'ObjSubSonic.Binddropdown(cmbWorkOrder, "AMC_GET_GADMIN_PAY_DTLS", "AMN_PLAN_ID", "AMN_PLAN_ID", param)
    '        Dim param1(4) As SqlParameter
    '        param1(0) = New SqlParameter("@AMN_CTM_ID", SqlDbType.NVarChar, 200)
    '        param1(0).Value = cmbVendor.SelectedItem.Value
    '        param1(1) = New SqlParameter("@BDGID", SqlDbType.NVarChar, 200)
    '        param1(1).Value = cboBuilding.SelectedItem.Value
    '        param1(2) = New SqlParameter("@GROUP_ID", SqlDbType.NVarChar, 200)
    '        param1(2).Value = ddlGroup.SelectedItem.Value
    '        param1(3) = New SqlParameter("@GROUP_TYPE_ID", SqlDbType.NVarChar, 200)
    '        param1(3).Value = ddlgrouptype.SelectedItem.Value
    '        param1(4) = New SqlParameter("@BRAND", SqlDbType.NVarChar, 200)
    '        param1(4).Value = ddlbrand.SelectedItem.Value
    '        ObjSubSonic.Binddropdown(cmbWorkOrder, "MN_AMC_GET_WORKORDER_BY_VENDOR", "AMN_PLAN_ID", "AMN_PLAN_ID", param1)
    '    Else
    '        clearitems4()
    '        'cmbWorkOrder.Items.Clear()
    '        'cmbWorkOrder.Items.Insert(0, "--Select--")
    '        'cmbPayment.Items.Clear()
    '        'cmbPayment.Items.Insert(0, "--Select--")
    '    End If
    'End Sub
    Protected Sub fillgrid()
        txtChqAmt.Text = ""
        Dim param() As SqlParameter
        If Session("rid") = Request.QueryString("rid") Then
            clearitems5()
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@amn_plan_id", SqlDbType.NVarChar, 50)
            param(0).Value = Session("rid")
            ObjSubSonic.BindListBox(lstasset, "AMC_GET_ASSETS_ADVISE", "ASSETNAME", "AMN_PLAN_ID", param)

            'cmbPayment.Items.Clear()
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@APA_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Session("rid")
            ObjSubSonic.Binddropdown(cmbPayment, "AMC_GET_PAY_ADVS_NO", "APA_PAYADVICE_NO", "APA_PAYADVICE_NO", param)
            If cmbPayment.Items.Count > 0 Then
                lblMsg.Text = " "
            Else
                lblMsg.Text = " Please Create Payment Advice And Then Proceed..."
                cmbPayment.Items.Insert(0, "--Select--")
                Exit Sub
            End If
        Else
            'clearitems5()
            'cmbPayment.Items.Clear()
            'cmbPayment.Items.Insert(0, "--Select--")
        End If
    End Sub

    'Protected Sub cmbWorkOrder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWorkOrder.SelectedIndexChanged

    'End Sub

    Protected Sub cmbPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPayment.SelectedIndexChanged
        Dim ds As New DataSet
        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
        param(0).Value = cmbPayment.SelectedItem.Text
        ds = New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_GENDT_PAY_DTLS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            txtcheckdate.Text = ds.Tables(0).Rows(0).Item("gendate")
        End If
        If cmbPayment.SelectedItem.Value <> "--Select--" Then
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
            param(0).Value = cmbPayment.SelectedItem.Value
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_PAY_DTLS_AMOUNT", param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtChqAmt.Text = ds.Tables(0).Rows(0).Item(0).ToString
            End If
            'If txtChqAmt.Text = 0 Then
            '    txtChqNo.Text = "NA"
            '    txtChqDate.Text = ""
            'End If
        Else
            txtChqAmt.Text = ""
        End If
    End Sub

    Protected Sub btnSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSub.Click
        Dim flag = 0
        Dim rid, uid

        Session("rid") = cmbPayment.SelectedItem.Value
        rid = Session("rid")
        uid = Session("uid")

        If flag = 0 Then
            If txtChqDate.Text = "" Then
                txtChqDate.Text = ""
            End If
            param = New SqlParameter(6) {}
            param(0) = New SqlParameter("@ACD_ID", SqlDbType.NVarChar, 50)
            param(0).Value = cmbPayment.SelectedItem.Value
            param(1) = New SqlParameter("@ACD_MPAPAYADVICE_NO", SqlDbType.NVarChar, 50)
            param(1).Value = cmbPayment.SelectedItem.Value
            param(2) = New SqlParameter("@ACD_CHQ_NO", SqlDbType.NVarChar, 50)
            param(2).Value = Replace(Trim(txtChqNo.Text), "'", "''")
            param(3) = New SqlParameter("@ACD_CHQ_DATE", SqlDbType.DateTime)
            param(3).Value = txtChqDate.Text
            param(4) = New SqlParameter("@ACD_CHQ_AMT", SqlDbType.NVarChar, 30)
            param(4).Value = txtChqAmt.Text
            param(5) = New SqlParameter("@ACD_UPDATED_DT", SqlDbType.NVarChar, 30)
            param(5).Value = getoffsetdatetime(DateTime.Now)
            param(6) = New SqlParameter("@ACD_UPDATED_BY", SqlDbType.NVarChar, 30)
            param(6).Value = uid
            ObjSubSonic.GetSubSonicExecute("AMC_INSRT_PAT_DTLS", param)

            param1 = New SqlParameter(0) {}
            param1(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
            param1(0).Value = cmbPayment.SelectedItem.Value
            ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_PAY_ADVS", param1)
            Response.Redirect("frmAMCfinalpage.aspx?staid=chequedetailsupdated&rid=" & Session("rid"))
        Else
            Response.Redirect("frmbackpage.aspx?flag=6")
        End If
    End Sub


    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmPaymentDetails.aspx")
    End Sub
End Class
