<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PaymentAdvice.ascx.vb"
    Inherits="MaintenanceManagement_AMC_Controls_PaymentAdvice" %>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
                <asp:TextBox ID="txtListCount" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                <asp:CompareValidator ID="cvList" runat="server" ErrorMessage="Please Select Payment Memos..."
                    Display="None" ControlToValidate="txtListCount" Operator="NotEqual" Type="Integer"
                    ValueToCompare="0"></asp:CompareValidator>
            </div>
        </div>
    </div>
</div>

<div id="Panel1" runat="server">

    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>PPM Plan Id <b>:</b></label>
                <asp:Label ID="lblReqId" Font-Bold="true" runat="server" CssClass="control-label"></asp:Label>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Location <b>:</b></label>
                <asp:Label ID="cboBuilding" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Group <b>:</b></label>
                <asp:Label ID="ddlGroup" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Group Type<b>:</b></label>
                <asp:Label ID="ddlgrouptype" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Brand <b>:</b></label>
                <asp:Label ID="ddlbrand" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Vendor<b>:</b></label>
                <asp:Label ID="cmbVen" runat="server" CssClass="control-label" Font-Bold="true"></asp:Label>
            </div>
        </div>
    </div>
<br />
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Name<span style="color: red;">*</span></label>
                <asp:ListBox ID="lstasset" runat="server" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Payment Memos <span style="color: red;">*</span></label>
                <asp:ListBox ID="lstPendingMemo" runat="server" CssClass="form-control" SelectionMode="Multiple" AutoPostBack="true"></asp:ListBox>
            </div>
        </div><div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Mode Of Payment<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="cfvcmbPayMode" runat="server" ValueToCompare="--Select--"
                    Operator="NotEqual" ControlToValidate="cmbPayMode" Display="None" ErrorMessage="Please Select Mode Of Payment"> </asp:CompareValidator>
                <asp:DropDownList ID="cmbPayMode" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                    <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                    <asp:ListItem Value="1">Cheque</asp:ListItem>
                    <asp:ListItem Value="2">Demand Draft</asp:ListItem>
                    <asp:ListItem Value="3">Cash</asp:ListItem>
                    <asp:ListItem Value="NA">NA</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="row">
                    <label>Payment Amount<span style="color: red;">*</span></label>
                    <asp:TextBox ID="txtPayAmt" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
       <%-- <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <asp:Button ID="btnRight" runat="server" CssClass="btn btn-primary custom-button-color" Text=">>" CausesValidation="False"></asp:Button>
                <asp:Button ID="btnLeft" runat="server" CssClass="btn btn-primary custom-button-color" Text="<<" CausesValidation="False"></asp:Button>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label>Selected Payment Memos<span style="color: red;">*</span></label>
                <asp:ListBox ID="lstSelectedMemo" runat="server" CssClass="form-control"></asp:ListBox>
            </div>
        </div>--%>
        
    </div>


   <%-- <div class="row">
    </div>

    <div class="row" id="PanSelAssets" runat="server">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-5">
                        <label class="col-md-3 control-label">
                            <asp:ListBox ID="LB" runat="server" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox></label>
                    </div>
                    <div class="col-md-3">
                        <asp:Button ID="btnRight" runat="server" CssClass="btn btn-primary custom-button-color" Text=">>" CausesValidation="False"></asp:Button>
                        <asp:Button ID="btnLeft" runat="server" CssClass="btn btn-primary custom-button-color" Text="<<" CausesValidation="False"></asp:Button>
                    </div>
                    <%-- <div class="col-md-3">
                        <asp:ListBox ID="lb1" runat="server" CssClass="form-control" SelectionMode="Multiple"></asp:ListBox>
                    </div>
                </div>
            </div>
        </div>
    <%-- </div>--%>


    <div class="row">        
        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
            <div class="form-group">
                <asp:Button ID="btnSub" CssClass="btn btn-default btn-primary" Text="Create Advice" runat="server"></asp:Button>
                <asp:Button ID="btnBack" runat="server" Text="Back" CausesValidation="false" CssClass="btn btn-default btn-primary" />
            </div>
        </div>
    </div>

</div>
                    