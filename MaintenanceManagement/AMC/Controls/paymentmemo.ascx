<%@ Control Language="VB" AutoEventWireup="false" CodeFile="paymentmemo.ascx.vb"
    Inherits="MaintenanceManagement_AMC_Controls_paymentmemo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <div class="row">
                <asp:Label ID="lblError" runat="server" class="col-md-12 control-label" ForeColor="Red" Visible="False"></asp:Label>
                <asp:Label ID="lblMsg" runat="server" class="col-md-12 control-label" ForeColor="Red" Visible="False"></asp:Label>
                <asp:Label ID="lblMessage" runat="server" class="col-md-12 control-label" ForeColor="Red"></asp:Label>
                <asp:TextBox ID="txtPaymentAmount" runat="server" Visible="false" CssClass="form-control" EnableViewState="False"></asp:TextBox>
                <asp:TextBox ID="txtWorkOrderAmount" runat="server" Visible="false" CssClass="form-control" EnableViewState="False"></asp:TextBox>
                <asp:TextBox ID="txthiddenDate" runat="server" CssClass="form-control" Visible="false" EnableViewState="False"></asp:TextBox>
                <asp:CompareValidator ID="cvAmount" runat="server" Visible="False" ErrorMessage="Please Enter Bill Amount LessThan Or Eqal To Maint. Contract Cost!"
                    Display="None" ControlToValidate="txtPaymentAmount" ControlToCompare="txtWorkOrderAmount"
                    Type="Integer" Operator="LessThanEqual"></asp:CompareValidator>
            </div>
        </div>
    </div>
</div>

<div id="pnlContainer" runat="server" width="100%">

    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Location <span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator9" runat="server" ErrorMessage="Please Select Location"
                    Display="None" ControlToValidate="cboBuilding" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                <asp:DropDownList ID="cboBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Group <span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="ddlGroup" Display="None" ErrorMessage="Please Select Asset Group "
                    Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                <asp:DropDownList ID="ddlGroup" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Group Type  <span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="ddlgrouptype" Display="None" ErrorMessage="Please Select Asset Group Type"
                    Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>

                <asp:DropDownList ID="ddlgrouptype" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Brand <span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="ddlbrand" Display="None" ErrorMessage="Please Select Asset Brand"
                    Operator="NotEqual" ValueToCompare="--Select--"></asp:CompareValidator>
                <asp:DropDownList ID="ddlbrand" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Vendor<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator5" runat="server" ErrorMessage="Please Select Vendor"
                    Display="None" ControlToValidate="cmbVen" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                <asp:DropDownList ID="cmbVen" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Work Order<span style="color: red;">*</span></label>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Please Select Work Order"
                    Display="None" ControlToValidate="cmbWork" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                <asp:DropDownList ID="cmbWork" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                    AutoPostBack="True">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Asset Name<span style="color: red;"></span></label>
                <asp:ListBox ID="lstasset" runat="server" CssClass="form-control selectpicker" data-live-search="true"></asp:ListBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Work Order Generated Date<span style="color: red;"></span></label>
                <asp:TextBox ID="txtWOGenDate" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Payment Memo Number <span style="color: red;"></span></label>
                <asp:TextBox ID="txtPay" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Invoice/Bill No<span style="color: red;">*</span></label>
                <asp:RegularExpressionValidator ID="regXbillno" runat="server" ControlToValidate="txtBillNo"
                    Display="None" ErrorMessage="Enter Numerics In Invoice/Bill No" ValidationExpression="^[0-9A-Z\a-z/-]+"></asp:RegularExpressionValidator>
                <asp:RequiredFieldValidator ID="rfvBillNo" runat="server" ErrorMessage="Please Enter Invoice/Bill No"
                    Display="None" ControlToValidate="txtBillNo"></asp:RequiredFieldValidator>
                <asp:TextBox ID="txtBillNo" runat="server" CssClass="form-control" ReadOnly="False"
                    MaxLength="30"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Invoice/Bill Date<span style="color: red;">*</span></label>
                <asp:RequiredFieldValidator ID="rfvIBDt" runat="server" ControlToValidate="txtBillDate"
                    Display="None" ErrorMessage="Please Enter Invoice/Bill Date"></asp:RequiredFieldValidator>
                <div class='input-group date' id='fromdate'>
                    <asp:TextBox ID="txtBillDate" runat="server" CssClass="form-control" EnableViewState="False"></asp:TextBox>
                    <span class="input-group-addon">
                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Maint. Contract Type<span style="color: red;"></span></label>
                <asp:TextBox ID="txtDesc" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Vendor Phone Number<span style="color: red;"></span></label>
                <asp:TextBox ID="txtConPh" CssClass="form-control" ReadOnly="True" runat="server"
                    MaxLength="20"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Vendor Email<span style="color: red;"></span></label>
                <asp:TextBox ID="txtConPer" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="form-group">
                <label>Vendor Address<span style="color: red;"></span></label>
                <asp:TextBox ID="txtConAdd" CssClass="form-control" ReadOnly="True" Height="30%"
                    runat="server" MaxLength="500" TextMode="MultiLine"></asp:TextBox>
            </div>
        </div>

    </div>
    <div class="row">
        <div visible="false" runat="server">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Premise Details<span style="color: red;"></span></label>
                    <asp:TextBox ID="txtPrem" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div id="panel" runat="server">
        <h3 class="panel-title" style="height: 41px;">Cost Details</h3>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Work Order Generated<span style="color: red;"></span></label>
                    <asp:TextBox ID="txtWOAmt" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Payment Memo Generated Amount<span style="color: red;"></span></label>
                    <asp:TextBox ID="txtPMGenFor" CssClass="form-control" ReadOnly="True" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Bill/Invoice Amount<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="regXBIA" runat="server" ControlToValidate="txtBillAmt"
                        Display="None" ErrorMessage="Please enter Bill/Invoice field in numerics. (Greater than 0)"
                        ValidationExpression="^[0-9]+"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtBillAmt" runat="server" ErrorMessage="Please Enter Bill / Invoice Amount"
                        Display="None" ControlToValidate="txtBillAmt"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvBillAmount" runat="server" Width="77px" ErrorMessage="Bill Amount Must be greater than 0"
                        Display="None" ControlToValidate="txtBillAmt" Height="17px" MinimumValue="0"
                        MaximumValue="99999" Visible="False"></asp:RangeValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtBillAmt"
                        FilterMode="ValidChars" ValidChars="0123456789">
                    </cc1:FilteredTextBoxExtender>
                    <asp:TextBox ID="txtBillAmt" CssClass="form-control" runat="server" TextMode="Number" min="0"
                        MaxLength="10"></asp:TextBox>
                </div>
            </div>
            <%--<div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>FFIP<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="revffip" runat="server" ControlToValidate="txtTaxFFIP"
                        Display="None" ErrorMessage="Enter Numerics In FFIP" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtTaxFFIP" runat="server" ErrorMessage="Please Enter FFIP Amount"
                        Display="None" ControlToValidate="txtTaxFFIP"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvFFIP" runat="server" Width="77px" ErrorMessage="FFIP Amount Must be greater than  or Equal to 0"
                        Display="None" ControlToValidate="txtTaxFFIP" Height="17px" MinimumValue="0"
                        MaximumValue="99999"></asp:RangeValidator>
                    <asp:TextBox ID="txtTaxFFIP" CssClass="form-control" runat="server" TextMode="Number" min="0"
                        MaxLength="10">0</asp:TextBox>
                </div>
            </div>--%>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>GST/ST<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="revcst" runat="server" ControlToValidate="txtTaxCST"
                        Display="None" ErrorMessage="Enter Numerics In CST" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtTaxCST" runat="server" ErrorMessage="Please Enter CST/ST Amount"
                        Display="None" ControlToValidate="txtTaxCST"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCST" runat="server" Width="77px" ErrorMessage="CST Amount Must be greater than   or Equal to 0"
                        Display="None" ControlToValidate="txtTaxCST" Height="17px" MinimumValue="0" MaximumValue="99999"></asp:RangeValidator>
                    <asp:TextBox ID="txtTaxCST" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <%--<div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>GST/ST<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="revcst" runat="server" ControlToValidate="txtTaxCST"
                        Display="None" ErrorMessage="Enter Numerics In CST" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtTaxCST" runat="server" ErrorMessage="Please Enter CST/ST Amount"
                        Display="None" ControlToValidate="txtTaxCST"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvCST" runat="server" Width="77px" ErrorMessage="CST Amount Must be greater than   or Equal to 0"
                        Display="None" ControlToValidate="txtTaxCST" Height="17px" MinimumValue="0" MaximumValue="99999"></asp:RangeValidator>
                    <asp:TextBox ID="txtTaxCST" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>--%>
           <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>WST<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="revwst" runat="server" ControlToValidate="txtTaxWST"
                        Display="None" ErrorMessage="Enter Numerics In WST" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtTaxWST" runat="server" ErrorMessage="Please Enter The WST Amount"
                        Display="None" ControlToValidate="txtTaxWST"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvWST" runat="server" Width="77px" ErrorMessage="WST Amount Must be greater than   or Equal to 0"
                        Display="None" ControlToValidate="txtTaxWST" Height="17px" MinimumValue="0" MaximumValue="99999"></asp:RangeValidator>
                    <asp:TextBox ID="txtTaxWST" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Octroi/Entry<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="revoct" runat="server" ControlToValidate="txtTaxOct"
                        Display="None" ErrorMessage="Enter Numerics In Octroi" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtTaxOct" runat="server" ErrorMessage="Please Enter Octroi/Entry Amount"
                        Display="None" ControlToValidate="txtTaxOct"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvOctroi" runat="server" Width="77px" ErrorMessage="Octroi Amount Must be greater than 0"
                        Display="None" ControlToValidate="txtTaxOct" Height="17px" MinimumValue="0" MaximumValue="99999"></asp:RangeValidator>
                    <asp:TextBox ID="txtTaxOct" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>TAN/TIN/PAN<span style="color: red;">*</span></label>

                    <asp:RegularExpressionValidator ID="revtan" runat="server" ControlToValidate="txtPAN" Display="None" ErrorMessage="Enter TAN/TIN/PAN"
                        ValidationExpression="^[0-9,A-Z,a-z]*"></asp:RegularExpressionValidator>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Enter TAN/TIN/PAN"
                        Display="None" ControlToValidate="txtPAN"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtPAN" runat="server" CssClass="form-control" MaxLength="10">0</asp:TextBox>
                </div>
            </div>--%>
        </div>
        <div class="row">
           <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Service Tax <span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="revst" runat="server" ControlToValidate="txtTaxRet"
                        Display="None" ErrorMessage="Enter Numerics In Service Tax" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>

                    <asp:RangeValidator ID="rvReten" runat="server" Width="77px" ErrorMessage="Service tax Amount Must be greater than 0"
                        Display="None" ControlToValidate="txtTaxRet" Height="17px" MinimumValue="0" MaximumValue="99999"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtTaxRet" runat="server" ErrorMessage="Please Enter Service Tax Amount"
                        Display="None" ControlToValidate="txtTaxRet"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtTaxRet" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>--%>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Others 1<span style="color: red;">*</span></label>
                    <asp:RegularExpressionValidator ID="revothrs" runat="server" ControlToValidate="txtTaxOth1"
                        Display="None" ErrorMessage="Enter Numerics In Others1" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RangeValidator ID="rvOthers1" runat="server" Width="77px" ErrorMessage="Other1 Amount Must be greater than 0"
                        Display="None" ControlToValidate="txtTaxOth1" Height="17px" MinimumValue="0"
                        MaximumValue="99999"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="rfvtxtTaxOth1" runat="server" ErrorMessage="Please Enter Others 1 Amount"
                        Display="None" ControlToValidate="txtTaxOth1"></asp:RequiredFieldValidator>
                    <asp:TextBox ID="txtTaxOth1" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Others 2<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfvtxtTaxOth2" runat="server" ErrorMessage="Please Enter Others 2 Amount!"
                        Display="None" ControlToValidate="txtTaxOth2"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revoth2" runat="server" ControlToValidate="txtTaxOth2"
                        Display="None" ErrorMessage="Enter Numerics In Others2" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RangeValidator ID="rvOthers2" runat="server" Width="77px" ErrorMessage="Other2 Amount Must be greater than 0"
                        Display="None" ControlToValidate="txtTaxOth2" Height="17px" MinimumValue="0"
                        MaximumValue="99999"></asp:RangeValidator>
                    <asp:TextBox ID="txtTaxOth2" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Penalty Amount<span style="color: red;">*</span></label>
                    <asp:RequiredFieldValidator ID="rfpenalty" runat="server" ErrorMessage="Please Enter Penalty Amount!"
                        Display="None" ControlToValidate="txtpenalty"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regexppenalty" runat="server" ControlToValidate="txtpenalty"
                        Display="None" ErrorMessage="Enter Numerics In Penalty Amount" ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                    <asp:RangeValidator ID="rngValpenalty" runat="server" Width="77px" ErrorMessage="Penalty Amount Must be greater than 0"
                        Display="None" ControlToValidate="txtpenalty" Height="17px" MinimumValue="0"
                        MaximumValue="99999"></asp:RangeValidator>
                    <asp:TextBox ID="txtpenalty" CssClass="form-control" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                    <label>Total Payable<span style="color: red;">*</span></label>
                    <asp:TextBox ID="txtTot" CssClass="form-control" ReadOnly="True" runat="server" MaxLength="10" TextMode="Number" min="0">0</asp:TextBox>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                <div class="form-group">
                    <asp:Button ID="btnCalc" CssClass="btn btn-default btn-primary" Visible="true" runat="server" Text="Calculate"></asp:Button>
                </div>
            </div>
        </div>
    </div>
<div class="row" id="Panel1" runat="server" visible="False">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Attach Documents (Evidence)</label>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" Display="None" ControlToValidate="fpBrowseDoc"
                ValidationGroup="Val1" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$"> 
            </asp:RegularExpressionValidator>
            <div class="btn-default">
                <i class="fa fa-folder-open-o fa-lg"></i>
                <asp:FileUpload ID="fpBrowseDoc" runat="Server" Width="90%" />
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Remarks</label>
            <asp:TextBox ID="txtRem" CssClass="form-control" runat="server" Height="30%" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
        </div>
    </div>
    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
        <div class="form-group">
            <asp:Button ID="btnSub" CssClass="btn btn-default btn-primary" Visible="true" runat="server" Text="Submit"></asp:Button>
        </div>
    </div>
</div>

</div>


<script  type="text/ecmascript">
    function refreshSelectpicker() {
        $("#<%=ddlbrand.ClientID%>").selectpicker();
        $("#<%=ddlGroup.ClientID%>").selectpicker();
        $("#<%=ddlgrouptype.ClientID%>").selectpicker();
        $("#<%=cmbVen.ClientID%>").selectpicker();
        $("#<%=cmbWork.ClientID%>").selectpicker();
        $("#<%=cboBuilding.ClientID%>").selectpicker();
        $("#<%=lstasset.ClientID%>").selectpicker();
    };
    refreshSelectpicker();
</script>