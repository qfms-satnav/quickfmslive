﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class MaintenanceManagement_AMC_Reports_frmAMCWorkOrder
    Inherits System.Web.UI.Page
    Dim digit(19) As String
    Dim tys(9) As String
    Dim other(4) As String
    Dim Words As String
    Dim mNos(99) As String
    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Function GetUrl(ByVal imagepath As String) As String

        Dim splits As String() = Request.Url.AbsoluteUri.Split("/"c)
        If splits.Length >= 2 Then
            Dim url As String = splits(0) & "//"
            For i As Integer = 2 To i = 6
                url += splits(i)
                url += "/"
            Next
            Return url + imagepath
        End If
        Return imagepath
    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            ' AWC.Src = GetUrl(AWC.Src)
            Dim rid
            rid = Request.QueryString("rid")
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = rid
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("GET_AMC_WO_RDLC_REPORT", param)
            Dim rds As New ReportDataSource()
            rds.Name = "AMCWorkOrderReport"
            rds.Value = ds.Tables(0)
            'This refers to the dataset name in the RDLC file
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/AMCWorkOrderReport.rdlc")
            Dim ci As New CultureInfo(Session("userculture").ToString())
            Dim nfi As NumberFormatInfo = ci.NumberFormat
            Dim p1 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
            Dim p2 As New ReportParameter("ImageVal", BindLogo())
            ReportViewer1.LocalReport.EnableExternalImages = True
            ReportViewer1.LocalReport.SetParameters(p1)
            ReportViewer1.LocalReport.SetParameters(p2)
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True
        End If
    End Sub
    Public Function BindLogo() As String
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
        sp3.Command.AddParameter("@type", "2", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        Dim ds3 As DataSet = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            Return "http://nxtver.quickfms.com/BootStrapCSS/images/" & ds3.Tables(0).Rows(0).Item("IMAGENAME")
        Else
            Return "http://nxtver.quickfms.com/BootStrapCSS/images/yourlogo.png"
        End If
    End Function

End Class
