﻿Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Imports System.Data

Partial Class MaintenanceManagement_AMC_WorkOrderApprovalVIew
    Inherits System.Web.UI.Page
    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim cost As Double
    Dim ReqId As String
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                Dim cmny As String = Session("COMPANYID")
                Session("req") = Request.QueryString("rid")

                BindRequisition()
                TxtTotCost.Text = CDbl(txtTax.Text) + CDbl(txtGrandTot.Text)
            End If
        End If
    End Sub
    Private Sub BindRequisition()
        ReqId = Request.QueryString("rid")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."

        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMC_WO_DETAILS_BY_ID")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)

            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                cboBuilding.Text = dr("LCM_NAME")
                ddlGroup.Text = dr("VT_TYPE")
                ddlgrouptype.Text = dr("AST_SUBCAT_NAME")
                ddlbrand.Text = dr("manufacturer")
                cboVendor.Text = dr("AVR_NAME")
                DrpDwnAMCId.Text = dr("AMN_PLAN_ID")

                txtRemarks.Text = dr("REMARKS")
                lstasset.Items.Add(dr("AAT_CODE"))
                txtDate.Text = dr("START_DATE")

                txtAdvance.Text = dr("AWO_ADVANCE")
                txtMonthly.Text = dr("AWO_MONTHLY")
                txtQuarter.Text = dr("AWO_QUARTERLY")
                txtHalfYear.Text = dr("AWO_HALFYEAR")
                txtAnnual.Text = dr("AWO_ANNUAL")
                TxtOthers.Text = dr("AWO_OTHERS")

                txtGrandTot.Text = dr("AWO_TOTAL")
                txtTax.Text = dr("AWO_TAXES")
                txtTerms.Text = dr("AWO_TERMS_COND")
                chkmail.Checked = dr("AWO_MAIL")

                txtConName.Text = dr("AVR_NAME")
                txtConAdd.Text = dr("AVR_ADDR")
                txtConPh.Text = dr("AVR_MOBILE_PHNO")
                txtConPer.Text = dr("AVR_EMAIL")
                txtAMCSDate.Text = dr("AMN_FROM_DATE")
                txtAMCEDate.Text = dr("AMN_TO_DATE")
            Else
                lblMsg.Text = "No such requisition found."
            End If
        End If
    End Sub
    Protected Sub txtGrandTot_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtGrandTot.TextChanged


        If txtTax.Text = "" Or txtGrandTot.Text = "" Then
            txtTax.Text = 0
            txtGrandTot.Text = 0
        End If
        TxtTotCost.Text = CDbl(txtTax.Text) + CDbl(txtGrandTot.Text)


    End Sub
    Protected Sub txtTax_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTax.TextChanged

        If txtTax.Text = "" Or txtGrandTot.Text = "" Then
            txtTax.Text = 0
            txtGrandTot.Text = 0
        End If
        TxtTotCost.Text = CDbl(txtTax.Text) + CDbl(txtGrandTot.Text)


    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("WorkOrderApproval.aspx")
    End Sub

    Protected Sub btnSub_Click(sender As Object, e As EventArgs) Handles btnSub.Click
       
        'approve
        UpdateWO(3003)

      
    End Sub
    Private Sub UpdateWO(ByVal STA_ID As Integer)
        Dim varTot As Double
        If CDate(txtDate.Text) >= CDate(txtAMCSDate.Text) And CDate(txtDate.Text) < CDate(txtAMCEDate.Text) Then
            lblerror.Visible = False
        Else
            lblerror.Visible = True
            lblerror.Text = "The Start Date Should Be Between AMC Start Date and AMC End Date..."
            Exit Sub
        End If
        If txtGrandTot.Text < 1 Then
            lblerror.Visible = True
            lblerror.Text = "Grand Total Value Should Be Greater Than 0..."
            Exit Sub
        Else
            lblerror.Visible = False
        End If

        varTot = (CInt(txtMonthly.Text) + CInt(txtQuarter.Text) + CInt(txtHalfYear.Text) + CInt(txtAnnual.Text) + CInt(TxtOthers.Text)) - CInt(txtAdvance.Text)
        TxtTotCost.Text = CDbl(txtTax.Text) + CDbl(txtGrandTot.Text)
        If varTot > CInt(TxtTotCost.Text) Then
            lblAssets.Text = "Total Sum Exceeds AMC Cost..."
            lblAssets.Visible = True
            Exit Sub
        Else
            lblAssets.Visible = False
        End If


        cost = CInt(TxtTotCost.Text)

        param = New SqlParameter(17) {}
        param(0) = New SqlParameter("@AWO_MAMPLAN_ID", SqlDbType.NVarChar, 50)
        param(0).Value = Session("req")
        param(1) = New SqlParameter("@AWO_DATE ", SqlDbType.DateTime)
        param(1).Value = Sanitize.SanitizeInput(txtDate.Text)
        param(2) = New SqlParameter("@AWO_AMC_COST", SqlDbType.Int)
        param(2).Value = cost
        param(3) = New SqlParameter("@AWO_ADVANCE ", SqlDbType.Int)
        param(3).Value = Replace(Trim(txtAdvance.Text), "'", "''")
        param(4) = New SqlParameter("@AWO_MONTHLY", SqlDbType.Int)
        param(4).Value = Replace(Trim(txtMonthly.Text), "'", "''")
        param(5) = New SqlParameter("@AWO_QUARTERLY", SqlDbType.Int)
        param(5).Value = Replace(Trim(txtQuarter.Text), "'", "''")
        param(6) = New SqlParameter("@AWO_HALFYEAR", SqlDbType.Int)
        param(6).Value = Replace(Trim(txtHalfYear.Text), "'", "''")
        param(7) = New SqlParameter("@AWO_ANNUAL", SqlDbType.Int)
        param(7).Value = Replace(Trim(txtAnnual.Text), "'", "''")
        param(8) = New SqlParameter("@AWO_OTHERS", SqlDbType.Int)
        param(8).Value = Replace(Trim(TxtOthers.Text), "'", "''")
        param(9) = New SqlParameter("@AWO_UPT_BY", SqlDbType.NVarChar, 50)
        param(9).Value = Session("uid")
        param(10) = New SqlParameter("@AWO_UPT_ON", SqlDbType.DateTime)
        param(10).Value = getoffsetdatetime(DateTime.Now)
        param(11) = New SqlParameter("@AWO_REMARKS", SqlDbType.NVarChar)
        param(11).Value = txtRemarks.Text
        param(12) = New SqlParameter("@COMAPNYID", SqlDbType.Int)
        param(12).Value = Session("COMPANYID")
        param(13) = New SqlParameter("@TAX", SqlDbType.Int)
        param(13).Value = txtTax.Text
        param(14) = New SqlParameter("@TOTAL", SqlDbType.Int)
        param(14).Value = txtGrandTot.Text
        param(15) = New SqlParameter("@AWO_TERMS_COND", SqlDbType.NVarChar)
        param(15).Value = txtTerms.Text
        param(16) = New SqlParameter("@AWO_MAIL", SqlDbType.Bit)
        param(16).Value = chkmail.Checked
        param(17) = New SqlParameter("@STA_ID", SqlDbType.Int)
        param(17).Value = STA_ID
        ObjSubSonic.GetSubSonicExecute("AMC_WO_SIGLE_APPROVAL", param)

        Dim strAst As String = String.Empty
        strAst = Trim(cboVendor.Text)
        Response.Redirect("frmAMCfinalpage.aspx?rid=" & Session("req") & "&staid=WorkOrderApp&asset=" & strAst)

    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        'reject
        UpdateWO(3004)
      

    End Sub
End Class
