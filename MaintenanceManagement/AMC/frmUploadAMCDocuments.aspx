<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmUploadAMCDocuments.aspx.vb" Inherits="MaintenanceManagement_AMC_frmUploadAMCDocuments" Title="Upload AMC Documents" %>

<%@ Register Src="Controls/UploadAMCDocuments.ascx" TagName="UploadAMCDocuments" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
    <style>
        .panel-heading h3 {
            font-size: 16px;
        }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Upload Maintenance Contract Documents" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Upload Maintenance Contract Documents</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="Assetpanel" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="False" ForeColor="Red"
                                ShowSummary="True" DisplayMode="List"></asp:ValidationSummary>
                            <uc1:UploadAMCDocuments ID="UploadAMCDocuments1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
        <%--  </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
