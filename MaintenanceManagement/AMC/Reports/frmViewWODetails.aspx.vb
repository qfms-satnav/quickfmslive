Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Partial Class MaintenanceManagement_AMC_Reports_frmViewWODetails
    Inherits System.Web.UI.Page
    Dim digit(19) As String
    Dim tys(9) As String
    Dim other(4) As String
    Dim Words As String
    Dim mNos(99) As String

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        Dim rid
        rid = Request.QueryString("rid")
        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
        param(0).Value = rid
        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_AMC_WO_DETAILS", param)
        Dim aa As String
        If ds.Tables(0).Rows.Count > 0 Then
            aa = ds.Tables(0).Rows(0).Item("awo_amc_cost").ToString()
            lblCost.Text = aa.ToString   'to display the cost for the first time in WO 
            lblCost2.Text = aa.ToString  'to display the cost for the second time in WO
            Dim str As String
            lblPlanId.Text = rid
            lbldate.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.LongDate)
            lblContractorName.Text = ds.Tables(0).Rows(0).Item("AMN_CNTR_NAME")
            lblBuilding.Text = ds.Tables(0).Rows(0).Item("BDG_NAME")
            lblPlace.Text = ds.Tables(0).Rows(0).Item("CityName")
        End If

        aa = Val(lblCost.Text)
        lblInWords.Text = No2Words(aa)
        lblCost2Inwords.Text = No2Words(aa)

        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
        param(0).Value = rid
        ds = New DataSet
        ds = objsubsonic.GetSubSonicDataSet("AMC_RPT_WO_DTLS_VENDOR", param)


        If ds.Tables(0).Rows.Count > 0 Then
            Dim str
            str = ds.Tables(0).Rows(0).Item(0).ToString() & "<br>" & ds.Tables(0).Rows(0).Item(1).ToString() & "<br>" & ds.Tables(0).Rows(0).Item(3).ToString() & "-" & ds.Tables(0).Rows(0).Item(4).ToString() & "<br>"
            lblAddress.Text = str
            lblPlanId.Text = rid
            lbldate.Text = FormatDateTime(getoffsetdatetime(DateTime.Now), DateFormat.LongDate)
        End If
        'exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Word
        'exportpanel1.FileName = "Tenant Details Report"
    End Sub

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Response.Write("<script>javascript:window.print();</script> ")

    End Sub
    '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Public Function No2Words(ByVal Text1 As String) As String
        Dim value As Object
        Dim temp As Integer
        Dim i As Integer
        Dim other(4) As String
        'Dim Words As String

        digit(1) = " One "
        digit(2) = " Two "
        digit(3) = " Three "
        digit(4) = " Four "
        digit(5) = " Five "
        digit(6) = " Six "
        digit(7) = " Seven "
        digit(8) = " Eight "
        digit(9) = " Nine "
        digit(10) = " Ten "
        digit(11) = " Eleven "
        digit(12) = " Twelve "
        digit(13) = " Thirteen "
        digit(14) = " Fourteen "
        digit(15) = " Fifteen "
        digit(16) = " Sixteen "
        digit(17) = " Seventeen "
        digit(18) = " Eighteen "
        digit(19) = " Nineteen "

        tys(0) = ""
        tys(1) = " Twenty "
        tys(2) = " Thirty "
        tys(3) = " Forty "
        tys(4) = " Fifty "
        tys(5) = " Sixty "
        tys(6) = " Seventy "
        tys(7) = " Eighty "
        tys(8) = " Ninety "

        other(1) = " Hundred "
        other(2) = " Thousand "
        other(3) = " Lakh "
        other(4) = " Crore "

        value = Val(Text1)


        If (value > 999999999) Then
            lblValidate.Visible = True
            Exit Function
        End If

        Words = ""

        'Zero
        If (value = 0) Then Words = "Zero"

        'Units
        temp = Val(Right(Text1, 2)) Mod 100

        two_digit(temp)

        'Hunderds
        If (value > 99) Then
            If (Words <> "") Then Words = " And " + Words
            temp = Val(Left(Right(Text1, 3), 1))
            If (temp <> 0) Then
                Words = " Hundered " + Words
                Words = digit(temp) + Words
            End If
        End If

        'One Thousand to 99 Cores
        If (value > 999) Then
            i = 2
            Dim t_string As String
            Dim l_string As Integer
            l_string = Len(Text1) - 3
            t_string = Text1
            Do
                t_string = Left(t_string, l_string)
                temp = Val(Right(t_string, 2))
                If (temp <> 0) Then
                    Words = other(i) + Words
                    two_digit(temp)
                End If
                i = i + 1
                l_string = Len(t_string) - 2
            Loop Until (l_string < 1)
        End If

        No2Words = "Rs." & Words & "Only"
        Return No2Words

    End Function
    Private Sub two_digit(ByVal no As Integer)
        If (no < 20) Then
            Words = digit(no) + Words
        Else
            Words = digit(no Mod 10) + Words
            Words = tys(no \ 10 - 1) + Words
        End If
    End Sub
 
   

   

    Protected Sub btnExpWord_Click(sender As Object, e As EventArgs) Handles btnExpWord.Click
        btnExpWord.Visible = False
        exportpanel1.ExportType = ControlFreak.ExportPanel.AppType.Word
        exportpanel1.FileName = "Work Order Details"
    End Sub
End Class
'Private Sub ExportToWordAMCWorkOrderReport()

'    Dim ds As New DataSet
'    ds = objsubsonic.GetSubSonicDataSet("AMC_RPT_WO_MAINADMIN")
'    Dim gv As New GridView
'    gv.DataSource = ds
'    gv.DataBind()
'    Export("AMCWorkOrder.docx", gv)
'End Sub

'Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
'    HttpContext.Current.Response.Clear()
'    HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
'    HttpContext.Current.Response.ContentType = "application/ms-word"
'    Dim sw As StringWriter = New StringWriter
'    Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
'    '  Create a form to contain the grid
'    Dim table As Table = New Table
'    table.GridLines = gv.GridLines
'    '  add the header row to the table
'    If (Not (gv.HeaderRow) Is Nothing) Then
'        PrepareControlForExport(gv.HeaderRow)
'        table.Rows.Add(gv.HeaderRow)
'    End If
'    '  add each of the data rows to the table
'    For Each row As GridViewRow In gv.Rows
'        PrepareControlForExport(row)
'        table.Rows.Add(row)
'    Next
'    '  add the footer row to the table
'    If (Not (gv.FooterRow) Is Nothing) Then
'        PrepareControlForExport(gv.FooterRow)
'        table.Rows.Add(gv.FooterRow)
'    End If
'    '  render the table into the htmlwriter
'    table.RenderControl(htw)
'    '  render the htmlwriter into the response
'    HttpContext.Current.Response.Write(sw.ToString)
'    HttpContext.Current.Response.End()
'End Sub
'Private Shared Sub PrepareControlForExport(ByVal control As Control)
'    Dim i As Integer = 0
'    Do While (i < control.Controls.Count)
'        Dim current As Control = control.Controls(i)
'        If (TypeOf current Is LinkButton) Then
'            control.Controls.Remove(current)
'            control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
'        ElseIf (TypeOf current Is ImageButton) Then
'            control.Controls.Remove(current)
'            control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
'        ElseIf (TypeOf current Is HyperLink) Then
'            control.Controls.Remove(current)
'            control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
'        ElseIf (TypeOf current Is DropDownList) Then
'            control.Controls.Remove(current)
'            control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
'        ElseIf (TypeOf current Is CheckBox) Then
'            control.Controls.Remove(current)
'            control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
'            'TODO: Warning!!!, inline IF is not supported ?
'        End If
'        If current.HasControls Then
'            PrepareControlForExport(current)
'        End If
'        i = (i + 1)
'    Loop
'End Sub


