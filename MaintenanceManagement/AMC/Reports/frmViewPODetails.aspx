<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewPODetails.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_frmViewPODetails" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <form id="form1" class="form-horizontal well" runat="server" defaultfocus="DrpDwnPremise">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="row">
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
        </div>


        <%--<div class="row">
            <div class="col-md-12">
                <asp:DataGrid ID="grdViewAssets" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                    CssClass="table table-condensed table-bordered table-hover table-striped" AllowPaging="True" PageSize="50">
                    <Columns>
                        <asp:BoundColumn DataField="APM_PAYMEMO_ID" HeaderText="Payment Memo Id">
                            <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="APM_BILL_NO" HeaderText="Bill No">
                            <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="APM_BILL_DATE" HeaderText="Bill Date" DataFormatString="{0:d}">
                            <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="APM_NET_PAYABLE" HeaderText="Amount" DataFormatString="{0:c2}">
                            <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="APM_MPAPAYADVICE_NO" HeaderText="Payment Advice No">
                            <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn Visible="False" DataField="APM_PAY_MODE" HeaderText="Mode of Pay">
                            <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="APM_REMARKS" HeaderText="Remarks">
                            <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                        </asp:BoundColumn>
                    </Columns>
                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                    <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
                </asp:DataGrid>
            </div>
        </div>--%>
    </form>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
