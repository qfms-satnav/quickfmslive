Imports System.Data
Imports System.io
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_AMC_Reports_AssetUnderAMCDetails
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim sp As SubSonic.StoredProcedure
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub BindData()
        'ObjSubSonic.BindGridView(grdViewAssets, "MN_AMC_RPT_VIEW_ASSETS")
        sp = New SubSonic.StoredProcedure(HttpContext.Current.Session("TENANT") + "." + "MN_AMC_RPT_VIEW_ASSETS")
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session("Uid"), DbType.String)
        sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        Dim i As Int16
        For i = 0 To ds.Tables(0).Rows.Count - 2
            Dim f, n As String
            f = ds.Tables(0).Rows(i).Item("AMN_PLAN_ID")
            n = ds.Tables(0).Rows(i + 1).Item("AMN_PLAN_ID")
            If f = n Then
                ds.Tables(0).Rows(i).Delete()
            End If
        Next
        If ds.Tables(0).Rows.Count() > 0 Then
            grdViewAssets.DataSource = ds
            grdViewAssets.DataBind()
            cmdExcel.Visible = True
        Else
            grdViewAssets.DataSource = Nothing
            grdViewAssets.DataBind()
            cmdExcel.Visible = False
        End If

        For i = 0 To grdViewAssets.Rows.Count - 1
            Dim lblidAsset As Label = CType(grdViewAssets.Rows(i).FindControl("lblidAsset"), Label)
            Dim lstAssets As ListBox = CType(grdViewAssets.Rows(i).FindControl("lstAssets"), ListBox)
            Dim param() As SqlParameter = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = grdViewAssets.Rows(i).Cells(0).Text
            ObjSubSonic.BindListBox(lstAssets, "GET_ASSETLIST", "AAT_NAME", "AMN_PLAN_ID", param)
        Next
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("ViewAMCReports.aspx")
    End Sub

    Protected Sub cmdExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ExportToExcelEmployeeAllocationReport()
    End Sub

    Private Sub ExportToExcelEmployeeAllocationReport()
        ObjSubSonic.BindGridView(grdViewAssets, "MN_AMC_RPT_VIEW_ASSETS")
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("MN_AMC_RPT_VIEW_ASSETS")
        'Dim i As Int16
        'For i = 0 To ds.Tables(0).Rows.Count - 2
        '    Dim f, n As String
        '    f = ds.Tables(0).Rows(i).Item("AMN_PLAN_ID")
        '    n = ds.Tables(0).Rows(i + 1).Item("AMN_PLAN_ID")
        '    If f = n Then
        '        ds.Tables(0).Rows(i).Delete()
        '    End If
        'Next


        'ds.Tables(0).Columns(0).ColumnName = "AMC Request ID (Plan Id)"
        'ds.Tables(0).Columns(1).ColumnName = "Asset Name"
        'ds.Tables(0).Columns(2).ColumnName = "Vendor"
        'ds.Tables(0).Columns(3).ColumnName = "From Date"
        'ds.Tables(0).Columns(4).ColumnName = "To Date"
        'ds.Tables(0).Columns(5).ColumnName = "Building"


        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("AssetUnderAMC.xls", gv)
    End Sub

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub

    ' Replace any of the contained controls with literals

    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Protected Sub grdViewAssets_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles grdViewAssets.PageIndexChanging
        grdViewAssets.PageIndex = e.NewPageIndex
        BindData()
    End Sub
End Class
