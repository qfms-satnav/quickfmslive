<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AMCWorkOrder.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_AMCWorkOrder"
    Title="AMC Report : AMC Work Order" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms"
    TagPrefix="rsweb" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Maintenance Contract Work Orders
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server" defaultfocus="DrpDwnPremise">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                            <div class="row" style="margin-top: 10px">
                               <div class="col-md-12">
                                    <asp:DataGrid ID="grdViewAssets" runat="server" AllowSorting="True" AutoGenerateColumns="False" OnPageIndexChanged="grdViewAssets_Paged" AllowPaging="True"
                                        CssClass="table GridStyle" GridLines="none" PageSize="6" EmptyDataText="No Work Order Details Found.">
                                        <Columns>
                                            <asp:BoundColumn DataField="AMN_PLAN_ID" HeaderText="Plan Id">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>

                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AMN_PLAN_FOR" HeaderText="Plan For">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                             <asp:BoundColumn DataField="LCM_NAME" HeaderText="Location">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AVR_NAME" HeaderText="Vendor">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AWO_AMC_COST" HeaderText="Cost" DataFormatString="{0:c2}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AMN_FROM_DATE" HeaderText="From Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="AMN_TO_DATE" HeaderText="To Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:ButtonColumn Text="View" Visible="false" HeaderText="Details" CommandName="ShowDetails">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:ButtonColumn>
                                            <asp:TemplateColumn HeaderText="Details">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <%--<a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmAMCWorkOrder.aspx")%>?rid=<%# Eval("AMN_PLAN_ID") %>',850,338,'')">View </a>--%>
                                                    <a href="#" onclick="showPopWin('<%# Eval("AMN_PLAN_ID") %>')">View</a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
                                    </asp:DataGrid>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="cmdExcel" OnClick="cmdExcel_Click" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export to Excel"></asp:Button>
                                        <asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Maint. Contract - Work Order Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function showPopWin(id) {

            $("#modalcontentframe").attr("src", "frmAMCWorkOrder.aspx?rid=" + id);
            $("#myModal").modal('show');

            return false;

            //$("#modelcontainer").load("frmAMCWorkOrder.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
            //    $("#myModal").modal('show');
            //});
        }
    </script>
</body>
</html>
