<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExpiredAMC.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_ExpiredAMC" Title="AMC Report : Expired AMC " %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
  <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Payment Memo" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Maintenance Contract Payment Memos </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1" class="form-horizontal well" runat="server" defaultfocus="DrpDwnPremise">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                            <asp:Label ID="lblmsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="False" ShowSummary="true" ValidationGroup="val1"
                                CssClass="alert alert-danger" ForeColor="Red"></asp:ValidationSummary>


                            <div id="Panel1" runat="server">

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Select Location</label>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--" 
                                                    ControlToValidate="DrpDwnPremise" Display="None" ValidationGroup="val1">
                                                </asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="DrpDwnPremise" runat="server" CssClass="selectpicker" data-live-search="true">
                                                        
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Till Date</label>
                                                <asp:RequiredFieldValidator ID="rvdate" runat="server" ControlToValidate="txtDate"
                                                    Display="None" ErrorMessage="Please Select Till Date" ValidationGroup="val1">
                                                </asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <div class='input-group date' id='fromdate'>
                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="cmdView" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit"
                                                ValidationGroup="val1"></asp:Button>
                                            <asp:Button ID="cmdBack" runat="server" Visible="false" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="row table table table-condensed table-responsive">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                    </div>
                                </div>
                            </div>
                            <div>&nbsp</div>
                            <div id="pnlbutton" runat="server" visible="False">

                             <%--<div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="cmdExcel" runat="server" CssClass="btn btn-primary custom-button-color" Text="Export to Excel"
                                                OnClick="cmdExcel_Click"></asp:Button>
                                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back"></asp:Button>
                                        </div>
                                    </div>
                                </div>--%>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
      </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
