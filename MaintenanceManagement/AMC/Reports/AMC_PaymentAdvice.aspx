<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AMC_PaymentAdvice.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_AMC_PaymentAdvice"
    Title="AMC - Payment Advice" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Maintenance Contract Payment Advice
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server" defaultfocus="DrpDwnPremise">
                            <div class="row" style="margin-top: 10px">
                               <div class="col-md-12">
                                    <asp:DataGrid ID="grdViewAssets" runat="server" PageSize="5" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Payment Advice Details Found."
                                        OnPageIndexChanged="grdViewAssets_Paged" CssClass="table table-condensed table-bordered table-hover table-striped">
                                        <Columns>
                                            <asp:BoundColumn DataField="APA_PAYADVICE_NO" HeaderText="Advice No">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="APA_MPWWRKORD_ID" HeaderText="Work Order No">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                             <asp:BoundColumn DataField="LCM_NAME" HeaderText="Location">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CONTRACTOR" HeaderText="Vendor">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="APA_PAYMENT_MODE" HeaderText="Mode Of Payment">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="APA_PAYMENT_AMOUNT" HeaderText="Amount" DataFormatString="{0:c2}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="APA_UPDATED_DT" HeaderText="Updated Date" DataFormatString="{0:d}">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:TemplateColumn HeaderText="View">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                <ItemTemplate>
                                                    <%--<a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmViewChqDetails.aspx")%>?rid=<%# Eval("APA_PAYADVICE_NO") %>',850,338,'')">View </a>--%>
                                                    <a href="#" onclick="showPopWin('<%# Eval("APA_PAYADVICE_NO") %>')">View</a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
                                    </asp:DataGrid>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnexcel" OnClick="btnexcel_Click" runat="server" Text="Export to Excel" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                        <asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color"></asp:Button>

                                    </div>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block --%>

    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">AMC- Payment Advice Details</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                    <iframe id="modalcontentframe" width="100%" height="300px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function showPopWin(id) {

            $("#modalcontentframe").attr("src", "frmViewChqDetails.aspx?rid=" + id);
            $("#myModal").modal('show');

            return false;
        }
    </script>
</body>
</html>
