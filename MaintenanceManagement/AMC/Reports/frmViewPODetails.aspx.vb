Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Partial Class MaintenanceManagement_AMC_Reports_frmViewPODetails
    Inherits System.Web.UI.Page
    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here'

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            BindData()
        End If

    End Sub

    Sub BindData()
        Dim rid, flag
        flag = 0
        rid = Request.QueryString("rid")
        'strSQL = "select * from AMC_PAYMENT_MEMO where APM_MPWWRKORD_ID='" & rid & "'"

        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
        param(0).Value = rid
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GET_PAYMENT_MEMO", param)
        Dim rds As New ReportDataSource()
        rds.Name = "PaymentMemoDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/PaymentMemoReport.rdlc")
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p1 As New ReportParameter("Currencyparam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p1)
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        'grdViewAssets.DataSource = ds
        'grdViewAssets.DataBind()


    End Sub

End Class
