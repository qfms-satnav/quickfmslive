<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewWODetails.aspx.vb"
    Inherits="MaintenanceManagement_AMC_Reports_frmViewWODetails" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>AMC Report : AMC Work Order</title>
</head>
<body>
    <table>
        <tr>
            <td></td>
        </tr>
    </table>
    <form id="form1" runat="server">
        
    <br /><br /><br /><br />
        <table>
          
            <tr>
                <td colspan="2">

                    <cc1:ExportPanel ID="exportpanel1" runat="server">
                    
                                    <div>
                                        <div style="z-index: 107; left: 32px; width: 736px; position: absolute; top: -8px; height: 1042px"
                                            ms_positioning="FlowLayout">
                                            <pre>			
<asp:Button ID="btnExpWord" runat="server" Text="Export to Word" /><strong><font size="4"><u><font face="Arial Narrow" size="6"><center>Work Order</center></font></u></font></strong>
                                                       
<font face="Arial">Ref:<asp:Label ID="lblAddress" runat="server" Font-Names="Arial" Font-Size="10pt" Width="394px" Height="73px">
				</asp:Label></font>
<br />
                                    
                                                <br />
<font face="Arial">Dear Sir/Madam,<br /></font>



<font face="Arial">We refer to your Estimate, where in you have </font>
                <font face="Arial">
    quoted Rs.=<asp:Label ID="lblCost" runat="server" Font-Names="Arial" Font-Size="10pt"
        Width="48px" Font-Bold="True"></asp:Label>,</font>
                                          
                                            <pre><font face="Arial">(<asp:Label ID="lblCost2Inwords" Style="text-underline-position:below"
                                            runat="server" Font-Names="Arial" Font-Size="10pt"></asp:Label>)</font></pre>
                                            <pre><font face="Arial">For the HSBC Electronic Data Processing India Pvt Ltd Work of Annual Maintenance</font><br />
<font face="Arial" size="2">  We hereby award you the contract of the above-mentioned work as per the specifications detailed in your quotation.</font>

<font face="Arial">The Other Terms and Condition are as follows:</font>
</pre>
                                            <pre></pre>
                                            <pre><font face="Arial"></font>&nbsp;</pre>
                                            <pre><font face="Arial">An Amount of Rs. <asp:Label ID="lblCost2" Style="text-underline-position:below "
                                            runat="server" Font-Names="Arial" Font-Size="10pt"></asp:Label> </font></pre>
                                            <pre><font face="Arial">(Rupees_<asp:Label ID="lblInWords" Style=" text-underline-position:below"
                                            runat="server" Font-Names="Arial" Font-Size="10pt"></asp:Label>)
            </font></pre>
                                            <pre><font face="Arial">Favoring M/s.:<asp:Label ID="lblContractorName" runat="server"
                Font-Names="Arial" Font-Size="10pt" Font-Bold="True"></asp:Label>
                Towards Advance for AMC work of HSBC:
                <asp:Label ID="lblBuilding" runat="server" Font-Names="Arial" Font-Size="10pt" Font-Bold="True"></asp:Label>
                Office At :<asp:Label ID="lblPlace" runat="server" Font-Names="Arial" Font-Size="10pt"
                    Font-Bold="True"></asp:Label>
                is being forwarded to you separately. The said advance will be refundable by you
                to us immediately without any defense or demur thereto, in the event of your failing
                to commence the work on or before the stipulated date and time. HSBC reserves the
                right to retain 5% of the gross contract value upto 6 months and further reserves
                the right to confiscateand or deduct such amount as may be decided by HSBC, if the
                work done by you is found to be of inferior quality or towards the delay and or
                for any other reason. Decision of HSBC with respect to the same shall be finaland
                binding. The estimated date for completion of work is within _____ weeks from the
                date of work-order. You are requested to return the duplicate copy of this letter
                duly signed,in token of your acceptance of this Work Order.. Yours sincerely, Authorized
                Signatory. </font>
		</pre>
                                            <pre></pre>
                                            <pre></pre>
                                        </pre>
                                        </div>
                                        

                                        
                                        <asp:Label ID="lbldate" 
                                            runat="server" Font-Names="Arial" Font-Size="10pt" Width="263px">
				<font face="Arial" size="2">Label</font></asp:Label>
                                        <asp:Label ID="lblPlanId" 
                                            runat="server" Font-Names="Arial" Font-Size="10pt" Width="259px">
				<font face="Arial" size="2">lblPlanId</font>
                                        </asp:Label>
                                        <%-- <asp:Label ID="lblParameters" Style="z-index: 106; left: 65px; position: absolute;
            top: 496px" runat="server" Font-Names="Arial" Font-Size="10pt" Width="586px"
            Height="103px" Visible="False">
				<font face="Arial" size="2">TAXES : Included.
					<br>
					DELIVERY & INSTALLATION : By August 31,2006
					<br>
					PAYMENT : 20% Advance Against Corporate Guarantee<br>
					55% Against delivery<br>
					20%</font></asp:Label>--%>
                                        
                                        <asp:Label ID="lblValidate" Style="z-index: 109; left: 259px; position: absolute; top: 162px"
                                            runat="server" Font-Names="Arial" Font-Size="X-Small" Visible="False"
                                            ForeColor="#C00000"> Sorry..Amount Entered Is Too Large.</asp:Label>
                                    
                                        
                                    </div>
                                                     
                    </cc1:ExportPanel>
                </td>
            </tr>

        </table>
    </form>

</body>
</html>
