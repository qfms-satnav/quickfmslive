Imports System
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Globalization
Imports Microsoft.Reporting.WebForms

Partial Class MaintenanceManagement_AMC_Reports_frmViewChqDetails
    Inherits System.Web.UI.Page

    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    ' ------------------- Json ------------------------------------


    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/JSon_Source/Source/themes/flick/jquery-ui.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/JSon_Source/Source/css/ui.jqgrid.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/JSon_Source/Source/js/jquery.min.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/JSon_Source/Source/js/i18n/grid.locale-en.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/JSon_Source/Source/js/jquery.jqGrid.min.js")))
    '    'Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/WorkSpace/Reports/Source/js_source/TowerwiseUtilization.js")))





    '    '--------------------------------------------------------------
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/popup.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/common.js")))
    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/modal/subModal.js")))


    '    ' Styles
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/modal/subModal.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-base.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/ddlevelsmenu-topbar.css")))

    '    Me.Page.Header.Controls.Add(addScript(Page.ResolveUrl("~/js/ddlevelsmenu.js")))


    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/js/Default.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/styAMTamantra.css")))
    '    Me.Page.Header.Controls.Add(addStyle(Page.ResolveUrl("~/Scripts/DateTimePicker.css")))

    'End Sub

    Public param() As SqlParameter

    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If


        Dim rid, flag
        flag = 0

        rid = Request.QueryString("rid")

        'StrSQL = "select * from AMC_PAYMENT_MEMO where APM_MPAPAYADVICE_NO='" & rid & "'"
        'ObjDR = SqlHelper.ExecuteReader(CommandType.Text, StrSQL)
        'If ObjDR.Read Then
        '    flag = 1
        'End If
        'ObjDR.Close()


        'If flag = 0 Then
        '    Response.Redirect("frmAMCfinalpage.aspx?val=2")
        'End If
        If Not Page.IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub cmdBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Response.Redirect("frmAMCViewChq.aspx")
    End Sub


    Sub BindData()
        Dim rid, flag
        flag = 0
        rid = Request.QueryString("rid")
        Dim ds As New DataSet
        Dim param() As SqlParameter = New SqlParameter(0) {}
        param(0) = New SqlParameter("@PAYADVICE_NO", SqlDbType.NVarChar, 200)
        param(0).Value = rid
        ds = ObjSubSonic.GetSubSonicDataSet("GET_PAYMENT_ADVICE_DETAILS", param)
        Dim rds As New ReportDataSource()
        rds.Name = "PaymentAdviceDS"
        rds.Value = ds.Tables(0)
        'This refers to the dataset name in the RDLC file
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Maintenance_Mgmt/PaymentAdviceReport.rdlc")
        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p1 As New ReportParameter("Currencyparam", nfi.CurrencySymbol())
        ReportViewer1.LocalReport.SetParameters(p1)
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        'grdViewAssets.DataSource = ds
        'grdViewAssets.DataBind()
        ' StrSQL = "select a.acd_chq_no CHEQUE,b.* from AMC_PAYMENT_MEMO b,AMC_CHQ_DETAILS a where a.ACD_MPAPAYADVICE_NO=b.APM_MPAPAYADVICE_NO and APM_MPAPAYADVICE_NO='" & rid & "'"
        'grdViewAssets.DataSource = SqlHelper.ExecuteDataset(CommandType.Text, StrSQL)
        'grdViewAssets.DataBind()

    End Sub

    'Sub grdViewAssets_Paged(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)

    '    grdViewAssets.CurrentPageIndex = e.NewPageIndex
    '    BindData()

    'End Sub


    'Private Sub grdViewAssets_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdViewAssets.PageIndexChanged

    '    grdViewAssets.CurrentPageIndex = e.NewPageIndex
    '    BindData()

    'End Sub


    'Private Sub cmdExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExcel.Click
    '    ExportToExcelEmployeeAllocationReport()

    'End Sub


    'Private Sub ExportToExcelEmployeeAllocationReport()
    '    Dim ds As New DataSet


    '    Dim param() As SqlParameter = New SqlParameter(0) {}
    '    param(0) = New SqlParameter("@PAYADVICE_NO", SqlDbType.NVarChar, 200)
    '    param(0).Value = Request.QueryString("rid")
    '    ds = ObjSubSonic.GetSubSonicDataSet("GET_PAYMENT_ADVICE_DETAILS", param)
    '    grdViewAssets.DataSource = ds
    '    grdViewAssets.DataBind()

    '    'ds.Tables(0).Columns(0).ColumnName = "AMC Request ID (Plan Id)"
    '    'ds.Tables(0).Columns(1).ColumnName = "Asset Name"
    '    'ds.Tables(0).Columns(2).ColumnName = "Vendor"
    '    'ds.Tables(0).Columns(3).ColumnName = "From Date"
    '    'ds.Tables(0).Columns(4).ColumnName = "To Date"
    '    'ds.Tables(0).Columns(5).ColumnName = "Building"


    '    Dim gv As New GridView
    '    gv.DataSource = ds
    '    gv.DataBind()
    '    Export("AMCPaymentAdviceDetails.xls", gv)
    'End Sub

    'Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
    '    HttpContext.Current.Response.Clear()
    '    HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
    '    HttpContext.Current.Response.ContentType = "application/ms-excel"
    '    Dim sw As StringWriter = New StringWriter
    '    Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
    '    '  Create a form to contain the grid
    '    Dim table As Table = New Table
    '    table.GridLines = gv.GridLines
    '    '  add the header row to the table
    '    If (Not (gv.HeaderRow) Is Nothing) Then
    '        PrepareControlForExport(gv.HeaderRow)
    '        table.Rows.Add(gv.HeaderRow)
    '    End If
    '    '  add each of the data rows to the table
    '    For Each row As GridViewRow In gv.Rows
    '        PrepareControlForExport(row)
    '        table.Rows.Add(row)
    '    Next
    '    '  add the footer row to the table
    '    If (Not (gv.FooterRow) Is Nothing) Then
    '        PrepareControlForExport(gv.FooterRow)
    '        table.Rows.Add(gv.FooterRow)
    '    End If
    '    '  render the table into the htmlwriter
    '    table.RenderControl(htw)
    '    '  render the htmlwriter into the response
    '    HttpContext.Current.Response.Write(sw.ToString)
    '    HttpContext.Current.Response.End()
    'End Sub
    '' Replace any of the contained controls with literals
    'Private Shared Sub PrepareControlForExport(ByVal control As Control)
    '    Dim i As Integer = 0
    '    Do While (i < control.Controls.Count)
    '        Dim current As Control = control.Controls(i)
    '        If (TypeOf current Is LinkButton) Then
    '            control.Controls.Remove(current)
    '            control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
    '        ElseIf (TypeOf current Is ImageButton) Then
    '            control.Controls.Remove(current)
    '            control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
    '        ElseIf (TypeOf current Is HyperLink) Then
    '            control.Controls.Remove(current)
    '            control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
    '        ElseIf (TypeOf current Is DropDownList) Then
    '            control.Controls.Remove(current)
    '            control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
    '        ElseIf (TypeOf current Is CheckBox) Then
    '            control.Controls.Remove(current)
    '            control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
    '            'TODO: Warning!!!, inline IF is not supported ?
    '        End If
    '        If current.HasControls Then
    '            PrepareControlForExport(current)
    '        End If
    '        i = (i + 1)
    '    Loop
    'End Sub



    'Private Sub cmdprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdprint.Click

    'Response.Write("***a-mantra***")

    'pnlButtons.Visible = False
    'cmdprint.Visible = False
    'cmdExcel.Visible = False

    'grdViewAssets.Visible = False
    'Response.Write("<script>javascript:window.print();</script> ")

    'End Sub
End Class
