﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class MaintenanceManagement_AMC_CreatePaymentDetails
    Inherits System.Web.UI.Page
    Dim rid, uid As String
    Public param(), param1() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If

        If Request.QueryString("rid") <> "" Then
            Session("rid") = Request.QueryString("rid")
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        txtChqDate.Attributes.Add("readonly", "readonly")
        Try
            rid = Session("rid")
            uid = Session("uid")
            If Not IsPostBack Then
                'BindBuilding()
                Panel1.Visible = True
                dispdata()
            End If
        Catch ex As Exception
        End Try

    End Sub

    Public Sub clearitems()

        lstasset.Items.Clear()
        cmbPayment.Items.Clear()
        txtcheckdate.Text = ""
        txtChqNo.Text = ""
        txtChqDate.Text = ""
        txtChqAmt.Text = ""
        lblMsg.Text = ""

    End Sub
    Private Sub dispdata()
        Dim ReqId As String = Request("RID")
        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_PAYMENT_DETAILS_BY_WO_ID")
        SP.Command.AddParameter("@AMN_PLAN_ID", Request.QueryString("RID"), DbType.String)
        SP.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim dr As SqlDataReader = SP.GetReader()
        If dr.Read() Then
            lblReqId.Text = ReqId
            cboBuilding.Text = dr("LCM_NAME")
            ddlGroup.Text = dr("VT_TYPE")
            ddlgrouptype.Text = dr("AST_SUBCAT_NAME")
            ddlbrand.Text = dr("manufacturer")
            cmbVen.Text = dr("AVR_NAME")

            fillgrid()

        End If
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        '     ObjSubSonic.BindGridView(gvItems, "AST_GET_CONSUMABLES_FOR_ASSETGRIDVR", param)
    End Sub
  
    
    Protected Sub fillgrid()
        txtChqAmt.Text = ""
        Dim param() As SqlParameter
        If Session("rid") = Request.QueryString("rid") Then
            clearitems()
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@amn_plan_id", SqlDbType.NVarChar, 50)
            param(0).Value = Session("rid")
            ObjSubSonic.BindListBox(lstasset, "AMC_GET_ASSETS_ADVISE", "ASSETNAME", "AMN_PLAN_ID", param)

            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@APA_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Session("rid")
            ObjSubSonic.Binddropdown(cmbPayment, "AMC_GET_PAY_ADVS_NO", "APA_PAYADVICE_NO", "APA_PAYADVICE_NO", param)
            If cmbPayment.Items.Count > 0 Then
                lblMsg.Text = " "
            Else
                lblMsg.Text = " Please Create Payment Advice And Then Proceed..."
                cmbPayment.Items.Insert(0, "--Select--")
                Exit Sub
            End If
        Else
            'clearitems5()
            'cmbPayment.Items.Clear()
            'cmbPayment.Items.Insert(0, "--Select--")
        End If
    End Sub

    Protected Sub cmbPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPayment.SelectedIndexChanged
        Dim ds As New DataSet
        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
        param(0).Value = cmbPayment.SelectedItem.Text
        ds = New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_GENDT_PAY_DTLS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            txtcheckdate.Text = ds.Tables(0).Rows(0).Item("gendate")
        End If
        If cmbPayment.SelectedItem.Value <> "--Select--" Then
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
            param(0).Value = cmbPayment.SelectedItem.Value
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("AMC_GET_PAY_DTLS_AMOUNT", param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtChqAmt.Text = ds.Tables(0).Rows(0).Item(0).ToString
            End If
            'If txtChqAmt.Text = 0 Then
            '    txtChqNo.Text = "NA"
            '    txtChqDate.Text = ""
            'End If
        Else
            txtChqAmt.Text = ""
        End If
    End Sub

    Protected Sub btnSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSub.Click
        Dim flag = 0
        Dim rid, uid

        Session("rid") = cmbPayment.SelectedItem.Value
        rid = Session("rid")
        uid = Session("uid")

        If flag = 0 Then
            If txtChqDate.Text = "" Then
                txtChqDate.Text = ""
            End If
            Dim sp As SubSonic.StoredProcedure
            sp = New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMC_INSRT_PAT_DTLS")
            sp.Command.AddParameter("@ACD_ID", cmbPayment.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@ACD_MPAPAYADVICE_NO", cmbPayment.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@ACD_CHQ_NO", Replace(Trim(txtChqNo.Text), "'", "''"), DbType.String)
            sp.Command.AddParameter("@ACD_CHQ_DATE", txtChqDate.Text, DbType.DateTime)
            sp.Command.AddParameter("@ACD_CHQ_AMT", txtChqAmt.Text, DbType.String)
            sp.Command.AddParameter("@ACD_UPDATED_DT", getoffsetdatetime(DateTime.Now), DbType.String)
            sp.Command.AddParameter("@ACD_UPDATED_BY", uid, DbType.String)
            sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            sp.ExecuteScalar()

            'param = New SqlParameter(7) {}
            'param(0) = New SqlParameter("@ACD_ID", SqlDbType.NVarChar, 50)
            'param(0).Value = cmbPayment.SelectedItem.Value
            'param(1) = New SqlParameter("@ACD_MPAPAYADVICE_NO", SqlDbType.NVarChar, 50)
            'param(1).Value = cmbPayment.SelectedItem.Value
            'param(2) = New SqlParameter("@ACD_CHQ_NO", SqlDbType.NVarChar, 50)
            'param(2).Value = Replace(Trim(txtChqNo.Text), "'", "''")
            'param(3) = New SqlParameter("@ACD_CHQ_DATE", SqlDbType.DateTime)
            'param(3).Value = txtChqDate.Text
            'param(4) = New SqlParameter("@ACD_CHQ_AMT", SqlDbType.NVarChar, 30)
            'param(4).Value = txtChqAmt.Text
            'param(5) = New SqlParameter("@ACD_UPDATED_DT", SqlDbType.NVarChar, 30)
            'param(5).Value = getoffsetdatetime(DateTime.Now)
            'param(6) = New SqlParameter("@ACD_UPDATED_BY", SqlDbType.NVarChar, 30)
            'param(6).Value = uid
            'param(7) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            'param(7).Value = HttpContext.Current.Session("COMPANYID")
            'ObjSubSonic.GetSubSonicExecute("AMC_INSRT_PAT_DTLS", param)

            param1 = New SqlParameter(0) {}
            param1(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
            param1(0).Value = cmbPayment.SelectedItem.Value
            ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_PAY_ADVS", param1)
            Response.Redirect("frmAMCfinalpage.aspx?staid=chequedetailsupdated&rid=" & Session("rid"))
        Else
            Response.Redirect("frmbackpage.aspx?flag=6")
        End If
    End Sub


    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmPaymentDetails.aspx")
    End Sub
End Class
