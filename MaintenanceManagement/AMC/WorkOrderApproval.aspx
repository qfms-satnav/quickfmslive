﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WorkOrderApproval.aspx.vb" Inherits="MaintenanceManagement_AMC_WorkOrderApproval" %>

<!DOCTYPE html>

<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    --%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <style>
        .panel-title {
            font-weight: 400;
            color: #666;
        }

        .panel-heading {
            height: 41px;
            border-bottom: 1px solid #d5d5d5;
        }
    </style>

    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=grdViewAssets.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {

                    return delrecord();

                }
            }
            alert("Please select atleast one checkbox");
            return false;

        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.grdViewAssets.Items.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }


        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.grdViewAssets.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=grdViewAssets.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="View Work Order" ba-panel-class="with-scroll">

                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">L1 Approval</h3>
            </div>
            <div class="card">
                <%-- <div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">

                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" Style="padding-bottom: 20px" ForeColor="Red" ValidationGroup="Val1" />

                                <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-7 control-label">Search by  Work Order ID / Location / Vendor <span style="color: red;">*</span></label>

                                    <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                        Display="none" ErrorMessage="Please Enter Work Order ID / Location / Vendor" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <div class="col-md-5">
                                        <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row col-md-6">
                            <div class="col-md-12">
                                <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                                    CausesValidation="true" TabIndex="2" />
                            </div>
                        </div>
                    </div>
                    <div id="Panel1" runat="server" class="row" style="margin-top: 10px">
                        <div class="col-md-12">
                            <asp:DataGrid ID="grdViewAssets" runat="server" AllowSorting="True" HorizontalAlign="Center" AutoGenerateColumns="False" OnPageIndexChanged="grdViewAssets_Paged"
                                AllowPaging="True" PageSize="6" CssClass="table GridStyle" GridLines="none" EmptyDataText="No records Found">
                                <Columns>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                           
                                            <asp:CheckBox ID="chkAll" runat="server" TextAlign="Right" onclick="javascript:CheckAllDataGridCheckBoxes('chkItem', this.checked);"
                                                ToolTip="Click to check all" />
                                        </HeaderTemplate>
                                         <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkItem" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                        </ItemTemplate>
                                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Work Order ID">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AWO_MAMPLAN_ID", "~/MaintenanceManagement/AMC/WorkOrderApprovalVIew.aspx?rid={0}")%>'
                                                Text='<%# Eval("AWO_MAMPLAN_ID")%> '></asp:HyperLink>
                                            <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AWO_MAMPLAN_ID")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="AMN_PLAN_ID" HeaderText="Maint.Contract Id">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="LCM_NAME" HeaderText="Location">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AVR_NAME" HeaderText="Vendor">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AWO_AMC_COST" HeaderText="Cost" DataFormatString="{0:c2}">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AMN_FROM_DATE" HeaderText="From Date" DataFormatString="{0:d}">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AMN_TO_DATE" HeaderText="To Date" DataFormatString="{0:d}">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="STA_DESC" HeaderText="Status">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:ButtonColumn Text="View" Visible="false" HeaderText="Details" CommandName="ShowDetails">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:ButtonColumn>

                                </Columns>
                                <HeaderStyle BackColor="#1c2b36" ForeColor="#E2E2E2" />

                                <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
                            </asp:DataGrid>
                            <br />
                            <div id="pnlbutton" runat="server">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">

                                        <label class="col-md-12">Remarks </label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtRemarks" CssClass="form-control" Width="100%" Height="30%"
                                                runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnAppall" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();"></asp:Button>
                                        <asp:Button ID="btnRejAll" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();"></asp:Button>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
        <%--  </div>
            </div>
        </div>--%>
    </div>
</body>
</html>
