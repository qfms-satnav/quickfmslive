<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPaymentAdvice.aspx.vb" Inherits="MaintenanceManagement_AMC_frmPaymentAdvice" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <style>
        .panel-title {
            font-size: 16px;
        }
    </style>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Payment Memo" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Payment Advice</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 50px;">--%>
                <form id="form1" runat="server">
                    <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" Style="padding-bottom: 20px" />
                    <div class="row" style="padding-top: 10px;">
                        <div class="col-md-8">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-6 control-label">Search by Requisition ID / Location / Vendor<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                        Display="none" ErrorMessage="Please Enter Request Id / Vendor Name / Location" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                    <div class="col-md-6">
                                        <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="col-md-12">
                                <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Search" ValidationGroup="Val1"
                                    CausesValidation="true" TabIndex="2" />
                            </div>
                        </div>
                    </div>

                    <div id="Panel1" runat="server" class="row" style="margin-top: 10px">
                        <div class="col-md-12">
                            <asp:DataGrid ID="grdViewAssets" runat="server" AllowSorting="True" HorizontalAlign="Center" AutoGenerateColumns="False" OnPageIndexChanged="grdViewAssets_Paged"
                                AllowPaging="True" PageSize="6" CssClass="table GridStyle" GridLines="none" EmptyDataText="No Records Found">
                                <Columns>
                                    <asp:TemplateColumn HeaderText="PPM Plan Id">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AMN_PLAN_ID", "~/MaintenanceManagement/AMC/CreatePaymentAdvice.aspx?rid={0}")%>'
                                                Text='<%# Eval("AMN_PLAN_ID")%> '></asp:HyperLink>
                                            <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AMN_PLAN_ID")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="LCM_NAME" HeaderText="Location">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AVR_NAME" HeaderText="Vendor">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="VT_TYPE" HeaderText="Asset Type">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="AST_SUBCAT_NAME" HeaderText="Asset Sub Type">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:BoundColumn DataField="manufacturer" HeaderText="Brand">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Document">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                        <ItemTemplate>
                                            <a href='<%=Page.ResolveUrl("~/UploadFiles") %>/<%# Eval("APM_DOC_PATH")%>' target="_blank">
                                                <%# Eval("APM_DOC_PATH")%>
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn Text="View" Visible="false" HeaderText="Details" CommandName="ShowDetails">
                                        <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                    </asp:ButtonColumn>
                                    <%-- <asp:TemplateColumn HeaderText="Details">
                                                <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                <ItemTemplate>
                                                     <a href="#" title="Name" onclick="showPopWin('<%=Page.ResolveUrl("frmAMCWorkOrder.aspx")%>?rid=<%# Eval("AMN_PLAN_ID") %>',850,338,'')">View </a>
                                                    <a href="#" onclick="showPopWin('<%# Eval("AMN_PLAN_ID") %>')">View</a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>--%>
                                </Columns>
                                <HeaderStyle ForeColor="white" BackColor="Black" />
                                <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top" CssClass="pagination-ys"></PagerStyle>
                            </asp:DataGrid>

                            <div id="pnlbutton" runat="server">

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                        <asp:Button ID="cmdExcel" OnClick="cmdExcel_Click" Text="Export to Excel" runat="server" CssClass="btn btn-primary custom-button-color"></asp:Button>
                                        <asp:Button ID="btnBack" OnClick="btnBack_Click" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" Visible="false"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <%-- </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
