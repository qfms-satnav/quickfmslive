﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Partial Class MaintenanceManagement_AMC_WorkOrderApproval
    Inherits System.Web.UI.Page

    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            BindData()
        End If
    End Sub

    Private Sub BindData()

        Dim ds As New DataSet
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@status", SqlDbType.Int)
        param(0).Value = 2
        param(1) = New SqlParameter("@AURID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        param(2) = New SqlParameter("@COMPANY", SqlDbType.Int)
        param(2).Value = Session("COMPANYID")
        ds = ObjSubSonic.GetSubSonicDataSet("MN_VIEW_WORKORDERS", param)

        Dim i As Int16
        For i = 0 To ds.Tables(0).Rows.Count - 2
            Dim f, n As String
            f = ds.Tables(0).Rows(i).Item("AMN_PLAN_ID")
            n = ds.Tables(0).Rows(i + 1).Item("AMN_PLAN_ID")
            If f = n Then
                ds.Tables(0).Rows(i).Delete()
            End If
        Next

        If ds.Tables(0).Rows.Count = 0 Then
            pnlbutton.Visible = False
        End If
        grdViewAssets.DataSource = ds
        grdViewAssets.DataBind()
    End Sub

    Sub grdViewAssets_Paged(ByVal sender As Object, ByVal e As DataGridPageChangedEventArgs)

        grdViewAssets.CurrentPageIndex = e.NewPageIndex
        BindData()

    End Sub

    Private Sub grdViewAssets_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdViewAssets.PageIndexChanged
        grdViewAssets.CurrentPageIndex = e.NewPageIndex
        BindData()

    End Sub

    Protected Sub btnAppall_Click(sender As Object, e As EventArgs) Handles btnAppall.Click
        UpdateWOAll(3003)
    End Sub
    Private Sub UpdateWOAll(ByVal STA_ID As Integer)
        Try
            Dim ds As New DataSet
            For Each row As DataGridItem In grdViewAssets.Items
                Dim chkselect As CheckBox = DirectCast(row.FindControl("chkItem"), CheckBox)
                Dim lblreqid As Label = DirectCast(row.FindControl("lblStatusId"), Label)
                If chkselect.Checked = True Then
                    Dim param As SqlParameter() = New SqlParameter(4) {}
                    param(0) = New SqlParameter("@AWO_MAMPLAN_ID", lblreqid.Text)
                    param(1) = New SqlParameter("@STA_ID", STA_ID)
                    param(2) = New SqlParameter("@REMARKS", txtRemarks.Text)
                    param(3) = New SqlParameter("@USR_ID", Session("UID").ToString())
                    param(4) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                    param(4).Value = Session("COMPANYID")
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "AMC_WO_APPROVAL", param)
                ElseIf (STA_ID = 3003) Then
                    lblMessage.Text = "Work order Successfully approved "
                Else
                    lblMessage.Text = "Work order Successfully Rejected "
                End If
                If (STA_ID = 3003) Then
                    lblMessage.Text = "Work order Successfully approved "
                Else
                    lblMessage.Text = "Work order Successfully Rejected "
                End If
            Next
            BindData()
        Catch ex As Exception

        End Try
    End Sub

 
    Protected Sub btnRejAll_Click(sender As Object, e As EventArgs) Handles btnRejAll.Click
        UpdateWOAll(3004)
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEARCH_AMC_WORK_ORDER_BY_PLAN_ID_LOCATION")
        sp.Command.AddParameter("@PLAN_ID", txtReqId.Text, Data.DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), Data.DbType.String)
        grdViewAssets.DataSource = sp.GetDataSet
        grdViewAssets.DataBind()
        txtRemarks.Text = ""
    End Sub
End Class
