﻿Imports System.Data
Imports System.Data.SqlClient
Partial Class MaintenanceManagement_AMC_CreatePaymentAdvice
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim rid, uid, strsql As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Request.QueryString("rid") <> "" Then
            Session("rid") = Request.QueryString("rid")
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("fmglogout"))
        End If
        rid = Session("rid")
        uid = Session("uid")
        If Not IsPostBack Then

            txtListCount.Text = 0
            lblMsg.Visible = False

            dispdata()
        End If

    End Sub
    Public Sub clearitems()

        lstPendingMemo.Items.Clear()
        cmbPayMode.SelectedIndex = 0
        txtPayAmt.Text = ""
        lblMsg.Text = ""

    End Sub
    Private Sub dispdata()
        Dim ReqId As String = Request("RID")
        Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." & "CREATE_PAYMENT_ADVICE_BY_WO_ID")
        SP.Command.AddParameter("@AMN_PLAN_ID", Request.QueryString("RID"), DbType.String)
        SP.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim dr As SqlDataReader = SP.GetReader()
        If dr.Read() Then
            lblReqId.Text = ReqId
            cboBuilding.Text = dr("LCM_NAME")
            ddlGroup.Text = dr("VT_TYPE")
            ddlgrouptype.Text = dr("AST_SUBCAT_NAME")
            ddlbrand.Text = dr("manufacturer")
            cmbVen.Text = dr("AVR_NAME")

            fillgrid()

        End If
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@Req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")

    End Sub

    Private Sub fillgrid()
        Dim param() As SqlParameter
        If Session("rid") = Request.QueryString("rid") Then
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@amn_plan_id", SqlDbType.NVarChar, 50)
            param(0).Value = Session("rid")
            ObjSubSonic.BindListBox(lstasset, "AMC_GET_ASSETS_ADVISE", "ASSETNAME", "AMN_PLAN_ID", param)

            lstPendingMemo.Items.Clear()
            clearitems()
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@APM_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
            param(0).Value = Session("rid")
            ObjSubSonic.BindListBox(lstPendingMemo, "AMC_GET_PAYMEMO_ID", "APM_PAYMEMO_ID", "APM_PAYMEMO_ID", param)
            If lstPendingMemo.Items.Count = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = " Please Create Payment Memo and Then Proceed..."
                'btnCalc.Visible = False
                Exit Sub
            End If
        Else
            clearitems()
            lstasset.Items.Clear()

        End If
    End Sub

    Protected Sub btnSub_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSub.Click

        Dim param() As SqlParameter

        Dim flag = 0
        Dim rid As String = ""
        Dim uid As String = ""
        rid = Session("rid")
        uid = Session("uid")
        Dim ds As New DataSet

        If flag = 0 Then
            Dim Reqseqid, TmpReqseqid As String
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("GET_AMC_PAYMENT_ADVICE_COUNT")
            If ds.Tables(0).Rows.Count > 0 Then
                Reqseqid = ds.Tables(0).Rows(0).Item(0).ToString
            End If

            If Reqseqid < 10 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/000000" & Reqseqid
            ElseIf Reqseqid < 100 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/00000" & Reqseqid
            ElseIf Reqseqid < 1000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/0000" & Reqseqid
            ElseIf Reqseqid < 10000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/000" & Reqseqid
            ElseIf Reqseqid < 100000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/00" & Reqseqid
            ElseIf Reqseqid < 1000000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/0" & Reqseqid
            ElseIf Reqseqid < 10000000 Then
                TmpReqseqid = Session("uid") & "/AMCPA" & "/" & Reqseqid
            End If

            'REPLACED BY SP

            'lblMsg.Visible = True
            'lblMsg.Text = " Please..."
            ''btnCalc.Visible = False

            Dim selcindx As Integer() = lstPendingMemo.GetSelectedIndices
            If selcindx.Count > 0 Then

                For i As Integer = 0 To selcindx.Count - 1
                    param = New SqlParameter(7) {}
                    param(0) = New SqlParameter("@APA_PAYADVICE_NO", SqlDbType.NVarChar, 50)
                    param(0).Value = TmpReqseqid
                    param(1) = New SqlParameter("@APA_MPWWRKORD_ID", SqlDbType.NVarChar, 50)
                    param(1).Value = Session("rid")
                    param(2) = New SqlParameter("@APA_PAYMENT_MODE", SqlDbType.NVarChar, 50)
                    param(2).Value = cmbPayMode.SelectedItem.Text
                    param(3) = New SqlParameter("@APA_PAYMENT_AMOUNT", SqlDbType.Float)
                    param(3).Value = txtPayAmt.Text
                    param(4) = New SqlParameter("@APA_UPDATED_DT", SqlDbType.DateTime)
                    param(4).Value = getoffsetdatetime(DateTime.Now)
                    param(5) = New SqlParameter("@APA_UPDATED_BY", SqlDbType.NVarChar, 50)
                    param(5).Value = uid
                    param(6) = New SqlParameter("@APA_STATUS", SqlDbType.Int)
                    param(6).Value = 3006
                    param(7) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                    param(7).Value = HttpContext.Current.Session("COMPANYID")
                    ObjSubSonic.GetSubSonicExecute("AMC_INSRT_PAY_ADVICE", param)

                    param = New SqlParameter(2) {}
                    param(0) = New SqlParameter("@APM_MPAPAYADVICE_NO", SqlDbType.NVarChar, 55)
                    param(0).Value = TmpReqseqid
                    param(1) = New SqlParameter("@apm_pay_mode", SqlDbType.NVarChar, 50)
                    param(1).Value = cmbPayMode.SelectedItem.Text
                    param(2) = New SqlParameter("@APM_PAYMEMO_ID", SqlDbType.NVarChar, 50)
                    param(2).Value = lstPendingMemo.Items(selcindx(i)).Value
                    ObjSubSonic.GetSubSonicExecute("AMC_UPDATE_PAY_MEMO", param)
                Next i
            Else

                Exit Sub
            End If
            Response.Redirect("frmAMCfinalpage.aspx?staid=updated&rid=" & TmpReqseqid)
        Else
            Response.Redirect("frmbackpage.aspx?flag=6")
        End If
        'End If
    End Sub


    Protected Sub lstPendingMemo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lstPendingMemo.SelectedIndexChanged


        Dim param1() As SqlParameter
        If lstPendingMemo.SelectedIndex > -1 Then
            Dim i
            Dim Total
            Total = 0
            For i = 0 To lstPendingMemo.Items.Count() - 1
                If lstPendingMemo.Items(i).Selected = True Then
                    param1 = New SqlParameter(0) {}
                    param1(0) = New SqlParameter("@APM_PAYMEMO_ID", SqlDbType.NVarChar, 50)
                    param1(0).Value = lstPendingMemo.Items(i).Value
                    Dim ds1 As New DataSet
                    ds1 = ObjSubSonic.GetSubSonicDataSet("AMC_GET_NET_PAYABLE", param1)
                    If ds1.Tables(0).Rows.Count > 0 Then
                        Total = Total + CInt(ds1.Tables(0).Rows(0).Item(0).ToString)
                    End If

                    txtPayAmt.Text = Total
                End If
            Next i
            txtListCount.Text = CInt(lstPendingMemo.Items.Count())
        Else
            Dim Total1
            Total1 = txtPayAmt.Text

            For i = 0 To lstPendingMemo.Items.Count() - 1

                param1 = New SqlParameter(0) {}
                param1(0) = New SqlParameter("@APM_PAYMEMO_ID", SqlDbType.NVarChar, 50)
                param1(0).Value = lstPendingMemo.Items(i).Value

                Dim ds2 As New DataSet
                ds2 = ObjSubSonic.GetSubSonicDataSet("AMC_GET_NET_PAYABLE", param1)
                If ds2.Tables(0).Rows.Count > 0 Then
                    Total1 = Total1 - CInt(ds2.Tables(0).Rows(0).Item(0).ToString)
                End If

                txtPayAmt.Text = Total1
            Next i

            If CInt(lstPendingMemo.Items.Count()) = 0 Then
                txtPayAmt.Text = 0
            End If
            txtListCount.Text = CInt(lstPendingMemo.Items.Count())

        End If

    End Sub
    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmPaymentAdvice.aspx")
    End Sub

End Class
