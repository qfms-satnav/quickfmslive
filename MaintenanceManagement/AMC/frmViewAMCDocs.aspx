<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewAMCDocs.aspx.vb" Inherits="MaintenanceManagement_AMC_frmViewAMCDocs" Title="View AMC Document" %>

<%@ Register Src="Controls/ViewAMCDocs.ascx" TagName="ViewAMCDocs" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />--%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

    <style>
        .panel-heading h3 {
            font-size: 16px;
        }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="View Maintenance Contract Documents" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">View Maintenance Contract Documents</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="Assetpanel" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowMessageBox="False"
                                ShowSummary="True" CssClass="alert alert-danger"></asp:ValidationSummary>
                            <uc1:ViewAMCDocs ID="ViewAMCDocs1" runat="server" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
        <%-- </div>
            </div>--%>
    </div>
    <%-- </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

