<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AMCWorkOrder.aspx.vb" Inherits="MaintenanceManagement_AMC_Reports_AMCWorkOrder"
    Title="VIew Work Order" %>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
  <%--  <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
               <%-- <div ba-panel ba-panel-title="View Work Order" ba-panel-class="with-scroll">

                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                            <h3 class="panel-title">View/Modify Work Order</h3>

                        </div>
                        <div class="card">
                        <%--<div class="panel-body" style="padding-right: 10px;">--%>
                            <form id="form1" runat="server">
                                <div class="col-md-12">
                                 
                                        <div class="form-group">
                                            <div class="row">
                                                  <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List" style="padding-bottom: 20px"  ForeColor="Red" ValidationGroup="Val1" />
                                                <asp:Label ID="lblMessage" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                   
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                           <%-- <div class="row">--%>
                                                <label class="col-md-5 control-label">Search by  Work Order ID / Location / Vendor<span style="color:red">*</span></label>


                                                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                    Display="none" ErrorMessage="Please Enter Work Order ID / Location / Vendor" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                           <%-- </div>--%>
                                        </div>
                                    </div>
                                    <div class="row col-md-6">
                                        <div class="col-md-12">
                                            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                                                CausesValidation="true" TabIndex="2" />
                                        </div>
                                    </div>
                                </div>
                                <div id="Panel1" runat="server" class="row">
                                    <div class="col-md-12">
                                        <asp:DataGrid ID="grdViewAssets" runat="server" AllowSorting="True" HorizontalAlign="Center" AutoGenerateColumns="False" OnPageIndexChanged="grdViewAssets_Paged"
                                            AllowPaging="True" PageSize="6" CssClass="table GridStyle" GridLines="none" EmptyDataText="No records Found">
                                            <Columns>
                                                <asp:TemplateColumn HeaderText="Work Order Id">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AWO_MAMPLAN_ID", "~/MaintenanceManagement/AMC/Reports/frmAMCWorkOrder.aspx?rid={0}")%>'
                                                            Text='<%# Eval("AWO_MAMPLAN_ID")%> '></asp:HyperLink>
                                                        <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AWO_MAMPLAN_ID")%>' Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn DataField="AMN_PLAN_ID" HeaderText="Maint. Contract Id">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="LCM_NAME" HeaderText="Location">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="AVR_NAME" HeaderText="Vendor">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="AWO_AMC_COST" HeaderText="Cost" DataFormatString="{0:c2}">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="AMN_FROM_DATE" HeaderText="From Date" DataFormatString="{0:d}">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:BoundColumn DataField="AMN_TO_DATE" HeaderText="To Date" DataFormatString="{0:d}">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                </asp:BoundColumn>
                                                <asp:ButtonColumn Text="View" Visible="false" HeaderText="Details" CommandName="ShowDetails">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                </asp:ButtonColumn>
                                                <asp:BoundColumn DataField="STA_DESC" HeaderText="Status">
                                                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                                                </asp:BoundColumn>
                                            </Columns>
                                            <HeaderStyle BackColor="#1c2b36" ForeColor="#E2E2E2" />

                                            <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
                                        </asp:DataGrid>

                                        <div id="pnlbutton" runat="server">

                                            <div class="row">
                                                <div class="col-md-12 text-right">
                                                    <div class="form-group">

                                                        <asp:Button ID="cmdExcel" OnClick="cmdExcel_Click" Text="Export to Excel" runat="server" CssClass="btn btn-primary custom-button-color" Visible="false"></asp:Button>
                                                        <asp:Button ID="btnBack" OnClick="btnBack_Click" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" Visible="false"></asp:Button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>
                    </div>
                </div>
           <%-- </div>
        </div>
    </div>--%>
</body>
</html>
