﻿app.service("ViewAndUpdatePlanMSService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.getdata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ViewAndUpdatePlanMS/getdata', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.DownloadServiceReport = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/ViewAndUpdatePlanMS/DownloadServiceReport', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('ViewAndUpdatePlanMSController', ['$scope', '$q', '$http', 'ViewAndUpdatePlanMSService', 'UtilityService', '$timeout', function ($scope, $q, $http, ViewAndUpdatePlanMSService, UtilityService, $timeout, $filter) {
    $scope.ViewAndUpdate = {};
    $scope.REMARKS1 = [];
    $scope.ACTUALDATE = [];
    $scope.UPSD = [];

    $scope.columDefs = [
        { headerName: "Plan ID", field: "AST_ID", width: 200, cellClass: 'grid-align', filter: 'set', hide: "true" },
        { headerName: "Equipment", field: "AAT_DESC", width: 250, cellClass: 'grid-align', filter: 'set' },
        { headerName: "Vendor", field: "AVR_NAME", width: 150, cellClass: "grid-align" },
        { headerName: "Store", field: "LOCATION", width: 80, cellClass: "grid-align" },
        { headerName: "Plan Type", field: "PLAN_TYPE", width: 90, cellClass: "grid-align" },
        { headerName: "Scheduled Date", field: "ScheduledDate", width: 120, template: '<span>{{data.ScheduledDate | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align" },
        //{ headerName: "Actual Date", field: "data.ActualDate", width: 120, template: '<input type ="date" ng-model="data.ActualDate">', cellClass: 'grid-align' },
        { headerName: "Actual Date", field: "ActualDate", width: 120, cellRenderer: createFromDatePicker },

        { headerName: "Status", field: "STA_TITLE", width: 80, cellClass: "grid-align" },
        //{ headerName: "Upload Service Report", field: "data.myFile", width: 210, template: '<input type="file" ng-model="data.myFile",name="img" multiple>', cellClass: "grid-align" },
        { headerName: "Upload", field: "data.myFile", width: 140, template: '<input type="file" ng-model="data.myFile" name="img" onchange= "angular.element(this).scope().SelectFile(event)" accept = ".png,.jpg,.xlsx,.pdf,.docx" >', cellClass: "grid-align" },
        { headerName: "Remarks", field: "REMARKS", width: 200, template: '<textarea name="message" ng-model="data.REMARKS"> </textarea>', cellClass: "grid-align" },
        { headerName: "Update", field: "Update", width: 90, cellClass: "grid-align", filter: 'set', template: '<input type="submit" value="Update" style="background-color:#099175;border-radius: 5px" ng-click="Update(data)" data-ng-if="data.activeBtnUpd">' },
        { headerName: "Download", field: "Download", width: 70, template: '<a ng-click = "Download(data)" data-ng-if="data.activeBtnDwn"> <i class="fa fa-download class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true },

    ];
    $scope.gridOptions = {
        columnDefs: $scope.columDefs,
        rowData: null,
        enableColResize: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
    };

    function createFromDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.ActualDate');
        newDate.type = "text";
        newDate.readOnly = true;
        newDate.id = params.rowIndex;
        newDate.className = "pickDate form-control";
        newDate.value = params.data.ActualDate;

        $(newDate).datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            //startDate: new Date()
        })

        return newDate;
    }

    //Page Load Function 1 Year Data
    function loadFn() {
        $scope.ViewAndUpdate.FromDate = moment().startOf('year').format('MM/DD/YYYY');
        $scope.ViewAndUpdate.ToDate = moment().endOf('year').format('MM/DD/YYYY');
        GridData();
        activeBtnUpd = true;
        activeBtnDwn = true;
    }

    loadFn();

    //Get Grid Data Function
    function GridData() {
        var dataObj = {
            FromDate: $scope.ViewAndUpdate.FromDate,
            ToDate: $scope.ViewAndUpdate.ToDate
        };
        ViewAndUpdatePlanMSService.getdata(dataObj).then(function (response) {
            if (response.griddata != null) {
                for (var i in response.griddata) {
                    response.griddata[i].activeBtnUpd = true;
                    //if (response.griddata[i].STA_TITLE == "Completed") {
                    if ((response.griddata[i].STA_TITLE == "Completed") && (response.griddata[i].PVD_UPLOADED_DOC != null && response.griddata[i].PVD_UPLOADED_DOC != "")) {

                        //response.griddata[i].activeBtnUpd = true;
                        response.griddata[i].activeBtnDwn = true;
                    }
                    else {
                        //response.griddata[i].activeBtnUpd = true;
                        response.griddata[i].activeBtnDwn = false;
                    }
                }
                $scope.gridOptions.api.setRowData(response.griddata);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                $scope.gridOptions.api.setRowData([]);
            }
            progress(0, 'Loading...', false);
        });
    };

    //Date Filters Search Function
    $scope.GetSearchData = function () {
        progress(0, 'Loading...', true);
        GridData();
    }

    //Excel .csv format Download Function
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "View & Update Plans.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    //Generate Excel Report Function
    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }

    //GridView Hedder Filters
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    //Img Variable For Update DOC
    var img = ""

    //Upload File Selection Click Function
    $scope.SelectFile = function (e) {
        img = e.target.files[0].name;

        var formData = new FormData();
        formData.append("UplFile", e.target.files[0]);
        $http.post(UtilityService.path + '/api/ViewAndUpdatePlanMS/UploadFiles', formData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        })
            .then(function successCallback(response) {
                // this callback will be called asynchronously
                showNotification('success', 8, 'bottom-right', response.data);
                // when the response is available
            }, function errorCallback(response) {
                showNotification('error', 8, 'bottom-right', response.data);
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            })

    };

    //Plan Update Function By Using  ajax calls
    $scope.Update = function (det) {
        if (det.ActualDate && img === "") {
            showNotification('error', 8, 'bottom-right', 'Please select an image to upload.');
            return;
        }
        var dataobj = {
            AST_ID: det.AST_ID,
            AAT_DESC: det.AAT_DESC,
            VT_CODE: det.VT_CODE,
            AST_SUBCAT_CODE: det.AST_SUBCAT_CODE,
            manufactuer_code: det.manufactuer_code,
            AST_MD_CODE: det.AST_MD_CODE,
            AVR_NAME: det.AVR_CODE,
            PLAN_TYPE: det.PLAN_TYPE,
            //FromDate: ActualDate,
            FromDate: moment(det.ActualDate, 'DD-MM-YYYY').format('MM-DD-YYYY'),
            ToDate: '12/31/9999 11:59:59 PM',
            PVD_ID: det.PVD_ID,
            REMARKS: det.REMARKS,
            FILES: img,
            ticked: true
        };
        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/ViewAndUpdatePlanMS/UpdatePPMPlane",
            data: JSON.stringify(dataobj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                GridData();
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', 'Plan Updated Succesfully');
            },
            //error: function () {
            //    showNotification('error', 8, 'bottom-right', 'No Documents Available');
            //}
        });
        //}
    }

    //Image Doenload Function
    $scope.Download = function (det) {
        window.location.assign(UtilityService.path + "/api/ViewAndUpdatePlanMS/DownloadServiceReport?id=" + det.PVD_ID);
    }
}]);