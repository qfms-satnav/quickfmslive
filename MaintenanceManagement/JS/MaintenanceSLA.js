﻿app.service("MaintSLAService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();

    //to save
    this.SaveSLADetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaintenanceSLA/SaveDetails', dataobject)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };
    //update
    this.UpdateSLADetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaintenanceSLA/UpdateDetails', dataobject)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };
    //edit
    this.EditSLADetails = function (dt) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaintenanceSLA/EditSLA/' + dt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };



    //For Timer View
    this.GetSLATime = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaintenanceSLA/GetSLATime')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {

              deferred.reject(response);
              return deferred.promise;

          });
    };

    //For Grid
    this.GetSLAList = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/MaintenanceSLA/GetSLAGrid')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);

app.controller('MaintSLAcontroller', ['$scope', '$q', 'MaintSLAService', '$timeout', 'UtilityService', 'SLAService', '$filter', function ($scope, $q, MaintSLAService, $timeout, UtilityService, SLAService, $filter) {
    $scope.SLAMaster = {};
    $scope.ActionStatus = 0;
    $scope.Countrylist = [];

    $scope.Citylist = [];
    $scope.locationlist = [];

    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    $scope.timetype = [{ value: "1", Name: "Minute(s)" }, { value: "60", Name: "Hour(s)" }, { value: "1440", Name: "Day(s)" }];



    //to bind city on country change
    $scope.CountryChanged = function () {
        var cnyyArray = [];
        cnyyArray.push($scope.SLAMaster.Country[0]);
        UtilityService.getCitiesbyCny(cnyyArray, 2).then(function (data) {
            $scope.Citylist = []
            $scope.locationlist = []
            if (data.data != null) {

                $scope.Citylist = data.data;
            }
        }, function (error) {
            console.log(error);
        });
    }

    //location by city
    $scope.CityChanged = function () {
        var cityArray = [];
        cityArray.push($scope.SLAMaster.City[0]);
        UtilityService.getLocationsByCity(cityArray, 2).then(function (data) {
            $scope.locationlist = []
            if (data.data != null) {

                $scope.locationlist = data.data;
            }
        }, function (error) {
            console.log(error);
        });
    }

    //For Grid column Headers
    var columnDefs = [
         { headerName: "Country", field: "CNY_NAME", cellClass: "grid-align" },
        { headerName: "City", field: "CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
        { headerName: "Edit", suppressMenu: true, template: '<a data-ng-click ="EditSLA(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        UtilityService.getCountires(1).then(function (Cnyresponse) {
            $scope.Countrylist = Cnyresponse.data;
        }, function (error) {
            console.log(error);
        });

        UtilityService.getCities(1).then(function (ctyresponse) {
            $scope.Citylist = ctyresponse.data;

        }, function (error) {
            console.log(error);
        });

        UtilityService.getLocations(1).then(function (locresponse) {
            $scope.locationlist = locresponse.data;

        }, function (error) {
            console.log(error);
        });

        MaintSLAService.GetSLAList().then(function (data) {
            //console.log(data);
            if (data != null) {
                $scope.gridata = data;
                $scope.gridOptions.api.setRowData(data);
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Data not found');
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };
    //$scope.pageSize = '10';


    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1500);

    //To save a record
    $scope.SaveDetails = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            var count = 0;
            var keepGoing = true;
            angular.forEach($scope.SLADetails, function (value) {
                count = 0;
                if (keepGoing) {
                    angular.forEach(value.Value, function (innerval) {
                        if (innerval.Value.SLAD_ESC_TIME != 0) {
                            count = count + 1;
                        }
                    });
                    if (count == 0) {
                        keepGoing = false;
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Data not found Enter atleast one value for the each Status');
                    }
                }
            });
            if (keepGoing) {
                $scope.dataobject = { SLA: $scope.SLAMaster, SLADET: $scope.SLADetails, loclst: $scope.SLAMaster.Location };
                MaintSLAService.UpdateSLADetails($scope.dataobject).then(function (dataobj) {
                    progress(0, '', false);
                    $scope.clear();
                    //console.log(dataobj.data);
                    if (dataobj.data != null) {
                        MaintSLAService.GetSLAList().then(function (data) {
                            $scope.gridata = data;
                            $scope.gridOptions.api.setRowData(data);
                        }, function (error) {
                            console.log(error);
                        });
                        progress(0, '', false);
                        showNotification('success', 8, 'bottom-right', 'Data successfully uploaded');
                    }
                    else {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Existing SLA Contains the Same Information');
                    }

                }, function (response) {
                    progress(0, '', false);
                });
            }
        }
        else {
            var count = 0;
            var keepGoing = true;
            angular.forEach($scope.SLADetails, function (value) {
                count = 0;
                if (keepGoing) {
                    angular.forEach(value.Value, function (innerval) {
                        if (innerval.Value.SLAD_ESC_TIME != 0) {
                            count = count + 1;
                        }
                    });
                    if (count == 0) {
                        keepGoing = false;
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Data not found Enter atleast one value for the each Status');
                    }
                }
            });

            if (keepGoing) {
                $scope.dataobject = { SLA: $scope.SLAMaster, SLADET: $scope.SLADetails, loclst: $scope.SLAMaster.Location };
                MaintSLAService.SaveSLADetails($scope.dataobject).then(function (dataobj) {
                    $scope.LoadData();
                    $scope.clear();
                    progress(0, '', false);
                    showNotification('success', 8, 'bottom-right', dataobj.msg);
                }, function (response) {
                    progress(0, '', false);
                });
            }
        }
    }

    //For Timer view
    $scope.GetSLA = function () {
        MaintSLAService.GetSLATime().then(function (response) {
            $scope.Roles = response.ROLELST;
            $scope.SLADetails = response.SLADET;
        }, function (error) {
        });
    }

    //clear 
    $scope.clear = function () {
        $scope.SLAMaster = {};
        $scope.SLADetails = false;
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmSLA.$submitted = false;
        angular.forEach($scope.Countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.locationlist, function (value, key) {
            value.ticked = false;
        });
    }

    //Edit
    $scope.EditSLA = function (Editdata) {
        $scope.SLAMaster.SLA_ID = Editdata.SLA_ID;
        progress(0, 'Loading...', true);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        var dt = Editdata.SLA_ID;

        angular.forEach($scope.Countrylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Citylist, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.locationlist, function (value, key) {
            value.ticked = false;
        });


        var cny = _.find($scope.Countrylist, { CNY_CODE: Editdata.SLA_CNY_CODE });
        if (cny != undefined) {
            cny.ticked = true;
        }

        var cnyyArra = [];
        var cityArra = [];
        cnyyArra.push(cny);
        UtilityService.getCitiesbyCny(cnyyArra, 2).then(function (data) {
            $scope.Citylist = data.data;
            var cty = _.find($scope.Citylist, { CTY_CODE: Editdata.SLA_CTY_CODE });
            if (cty != undefined) {
                cityArra.push(cty);
                $scope.SLAMaster.City[0] = cty;
                cty.ticked = true;

                UtilityService.getLocationsByCity(cityArra, 2).then(function (data) {
                    $scope.locationlist = [];
                    $scope.locationlist = data.data;
                    var loc = _.find($scope.locationlist, { LCM_CODE: Editdata.SLA_LOC_CODE });
                    if (loc != undefined) {
                        $scope.SLAMaster.Location[0] = loc;
                        loc.ticked = true;
                    }
                });

            }
        });




        MaintSLAService.EditSLADetails(dt).then(function (response) {
            $scope.Roles = response.ROLELST;
            $scope.SLADetails = response.SLADET;
            console.log(response.SLADET);
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });

    }
}]);
