﻿app.service("CreatePlanMSService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.getdata = function (CRPT) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreatePlanMS/getdata', CRPT)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('CreatePlanMSController', ['$scope', '$q', '$http', 'CreatePlanMSService', 'UtilityService', '$timeout', function ($scope, $q, $http, CreatePlanMSService, UtilityService, $timeout, $filter) {
    $scope.CreatePlan = {};
    $scope.REMARKS1 = [];
    $scope.ACTUALDATE = [];
    $scope.UPSD = [];
    $scope.Locations = [];
    $scope.Equipments = [];
    $scope.Vendors = [];

    $scope.PlanTypes = [{ CODE: "1", NAME: "Quarterly" }, { CODE: "2", NAME: "Monthly" }, { CODE: "4", NAME: "Fortnightly" }, { CODE: "5", NAME: "Weekly" }];

    $scope.columDefs = [
        { headerName: "Plan ID", field: "AAT_NAME", width: 200, cellClass: 'grid-align', filter: 'set', hide: "true" },
        { headerName: "Store", field: "LCM_NAME", width: 200, cellClass: "grid-align" },
        { headerName: "Equipment", field: "AST_SUBCAT_NAME", width: 200, cellClass: 'grid-align', filter: 'set' },
        { headerName: "Vendor", field: "AVR_NAME", width: 150, cellClass: "grid-align" },
        {
            headerName: "Plan Type", field: "PlanTypes", cellClass: "grid-align", filter: 'set', suppressMenu: true, width: 150,
            template: "<select  ng-model='data.PlanTypes'><option value=''>--Select--</option><option ng-repeat='type in PlanTypes'  value='{{type.CODE}}'>{{type.NAME}}</option></select>"
        },
        { headerName: "Scheduled Date", field: "ScheduledDate", width: 120, cellRenderer: createFromDatePicker },
        //{ headerName: "Scheduled Date", field: "ScheduledDate", width: 120, template: '<input type ="date" ng-model="data.ScheduledDate">', cellRenderer: createFromDatePicker },
        //{
        //    headerName: "Add/Remove", field: "Download", width: 100,
        //    template: '<button class="btn" style="background-color:#669900" ng-click="add(data)"> <i class="fa fa-plus"></i></button> <button class="btn" style="background-color:#669900"> <i class="fa fa-minus"></i></button> ',
        //    cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true
        //},
        { headerName: "Create", field: "Create", width: 90, cellClass: "grid-align", filter: 'set', template: '<input type="submit" value="Create" style="background-color:#099175;border-radius: 5px" ng-click="Create(data)" data-ng-if="data.activeBtnCreate">' },// onclick="this.disabled=true">
        { headerName: "Update", field: "Update", width: 90, cellClass: "grid-align", filter: 'set', template: '<input type="submit" value="Update" style="background-color:#099175;border-radius: 5px" ng-click="Update(data)" data-ng-if="data.activeBtnUpdate">' },
        { headerName: "Delete", field: "Delete", width: 90, cellClass: "grid-align", filter: 'set', template: '<input type="submit" value="Delete" style="background-color:#099175;border-radius: 5px" ng-click="Delete(data)" data-ng-if="data.activeBtnDelete">' },

    ];
    $scope.gridOptions = {
        columnDefs: $scope.columDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
        enableColResize: true,
    };

    //Page Load Function 1 Year Data

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;
            $scope.Locations.ticked = true;
            angular.forEach($scope.Locations, function (value, key) {
                value.ticked = true;
            });
            UtilityService.GetSubTypes().then(function (response) {
                if (response.data != null) {
                    $scope.Equipments = response.data;
                    $scope.Equipments.ticked = true;
                    angular.forEach($scope.Equipments, function (value, key) {
                        value.ticked = true;
                    });
                    UtilityService.GetVendors().then(function (response) {
                        if (response.data != null) {
                            $scope.Vendors = response.data;
                            $scope.Vendors.ticked = true;
                            angular.forEach($scope.Vendors, function (value, key) {
                                value.ticked = true;
                            });
                            setTimeout(function () {
                                $scope.GridData();
                            }, 3000);
                        }
                    });
                }
            });
        }
    });

    function createFromDatePicker(params) {
        var editing = false;
        var newDate;
        newDate = document.createElement('input');
        newDate.setAttribute('ng-model', 'data.ScheduledDate');
        newDate.type = "text";
        newDate.readOnly = true;
        newDate.id = params.rowIndex;
        newDate.className = "pickDate form-control";
        newDate.value = params.data.ScheduledDate;

        $(newDate).datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            todayHighlight: true,
            /*startDate: new Date()*/
        })
           
        return newDate;

    }

    activeBtnCreate = true;
    activeBtnUpdate = true;
    activeBtnDelete = true;

    $scope.GridData = function () {
        progress(0, 'Loading...', true);
        var params =
        {
            SearchValue: '1',
            selectedLoc: $scope.CreatePlan.Locations,
            Equipments: $scope.CreatePlan.Equipments,
            Vendors: $scope.CreatePlan.Vendors
        }

        CreatePlanMSService.getdata(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata != null) {
                for (var i in data.data) {

                    if (data.data[i].PVD_PLANSTA_ID == "No Plan") {
                        data.data[i].activeBtnUpdate = false;
                        data.data[i].activeBtnCreate = true;
                        data.data[i].activeBtnDelete = false;
                    }
                    else {
                        data.data[i].activeBtnUpdate = true;
                        data.data[i].activeBtnCreate = false;
                        data.data[i].activeBtnDelete = true;
                        data.data[i].PlanTypes = data.data[i].PLAN_TYPE_ID;
                        /*data.data[i].ScheduledDate = data.data[i].ScheduledDate;*/
                    }
                }
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');

            }
            else {
                showNotification('', 8, 'bottom-right', '');
                $scope.gridOptions.api.setRowData([]);
            }
            progress(0, 'Loading...', false);

        });
    }

    //Date Filters Search Function
    $scope.GetSearchData = function () {
        $scope.GridData();
    }

    //Excel .csv format Download Function
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Create Plans.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    //Generate Excel Report Function
    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }

    //GridView Hedder Filters
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })


    //Plan Create Function By Using  ajax calls
    $scope.Create = function (det) {
        if (det.ScheduledDate == undefined || det.ScheduledDate == "") {
            showNotification('error', 8, 'bottom-right', 'Please Select Scheduled Date');
            return;
        }
        else {
            //var ActualDate = moment(new Date(det.ScheduledDate)).format('MM/DD/YYYY');
            var dataobj = {
                AAT_NAME: det.AAT_NAME,
                AAT_CODE: det.AAT_CODE,
                AST_SUBCAT_CODE: det.AST_SUBCAT_CODE,
                AMN_FROM_DATE: det.AMN_FROM_DATE,
                AMN_TO_DATE: det.AMN_TO_DATE,
                LCM_CODE: det.LCM_CODE,
                AVR_CODE: det.AVR_CODE,
                PLAN_TYPE: det.PlanTypes,
                //ScheduledDate: det.ScheduledDate,
                ScheduledDate: moment(det.ScheduledDate, 'DD-MM-YYYY').format('MM-DD-YYYY'),
                PVD_ID: det.PVD_ID,
                PVD_PLAN_ID: det.PVD_PLAN_ID,
                ticked: true
            };
            $.ajax({
                type: "POST",
                url: window.location.origin + "/api/CreatePlanMS/CreatePPMPlane",
                data: JSON.stringify(dataobj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    $scope.GetSearchData();
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', 'Plans Created  Succesfully');
                },
                error: function () {
                    showNotification('error', 8, 'bottom-right', 'Somethig Went Wrong');
                }
            });
        }
    }

    //Plan Update Function By Using  ajax calls
    $scope.Update = function (det) {
        if (det.ScheduledDate == undefined || det.ScheduledDate == "") {
            showNotification('error', 8, 'bottom-right', 'Please Select Scheduled Date');
            return;
        }
        else {
            //var ActualDate = moment(new Date(det.ScheduledDate)).format('MM/DD/YYYY');
            var dataobj = {
                AAT_CODE: det.AAT_CODE,
                AAT_DESC: det.AAT_DESC,
                VT_CODE: det.VT_CODE,
                AST_SUBCAT_CODE: det.AST_SUBCAT_CODE,
                AVR_NAME: det.AVR_CODE,
                PLAN_TYPE: det.PlanTypes,
                //ScheduledDate: det.ScheduledDate,
                ScheduledDate: moment(det.ScheduledDate, 'DD-MM-YYYY').format('MM-DD-YYYY'),
                PVD_ID: det.PVD_ID,
                REMARKS: det.REMARKS,
                ticked: true,
                LCM_CODE: det.LCM_CODE,
            };
            $.ajax({
                type: "POST",
                url: window.location.origin + "/api/CreatePlanMS/UpdatePPMPlane",
                data: JSON.stringify(dataobj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function () {
                    $scope.GetSearchData();
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', 'Plan Updated Succesfully');
                },
                error: function () {
                    showNotification('error', 8, 'bottom-right', 'Somethig Went Wrong');
                }
            });
        }
    }

    //Plan Delete Function By Using  ajax calls
    $scope.Delete = function (det) {
        var dataobj = {
            AAT_CODE: det.AAT_CODE,
            AAT_DESC: det.AAT_DESC,
            VT_CODE: det.VT_CODE,
            AST_SUBCAT_CODE: det.AST_SUBCAT_CODE,
            AVR_NAME: det.AVR_CODE,
            PLAN_TYPE: det.PlanTypes,
            PVD_ID: det.PVD_ID,
            REMARKS: det.REMARKS,
            ticked: true,
            LCM_CODE: det.LCM_CODE,
        };
        $.ajax({
            type: "POST",
            url: window.location.origin + "/api/CreatePlanMS/DeletePPMPlane",
            data: JSON.stringify(dataobj),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function () {
                $scope.GetSearchData();
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', 'Entire Plans Deleted Succesfully');
            },
            error: function () {
                showNotification('error', 8, 'bottom-right', 'Somethig Went Wrong');
            }
        });
    }

}]);