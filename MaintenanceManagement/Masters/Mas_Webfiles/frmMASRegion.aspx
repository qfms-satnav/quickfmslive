<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmMASRegion.aspx.vb" Inherits="Masters_GeneralMasterWebFiles_frmMASRegion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="javascript" type="text/javascript"> 
     function maxLength(s,args)
     { 
     if(args.Value.length >= 500)
     args.IsValid=false;
     }
   
    </script>

    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript" defer></script>

    <div>
        <table id="table1" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
            align="center" border="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Region 
    <hr align="center" width="60%" />
            </asp:Label>
                </td>
            </tr>
        </table>
        <asp:Panel ID="PNLCONTAINER" runat="server" Width="85%" Height="100%">
            <table id="table3" cellspacing="0" cellpadding="0" style="vertical-align: top;" width="95%"
                align="center" border="0">
                <tr>
                    <td width="100%" align="left" colspan="3">
                        <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img height="27" src="../../Images/table_left_top_corner.gif" width="9"></td>
                    <td width="100%" class="tableHEADER" align="left">
                        &nbsp;<strong>Region Details</strong>
                    </td>
                    <td>
                        <img height="27" src="../../Images/table_right_top_corner.gif" width="16"></td>
                </tr>
                <tr>
                    <td background="../../Images/table_left_mid_bg.gif">
                        &nbsp;</td>
                    <td align="left">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                            ForeColor="" ValidationGroup="Val1" /><br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                        <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                            <tr>
                            </tr>
                            <tr>
                                <td align="center" colspan="2" style="height: 43px">
                                    <asp:RadioButtonList ID="rbActions" runat="server" CssClass="clsRadioButton" RepeatDirection="Horizontal"
                                        AutoPostBack="True">
                                        <asp:ListItem Value="0" Selected="True">Add</asp:ListItem>
                                        <asp:ListItem Value="1">Modify</asp:ListItem>
                                    </asp:RadioButtonList></td>
                            </tr>
                            <tr>
                                <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                    &nbsp;Select Country <font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvCountry" runat="server" ControlToValidate="ddlCountry"
                                        Display="None" ErrorMessage="Please Select Country " InitialValue="--Select--"
                                        ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator></td>
                                <td align="left" style="width: 43%; height: 16px">
                                    <asp:DropDownList ID="ddlCountry" runat="server" Width="97%" AutoPostBack="True"
                                        CssClass="clsComboBox" ToolTip="Select Country">
                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr id="trCName" runat="server">
                                <td align="left" class="clslabel" style="height: 11px; width: 50%;">
                                    &nbsp;Select Region <span style="font-size: 8pt; color: #d90000">*</span>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlregion"
                                        Display="None" ErrorMessage="Please Select Region " ValidationGroup="Val1" InitialValue="--Select--">
                                    </asp:RequiredFieldValidator>
                                </td>
                                <td align="left" style="width: 43%; height: 11px">
                                    <asp:DropDownList ID="ddlregion" runat="server" Width="97%" AutoPostBack="True" CssClass="clsComboBox">
                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left" class="clslabel" style="width: 50%; height: 26px;">
                                    &nbsp;Region Code<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvCode" runat="server" ControlToValidate="txtregioncode"
                                        Display="None" ErrorMessage="Please Enter Region Code " ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revCityCode" runat="server" ControlToValidate="txtregioncode"
                                        Display="None" ErrorMessage="Please enter Region code in alphabets and numbers, upto 15 characters allowed"
                                        ValidationExpression="^[A-Za-z0-9]+" ValidationGroup="Val1">
                                    </asp:RegularExpressionValidator>&nbsp;
                                </td>
                                <td align="left" style="width: 43%; height: 26px;">
                                    <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtregioncode" MaxLength="15" runat="server" Width="97%" CssClass="clsTextField">
                                        </asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="clslabel" style="height: 16px; width: 50%;">
                                    &nbsp;Region Name<font class="clsNote">*</font>
                                    <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtregionname"
                                        Display="None" ErrorMessage="Please Enter Region Name " ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revNmae" runat="server" ControlToValidate="txtregionname"
                                        Display="None" ErrorMessage="Please Enter Region Name in alphabets and numbers and (space,-,_ ,(,),, allowed)"
                                        ValidationExpression="^[0-9a-zA-Z-_\/(), ]+" ValidationGroup="Val1">
                                    </asp:RegularExpressionValidator></td>
                                <td align="left" style="height: 16px; width: 43%;">
                                    <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/,, allowed) and upto 50 characters allowed')"
                                        onmouseout="UnTip()">
                                        <asp:TextBox ID="txtregionname" MaxLength="50" runat="server" Width="97%" CssClass="clsTextField">
                                        </asp:TextBox>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="clslabel" style="width: 50%; height: 16px">
                                    &nbsp;Remarks<font class="clsNote">*</font>
                                    <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                        ControlToValidate="txtRemarks" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters "
                                        ValidationGroup="Val1">
                                    </asp:CustomValidator>
                                    <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                        Display="None" ErrorMessage="Please enter Remarks " ValidationGroup="Val1">
                                    </asp:RequiredFieldValidator></td>
                                <td align="left" style="width: 43%; height: 16px">
                                    <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" TextMode="MultiLine"
                                            Width="97%" MaxLength="500" Height="35px">
                                        </asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <table class="table" style="height: 22px" width="100%" border="1">
                            <tr>
                                <td align="center" style="height: 20px">
                                    &nbsp;
                                    <asp:Button ID="btnSubmit" runat="server" Width="76px" CssClass="button" Text="Submit"
                                        ValidationGroup="Val1"></asp:Button>&nbsp;<asp:Button ID="btnback" runat="server"
                                            Width="76px" CssClass="button" Text="Back"> </asp:Button></td>
                            </tr>
                       
                        </table>
                        <br />
                    </td>
                    <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 10px; height: 17px;">
                        <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                    <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                        <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                    <td style="height: 17px">
                        <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>
