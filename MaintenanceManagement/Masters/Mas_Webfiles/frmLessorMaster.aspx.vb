Imports System.Data
Imports System.Data.DataRowView

Partial Class Masters_Mas_Webfiles_frmLessorMaster
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not IsValid Then
            Exit Sub
        End If
        If rbActions.SelectedItem.Value = "Add" Then
            Dim Validate As Integer
            Validate = ValidateLessorCode()
            If Validate = 1 Then
                lblmsg.Text = ""
                AddLessor()
                BindGrid()
                BindLessor()
                'Response.Redirect("../../WorkSpace/frmThanks.aspx?id=17")
                lblmsg.Text = "Lessor Added Successfully"
                Cleardata()
            Else
                lblmsg.Text = "Lessor Code already exists Please Enter another code"
            End If
        ElseIf rbActions.SelectedItem.Value = "Modify" Then
            Modifylessor()
            BindGrid()
            BindLessor()
            'Response.Redirect("../../WorkSpace/frmThanks.aspx?id=18")
            lblmsg.Text = "Lessor Modified Successfully"
            Cleardata()
        End If

    End Sub

    Private Sub Modifylessor()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MODIFY_LESSORS")
        sp.Command.AddParameter("@LESSOR_CODE", cmbPKeys.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LESSOR_NAME", txtName.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_ADDRESS", txtaddress.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_CITY", cmbloc.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LESSOR_PIN", txtpincode.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_OPHNO", txtoffphno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_MPHNO", txtmobilephno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_RPHNO", txtresiphno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_EMAIL", txtemail.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_PAN", txtPANno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_GIR", txtGIRno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_TAN", txtTANno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_ACCNO", txtICICIACno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_LST", txtlstno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_WCT", txtwctno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_CST", txtcstno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_STATUS", ddlstatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Function ValidateLessorCode()
        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_LESSOR")
        sp.Command.AddParameter("@LESSOR_CODE", txtcode.Text, DbType.String)
        valid = sp.ExecuteScalar
        Return valid
    End Function

    Private Sub AddLessor()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_LESSORS")
        sp.Command.AddParameter("@LESSOR_CODE", txtcode.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_NAME", txtName.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_ADDRESS", txtaddress.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_CITY", cmbloc.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@LESSOR_PIN", txtpincode.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_OPHNO", txtoffphno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_MPHNO", txtmobilephno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_RPHNO", txtresiphno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_EMAIL", txtemail.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_PAN", txtPANno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_GIR", txtGIRno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_TAN", txtTANno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_ACCNO", txtICICIACno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_LST", txtlstno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_WCT", txtwctno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_CST", txtcstno.Text, DbType.String)
        sp.Command.AddParameter("@LESSOR_STATUS", ddlstatus.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack() Then
            BindCity()
            BindGrid()
            rbActions.Items.FindByValue("Add").Selected = True
            BindLessor()
            trLessor.Visible = False
        End If
    End Sub

    Private Sub BindLessor()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LESSORS")
        Dim dummy As String = sp.Command.CommandSql()
        cmbPKeys.DataSource = sp.GetDataSet()
        cmbPKeys.DataTextField = "PLR_NAME"
        cmbPKeys.DataValueField = "PLR_CODE"
        'cmbPKeys.DataTextField = "LESSOR_NAME"
        'cmbPKeys.DataValueField = "LESSOR_ID"
        cmbPKeys.DataBind()
        cmbPKeys.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LESSORS")
        Dim dummy As String = sp.Command.CommandSql
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_CITY")
        sp.Command.AddParameter("@dummy", "")
        Dim dummy As String = sp.Command.CommandSql
        cmbloc.DataSource = sp.GetDataSet()

        cmbloc.DataTextField = "CTY_NAME"
        cmbloc.DataValueField = "CTY_CODE"
        cmbloc.DataBind()
        cmbloc.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        If rbActions.SelectedItem.Value = "Add" Then
            Cleardata()
            lblmsg.Text = ""
            trLessor.Visible = False
            txtcode.ReadOnly = False

        Else
            Cleardata()
            txtcode.ReadOnly = True
            trLessor.Visible = True
            cmbPKeys_SelectedIndexChanged(Me, EventArgs.Empty)

        End If
    End Sub

    Private Sub Cleardata()
        cmbPKeys.SelectedIndex = -1
        txtcode.Text = ""
        txtName.Text = ""
        txtaddress.Text = ""
        cmbloc.SelectedIndex = 0
        txtpincode.Text = ""
        txtoffphno.Text = ""
        txtmobilephno.Text = ""
        txtresiphno.Text = ""
        txtemail.Text = ""
        txtPANno.Text = ""
        txtGIRno.Text = ""
        txtTANno.Text = ""
        txtICICIACno.Text = ""
        txtlstno.Text = ""
        txtwctno.Text = ""
        txtcstno.Text = ""
        ddlstatus.SelectedIndex = -1
    End Sub

    Protected Sub cmbPKeys_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbPKeys.SelectedIndexChanged
        ' Cleardata()
        BindDetails()
        lblmsg.Text = ""

        'If cmbPKeys.SelectedIndex > 0 Then
        '    BindDetails()
        'Else
        '    Cleardata()
        'End If
    End Sub

    Private Sub BindDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_DETAILS_LESSOR")
        sp.Command.AddParameter("@LESSOR_CODE", cmbPKeys.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            txtcode.Text = ds.Tables(0).Rows(0).Item("PLR_CODE")
            txtName.Text = ds.Tables(0).Rows(0).Item("PLR_NAME")
            txtaddress.Text = ds.Tables(0).Rows(0).Item("PLR_ADDRESS")
            cmbloc.ClearSelection()
            cmbloc.Items.FindByValue(ds.Tables(0).Rows(0).Item("PLR_CITY")).Selected = True
            txtpincode.Text = ds.Tables(0).Rows(0).Item("PLR_PINCODE")
            txtoffphno.Text = ds.Tables(0).Rows(0).Item("PLR_OFFICE_PHNO")
            txtmobilephno.Text = ds.Tables(0).Rows(0).Item("PLR_MOBILE_PHNO")
            txtresiphno.Text = ds.Tables(0).Rows(0).Item("PLR_RESIDENCE_PHNO")
            txtemail.Text = ds.Tables(0).Rows(0).Item("PLR_EMAIL")
            txtPANno.Text = ds.Tables(0).Rows(0).Item("PLR_PAN_NO")
            txtGIRno.Text = ds.Tables(0).Rows(0).Item("PLR_GIR_NO")
            txtTANno.Text = ds.Tables(0).Rows(0).Item("PLR_TAN_NO")
            txtICICIACno.Text = ds.Tables(0).Rows(0).Item("PLR_BANKAC_NO")
            txtlstno.Text = ds.Tables(0).Rows(0).Item("PLR_LST_NO")
            txtwctno.Text = ds.Tables(0).Rows(0).Item("PLR_WCT_NO")
            txtcstno.Text = ds.Tables(0).Rows(0).Item("PLR_CST_NO")
            ddlstatus.ClearSelection()
            ddlstatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("PLR_STA_ID")).Selected = True

        End If
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    'Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
    '    If e.CommandName = "Stat" Then
    '        Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
    '        Dim lbllcode As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lbllcode"), Label)
    '        Dim lnkbtnstatus As LinkButton = DirectCast(gvitems.Rows(rowIndex).FindControl("lnkbtnstatus"), LinkButton)
    '        Dim stat As Integer = 0
    '        If lnkbtnstatus.Text = "ACTIVE" Then
    '            stat = 1
    '        Else
    '            stat = 0
    '        End If
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"UPDATE_LESSOR_STATUS")
    '        sp.Command.AddParameter("@LESSOR_CODE", lbllcode.Text, DbType.String)
    '        sp.Command.AddParameter("@STAT", stat, DbType.String)
    '        sp.ExecuteScalar()
    '        BindGrid()
    '    End If
    'End Sub


    Protected Sub btnback_Click(sender As Object, e As EventArgs) Handles btnback.Click
        Response.Redirect("~/MaintenanceManagement/Masters/Mas_Webfiles/frmPropMasters.aspx")
    End Sub
End Class
