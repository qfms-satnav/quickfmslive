<%@ Page Language="VB" AutoEventWireup="false" Title="Maintenance Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script language="javascript" type="text/javascript" defer>
        function noway(go) {
            if (document.all) {
                if (event.button == 2) {
                    alert('For Security reasons Right click option was disabled,Inconvenience regreted!');
                    return false;
                }
            }
            if (document.layers) {
                if (go.which == 3) {
                    alert('For Security reasons Right click option was disabled,,Inconvenience regreted!');
                    return false;
                }
            }
        }
        if (document.layers) {
            document.captureEvents(Event.MOUSEDOWN);
        }
        document.onmousedown = noway;
    </script>

</head>
<body>
    <div class="al-content">
        <div class="widgets">
            <h3>Maintenance Masters</h3>
        </div>
        <div class="card" style="padding-right: 50px;">
            <form id="form1" runat="server">
                <div class="box-body">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <asp:HyperLink ID="HyperLink9" runat="server" role="button" class="btn btn-block btn-primary" NavigateUrl="~/MaintenanceManagement/Views/MaintenanceSLA.aspx">Maintenance SLA</asp:HyperLink>
                    </div>

                    <%--<div class="col-md-6 ">
                                <div class="form-group">

                                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/MaintenanceManagement/MaintenanceMaster/frmAssetGroupsType.aspx">Asset Group Type Master</asp:HyperLink>
                                </div>

                            </div>--%>
                </div>
                <%--<div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink4" runat="server" NavigateUrl="~/MaintenanceManagement/MaintenanceMaster/frmAssetBrands.aspx"> Asset Brand Master</asp:HyperLink>
                                </div>

                            </div>

                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink6" runat="server" NavigateUrl="~/MaintenanceManagement/MaintenanceMaster/frmAssetModels.aspx">Asset Model Master</asp:HyperLink>
                                </div>

                            </div>
                        </div>--%>
                <%--  <div class="row">
                            <div class="col-md-6 ">
                                <div class="form-group">
                                    <asp:HyperLink ID="HyperLink11" runat="server" NavigateUrl="~/MaintenanceManagement/Masters/Mas_Webfiles/frmMasVendor.aspx">Vendor Master</asp:HyperLink></td>
                                </div>

                            </div>

                            <div class="col-md-6 ">
                                <div class="form-group">

                                    <asp:HyperLink ID="hlkFloor" runat="server" NavigateUrl="~/MaintenanceManagement/Masters/Mas_Webfiles/frmAssetMaster.aspx">Asset Master</asp:HyperLink>
                                </div>

                            </div>
                        </div>--%>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

