<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmPropMasters.aspx.vb" Inherits="Masters_Mas_Webfiles_frmPropMasters"
    Title="Property Masters" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body>

    <div class="al-content">
        <div class="widgets">
            <h3>Property Masters</h3>
        </div>
        <div class="card">
            <form id="form1" runat="server">
                <div class="box-body">
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/WorkSpace/SMS_Webfiles/frmAddPropertyType.aspx">Add Property Type</asp:HyperLink>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <asp:HyperLink ID="HyperLink5" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/ExpenseHead.aspx">Expense Head</asp:HyperLink>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <asp:HyperLink ID="HyperLink6" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/LeaseEscalation.aspx">Lease Escalation</asp:HyperLink>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <asp:HyperLink ID="HyperLink2" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/PropertyManagement/Views/ServiceType.aspx">Service Type</asp:HyperLink>
                        </div>
                    </div>
                    <br />
                    <%-- <div class="clearfix">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <asp:HyperLink ID="HyperLink3" runat="server" class="btn btn-block btn-primary" NavigateUrl="~/Masters/CheckList_Masters/Views/ProjectTypeMaster.aspx">Project Type</asp:HyperLink>
                                    </div>
                                </div>--%>
                </div>
            </form>
        </div>
    </div>
</body>
</html>

