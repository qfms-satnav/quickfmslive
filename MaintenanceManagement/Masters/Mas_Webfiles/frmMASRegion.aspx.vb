Imports System.Data
Imports System.Data.SqlClient
Partial Class Masters_GeneralMasterWebFiles_frmMASRegion
    Inherits System.Web.UI.Page
    Dim obj As clsMasters = New clsMasters

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblMsg.Text = ""
        Try
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            End If
            If Not Page.IsPostBack Then
                'BindGrid()
                trCName.Visible = False
                rbActions.Items(0).Selected = True

                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
            'Throw New AmantraAxis.Exception.DataException("Error has been occured while retrieving data from database", "frmRegion", "Page_Load", exp)
        End Try
    End Sub
    'Private Sub BindGrid()
    '    Try
    '        lblMsg.Text = ""
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_GET_REGION")
    '        gvItem.DataSource = sp.GetDataSet()
    '        gvItem.DataBind()
    '        For i As Integer = 0 To gvItem.Rows.Count - 1
    '            Dim lblstatus As LinkButton = CType(gvItem.Rows(i).FindControl("lblstatus"), LinkButton)
    '            If lblstatus.Text = "1" Then
    '                lblstatus.Text = "Active"
    '            Else
    '                lblstatus.Text = "Inactive"
    '            End If
    '        Next


    '    Catch ex As Exception
    '        lblMsg.Text = ex.Message
    '    End Try
    'End Sub
    Private Sub BindRegion()
        Try
            lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_GET_REGION_ACTIVE")
            sp.Command.AddParameter("@COUNTRY", ddlCountry.SelectedItem.Value, DbType.String)
            ddlregion.DataSource = sp.GetDataSet()
            ddlregion.DataTextField = "RGN_NAME"
            ddlregion.DataValueField = "RGN_CODE"
            ddlregion.DataBind()
            ddlregion.Items.Insert(0, New ListItem("--Select--", "--Select--"))

        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub
    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbActions.SelectedIndexChanged
        Try
            Dim i As Integer
            For i = 0 To rbActions.Items.Count - 1
                If rbActions.Items(i).Selected = True Then
                    If rbActions.Items(i).Text = "Add" Then
                        trCName.Visible = False
                        txtregioncode.ReadOnly = False
                        btnSubmit.Text = "Submit"
                        Cleardata()
                    ElseIf rbActions.Items(i).Text = "Modify" Then
                        trCName.Visible = True
                        txtregioncode.ReadOnly = True
                        btnSubmit.Text = "Modify"
                        BindRegion()
                        obj.BindCountry(ddlCountry)
                    End If
                End If
            Next
        Catch exp As System.Exception
            'Throw New AmantraAxis.Exception.DataException("Error has been occured while retrieving data from database", "frmRegion", "rbActions_SelectedIndexChanged", exp)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strEroorMsg As String = String.Empty
        Try
            If rbActions.SelectedItem.Text = "Add" Then
                strEroorMsg = "Error has been occured while inserting data"
                btnSubmit.Text = "Submit"
                If txtregioncode.Text = String.Empty Or txtregionname.Text = String.Empty Or txtRemarks.Text = String.Empty Or ddlCountry.SelectedItem.Text = "--Select--" Then
                    PopUpMessage("Please Enter Mandatory fields", Me)
                ElseIf txtRemarks.Text.Length > 500 Then
                    PopUpMessage("Please Enter remarks in less than or equal to 500 characters", Me.Page)
                Else
                    InsertData()
                    'BindGrid()
                    Cleardata()
                    lblMsg.Text = "Region Added Succesfully"
                End If
            ElseIf rbActions.SelectedItem.Text = "Modify" Then
                strEroorMsg = "Error has been occured while Updating data"
                btnSubmit.Text = "Modify"
                If ddlregion.SelectedItem.Value = "--Select--" Or txtregioncode.Text = String.Empty Or txtregionname.Text = String.Empty Or txtRemarks.Text = String.Empty Or ddlCountry.SelectedItem.Text = "--Select--" Then
                    PopUpMessage("Please Enter Mandatory fields", Me)
                ElseIf txtRemarks.Text.Length > 500 Then
                    PopUpMessage("Please Enter remarks in less than or equal to 500 characters", Me.Page)
                Else
                    ModifyData()
                    'BindGrid()
                    Cleardata()
                    lblMsg.Text = "Region Modified Succesfully"
                End If
            End If
        Catch exp As System.Exception
            'Throw New AmantraAxis.Exception.DataException(strEroorMsg, "frmRegion", "btnSubmit_Click", exp)
        End Try
    End Sub

    Protected Sub ddlregion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlregion.SelectedIndexChanged
        Try
            If ddlregion.SelectedItem.Value <> "--Select--" Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"SP_GET_REGION_COUNTRY")
                sp.Command.AddParameter("@COUNTRY", ddlCountry.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@REGION_CODE", ddlregion.SelectedItem.Value, DbType.String)
                Dim ds As New DataSet()
                ds = sp.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    txtregioncode.Text = ds.Tables(0).Rows(0).Item("RGN_CODE")
                    txtregionname.Text = ds.Tables(0).Rows(0).Item("RGN_NAME")
                    txtRemarks.Text = ds.Tables(0).Rows(0).Item("RGN_REM")
                End If
            Else
                Cleardata()
                BindRegion()
                obj.BindCountry(ddlCountry)
            End If
        Catch exp As System.Exception
          '  Throw New AmantraAxis.Exception.DataException("Error has been occured while retrieving data from database", "frmRegion", "ddlregion_SelectedIndexChanged", exp)
        End Try
    End Sub
    Private Sub Cleardata()

        ddlCountry.SelectedIndex = 0
        ddlregion.Items.Clear()
        txtregioncode.Text = ""
        txtregionname.Text = ""
        txtRemarks.Text = ""
    End Sub
    Private Sub InsertData()
        Try
            lblMsg.Text = ""
            Dim validrgncode As Integer
            validrgncode = Validateregioncode()
            If validrgncode = 0 Then
                lblMsg.Text = "Region code already exists"
            Else
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_REGION")
                sp.Command.AddParameter("@REGION_CODE", txtregioncode.Text, DbType.String)
                sp.Command.AddParameter("@REGION_NAME", txtregionname.Text, DbType.String)
                sp.Command.AddParameter("@COUNTRY", ddlCountry.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@REGION_REMARKS", txtRemarks.Text, DbType.String)
                sp.ExecuteScalar()

            End If
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub
    Private Function Validateregioncode()
        Dim validcode As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"VALIDATE_REGION_CODE")
        sp.Command.AddParameter("@REGION_CODE", txtregioncode.Text, DbType.String)
        sp.Command.AddParameter("@COUNTRY", ddlCountry.SelectedItem.Value, DbType.String)
        validcode = sp.ExecuteScalar()
        Return validcode
    End Function
    Private Sub ModifyData()
        Try
            lblMsg.Text = ""

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MODIFY_REGION")
            sp.Command.AddParameter("@COUNTRY", ddlCountry.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@REGION_CODE", txtregioncode.Text, DbType.String)
            sp.Command.AddParameter("@REGION_NAME", txtregionname.Text, DbType.String)
            sp.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String)
            sp.ExecuteScalar()


        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        Try
            BindRegion()
        Catch ex As Exception
            lblMsg.Text = ex.Message
        End Try
    End Sub

    
    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("frmMASRegionlist.aspx")
    End Sub
End Class
