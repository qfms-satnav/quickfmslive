<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmPVMUpdateDetails.aspx.vb" Culture="hi-IN" Inherits="MaintenanceManagement_PMC_frmPVMUpdateDetails" %>

<%@ Register Src="Controls/PVMUpdateDetails.ascx" TagName="PVMUpdateDetails" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'dd-mm-yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Update Plan Details</h3>
            </div>
            <div class="card">
                <%--  <div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server" />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="True"
                        ForeColor="Red" ShowMessageBox="false"></asp:ValidationSummary>
                    <uc1:PVMUpdateDetails ID="PVMUpdateDetails1" runat="server"></uc1:PVMUpdateDetails>
                </form>
            </div>
        </div>
    </div>
    <%--  </div>
    </div></div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
