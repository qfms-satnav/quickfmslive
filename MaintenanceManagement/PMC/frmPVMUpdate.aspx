<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmPVMUpdate.aspx.vb" Inherits="MaintenanceManagement_PMC_frmPVMUpdate" %>

<%@ Register Src="Controls/PVMUpdate.ascx" TagName="PVMUpdate" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        .panel-title {
            font-size: 16px;
        }
    </style>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Update Plan</h3>
            </div>
            <div class="card">
                <%-- <div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" class="form-horizontal " runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server"
                        ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" Style="padding-bottom: 20px" />
                    <uc1:PVMUpdate ID="PVMUpdate1" runat="server" />
                </form>
            </div>
        </div>
    </div>
    <%-- </div>
    </div></div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
