<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PVMUpdate.ascx.vb" Inherits="MaintenanceManagement_PMC_Controls_PVMUpdate" %>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <label class="col-md-6 control-label">Search by PM Plan Id/Location Name<span style="color: red; padding-right: 34px">*</span></label>
                    <asp:RequiredFieldValidator ID="rfAssetName" runat="server" ControlToValidate="txtAssetName"
                        Display="none" ErrorMessage="Please Enter PM Plan Id/Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtAssetName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                    CausesValidation="true" TabIndex="2" />
            </div>
        </div>
    </div>
</div>
<%--style="margin-top: 10px"--%>
<div class="row">
    <div class="col-md-12">
        <asp:DataGrid ID="PM_REQ_DATA" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Update Plan Found."
            PageSize="15" CssClass="table GridStyle" GridLines="none">
            <Columns>
                <asp:HyperLinkColumn DataNavigateUrlField="PVM_PLAN_ID" DataNavigateUrlFormatString="../frmPVMUpdateDetails.aspx?rid={0}"
                    DataTextField="mPVM_PLAN_ID" HeaderText="PM Plan Id">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:HyperLinkColumn>
                <asp:BoundColumn DataField="VENDOR" HeaderText="Vendor">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="BDG_NAME" HeaderText="Location">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="NAME" HeaderText="Asset Type/Name">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="PVM_PLAN_FREQ" HeaderText="Plan Type">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                </asp:BoundColumn>
                <asp:TemplateColumn  HeaderText="Download Checklist">
                    <HeaderStyle CssClass="clsTblHead"></HeaderStyle>
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDownload" Text='<%# If(Eval("VT_UPLOADED_DOC").ToString() = "NA", "NA", "Download")%>' CommandArgument='<%# Eval("VT_UPLOADED_DOC")%>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>

            </Columns>
            <HeaderStyle ForeColor="white" BackColor="Black" />
            <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
        </asp:DataGrid>
        <%--<asp:GridView ID="PM_REQ_DATA" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No Update Plan Found."
            PageSize="15" CssClass="table table-condensed table-bordered table-hover table-striped">
            <Columns>
                  
        

                <asp:TemplateField HeaderText="PM Plan ID" ItemStyle-HorizontalAlign="left">
                    
                    <ItemTemplate>
                        <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("PVM_PLAN_ID", "../frmPVMUpdateDetails.aspx?rid={0}")%>'
                            Text='<%#Eval("PVM_PLAN_ID")%>'></asp:HyperLink>
                        <asp:Label ID="lblPlanid" runat="server" Text='<%#Eval("PVM_PLAN_ID")%>' Visible="false"></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                  <asp:TemplateField HeaderText="Vendor" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblVendor" Text='<%#Eval("VENDOR")%>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblLocation" Text='<%#Eval("BDG_NAME")%>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asset Category" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblAssetName" Text='<%#Eval("NAME")%>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                  <asp:TemplateField HeaderText="Asset Sub Category" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblAssetSubcatName" Text='<%#Eval("AST_SUBCAT_NAME")%>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                                
                <asp:TemplateField HeaderText="Plan Type" ItemStyle-HorizontalAlign="left">
                    <ItemTemplate>
                        <asp:Label ID="lblPlanReq" Text='<%#Eval("PVM_PLAN_FREQ")%>' runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Download Checklist">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkDownload" Text='<%# If(Eval("VT_UPLOADED_DOC").ToString() = "NA", "", "Download")%>' CommandArgument='<%# Eval("VT_UPLOADED_DOC")%>' runat="server" OnClick="DownloadFile"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

               
            </Columns>
            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
            <PagerStyle CssClass="pagination-ys" />
        </asp:GridView>--%>
    </div>
</div>




