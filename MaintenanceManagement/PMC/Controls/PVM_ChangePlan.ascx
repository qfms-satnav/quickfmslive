<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PVM_ChangePlan.ascx.vb"
    Inherits="MaintenanceManagement_PMC_Controls_PVM_ChangePlan" %>
<div class="row">
        <div class="col-md-12">
    <div class="col-md-8">
        <div class="form-group">
            <div class="row">
                <label class="col-md-6 control-label">Search by PM Plan ID/Location Name / Vendor Name<span style="color: red;padding-right:34px">*</span></label>
                <asp:RequiredFieldValidator ID="rfAssetName1" runat="server" ControlToValidate="txtAssetName1"
                    Display="none" ErrorMessage="Please Enter PPM Plan ID/Location Name / Vendor Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                <div class="col-md-4">
                    <asp:TextBox ID="txtAssetName1" runat="server" CssClass="form-control" ValidationGroup="Val1"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="col-md-12">
            <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search" ValidationGroup="Val1"
                CausesValidation="true" TabIndex="2"  />
        </div>
    </div>
</div></div>
<div class="row">
   <div class="col-md-12">
        <asp:DataGrid ID="dgPvmReq" runat="server" PageSize="15" AllowPaging="True" AllowSorting="True" HorizontalAlign="Center" AutoGenerateColumns="False" EmptyDataText="No Change Plan Found."
            CssClass="table GridStyle" GridLines="none">
            <Columns>
                <asp:HyperLinkColumn DataNavigateUrlField="PVM_PLAN_ID" ItemStyle-HorizontalAlign="left"
                    DataNavigateUrlFormatString="../frmPVMAmenDetails.aspx?rid={0}" DataTextField="mPVM_PLAN_ID"
                    HeaderText="PM Plan Id">
                    <HeaderStyle CssClass="tableTHColor"></HeaderStyle>
                </asp:HyperLinkColumn>
                <asp:BoundColumn DataField="vendor" HeaderText="Vendor">
                    <HeaderStyle CssClass="tableTHColor"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="BDG_NAME" HeaderText="Location">
                    <HeaderStyle CssClass="tableTHColor"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="NAME" HeaderText="Asset Type/Name">
                    <HeaderStyle CssClass="tableTHColor"></HeaderStyle>
                </asp:BoundColumn>
                <asp:BoundColumn DataField="PVM_PLAN_FREQ" HeaderText="Plan Type">
                    <HeaderStyle CssClass="tableTHColor"></HeaderStyle>
                </asp:BoundColumn>
            </Columns>
         <HeaderStyle ForeColor="#0f3a9f" BackColor="#ebf0fb" />
            <PagerStyle NextPageText="Next" PrevPageText="Previous" Position="Top"></PagerStyle>
        </asp:DataGrid>
    </div>
</div>


