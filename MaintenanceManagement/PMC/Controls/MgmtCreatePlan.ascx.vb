Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_PMC_Controls_MgmtCreatePlan
    Inherits System.Web.UI.UserControl

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim strUid As String
    Dim rid As String
    Dim arparam() As SqlParameter
    Dim param() As SqlParameter
    Dim result As String


    Private Sub Fill_BFT_Combos()
        'ObjSubSonic.Binddropdown(cboBuilding, "PVM_GET_BDG_GROLE", "BDG_NAME", "BDG_ADM_CODE")
        ObjSubSonic.Binddropdown(cboBuilding, "MN_CREATE_AMC_GET_LOCATIONS", "LCM_NAME", "LCM_CODE")
    End Sub

    Public Sub clearitems()
        ddlbrand.Items.Clear()
        ddlGroup.Items.Clear()
        ddlgrouptype.Items.Clear()
        cboVendor11.Items.Clear()
        cboContract.Items.Clear()

    End Sub

    Private Sub Disable_All()
        panAssetService.Visible = False
        PanPremise.Visible = True
        PanSelAssets.Visible = False
        panPeriod.Visible = False
        panAllPeriods.Visible = False
    End Sub

    Private Sub BindBuilding()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@USER_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID").ToString
        ' ObjSubSonic.Binddropdown(cboBuilding, "AMC_GETBUILDING", "BDG_NAME", "BDG_ADM_CODE")
        ObjSubSonic.Binddropdown(cboBuilding, "MN_GET_ALL_LOCATIONS", "LCM_NAME", "LCM_CODE", param)
    End Sub

    Private Sub Disable_All_Premise()
        PanPremise.Visible = True
        panAssetService.Visible = False
        PanSelAssets.Visible = False
        panPeriod.Visible = False
        panAllPeriods.Visible = False
    End Sub

    Private Sub Disable_All_AssetService(ByVal Type)
        PanSelAssets.Visible = False
        panPeriod.Visible = False
        panAllPeriods.Visible = False
        pnlCon.Visible = False
    End Sub

    Sub LoadVendorsByContractID(ByVal ContractID As String)
        'ConInfo.Visible = True
        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
        param(0).Value = ContractID

        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("PVM_GET_VNDR_DTLS_BY_CONTRACT_ID", param)
        If ds.Tables(0).Rows.Count > 0 Then
            'txtConName.Text = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
            'txtConStart.Text = ds.Tables(0).Rows(0).Item("AAP_AMC_FROMDT").ToString
            'txtConTo.Text = ds.Tables(0).Rows(0).Item("AAP_AMC_TODT").ToString
            'lblMsg.Visible = False
            'lblMsg.Text = ""
            'result = "T"

            venName.InnerText = "Vendor Name : " + ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
            NoOfYears.InnerText = "No of Years : " + ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString
            ConStartDate.InnerText = "Contract Start Date : " + ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            ConEndDate.InnerText = "Contract End Date    :" + ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
            'venName.InnerText = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
            'NoOfYears.InnerText = ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString
            'ConStartDate.InnerText = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            'ConEndDate.InnerText = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString


            txtConName.Text = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString

            'txtConStart.Text = ds.Tables(0).Rows(0).Item("AAP_AMC_FROMDT").ToString
            'txtConTo.Text = ds.Tables(0).Rows(0).Item("AAP_AMC_TODT").ToString
            txtConStart.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            txtConTo.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
            txtNoofYears.Text = ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString

            txtFromDate.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            txtToDate.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
            lblAmcFromDT.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            lblAmcToDT.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
            lblMsg.Visible = False
            lblMsg.Text = ""
            result = "T"
        Else
            txtConName.Text = ""
            txtConStart.Text = ""
            txtConTo.Text = ""
            txtNoofYears.Text = ""

            lblMsg.Visible = True
            lblMsg.Text = "Use AMC for the Selected Asset"
            result = "F"
            Exit Sub
        End If
    End Sub
    Sub LoadVendors(ByVal i As String, ByVal ContractID As String)
        'ConInfo.Visible = True
        param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@AAP_SNO", SqlDbType.NVarChar, 200)
        param(0).Value = i

        param(1) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 200)
        param(1).Value = ContractID

        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("PVM_GET_VNDR_DTLS_BY_ASNO", param)
        If ds.Tables(0).Rows.Count > 0 Then
            'txtConName.Text = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
            'txtConStart.Text = ds.Tables(0).Rows(0).Item("AAP_AMC_FROMDT").ToString
            'txtConTo.Text = ds.Tables(0).Rows(0).Item("AAP_AMC_TODT").ToString
            'lblMsg.Visible = False
            'lblMsg.Text = ""
            'result = "T"

            'venName.InnerText = "Vendor Name : " + ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
            'NoOfYears.InnerText = "No of Years : " + ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString
            'ConStartDate.InnerText = "Contract Start Date : " + ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            'ConEndDate.InnerText = "Contract End Date    :" + ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
            'venName.InnerText = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
            'NoOfYears.InnerText = ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString
            'ConStartDate.InnerText = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            'ConEndDate.InnerText = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString


            txtConName.Text = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString

            'txtConStart.Text = ds.Tables(0).Rows(0).Item("AAP_AMC_FROMDT").ToString
            'txtConTo.Text = ds.Tables(0).Rows(0).Item("AAP_AMC_TODT").ToString
            txtConStart.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            txtConTo.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
            txtNoofYears.Text = ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString

            txtFromDate.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
            txtToDate.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString

            lblMsg.Visible = False
            lblMsg.Text = ""
            result = "T"
            'Else
            '    txtConName.Text = ""
            '    txtConStart.Text = ""
            '    txtConTo.Text = ""

            '    lblMsg.Visible = True
            '    lblMsg.Text = "Use AMC for the Selected Asset"
            '    result = "F"
            Exit Sub
        End If
    End Sub

    Private Function Check_Dates()
        Dim fd, td, maindate, tempdate As Date
        Dim diffmm, i, diffyy, dd, mm, yy, dd1, mm1, yy1, dd2, mm2, dd3, mm3, dd4, mm4, flag, dateflag
        fd = CDate(txtFromDate.Text)
        td = CDate(txtToDate.Text)
        dateflag = 0

        If radWeekly.Checked = True Then
            flag = 1
            tempdate = fd
            While flag = 1
                If cboWWeek.SelectedItem.Value = DatePart(DateInterval.Weekday, tempdate) Then
                    flag = 2
                Else
                    tempdate = DateAdd(DateInterval.Day, 1, tempdate)
                End If
            End While
            fd = tempdate
            maindate = fd
            For i = 1 To DateDiff(DateInterval.Day, fd, td)
                If maindate >= fd And maindate <= td Then
                    dateflag = 1
                    Exit For
                End If
                maindate = DateAdd(DateInterval.Day, 7, maindate)
            Next

        ElseIf radMonthly.Checked = True Then

            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm = Month(fd)
            yy = Year(fd)
            dd = cboMDate.SelectedItem.Value

            If mm = 12 Then
                mm1 = 1
                yy1 = yy + 1
            Else
                mm1 = mm + 1
                yy1 = yy
            End If

            For i = 0 To diffmm

                maindate = mm & "/1/" & yy
                tempdate = mm1 & "/1/" & yy1
                If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                    maindate = mm & "/" & dd & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm = 12 Then
                    mm = 1
                    yy = yy + 1
                Else
                    mm = mm + 1
                End If

                If mm1 = 12 Then
                    mm1 = 1
                    yy1 = yy1 + 1
                Else
                    mm1 = mm1 + 1
                End If
            Next

        ElseIf radBiMonthly.Checked = True Then
            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm = cboBMonth.SelectedItem.Value
            yy = Year(fd)
            dd = cboBday.SelectedItem.Value

            If mm = 12 Then
                mm1 = 1
                yy1 = yy + 1
            Else
                mm1 = mm + 1
                yy1 = yy
            End If

            For i = 0 To diffmm

                maindate = mm & "/1/" & yy
                tempdate = mm1 & "/1/" & yy1
                If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                    maindate = mm & "/" & dd & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm = 12 Then
                    mm = 2
                    yy = yy + 1
                ElseIf mm = 11 Then
                    mm = 1
                    yy = yy + 1
                Else
                    mm = mm + 2
                End If

                If mm = 12 Then
                    mm1 = 1
                    yy1 = yy + 1
                Else
                    mm1 = mm + 1
                    yy1 = yy
                End If
            Next

        ElseIf radQuarterly.Checked = True Then
            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm1 = cboQ1Month.SelectedItem.Value
            dd1 = cboQ1Date.SelectedItem.Value

            mm2 = cboQ2Month.SelectedItem.Value
            dd2 = cboQ2Date.SelectedItem.Value

            mm3 = cboQ3Month.SelectedItem.Value
            dd3 = cboQ3Date.SelectedItem.Value

            mm4 = cboQ4Month.SelectedItem.Value
            dd4 = cboQ4Date.SelectedItem.Value

            mm = Month(fd)
            yy = Year(fd)
            For i = 0 To diffmm

                If mm = mm1 Then
                    maindate = mm1 & "/" & dd1 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If
                If mm = mm2 Then
                    maindate = mm2 & "/" & dd2 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If
                If mm = mm3 Then
                    maindate = mm3 & "/" & dd3 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If
                If mm = mm4 Then
                    maindate = mm4 & "/" & dd4 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm = 12 Then
                    mm = 1
                    yy = yy + 1
                Else
                    mm = mm + 1
                End If
            Next

        ElseIf radYearly.Checked = True Then
            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm = cboYMonth.SelectedItem.Value
            yy = Year(fd)
            dd = cboYDate.SelectedItem.Value

            mm1 = cboYMonth.SelectedItem.Value

            For i = 0 To diffmm

                If mm = mm1 Then
                    maindate = mm & "/" & dd & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm1 = 12 Then
                    mm1 = 1
                    yy = yy + 1
                Else
                    mm1 = mm1 + 1
                End If
            Next

        ElseIf radDaily.Checked = True Then
            maindate = fd
            For i = 0 To DateDiff(DateInterval.Day, fd, td)
                If maindate >= fd And maindate <= td Then
                    dateflag = 1
                    Exit For
                End If
                maindate = DateAdd(DateInterval.Day, 1, maindate)
            Next
        ElseIf radhalfyearly.Checked = True Then
            If Year(td) <> Year(fd) Then
                diffmm = 12 - Month(fd)
                diffyy = Year(td) - Year(fd)
                If diffyy = 1 Then
                    diffmm = diffmm + Month(td)
                Else
                    diffmm = diffmm + (diffyy * 12) + Month(td)
                End If
            Else
                diffmm = Month(td) - Month(fd)
            End If

            mm1 = H1_DrpDwnMonth.SelectedItem.Value
            dd1 = H1_DrpDwnDate.SelectedItem.Value

            mm2 = H2_DrpDwnMonth.SelectedItem.Value
            dd2 = H2_DrpDwnDate.SelectedItem.Value

            mm = Month(fd)
            yy = Year(fd)
            For i = 0 To diffmm

                If mm = mm1 Then
                    maindate = mm1 & "/" & dd1 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If
                If mm = mm2 Then
                    maindate = mm2 & "/" & dd2 & "/" & yy
                    If maindate >= fd And maindate <= td Then
                        dateflag = 1
                        Exit For
                    End If
                End If

                If mm = 12 Then
                    mm = 1
                    yy = yy + 1
                Else
                    mm = mm + 1
                End If
            Next
        End If
        Check_Dates = dateflag
    End Function

    Private Function Check_Plan_Id(ByVal planid As String)
        Dim insertflag
        insertflag = 1
        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@PLANID", SqlDbType.NVarChar, 200)
        param(0).Value = planid
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("GET_PVM_MAIN_PLANID", param)
        If CInt(ds.Tables(0).Rows(0).Item("CNT")) > 0 Then
            insertflag = 0
        End If
        Check_Plan_Id = insertflag
    End Function

    Private Function Check_Assets()
        Dim assetflag
        assetflag = 1
        Dim fd, td As Date
        fd = CDate(txtFromDate.Text)
        td = CDate(txtToDate.Text)
        Dim i
        For i = 0 To lstSelAssets.Items.Count - 1
            Dim str As String
            str = lstSelAssets.Items(i).Text

            param = New SqlParameter(6) {}
            param(0) = New SqlParameter("@PVM_BDG_ID", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@PVM_FLR_ID", SqlDbType.NVarChar, 200)
            param(1).Value = ""
            param(2) = New SqlParameter("@PVM_TWR_ID", SqlDbType.NVarChar, 200)
            param(2).Value = ""
            param(3) = New SqlParameter("@PVM_ASSET_CODE", SqlDbType.NVarChar, 200)
            param(3).Value = lstSelAssets.SelectedItem.Value
            param(4) = New SqlParameter("@FDATE", SqlDbType.DateTime)
            param(4).Value = fd
            param(5) = New SqlParameter("@TDATE", SqlDbType.DateTime)
            param(5).Value = td
            param(6) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
            param(6).Value = lstSelAssets.Items(i).Value


            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("PVM_GET_PVM_PLANID", param)
            If ds.Tables(0).Rows.Count > 0 Then
                assetflag = 0
                Exit For
            End If

        Next
        Check_Assets = assetflag
    End Function

    Private Sub Enable_All()
        panAssetService.Visible = True
        PanPremise.Visible = True
        PanSelAssets.Visible = True
        panPeriod.Visible = True
        panAllPeriods.Visible = True
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("Uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If

        strUid = Session("Uid")
        Session("rid") = strUid & getoffsetdatetime(DateTime.Now).ToString.Replace("/", "").Replace(" ", "").Replace("PM", "").Replace("AM", "").Replace(":", "")
        rid = Session("rid")
        txtFromDate.Attributes.Add("readonly", "readonly")
        txtToDate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack = True Then
            If Session("ViewOrModify") = "1" Then
                btnSubmit.Enabled = False
            End If
            BindBuilding()
            Disable_All()
            'Fill_BFT_Combos()
            lblMsg.Visible = False
            If cboBuilding.Items.Count = 1 Then

                lblMsg.Visible = True
                lblMsg.Text = "There are no assets available for preventive maintenance. "
                pnlContainer.Visible = False
            End If
            txtHiddenDate.Text = FormatDateTime(Now, DateFormat.ShortDate)
            'If cboBuilding.Items.Count = 1 Then
            '    lblMsg.Visible = True
            '    lblMsg.Text = "Assets Not Available For AMC !"
            '    Exit Sub
            'End If
        End If
        'txtFromDate.Attributes.Add("onClick", "displayDatePicker('" + txtFromDate.ClientID + "')")
        'txtFromDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        'txtToDate.Attributes.Add("onClick", "displayDatePicker('" + txtToDate.ClientID + "')")
        'txtToDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    End Sub

    Protected Sub cboBuilding_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboBuilding.SelectedIndexChanged
        PanSelAssets.Visible = False
        lblMsg.Visible = False
        venName.InnerText = ""
        NoOfYears.InnerText = ""
        ConStartDate.InnerText = ""
        ConEndDate.InnerText = ""
        Disable_All_Premise()
        If cboBuilding.SelectedItem.Value <> "--Select--" Then
            If cboBuilding.SelectedItem.Value <> "--All--" Then
                Dim arparam() As SqlParameter = New SqlParameter(0) {}
                'arparam(0) = New SqlParameter("@BDG_ID", SqlDbType.Int, 4)
                arparam(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
                arparam(0).Value = cboBuilding.SelectedItem.Value
                clearitems()
                'ObjSubSonic.Binddropdown(ddlGroup, "GET_ASSET_GROUP_BLDG", "GROUP_NAME", "GROUP_ID", arparam)
                ObjSubSonic.Binddropdown(ddlGroup, "MN_CREATE_GET_ASSET_GROUP_BY_LOCATION", "GROUP_NAME", "GROUP_ID", arparam)
                'ObjSubSonic.Binddropdown(cboVendor11, "GET_VENDORS", "AVR_NAME", "AVR_CODE", arparam)
            End If
        Else
            clearitems()
            'cboVendor11.Items.Clear()
            'ddlGroup.Items.Clear()
            'ddlgrouptype.Items.Clear()
            'ddlbrand.Items.Clear()
            'cboVendor11.Items.Insert("0", "--Select--")
            lblMsg.Visible = False
        End If
    End Sub

    'End If
    'End Sub

    Protected Sub ddlGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlGroup.SelectedIndexChanged
        PanSelAssets.Visible = False
        venName.InnerText = ""
        NoOfYears.InnerText = ""
        ConStartDate.InnerText = ""
        ConEndDate.InnerText = ""

        Disable_All_Premise()
        If ddlGroup.SelectedIndex <> 0 Then
            Dim param(1) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            lblMsg.Visible = False
            ddlbrand.Items.Clear()
            ddlgrouptype.Items.Clear()
            cboVendor11.Items.Clear()
            cboContract.Items.Clear()
            'ObjSubSonic.Binddropdown(ddlgrouptype, "GET_ASSET_GROUPTYPE_BLDGGROOUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
            ObjSubSonic.Binddropdown(ddlgrouptype, "MN_GET_ASSET_GROUPTYPE_BY_LOCATION_GROUP", "GROUPTYPE_NAME", "GROUPTYPE_ID", param)
        Else
            cboVendor11.Items.Clear()
            ddlgrouptype.Items.Clear()
            ddlbrand.Items.Clear()
            cboContract.Items.Clear()
        End If
    End Sub

    Protected Sub ddlgrouptype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlgrouptype.SelectedIndexChanged
        PanSelAssets.Visible = False
        venName.InnerText = ""
        NoOfYears.InnerText = ""
        ConStartDate.InnerText = ""
        ConEndDate.InnerText = ""
        Disable_All_Premise()
        If ddlgrouptype.SelectedIndex <> 0 Then
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            lblMsg.Visible = False
            ddlbrand.Items.Clear()
            cboVendor11.Items.Clear()
            cboContract.Items.Clear()
            'ObjSubSonic.Binddropdown(ddlbrand, "GET_ASSET_BRAND_BLDGGROOUP", "BRAND_NAME", "BRAND_ID", param)
            ObjSubSonic.Binddropdown(ddlbrand, "MN_GET_ASSETBRAND_LOCGRUPTYPE", "BRAND_NAME", "BRAND_ID", param)
        Else
            ddlbrand.Items.Clear()
            cboVendor11.Items.Clear()
            cboContract.Items.Clear()
            'ddlbrand.ClearSelection()
            'cboVendor11.ClearSelection()
        End If
    End Sub

    Protected Sub ddlbrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlbrand.SelectedIndexChanged
        PanSelAssets.Visible = False
        venName.InnerText = ""
        NoOfYears.InnerText = ""
        ConStartDate.InnerText = ""
        ConEndDate.InnerText = ""
        Disable_All_Premise()


        Try


            If ddlbrand.SelectedIndex <> 0 Then
                If Rbtwith.Checked = True Then
                    Dim param(3) As SqlParameter
                    param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
                    param(0).Value = cboBuilding.SelectedItem.Value
                    param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
                    param(1).Value = ddlGroup.SelectedItem.Value
                    param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
                    param(2).Value = ddlgrouptype.SelectedItem.Value
                    param(3) = New SqlParameter("@VEND_ID", SqlDbType.NVarChar, 200)
                    param(3).Value = ddlbrand.SelectedItem.Value
                    lblMsg.Visible = False
                    cboVendor11.Items.Clear()
                    cboContract.Items.Clear()
                    'ObjSubSonic.Binddropdown(cboVendor11, "GET_VENDORS", "AVR_NAME", "AVR_CODE", param)
                    'ObjSubSonic.Binddropdown(cboVendor11, "GET_ASSET_VENDOR_BLDGGROOUPBR", "VENDOR_NAME", "VENDOR_ID", param)
                    ObjSubSonic.Binddropdown(cboVendor11, "MN_GET_ASSET_VENDOR_BY_LOCGRUPBRND", "VENDOR_NAME", "VENDOR_ID", param)
                    If cboVendor11.Items.Count = 1 Then
                        cboVendor11.Items.Insert(0, "NA")
                        cboContract.Items.Insert(0, "--Select--")
                    End If
                    Dim param1(0) As SqlParameter
                    param1(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
                    param1(0).Value = Session("UID")
                    If cboVendor11.Items.Count = 1 Then
                        cboVendor11.Items.Clear()
                        ObjSubSonic.Binddropdown(cboVendor11, "MM_GET_VENDORS_LIST_SERVICE", "AVR_NAME", "AVR_CODE", param1)
                    End If
                ElseIf Rbtwithout.Checked = True Then
                    cboVendor11.Items.Clear()
                    WithCon.Visible = False
                    WithoutCon.Visible = True
                    'con.Visible = False
                    lstDisAssets.Items.Clear()
                    lstSelAssets.Items.Clear()
                    PanSelAssets.Visible = False
                    pnlCon.Visible = False
                    panPeriod.Visible = False
                    panAllPeriods.Visible = False
                    txtConName.Text = ""
                    txtConStart.Text = ""
                    txtConTo.Text = ""
                    txtNoofYears.Text = ""
                    venName.InnerText = ""
                    NoOfYears.InnerText = ""
                    ConStartDate.InnerText = ""
                    ConEndDate.InnerText = ""
                    lblMsg.Visible = False
                    btnRight.Enabled = True
                    'pnlCon.Visible = True
                    'Dim MyDateTime As DateTime = Now()
                    param = New SqlParameter(3) {}
                    param(0) = New SqlParameter("@LOC_ID", SqlDbType.NVarChar, 200)
                    param(0).Value = cboBuilding.SelectedItem.Value
                    param(1) = New SqlParameter("@AAG_CODE", SqlDbType.NVarChar, 200)
                    param(1).Value = ddlGroup.SelectedItem.Value
                    param(2) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 200)
                    param(2).Value = ddlgrouptype.SelectedItem.Value
                    param(3) = New SqlParameter("@AAB_CODE", SqlDbType.NVarChar, 200)
                    param(3).Value = ddlbrand.SelectedItem.Value
                    ddlVendorWithoutCon.Items.Clear()
                    ObjSubSonic.Binddropdown(ddlVendorWithoutCon, "MM_GET_VENDORS_LIST_SERVICE", "AVR_NAME", "AVR_CODE")

                    Dim ds As New DataSet
                    ds = ObjSubSonic.GetSubSonicDataSet("MM_GET_VNDR_ASSET_DETAILS_BY_SEARCH", param)
                    lstDisAssets.DataSource = ds
                    lstDisAssets.DataTextField = "AAT_NAME"
                    lstDisAssets.DataValueField = "AAT_CODE"
                    lstDisAssets.DataBind()
                    If ds.Tables(0).Rows.Count > 0 Then
                        PanSelAssets.Visible = True
                        venName.InnerText = "Vendor Name : " + ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
                        NoOfYears.InnerText = "No of Years : " + ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString
                        ConStartDate.InnerText = "Contract Start Date : " + ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
                        ConEndDate.InnerText = "Contract End Date    :" + ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString

                        txtConName.Text = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
                        txtConStart.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
                        txtConTo.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
                        txtNoofYears.Text = ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString
                        Dim MyDateTime As DateTime = Now()
                        txtFromDate.Text = MyDateTime.ToString("MM/dd/yyyy")
                        'Response.Write(txtFromDate.Text)
                        Dim newDate As DateTime = DateTime.Now.AddYears(1)
                        txtToDate.Text = newDate.ToString("MM/dd/yyyy")
                        lblAmcFromDT.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
                        lblAmcToDT.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
                        lblMsg.Visible = False
                        lblMsg.Text = ""
                        result = "T"
                        'Vend.Visible = True
                        pnlCon.Visible = True
                        panAssetService.Visible = True
                        PanPremise.Visible = True
                        PanSelAssets.Visible = True
                        radWeekly.Checked = False
                        radBiMonthly.Checked = False
                        radMonthly.Checked = False
                        radQuarterly.Checked = False
                        radYearly.Checked = False
                        radDaily.Checked = False
                        panAllPeriods.Visible = False

                        'If cboVendor12.SelectedItem.Value = 1 Then
                        '    cboVendor12.Items.Clear()
                        '    ObjSubSonic.Binddropdown(cboVendor12, "MM_GET_VENDORS_LIST_SERVICE", "AVR_NAME", "AVR_CODE")
                        'End If
                    Else
                        txtConName.Text = ""
                        txtConStart.Text = ""
                        txtConTo.Text = ""
                        txtNoofYears.Text = ""
                        lblMsg.Visible = True
                        lblMsg.Text = "No Assets Found For Without Contract"
                        result = "F"
                        Exit Sub
                    End If

                End If
            Else
                cboVendor11.Items.Clear()
                cboContract.Items.Clear()
                'cboVendor11.ClearSelection()
            End If
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Protected Sub cboVendor11_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboVendor11.SelectedIndexChanged
        PanSelAssets.Visible = False

        pnlCon.Visible = False
        PanSelAssets.Visible = False

        pnlCon.Visible = False
        panPeriod.Visible = False
        panAllPeriods.Visible = False
        venName.InnerText = ""
        NoOfYears.InnerText = ""
        ConStartDate.InnerText = ""
        ConEndDate.InnerText = ""

        If ddlbrand.SelectedIndex <> 0 Then
            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@BLDG", SqlDbType.NVarChar, 200)
            param(0).Value = cboBuilding.SelectedItem.Value
            param(1) = New SqlParameter("@GRPID", SqlDbType.NVarChar, 200)
            param(1).Value = ddlGroup.SelectedItem.Value
            param(2) = New SqlParameter("@GRPTYPID", SqlDbType.NVarChar, 200)
            param(2).Value = ddlgrouptype.SelectedItem.Value
            param(3) = New SqlParameter("@BRANDID", SqlDbType.NVarChar, 200)
            param(3).Value = ddlbrand.SelectedItem.Value
            param(4) = New SqlParameter("@VENDOR_ID", SqlDbType.NVarChar, 200)
            param(4).Value = cboVendor11.SelectedItem.Value
            param(5) = New SqlParameter("@COMPANY_ID", SqlDbType.Int)
            param(5).Value = Session("COMPANYID")
            lblMsg.Visible = False
            cboContract.Items.Clear()
            ObjSubSonic.Binddropdown(cboContract, "MN_GET_CONTRACTS_BY_VENDOR", "AMN_PLAN_ID", "AMN_PLAN_ID", param)
            'If cboContract.Items.Count = 1 Then

            '    lblMsg.Visible = True
            '    lblMsg.Text = "Please Create contract for this vendor..."
            'End If

        Else
            cboContract.Items.Clear()
        End If


        'lstDisAssets.Items.Clear()
        'lstSelAssets.Items.Clear()
        'PanSelAssets.Visible = False
        'pnlCon.Visible = False
        'panPeriod.Visible = False
        'panAllPeriods.Visible = False
        'txtConName.Text = ""
        'txtConStart.Text = ""
        'txtConTo.Text = ""
        'lblMsg.Visible = False
        'btnRight.Enabled = True
        'If cboVendor11.SelectedItem.Text <> "--Select--" Then

        '    arparam = New SqlParameter(1) {}
        '    arparam(0) = New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 50)
        '    arparam(0).Value = cboBuilding.SelectedItem.Value
        '    arparam(1) = New SqlParameter("@AAP_VENDOR", SqlDbType.NVarChar, 50)
        '    arparam(1).Value = cboVendor11.SelectedItem.Value
        '    ObjSubSonic.BindListBox(lstDisAssets, "GET_PVM_VENDORASSETS", "AAP_CODE", "AAP_RUNNO", arparam)
        '    If lstDisAssets.Items.Count > 0 Then
        '        panPeriod.Visible = True
        '        pnlCon.Visible = True
        '        panAssetService.Visible = True
        '        PanPremise.Visible = True
        '        PanSelAssets.Visible = True
        '        radWeekly.Checked = False
        '        radBiMonthly.Checked = False
        '        radMonthly.Checked = False
        '        radQuarterly.Checked = False
        '        radYearly.Checked = False
        '        radDaily.Checked = False
        '        panAllPeriods.Visible = False
        '    Else
        '        panPeriod.Visible = False
        '        radWeekly.Checked = False
        '        radBiMonthly.Checked = False
        '        radMonthly.Checked = False
        '        radQuarterly.Checked = False
        '        radYearly.Checked = False
        '        radDaily.Checked = False
        '        panAllPeriods.Visible = False
        '    End If
        'Else
        '    lstDisAssets.Items.Clear()
        '    lstSelAssets.Items.Clear()
        '    PanSelAssets.Visible = False
        '    pnlCon.Visible = False
        '    panPeriod.Visible = False
        '    panAllPeriods.Visible = False
        'End If
    End Sub

    Protected Sub btnRight_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRight.Click
        lstSelAssets.ClearSelection()

        If lstDisAssets.SelectedIndex > -1 Then
            Dim i
            For i = 0 To lstDisAssets.Items.Count() - 1
                If lstDisAssets.Items(i).Selected = True Then
                    lstSelAssets.ClearSelection()
                    lstSelAssets.Items.Add(lstDisAssets.Items(i))
                    If lstSelAssets.Items.Count() > 0 Then
                        panPeriod.Visible = True
                        'pnlCon.Visible = True
                        radWeekly.Checked = False
                        radBiMonthly.Checked = False
                        radMonthly.Checked = False
                        radQuarterly.Checked = False
                        radYearly.Checked = False
                        radDaily.Checked = False
                        panAllPeriods.Visible = False
                        'LoadVendors(lstSelAssets.SelectedItem.Value)
                        If Rbtwith.Checked = True Then
                            LoadVendors(lstSelAssets.SelectedItem.Value, cboContract.SelectedItem.Value)

                        Else
                            LoadVendors(lstSelAssets.SelectedItem.Value, ddlVendorWithoutCon.SelectedItem.Value)
                        End If


                        If result = "F" Then
                            btnRight.Enabled = True
                            Exit For
                        ElseIf result = "T" Then
                            btnRight.Enabled = True
                        End If
                    Else
                        panPeriod.Visible = False
                        radWeekly.Checked = False
                        radBiMonthly.Checked = False
                        radMonthly.Checked = False
                        radQuarterly.Checked = False
                        radYearly.Checked = False
                        radDaily.Checked = False
                        panAllPeriods.Visible = False
                    End If
                End If

            Next i
            For i = 0 To lstSelAssets.Items.Count() - 1
                'If lstSelAssets.Items(i).Selected = True Then
                lstDisAssets.Items.Remove(lstSelAssets.Items(i))
                'End If
            Next i
        End If
        'If lstSelAssets.Items.Count() > 0 Then
        '    panPeriod.Visible = True
        '    pnlCon.Visible = True
        '    radWeekly.Checked = False
        '    radBiMonthly.Checked = False
        '    radMonthly.Checked = False
        '    radQuarterly.Checked = False
        '    radYearly.Checked = False
        '    radDaily.Checked = False
        '    panAllPeriods.Visible = False
        '    LoadVendors()
        'Else
        '    panPeriod.Visible = False
        '    radWeekly.Checked = False
        '    radBiMonthly.Checked = False
        '    radMonthly.Checked = False
        '    radQuarterly.Checked = False
        '    radYearly.Checked = False
        '    radDaily.Checked = False
        '    panAllPeriods.Visible = False
        'End If

    End Sub

    Protected Sub btnLeft_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLeft.Click
        lblMsg.Visible = False
        'pnlCon.Visible = False
        CheckBoxList1.ClearSelection()

        lblMsg.Text = ""
        lstDisAssets.ClearSelection()
        btnRight.Enabled = True
        If lstSelAssets.SelectedIndex > -1 Then
            Dim i
            For i = 0 To lstSelAssets.Items.Count() - 1
                If lstSelAssets.Items(i).Selected = True Then
                    lstDisAssets.Items.Add(lstSelAssets.Items(i))
                End If
            Next i
            For i = 0 To lstDisAssets.Items.Count() - 1
                If lstDisAssets.Items(i).Selected = True Then
                    lstSelAssets.Items.Remove(lstDisAssets.Items(i))
                End If
            Next i
        End If
        If lstSelAssets.Items.Count() > 0 Then
            panPeriod.Visible = True
            radWeekly.Checked = False
            radBiMonthly.Checked = False
            radMonthly.Checked = False
            radQuarterly.Checked = False
            radYearly.Checked = False
            radDaily.Checked = False
            panAllPeriods.Visible = False
        Else
            panPeriod.Visible = False
            radWeekly.Checked = False
            radBiMonthly.Checked = False
            radMonthly.Checked = False
            radQuarterly.Checked = False
            radYearly.Checked = False
            radDaily.Checked = False
            panAllPeriods.Visible = False
            txtConName.Text = ""
            txtConStart.Text = ""
            txtConTo.Text = ""
            txtNoofYears.Text = ""

        End If
    End Sub

    Protected Sub radDaily_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radDaily.CheckedChanged
        lblMsg.Visible = False
        CheckBoxList1.ClearSelection()
        radWeekly.Checked = False
        radBiMonthly.Checked = False
        radMonthly.Checked = False
        radQuarterly.Checked = False
        radYearly.Checked = False
        radDaily.Checked = True

        radhalfyearly.Checked = False
        panHalfYearly.Visible = False
        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = False
        panBiMonthly.Visible = False
        panQuarterly.Visible = False
        panYearly.Visible = False
        panDaily.Visible = True

        cboHour.Items.Clear()
        Dim i
        For i = 0 To 23
            cboHour.Items.Add(i)
        Next

    End Sub

    Protected Sub radWeekly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radWeekly.CheckedChanged
        lblMsg.Visible = False
        CheckBoxList1.ClearSelection()

        radWeekly.Checked = True

        radBiMonthly.Checked = False
        radMonthly.Checked = False
        radQuarterly.Checked = False
        radYearly.Checked = False
        radDaily.Checked = False
        radhalfyearly.Checked = False
        panHalfYearly.Visible = False
        panAllPeriods.Visible = True
        panWeekly.Visible = True
        panMonthly.Visible = False
        panBiMonthly.Visible = False
        panQuarterly.Visible = False
        panYearly.Visible = False
        panDaily.Visible = False

    End Sub

    Protected Sub radMonthly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radMonthly.CheckedChanged
        lblMsg.Visible = False
        CheckBoxList1.ClearSelection()
        radWeekly.Checked = False
        radBiMonthly.Checked = False
        radMonthly.Checked = True
        radQuarterly.Checked = False
        radYearly.Checked = False
        radDaily.Checked = False
        radMonthly.ForeColor = Drawing.Color.DarkOrange
        radhalfyearly.Checked = False
        panHalfYearly.Visible = False
        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = True
        panBiMonthly.Visible = False
        panQuarterly.Visible = False
        panYearly.Visible = False
        panDaily.Visible = False

        Dim i
        cboMDate.Items.Clear()
        For i = 1 To 31
            cboMDate.Items.Add(i)
        Next
    End Sub

    Protected Sub radBiMonthly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radBiMonthly.CheckedChanged
        lblMsg.Visible = False
        CheckBoxList1.ClearSelection()
        radWeekly.Checked = False
        radBiMonthly.Checked = True
        radMonthly.Checked = False
        radQuarterly.Checked = False
        radYearly.Checked = False
        radDaily.Checked = False
        radhalfyearly.Checked = False
        panHalfYearly.Visible = False
        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = False
        panBiMonthly.Visible = True
        panQuarterly.Visible = False
        panYearly.Visible = False
        panDaily.Visible = False

        Dim i
        cboBday.Items.Clear()
        For i = 1 To 31
            cboBday.Items.Add(i)
        Next

    End Sub

    Protected Sub radQuarterly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radQuarterly.CheckedChanged
        lblMsg.Visible = False
        CheckBoxList1.ClearSelection()
        radWeekly.Checked = False
        radBiMonthly.Checked = False
        radMonthly.Checked = False
        radQuarterly.Checked = True
        radYearly.Checked = False
        radDaily.Checked = False
        radhalfyearly.Checked = False
        panHalfYearly.Visible = False
        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = False
        panBiMonthly.Visible = False
        panQuarterly.Visible = True
        panYearly.Visible = False
        panDaily.Visible = False

        Dim i
        cboQ1Date.Items.Clear()
        For i = 1 To 30
            cboQ1Date.Items.Add(i)
        Next

        cboQ2Date.Items.Clear()
        For i = 1 To 31
            cboQ2Date.Items.Add(i)
        Next

        cboQ3Date.Items.Clear()
        For i = 1 To 31
            cboQ3Date.Items.Add(i)
        Next

        cboQ4Date.Items.Clear()
        For i = 1 To 31
            cboQ4Date.Items.Add(i)
        Next

    End Sub
    Private Sub radHalfYearly_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radhalfyearly.CheckedChanged
        Try
            lblMsg.Visible = False
            radWeekly.Checked = False
            radBiMonthly.Checked = False
            radMonthly.Checked = False
            radQuarterly.Checked = False
            radYearly.Checked = False
            radDaily.Checked = False
            radhalfyearly.Checked = True
            panHalfYearly.Visible = True
            panAllPeriods.Visible = True
            panWeekly.Visible = False
            panMonthly.Visible = False
            panBiMonthly.Visible = False
            panQuarterly.Visible = False
            'Response.Write(panQuarterly.Visible)
            panYearly.Visible = False
            panDaily.Visible = False

            Dim i
            H1_DrpDwnDate.Items.Clear()
            For i = 1 To 30
                H1_DrpDwnDate.Items.Add(i)
            Next

            H2_DrpDwnDate.Items.Clear()
            For i = 1 To 31
                H2_DrpDwnDate.Items.Add(i)
            Next

        Catch ex As Exception
            'Dim pageURL, trace, MSG As String
            Session("pageURL") = ex.GetBaseException.ToString
            Session("trace") = (ex.StackTrace.ToString)
            Session("MSG") = Session("pageURL") + Session("Trace")
            Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
        End Try

    End Sub
    Protected Sub radYearly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radYearly.CheckedChanged
        lblMsg.Visible = False
        CheckBoxList1.ClearSelection()
        radWeekly.Checked = False
        radBiMonthly.Checked = False
        radMonthly.Checked = False
        radQuarterly.Checked = False
        radYearly.Checked = True
        radDaily.Checked = False
        radhalfyearly.Checked = False
        panHalfYearly.Visible = False
        panAllPeriods.Visible = True
        panWeekly.Visible = False
        panMonthly.Visible = False
        panBiMonthly.Visible = False
        panQuarterly.Visible = False
        panYearly.Visible = True
        panDaily.Visible = False

        Dim i
        cboYDate.Items.Clear()
        For i = 1 To 31
            cboYDate.Items.Add(i)
        Next

    End Sub
    Private Sub H1_DrpDwnMonth_SelectedIndexChanged1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles H1_DrpDwnMonth.SelectedIndexChanged

        Dim i, j As Int16
        j = H1_DrpDwnMonth.SelectedIndex
        Dim dt As DateTime
        H1_DrpDwnDate.Items.Clear()

        Select Case j
            Case 0
                For i = 1 To 31
                    H1_DrpDwnDate.Items.Add(i)

                Next
            Case 1
                dt = txtToDate.Text
                If dt.IsLeapYear(dt.Year) = True Then
                    For i = 1 To 29
                        H1_DrpDwnDate.Items.Add(i)
                    Next
                Else
                    For i = 1 To 28
                        H1_DrpDwnDate.Items.Add(i)
                    Next

                End If
            Case 2
                For i = 1 To 31
                    H1_DrpDwnDate.Items.Add(i)

                Next
            Case 3
                For i = 1 To 30
                    H1_DrpDwnDate.Items.Add(i)
                Next
            Case 4
                For i = 1 To 31
                    H1_DrpDwnDate.Items.Add(i)
                Next
            Case 5
                For i = 1 To 30
                    H1_DrpDwnDate.Items.Add(i)
                Next
            Case 6
                For i = 1 To 31
                    H1_DrpDwnDate.Items.Add(i)
                Next
            Case 7
                For i = 1 To 31
                    H1_DrpDwnDate.Items.Add(i)
                Next
            Case 8
                For i = 1 To 30
                    H1_DrpDwnDate.Items.Add(i)
                Next
            Case 9
                For i = 1 To 31
                    H1_DrpDwnDate.Items.Add(i)
                Next
            Case 10
                For i = 1 To 30
                    H1_DrpDwnDate.Items.Add(i)
                Next
            Case 11
                For i = 1 To 31
                    H1_DrpDwnDate.Items.Add(i)
                Next
        End Select
    End Sub

    Protected Sub H2_DrpDwnDate_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim i, j As Int16
        j = H2_DrpDwnMonth.SelectedIndex
        H2_DrpDwnDate.Items.Clear()

        Select Case j
            Case 0
                For i = 1 To 31
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 1
                For i = 1 To 31
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 2
                For i = 1 To 30
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 3
                For i = 1 To 31
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 4
                For i = 1 To 30
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 5
                For i = 1 To 31
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 6
                For i = 1 To 31
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 7
                For i = 1 To 31
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 8
                For i = 1 To 30
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 9
                For i = 1 To 31
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 10
                For i = 1 To 30
                    H2_DrpDwnDate.Items.Add(i)
                Next
            Case 11
                For i = 1 To 31
                    H2_DrpDwnDate.Items.Add(i)
                Next
        End Select

    End Sub
    Private Sub cboBMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboBMonth.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboBMonth.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If


        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)

        cboBday.Items.Clear()
        For i = 1 To dd
            cboBday.Items.Add(i)

        Next

    End Sub

    Private Sub cboQ1Month_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQ1Month.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboQ1Month.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If


        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)

        cboQ1Date.Items.Clear()
        For i = 1 To dd
            cboQ1Date.Items.Add(i)
        Next

    End Sub

    Private Sub cboQ2Month_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQ2Month.SelectedIndexChanged

        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboQ2Month.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If

        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)
        cboQ2Date.Items.Clear()
        For i = 1 To dd
            cboQ2Date.Items.Add(i)
        Next

    End Sub

    Private Sub cboQ3Month_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQ3Month.SelectedIndexChanged
        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboQ3Month.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If
        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)
        cboQ3Date.Items.Clear()
        For i = 1 To dd
            cboQ3Date.Items.Add(i)
        Next
    End Sub

    Private Sub cboQ4Month_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboQ4Month.SelectedIndexChanged
        Dim i, mmold, mmnew, yy, dd, date1, date2
        mmold = cboQ4Month.SelectedItem.Value
        yy = Year(getoffsetdatetime(DateTime.Now))
        If mmold = 12 Then
            mmnew = 1
            yy = yy + 1
        Else
            mmnew = mmold + 1
        End If

        date1 = mmold & "/01/" & Year(getoffsetdatetime(DateTime.Now))
        date2 = mmnew & "/01/" & yy
        dd = DateDiff(DateInterval.Day, date1, date2)
        cboQ4Date.Items.Clear()
        For i = 1 To dd
            cboQ4Date.Items.Add(i)
        Next
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        lblMsg.Visible = True
        If Rbtwithout.Checked = True Then
            If txtFromDate.Text = "" And txtToDate.Text = "" Then
                'Response.Write("<script language=javascript>alert(""Select Start Date And End Date"")</script>")

                lblMsg.Text = "Select Start Date And End Date"
                Exit Sub
            ElseIf txtFromDate.Text = "" Then

                'Response.Write("<script language=javascript>alert(""Select Start Date"")</script>")

                lblMsg.Text = "Select Start Date"
                Exit Sub
            ElseIf txtToDate.Text = "" Then
                ' Response.Write("<script language=javascript>alert(""Select End Date"")</script>")

                lblMsg.Text = "Select End Date"
                Exit Sub


            End If
        Else
            If txtFromDate.Text = "" And txtToDate.Text = "" Then
                'Response.Write("<script language=javascript>alert(""Select Start Date And End Date"")</script>")

                lblMsg.Text = "Select Start Date And End Date"
                Exit Sub
            ElseIf txtFromDate.Text = "" Then

                'Response.Write("<script language=javascript>alert(""Select Start Date"")</script>")

                lblMsg.Text = "Select Start Date"
                Exit Sub
            ElseIf txtToDate.Text = "" Then
                ' Response.Write("<script language=javascript>alert(""Select End Date"")</script>")

                lblMsg.Text = "Select End Date"
                Exit Sub
            ElseIf CDate(txtFromDate.Text) < CDate(txtConStart.Text) Then
                '            Response.Write("<script language=javascript>alert(""From Date Should Be within in the Contract Period "")</script>")

                lblMsg.Text = "'From Date' Should Be Within The 'Contract Period'"
                Exit Sub
            ElseIf CDate(txtFromDate.Text) > CDate(txtConTo.Text) Then
                ' Response.Write("<script language=javascript>alert(""From Date Should Be within in the Contract Period "")</script>")
                lblMsg.Text = "'From Date' Should Be Within The 'Contract Period'"
                Exit Sub
            ElseIf CDate(txtToDate.Text) > CDate(txtConTo.Text) Then
                'Response.Write("<script language=javascript>alert(""To Date Should Be within in the Contract Period "")</script>")

                lblMsg.Text = "'To Date' Should Be within in the 'Contract Period'"
                Exit Sub
            ElseIf CDate(txtToDate.Text) < CDate(txtConStart.Text) Then
                'Response.Write("<script language=javascript>alert(""To Date Should Be within in the Contract Period "")</script>")

                lblMsg.Text = "'To Date' Should Be Within The 'Contract Period'"
                Exit Sub
            ElseIf CDate(txtConTo.Text) < CDate(getoffsetdate(Date.Today)) Then
                'Response.Write("<script language=javascript>alert(""To Date Should Be within in the Contract Period "")</script>")

                lblMsg.Text = "Update AMC..."
                Exit Sub

            End If
        End If


        Dim strUid As String = Session("Uid")
        rid = Session("rid")

        ' if 1

        'Dim cntVenCat As Integer = 0
        'For i As Integer = 0 To CheckBoxList1.Items.Count - 1
        '    If CheckBoxList1.Items(i).Selected = True Then
        '        cntVenCat += 1
        '    End If
        'Next
        'If cntVenCat = 0 Then
        '    lblMsg.Text = "Please Select Reminder before days"
        '    Exit Sub
        'End If
        If Page.IsValid = True Then


            Dim dateflag
            dateflag = Check_Dates()
            'if 2
            If dateflag = 1 Then

                Dim Planflag
                Planflag = Check_Plan_Id(rid)

                'if 3

                If Planflag = 1 Then

                    Dim assetflag
                    assetflag = Check_Assets()
                    If assetflag = 1 Then

                        Dim insertflag As Boolean
                        Dim schdtime As String
                        Dim fd, td, maindate, tempdate As Date
                        Dim diffmm, i, j, diffyy, dd, mm, yy, dd1, mm1, yy1, dd2, mm2, dd3, mm3, dd4, mm4, flag, ast_Id
                        Dim selradfreq, selradfor, selradtype As String
                        Dim prmid, catid, ctr As String
                        Dim flrid, wngig


                        selradfor = "Office"

                        prmid = cboBuilding.SelectedItem.Value
                        flrid = "0" 'cboFloor.SelectedItem.Value
                        wngig = "0" ' cboWing.SelectedItem.Value

                        selradtype = "Asset"
                        catid = 0 ' cboModel.SelectedItem.Value
                        ctr = lstSelAssets.Items.Count


                        If radYearly.Checked = True Then
                            selradfreq = "Yearly"
                        ElseIf radQuarterly.Checked = True Then
                            selradfreq = "Quarterly"
                        ElseIf radBiMonthly.Checked = True Then
                            selradfreq = "BiMonthly"
                        ElseIf radMonthly.Checked = True Then
                            selradfreq = "Monthly"
                        ElseIf radWeekly.Checked = True Then
                            selradfreq = "Weekly"
                        ElseIf radDaily.Checked = True Then
                            selradfreq = "Daily"
                        ElseIf radhalfyearly.Checked = True Then
                            selradfreq = "HalfYearly"
                        End If

                        fd = CDate(txtFromDate.Text)
                        td = CDate(txtToDate.Text)
                        schdtime = "00:00"

                        For j = 0 To ctr - 1
                            Dim str As String = lstSelAssets.Items(j).Text

                            param = New SqlParameter(26) {}
                            param(0) = New SqlParameter("@PVM_BKG_TS", SqlDbType.NVarChar, 200)
                            param(0).Value = rid
                            param(1) = New SqlParameter("@PVM_PLAN_ID", SqlDbType.NVarChar, 200)
                            param(1).Value = rid
                            param(2) = New SqlParameter("@PVM_PLAN_FOR", SqlDbType.NVarChar, 200)
                            param(2).Value = selradfor
                            param(3) = New SqlParameter("@PVM_BDG_ID", SqlDbType.NVarChar, 200)
                            param(3).Value = prmid
                            param(4) = New SqlParameter("@PVM_TWR_ID", SqlDbType.Int)
                            param(4).Value = wngig
                            param(5) = New SqlParameter("@PVM_PLAN_TYPE", SqlDbType.NVarChar, 200)
                            param(5).Value = selradtype
                            param(6) = New SqlParameter("@PVM_PLANTYPE_ID", SqlDbType.Int)
                            param(6).Value = catid
                            param(7) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                            param(7).Value = selradfreq
                            param(8) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                            param(8).Value = fd
                            param(9) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                            param(9).Value = td
                            If Rbtwithout.Checked = True Then
                                param(10) = New SqlParameter("@PVM_AVR_ID", SqlDbType.NVarChar, 200)
                                param(10).Value = ddlVendorWithoutCon.SelectedItem.Value
                                param(11) = New SqlParameter("@PVM_AVR_NAME", SqlDbType.NVarChar, 200)
                                param(11).Value = ddlVendorWithoutCon.SelectedItem.Text

                            Else
                                param(10) = New SqlParameter("@PVM_AVR_ID", SqlDbType.NVarChar, 200)
                                param(10).Value = cboVendor11.SelectedItem.Value
                                param(11) = New SqlParameter("@PVM_AVR_NAME", SqlDbType.NVarChar, 200)
                                param(11).Value = cboVendor11.SelectedItem.Text
                            End If
                            param(12) = New SqlParameter("@PVM_PLANSTA_ID", SqlDbType.Int)
                            param(12).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                            'param(12).Value = 2
                            param(13) = New SqlParameter("@PVM_ALTERD_DT", SqlDbType.DateTime)
                            param(13).Value = getoffsetdate(Date.Today)
                            param(14) = New SqlParameter("@PVM_UPDT_DT", SqlDbType.DateTime)
                            param(14).Value = getoffsetdate(Date.Today)
                            param(15) = New SqlParameter("@PVM_UPDT_BY", SqlDbType.NVarChar, 200)
                            param(15).Value = strUid
                            param(16) = New SqlParameter("@PVM_FLR_ID", SqlDbType.Int)
                            param(16).Value = flrid
                            param(17) = New SqlParameter("@PVM_ASSET_CODE", SqlDbType.NVarChar, 200)
                            param(17).Value = lstSelAssets.Items(j).Value
                            param(18) = New SqlParameter("@PVM_GRP_ID", SqlDbType.NVarChar, 200)
                            param(18).Value = ddlGroup.SelectedItem.Value
                            param(19) = New SqlParameter("@PVM_GRP_TYPE_ID", SqlDbType.NVarChar, 200)
                            param(19).Value = ddlgrouptype.SelectedItem.Value
                            param(20) = New SqlParameter("@PVM_BRND_ID", SqlDbType.NVarChar, 200)
                            param(20).Value = ddlbrand.SelectedItem.Value
                            param(21) = New SqlParameter("@PVM_ASSET_NAME", SqlDbType.NVarChar, 200)
                            param(21).Value = lstSelAssets.Items(j).Text
                            If Rbtwith.Checked = True Then
                                param(22) = New SqlParameter("@PVM_CONTRACT_ID", SqlDbType.NVarChar, 200)
                                param(22).Value = cboContract.SelectedItem.Value
                            Else
                                param(22) = New SqlParameter("@PVM_CONTRACT_ID", SqlDbType.NVarChar, 200)
                                param(22).Value = ""
                            End If
                            Dim selectedItems As String = [String].Join(",", CheckBoxList1.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value))
                            param(23) = New SqlParameter("@PVM_UPDATE_PRIOR_DAYS", SqlDbType.NVarChar, 200)
                            param(23).Value = selectedItems
                            param(24) = New SqlParameter("@PVM_COMPANY_ID", SqlDbType.Int)
                            param(24).Value = Session("COMPANYID")
                            param(25) = New SqlParameter("@PVM_VEND_MAIL", SqlDbType.Bit)
                            param(25).Value = chkmail.Checked
                            param(26) = New SqlParameter("@PVM_REM", SqlDbType.VarChar, 500)
                            param(26).Value = txtRemarks.Text
                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_MAIN", param)

                            'param = New SqlParameter(2) {}
                            'param(0) = New SqlParameter("@AAP_PVM_FROMDT", SqlDbType.NVarChar, 200)
                            'param(0).Value = fd
                            'param(1) = New SqlParameter("@AAP_PVM_TODT", SqlDbType.NVarChar, 200)
                            'param(1).Value = td
                            'param(2) = New SqlParameter("@AAP_RUNNO", SqlDbType.NVarChar, 200)
                            'param(2).Value = lstSelAssets.Items(j).Value
                            'ObjSubSonic.GetSubSonicExecute("PVM_UPDATE_PVM_STATUS", param)

                            If radWeekly.Checked = True Then
                                flag = 1
                                tempdate = fd
                                While flag = 1
                                    If cboWWeek.SelectedItem.Value = DatePart(DateInterval.Weekday, tempdate) Then
                                        flag = 2
                                    Else
                                        tempdate = DateAdd(DateInterval.Day, 1, tempdate)
                                    End If
                                End While
                                fd = tempdate
                                maindate = fd
                                For i = 1 To DateDiff(DateInterval.Day, fd, td)
                                    If maindate >= fd And maindate <= td Then
                                        param = New SqlParameter(9) {}
                                        param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param(0).Value = rid
                                        param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        param(1).Value = catid
                                        param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        param(2).Value = lstSelAssets.Items(j).Value
                                        param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        param(3).Value = maindate
                                        param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        param(4).Value = schdtime
                                        param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                        'param(5).Value = 2
                                        param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        param(6).Value = getoffsetdate(Date.Today)
                                        param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        param(7).Value = strUid
                                        param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                        param(8).Value = lstSelAssets.Items(j).Value
                                        param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                        param(9).Value = HttpContext.Current.Session("COMPANYID").ToString
                                        ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)
                                    End If
                                    maindate = DateAdd(DateInterval.Day, 7, maindate)
                                Next

                            ElseIf radMonthly.Checked = True Then

                                If Year(td) <> Year(fd) Then
                                    diffmm = 12 - Month(fd)
                                    diffyy = Year(td) - Year(fd)
                                    If diffyy = 1 Then
                                        diffmm = diffmm + Month(td)
                                    Else
                                        diffmm = diffmm + (diffyy * 12) + Month(td)
                                    End If
                                Else
                                    diffmm = Month(td) - Month(fd)
                                End If

                                mm = Month(fd)
                                yy = Year(fd)
                                dd = cboMDate.SelectedItem.Value

                                If mm = 12 Then
                                    mm1 = 1
                                    yy1 = yy + 1
                                Else
                                    mm1 = mm + 1
                                    yy1 = yy
                                End If

                                For i = 0 To diffmm
                                    maindate = mm & "/1/" & yy
                                    tempdate = mm1 & "/1/" & yy1
                                    If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                                        maindate = mm & "/" & dd & "/" & yy
                                        If maindate >= fd And maindate <= td Then
                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            'param(5).Value = 2
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdate(Date.Today)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                            param(9).Value = Session("COMPANYID").ToString
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)
                                        End If
                                    End If

                                    If mm = 12 Then
                                        mm = 1
                                        yy = yy + 1
                                    Else
                                        mm = mm + 1
                                    End If

                                    If mm1 = 12 Then
                                        mm1 = 1
                                        yy1 = yy1 + 1
                                    Else
                                        mm1 = mm1 + 1
                                    End If
                                Next

                            ElseIf radBiMonthly.Checked = True Then
                                If Year(td) <> Year(fd) Then
                                    diffmm = 12 - Month(fd)
                                    diffyy = Year(td) - Year(fd)
                                    If diffyy = 1 Then
                                        diffmm = diffmm + Month(td)
                                    Else
                                        diffmm = diffmm + (diffyy * 12) + Month(td)
                                    End If
                                Else
                                    diffmm = Month(td) - Month(fd)
                                End If

                                mm = cboBMonth.SelectedItem.Value
                                yy = Year(fd)
                                dd = cboBday.SelectedItem.Value

                                If mm = 12 Then
                                    mm1 = 1
                                    yy1 = yy + 1
                                Else
                                    mm1 = mm + 1
                                    yy1 = yy
                                End If

                                For i = 0 To diffmm

                                    maindate = mm & "/1/" & yy
                                    tempdate = mm1 & "/1/" & yy1
                                    If DateDiff(DateInterval.Day, maindate, tempdate) >= dd Then
                                        maindate = mm & "/" & dd & "/" & yy
                                        If maindate >= fd And maindate <= td Then
                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            'param(5).Value = 2
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdate(Date.Today)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                            param(9).Value = HttpContext.Current.Session("COMPANYID").ToString
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)

                                        End If
                                    End If

                                    If mm = 12 Then
                                        mm = 2
                                        yy = yy + 1
                                    ElseIf mm = 11 Then
                                        mm = 1
                                        yy = yy + 1
                                    Else
                                        mm = mm + 2
                                    End If

                                    If mm = 12 Then
                                        mm1 = 1
                                        yy1 = yy + 1
                                    Else
                                        mm1 = mm + 1
                                        yy1 = yy
                                    End If
                                Next

                            ElseIf radQuarterly.Checked = True Then
                                If Year(td) <> Year(fd) Then
                                    diffmm = 12 - Month(fd)
                                    diffyy = Year(td) - Year(fd)
                                    If diffyy = 1 Then
                                        diffmm = diffmm + Month(td)
                                    Else
                                        diffmm = diffmm + (diffyy * 12) + Month(td)
                                    End If
                                Else
                                    diffmm = Month(td) - Month(fd)
                                End If

                                mm1 = cboQ1Month.SelectedItem.Value
                                dd1 = cboQ1Date.SelectedItem.Value

                                mm2 = cboQ2Month.SelectedItem.Value
                                dd2 = cboQ2Date.SelectedItem.Value

                                mm3 = cboQ3Month.SelectedItem.Value
                                dd3 = cboQ3Date.SelectedItem.Value

                                mm4 = cboQ4Month.SelectedItem.Value
                                dd4 = cboQ4Date.SelectedItem.Value

                                mm = Month(fd)
                                yy = Year(fd)
                                For i = 0 To diffmm

                                    If mm = mm1 Then
                                        maindate = mm1 & "/" & dd1 & "/" & yy
                                        If maindate >= fd And maindate <= td Then



                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            'param(5).Value = 2
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdate(Date.Today)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                            param(9).Value = HttpContext.Current.Session("COMPANYID").ToString
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)



                                        End If
                                    End If
                                    If mm = mm2 Then
                                        maindate = mm2 & "/" & dd2 & "/" & yy
                                        If maindate >= fd And maindate <= td Then


                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            'param(5).Value = 2
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdate(Date.Today)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                            param(9).Value = HttpContext.Current.Session("COMPANYID").ToString
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)



                                        End If
                                    End If
                                    If mm = mm3 Then
                                        maindate = mm3 & "/" & dd3 & "/" & yy
                                        If maindate >= fd And maindate <= td Then

                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            'param(5).Value = 2
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdate(Date.Today)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                            param(9).Value = HttpContext.Current.Session("COMPANYID").ToString
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)

                                        End If
                                    End If
                                    If mm = mm4 Then
                                        maindate = mm4 & "/" & dd4 & "/" & yy
                                        If maindate >= fd And maindate <= td Then

                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            'param(5).Value = 2
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdate(Date.Today)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                            param(9).Value = HttpContext.Current.Session("COMPANYID").ToString
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)

                                        End If
                                    End If

                                    If mm = 12 Then
                                        mm = 1
                                        yy = yy + 1
                                    Else
                                        mm = mm + 1
                                    End If
                                Next

                            ElseIf radYearly.Checked = True Then
                                If Year(td) <> Year(fd) Then
                                    diffmm = 12 - Month(fd)
                                    diffyy = Year(td) - Year(fd)
                                    If diffyy = 1 Then
                                        diffmm = diffmm + Month(td)
                                    Else
                                        diffmm = diffmm + (diffyy * 12) + Month(td)
                                    End If
                                Else
                                    diffmm = Month(td) - Month(fd)
                                End If

                                mm = cboYMonth.SelectedItem.Value
                                yy = Year(fd)
                                dd = cboYDate.SelectedItem.Value

                                mm1 = cboYMonth.SelectedItem.Value

                                For i = 0 To diffmm

                                    If mm = mm1 Then
                                        maindate = mm & "/" & dd & "/" & yy
                                        If maindate >= fd And maindate <= td Then

                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            'param(5).Value = 2
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdate(Date.Today)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                            param(9).Value = HttpContext.Current.Session("COMPANYID").ToString
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)

                                        End If
                                    End If

                                    If mm1 = 12 Then
                                        mm1 = 1
                                        yy = yy + 1
                                    Else
                                        mm1 = mm1 + 1
                                    End If
                                Next

                            ElseIf radDaily.Checked = True Then
                                maindate = fd
                                schdtime = cboHour.SelectedItem.Value
                                For i = 0 To DateDiff(DateInterval.Day, fd, td)
                                    If maindate >= fd And maindate <= td Then
                                        param = New SqlParameter(9) {}
                                        param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                        param(0).Value = rid
                                        param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                        param(1).Value = catid
                                        param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                        param(2).Value = lstSelAssets.Items(j).Value
                                        param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                        param(3).Value = maindate
                                        param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                        param(4).Value = schdtime
                                        param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                        'param(5).Value = 2
                                        param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                        param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                        param(6).Value = getoffsetdate(Date.Today)
                                        param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                        param(7).Value = strUid
                                        param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                        param(8).Value = lstSelAssets.Items(j).Value
                                        param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.NVarChar, 200)
                                        param(9).Value = HttpContext.Current.Session("COMPANYID").ToString
                                        ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)
                                    End If
                                    maindate = DateAdd(DateInterval.Day, 1, maindate)
                                Next
                            ElseIf radhalfyearly.Checked = True Then
                                If Year(td) <> Year(fd) Then
                                    diffmm = 12 - Month(fd)
                                    diffyy = Year(td) - Year(fd)
                                    If diffyy = 1 Then
                                        diffmm = diffmm + Month(td)
                                    Else
                                        diffmm = diffmm + (diffyy * 12) + Month(td)
                                    End If
                                Else
                                    diffmm = Month(td) - Month(fd)
                                End If

                                mm1 = H1_DrpDwnMonth.SelectedItem.Value
                                dd1 = H1_DrpDwnDate.SelectedItem.Value

                                mm2 = H2_DrpDwnMonth.SelectedItem.Value
                                dd2 = H2_DrpDwnDate.SelectedItem.Value



                                mm = Month(fd)
                                yy = Year(fd)
                                For i = 0 To diffmm

                                    If mm = mm1 Then
                                        maindate = mm1 & "/" & dd1 & "/" & yy
                                        If maindate >= fd And maindate <= td Then

                                            Dim myDate2 As DateTime = DateTime.ParseExact(txtFromDate.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                                            Dim frmDate1 As DateTime = myDate2.ToString("yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture)

                                            Dim tomyDate As DateTime = DateTime.ParseExact(txtToDate.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                                            Dim toDate1 As DateTime = tomyDate.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture)

                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            'param(5).Value = 2
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdatetime(DateTime.Now)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.Int)
                                            param(9).Value = Session("COMPANYID")
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)

                                            param = New SqlParameter(5) {}
                                            param(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                            param(0).Value = selradfreq
                                            param(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                            param(1).Value = frmDate1
                                            param(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                            param(2).Value = toDate1
                                            param(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(3).Value = rid
                                            param(4) = New SqlParameter("@PVM_VEND_MAIL", SqlDbType.Bit)
                                            param(4).Value = chkmail.Checked
                                            param(5) = New SqlParameter("@PVM_REM", SqlDbType.VarChar, 500)
                                            param(5).Value = txtRemarks.Text
                                            ObjSubSonic.GetSubSonicExecute("PVM_UPDATE_PVM_DTS", param)
                                        End If
                                    End If
                                    If mm = mm2 Then
                                        maindate = mm2 & "/" & dd2 & "/" & yy
                                        If maindate >= fd And maindate <= td Then

                                            Dim myDate3 As DateTime = DateTime.ParseExact(txtFromDate.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                                            Dim frmDate2 As DateTime = myDate3.ToString("yyyy-MM-dd HH:mm", System.Globalization.CultureInfo.InvariantCulture)

                                            Dim tomyDate1 As DateTime = DateTime.ParseExact(txtToDate.Text, "MM/dd/yyyy", System.Globalization.CultureInfo.InvariantCulture)
                                            Dim toDate2 As DateTime = tomyDate1.ToString("yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture)

                                            param = New SqlParameter(9) {}
                                            param(0) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(0).Value = rid
                                            param(1) = New SqlParameter("@PVD_PLANTYPE_ID", SqlDbType.Int)
                                            param(1).Value = catid
                                            param(2) = New SqlParameter("@PVD_PLANAAT_ID", SqlDbType.NVarChar, 200)
                                            param(2).Value = lstSelAssets.Items(j).Value
                                            param(3) = New SqlParameter("@PVD_PLANSCHD_DT", SqlDbType.DateTime)
                                            param(3).Value = maindate
                                            param(4) = New SqlParameter("@PVD_PLANSCHD_TIME", SqlDbType.NVarChar, 200)
                                            param(4).Value = schdtime
                                            param(5) = New SqlParameter("@PVD_PLANSTA_ID", SqlDbType.Int)
                                            param(5).Value = 3008 'in status table 2 indicates closed status, so modified to 1(Pending)
                                            'param(5).Value = 2
                                            param(6) = New SqlParameter("@PVD_UPDT_DT", SqlDbType.DateTime)
                                            param(6).Value = getoffsetdatetime(DateTime.Now)
                                            param(7) = New SqlParameter("@PVD_UPDT_BY", SqlDbType.NVarChar, 200)
                                            param(7).Value = strUid
                                            param(8) = New SqlParameter("@PVD_ASSET_CODE", SqlDbType.NVarChar, 200)
                                            param(8).Value = lstSelAssets.Items(j).Value
                                            param(9) = New SqlParameter("@PVD_COMPANY_ID", SqlDbType.Int)
                                            param(9).Value = Session("COMPANYID")
                                            ObjSubSonic.GetSubSonicExecute("PVM_INSRT_PVM_DTLS", param)

                                            param = New SqlParameter(5) {}
                                            param(0) = New SqlParameter("@PVM_PLAN_FREQ", SqlDbType.NVarChar, 200)
                                            param(0).Value = selradfreq
                                            param(1) = New SqlParameter("@PVM_PLAN_FDATE", SqlDbType.DateTime)
                                            param(1).Value = frmDate2
                                            param(2) = New SqlParameter("@PVM_PLAN_TDATE", SqlDbType.DateTime)
                                            param(2).Value = toDate2
                                            param(3) = New SqlParameter("@PVD_PLAN_ID", SqlDbType.NVarChar, 200)
                                            param(3).Value = rid
                                            param(4) = New SqlParameter("@PVM_VEND_MAIL", SqlDbType.Bit)
                                            param(4).Value = chkmail.Checked
                                            param(5) = New SqlParameter("@PVM_REM", SqlDbType.VarChar, 500)
                                            param(5).Value = txtRemarks.Text
                                            ObjSubSonic.GetSubSonicExecute("PVM_UPDATE_PVM_DTS", param)
                                        End If
                                    End If
                                    If mm = 12 Then
                                        mm = 1
                                        yy = yy + 1
                                    Else
                                        mm = mm + 1
                                    End If
                                Next
                            End If
                        Next
                        If Rbtwith.Checked = True Then
                            Dim group As String = Trim(cboVendor11.SelectedItem.Value) 'cboGP.SelectedItem.Text
                            Response.Redirect("frmPVMFinalpage.aspx?staid=submitted&rid=" & rid & "&group=" & group & "&Ven_mail=" & chkmail.Checked)
                        Else
                            Dim group As String = Trim(ddlVendorWithoutCon.SelectedItem.Value) 'cboGP.SelectedItem.Text
                            Response.Redirect("frmPVMFinalpage.aspx?staid=submitted&rid=" & rid & "&group=" & group & "&Ven_mail=" & chkmail.Checked)
                        End If
                    Else
                        'Response.Write("<script language=javascript>alert(""Plan already Submitted"")</script>")

                        lblMsg.Text = "Plan already Submitted"
                    End If
                Else
                    'Response.Write("<script language=javascript>alert(""Plan Id Already Submited"")</script>")

                    lblMsg.Text = "Plan ID already Submitted"
                End If
            Else
                'Response.Write("<script language=javascript>alert(""Select The Valid Dates"")</script>")

                lblMsg.Text = "Select The Valid Dates"
            End If
        End If
    End Sub

    Protected Sub cboContract_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboContract.SelectedIndexChanged
        lstDisAssets.Items.Clear()
        lstSelAssets.Items.Clear()
        PanSelAssets.Visible = False
        pnlCon.Visible = False
        panPeriod.Visible = False
        panAllPeriods.Visible = False
        txtConName.Text = ""
        txtConStart.Text = ""
        txtConTo.Text = ""
        txtNoofYears.Text = ""
        'venName.InnerText = ""
        'NoOfYears.InnerText = ""
        'ConStartDate.InnerText = ""
        'ConEndDate.InnerText = ""
        lblMsg.Visible = False
        btnRight.Enabled = True
        pnlCon.Visible = True
        If cboVendor11.SelectedItem.Text <> "--Select--" Then
            'btnSearch.Visible = False
            LoadVendorsByContractID(cboContract.SelectedItem.Value)
            arparam = New SqlParameter(5) {}
            arparam(0) = New SqlParameter("@BDG_ID", SqlDbType.NVarChar, 50)
            arparam(0).Value = cboBuilding.SelectedItem.Value
            arparam(1) = New SqlParameter("@AAP_VENDOR", SqlDbType.NVarChar, 50)
            arparam(1).Value = cboVendor11.SelectedItem.Value

            arparam(2) = New SqlParameter("@AMN_PLAN_ID", SqlDbType.NVarChar, 50)
            arparam(2).Value = cboContract.SelectedItem.Value

            arparam(3) = New SqlParameter("@AMN_FROM_DATE", SqlDbType.DateTime)
            arparam(3).Value = CDate(lblAmcFromDT.Text)
            arparam(4) = New SqlParameter("@AMN_TO_DATE", SqlDbType.DateTime)
            arparam(4).Value = CDate(lblAmcToDT.Text)
            arparam(5) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            arparam(5).Value = Session("COMPANYID")

            'ObjSubSonic.BindListBox(lstDisAssets, "GET_PVM_VENDORASSETS", "AAP_CODE", "AAP_RUNNO", arparam)
            ObjSubSonic.BindListBox(lstDisAssets, "GET_PVM_VENDORASSETS", "AAT_NAME", "AAT_CODE", arparam)

            ' LoadVendors(lstSelAssets.SelectedItem.Value, cboContract.SelectedItem.Value)
            If lstDisAssets.Items.Count > 0 Then
                ' panPeriod.Visible = True
                'Vend.Visible = True
                pnlCon.Visible = True
                panAssetService.Visible = True
                PanPremise.Visible = True
                PanSelAssets.Visible = True
                radWeekly.Checked = False
                radBiMonthly.Checked = False
                radMonthly.Checked = False
                radQuarterly.Checked = False
                radYearly.Checked = False
                radDaily.Checked = False
                panAllPeriods.Visible = False
            Else
                'btnSearch.Visible = True
                'Vend.Visible = False
                pnlCon.Visible = False
                lblMsg.Visible = True
                lblMsg.Text = "Assets under Contract Id have previously been utilized for another Plan...."
                panPeriod.Visible = False
                radWeekly.Checked = False
                radBiMonthly.Checked = False
                radMonthly.Checked = False
                radQuarterly.Checked = False
                radYearly.Checked = False
                radDaily.Checked = False
                panAllPeriods.Visible = False
            End If
        Else
            lstDisAssets.Items.Clear()
            lstSelAssets.Items.Clear()
            PanSelAssets.Visible = False
            pnlCon.Visible = False
            panPeriod.Visible = False
            panAllPeriods.Visible = False
        End If

    End Sub
    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rbtwith.CheckedChanged, Rbtwithout.CheckedChanged

        Try
            'If RbtEmployee.Checked = True Then
            'ddlLocation.Items.Clear()
            'BindLocation()
            'BindAssets()
            If Rbtwith.Checked = True Then

                cboBuilding.ClearSelection()
                ddlbrand.ClearSelection()
                ddlGroup.ClearSelection()
                ddlgrouptype.ClearSelection()
                ddlbrand.ClearSelection()
                cboVendor11.ClearSelection()
                WithoutCon.Visible = False
                WithCon.Visible = True

            Else
                cboBuilding.ClearSelection()
                ddlbrand.ClearSelection()
                ddlGroup.ClearSelection()
                ddlgrouptype.ClearSelection()
                ddlbrand.ClearSelection()
                ddlVendorWithoutCon.ClearSelection()
                WithoutCon.Visible = True
                WithCon.Visible = False
            End If

            'If Rbtwithout.Checked = True Then
            '    cboBuilding.ClearSelection()
            '    ddlbrand.ClearSelection()
            '    ddlGroup.ClearSelection()
            '    ddlgrouptype.ClearSelection()
            '    ddlbrand.ClearSelection()
            '    cboVendor11.ClearSelection()
            'End If


            'LoadGridData()
            'End If
        Catch ex As Exception

        End Try

    End Sub

    'Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

    '    lstDisAssets.Items.Clear()
    '    lstSelAssets.Items.Clear()
    '    PanSelAssets.Visible = False
    '    pnlCon.Visible = False
    '    panPeriod.Visible = False
    '    panAllPeriods.Visible = False
    '    txtConName.Text = ""
    '    txtConStart.Text = ""
    '    txtConTo.Text = ""
    '    txtNoofYears.Text = ""
    '    'venName.InnerText = ""
    '    'NoOfYears.InnerText = ""
    '    'ConStartDate.InnerText = ""
    '    'ConEndDate.InnerText = ""
    '    lblMsg.Visible = False
    '    btnRight.Enabled = True
    '    'pnlCon.Visible = True
    '    'Dim MyDateTime As DateTime = Now()
    '    param = New SqlParameter(4) {}
    '    param(0) = New SqlParameter("@LOC_ID", SqlDbType.NVarChar, 200)
    '    param(0).Value = cboBuilding.SelectedItem.Value
    '    param(1) = New SqlParameter("@AAG_CODE", SqlDbType.NVarChar, 200)
    '    param(1).Value = ddlGroup.SelectedItem.Value
    '    param(2) = New SqlParameter("@SUB_CODE", SqlDbType.NVarChar, 200)
    '    param(2).Value = ddlgrouptype.SelectedItem.Value
    '    param(3) = New SqlParameter("@AAB_CODE", SqlDbType.NVarChar, 200)
    '    param(3).Value = ddlbrand.SelectedItem.Value
    '    param(4) = New SqlParameter("@AVR_CODE", SqlDbType.NVarChar, 200)
    '    param(4).Value = cboVendor11.SelectedItem.Value
    '    Dim ds As New DataSet
    '    ds = ObjSubSonic.GetSubSonicDataSet("MM_GET_VNDR_ASSET_DETAILS_BY_SEARCH", param)
    '    lstDisAssets.DataSource = ds
    '    lstDisAssets.DataTextField = "AAT_NAME"
    '    lstDisAssets.DataValueField = "AAT_CODE"
    '    lstDisAssets.DataBind()
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        PanSelAssets.Visible = True
    '        venName.InnerText = "Vendor Name : " + ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
    '        NoOfYears.InnerText = "No of Years : " + ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString
    '        ConStartDate.InnerText = "Contract Start Date : " + ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
    '        ConEndDate.InnerText = "Contract End Date    :" + ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString

    '        txtConName.Text = ds.Tables(0).Rows(0).Item("AVR_NAME").ToString
    '        txtConStart.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
    '        txtConTo.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
    '        txtNoofYears.Text = ds.Tables(0).Rows(0).Item("AMN_NO_OF_YEARS").ToString
    '        Dim MyDateTime As DateTime = Now()
    '        txtFromDate.Text = MyDateTime.ToString("MM/dd/yyyy")
    '        Response.Write(txtFromDate.Text)
    '        Dim newDate As DateTime = DateTime.Now.AddYears(1)
    '        txtToDate.Text = newDate.ToString("MM/dd/yyyy")
    '        lblAmcFromDT.Text = ds.Tables(0).Rows(0).Item("AMN_FROM_DATE").ToString
    '        lblAmcToDT.Text = ds.Tables(0).Rows(0).Item("AMN_TO_DATE").ToString
    '        lblMsg.Visible = False
    '        lblMsg.Text = ""
    '        result = "T"
    '        'Vend.Visible = True
    '        'pnlCon.Visible = True
    '        panAssetService.Visible = True
    '        PanPremise.Visible = True
    '        PanSelAssets.Visible = True
    '        radWeekly.Checked = False
    '        radBiMonthly.Checked = False
    '        radMonthly.Checked = False
    '        radQuarterly.Checked = False
    '        radYearly.Checked = False
    '        radDaily.Checked = False
    '        panAllPeriods.Visible = False

    '        If cboVendor11.SelectedItem.Value = "NA" Then
    '            cboVendor11.Items.Clear()
    '            ObjSubSonic.Binddropdown(cboVendor11, "MM_GET_VENDORS_LIST_SERVICE", "AVR_NAME", "AVR_CODE")
    '        End If
    '    Else
    '        txtConName.Text = ""
    '        txtConStart.Text = ""
    '        txtConTo.Text = ""
    '        txtNoofYears.Text = ""
    '        lblMsg.Visible = True
    '        lblMsg.Text = "No Assets Were Found for the Contractless"
    '        result = "F"
    '        Exit Sub
    '    End If


    'End Sub


End Class

