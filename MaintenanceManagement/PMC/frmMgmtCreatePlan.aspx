<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmMgmtCreatePlan.aspx.vb" Inherits="MaintenanceManagement_PMC_frmMgmtCreatePlan" Title="Create Plan" %>

<%@ Register Src="Controls/MgmtCreatePlan.ascx" TagName="MgmtCreatePlan" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };

    </script>
    <style>
        label {
            margin-right: 0rem;
        }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Create Plan</h3>
            </div>
            <div class="card">
                <%--  <div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <uc1:MgmtCreatePlan ID="MgmtCreatePlan1" runat="server" />
                </form>
            </div>
        </div>
    </div>
</body>
</html>
