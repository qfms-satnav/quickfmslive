<%@ Page Language="VB"  AutoEventWireup="false" CodeFile="frmPVMFinalpage.aspx.vb" Inherits="MaintenanceManagement_PMC_frmPVMFinalpage"   %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
   <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Maintenance Management</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                        <form id="form1"  runat="server">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group text-center">
                                    <%--<div class="alert bg-info text-center">--%>
                                        <div class="row">
                                            <asp:Label ID="lblmsg" runat="server" ForeColor="#209e91"  CssClass="col-md-12 control-label"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
       </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

