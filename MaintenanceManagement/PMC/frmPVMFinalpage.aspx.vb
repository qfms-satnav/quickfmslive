Imports System.Web.Mail
Imports System.Data
Imports System.Data.SqlClient

Partial Class MaintenanceManagement_PMC_frmPVMFinalpage
    Inherits System.Web.UI.Page

    'Dim strSql As String
    Dim lid, rid, uid, group As String
    Dim COMPANYID, MAIL_ID As Integer
    Dim uprm_id As String
    Dim TmpReqseqid As String
    Dim Reqseqid As Integer
    Dim Ven_mail As Boolean

    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter

    Public area, cat, subcat, subj, msg, msgadm, msgstr, reqdate, empname, empemail, appempname, appempemail, str, strqry

    Dim ds As New DataSet

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        rid = Request.QueryString("rid")
        uid = Session("uid")
        COMPANYID = Session("COMPANYID")
        group = Request.QueryString("group")
        If Request.QueryString("staid") = "submitted" Then
            Ven_mail = Request.QueryString("Ven_mail")
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Session("rid")
            ds = New DataSet
            ds = objsubsonic.GetSubSonicDataSet("GET_PVM_SNO", param)
            If ds.Tables(0).Rows.Count > 0 Then
                Reqseqid = ds.Tables(0).Rows(0).Item("PVM_SNO")
            End If
 
            If Reqseqid < 10 Then
                TmpReqseqid = group & "/PPM" & "/000000" & Reqseqid
            ElseIf Reqseqid < 100 Then
                TmpReqseqid = group & "/PPM" & "/00000" & Reqseqid
            ElseIf Reqseqid < 1000 Then
                TmpReqseqid = group & "/PPM" & "/0000" & Reqseqid
            ElseIf Reqseqid < 10000 Then
                TmpReqseqid = group & "/PPM" & "/000" & Reqseqid
            ElseIf Reqseqid < 100000 Then
                TmpReqseqid = group & "/PPM" & "/00" & Reqseqid
            ElseIf Reqseqid < 1000000 Then
                TmpReqseqid = group & "/PPM" & "/0" & Reqseqid
            ElseIf Reqseqid < 10000000 Then
                TmpReqseqid = group & "/PPM" & "/" & Reqseqid
            End If

            If Reqseqid Then

                param = New SqlParameter(3) {}

                param(0) = New SqlParameter("@TMPREQID", SqlDbType.NVarChar, 200)
                param(0).Value = TmpReqseqid
                param(1) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
                param(1).Value = Session("RID")
                param(2) = New SqlParameter("@VEN_MAIL", SqlDbType.Bit)
                param(2).Value = Ven_mail
                param(3) = New SqlParameter("@COMPANYID", SqlDbType.Int)
                param(3).Value = COMPANYID
                ObjSubSonic.GetSubSonicExecute("UPDATE_PVM_DETAILS_REQ", param)

                lblmsg.Text = lblmsg.Text & "<b><center><br><br>"
                lblmsg.Text = lblmsg.Text & "Requisition ID : "
                lblmsg.Text = lblmsg.Text & TmpReqseqid
                lblmsg.Text = lblmsg.Text & "<br>Plan Has Been Created Successfully !"
                'lblmsg.Text = lblmsg.Text & Request.QueryString("staid")
                lblmsg.Text = lblmsg.Text & "<br><br>Thank You For Using The System.</center>"
                'mail_PVMCreate(TmpReqseqid)

            End If
        End If

        If Request.QueryString("staid") = "Changed" Then
            'Dim mail As New cmpMMTmails()

            lblmsg.Text = lblmsg.Text & "<b><center><br><br>"
            lblmsg.Text = lblmsg.Text & "Requisition ID : "
            lblmsg.Text = lblmsg.Text & Request.QueryString("rid")
            lblmsg.Text = lblmsg.Text & "<br>Plan Has Been Changed Successfully !"
            'lblmsg.Text = Request.QueryString("staid")
            lblmsg.Text = lblmsg.Text & "<br><br>Thank You For Using The System.</center>"
            'mail_PVMChange(Request.QueryString("rid"))

        End If

        If Request.QueryString("staid") = "Updated" Then
            'Dim mail As New cmpMMTmails()

            lblmsg.Text = lblmsg.Text & "<b><center><br><br>"
            lblmsg.Text = lblmsg.Text & "Requisition ID : "
            lblmsg.Text = lblmsg.Text & Request.QueryString("rid")
            lblmsg.Text = lblmsg.Text & "<br>Plan Has Been Updated Successfully !"
            'lblmsg.Text = Request.QueryString("staid")
            lblmsg.Text = lblmsg.Text & "<br><br>Thank You For Using The System.</center>"
            'mail_PVMUpdate(Request.QueryString("rid"))
        End If
        lblmsg.Text = lblmsg.Text + "</body>"

    End Sub

    '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    'PVM MAILS:

    'Public Sub mail_PVMCreate(ByVal reqid As String)
    '    Try

    '        subj = "Preventive Maintenance Requisition."


    '        param = New SqlParameter(1) {}
    '        param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
    '        param(0).Value = reqid
    '        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
    '        param(1).Value = Session("uid")
    '        ds = New DataSet
    '        ds = objsubsonic.GetSubSonicDataSet("GET_PVM_MAINDETAILS_AUR_ID", param)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            rid = ds.Tables(0).Rows(0).Item("PVM_PLAN_ID").ToString
    '            reqdate = ds.Tables(0).Rows(0).Item("PVM_CREATED_DT").ToString
    '            empname = ds.Tables(0).Rows(0).Item("AUR_FULL_NAME").ToString
    '            empemail = ds.Tables(0).Rows(0).Item("AUR_EMAIL").ToString
    '        End If

    '        'Mail message for the Requisitioner. 
    '        msg = "Dear " & empname & "," & "<br>" & "<br>" & _
    '         "This is to inform you that your request for Preventive Maintenance has been created and sent to your approver for approval." & "<br>" & _
    '         "The Requisition ID and date of requisition are " & rid & " and " & reqdate & "." & "<br>" & "<br>" & _
    '         "Thanking You," & "<br>" & _
    '         "Amantra Admin."


    '        param = New SqlParameter(1) {}
    '        param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
    '        param(0).Value = reqid
    '        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
    '        param(1).Value = Session("uid")
    '        ds = New DataSet
    '        ds = objsubsonic.GetSubSonicDataSet("GET_PVM_MAINDETAILS_AUR_ID", param)
    '        If ds.Tables(1).Rows.Count > 0 Then
    '            appempname = ds.Tables(1).Rows(0).Item("AUR_FULL_NAME").ToString
    '            appempemail = ds.Tables(1).Rows(0).Item("AUR_EMAIL").ToString
    '            rid = ds.Tables(1).Rows(0).Item("PVM_PLAN_ID").ToString
    '            reqdate = ds.Tables(1).Rows(0).Item("PVM_CREATED_DT").ToString
    '        End If


    '        'Mail message for the Approver. 
    '        msgadm = "Dear " & appempname & "," & "<br>" & "<br>" & _
    '        "This is to inform you that a request for Preventive Maintenance has been created. " & "<br>" & _
    '        "And it is pending for your approval.The Requisition ID and date of requisitions are " & rid & " and " & _
    '        "" & reqdate & "." & "<br>" & _
    '        "Kindly log into " & Application("AppURL") & " to approve ." & "<br>" & "<br>" & _
    '        "Thanking You," & "<br>" & _
    '        "Amantra Admin."


    '        If empemail <> "" Then                   'Mail  for the Requisitioner. 
    '            'Send_Mail(empemail, reqid, subj, msg)


    '            param = New SqlParameter(6) {}
    '            param(0) = New SqlParameter("@AMT_ID", SqlDbType.NVarChar, 200)
    '            param(0).Value = rid
    '            param(1) = New SqlParameter("@AMT_MESSAGE", SqlDbType.NVarChar, 200)
    '            param(1).Value = msg
    '            param(2) = New SqlParameter("@AMT_MAIL_TO", SqlDbType.NVarChar, 200)
    '            param(2).Value = empemail
    '            param(3) = New SqlParameter("@AMT_SUBJECT", SqlDbType.NVarChar, 200)
    '            param(3).Value = subj
    '            param(4) = New SqlParameter("@AMT_MAIL_TIME", SqlDbType.DateTime)
    '            param(4).Value = getoffsetdatetime(DateTime.Now)
    '            param(5) = New SqlParameter("@AMT_FLAG", SqlDbType.NVarChar, 200)
    '            param(5).Value = "REQUEST SUBMITTED"
    '            param(6) = New SqlParameter("@AMT_TYPE", SqlDbType.NVarChar, 200)
    '            param(6).Value = "NORMAL MAIL"
    '            ObjSubSonic.GetSubSonicExecute("INSERT_AMT_MAIL", param)
    '        End If

    '        If appempemail <> "" Then                 'Mail  to Approver.
    '            'Send_Mail(appempemail, rid, subj, msgadm)
    '            param = New SqlParameter(6) {}
    '            param(0) = New SqlParameter("@AMT_ID", SqlDbType.NVarChar, 200)
    '            param(0).Value = rid
    '            param(1) = New SqlParameter("@AMT_MESSAGE", SqlDbType.NVarChar, 200)
    '            param(1).Value = msgadm
    '            param(2) = New SqlParameter("@AMT_MAIL_TO", SqlDbType.NVarChar, 200)
    '            param(2).Value = appempemail
    '            param(3) = New SqlParameter("@AMT_SUBJECT", SqlDbType.NVarChar, 200)
    '            param(3).Value = subj
    '            param(4) = New SqlParameter("@AMT_MAIL_TIME", SqlDbType.DateTime)
    '            param(4).Value = getoffsetdatetime(DateTime.Now)
    '            param(5) = New SqlParameter("@AMT_FLAG", SqlDbType.NVarChar, 200)
    '            param(5).Value = "Forced Modification of Withheld"
    '            param(6) = New SqlParameter("@AMT_TYPE", SqlDbType.NVarChar, 200)
    '            param(6).Value = "NORMAL MAIL"
    '            ObjSubSonic.GetSubSonicExecute("INSERT_AMT_MAIL", param)

    '        End If
    '    Catch ex As Exception
    '        'Dim pageURL, trace, MSG As String
    '        Session("pageURL") = ex.GetBaseException.ToString
    '        Session("trace") = (ex.StackTrace.ToString)
    '        Session("MSG") = Session("pageURL") + Session("Trace")
    '        Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
    '    End Try
    'End Sub

    'Public Sub mail_PVMChange(ByVal reqid As String)
    '    Try

    '        subj = "Preventive Maintenance Amendment."


    '        param = New SqlParameter(1) {}
    '        param(0) = New SqlParameter("@PVM_PLAN_ID", SqlDbType.NVarChar, 200)
    '        param(0).Value = reqid
    '        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
    '        param(1).Value = Session("uid")

    '        ds = New DataSet
    '        ds = objsubsonic.GetSubSonicDataSet("GET_PVM_MAIL_DETAILS", param)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            rid = ds.Tables(0).Rows(0).Item("PVM_PLAN_ID").ToString
    '            reqdate = ds.Tables(0).Rows(0).Item("PVM_CREATED_DT").ToString
    '            empname = ds.Tables(0).Rows(0).Item("AUR_FULL_NAME").ToString
    '            empemail = ds.Tables(0).Rows(0).Item("AUR_EMAIL").ToString
    '        End If


    '        'Mail message for the Requisitioner. 
    '        msg = "Dear " & empname & "," & "<br>" & "<br>" & _
    '         "This is to inform you that your request for Preventive Maintenance has been changed and sent to your approver for approval." & "<br>" & _
    '         "The Requisition ID and date of requisition are " & rid & " and " & reqdate & "." & "<br>" & "<br>" & _
    '         "Thanking You," & "<br>" & _
    '         "Amantra Admin."
    '        '----------------------------------------------------------------------------------------------


    '        param = New SqlParameter(1) {}
    '        param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
    '        param(0).Value = reqid
    '        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
    '        param(1).Value = Session("uid")
    '        ds = New DataSet
    '        ds = objsubsonic.GetSubSonicDataSet("GET_PVM_MAINDETAILS_AUR_ID", param)
    '        If ds.Tables(1).Rows.Count > 0 Then
    '            appempname = ds.Tables(1).Rows(0).Item("AUR_FULL_NAME").ToString
    '            appempemail = ds.Tables(1).Rows(0).Item("AUR_EMAIL").ToString
    '            rid = ds.Tables(1).Rows(0).Item("PVM_PLAN_ID").ToString
    '            reqdate = ds.Tables(1).Rows(0).Item("PVM_CREATED_DT").ToString
    '        End If


    '        'Mail message for the Approver. 
    '        msgadm = "Dear " & appempname & "," & "<br>" & "<br>" & _
    '        "This is to inform you that a request for Preventive Maintenance has been changed. " & "<br>" & _
    '        "And it is pending for your approval.The Requisition ID and date of requisitions are " & rid & " and " & _
    '        "" & reqdate & "." & "<br>" & _
    '        "Kindly log into " & Application("AppURL") & " to approve ." & "<br>" & "<br>" & _
    '        "Thanking You," & "<br>" & _
    '        "Amantra Admin."


    '        If empemail <> "" Then                   'Mail  for the Requisitioner. 
    '            'Send_Mail(empemail, reqid, subj, msg)

    '            param = New SqlParameter(6) {}
    '            param(0) = New SqlParameter("@AMT_ID", SqlDbType.NVarChar, 200)
    '            param(0).Value = rid
    '            param(1) = New SqlParameter("@AMT_MESSAGE", SqlDbType.NVarChar, 200)
    '            param(1).Value = msg
    '            param(2) = New SqlParameter("@AMT_MAIL_TO", SqlDbType.NVarChar, 200)
    '            param(2).Value = empemail
    '            param(3) = New SqlParameter("@AMT_SUBJECT", SqlDbType.NVarChar, 200)
    '            param(3).Value = subj
    '            param(4) = New SqlParameter("@AMT_MAIL_TIME", SqlDbType.DateTime)
    '            param(4).Value = getoffsetdatetime(DateTime.Now)
    '            param(5) = New SqlParameter("@AMT_FLAG", SqlDbType.NVarChar, 200)
    '            param(5).Value = "Changed"
    '            param(6) = New SqlParameter("@AMT_TYPE", SqlDbType.NVarChar, 200)
    '            param(6).Value = "NORMAL MAIL"
    '            ObjSubSonic.GetSubSonicExecute("INSERT_AMT_MAIL", param)




    '        End If

    '        If appempemail <> "" Then                 'Mail  to Approver.
    '            'Send_Mail(appempemail, rid, subj, msgadm)
    '            Dim t1 As String


    '            param = New SqlParameter(6) {}
    '            param(0) = New SqlParameter("@AMT_ID", SqlDbType.NVarChar, 200)
    '            param(0).Value = rid
    '            param(1) = New SqlParameter("@AMT_MESSAGE", SqlDbType.NVarChar, 200)
    '            param(1).Value = msgadm
    '            param(2) = New SqlParameter("@AMT_MAIL_TO", SqlDbType.NVarChar, 200)
    '            param(2).Value = appempemail
    '            param(3) = New SqlParameter("@AMT_SUBJECT", SqlDbType.NVarChar, 200)
    '            param(3).Value = subj
    '            param(4) = New SqlParameter("@AMT_MAIL_TIME", SqlDbType.DateTime)
    '            param(4).Value = getoffsetdatetime(DateTime.Now)
    '            param(5) = New SqlParameter("@AMT_FLAG", SqlDbType.NVarChar, 200)
    '            param(5).Value = "Changed"
    '            param(6) = New SqlParameter("@AMT_TYPE", SqlDbType.NVarChar, 200)
    '            param(6).Value = "NORMAL MAIL"
    '            ObjSubSonic.GetSubSonicExecute("INSERT_AMT_MAIL", param)



    '        End If
    '    Catch ex As Exception
    '        'Dim pageURL, trace, MSG As String
    '        Session("pageURL") = ex.GetBaseException.ToString
    '        Session("trace") = (ex.StackTrace.ToString)
    '        Session("MSG") = Session("pageURL") + Session("Trace")
    '        Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
    '    End Try
    'End Sub

    'Public Sub mail_PVMUpdate(ByVal reqid As String)
    '    Try

    '        subj = "Preventive Maintenance Updated."




    '        param = New SqlParameter(1) {}
    '        param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
    '        param(0).Value = reqid
    '        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
    '        param(1).Value = Session("uid")
    '        ds = New DataSet
    '        ds = objsubsonic.GetSubSonicDataSet("GET_PVM_MAINDETAILS_AUR_ID", param)
    '        If ds.Tables(0).Rows.Count > 0 Then
    '            rid = ds.Tables(0).Rows(0).Item("PVM_PLAN_ID").ToString
    '            reqdate = ds.Tables(0).Rows(0).Item("PVM_CREATED_DT").ToString
    '            empname = ds.Tables(0).Rows(0).Item("AUR_FULL_NAME").ToString
    '            empemail = ds.Tables(0).Rows(0).Item("AUR_EMAIL").ToString
    '        End If



    '        'Mail message for the Requisitioner. 
    '        msg = "Dear " & empname & "," & "<br>" & "<br>" & _
    '         "This is to inform you that your request for Preventive Maintenance has been changed and sent to your approver for approval." & "<br>" & _
    '         "The Requisition ID and date of requisition are " & rid & " and " & reqdate & "." & "<br>" & "<br>" & _
    '         "Thanking You," & "<br>" & _
    '         "Amantra Admin."
    '        '---------------------------------------



    '        param = New SqlParameter(1) {}
    '        param(0) = New SqlParameter("@PLAN_ID", SqlDbType.NVarChar, 200)
    '        param(0).Value = reqid
    '        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
    '        param(1).Value = Session("uid")
    '        ds = New DataSet
    '        ds = objsubsonic.GetSubSonicDataSet("GET_PVM_MAINDETAILS_AUR_ID", param)
    '        If ds.Tables(1).Rows.Count > 0 Then
    '            appempname = ds.Tables(1).Rows(0).Item("AUR_FULL_NAME").ToString
    '            appempemail = ds.Tables(1).Rows(0).Item("AUR_EMAIL").ToString
    '            rid = ds.Tables(1).Rows(0).Item("PVM_PLAN_ID").ToString
    '            reqdate = ds.Tables(1).Rows(0).Item("PVM_CREATED_DT").ToString
    '        End If

    '        'Mail message for the Approver. 
    '        msgadm = "Dear " & appempname & "," & "<br>" & "<br>" & _
    '        "This is to inform you that your request for Preventive Maintenance has been updated. " & "<br>" & _
    '        "The Requisition ID and date of requisitions are " & rid & " and " & _
    '        "" & reqdate & "." & "<br>" & "<br>" & _
    '        "Thanking You," & "<br>" & _
    '        "Amantra Admin."


    '        If empemail <> "" Then                   'Mail  for the Requisitioner. 
    '            'Send_Mail(empemail, reqid, subj, msg)



    '            param = New SqlParameter(6) {}
    '            param(0) = New SqlParameter("@AMT_ID", SqlDbType.NVarChar, 200)
    '            param(0).Value = rid
    '            param(1) = New SqlParameter("@AMT_MESSAGE", SqlDbType.NVarChar, 200)
    '            param(1).Value = msg
    '            param(2) = New SqlParameter("@AMT_MAIL_TO", SqlDbType.NVarChar, 200)
    '            param(2).Value = empemail
    '            param(3) = New SqlParameter("@AMT_SUBJECT", SqlDbType.NVarChar, 200)
    '            param(3).Value = subj
    '            param(4) = New SqlParameter("@AMT_MAIL_TIME", SqlDbType.DateTime)
    '            param(4).Value = getoffsetdatetime(DateTime.Now)
    '            param(5) = New SqlParameter("@AMT_FLAG", SqlDbType.NVarChar, 200)
    '            param(5).Value = "Updation"
    '            param(6) = New SqlParameter("@AMT_TYPE", SqlDbType.NVarChar, 200)
    '            param(6).Value = "NORMAL MAIL"
    '            ObjSubSonic.GetSubSonicExecute("INSERT_AMT_MAIL", param)


    '        End If

    '        If appempemail <> "" Then                 'Mail  to Approver.
    '            'Send_Mail(appempemail, rid, subj, msgadm)

    '            param = New SqlParameter(6) {}
    '            param(0) = New SqlParameter("@AMT_ID", SqlDbType.NVarChar, 200)
    '            param(0).Value = rid
    '            param(1) = New SqlParameter("@AMT_MESSAGE", SqlDbType.NVarChar, 200)
    '            param(1).Value = msgadm
    '            param(2) = New SqlParameter("@AMT_MAIL_TO", SqlDbType.NVarChar, 200)
    '            param(2).Value = appempemail
    '            param(3) = New SqlParameter("@AMT_SUBJECT", SqlDbType.NVarChar, 200)
    '            param(3).Value = subj
    '            param(4) = New SqlParameter("@AMT_MAIL_TIME", SqlDbType.DateTime)
    '            param(4).Value = getoffsetdatetime(DateTime.Now)
    '            param(5) = New SqlParameter("@AMT_FLAG", SqlDbType.NVarChar, 200)
    '            param(5).Value = "Updation"
    '            param(6) = New SqlParameter("@AMT_TYPE", SqlDbType.NVarChar, 200)
    '            param(6).Value = "NORMAL MAIL"
    '            ObjSubSonic.GetSubSonicExecute("INSERT_AMT_MAIL", param)



    '        End If
    '    Catch ex As Exception
    '        'Dim pageURL, trace, MSG As String
    '        Session("pageURL") = ex.GetBaseException.ToString
    '        Session("trace") = (ex.StackTrace.ToString)
    '        Session("MSG") = Session("pageURL") + Session("Trace")
    '        Response.Redirect(Mid(Request.Url.ToString(), 1, InStr(Request.Url.ToString(), "amantra", CompareMethod.Text) + 7) & "\frmCommonGlobalErrorPage.aspx?msg=" & ex.Message)
    '    End Try
    'End Sub
     
End Class
