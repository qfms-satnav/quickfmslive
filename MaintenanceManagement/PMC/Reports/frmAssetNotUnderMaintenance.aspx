<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmAssetNotUnderMaintenance.aspx.vb" Inherits="MaintenanceManagement_PMC_Reports_frmAssetNotUnderMaintenance"
    Title="" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Warranty Expired Assets
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowSummary="true" ShowMessageBox="false"
                                CssClass="alert alert-danger" ForeColor="Red"></asp:ValidationSummary>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" Visible="false">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <asp:CompareValidator ID="cvfBuilding" runat="server" ValueToCompare="--Select--" ControlToValidate="cboBuilding" Operator="NotEqual" Display="None" ErrorMessage="Please Select Location">
                            </asp:CompareValidator>
                            <asp:CompareValidator ID="cvfFloor" runat="server" ValueToCompare="--Select--" ControlToValidate="cboGrp" Operator="NotEqual" Display="None" ErrorMessage="Please Select Asset Group"></asp:CompareValidator>
                            <asp:CompareValidator ID="cvfGrpType" runat="server" ValueToCompare="--Select--" ControlToValidate="cboGrpType" Operator="NotEqual" Display="None" ErrorMessage="Please Select Asset Group Type"></asp:CompareValidator>
                            <asp:CompareValidator ID="cvfBrd" runat="server" ValueToCompare="--Select--" ControlToValidate="cboBrd" Operator="NotEqual" Display="None" ErrorMessage="Please Select Asset Brand"></asp:CompareValidator>
                            <asp:CompareValidator ID="cvfMdl" runat="server" ValueToCompare="--Select--" ControlToValidate="cboMdl" Operator="NotEqual" Display="None" ErrorMessage="Please Select Asset Model"></asp:CompareValidator>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Location</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="cboBuilding" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Group</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="cboGrp" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Group Type</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="cboGrpType" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Brand</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="cboBrd" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Asset Model</label>
                                            <div class="col-md-7">
                                                <asp:DropDownList ID="cboMdl" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">

                                        <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" Text="View"></asp:Button>

                                        <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" CausesValidation="False"></asp:Button>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 ">
                                    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                </div>
                            </div>
                            <asp:Label ID="lblNo" runat="server" Visible="False" Font-Bold="True" CssClass="clsMessage">All Assets Are Under Maintenance</asp:Label>
                            <asp:CompareValidator ID="cvfGrp" runat="server" Visible="False" ValueToCompare="--Select--" ControlToValidate="cboGrp" Operator="NotEqual" Display="None" ErrorMessage="Select Asset Group">
                            </asp:CompareValidator>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
