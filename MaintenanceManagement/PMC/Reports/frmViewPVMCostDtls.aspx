<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmViewPVMCostDtls.aspx.vb" Inherits="MaintenanceManagement_PMC_Reports_frmViewPVMCostDtls"
    Title="" %>

<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Preventive Maintenance Cost Details
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="False" ShowSummary="True" CssClass="alert alert-danger" ForeColor="Red"></asp:ValidationSummary>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location</label>
                                        <asp:CompareValidator ID="cvfBuilding" runat="server" ControlToValidate="cboBuilding" Display="None" ErrorMessage="Please Select Location" Operator="NotEqual" ValueToCompare="--Select--">                                            
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cboBuilding" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">PVM Type</label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="cboType" Display="None" ErrorMessage="Please Select PPM Type" Operator="NotEqual" ValueToCompare="--Select--">
                                            
                                        </asp:CompareValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cboType" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date</label>
                                        <asp:RequiredFieldValidator ID="rvfFrom" runat="server" ControlToValidate="txtFromDate" Display="None" ErrorMessage="Please Select From Date">                                           
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date</label>
                                        <asp:RequiredFieldValidator ID="rvfTo" runat="server" ControlToValidate="txtToDate" Display="None" ErrorMessage="Please Select To Date">                                           
                                        </asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <%-- <asp:Button ID="cmdExcel" runat="server" CssClass="clsButton" Text="Export to Excel" />--%>
                                    <asp:Button ID="btnView" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnView_Click" Text="View" />
                                    <asp:Button ID="btnBack" runat="server" CausesValidation="False" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" Text="Back" />
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12 ">
                                <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblWarn" runat="server" Font-Bold="True" ForeColor="Navy" CssClass="col-md-12 control-label">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
