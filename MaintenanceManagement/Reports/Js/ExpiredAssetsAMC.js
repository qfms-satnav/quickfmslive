﻿app.service("ExpiredAssetsAMCService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.BindDataToGrid = function (Wrnty) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ExpiredAssetsAMC/BindDataToGrid', Wrnty)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('ExpiredAssetsAMCController', ['$scope', '$q', '$http', 'UtilityService', 'ExpiredAssetsAMCService','$timeout', function ($scope, $q, $http, UtilityService, ExpiredAssetsAMCService, $timeout) {
    $scope.ExpiredAssets = {};
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Loclist = [];
    $scope.Grplist = [];
    $scope.GrpTypelist = [];
    $scope.Brndlist = [];
    $scope.Modellist = [];

    $scope.columnDefs = [
            { headerName: "Asset Group Type", field: "CATEGORY", width: 200, cellClass: 'grid-align', },
            { headerName: "Asset Model", field: "MODEL", cellClass: 'grid-align', width: 200, },
            { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 200 },
            { headerName: "Asset Name", field: "Asset_Name", cellClass: 'grid-align', width: 200 },
            { headerName: "Vendor", field: "VENDOR", cellClass: 'grid-align', width: 200, },
            { headerName: "From Date", field: "FROM_DATE", cellClass: 'grid-align', width: 200, },
            { headerName: "To Date", field: "TO_DATE", cellClass: 'grid-align', width: 200, },

    ];

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Loclist = response.data;
            $scope.Loclist[0].ticked = true;
        }
    });
 
    $scope.ExpiredAssets.FromDate = moment(new Date()).format('MM/DD/YYYY');
    $scope.LoadData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,

            getRows: function (params) {
                var params =
                {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    selectedLoc: $scope.ExpiredAssets.selectedLoc,
                    FromDate: $scope.ExpiredAssets.FromDate,
                    CNP_NAME: $scope.ExpiredAssets.CNP_NAME[0].CNP_ID
                }
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                ExpiredAssetsAMCService.BindDataToGrid(params).then(function (response) {
                    $scope.gridata = response;
                    if ($scope.gridata.data == null) {
                        $("#btNext").attr("disabled", true);
                        $scope.gridOptions.api.setRowData([]);
                        progress(0, 'Loading...', false);
                        setTimeout(function () { showNotification('error', 8, 'bottom-right', $scope.gridata.Message); }, 200);

                    }
                    else {
                        $scope.gridOptions.api.setRowData([]);
                        $scope.gridOptions.api.setRowData($scope.gridata.data);
                        progress(0, 'Loading...', false);

                    }
                }, function (error) {
                    console.log(error);
                });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);

    };
    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.ExpiredAssets.CNP_NAME = parseInt(CompanySessionId);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                    a.ticked = true;
                });
                if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 500);

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };


    $scope.GenReport = function (ExpiredAssets, Type) {
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        eaobj = {};
        angular.copy(ExpiredAssets, eaobj);
        eaobj.SearchValue = searchval;
        eaobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        eaobj.PageSize = $scope.gridata.data[0].OVERALL_COUNT;
        eaobj.CNP_NAME = $scope.ExpiredAssets.CNP_NAME[0].CNP_ID;
        eaobj.Type = Type;

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/ExpiredAssetsAMC/ExportReportdata',
                method: 'POST',
                data: eaobj,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'ExpiredAssetsAMC.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Asset Group Type", key: "CATEGORY" }, { title: "Asset Model ", key: "MODEL" }, { title: "Location", key: "LOCATION" },
            { title: "Asset Name", key: "Asset_Name" }, { title: "Vendor", key: "VENDOR" }, { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("ExpiredAssetsAMC.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ExpiredAssetsAMC.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

 
    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
}]);