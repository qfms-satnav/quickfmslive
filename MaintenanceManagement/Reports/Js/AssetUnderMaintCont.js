﻿app.service("AssetUnderMaintContService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.GetGriddata = function (MntRpt) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetUnderMaintCont/GetGriddata', MntRpt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };
    this.GetDetailsOnSelection = function (MaitDetails) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetUnderMaintCont/GetDetailsOnSelection/', MaitDetails)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('AssetUnderMaintContController', ['$scope', '$q', '$http', 'UtilityService', 'AssetUnderMaintContService','$timeout', function ($scope, $q, $http, UtilityService, AssetUnderMaintContService, $timeout) {
    $scope.MaintCont = {};
    $scope.DocTypeVisible = 0;
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;
    $scope.Sel = {};

    $scope.columnDefs = [
        { headerName: "Plan Id", field: "PLANID", cellClass: "grid-align", template: '<a ng-click="ShowPopup(data)">{{data.PLANID}}</a>', filter: 'text', pinned: 'left', suppressMenu: true },
        { headerName: "Asset Group Type", field: "AST_SUBCAT_NAME", cellClass: "grid-align" },
        { headerName: "Asset Model", field: "AST_MD_NAME", cellClass: "grid-align" },
        { headerName: "Maintenance Contract Vendor", field: "AVR_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
        { headerName: "From Date", field: "AMN_FROM_DATE", template: '<span>{{data.AMN_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200, },
        { headerName: "To Date", field: "AMN_TO_DATE", template: '<span>{{data.AMN_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200, }];
    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.MaintCont.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }
            progress(0, 'Loading...', false);
        });
    }, 500);

    $scope.LoadData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,
            getRows: function (params) {
        progress(0, 'Loading...', true);
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
            CNP_NAME: $scope.MaintCont.CNP_NAME[0].CNP_ID
        };
        $scope.GridVisiblity = true;
        AssetUnderMaintContService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $("#btNext").attr("disabled", true);
                $scope.gridOptions.api.setRowData([]);
            }
            else {

                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            progress(0, 'Loading...', false);
        });
   
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    $scope.PopDefs = [
          { headerName: "Assets", field: "AAT_NAME", width: 500, cellClass: 'grid-align', suppressMenu: true, }];

    $scope.PopOptions = {
        columnDefs: $scope.PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    }
    $scope.ShowPopup = function (data) {
        $scope.Sel = data.PLANID;
        $("#historymodal").modal('show');
        var obj = { PLAN_ID: data.PLANID };
        AssetUnderMaintContService.GetDetailsOnSelection(obj).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.popdata = response;
            $scope.PopOptions.api.setRowData($scope.popdata);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 500);
        });
    }
    //$('#historymodal').on('shown.bs.modal', function () {
      
    //});
    $scope.GenReport = function (MaintCont, Type) {
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        woobj = {};
        angular.copy(MaintCont, woobj);
        woobj.SearchValue = searchval;
        woobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        woobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        woobj.CNP_NAME = $scope.MaintCont.CNP_NAME[0].CNP_ID;
        woobj.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/AssetUnderMaintCont/GetGrid',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'AssetUnderMaintenance.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function (error,data, status, headers, config) {

            };
        };
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Asset Group", key: "AAG_MFTYPE" }, { title: "Asset Group Type", key: "AGT_NAME" },
            { title: "Asset Brand", key: "AAB_NAME" }, { title: "Asset Model", key: "AAM_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Asset Code", key: "ASSET_CODE" },
            { title: "Warranty Date", key: "AAP_WRNT_DATE" }, { title: "Asset Vendor", key: "AVR_NAME" }, { title: "AMC Status", key: "AMC_STATUS" }, { title: "Preventive Status", key: "PVM_STATUS" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("AssetUnderMaintenance.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "AssetUnderMaintenance.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
}]);