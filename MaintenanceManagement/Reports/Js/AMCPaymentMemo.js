﻿app.service("AMCPaymentMemoService", function ($http, $q, UtilityService) {
    this.GetGriddata = function (MntRpt) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AMCPaymentMemo/GetGriddata', MntRpt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };

    this.GetGrid = function (MntRpt) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AMCPaymentMemo/GetGrid', MntRpt)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };
});
app.controller('AMCPaymentMemoController', function ($scope, $q, $http, UtilityService, AMCPaymentMemoService, $timeout) {
    $scope.MaintCont = {};
    $scope.DocTypeVisible = 0;
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;
    $scope.Sel = {};
    $scope.GridVisiblity = false;



    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.MaintCont.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }
            progress(0, 'Loading...', false);
        });
    }, 500);

    $scope.LoadData = function () {        
        progress(0, 'Loading...', true);
       
        var params = {
            CNP_NAME: $scope.MaintCont.CNP_NAME[0].CNP_ID
        };
        $scope.GridVisiblity = true;
        AMCPaymentMemoService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            progress(0, 'Loading...', false);
        });
    }, function (error) {
        console.log(error);
    }

    $scope.columnDefs = [
    { headerName: "Plan Id", field: "AMN_PLAN_ID", cellClass: "grid-align", suppressMenu: true },
    { headerName: "Payment Memo Id", field: "APM_PAYMEMO_ID", cellClass: "grid-align" },
    { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
    { headerName: "Vendor", field: "AVR_NAME", cellClass: "grid-align" },    
    { headerName: "From Date", field: "AMN_FROM_DATE", template: '<span>{{data.AMN_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200, },
    { headerName: "To Date", field: "AMN_TO_DATE", template: '<span>{{data.AMN_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 200, },
    { headerName: "Payment Advice No.", field: "APM_MPAPAYADVICE_NO", cellClass: "grid-align" },
    { headerName: "Bill No", field: "APM_BILL_NO", cellClass: "grid-align" },
    { headerName: "Bill Date", field: "APM_BILL_DATE", cellClass: "grid-align" },    
    { headerName: "Amount", field: "APM_NET_PAYABLE", cellClass: "grid-align", cellStyle: { 'text-align':'right' }}];
    

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };
    setTimeout(function () {
        progress(0, 'Loading...', true);
        $scope.LoadData();
        progress(0, 'Loading...', false);
    }, 1000);


    $scope.GenReport = function (MaintCont, Type) {
        progress(0, 'Loading...', true);
        //MaintCont.CNP_NAME = $scope.MaintCont.CNP_NAME[0].CNP_ID
        //MaintCont.Type = Type;
        amcobj = {};
        amcobj.CNP_NAME = MaintCont.CNP_NAME[0].CNP_ID;
        amcobj.Type = Type;
        amcobj.FromDate = MaintCont.FromDate;
        amcobj.ToDate = MaintCont.ToDate;

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/AMCPaymentMemo/GetGrid',
                method: 'POST',
                data: amcobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'AMCPaymentMemo.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function (error,data, status, headers, config) {

            };
        };
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Plan Id", key: "AMN_PLAN_ID" }, { title: "Payment Memo Id", key: "APM_PAYMEMO_ID" }, { title: "Vendor", key: "AVR_NAME" }, { title: "Location", key: "LCM_NAME" },
            { title: "From Date", key: "AMN_FROM_DATE" }, { title: "To Date", key: "AMN_TO_DATE" }, { title: "Payment Advice No.", key: "APM_MPAPAYADVICE_NO" },
            { title: "Amount", key: "APM_NET_PAYABLE" }, { title: "Bill No", key: "APM_BILL_NO" }, { title: "Bill Date", key: "APM_BILL_DATE" }, { title: "Remarks", key: "APM_REMARKS" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("AMCPaymentMemo.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "AMCPaymentMemo.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    
});