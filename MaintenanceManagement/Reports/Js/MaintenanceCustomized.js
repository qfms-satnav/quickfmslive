﻿app.service("MaintenanceCustomizedService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetCustomizedDetails = function (MntRpt) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaintenanceCustomized/GetCustomizedDetails', MntRpt)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetAssetDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/MaintenanceCustomized/GetAssetDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('MaintenanceCustomizedController', ['$scope', '$q', '$http', 'UtilityService', 'MaintenanceCustomizedService', '$timeout', function ($scope, $q, $http, UtilityService, MaintenanceCustomizedService, $timeout) {
    $scope.MaintCust = {};
    $scope.GridVisiblity = true;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Cols = [
        { COL: "Asset Group", value: "AAG_MFTYPE", ticked: false },
        { COL: "Asset Group Type", value: "AGT_NAME", ticked: false },
        { COL: "Asset Brand", value: "AAB_NAME", ticked: false },
        { COL: "Location", value: "LCM_NAME", ticked: false },
        { COL: "City", value: "CTY_NAME", ticked: false },
        { COL: "Country", value: "CNY_NAME", ticked: false },
        { COL: "Asset Code", value: "ASSET_CODE", ticked: false },
        { COL: "Warranty Date", value: "AAP_WRNT_DATE", ticked: false },
        { COL: "AMC From Date", value: "AMN_FROM_DATE", ticked: false },
        { COL: "AMC To Date", value: "AMN_TO_DATE", ticked: false },
        { COL: "Asset Vendor", value: "AVR_NAME", ticked: false },
        { COL: "AMC Status", value: "AMC_STATUS", ticked: false },
        { COL: "Preventive Status", value: "PVM_STATUS", ticked: false },
        { COL: "PPM Plan  Frequency", value: "PLAN_FREQUENCY", ticked: false },
        { COL: "Break Down Status", value: "BRK_STATUS", ticked: false },
        { COL: "Labour Cost", value: "PVD_PLANLABOUR_COST", ticked: false },
        { COL: "Spare Cost", value: "PVD_PLANSPARES_COST", ticked: false },
        { COL: "Company Name", value: "CNP_NAME", ticked: false },
        { COL: "PPM Date", value: "PVD_PLANSCHD_DT", ticked: false },
        { COL: "Remarks", value: "PVD_PLAN_REMARKS", ticked: false },
        { COL: "Project", value: "TAG_NAME", ticked: false }];

    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.columnDefs = [
        { headerName: "Asset Group", field: "AAG_MFTYPE", width: 100, cellClass: 'grid-align', width: 100, },
        { headerName: "Asset Group Type", field: "AGT_NAME", cellClass: 'grid-align', width: 150, },
        { headerName: "Asset Brand", field: "AAB_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Country", field: "CNY_NAME", cellClass: 'grid-align', width: 200 },
        { headerName: "Asset Name", field: "ASSET_CODE", cellClass: 'grid-align', width: 300 },
        { headerName: "Warranty Date", field: "AAP_WRNT_DATE", cellClass: 'grid-align', width: 150, template: '<span>{{data.AAP_WRNT_DATE | date:"dd MMM, yyyy"}}</span>', },
        { headerName: "AMC From Date", field: "AMN_FROM_DATE", cellClass: 'grid-align', width: 150, template: '<span>{{data.AMN_FROM_DATE | date:"dd MMM, yyyy"}}</span>', },
        { headerName: "AMC To Date", field: "AMN_TO_DATE", cellClass: 'grid-align', width: 150, template: '<span>{{data.AMN_TO_DATE | date:"dd MMM, yyyy"}}</span>', },
        { headerName: "Asset Vendor", field: "AVR_NAME", cellClass: 'grid-align', width: 100, },
        { headerName: "AMC Status", field: "AMC_STATUS", cellClass: 'grid-align', width: 200 },
        { headerName: "Preventive Status", field: "PVM_STATUS", cellClass: 'grid-align', width: 150, },
        { headerName: "PPM Plan  Frequency", field: "PLAN_FREQUENCY", cellClass: 'grid-align', width: 150, },
        { headerName: "Break Down Status", field: "BRK_STATUS", cellClass: 'grid-align', width: 150, },
        { headerName: "Labour Cost", field: "PVD_PLANLABOUR_COST", cellClass: 'grid-align', width: 150, cellStyle: { 'text-align': 'right' }, },
        { headerName: "Spare Cost", field: "PVD_PLANSPARES_COST", cellClass: 'grid-align', width: 150, cellStyle: { 'text-align': 'right' } },
        { headerName: "Company Name", field: "CNP_NAME", cellClass: 'grid-align', width: 150, cellStyle: { 'text-align': 'right' } },
        { headerName: "PPM Date", field: "PVD_PLANSCHD_DT", cellClass: 'grid-align', width: 150, template: '<span>{{data.PVD_PLANSCHD_DT | date:"dd MMM, yyyy"}}</span>', },
        { headerName: "Executed Date", field: "PVD_PLANEXEC_DT", cellClass: 'grid-align', width: 150, template: '<span>{{data.PVD_PLANEXEC_DT | date:"dd MMM, yyyy"}}</span>', },
        { headerName: "Status", field: "STA_TITLE", cellClass: 'grid-align', width: 150, },
        { headerName: "Project", field: "TAG_NAME", cellClass: 'grid-align', width: 150, },
        { headerName: "Remarks", field: "PVD_PLAN_REMARKS", cellClass: 'grid-align', width: 150, cellStyle: { 'text-align': 'right' } }];

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.MaintCust.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });

    }, 100);

    $scope.LoadData = function () {
        var fromdate = moment($scope.MaintCust.FromDate);
        var todate = moment($scope.MaintCust.ToDate);
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {

            rowCount: null,

            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    FromDate: $scope.MaintCust.FromDate,
                    ToDate: $scope.MaintCust.ToDate,
                    Request_Type: $scope.MaintCust.Request_Type,
                    CNP_NAME: $scope.MaintCust.CNP_NAME[0].CNP_ID,
                    /*  tag_name: $scope.MaintCust.tag_name[0].tag_name*/
                    tag_name: _.filter($scope.MaintCust.tag_name, function (o) { return o.ticked == true; }).map(function (x) { return x.tag_name; }).join(','),

                };



                if (fromdate > todate) {
                    $scope.GridVisiblity = false;
                    showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                }
                else {
                    progress(0, 'Loading...', true);
                    $scope.GridVisiblity = true;

                    MaintenanceCustomizedService.GetCustomizedDetails(params).then(function (data) {
                        $scope.gridata = data;
                        if ($scope.gridata == null) {
                            $("#btNext").attr("disabled", true);
                            $scope.gridOptions.api.setRowData([]);
                        }
                        else {
                            $scope.gridOptions.api.setRowData([]);
                            $scope.gridOptions.api.setRowData($scope.gridata.data);
                            var cols = [];
                            var unticked = _.filter($scope.Cols, function (item) {
                                return item.ticked == false;
                            });
                            var ticked = _.filter($scope.Cols, function (item) {
                                return item.ticked == true;
                            });
                            for (i = 0; i < unticked.length; i++) {
                                cols[i] = unticked[i].value;
                            }
                            $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                            cols = [];
                            for (i = 0; i < ticked.length; i++) {
                                cols[i] = ticked[i].value;
                            }
                            $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
                        }

                        progress(0, 'Loading...', false);
                    }, function (error) {
                        console.log(error);
                    });
                }
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);

    };


    MaintenanceCustomizedService.GetAssetDetails().then(function (response) {

        if (response.data != null) {
            $scope.Type = response.data;
            angular.forEach($scope.Type, function (value, key) {
                value.ticked = true;
            })
        }
        else
            showNotification('error', 8, 'bottom-right', response.Message);
    }, function (response) {
        progress(0, '', false);
    });



    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    function groupAggFunction(rows) {
        var sums = {
            Asset_count: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.Asset_count += (data.Asset_count);
        });
        return sums;
    }

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();

        var params = {
            SearchValue: searchval,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: $scope.gridata.data[0].OVERALL_COUNT,
            FromDate: $scope.MaintCust.FromDate,
            ToDate: $scope.MaintCust.ToDate,
            Request_Type: $scope.MaintCust.Request_Type,
            CNP_NAME: $scope.MaintCust.CNP_NAME[0].CNP_ID,
            Type: Type,
            /*  tag_name: $scope.MaintCust.tag_name[0].tag_name*/
            tag_name: _.filter($scope.MaintCust.tag_name, function (o) { return o.ticked == true; }).map(function (x) { return x.tag_name; }).join(','),

        };
        //Customized.SearchValue = searchval;
        //Customized.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        //Customized.PageSize = $scope.gridata.data[0].OVERALL_COUNT;
        //Customized.Type = Type;
        //Customized.loclst = Customized.Locations;
        //Customized.tag_name = _.filter($scope.MaintCust.tag_name, function (o) { return o.ticked == true; }).map(function (x) { return x.tag_name; }).join(',');
                if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/MaintenanceCustomized/GetGrid',
                method: 'POST',
                data: params,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'MaintenanceCustomizableReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function (error,data, status, headers, config) {

            };
        };
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Asset Group", key: "AAG_MFTYPE" }, { title: "Asset Group Type", key: "AGT_NAME" },
        { title: "Asset Brand", key: "AAB_NAME" }, { title: "Asset Model", key: "AAM_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Asset Code", key: "ASSET_CODE" },
        { title: "Warranty Date", key: "AAP_WRNT_DATE" }, { title: "Asset Vendor", key: "AVR_NAME" }, { title: "AMC Status", key: "AMC_STATUS" },
            { title: "Preventive Status", key: "PVM_STATUS" }, { title: "Project", key: "TAG_NAME" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("MaintenanceCustomizableReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "MaintenanceCustomizableReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }



    $scope.selVal = "THISYEAR";
    $scope.Request_Type = "0";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.MaintCust.FromDate = "";
                $scope.MaintCust.ToDate = "";
                break;
            case 'TODAY':
                $scope.MaintCust.FromDate = moment().format('MM/DD/YYYY');
                $scope.MaintCust.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.MaintCust.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.MaintCust.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.MaintCust.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.MaintCust.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.MaintCust.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.MaintCust.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.MaintCust.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.MaintCust.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.MaintCust.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.MaintCust.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.MaintCust.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.MaintCust.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;
        }
    }
    $scope.rptDateRanges();
    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
    $scope.MaintCust.Request_Type = "0";
}]);