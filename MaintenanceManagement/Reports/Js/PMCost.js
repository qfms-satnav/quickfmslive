﻿app.service("PMCostService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.GetCostDetails = function (Cost) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/PMCost/GetCostDetails', Cost)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('PMCostController', ['$scope', '$q', '$http', 'UtilityService', 'PMCostService','$timeout', function ($scope, $q, $http, UtilityService, PMCostService, $timeout) {
    $scope.Cost = {};
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.Loclist = [];

    $scope.columnDefs = [
            { headerName: "Asset / Service Name", field: "PVM_PLAN_TYPE", width: 350, cellClass: 'grid-align', },
            { headerName: "Asset/Service Id", field: "PVM_ASSET_NAME", cellClass: 'grid-align', width: 350, },
            { headerName: "Executed Date", field: "PVD_PLANEXEC_DT", cellClass: 'grid-align', width: 150, template: '<span>{{data.PVD_PLANEXEC_DT | date:"dd MMM, yyyy"}}</span>', },
            { headerName: "Status", field: "STA_TITLE", cellClass: 'grid-align', width: 350 },
            { headerName: "Spare Cost", field: "PVD_PLANSPARES_COST", cellClass: 'grid-align', width: 350, cellStyle: { 'text-align': 'right' }, },
            { headerName: "Labor Cost", field: "PVD_PLANLABOUR_COST", cellClass: 'grid-align', width: 350, cellStyle: { 'text-align': 'right' }, }
    ];

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Loclist = response.data;
            $scope.Loclist[0].ticked = true;
            angular.forEach($scope.Loclist[0], function (value, key) {
                value.ticked = true;
            });
        }
    });
    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.Cost.CNP_NAME = parseInt(CompanySessionId);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                    a.ticked = true;
                });
                if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }
            progress(0, 'Loading...', false);
        });
    }, 500);

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,
            getRows: function (params) {
        var params =
        {
            SearchValue: searchval,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: 10,
            selectedLoc: $scope.Cost.selectedLoc,
            FromDate: $scope.Cost.FromDate,
            ToDate: $scope.Cost.ToDate,
            CNP_NAME: $scope.Cost.CNP_NAME[0].CNP_ID
        }
        $scope.GridVisiblity = true;
        PMCostService.GetCostDetails(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $("#btNext").attr("disabled", true);
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            progress(0, 'Loading...', false);
        }, function (error) {
            console.log(error);
        });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };

    $scope.GenReport = function (Cost, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        woobj = {};
        angular.copy(Cost, woobj);
        woobj.SearchValue = searchval;
        woobj.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        woobj.PageSize = $scope.gridata[0].OVERALL_COUNT;
        woobj.CNP_NAME = $scope.Cost.CNP_NAME[0].CNP_ID;
        woobj.Type = Type;

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/PMCost/GetGrid',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Preventive Maintenance Cost.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function (error,data, status, headers, config) {

            };
        };
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Asset Name", key: "AAT_NAME" }, { title: "Vendor ", key: "AVR_NAME" }, { title: "Location", key: "LCM_NAME" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("Preventive Maintenance Cost.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Preventive Maintenance Cost.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    setTimeout(function () {
        $scope.LoadData();
    },1000);
}]);