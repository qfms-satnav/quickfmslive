﻿app.service("PMStatusDetailsService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {
    this.GetGriddata = function (MntRpt) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/PMStatusDetails/BindDataToGridList', MntRpt)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;

            });
    };

}]);
app.controller('PMStatusDetailsController', ['$scope', '$q', '$http', 'UtilityService', 'PMStatusDetailsService','$timeout', function ($scope, $q, $http, UtilityService, PMStatusDetailsService, $timeout) {
    $scope.PMStatus = {};
    $scope.DocTypeVisible = 0;
    $scope.EnableStatus = 0;
    $scope.CompanyVisible = 0;
    $scope.Sel = {};
    $scope.Loclist = [];
    $scope.Astlist = [];
    $scope.columnDefs = [
        { headerName: "Asset Group ", field: "AssetGroup", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Asset Group Type", field: "AssetGroupType", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Asset Brand", field: "AssetBrand", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Asset Name", field: "AssetName", cellClass: "grid-align", suppressMenu: true },
        { headerName: "Location", field: "Location", cellClass: "grid-align" },
        { headerName: "Vendor", field: "Vendor", cellClass: "grid-align" },
        { headerName: "Plan Type", field: "PlanType", cellClass: "grid-align" },
        { headerName: "Scheduled Date", field: "ScheduledDate", cellClass: 'grid-align', width: 200, },
        { headerName: "Status", field: "Status", cellClass: "grid-align" },
        //{ headerName: "PVM_PLAN_FREQ", field: "PVM_PLAN_FREQ", cellClass: "grid-align" },

    ];
    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Loclist = response.data;
            $scope.Loclist[0].ticked = true;
        }
    });

    UtilityService.GetTypes().then(function (response) {
        if (response.data != null) {
            $scope.Astlist = response.data;
            $scope.Astlist[0].ticked = true;
        }
    });
    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.PMStatus.CNP_NAME = parseInt(CompanySessionId);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                    a.ticked = true;
                });
                if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
                progress(0, 'Loading...', false);
            }

        });
    }, 500);

    $scope.PMStatus.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
    $scope.PMStatus.ToDate = moment().format('MM/DD/YYYY');
    //$scope.PMStatus.FromDate = moment(new Date()).format('MM/DD/YYYY');
    //$scope.PMStatus.ToDate = moment(new Date()).format('MM/DD/YYYY');

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
            selectedLoc: $scope.PMStatus.selectedLoc,
            FromDate: $scope.PMStatus.FromDate,
            ToDate: $scope.PMStatus.ToDate,
            CNP_NAME: $scope.PMStatus.CNP_NAME[0].CNP_ID,
            selectedgroup: $scope.PMStatus.selectedgroup

        };
        $scope.GridVisiblity = true;
        PMStatusDetailsService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            if ($scope.gridata == null) {
                $("#btNext").attr("disabled", true);
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            progress(0, 'Loading...', false);
        });
   
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        },
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };


    $scope.GenReport = function (PMStatus, Type) {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        RPMStatus = {};
        angular.copy(PMStatus, RPMStatus);
        RPMStatus.SearchValue = searchval;
        RPMStatus.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        RPMStatus.PageSize = $scope.gridata[0].OVERALL_COUNT;
        RPMStatus.CNP_NAME = $scope.PMStatus.CNP_NAME[0].CNP_ID;
        RPMStatus.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/PMStatusDetails/ExportReportdata',
                method: 'POST',
                data: RPMStatus,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'PMStatusDetails.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function (error,data, status, headers, config) {

            };
        };
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Asset Group", key: "AAG_MFTYPE" }, { title: "Asset Group Type", key: "AGT_NAME" },
        { title: "Asset Brand", key: "AAB_NAME" }, { title: "Asset Model", key: "AAM_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Asset Code", key: "ASSET_CODE" },
        { title: "Warranty Date", key: "AAP_WRNT_DATE" }, { title: "Asset Vendor", key: "AVR_NAME" }, { title: "AMC Status", key: "AMC_STATUS" }, { title: "Preventive Status", key: "PVM_STATUS" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("PMStatusDetails.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "PMStatusDetails.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1000);

}]);