<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Lease_Agreement_Details.ascx.vb"
    Inherits="PropertyManagement_Reports_Controls_Lease_Agreement_Details" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Lease Agreement Details
             <hr align="center" width="60%" /></asp:Label>
                        &nbsp;
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                            width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Lease Agreement Details</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                            width="16" /></td>
                </tr>
                <tr>
                    <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                        &nbsp;</td>
                    <td align="left">
                        &nbsp;
                        <cc1:ExportPanel ID="exp_CityWise" runat="server">
                            <asp:Panel ID="panel1" runat="server" Width="100%">
                                <asp:GridView ID="gvitems" runat="Server" AllowPaging="true" PageSize="10" AllowSorting="true"
                                    AutoGenerateColumns="false" EmptyDataText="No Records Found" Width="100%">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Agreement No">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllag_agr_no" runat="server" Text='<%#Eval("lag_agr_no" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Country">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCountry" runat="server" Text='<%#Eval("country" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Region">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRegion" runat="server" Text='<%#Eval("Region" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="State">
                                            <ItemTemplate>
                                                <asp:Label ID="lblState" runat="server" Text='<%#Eval("State" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Centre">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCentre" runat="server" Text='<%#Eval("Centre" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Location" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="GSC">
                                            <ItemTemplate>
                                                <asp:Label ID="lblGSC" runat="server" Text='<%#Eval("GSC" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Premise">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPremise" runat="server" Text='<%#Eval("PRM_NAME" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Deed Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllag_ldeed_dt" runat="server" Text='<%#Eval("lag_ldeed_dt" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Commence Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllag_lcomm_dt" runat="server" Text='<%#Eval("lag_lcomm_dt" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllag_closing_dt" runat="server" Text='<%#Eval("lag_closing_dt" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Period">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLeasePeriod" runat="server" Text='<%#Eval("LAG_Lease_Period" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Occupied By">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOccupiedBy" runat="server" Text='<%#Eval("OccupiedBy" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lock Period">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLAG_Lockin_Per" runat="server" Text='<%#Eval("LAG_LOCKIN_PERIOD" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Lease Lock Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLAG_Lock_DT" runat="server" Text='<%#Eval("LAG_Lock_DT" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Status" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Sta_Name" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Rent">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLAG_MRENT_AMT" runat="server" Text='<%#Eval("LAG_MRENT_AMT" ) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </cc1:ExportPanel>
                        <asp:Button ID="btnExport" runat="server" Width="150px" CssClass="button" Text="Export">
                        </asp:Button>
                    </td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif") %>">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif") %>"
                            width="9" /></td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>" width="25" /> </td>
                    <td>
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                            width="16"/></td>
                </tr>
            </table>
        </div>


</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnExport" />
</Triggers>
</asp:UpdatePanel>