<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PropertyDetails.ascx.vb"
    Inherits="PropertyManagement_Reports_Controls_PropertyDetails" %>
    <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    
    
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Property Details
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                <strong>&nbsp;Property Details</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                &nbsp;<br />
                <asp:Panel ID="panel1" runat="server" Width="100%">
                    <asp:GridView ID="gvitems" runat="Server" AllowPaging="true" PageSize="10" AllowSorting="true"
                        AutoGenerateColumns="false" EmptyDataText="No Records Found" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Agreement No.">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_AGR_NO" runat="server" Text='<%#Eval("LAG_AGR_NO" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Requisition ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblPRM_PREQ_ID" runat="server" Text='<%#Eval("PRM_PREQ_ID" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Centre">
                                <ItemTemplate>
                                    <asp:Label ID="lblCentre" runat="server" Text='<%#Eval("Centre" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Location">
                                <ItemTemplate>
                                    <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Location" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lease Commencement Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_LCOMM_DT" runat="server" Text='<%#Eval("LAG_LCOMM_DT" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lease Expiry Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_CLOSING_DT" runat="server" Text='<%#Eval("LAG_CLOSING_DT" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lease Term (months)">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_Lease_Period" runat="server" Text='<%#Eval("LAG_Lease_Period" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Country">
                                <ItemTemplate>
                                    <asp:Label ID="lblcountry" runat="server" Text='<%#Eval("country" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Currency">
                                <ItemTemplate>
                                    <asp:Label ID="lblINR" runat="server" Text='INR'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address">
                                <ItemTemplate>
                                    <asp:Label ID="lblprm_address" runat="server" Text='<%#Eval("prm_address" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No. of Car Parking" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_cpark_nos" runat="server" Text=''></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Built Up Area (SFT)">
                                <ItemTemplate>
                                    <asp:Label ID="lblBUILTUP" runat="server" Text='<%#Eval("BUILTUP" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Carpet Area (sft)">
                                <ItemTemplate>
                                    <asp:Label ID="lblCARPET" runat="server" Text='<%#Eval("CARPET" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Monthly Rent">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_MRENT_AMT" runat="server" Text='<%#Eval("LAG_MRENT_AMT" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rent Free Period">
                                <ItemTemplate>
                                    <asp:Label ID="lblRENT_FREE_PERIOD" runat="server" Text='<%#Eval("RENT_FREE_PERIOD" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Floor">
                                <ItemTemplate>
                                    <asp:DataGrid ID="dgFloors" runat="server" AutoGenerateColumns="False" Visible="False">
                                        <Columns>
                                            <asp:BoundColumn DataField="Floor" HeaderText="Floor">
                                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="BUILTUP" HeaderText="BuiltUp">
                                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="5%" VerticalAlign="Middle">
                                                </HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundColumn>
                                            <asp:BoundColumn DataField="CARPET" HeaderText="Carpet">
                                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center" Width="5%"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                            </asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Landlords">
                                <ItemTemplate>
                                    <asp:DataGrid ID="dgLandlord" runat="server" AutoGenerateColumns="False" Visible="False">
                                        <Columns>
                                            <asp:BoundColumn DataField="plr_name"  >
                                                <HeaderStyle Font-Bold="True" HorizontalAlign="Center"></HeaderStyle>
                                            </asp:BoundColumn>
                                             
                                        </Columns>
                                    </asp:DataGrid>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rent Free Period">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_LOCKIN_PERIOD" runat="server" Text='<%#Eval("LAG_LOCKIN_PERIOD" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Escalation Term">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_ESC_TERM" runat="server" Text='<%#Eval("LAG_ESC_TERM" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Escalation Percent">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_ESC_PER" runat="server" Text='<%#Eval("LAG_ESC_PER" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rentable Car Parks">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_RENT_CAR_PARKS" runat="server" Text='<%#Eval("LAG_RENT_CAR_PARKS" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Car Park Per Slot">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_CPARK_PER_SLOT" runat="server" Text='<%#Eval("LAG_CPARK_PER_SLOT" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CAM Charges">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_CAM_CHARGES" runat="server" Text='<%#Eval("LAG_CAM_CHARGES" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Renewal Term">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_RENEWAL_TERMINATION" runat="server" Text='<%#Eval("LAG_RENEWAL_TERMINATION" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Interest Free Security Deposit">
                                <ItemTemplate>
                                    <asp:Label ID="lblLAG_INTEREST_FEE_DEPOSIT" runat="server" Text='<%#Eval("LAG_INTEREST_FEE_DEPOSIT" ) %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
		<asp:Button ID="btnExport" runat="server" Width="150px" CssClass="button" Text="Export">
                </asp:Button>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif") %>">
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                <img height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif") %>"
                    width="9" /></td>
            <td background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>" width="25" /></td>
            <td>
                <img height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</div>
</ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExport" />
    </Triggers>
</asp:UpdatePanel>
