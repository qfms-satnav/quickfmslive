<%@ Control Language="VB" AutoEventWireup="false" CodeFile="Lease_Expiry_Details.ascx.vb"
    Inherits="PropertyManagement_Reports_Controls_Lease_Expiry_Details" %>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>--%>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<script src="DateTimePicker.js" type="text/javascript" language="javascript"></script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">    
    <ContentTemplate>        
        <div>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="100%" align="center">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                            ForeColor="Black">Lease Expiry Details
             <hr align="center" width="60%" /></asp:Label>
                        &nbsp;
                        <br />
                    </td>
                </tr>
            </table>
            <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
                <tr>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_left_top_corner.gif")%>"
                            width="9" /></td>
                    <td width="100%" class="tableHEADER" align="left">
                        <strong>&nbsp;Lease Expiry Details</strong>
                    </td>
                    <td>
                        <img alt="" height="27" src="<%=Page.ResolveUrl("~/Images/table_right_top_corner.gif")%>"
                            width="16" /></td>
                </tr>
                <tr>
                    <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                        &nbsp;</td>
                    <td align="left">
                        <br />
                        <table cellpadding="0" width="100%" cellspacing="0" border="1">
                            <tr>
                                <td colspan="4" valign="top" align="center">
                                    <asp:Label ID="lblmsg" CssClass="note" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%">
                                    From Date <font class="clsNote">*</font></td>
                                <td>
                                    <asp:TextBox ID="txtFromDt" runat="server" Width="50%" CssClass="clstextfield"></asp:TextBox>
                                    <%--<cc1:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFromDt">
                                    </cc1:CalendarExtender>--%>
                                </td>
                                </tr>
                            <tr>
                                <td width="50%">
                                    To Date <font class="clsNote">*</font></td>
                                <td align="left" valign="top">
                                    <asp:TextBox ID="txtToDT" runat="server" Width="50%" CssClass="clstextfield"></asp:TextBox>
                                    <%--<cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtToDT">
                                    </cc1:CalendarExtender>--%>
                                    </td>
                                </tr>
                            <tr>
                                <td></td>
                                <td align="right">
                                    <asp:Button ID="btnAddsites" runat="server" Width="150px" CssClass="button" Text="Submit">
                                    </asp:Button>&nbsp;<asp:Button ID="btnExport" Visible="false" runat="server" Width="150px"
                                        CssClass="button" Text="Export"></asp:Button>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" colspan="4" align="left">
                                    <cc1:ExportPanel ID="exp_CityWise" runat="server">
                                        <asp:Panel ID="panel1" runat="server" Width="100%">
                                            <asp:GridView ID="gvitems" runat="Server" AllowPaging="true" PageSize="10" AllowSorting="true"
                                                AutoGenerateColumns="false" EmptyDataText="No Records Found" Width="100%">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Agreement No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLAG_AGR_NO" runat="server" Text='<%#Eval("LAG_AGR_NO" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Lease Period">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLAG_LEASE_PERIOD" runat="server" Text='<%#Eval("LAG_LEASE_PERIOD" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Country">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCountry" runat="server" Text='<%#Eval("Country" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Region">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRegion" runat="server" Text='<%#Eval("Region" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="State">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblState" runat="server" Text='<%#Eval("State" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Centre">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCentre" runat="server" Text='<%#Eval("Centre" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Location" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="GSC">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblGSC" runat="server" Text='<%#Eval("GSC" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Premise">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPremise" runat="server" Text='<%#Eval("PRM_ADDRESS" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Lease Deed Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPremise" runat="server" Text='<%#Eval("LAG_LDEED_DT" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Commence Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPremise" runat="server" Text='<%#Eval("LAG_LCOMM_DT" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Expiry Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPremise" runat="server" Text='<%#Eval("LAG_CLOSING_DT" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Lease Lock Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPremise" runat="server" Text='<%#Eval("LAG_LOCK_DT" ) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </asp:Panel>
                                    </cc1:ExportPanel>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif") %>">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif") %>"
                            width="9" /></td>
                    <td background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>" width="25" /></td>
                    <td>
                        <img height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                            width="16" /></td>
                </tr>
            </table>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExport" />
    </Triggers>
</asp:UpdatePanel>
