﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ChangepasswordMaster : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        Session["LoginUser"] = "";
        if (!IsPostBack)
        {
            if (Session["uid"].ToString() == "")
                Response.Redirect(Application["FMGLogout"].ToString());
            else
            {
                string UID = Session["uid"].ToString();

                lblLogTime.Text = Session["Lastlogintime"].ToString();

                string aurid = Session["uid"].ToString();

                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 200);
                param[0].Value = aurid;
                DataSet ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "USP_GET_DETAILS", param); 
                lbluser.Text = ds.Tables[0].Rows[0]["Name"].ToString();

                if (ds.Tables[0].Rows[0]["EMP_IMG"].ToString() == "")
                    img.ImageUrl = "~/Userprofiles/default-user-icon-profile.jpg";
                else
                    img.ImageUrl = "~/userprofiles/" + ds.Tables[0].Rows[0]["EMP_IMG"];
                //SqlParameter[] para = new SqlParameter[1];
                param[0] = new SqlParameter("@usr_id", SqlDbType.NVarChar, 200);
                param[0].Value = aurid;
                ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "usp_getRoleNoforUser", param);
                string rolename = ds.Tables[0].Rows[0]["Name"].ToString();
                BindLogo();
                if (rolename == "1")
                {
                    logoimg.ToolTip = "Click here to change logo";
                    rolehf.Value = rolename;
                }
                Session["timeout"] = DateTime.Now.AddMinutes(Convert.ToInt32(ConfigurationManager.AppSettings["timeout"])).ToString();
                Session["LoginUser"] = lbluser.Text;
            }
        }
    }


    private void BindLogo()
    {
        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "Update_Get_LogoImage");
        sp3.Command.AddParameter("@type", "2", DbType.String);
        sp3.Command.AddParameter("@AUR_ID", Session["uid"], DbType.String);
        // sp3.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
        DataSet ds3 = sp3.GetDataSet();
        if (ds3.Tables[0].Rows.Count > 0)
            logoimg.ImageUrl = ds3.Tables[0].Rows[0]["ImagePath"].ToString();
        else
            logoimg.ImageUrl = "~/BootStrapCSS/images/yourlogo.png";
    }

    protected void lbtnLogOut_Click(object sender, System.EventArgs e)
    {
        string strQuery = "";
        TokenData td = TokenDataManager.GetTokenObject();
        if (td.IsAuthenticated)
        {
            Response.Redirect("~/logout");
        }
        else
        {
            strQuery = ("Update "
                        + (Session["TENANT"]+ ("." + ("[user] set Usr_logged=\'N\' WHERE USR_ID=\'"
                        + (Session["uid"] + "\'")))));
            SqlHelper.ExecuteScalar(CommandType.Text, strQuery);
            string Mode = "2";
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure((Session["TENANT"] + ("." + "UPDATE_LOGOUT_TIME")));
            sp.Command.AddParameter("@USR_ID", Session["uid"], DbType.String);
            sp.Command.AddParameter("@Mode", Mode, DbType.String);
            sp.Command.AddParameter("@LoginUniqueID", Session["LoginUniqueID"], DbType.String);
            sp.ExecuteScalar();
            Session.Abandon();
            Session["UID"] = "";
            Response.Redirect("~/login.aspx");
        }

    }
}
