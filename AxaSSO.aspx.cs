﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using OneLogin.AXASaml;
using System.Diagnostics;
using System.Net;

public partial class _AxaSSO : System.Web.UI.Page
{

    public static bool CreateLogFile(string message)
    {
        try
        {
            // string location = @"C://IRPC//myfile1.txt";
            string filename = "NewLog_AXA" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
            string location = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") + filename); // ''(System.Environment.CurrentDirectory + ("\log.txt"))
            if (!File.Exists(location))
            {
                FileStream fs;
                fs = new FileStream(location, FileMode.Append, FileAccess.Write, FileShare.ReadWrite);
                fs.Close();
            }

            Console.WriteLine(message);
            // Release the File that is created
            StreamWriter sw = new StreamWriter(location, true);
            sw.Write((message + Environment.NewLine));
            sw.Close();
            sw = null;
            return true;
        }
        catch (Exception ex)
        {
            EventLog.WriteEntry("MIDocShare", ("Error in CreateLogFile" + ex.Message.ToString()), EventLogEntryType.Error, 6000);
            return false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)

    {
       
        AXASettings accountSettings = new AXASettings();
        if (Request.Form["SAMLResponse"] == "" || Request.Form["SAMLResponse"] == null)
        {
           AuthRequest req = new AuthRequest(new AxaSettings2(), accountSettings);

            string st = accountSettings.idp_sso_target_url+ "?SAMLRequest=" + Server.UrlEncode(req.GetRequest(AuthRequest.AuthRequestFormat.Base64));
            Response.Redirect(st);
        }
        else
        {
            CreateLogFile("Saml Start");
            Response samlResponse = new Response(accountSettings);
            CreateLogFile(Request.Form["SAMLResponse"]);
            samlResponse.LoadXmlFromBase64(Request.Form["SAMLResponse"]); 
            string email_id = "";
            string emp_id = "";
            string filename = "";
            string filepath = "";
            string str1 = "~/LogAXA_kartin.aspx/?company=";
            string redirect_url = "";
            CreateLogFile(str1);
            if (samlResponse.IsValid())
            {
                CreateLogFile("Started AXA In IsValid");
                emp_id = samlResponse.GetNameID();
                email_id = samlResponse.GetNameID();
                CreateLogFile("emp_id:- " + emp_id + " email_id : - " + email_id);
                redirect_url = str1 + "Axa" + "&empid=" + emp_id + "&email=" + email_id;
                CreateLogFile("Redirect URL: " + redirect_url);
                //Server.TransferRequest(redirect_url);
                Response.Redirect(redirect_url);
            }
            else
            {
               
                Response.Write("Failed");
                CreateLogFile("Error in Isvalid");
            }
        }
    }
}