﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="ModifyFactSheetController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Modify Contract</h3>
            </div>
            <div class="card">
                <div data-ng-show="ViewUploption">
                    <form id="form1" name="griddetails">
                        <div class="clearfix">
                            <div class="col-md-12">
                                <div class="box">
                                    <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div data-ng-show="Viewstatus==1">
                    <form role="form" id="frm" name="frm" data-valid-submit="" novalidate>
                        <div class="row">
                            <div class="clearfix">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_DCT_CODE.$invalid}">
                                        <label>Contract Type <span style="color: red;">*</span></label>
                                        <select data-ng-model="FactSheet.DFM_DCT_CODE" id="ddlsmm" required="" class="form-control" name="DFM_DCT_CODE" data-ng-disabled="EnableStatus !=0 " data-live-search="true">
                                            <option value="" data-ng-selected="true">--Select--</option>
                                            <option data-ng-repeat="obj in Contracts" value="{{obj.DCT_CODE}}">{{obj.DCT_NAME}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_DST_CODE.$invalid}">
                                        <label>Spend Category <span style="color: red;">*</span></label>
                                        <select data-ng-model="FactSheet.DFM_DST_CODE" id="Select1" required="" class="form-control" name="DFM_DST_CODE" data-ng-disabled="EnableStatus !=0 " data-live-search="true">
                                            <option value="" data-ng-selected="true">--Select--</option>
                                            <option data-ng-repeat="obj in SpendCats" value="{{obj.DST_CODE}}">{{obj.DST_NAME}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_DBT_CODE.$invalid}">
                                        <label>Business Group <span style="color: red;">*</span></label>
                                        <select data-ng-model="FactSheet.DFM_DBT_CODE" id="Select2" required="" class="form-control" name="DFM_DBT_CODE" data-ng-disabled="EnableStatus !=0 " data-live-search="true">
                                            <option value="" data-ng-selected="true">--Select--</option>
                                            <option data-ng-repeat="obj in BusinessGrp" value="{{obj.DBT_CODE}}">{{obj.DBT_NAME}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_DSD_CODE.$invalid}">
                                        <label>Supplier Name <span style="color: red;">*</span></label>

                                        <select data-ng-model="FactSheet.DFM_DSD_CODE" id="Select3" required="" ng-change="GetParties(FactSheet.DFM_DSD_CODE)" data-ng-disabled="EnableStatus !=0 " class="form-control" name="DFM_DSD_CODE" data-live-search="true">
                                            <option value="" data-ng-selected="true">--Select--</option>
                                            <option data-ng-repeat="obj in Suppliers" value="{{obj.DSD_CODE}}">{{obj.DSD_NAME}}</option>
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_REF_NO.$invalid}">
                                    <label>Contract Reference No.<span style="color: red;">*</span></label>
                                    <input type="text" id="txtcode" name="DFM_REF_NO" class="form-control" ng-readonly="FactSheet.DFM_REF_NO" data-ng-model="FactSheet.DFM_REF_NO" required="" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.DFM_REF_NO.$invalid">Please enter Ref No </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <label>Parties: </label>
                                <p data-ng-show="FactSheet.DFM_DSD_CODE">
                                    <input type="text" id="txt1code" class="form-control" data-ng-model="Parties" ng-readonly="Parties" />
                                </p>
                            </div>
                        </div>


                        <div class="panel-group box box-primary" id="AgreeDates">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-ng-click="AgreeDates=!AgreeDates">
                                    <h4 class="panel-title"
                                        data-target="#collapseOne">Agreement Dates
                                        <i class="pull-right glyphicon" data-ng-class="{'glyphicon-chevron-down': AgreeDates, 'glyphicon-chevron-up': !AgreeDates}"></i>
                                    </h4>
                                </div>
                                <div id="Div2" class="panel-collapse collapse" data-ng-class="{'in': AgreeDates, '': !AgreeDates}">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFDT_SIGN_DATE.$invalid}">
                                                    <label>Signing Date <span style="color: red;">*</span></label>
                                                    <div class="input-group date" id='Signdate'>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar" onclick="setup('Signdate')"></i>
                                                        </div>
                                                        <input class="form-control" data-ng-model="FactSheetDate.DFDT_SIGN_DATE" name="DFDT_SIGN_DATE" required="" type="text" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFDT_EFRM_DATE.$invalid}">
                                                    <label>Effective Date <span style="color: red;">*</span></label>
                                                    <div class="input-group date" id='Effectdate'>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar" onclick="setup('Effectdate')"></i>
                                                        </div>
                                                        <input class="form-control" data-ng-model="FactSheetDate.DFDT_EFRM_DATE" name="DFDT_EFRM_DATE" ng-change="EffDatechange()" required="" type="text" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFDT_TRMN_DATE.$invalid}">
                                                    <label>Expiry Date <span style="color: red;">*</span></label>
                                                    <div class="input-group date" id='Termdate'>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar" onclick="setup('Termdate')"></i>
                                                        </div>
                                                        <input class="form-control" data-ng-model="FactSheetDate.DFDT_TRMN_DATE" name="DFDT_TRMN_DATE" ng-change="EffDatechange()" required="" type="text" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>Renewal Date </label>
                                                    <div class="input-group date" id='AutoDate'>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar" onclick="setup('AutoDate')"></i>
                                                        </div>
                                                        <input class="form-control" data-ng-model="FactSheetDate.DFDT_REN_DATE" name="DFDT_REN_DATE" type="text" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFDT_NOTF_PRD_WITH_CLAUSE.$invalid}">
                                                    <label>Notice period with  Termination </label>
                                                    <select data-ng-model="FactSheetDate.DFDT_NOTF_PRD_WITH_CLAUSE" id="Select4" class="form-control" name="DFDT_NOTF_PRD_WITH_CLAUSE" data-live-search="true">
                                                        <option value="" data-ng-selected="true">--Select--</option>
                                                        <option data-ng-repeat="obj in NoticePrd" value="{{obj.DNP_CODE}}">{{obj.DNP_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFDT_NOTF_PRD_WITH_CLAUSE.$invalid">Please select notice period with clause </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFDT_NOTF_PRD_WITHOUT_CLAUSE.$invalid}" data-ng-hide="Select5">
                                                    <label>Notice period without  Termination </label>
                                                    <select data-ng-model="FactSheetDate.DFDT_NOTF_PRD_WITHOUT_CLAUSE" id="Select5" class="form-control" name="DFDT_NOTF_PRD_WITHOUT_CLAUSE" data-live-search="true">
                                                        <option value="" data-ng-selected="true">--Select--</option>
                                                        <option data-ng-repeat="obj in NoticePrd" value="{{obj.DNP_CODE}}">{{obj.DNP_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFDT_NOTF_PRD_WITHOUT_CLAUSE.$invalid">Please select notice period without clause</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group box box-primary" id="PODates" data-ng-hide="PODates">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-ng-click="PODates=!PODates">
                                    <h4 class="panel-title"
                                        data-target="#collapseOne">PO Dates
                                        <i class="pull-right glyphicon" data-ng-class="{'glyphicon-chevron-down': PODates, 'glyphicon-chevron-up': !PODates}"></i>
                                    </h4>
                                </div>
                                <div id="Div9" class="panel-collapse collapse" data-ng-class="{'in': PODates, '': !PODates}">
                                    <div class="panel-body">
                                        <div class="clearfix">

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>PO Effective Date</label>
                                                    <div class="input-group date" id='POEffectdate'>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar" onclick="setup('POEffectdate')"></i>
                                                        </div>
                                                        <input class="form-control" data-ng-model="FactSheetPODate.DFDT_PO_EFRM_DATE" name="DFDT_PO_EFRM_DATE" ng-change="POEffDatechange()" required="" type="text" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>PO Expiry Date</label>
                                                    <div class="input-group date" id='POTermdate'>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar" onclick="setup('POTermdate')"></i>
                                                        </div>
                                                        <input class="form-control" data-ng-model="FactSheetPODate.DFDT_PO_TRMN_DATE" name="DFDT_PO_TRMN_DATE" ng-change="POEffDatechange()" required="" type="text" data-inputmask="'alias': 'mm/dd/yyyy'" data-mask />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label>PO Auto Renewal Date </label>
                                                    <div class="input-group date" id='POAutoDate'>
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar" onclick="setup('POAutoDate')"></i>
                                                        </div>
                                                        <input class="form-control" data-ng-model="FactSheetPODate.DFDT_PO_REN_DATE" name="DFDT_PO_REN_DATE" type="text" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFDT_NOTF_PRD_WITH_CLAUSE.$invalid}">
                                                    <label>PO Notice period with Termination</label>
                                                    <select data-ng-model="FactSheetPODate.DFDT_PO_NOTF_PRD_WITH_CLAUSE" id="Select6" class="form-control" name="DFDT_PO_NOTF_PRD_WITH_CLAUSE" data-live-search="true">
                                                        <option value="" data-ng-selected="true">--Select--</option>
                                                        <option data-ng-repeat="obj in NoticePOPrd" value="{{obj.DNP_CODE}}">{{obj.DNP_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFDT_PO_NOTF_PRD_WITH_CLAUSE.$invalid">Please select PO notice period with clause </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFDT_NOTF_PRD_WITHOUT_CLAUSE.$invalid}">
                                                    <label>PO Notice period without Termination</label>
                                                    <select data-ng-model="FactSheetPODate.DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE" id="Select7" class="form-control" name="DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE" data-live-search="true">
                                                        <option value="" data-ng-selected="true">--Select--</option>
                                                        <option data-ng-repeat="obj in NoticePOPrd" value="{{obj.DNP_CODE}}">{{obj.DNP_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFDT_PO_NOTF_PRD_WITHOUT_CLAUSE.$invalid">Please select PO notice period without clause</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="panel-group box box-primary" id="Remainder">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-ng-click="Remainder=!Remainder">
                                    <h4 class="panel-title"
                                        data-target="#collapseOne">Reminders
                                        <i class="pull-right glyphicon" data-ng-class="{'glyphicon-chevron-down': Remainder, 'glyphicon-chevron-up': !Remainder}"></i>
                                    </h4>
                                </div>
                                <div id="Div12" class="panel-collapse collapse" data-ng-class="{'in': Remainder, '': !Remainder}">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <label>Renewal Reminder Date <span style="color: red;">*</span></label>
                                            <div class="form-group">
                                                <div data-ng-repeat="obj in RemainderDates" class="col-md-3 col-sm-6 col-xs-12">
                                                    <label>
                                                        <input type="radio" data-ng-model="RemainderDates.DRD_SNO" data-ng-value="{{obj.DRD_SNO}}" name="RenDate">{{obj.DRD_NAME}}</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <label>Repeat Notification<span style="color: red;">*</span></label>
                                            <div class="form-group">
                                                <div data-ng-repeat="obj in RepeatNoft" class="col-md-3 col-sm-6 col-xs-12">
                                                    <label>
                                                        <input type="radio" data-ng-model="RepeatNoft.DRN_SNO" data-ng-value="{{obj.DRN_SNO}}" name="RepNotf">{{obj.DRN_NAME}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel-group box box-primary" id="PORemainder" data-ng-hide="PORemainder">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-ng-click="PORemainder=!PORemainder">
                                    <h4 class="panel-title"
                                        data-target="#collapseOne">PO Reminders
                                        <i class="pull-right glyphicon" data-ng-class="{'glyphicon-chevron-down': PORemainder, 'glyphicon-chevron-up': !PORemainder}"></i>
                                    </h4>
                                </div>
                                <div id="Div13" class="panel-collapse collapse" data-ng-class="{'in': PORemainder, '': !PORemainder}">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <label>PO Renewal Reminder Date </label>
                                            <div class="form-group">
                                                <div data-ng-repeat="obj in RemainderPODates" class="col-md-3 col-sm-6 col-xs-12">
                                                    <label>
                                                        <input type="radio" data-ng-model="RemainderPODates.DRD_SNO" data-ng-value="{{obj.DRD_SNO}}" name="PORenDate">{{obj.DRD_NAME}}</label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <label>PO Repeat Notification </label>
                                            <div class="form-group">
                                                <div data-ng-repeat="obj in RepeatPONoft" class="col-md-3 col-sm-6 col-xs-12">
                                                    <label>
                                                        <input type="radio" data-ng-model="RepeatPONoft.DRN_SNO" data-ng-value="{{obj.DRN_SNO}}" name="PORepNotf">{{obj.DRN_NAME}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="panel-group box box-primary" id="Values">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-ng-click="Values=!Values">
                                    <h4 class="panel-title"
                                        data-target="#collapseOne">Values
                                        <i class="pull-right glyphicon" data-ng-class="{'glyphicon-chevron-down': Values, 'glyphicon-chevron-up': !Values}"></i>
                                    </h4>
                                </div>
                                <div id="Div4" class="panel-collapse collapse" data-ng-class="{'in': Values, '': !Values}">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFV_CNRT_VAL.$invalid}">
                                                    <label>Total Contract Value<span style="color: red;">*</span></label>
                                                    <input type="number" id="DFV_CNRT_VAL" name="DFV_CNRT_VAL" class="form-control" data-ng-pattern="/^[0-9]/" data-ng-model="FactSheetValues.DFV_CNRT_VAL" required="" />
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFV_CNRT_VAL.$invalid">Please enter numerics only </span>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFV_ANNL_VAL.$invalid}">
                                                    <label>Annual Value</label>
                                                    <input type="number" id="DFV_ANNL_VAL" name="DFV_ANNL_VAL" class="form-control" data-ng-pattern="/^[0-9]*$/" data-ng-model="FactSheetValues.DFV_ANNL_VAL" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFV_CNRT_SCP.$invalid}">
                                                    <label>Scope Of Work<span style="color: red;">*</span></label>
                                                    <textarea class="form-control" id="DFV_CNRT_SCP" name="DFV_CNRT_SCP" data-ng-model="FactSheetValues.DFV_CNRT_SCP" required=""> </textarea>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFV_CNRT_SCP.$invalid">Please enter valid text. </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group box box-primary" id="Stakehol">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-ng-click="Stakehol=!Stakehol">
                                    <h4 class="panel-title"
                                        data-target="#collapseOne">Stakeholders Values
                                        <i class="pull-right glyphicon" data-ng-class="{'glyphicon-chevron-down': Stakehol, 'glyphicon-chevron-up': !Stakehol}"></i>
                                    </h4>
                                </div>
                                <div id="Div6" class="panel-collapse collapse" data-ng-class="{'in': Stakehol, '': !Stakehol}">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFKD_NAME.$invalid}">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <label>Key Business Stakeholders<span style="color: red;">*</span></label>
                                                    </div>
                                                    <div class="clearfix" data-ng-repeat="obj in KBPDetails" data-ng-class="{'has-error': obj.IS_ERROR}">
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                        </div>
                                                        <div class="col-md-4 col-sm-6 col-xs-6">
                                                            <input type="text" id="DFKD_NAME" name="DFKD_NAME" data-ng-pattern="namepattern" class="form-control" data-ng-change="obj.IS_ERROR = false" data-ng-model="obj.DFKD_NAME" required="" />
                                                        </div>
                                                        <div class="col-md-1 col-sm-6 col-xs-6">
                                                            <a href="">
                                                                <i class="glyphicon glyphicon-plus" data-ng-click="AddNew()"></i>
                                                            </a>
                                                        </div>
                                                        <div class="col-md-1 col-sm-6 col-xs-6" data-ng-show="KBPDetails.length > 1">
                                                            <i class="glyphicon glyphicon-minus" data-ng-click="Delete(obj)"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFD_CT.$invalid}">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <label>Commercial Terms<span style="color: red;">*</span></label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <!--<input type="text" id="DFD_CT" name="DFD_CT" data-ng-pattern="namepattern" class="form-control" data-ng-model="FactSheetDetails.DFD_CT" required="" />-->
                                                        <textarea class="form-control" id="DFD_CT" name="DFD_CT" data-ng-model="FactSheetDetails.DFD_CT" required=""> </textarea>
                                                        <span class="error" data-ng-show="frm.$submitted && frm.DFD_CT.$invalid">Please enter valid text. </span>
                                                    </div>
                                                    </br>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFD_AC.$invalid}">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <label>Ancillary Service<span style="color: red;">*</span></label>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <!--<input type="text" id="DFD_AC" name="DFD_AC" data-ng-pattern="namepattern" class="form-control" data-ng-model="FactSheetDetails.DFD_AC" required="" />-->
                                                        <textarea class="form-control" id="DFD_AC" name="DFD_AC" data-ng-model="FactSheetDetails.DFD_AC" required=""> </textarea>
                                                        <span class="error" data-ng-show="frm.$submitted && frm.DFD_AC.$invalid">Please enter valid text. </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-group box box-primary" id="TermsClause">
                            <div class="panel panel-default">
                                <div class="panel-heading" data-ng-click="TermsClause=!TermsClause">
                                    <h4 class="panel-title"
                                        data-target="#collapseOne">Terms & Clauses
                                        <i class="pull-right glyphicon" data-ng-class="{'glyphicon-chevron-down': TermsClause, 'glyphicon-chevron-up': !TermsClause}"></i>
                                    </h4>
                                </div>
                                <div id="Div8" class="panel-collapse collapse" data-ng-class="{'in': TermsClause, '': !TermsClause}">
                                    <div class="panel-body">
                                        <div class="clearfix">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFD_PT.$invalid}">
                                                    <label>Payment Terms<span style="color: red;">*</span></label>
                                                    <textarea class="form-control" id="DFD_PT" name="DFD_PT" data-ng-model="FactSheetDetails.DFD_PT" required=""> </textarea>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFD_PT.$invalid">Please enter valid text. </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFD_TC.$invalid}">
                                                    <label>Termination Clauses<span style="color: red;">*</span></label>
                                                    <textarea class="form-control" id="DFD_TC" name="DFD_TC" data-ng-model="FactSheetDetails.DFD_TC" required=""> </textarea>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFD_TC.$invalid">Please enter valid text </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFD_DW.$invalid}">
                                                    <label>Defects,Warranty<span style="color: red;">*</span></label>
                                                    <textarea class="form-control" id="DFD_DW" name="DFD_DW" data-ng-model="FactSheetDetails.DFD_DW" required=""> </textarea>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFD_DW.$invalid">Please enter valid text. </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFD_IPR.$invalid}">
                                                    <label>Intellectual Property Rights<span style="color: red;">*</span></label>
                                                    <textarea class="form-control" id="DFD_IPR" name="DFD_IPR" data-ng-model="FactSheetDetails.DFD_IPR" required=""> </textarea>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFD_IPR.$invalid">Please enter valid text. </span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clearfix">
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFD_CONFD.$invalid}">
                                                    <label>Confidentiality<span style="color: red;">*</span></label>
                                                    <textarea class="form-control" id="DFD_CONFD" name="DFD_CONFD" data-ng-model="FactSheetDetails.DFD_CONFD" required=""> </textarea>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFD_CONFD.$invalid">Please enter valid text. </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12 col-xs-12">
                                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFD_CD.$invalid}">
                                                    <label>Jurisdiction Clause<span style="color: red;">*</span></label>
                                                    <textarea class="form-control" id="DFD_CD" name="DFD_CD" data-ng-model="FactSheetDetails.DFD_CD" required=""> </textarea>
                                                    <span class="error" data-ng-show="frm.$submitted && frm.DFD_CD.$invalid">Please enter valid text. </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form id="frmPRMUpl" name="frmPRMUpl" method="post" action="api/FactSheet/UploadTemplateIE" enctype="multipart/form-data">
                        <div class="box-body">
                            <div class="clearfix">
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmPRMUpl.$submitted && frmPRMUpl.DFDC_DDT_CODE.$invalid}">
                                        <label>Type Of Document <span style="color: red;">*</span></label>
                                        <select data-ng-model="document.DFDC_DDT_CODE" id="DFDC_DDT_CODE" class="form-control" required="" name="DFDC_DDT_CODE" data-live-search="true">
                                            <option value="" data-ng-selected="true">--Select--</option>
                                            <option data-ng-repeat="obj in TypeOfDoc" value="{{obj.DDT_CODE}}">{{obj.DDT_NAME}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': frmPRMUpl.$submitted && frmPRMUpl.UPLFILE.$invalid}">
                                        <label>Upload <span style="color: red;">*</span></label>
                                        <input type="file" name="UPLFILE" id="FileUpl" required="" />
                                        <input type="hidden" value="{{Login}}" name="Login" />
                                        <input type="hidden" value="{{document.DFDC_DDT_CODE}}" name="DocType" />
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <input type="button" id="btnUpload" data-ng-click="UploadFile()" class="btn btn-primary custom-button-color" value="Upload" />
                                </div>
                            </div>

                            <div class="clearfix" ng-show="DocumentDetails.length!=0">
                                <div class="col-md-12">
                                    <div class="box">
                                        <div data-ag-grid="gridOptions1" style="height: 250px; width: 100%;" class="ag-blue"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary" data-ng-click="UploadOutsideSubmit()">Modify</button>
                        <button id="backbtn" class="btn btn-primary" data-ng-click="back()">Back</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        $(function () {
            //Datemask2 MM/dd/yyyy
            $("#datemask2").inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" });
            //Money Euro
            $("[data-mask]").inputmask();
        });

    </script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../JS/ModifyFactSheet.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/FactSheet.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>

</body>
</html>
