﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
  <%--  <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    <%----%>

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->


    <style>
       /* .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
         #pageRowSummaryPanel {
            display:none !important;

        }*/
    </style>
   <%-- <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>

</head>
<body data-ng-controller="AgreementNearExpiryReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Agreement Near Expiry Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Agreement Near Expiry Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">--%>
                             <div class="widgets">
                              <h3>Agreement Near Expiry Report</h3>
                               </div>
                                  <div class="card">
                            <form role="form" id="form1" name="frmAgreementNearExpiryReport" data-valid-submit="GetAgreementNearExpiryReport()" novalidate>
                                  <div class="clearfix">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmAgreementNearExpiryReport.$submitted && frmAgreementNearExpiryReport.FROM_DATE.$invalid}">
                                            <label>From Date<span style="color: red;">*</span></label>                                        
                                            <div class="input-group date" style="width: 150px" id='FromDate'>
                                                    <input type="text" id="Text1" class="form-control" required="" placeholder="mm/dd/yyyy" name="FROM_DATE" data-ng-model="AgreementNearExpiry.FROM_DATE" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('FromDate')"></span>
                                                    </span>
                                                </div>
                                                <span class="error" data-ng-show="frmAgreementNearExpiryReport.$submitted && frmAgreementNearExpiryReport.FROM_DATE.$invalid" style="color: red">Please select from date </span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmAgreementNearExpiryReport.$submitted && frmAgreementNearExpiryReport.TO_DATE.$invalid}">
                                            <label for="Text1">To Date<span style="color: red;">*</span></label>                                         
                                            <div class="input-group date" style="width: 150px" id='ToDate'>
                                                    <input type="text" id="Text3" class="form-control" required="" placeholder="mm/dd/yyyy" name="TO_DATE" data-ng-model="AgreementNearExpiry.TO_DATE" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('ToDate')"></span>
                                                    </span>
                                                </div>
                          
                                       <span class="error" data-ng-show="frmAgreementNearExpiryReport.$submitted && frmAgreementNearExpiryReport.TO_DATE.$invalid" style="color: red">Please select to date </span>
                                            </div>
                                        </div> 

                                    </div>
                                
                                 <div class="row form-inline">
                                    <div class="col-md-12 text-right" style="padding-top: 17px">
                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color'/>                                         
                                    </div>
                                </div>
                                <br />
                                  <a data-ng-click="GenReport(SpaceAllocation,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <div class="row" style="padding-left: 30px;">
                                    <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%;" />
                                    <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                <%--</div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../JS/AgreementNearExpiryReport.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
</body>
</html>
