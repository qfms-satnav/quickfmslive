﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/ContractManagement/Masters/SupplierMaster.cs" Inherits="ContractManagement_Masters_SupplierMaster" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    <%----%>

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->


    <%--<style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>

</head>
<body data-ng-controller="SupplierMasterController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Supplier Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Supplier Master</h3>
            </div>
            <div class="card">
            <%-- <div class="panel-body" style="padding-right: 50px;">--%>
            <form role="form" id="frmSupMas" name="frmSupMas" data-valid-submit="Save()" novalidate>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="form-group" data-ng-class="{'has-error': frmSupMas.$submitted && frmSupMas.DSD_CODE.$invalid}">
                            <label for="txtcode" class="col-md-12 control-label">Supplier Code<span style="color: red;">*</span></label>
                            <div class="col-md-12">
                                <input type="text" name="DSD_CODE" class="form-control" data-ng-pattern="codepattern"
                                    data-ng-model="Supplier.DSD_CODE" autofocus required="" />
                                <span class="error" data-ng-show="frmSupMas.$submitted && frmSupMas.DSD_CODE.$invalid">Please enter valid code </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="form-group" data-ng-class="{'has-error': frmSupMas.$submitted && frmSupMas.DSD_NAME.$invalid}">
                            <label for="Text1" class="col-md-12 control-label">Supplier Name<span style="color: red;">*</span></label>
                            <div class="col-md-12">
                                <input type="text" name="DSD_NAME" class="form-control" data-ng-pattern="namepattern"
                                    data-ng-model="Supplier.DSD_NAME" required="" />
                                <span class="error" data-ng-show="frmSupMas.$submitted && frmSupMas.DSD_NAME.$invalid">Please enter valid name </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frmSupMas.$submitted && frmSupMas.DSD_PHONE.$invalid}">
                            <label for="Text1" class="col-md-12 control-label">Contact No</label>
                            <div class="col-md-12">
                                <input type="text" id="Text1" name="DSD_PHONE" class="form-control" data-ng-pattern="/^[0-9]*$/" maxlength="10"
                                    data-ng-minlength="10" data-ng-maxlength="10" data-ng-model="Supplier.DSD_PHONE" />
                                <span class="error" data-ng-show="frmSupMas.$submitted && frmSupMas.DSD_PHONE.$invalid">Please enter valid phone no </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frmSupMas.$submitted && frmSupMas.DSD_EMAIL.$invalid}">
                            <label for="txtcode" class="col-md-12 control-label">Email Id</label>
                            <div class="col-md-12">
                                <input type="text" id="Text3" name="DSD_EMAIL" class="form-control"
                                    data-ng-pattern="email"
                                    data-ng-model="Supplier.DSD_EMAIL" />
                                <span class="error" data-ng-show="frmSupMas.$submitted && frmSupMas.DSD_EMAIL.$invalid">Please enter valid email id </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="form-group" data-ng-class="{'has-error': frmSupMas.$submitted && frmSupMas.DSD_STA_ID.$invalid}">
                            <label for="txtcode" class="col-md-12 control-label">Status<span style="color: red;">*</span></label>
                            <div class="col-md-12">
                                <select data-ng-model="Supplier.DSD_STA_ID" name="DSD_STA_ID" class="form-control"
                                    data-live-serach="true" required="">
                                    <option value="" selected>--Select--</option>
                                    <option data-ng-repeat="obj in StaDet" value="{{obj.Id}}">{{obj.Name}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="form-group" data-ng-class="{'error': frmSupMas.$submitted && frmSupMas.DSD_ADDR.$invalid}">
                            <label for="txtcode" class="col-md-12 control-label">Address</label>
                            <div class="col-md-12">
                                <textarea class="form-control" style="height: 60px" name="DSD_ADDR"
                                    data-ng-model="Supplier.DSD_ADDR"></textarea>
                                <span class="error" data-ng-show="frmSupMas.$submitted && frmSupMas.DSD_ADDR.$invalid">Please enter valid text </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="form-group" data-ng-class="{'error': frmSupMas.$submitted && frmSupMas.DSD_REM.$invalid}">
                            <label for="txtcode" class="col-md-12 control-label">Remarks</label>
                            <div class="col-md-12">
                                <textarea class="form-control" style="height: 60px" name="DSD_REM" data-ng-pattern="codepattern"
                                    data-ng-model="Supplier.DSD_REM"></textarea>
                                <span class="error" data-ng-show="frmSupMas.$submitted && frmSupMas.DSD_REM.$invalid">Please enter valid text </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row form-inline">
                    <div class="col-md-12 text-right" style="padding-top: 17px">
                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                        <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                        <input type="button" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                        <a class='btn btn-primary custom-button-color' href="../../ContractManagement/Masters/MasterScreens.aspx">Back</a>
                    </div>
                    <div class="row" style="padding-left: 30px;">
                        <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                    </div>
                </div>
                <div data-ag-grid="gridOptions" style="height: 250px; width: 1100px;" class="ag-blue"></div>
            </form>
        </div>
    </div>
     </div>
          <%--  </div>
        </div>--%>
    <%--</div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <%--<script src="../JS/SupplierMaster.min.js" defer></script>--%>
    <script src="../JS/SupplierMaster.js"></script>
    <%--<script src="../../SMViews/Utility.js"></script>--%>
    <script src="../../SMViews/Utility.min.js" defer></script>
</body>
</html>
