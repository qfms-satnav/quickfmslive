﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->


    <%-- <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>--%>
    <%--<link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>

</head>
<body data-ng-controller="ContractReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Contract Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Contract Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">--%>
            <div class="widgets">
                <h3>Contract Report </h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="frmConReport" data-valid-submit="GetData()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_DCT_CODE.$invalid}">
                                <label>Contract Type <span style="color: red;">*</span></label>
                                <select data-ng-model="ContractReport.DFM_DCT_CODE" id="ddlsmm" required="" class="form-control" name="DFM_DCT_CODE" data-live-search="true">
                                    <option value="" data-ng-selected="true">--Select--</option>
                                    <option data-ng-repeat="obj in Contracts" value="{{obj.DCT_CODE}}">{{obj.DCT_NAME}}</option>
                                </select>
                                <span class="error" data-ng-show="frmConReport.$submitted && frmConReport.DFM_DCT_CODE.$invalid" style="color: red">Please select contract type</span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_DST_CODE.$invalid}">
                                <label>Spend Category<span style="color: red;">*</span></label>
                                <select data-ng-model="ContractReport.DFM_DST_CODE" id="Select1" required="" class="form-control" name="DFM_DST_CODE" data-live-search="true">
                                    <option value="" data-ng-selected="true">--Select--</option>
                                    <option value="ALL">All</option>
                                    <option data-ng-repeat="obj in SpendCats" value="{{obj.DST_CODE}}">{{obj.DST_NAME}}</option>
                                </select>
                                <span class="error" data-ng-show="frmConReport.$submitted && frmConReport.DFM_DST_CODE.$invalid" style="color: red">Please select spend category</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_DBT_CODE.$invalid}">
                                <label>Business Group <span style="color: red;">*</span></label>
                                <select data-ng-model="ContractReport.DFM_DBT_CODE" id="Select2" required="" class="form-control" name="DFM_DBT_CODE" data-live-search="true">
                                    <option value="" data-ng-selected="true">--Select--</option>
                                    <option value="ALL">All</option>
                                    <option data-ng-repeat="obj in BusinessGrp" value="{{obj.DBT_CODE}}">{{obj.DBT_NAME}}</option>
                                </select>
                                <span class="error" data-ng-show="frmConReport.$submitted && frmConReport.DFM_DBT_CODE.$invalid" style="color: red">Please select business group</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.DFM_DSD_CODE.$invalid}">
                                <label>Supplier Name <span style="color: red;">*</span></label>
                                <select data-ng-model="ContractReport.DFM_DSD_CODE" id="Select3" required="" data-ng-change="GetParties(ContractReport.DFM_DSD_CODE)" class="form-control" name="DFM_DSD_CODE" data-live-search="true">
                                    <option value="" data-ng-selected="true">--Select--</option>
                                    <option value="ALL">All</option>
                                    <option data-ng-repeat="obj in Suppliers" value="{{obj.DSD_CODE}}">{{obj.DSD_NAME}}</option>
                                </select>
                                <span class="error" data-ng-show="frmConReport.$submitted && frmConReport.DFM_DSD_CODE.$invalid" style="color: red">Please select supplier name</span>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix">
                        <%--<div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': frmConReport.$submitted && frmConReport.DCT_CODE.$invalid}">
                                            <label>Effective/Expiry<span style="color: red;">*</span></label>
                                            <div class="input-group date" style="width: 150px" id='EffectiveDate'>
                                                <input type="text" id="EFFECTIVE_DATE" class="form-control" required="" placeholder="mm/dd/yyyy" name="EFFECTIVE_DATE" data-ng-model="ContractReport.EFFECTIVE_DATE" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('EffectiveDate')"></span>
                                                </span>
                                            </div>
                                            <span class="error" data-ng-show="frmConReport.$submitted && frmConReport.EFFECTIVE_DATE.$invalid" style="color: red">Please Select Effective Date</span>

                                        </div>
                                    </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmConReport.$submitted && frmConReport.FROM_DATE.$invalid}">
                                <label>From Date<span style="color: red;">*</span></label>

                                <div class="input-group date" id='FromDate'>
                                    <input type="text" id="Text1" class="form-control" required="" placeholder="mm/dd/yyyy" name="FROM_DATE" data-ng-model="ContractReport.FROM_DATE" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('FromDate')"></span>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="frmConReport.$submitted && frmConReport.FROM_DATE.$invalid" style="color: red">Please select from date</span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmConReport.$submitted && frmConReport.TO_DATE.$invalid}">
                                <label>To Date<span style="color: red;">*</span></label>

                                <div class="input-group date" id='ToDate'>
                                    <input type="text" id="Text2" class="form-control" required="" placeholder="mm/dd/yyyy" name="TO_DATE" data-ng-model="ContractReport.TO_DATE" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('ToDate')"></span>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="frmConReport.$submitted && frmConReport.TO_DATE.$invalid" style="color: red">Please select to date</span>

                            </div>
                        </div>

                    </div>
                    <div class="row form-inline">
                        <div class="col-md-12 text-right" style="padding-top: 17px">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' />
                        </div>
                    </div>
                    <br />
                    <a data-ng-click="GenReport(SpaceAllocation,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../JS/ContractReport.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
</body>
</html>
