﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    <%----%>

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->


    <style>
        /* .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }*/
    </style>
    <%--<link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>

</head>
<body data-ng-controller="AgreementReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Agreement Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Agreement Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">--%>
            <div class="widgets">
                <h3>Agreement Report </h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="frmAgreementReportAgreementReport" data-valid-submit="GetAgreementReportData()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAgreementReport.$submitted && frmAgreementReport.DMC_CODE.$invalid}">
                                <label>Main Category<span style="color: red;">*</span></label>
                                <select data-ng-model="AgreementReport.DMC_CODE" id="ddlsmm" required="" class="form-control" name="DMC_CODE" data-live-search="true"
                                    data-ng-change="CatChanged(AgreementReport.DMC_CODE, AgreementReport)">
                                    <option value="" data-ng-selected="true">--Select--</option>
                                    <option data-ng-repeat="mc in MainCategory" value="{{mc.DMC_CODE}}">{{mc.DMC_NAME}}</option>
                                </select>
                                <span class="error" data-ng-show="frmAgreementReportAgreementReport.$submitted && frmAgreementReportAgreementReport.DMC_CODE.$invalid" style="color: red">Please select main category </span>
                            </div>
                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmAgreementReport.$submitted && frmAgreementReport.DBT_CODE.$invalid}">
                                                <label>Subcategory <span style="color: red;">*</span></label>
                                                <select data-ng-model="AgreementReport.DBT_CODE" id="Select1" required="" class="form-control" name="DBT_CODE" data-live-search="true">
                                                    <option value="" data-ng-selected="true">--Select--</option>
                                                    <option value="ALL">All</option>
                                                    <option data-ng-repeat="obj in  SubCategories" value="{{obj.DBT_CODE}}">{{obj.DBT_NAME}}</option>
                                                </select>
                                                 <span class="error" data-ng-show="frmAgreementReportAgreementReport.$submitted && frmAgreementReportAgreementReport.DBT_CODE.$invalid" style="color: red">Please select subcategory </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmAgreementReport.$submitted && frmAgreementReport.DRS_CODE.$invalid}">
                                                <label>Status <span style="color: red;">*</span></label>
                                                <select data-ng-model="AgreementReport.DRS_CODE" id="Select2" required="" class="form-control" name="DRS_CODE" data-live-search="true">
                                                    <option value="" data-ng-selected="true">--Select--</option>
                                                    <option data-ng-repeat="st in Status" value="{{st.DRS_CODE}}">{{st.DRS_NAME}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frmAgreementReportAgreementReport.$submitted && frmAgreementReportAgreementReport.DRS_CODE.$invalid" style="color: red">Please select status </span>
                                            </div>
                                        </div>
                                        

                    </div>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmAgreementReport.$submitted && frmAgreementReport.FROM_DATE.$invalid}">
                                                <label>From Date <span style="color: red;">*</span></label>                                               
                                               <div class="input-group date" id='FromDate'>
                                                    <input type="text" id="FROM_DATE" class="form-control" required="" placeholder="mm/dd/yyyy" name="FROM_DATE" data-ng-model="AgreementReport.FROM_DATE" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('FromDate')"></span>
                                                    </span>
                                                  
                                                </div>
                                                 <span class="error" data-ng-show="frmAgreementReportAgreementReport.$submitted && frmAgreementReportAgreementReport.FROM_DATE.$invalid" style="color: red">Please select from date </span>
                                            </div>
                                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAgreementReportAgreementReport.$submitted && frmAgreementReportAgreementReport.TO_DATE.$invalid}">
                                <label>To Date<span style="color: red;">*</span></label>
                                <div class="input-group date"  id='ToDate'>
                                    <input type="text" id="TO_DATE" class="form-control" required="" placeholder="mm/dd/yyyy" name="TO_DATE" data-ng-model="AgreementReport.TO_DATE" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('ToDate')"></span>
                                    </span>

                                </div>
                                <span class="error" data-ng-show="frmAgreementReportAgreementReport.$submitted && frmAgreementReportAgreementReport.TO_DATE.$invalid" style="color: red">Please select to date </span>
                            </div>
                        </div>
                        <div class="row form-inline">
                        <div class="col-md-12 text-right" style="padding-top: 17px">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' />
                        </div>
                    </div>

                    </div>
                    
                    <br />
                    <a data-ng-click="GenReport(SpaceAllocation,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <%--  </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../JS/AgreementReport.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
</body>
</html>
