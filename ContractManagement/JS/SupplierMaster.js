﻿app.service("SupplierMasterService", ['$q', '$http', function ($q, $http) {
    this.BindGrid = function () {
        deferred = $q.defer();
        return $http.get('../../api/SupplierMaster/BindSupplierData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Insertdata = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/SupplierMaster/Insert', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }

    this.Updatedata = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/SupplierMaster/Update', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }
}]);

app.controller("SupplierMasterController", ['$http', '$q', '$scope', 'SupplierMasterService', function ($http, $q, $scope, SupplierMasterService) {
    $scope.ActionStatus = 0;
    $scope.DSD_STA_ID = {};
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    $scope.codepattern = /^[a-zA-Z0-9. ,/"';]*$/;
    $scope.namepattern = /^[a-zA-Z0-9-_ /():. ]*$/;
    //for GridView 
    var columnDefs = [
       { headerName: "Supplier Code", field: "DSD_CODE", width: 150, cellClass: 'grid-align' },
       { headerName: "Supplier Name", field: "DSD_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Status", template: "{{ShowStatus(data.DDT_STA_ID)}}", width: 100, cellClass: 'grid-align' },
       { headerName: "Edit", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        //enableColResize: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    $scope.Loaddata = function () {
        SupplierMasterService.BindGrid().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
        });
    }
    $scope.Loaddata();

    $scope.EditFunction = function (data) {
        $scope.Supplier = {};
        angular.copy(data, $scope.Supplier);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
    }

    $scope.ClearData = function () {
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.Supplier = {};
        $scope.frmSupMas.$submitted = false;
    }

    $scope.Save = function () {
        if ($scope.IsInEdit) {
            SupplierMasterService.Updatedata($scope.Supplier).then(function () {
                var updatedobj = {};
                angular.copy($scope.Supplier, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully uploaded";
                SupplierMasterService.BindGrid().then(function (data) {
                    $scope.gridata = data;
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                });
                $scope.IsInEdit = false;
                $scope.ClearData();
                showNotification('success', 8, 'bottom-right', $scope.Success);
            }, function (error) {
            })
        }
        else {
            SupplierMasterService.Insertdata($scope.Supplier).then(function (data) {
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully inserted";
                var savedobj = {};
                angular.copy($scope.Supplier, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.Supplier = {};
                $scope.ClearData();
            }
            , function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
            });
        }
    }

}]);