﻿app.service("ViewFactSheetService", function ($http, $q) {

    this.DRMExcelDownload = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/ModifyFactSheet/DRMExcelDownload', dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});

app.controller('ViewFactSheetController', function ($scope, ViewFactSheetService, UtilityService, FactSheetService, ModifyFactSheetService, $filter) {


    $scope.Viewstatus = 0;
    $scope.ViewUploption = true;
    $scope.PODates = true;
    $scope.PORemainder = true;
    $scope.AgreeDates = true;
    $scope.UploadDoc = true;
    $scope.TermsClause = true;
    $scope.Stakehol = true;
    $scope.Values = true;
    $scope.Remainder = true;
    $scope.FactSheet = {};
    $scope.FactSheetDate = {};
    $scope.FactSheetRenewal = {};
    $scope.FactSheetValues = {};
    $scope.FactSheetDetails = {};
    $scope.document = {};
    $scope.DocumentDetails = [];
    $scope.Select5 = true;
    $scope.PODates = true;
    $scope.PORemainder = true;
    $scope.KBPDetails = [
     {
         'KPH_NAME': '',
         'IS_ERROR': false,
     }
    ]

    $scope.AddNew = function () {
        $scope.KBPDetails.push({
            'KPH_NAME': '',
            'IS_ERROR': false,
        });
    }

    $scope.AddNew = function () {
        $scope.KBPDetails.push({
            'KPH_NAME': '',
            'IS_ERROR': false,
        });
    }

    $scope.Delete = function (item) {
        $scope.KBPDetails = _.without($scope.KBPDetails, item);
    }


    //for GridView 
    var columnDefs = [
       { headerName: "Request Id", field: "DFM_REQ_ID", width: 200, cellClass: 'grid-align', template: '<a ng-click="GetDetailsOnSelection(data)">{{data.DFM_REQ_ID}}</a>' },
       { headerName: "Contract Name", field: "DCT_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Supplier Name", field: "DST_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Spend Category", field: "DSD_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Business Group", field: "DBT_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Reference No", field: "DFM_REF_NO", width: 180, cellClass: 'grid-align' },
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        //enableColResize: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.Loaddata = function () {
        FactSheetService.Contract().then(function (data) {
            $scope.Contracts = data;
            FactSheetService.Spend().then(function (data1) {
                $scope.SpendCats = data1;
                FactSheetService.Business().then(function (data2) {
                    $scope.BusinessGrp = data2;
                    FactSheetService.Supplier().then(function (data3) {
                        $scope.Suppliers = data3;
                        FactSheetService.Remainder().then(function (data4) {
                            $scope.RemainderDates = data4;
                            FactSheetService.PORemainder().then(function (data9) {
                                $scope.RemainderPODates = data9
                                FactSheetService.Notification().then(function (data5) {
                                    $scope.RepeatNoft = data5;
                                    FactSheetService.PONotification().then(function (data10) {
                                        $scope.RepeatPONoft = data10;
                                        FactSheetService.Notisperiod().then(function (data6) {
                                            $scope.NoticePrd = data6;
                                            FactSheetService.NotisPOperiod().then(function (data11) {
                                                $scope.NoticePOPrd = data11;

                                                FactSheetService.Doctype().then(function (data7) {
                                                    $scope.TypeOfDoc = data7;
                                                    FactSheetService.GetRefNum().then(function (data8) {
                                                        $scope.FactSheet.DFM_REF_NO = data8;
                                                        ModifyFactSheetService.GetDetails().then(function (data) {
                                                            $scope.gridata = data;
                                                            $scope.gridOptions.api.setRowData(data);
                                                        }, function (error) {
                                                        })
                                                    }, function (error) {
                                                    })
                                                }, function (error) {
                                                })
                                            }, function (error) {
                                            })
                                        }, function (error) {
                                        })
                                    }, function (error) {
                                    })
                                }, function (error) {
                                });
                            }, function (error) {
                            });
                        }, function (error) {
                        });
                    }, function (error) {
                    });
                }, function (error) {

                });
            }, function (error) {

            });
        });
    }

    $scope.Loaddata();

    $scope.Parties = {};
    $scope.GetParties = function (obj) {
        for (var i = 0; i < $scope.Suppliers.length; i += 1) {
            var result = $scope.Suppliers[i];
            if (result.DSD_CODE == obj) {
                $scope.Parties = " Contract between " + result.DSD_NAME + " and QuickFMS";
            }
        }
    }
    var value;
    var EFRM, TRMN;
    $scope.EffDatechange = function () {
        if (new Date($scope.FactSheetDate.DFDT_EFRM_DATE) > new Date($scope.FactSheetDate.DFDT_TRMN_DATE)) {
            showNotification('error', 8, 'bottom-right', 'Invalid date! Expiry Date must to be later than Effective Date.');
        }
        else {
            var EFRM = moment($scope.FactSheetDate.DFDT_EFRM_DATE, "MM/DD/YYYY").year()
            var TRMN = moment($scope.FactSheetDate.DFDT_TRMN_DATE, "MM/DD/YYYY").year()
            value = parseInt(TRMN) - parseInt(EFRM);
        }
    }

    $scope.back = function () {
        $scope.ViewUploption = true;
        $scope.Viewstatus = 0;
    }
    $scope.Requestid = {};

    $scope.GetDetailsOnSelection = function (data) {
        $scope.Requestid = data.DFM_REQ_ID;
        $scope.ViewUploption = false;
        $scope.Viewstatus = 1;
        $scope.EnableStatus = 1;
        ModifyFactSheetService.GetFSDetails(data).then(function (data) {
            $scope.FactSheet = data.MainSheet;
            $scope.FactSheetDate = data.AgreeMentSheet;
            $scope.FactSheetDate.DFDT_SIGN_DATE = $filter('date')($scope.FactSheetDate.DFDT_SIGN_DATE, "dd/MM/yyyy");
            $scope.FactSheetDate.DFDT_EFRM_DATE = $filter('date')($scope.FactSheetDate.DFDT_EFRM_DATE, "dd/MM/yyyy");
            $scope.FactSheetDate.DFDT_TRMN_DATE = $filter('date')($scope.FactSheetDate.DFDT_TRMN_DATE, "dd/MM/yyyy");
            $scope.FactSheetDate.DFDT_REN_DATE = $filter('date')($scope.FactSheetDate.DFDT_REN_DATE, "dd/MM/yyyy");
            $scope.FactSheetPODate = data.POSheet;
            $scope.FactSheetPODate.DFDT_PO_EFRM_DATE = $filter('date')($scope.FactSheetPODate.DFDT_PO_EFRM_DATE, "dd/MM/yyyy");
            $scope.FactSheetPODate.DFDT_PO_TRMN_DATE = $filter('date')($scope.FactSheetPODate.DFDT_PO_TRMN_DATE, "dd/MM/yyyy");
            $scope.FactSheetPODate.DFDT_PO_REN_DATE = $filter('date')($scope.FactSheetPODate.DFDT_PO_REN_DATE, "dd/MM/yyyy");
            $scope.RepeatNoft.DRN_SNO = data.DRD_SNO;
            $scope.RemainderDates.DRD_SNO = data.DRN_SNO;
            $scope.RepeatPONoft.DRN_SNO = data.DRD_SNO;
            $scope.RemainderPODates.DRD_SNO = data.DRN_SNO;
            $scope.FactSheetDetails = data.StakeholderSheet;
            $scope.KBPDetails = data.BusinessStakehol;
            $scope.FactSheetValues = data.ValuesSheet;
            $scope.GetParties($scope.FactSheet.DFM_DSD_CODE);
            $scope.DocumentDetails = data.DocumentDetails;
            $scope.gridOptions1.api.setRowData(data.DocumentDetails);
        }, function (error) {
        })
    }


    var columnDefs1 = [
    { headerName: "Type of document", field: "DFDC_DDT_CODE", width: 150, cellClass: 'grid-align' },
    { headerName: "Document Name", field: "DFDC_ACT_NAME", width: 180, cellClass: 'grid-align' },
    { headerName: "Download", width: 70, template: '<a ng-click = "Download(data)"> <i class="fa fa-download class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
    ];

    $scope.gridOptions1 = {
        columnDefs: columnDefs1,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions1.api.sizeColumnsToFit()
        }
    };

    $scope.DeleteDoc = function (dataobj) {
        $scope.DocumentDetails = _.without($scope.DocumentDetails, dataobj);
        $scope.gridOptions1.api.setRowData($scope.DocumentDetails);
    }

    $scope.Download = function (dataobj) {
        window.location.assign(UtilityService.path + "/api/ModifyFactSheet/DRMExcelDownload?id=" + dataobj.DFDC_ID);
    }

});