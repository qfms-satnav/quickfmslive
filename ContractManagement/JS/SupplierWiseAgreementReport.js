﻿app.service("SupplierWiseAgreementReportService", function ($http, $q) {
    this.GetSupplierWiseData = function (SupplierData) {
        deferred = $q.defer();
        return $http.post('../../api/SupplierWiseAgreementReport/GetSupplierWiseData', SupplierData)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }
    this.SearchSupplierWiseData = function (SearchSupplierData) {
        deferred = $q.defer();
        return $http.post('../../api/SupplierWiseAgreementReport/SearchSupplierWiseData', SearchSupplierData)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }
});

app.controller('SupplierWiseAgreementReportController', function ($scope, $q, $location, SupplierWiseAgreementReportService, UtilityService, $filter) {
    $scope.Columndata = [];
    $scope.griddata = [];

    $scope.chart = c3.generate({
        data: {
            x: 'DSD_NAME',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                height: 130,
                show: true,
                tick: {
                    rotate: 75,
                    multiline: false
                }
            },
            y: {
                show: true,
                label: {
                    text: 'Count',
                    position: 'outer-middle'
                }
            }
        },
        width:
        {
            ratio: 0.5
        }
    });

    $scope.columDefs = [
          { headerName: "Supplier Name", field: "DSD_NAME", cellClass: 'grid-align', filter: 'set', suppressMenu: true },
             { headerName: "Total Agreements", field: "Total_Count", cellClass: 'grid-align', filter: 'set', suppressMenu: true }
    ];

    $scope.gridOptions = {
        columnDefs: $scope.columDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
        floatingFilter: false,
        debug: true,
        enableServerSideSorting: true,
        enableServerSideFilter: true,
        rowModelType: 'infinite',
        paginationPageSize: 10,
        cacheOverflowSize: 2,
        maxConcurrentDatasourceRequests: 2,
        infiniteInitialRowCount: 1,
        maxBlocksInCache: 2,
    };

    var dataSource = {
        rowCount: null,
        getRows: function (params) {
            setTimeout(function () {
                var lastRow = -1;
                var dataObj = {
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 20
                };
                SupplierWiseAgreementReportService.GetSupplierWiseData(dataObj).then(function (response) {
                    console.log(dataObj)
                    if (response.data != null) {
                        $scope.griddata = response.data.gridData;

                        $scope.Columndata.push(response.data.columnNames);
                        angular.forEach(response.data.graphData, function (value, key) {
                            $scope.Columndata.push(value);
                        });
                        console.log($scope.Columndata);
                        $scope.chart.unload();
                        $scope.chart.load({ columns: $scope.Columndata });
                        $scope.ColumnNames = response.data.Columnnames;
                        setTimeout(function () {
                            $("#AllocGraph").empty();
                            $("#AllocGraph").append($scope.chart.element);
                        }, 700);
                        params.successCallback($scope.griddata, lastRow);
                    }
                    else {
                        params.failCallback();
                        $("#btNext").attr("disabled", true);
                    }
                });
            }, 500);

        }
    }

    $scope.GetSupplierWiseData = function () {
        $("#btLast").hide(); 
        $("#btFirst").hide();
       
        $scope.gridOptions.api.setDatasource(dataSource);
    }

    setTimeout(function () { 
        $scope.GetSupplierWiseData();
    }, 300);



    $("#filtertxt").change(function () {
        var searchval = $(this).val();
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                setTimeout(function () {
                    var lastRow = -1;
                    var dataObj = {
                        PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                        PageSize: 20,
                        SearchValue: searchval
                    };
                    SupplierWiseAgreementReportService.SearchSupplierWiseData(dataObj).then(function (response) {
                        console.log(dataObj)
                        if (response.data != null) {
                            $scope.griddata = response.data.gridData;

                            $scope.Columndata.push(response.data.columnNames);
                            angular.forEach(response.data.graphData, function (value, key) {
                                $scope.Columndata.push(value);
                            });
                            console.log($scope.Columndata);
                            $scope.chart.unload();
                            $scope.chart.load({ columns: $scope.Columndata });
                            $scope.ColumnNames = response.data.Columnnames;
                            setTimeout(function () {
                                $("#AllocGraph").empty();
                                $("#AllocGraph").append($scope.chart.element);
                            }, 700);
                            params.successCallback($scope.griddata, lastRow);
                        }
                        else {
                            params.failCallback();
                            $("#btNext").attr("disabled", true);
                        }
                    });
                }, 500);

            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    })

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SupplierWiseAgreementReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.GenReport = function (Allocdata, Type) {
        $scope.GenerateFilterExcel();

    };

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
});