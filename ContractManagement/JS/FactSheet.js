﻿app.service("FactSheetService", function ($http, $q) {
    this.Contract = function () {
        deferred = $q.defer();
        return $http.get('../../api/ContractMaster/BindContractData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Spend = function () {
        deferred = $q.defer();
        return $http.get('../../api/SpendCategoryMaster/BindCategoryData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Business = function () {
        deferred = $q.defer();
        return $http.get('../../api/BusinessGroupMaster/BindBusinessData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Supplier = function () {
        deferred = $q.defer();
        return $http.get('../../api/SupplierMaster/BindSupplierData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Remainder = function () {
        deferred = $q.defer();
        return $http.get('../../api/FactSheet/GetReaminderDetails')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.PORemainder = function () {
        deferred = $q.defer();
        return $http.get('../../api/FactSheet/GetReaminderDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


    this.Notification = function () {
        deferred = $q.defer();
        return $http.get('../../api/FactSheet/GetNotificationDetails')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.PONotification = function () {
        deferred = $q.defer();
        return $http.get('../../api/FactSheet/GetNotificationDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.Notisperiod = function () {
        deferred = $q.defer();
        return $http.get('../../api/FactSheet/GetNotificDetails')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.NotisPOperiod = function () {
        deffered = $q.defer();
        return $http.get('../../api/FactSheet/GetNotificDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.Doctype = function () {
        deferred = $q.defer();
        return $http.get('../../api/DocumentTypeMaster/BindDocumentData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetRefNum = function () {
        deferred = $q.defer();
        return $http.get('../../api/FactSheet/Getrefnum')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Submit = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/FactSheet/Submit', dataobj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

});

app.controller('FactSheetController', function ($scope, FactSheetService, UtilityService) {
    $scope.AgreeDates = true;
    $scope.PODates = true;
    $scope.PORemainder = true;
    $scope.UploadDoc = true;
    $scope.TermsClause = true;
    $scope.Stakehol = true;
    $scope.Values = true;
    $scope.Remainder = true;
    $scope.FactSheet = {};
    $scope.FactSheetDate = {};
    $scope.FactSheetRenewal = {};
    $scope.FactSheetValues = {};
    $scope.FactSheetDetails = {};
    $scope.document = {};
    $scope.DocumentDetails = [];
    $scope.Select5 = true;
    $scope.PODates = true;
    $scope.PORemainder = true;
    
    
    $scope.KBPDetails = [
     {
         'KPH_NAME': '',
         'IS_ERROR': false,
     }
    ]

    $scope.AddNew = function () {
        $scope.KBPDetails.push({
            'KPH_NAME': '',
            'IS_ERROR': false,
        });
    }

    $scope.Delete = function (item) {
        $scope.KBPDetails = _.without($scope.KBPDetails, item);
    }

    $scope.RepeatNoft = {};

    $scope.Loaddata = function () {
        FactSheetService.Contract().then(function (data) {
            $scope.Contracts = data;
            FactSheetService.Spend().then(function (data1) {
                $scope.SpendCats = data1;
                FactSheetService.Business().then(function (data2) {
                    $scope.BusinessGrp = data2;
                    FactSheetService.Supplier().then(function (data3) {
                        $scope.Suppliers = data3;
                        FactSheetService.Remainder().then(function (data4) {
                            $scope.RemainderDates = data4;
                            FactSheetService.PORemainder().then(function (data9) {
                                $scope.RemainderPODates = data9
                                FactSheetService.Notification().then(function (data5) {
                                    $scope.RepeatNoft = data5;
                                    FactSheetService.PONotification().then(function (data10) {
                                        $scope.RepeatPONoft = data10;
                                        FactSheetService.Notisperiod().then(function (data6) {
                                            $scope.NoticePrd = data6;
                                            FactSheetService.NotisPOperiod().then(function (data11) {
                                                $scope.NoticePOPrd = data11;
                                                FactSheetService.Doctype().then(function (data7) {
                                                    $scope.TypeOfDoc = data7;
                                                    FactSheetService.GetRefNum().then(function (data8) {
                                                        $scope.FactSheet.DFM_REF_NO = data8;
                                                    }, function (error) {
                                                    })
                                                }, function (error) {
                                                })
                                            }, function (error) {
                                            })
                                        }, function (error) {
                                        })
                                    }, function (error) {
                                    })
                                }, function (error) {
                                });
                            }, function (error) {
                            });
                        }, function (error) {
                        });
                    }, function (error) {
                    });
                }, function (error) {

                });
            }, function (error) {

            });
        });
    }

    $scope.Loaddata();

    $scope.Parties = {};
    $scope.GetParties = function (obj) {
        for (var i = 0; i < $scope.Suppliers.length; i += 1) {
            var result = $scope.Suppliers[i];
            if (result.DSD_CODE == obj) {
                $scope.Parties = " Contract between " + result.DSD_NAME + " and " + companyid;
            }
        }
    }
    var value;
    var EFRM, TRMN;
  
    $scope.EffDatechange = function () {
        if (new Date($scope.FactSheetDate.DFDT_EFRM_DATE) > new Date($scope.FactSheetDate.DFDT_TRMN_DATE)) {
            showNotification('error', 8, 'bottom-right', 'Invalid date! Expiry Date must to be later than Effective Date.');
        }
        else {
             EFRM = moment($scope.FactSheetDate.DFDT_EFRM_DATE, "DD/MM/YYYY").year()
             TRMN = moment($scope.FactSheetDate.DFDT_TRMN_DATE, "DD/MM/YYYY").year()
            value = parseInt(TRMN) - parseInt(EFRM);
        }
    }
    
    $scope.POEffDatechange = function () {
        if (new Date($scope.FactSheetPODate.DFDT_PO_EFRM_DATE) > new Date($scope.FactSheetPODate.DFDT_PO_TRMN_DATE)) {
            showNotification('error', 9, 'bottom-right', 'Invalid date! PO Expiry Date must to be later than PO Effective Date.')
        }
        else {
            var POEFRM = moment($scope.FactSheetPODate.DFDT_PO_EFRM_DATE, "DD/MM/YYYY").year()
            var POTRMN = moment($scope.FactSheetPODate.DFDT_PO_TRMN_DATE, "DD/MM/YYYY").year()
            value = parseInt(POTRMN) - parseInt(POEFRM);
        }
    }
    $scope.SaveFactSheet = function () {
        //if ($scope.RemainderDates.DRD_SNO != undefined && $scope.RemainderPODates.DRD_SNO != undefined) {
            //if ($scope.RepeatNoft.DRN_SNO != undefined && $scope.RepeatPONoft.DRN_SNO != undefined) {
                //if ($scope.FactSheetDate.DFDT_EFRM_DATE < $scope.FactSheetDate.DFDT_TRMN_DATE && $scope.FactSheetPODate.DFDT_PO_EFRM_DATE < $scope.FactSheetPODate.DFDT_PO_TRMN_DATE) {
                    $scope.dataobj = {
                        MainSheet: $scope.FactSheet,
                        AgreeMentSheet: $scope.FactSheetDate,
                        DRN_SNO: $scope.RepeatNoft.DRN_SNO,
                        DRD_SNO: $scope.RemainderDates.DRD_SNO,
                        POSheet: $scope.FactSheetPODate,
                        DRN_SNO1: $scope.RepeatPONoft.DRN_SNO,
                        DRD_SNO2: $scope.RemainderPODates.DRD_SNO,
                        ValuesSheet: $scope.FactSheetValues,
                        StakeholderSheet: $scope.FactSheetDetails,
                        BusinessStakehol: $scope.KBPDetails,
                        DocumentDetails: $scope.DocumentDetails
                    };
                    console.log($scope.dataobj);
                    FactSheetService.Submit($scope.dataobj).then(function (data) {
                            if (data != "ERROR") {
                                $scope.FactSheet = {};
                                $scope.FactSheetDate = {};
                                $scope.FactSheetPODate = {};
                                $scope.FactSheetRenewal = {};
                                $scope.FactSheetValues = {};
                                $scope.FactSheetDetails = {};
                                $scope.KBPDetails = [
          {
              'KPH_NAME': '',
          }
                                ]
                                angular.forEach($scope.RemainderDates, function (val) {
                                    if (val.DRD_ISCHECKED == true) {
                                        val.DRD_ISCHECKED = false;
                                    }
                                });
                                $scope.RepeatNoft.DRN_SNO = {};
                                $scope.RemainderDates.DRD_SNO = {};
                                $scope.RepeatPONoft.DRN_SNO = {};
                                $scope.RemainderPODates.DRD_SNO = {};
                                $scope.frm.$submitted = false;
                                $scope.DocumentDetails = [];
                                $scope.document = {};
                                showNotification('success', 8, 'bottom-right', "Submitted Succesfully..");
                            }
                           
                    }, function (error) {
                    });
            //    }
            //    else if (error = 8) {
            //        showNotification('error', 8, 'bottom-right', 'Invalid date! Expiry Date should be greater than Effective Date.');

            //    }
            //    else {
            //        showNotification('error', 9, 'bottom-right', 'Invalid date ! PO Expiry Date should be greater than PO Effective Date.');
            //    }            
            ////}
            //else if (error = 4) {
            //    showNotification('error', 4, 'bottom-right', "Please Select atleast One Repeat Notification");

            //}
            //else {
            //    showNotification('error', 10, 'bottom-right', "Please Select atleast One PO Repeat Notification");
            //}
        //}
        //else if (error = 4) {
        //    showNotification('error', 4, 'bottom-right', "Please Select atleast One Renewal Remainder Date");
        //}
        //else {
        //    showNotification('error', 10, 'bottom-right', "Please Select atleast One PO Renewal Remainder Date");
        //}
    }

    $scope.UploadOutsideSubmit = function () {
        $scope.frm.$submitted = true;
        $scope.SaveFactSheet();
        //if ($scope.frm.$valid) {
        //    $scope.SaveFactSheet();
        //}
    }


    var columnDefs = [
      { headerName: "Type of document", field: "DFDC_DDT_CODE", width: 150, cellClass: 'grid-align' },
      { headerName: "Document Name", field: "DFDC_ACT_NAME", width: 180, cellClass: 'grid-align' },
      { headerName: "Download", width: 70, template: '<a ng-click = "DeleteDoc(data)"> <i class="fa fa-times class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        //enableColResize: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.DeleteDoc = function (dataobj) {
       // $scope.DocumentDetails = _.without($scope.DocumentDetails, _.findWhere($scope.DocumentDetails, dataobj));
       // _.remove($scope.gridData, function (x) { if (DeletebyPOW_ID.POW_PSY_ID == x.POW_PSY_ID && DeletebyPOW_ID.POW_USR_ID == x.POW_USR_ID && DeletebyPOW_ID.POW_ID == x.POW_ID) return x; });
        $scope.DocumentDetails = _.without($scope.DocumentDetails, dataobj);
        $scope.gridOptions.api.setRowData($scope.DocumentDetails);
    }

    $scope.UploadFile = function () {
        if ($scope.document.DFDC_DDT_CODE != undefined) {
            var Ext = $('#FileUpl').val().split('.').pop().toLowerCase();
            var fname = $('#FileUpl').val().split('\\');
            var str = fname[fname.length - 1];
            var ExtCount = str.split('.');
            if (ExtCount.length <= 2) {
                if ($('#FileUpl').val()) {
                    if (/^[a-zA-Z0-9-_. ]*$/.test(str) == true) {
                        if (Ext == "xlsx" || Ext == "xls" || Ext == "doc" || Ext == "docx" || Ext == "PDF" || Ext == "ppt" || Ext == "pptx" || Ext == "pdf" || Ext == "txt" || Ext == "Txt" || Ext == "msg" || Ext == "jpeg" || Ext == "jpg" || Ext == "png") {
                            if (!window.FormData) {
                                redirect(); // if IE8   
                            }
                            else {
                                var formData = new FormData();
                                var UplFile = $('#FileUpl')[0];
                                formData.append("UplFile", UplFile.files[0]);
                                formData.append("CurrObj", $scope.document.DFDC_DDT_CODE);
                                $.ajax({
                                    url: UtilityService.path + '/api/FactSheet/UploadTemplate',
                                    type: "POST",
                                    data: formData,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function (data) {
                                        var respdata = JSON.parse(data);
                                        if (respdata.data != null) {                                  
                                            angular.forEach(respdata.data, function (data) {
                                                $scope.DocumentDetails.push(data);
                                            });
                                            console.log($scope.DocumentDetails);
                                            $scope.gridOptions.api.setRowData($scope.DocumentDetails);
                                            $scope.document = {};
                                            showNotification('success', 8, 'bottom-right', "Successfully Uploaded");
                                        }
                                        else {
                                            showNotification('error', 8, 'bottom-right', respdata.Message);
                                        }
                                    }
                                });
                            }
                        }
                        else {
                            showNotification('error', 8, 'bottom-right', 'Upload .xls, .xlsx, .pdf, .psd & .doc files only');
                        }
                    }
                    else {
                        showNotification('error', 8, 'bottom-right', 'Please ensure your input contains only letters (a-z, A-Z), digits (0-9), hyphen (-), underscore (_), period (.), or space.');
                    }
                }
                else {
                    showNotification('error', 8, 'bottom-right', 'Select a file to upload');
                }
            }
            else {
                showNotification('error', 8, 'bottom-right', 'Upload valid file');
            }
        }

        else {
            showNotification('error', 8, 'bottom-right', 'Select document type');
        }
    }

});