﻿app.service("AgreementNearExpiryReportService", function ($http, $q) {

    this.GetAgreementNearExpiryReport = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/AgreementNearExpiryReport/GetAgreementNearExpiryReport', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }

    this.SearchAgreementNearExpiryReport = function (SearchExpiryReport) {
        deferred = $q.defer();
        return $http.post('../../api/AgreementNearExpiryReport/SearchAgreementNearExpiryReport', SearchExpiryReport)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }
});

app.controller('AgreementNearExpiryReportController', function ($scope, $q, $location, AgreementNearExpiryReportService, UtilityService, $filter) {

    $scope.AgreementNearExpiry = {};
 
    $scope.columDefs = [
          { headerName: "Business group", field: "DBT_NAME", cellClass: 'grid-align', filter: 'set', suppressMenu: true },
             { headerName: "Contract Name", field: "DCT_NAME", cellClass: 'grid-align', filter: 'set', suppressMenu: true },
          { headerName: "Created Date", field: "DFM_CREATED_DT", template: '<span>{{data.DFM_CREATED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true },
           { headerName: "Contract Ref No", field: "DFM_REF_NO", cellClass: "grid-align", suppressMenu: true },
            { headerName: "Spend Category", field: "DST_NAME", cellClass: "grid-align", suppressMenu: true },
               { headerName: "Expiry Date", field: "DFDT_TRMN_DATE", template: '<span>{{data.DFDT_TRMN_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: "grid-align", suppressMenu: true }

    ];

    //$scope.gridOptions = {
    //    columnDefs: $scope.columDefs,
    //    rowData: null,
    //    cellClass: 'grid-align',
    //    angularCompileRows: true,
    //    enableScrollbars: false,
    //    enableFilter: true,
    //    rowSelection: 'multiple',
    //    floatingFilter: false,
    //    debug: true,
    //    enableServerSideSorting: true,
    //    enableServerSideFilter: true,
    //    rowModelType: 'infinite',
    //    paginationPageSize: 10,
    //    cacheOverflowSize: 2,
    //    maxConcurrentDatasourceRequests: 2,
    //    infiniteInitialRowCount: 1,
    //    maxBlocksInCache: 2,
    //};
    $scope.gridOptions = {
        columnDefs: $scope.columDefs,
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple',
    };

    $scope.GetAgreementNearExpiryReport = function () {
        if (new Date($scope.AgreementNearExpiry.FROM_DATE) > new Date($scope.AgreementNearExpiry.TO_DATE)) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        $("#btLast").hide();
        $("#btFirst").hide();
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                //Removed SetTiomeout functions and placed outside of function
            }
        }
        setTimeout(function () {
            var lastRow = -1;
            var dataObj = {
                FROM_DATE: $scope.AgreementNearExpiry.FROM_DATE,
                TO_DATE: $scope.AgreementNearExpiry.TO_DATE,
                PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                PageSize: 20
            };

            AgreementNearExpiryReportService.GetAgreementNearExpiryReport(dataObj).then(function (response) {
                console.log(dataObj)
                console.log(response)
                if (response.data.length > 0) {
                    //params.successCallback(response.data, lastRow);
                    $scope.gridOptions.api.setRowData(response.data);
                }
                else {
                    //params.failCallback();
                    //$("#btNext").attr("disabled", true);
                    $scope.gridOptions.api.setRowData([]);
                }
            });
        }, 500);
        //$scope.gridOptions.api.setDatasource(dataSource);
        //$scope.gridOptions.api.setRowData(response.data);
    }


    //$("#filtertxt").change(function () {
    //    var searchval = $(this).val();
    //    var dataSource = {
    //        rowCount: null,
    //        getRows: function (params) {
    //            setTimeout(function () {
    //                var lastRow = -1;
    //                var dataObj = {
    //                    FROM_DATE: $scope.AgreementNearExpiry.FROM_DATE,
    //                    TO_DATE: $scope.AgreementNearExpiry.TO_DATE,
    //                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
    //                    PageSize: 20,
    //                    SearchValue: searchval
    //                };

    //                AgreementNearExpiryReportService.SearchAgreementNearExpiryReport(dataObj).then(function (response) {
    //                    console.log(dataObj)
    //                    if (response.data.length > 0) {
    //                        //$scope.gridOptions.api.setRowData(response.data);
    //                        //$scope.griddata = response.data.gridData;
    //                        params.successCallback(response.data, lastRow);
    //                    }
    //                    else {
    //                        params.failCallback();
    //                        $("#btNext").attr("disabled", true);
    //                    }
    //                });
    //            }, 500);

    //        }
    //    }
    //    $scope.gridOptions.api.setDatasource(dataSource);
    //})

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "AgreementNearExpiryReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.GenReport = function (Allocdata, Type) {
        $scope.GenerateFilterExcel();

    };


    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
});