﻿app.service("BusinessGroupMasterService", ['$http', '$q', function ($http, $q) {
    this.BindGrid = function () {
        deferred = $q.defer();
        return $http.get('../../api/BusinessGroupMaster/BindBusinessData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.Insertdata = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/BusinessGroupMaster/Insert', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }

    this.Updatedata = function (dataobj) {
        deferred = $q.defer();
        return $http.post('../../api/BusinessGroupMaster/Update', dataobj)
        .then(function (response) {
            deferred.resolve(response.data);
            return deferred.promise;
        }, function (response) {
            deferred.reject(response);
            return deferred.promise;
        });
    }

}]);

app.controller('BusinessGroupMasterController', ['$scope', '$q', 'BusinessGroupMasterService', function ($scope, $q, BusinessGroupMasterService) {

    $scope.ActionStatus = 0;
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.BusinessGroup = {};
    $scope.IsInEdit = false;
    $scope.codepattern = /^[a-zA-Z0-9. ]*$/;
    $scope.namepattern = /^[a-zA-Z0-9-_ /():. ]*$/;

    //for GridView 
    var columnDefs = [
       { headerName: "Business Group Code", field: "DBT_CODE", width: 150, cellClass: 'grid-align' },
       { headerName: "Business Group Name", field: "DBT_NAME", width: 180, cellClass: 'grid-align' },
       { headerName: "Status", template: "{{ShowStatus(data.DBT_STA_ID)}}", width: 100, cellClass: 'grid-align' },
       { headerName: "Edit", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableFilter: true,
        //enableColResize: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    $scope.Loaddata = function () {
        BusinessGroupMasterService.BindGrid().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
        });
    }
    $scope.Loaddata();
    
    $scope.EditFunction = function (data) {
        $scope.BusinessGroup = {};
        angular.copy(data, $scope.BusinessGroup);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        $scope.BusinessGroup.DBT_STA_ID = data.DBT_STA_ID;
    }

    $scope.ClearData = function () {
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.BusinessGroup = {};
        $scope.frmBussMas.$submitted = false;
    }

    $scope.Save = function () {
        if ($scope.IsInEdit) {
            BusinessGroupMasterService.Updatedata($scope.BusinessGroup).then(function () {
                var updatedobj = {};
                angular.copy($scope.BusinessGroup, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully uploaded";
                BusinessGroupMasterService.BindGrid().then(function (data) {
                    $scope.gridata = data;
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                });
                $scope.IsInEdit = false;
                $scope.ClearData();
                showNotification('success', 8, 'bottom-right', $scope.Success);
            }, function (error) {
            })
        }
        else {
            BusinessGroupMasterService.Insertdata($scope.BusinessGroup).then(function (data) {
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully inserted";
                var savedobj = {};
                angular.copy($scope.BusinessGroup, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.BusinessGroup = {};
                $scope.ClearData();
            }
            , function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
            });
        }
    }

}]);