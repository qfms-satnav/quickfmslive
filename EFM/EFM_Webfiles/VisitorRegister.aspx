<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VisitorRegister.aspx.vb" Inherits="ESP_ESP_Webfiles_VisitorRegister" Title="Visitor Register" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/bootstrap-select.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/amantra.min.css")%>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/BootStrapCSS/font-awesome/css/font-awesome.min.css")%>' rel="stylesheet" />
    <link rel="stylesheet" href='<%= Page.ResolveUrl("~/BootStrapCSS/datepicker.css")%>' />
    

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Visitor Register
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="row">
                                        <asp:HyperLink ID="hypAddVisitor" runat="server" Text="Add Visitor" NavigateUrl="NewVisitor.aspx"></asp:HyperLink>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="No Register Found.">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReqId" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_REQID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Visitor Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblname" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemail" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_EMAIL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mobile">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmobile" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_MOBILE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Whom to Meet">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmeet" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_MEET") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="In Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIntime" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_INTIME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:ButtonField Text="PunchOut" CommandName="PunchOut" />
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap.min.js")%>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-select.min.js")%>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-datepicker.js") %>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/ObjectKeys.js")%>' defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/wz_tooltip.js")%>' type="text/javascript" defer></script>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" temp_src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" defer></script>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" temp_src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" temp_src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</body>
</html>



