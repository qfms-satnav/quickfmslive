<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmLveRMApprovalEncash.aspx.vb" Inherits="ESP_ESP_Webfiles_frmLveRMApprovalEncash" Title="RM Approval" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Approve Leave Encashment
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Associate Name<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAssociateName" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Department<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlDep" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Associate ID<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAssociateID" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Reporting Manager<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlRM" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Designation<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtDesig" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Minimum Leave Balance for Encashment<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtContactNo" runat="server" CssClass="form-control"
                                                MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Balance Paid Leaves<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtPleaves" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Balance Compensatory Leaves<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtCompLeaves" runat="server" CssClass="form-control"
                                                MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Type of Encashment<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlEncash" runat="server" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Cheque</asp:ListItem>
                                                <asp:ListItem Value="2">Cash</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Type of Leave<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLeave" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Applied Days for Encashment<span style="color: red;"></span></label>

                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtEncash" runat="server" CssClass="form-control"
                                                MaxLength="10"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Balance Leaves<span style="color: red;"></span></label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtNoLeaves" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="row">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                                        CausesValidation="true" />
                                    <asp:Button ID="btnReject" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"
                                        CausesValidation="true" />
                                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>




