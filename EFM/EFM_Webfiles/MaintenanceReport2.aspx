<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="MaintenanceReport2.aspx.vb" Inherits="EFM_EFM_Webfiles_MaintenanceReport2"
    Title="Report By Category" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Report By Category
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="Server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">From Date</label>
                                        <asp:RequiredFieldValidator ID="rfvfromdate" runat="server" ControlToValidate="txtfromdate"
                                            Display="None" ErrorMessage="Please Select From Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">To Date</label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txttodate"
                                            Display="None" ErrorMessage="Please Select To Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div class='input-group date' id='todate'>
                                                <asp:TextBox ID="txttodate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location</label>
                                        <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlType"
                                            Display="None" ErrorMessage="Please Select Location " ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlType" runat="server" CssClass="selectpicker" data-live-search="true"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Service request category</label>
                                        <asp:RequiredFieldValidator ID="rfvreq" runat="server" ControlToValidate="ddlReq"
                                            Display="NONE" ErrorMessage="Please Select Service Request Category" ValidationGroup="Val1"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlReq" runat="server" TabIndex="11" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Service request</label>
                                        <asp:RequiredFieldValidator ID="rfvreqtype" runat="server" ControlToValidate="ddlreqtype"
                                            Display="NONE" ErrorMessage="Please Select Service Request" ValidationGroup="Val1"
                                            InitialValue="--Select--" Enabled="true"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlreqtype" runat="server" TabIndex="12" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select From Time</label>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cbohr" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select To Time</label>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="cboToHr" runat="server" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Status</label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlstatus"
                                            Display="None" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlstatus" runat="server" CssClass="selectpicker" data-live-search="true">
                                                <%--<asp:ListItem Value="--Select--">--Select--</asp:ListItem>--%>
                                                <asp:ListItem Value="">--All--</asp:ListItem>
                                                <asp:ListItem Value="1">Request Submitted</asp:ListItem>
                                                <asp:ListItem Value="2">Request Modified</asp:ListItem>
                                                <asp:ListItem Value="3">Request Cancelled</asp:ListItem>
                                                <asp:ListItem Value="4">RM Approved</asp:ListItem>
                                                <asp:ListItem Value="5">RM Rejected</asp:ListItem>
                                                <asp:ListItem Value="6">On Hold</asp:ListItem>
                                                <asp:ListItem Value="7">InProgress</asp:ListItem>
                                                <asp:ListItem Value="8">Closed</asp:ListItem>
                                                <asp:ListItem Value="9">Assigned</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true" ValidationGroup="Val1" />
                                </div>
                            </div>
                            <div>&nbsp</div>
                            <div class="row table table table-condensed table-responsive">
                                 <div class="form-group">
                                    <div class="col-md-12">
                                        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
