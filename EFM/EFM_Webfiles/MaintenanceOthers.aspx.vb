Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class EFM_EFM_Webfiles_MaintenanceOthers
    Inherits System.Web.UI.Page
    Dim lbl As Integer = 1
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack() Then

            BindTimings1()
            txtfromdate.Attributes.Add("onClick", "displayDatePicker('" + txtfromdate.ClientID + "')")
            txtfromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txttodate.Attributes.Add("onClick", "displayDatePicker('" + txttodate.ClientID + "')")
            txttodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If
    End Sub
    Private Sub BindTimings1()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_TIMINGS_2")
        sp.Command.AddParameter("@TIMING", 0, DbType.Int32)
        cbohr.DataSource = sp.GetDataSet()
        cbohr.DataTextField = "VALUE"
        cbohr.DataValueField = "VALUE"
        cbohr.DataBind()
        cbohr.Items.Insert(0, New ListItem("--HH--", "--HH--"))
    End Sub
    Private Sub BindTimings2()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_TIMINGS_REPORT")
        sp.Command.AddParameter("@FROM_dATE", txtfromdate.Text, DbType.Date)
        sp.Command.AddParameter("@TO_dATE", txttodate.Text, DbType.Date)
        sp.Command.AddParameter("@FROM_TIME", cbohr.SelectedValue, DbType.Int32)
        cboToHr.DataSource = sp.GetDataSet()
        cboToHr.DataTextField = "VALUE"
        cboToHr.DataValueField = "VALUE"
        cboToHr.DataBind()

    End Sub

    Protected Sub cbohr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbohr.SelectedIndexChanged
        If cbohr.SelectedIndex > 0 And txtfromdate.Text <> "" And txttodate.Text <> "" Then
            BindTimings2()
        Else
            lblMsg.Text = "Please enter mandatory fields"

        End If
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If Convert.ToDateTime(txtfromdate.Text) <= Convert.ToDateTime(txttodate.Text) Then



            If cbohr.SelectedItem.Value = "--HH--" Then
                Response.Redirect("mothers.aspx?FromDate=" + txtfromdate.Text + "&toDate=" + txttodate.Text + "&FromTime=0&ToTime=23  ")
            End If
            Response.Redirect("mothers.aspx?FromDate=" + txtfromdate.Text + "&toDate=" + txttodate.Text + "&FromTime=" + cbohr.SelectedItem.Value + "&ToTime=" + cboToHr.SelectedItem.Value + " ")
        Else
            lblMsg.Text = "From date should be less than to date."
        End If
    End Sub
End Class
