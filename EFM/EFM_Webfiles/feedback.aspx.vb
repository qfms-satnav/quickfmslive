Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class EFM_EFM_Webfiles_feedback
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If Page.IsValid = True Then
            AddFeedback()
            lblMsg.Text = "Feedback Added Succesfully"
            BindGrid()
            Cleardata()

        End If
    End Sub
    Private Sub Cleardata()
        ddlid.SelectedIndex = -1
        txtcomments.Text = ""
    End Sub
    Private Sub AddFeedback()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_FEEDBACK")
        sp.Command.AddParameter("@REQUEST_ID", ddlid.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@feedback", txtcomments.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@TEST", ddlfeedback.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack() Then
            BindRequstids()
            BindGrid()
            ddlid.Items.Clear()
            ddlid.Items.Insert(0, New ListItem(Request.QueryString("id"), Request.QueryString("id")))
        End If
        lblMsg.Text = ""


    End Sub
    Private Sub BindRequstids()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_REQUEST_IDS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        Dim ds As DataSet = sp.GetDataSet()
        txtreq.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        txtspace.Text = ds.Tables(0).Rows(0).Item("SER_SPC_ID")
        txtreqdesc.Text = ds.Tables(0).Rows(0).Item("SER_dESCRIPTION")
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_GRID_FEEDBACK")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub
End Class
