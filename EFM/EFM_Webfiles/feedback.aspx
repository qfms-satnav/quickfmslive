<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="feedback.aspx.vb" Inherits="EFM_EFM_Webfiles_feedback" Title="Feedback" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Feedback
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="85%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Feedback</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    &nbsp; &nbsp; &nbsp;
                    <table id="t" runat="server" cellspacing="0" cellpadding="1" width="100%" border="1">
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Requisitioner
                               
                            </td>
                            <td align="left" style="width: 50%">
                               <asp:TextBox ID="txtreq" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="TRUE" ></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                 Requisition ID<%--<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvid" runat="server" Display="none" ErrorMessage="Please Select Requisition ID"
                                    ControlToValidate="ddlid" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>--%>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:DropDownList ID="ddlid" runat="server" CssClass="clsComboBox" Width="97%" Enabled="false">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Space ID
                               
                            </td>
                            <td align="left" style="width: 50%">
                               <asp:TextBox ID="txtspace" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="TRUE"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Request Description
                               
                            </td>
                            <td align="left" style="width: 50%">
                               <asp:TextBox ID="txtreqdesc" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="TRUE"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                Give Feedback by entering Comments<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none"
                                    ErrorMessage="Please Enter Comments " ControlToValidate="txtcomments" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:TextBox ID="txtcomments" runat="server"  Width="97%" TextMode="MultiLine" Rows="3" MaxLength="1000"></asp:TextBox>
                            </td>
                        </tr>
                        
                         <tr>
                            <td align="left" style="height: 26px; width: 50%">
                               Select Feedback<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="none"
                                    ErrorMessage="Please Enter Comments " ControlToValidate="txtcomments" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="width: 50%">
                               <asp:DropDownList ID="ddlfeedback" runat="server" CssClass="clsComboBox" Width="97%">
                               <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                               <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                               <asp:ListItem Value="VeryGood">Very Good</asp:ListItem>
                               <asp:ListItem Value="Good">Good</asp:ListItem>
                               <asp:ListItem Value="Satisfactory">Satisfactory</asp:ListItem>
                               <asp:ListItem Value="Poor">Poor</asp:ListItem>
                              
                               
                               </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit" CausesValidation="true"
                                    ValidationGroup="Val1" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel ID="panel1" runat="server" Width="100%">
                    <asp:GridView ID="gvitems" runat="server" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false" EmptyDataText="No Records Found" Width="100%">
               <Columns>
                <asp:TemplateField Visible="false">
               <ItemTemplate>
               <asp:Label ID="lblsno" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Requisitioner">
               <ItemTemplate>
               <asp:Label ID="lblreqstner" runat="server" Text='<%#Eval("REQUISITIONER") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Requestid">
               <ItemTemplate>
               <asp:Label ID="lblid" runat="server" Text='<%#Eval("REQUEST_ID") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="SpaceID">
               <ItemTemplate>
               <asp:Label ID="lblspace" runat="server" Text='<%#Eval("SER_SPC_ID") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Request Description">
               <ItemTemplate>
               <asp:Label ID="lblreqdesc" runat="server" Text='<%#Eval("SER_DESCRIPTION") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Feedback">
               <ItemTemplate>
               <asp:Label ID="lblfeedback" runat="server" Text='<%#Eval("FEEDBACK") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="Comments">
               <ItemTemplate>
               <asp:Label ID="lblComments" runat="server" Text='<%#Eval("COMMENTS") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
                <asp:TemplateField HeaderText="Date">
               <ItemTemplate>
               <asp:Label ID="lbldate" runat="server" Text='<%#Eval("FEEDBACK_DATE") %>'></asp:Label>
               </ItemTemplate>
               </asp:TemplateField>
               </Columns>
               </asp:GridView>
                    </asp:Panel>
                </td>
                <td background="../../Images/table_right_mid_bg.gif">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td>
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
