﻿// JScript File

function dg_maintenance_Activities(User)
{ 

   $("#UsersGrid").jqGrid(
   {    url: '../../Generics_Handler/AssignMaintenance.ashx?MUser='+ User +'',
        datatype: 'json',
        height: 250,
        
        colNames: ['User Type','Requisition ID','Service Type','Request Type','Status','Alert'],
        colModel: [
                           { name: 'AUR_TYPE', width: 150, sortable: true },
                           { name: 'SER_ID', width: 150, sortable: true }, 
                           { name: 'SERVICE_TYPE', width: 150, sortable: true }, 
                           { name: 'REQUEST_TYPE',  width: 150,   sortable: true },
                        { name: 'SER_STATUS',  width: 150,   sortable: true },
                        { name: 'ALERT',  width: 150,   sortable: true }
                           
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UsersGridPager'),
        sortname: 'SER_ID',
        viewrecords: true,
        sortorder: 'DESC'
        //caption: 'Report'
        }).navGrid('#UsersGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					// add custom button to export the data to excel
			jQuery("#UsersGrid").jqGrid('navButtonAdd','#UsersGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/AssignMaintenance.aspx?Muser='+ User +''; jQuery("#UsersGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 