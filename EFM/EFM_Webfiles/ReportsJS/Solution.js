﻿// JScript File

function dg_maintenance_Solution(User)
{ 

   $("#UsersGrid1").jqGrid(
   {    url: '../../Generics_Handler/Solution.ashx?MUser='+ User +'',
        datatype: 'json',
        height: 250,
        
        colNames: ['Comments','Updated By','Request Status','Alert'],
        colModel: [
                           
                          
                           { name: 'COMMENTS', width: 200, sortable: true }, 
                           { name: 'UPDATED_BY', width: 130, sortable: true },
                           { name: 'REQUEST_STATUS',  width: 90,   sortable: true },
                           { name: 'Alert',  width: 50,   sortable: true }
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UsersGridPager1'),
        sortname: 'COMMENTS',
        viewrecords: true,
        sortorder: 'DESC'
        //caption: 'Report'
        }).navGrid('#UsersGridPager1',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					
					// add custom button to export the data to excel
			jQuery("#UsersGrid1").jqGrid('navButtonAdd','#UsersGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/MaintenanceSolution.aspx?Muser='+ User +''; jQuery("#UsersGrid1").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 