﻿// JScript File

function dg_manage_stores(Dep,User,FromDate,toDate,stat,Usertype)
{ 

   $("#UsersGrid").jqGrid(
   {    url: '../../Generics_Handler/Maintenance.ashx?Mdep='+ Dep +'&Muser='+ User + '&Mfromdate='+ FromDate +'&Mtodate='+ toDate +'&Mstatus='+ stat +'&Mtype='+ Usertype +'',
        datatype: 'json',
        height: 250,
        
        colNames: ['Requisition ID','Requested Date','Space ID','Requested By','Request Type','Request','Description','Assigned To','Closed DateTime','Total Time (in Minutes)','Delayed by (in Minutes)','Status'],
        colModel: [
                           { name: 'REQUESITION_ID', width: 130, sortable: true },
                           { name: 'REQUESTED_DATE1', width: 130, sortable: true }, 
                           { name: 'SPACE_ID', width: 130, sortable: true }, 
                           { name: 'REQUESTED_BY', width: 100, sortable: true }, 
                           { name: 'REQUEST_TYPE',  width: 100,   sortable: true },
                           { name: 'REQUEST',  width: 100,   sortable: true },
                           { name: 'DESCRIPTION',  width: 100,   sortable: true },
                           { name: 'ASSIGNED_TO', width: 90, sortable: true },
                           { name: 'CLOSED_TIME', width: 90, sortable: true }, 
                           { name: 'TOTAL_TIME',  width: 70,   sortable: true },
                            { name: 'DELAYED_BY', width: 90, sortable: true }, 
                            { name: 'SER_STATUS',  width: 90,   sortable: true }
                       
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UsersGridPager'),
        sortname: 'REQUESTED_DATE',
        viewrecords: true,
        sortorder: 'DESC'
        //caption: 'Report'
        }).navGrid('#UsersGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					
					// add custom button to export the data to excel
			jQuery("#UsersGrid").jqGrid('navButtonAdd','#UsersGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/MaintenanceReport.aspx?Mdep='+ Dep +'&Muser='+ User + '&Mfromdate='+ FromDate +'&Mtodate='+ toDate +'&Mstatus='+ stat +'&Mtype='+ Usertype +''; jQuery("#UsersGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 
 