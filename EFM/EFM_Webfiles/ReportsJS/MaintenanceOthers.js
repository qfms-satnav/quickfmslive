﻿// JScript File

function dg_manage_stores(FromDate,toDate,FromTime,ToTime)
{ 

   $("#UserGrid").jqGrid(
   {    url: '../../Generics_Handler/MaintenanceOthers.ashx?Mfromdate='+ FromDate +'&Mtodate='+ toDate +'&Mfromtime='+ FromTime +'&Mtotime='+ ToTime +'',
        datatype: 'json',
        height: 250,
        
        colNames: ['Requisition ID','Requested Date','Space ID','Requested By','Request Type','Request','Description','Assigned To','Closed DateTime','Total Time (in Minutes)','Delayed by (in Minutes)','Status'],
        colModel: [
                           { name: 'REQUESITION_ID', width: 130, sortable: true },
                           { name: 'REQUESTED_DATE', width: 130, sortable: true }, 
                           { name: 'SPACE_ID', width: 130, sortable: true }, 
                           { name: 'REQUESTED_BY', width: 100, sortable: true }, 
                           { name: 'REQUEST_TYPE',  width: 100,   sortable: true },
                           { name: 'REQUEST',  width: 100,   sortable: true },
                           { name: 'DESCRIPTION',  width: 100,   sortable: true },
                           { name: 'ASSIGNED_TO', width: 90, sortable: true },
                           { name: 'CLOSED_TIME', width: 90, sortable: true }, 
                           { name: 'TOTAL_TIME',  width: 70,   sortable: true },
                            { name: 'DELAYED_BY', width: 90, sortable: true }, 
                            { name: 'SER_STATUS',  width: 90,   sortable: true }
                       
                           ],
        rowNum: 10,
        rowList: [10,50,100,150,200,250,300],
        pager: jQuery('#UserGridPager'),
        sortname: 'REQUESTED_DATE',
        viewrecords: true,
        sortorder: 'DESC'
        //caption: 'Report'
        }).navGrid('#UserGridPager',{view:false,edit:false,del:false,search:false, add:false,refresh:true}  
					);		
					
					// add custom button to export the data to excel
			jQuery("#UserGrid").jqGrid('navButtonAdd','#UserGridPager',
			{caption:'Export to Excel',buttonicon: "ui-icon-calculator", onClickButton : function () { 
            var csv_url = 'Exports/MaintenanceOthers.aspx?Mfromdate='+ FromDate +'&Mtodate='+ toDate +'&Mfromtime='+ FromTime +'&Mtotime='+ ToTime +''; jQuery("#UserGrid").jqGrid('excelExport',{url:csv_url});
            }
				    
		}); 		 
};	 