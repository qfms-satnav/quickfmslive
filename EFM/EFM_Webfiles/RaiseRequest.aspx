<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="RaiseRequest.aspx.vb" Inherits="EFM_EFM_Webfiles_RaiseRequest" Title="Raise Request" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/DateTimePicker.js" type="text/javascript" language="javascript" defer></script>
    <script type="text/javascript" defer>
        function checkDate(sender, args) {
            var toDate= new Date();
            toDate.setMinutes(0);
            toDate.setSeconds(0);
            toDate.setHours(0);
            toDate.setMilliseconds(0);
            if ( sender._selectedDate < toDate) {
              document.getElementById("<%= txtconvdate.ClientID %>").value = "";
                alert("You can't select day earlier than today!");
                 
               
                sender._selectedDate = toDate;
                //set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }
        }
    </script>
 <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
 <ContentTemplate>
    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Raise Request
             <hr align="center" width="60%" /></asp:Label>
                    &nbsp;
                    <br />
                </td>
            </tr>
        </table>
        <table width="85%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>Raise Request</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../Images/table_left_mid_bg.gif">
                    &nbsp;</td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                        ForeColor="" ValidationGroup="Val1" />
                    <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                    <table id="table2" cellspacing="0" cellpadding="1" width="100%" border="1">
                        <tr id="personal" runat="server">
                            <td colspan="4">
                                <table id="tpersonal" runat="server" cellspacing="0" cellpadding="0" width="100%"
                                    border="1">
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Name
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:TextBox ID="txtesname" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Designation
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:TextBox ID="txtdesig" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Department
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:TextBox ID="txtdept" runat="server" CssClass="clsTextField" Width="97%" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Contact Details<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtescontact"
                                                ValidationGroup="Val1" Display="NONE" ErrorMessage="Enter Contact Details "></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please Enter Numerics Only"
                                                Display="None" ControlToValidate="txtescontact" ValidationGroup="Val1" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:TextBox ID="txtescontact" runat="server" CssClass="clsTextField" Width="97%"
                                                MaxLength="10"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">
                                City<font class="clsNote"> *</font>
                                <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlCity"
                                    Display="NONE" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:DropDownList ID="ddlCity" runat="server" TabIndex="6" CssClass="clsComboBox"
                                    Width="99%" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="50%" style="height: 26px">
                                Service Raised For <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlType"
                                    Display="None" ErrorMessage="Select Location " ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" width="50%">
                                <asp:DropDownList ID="ddlType" runat="server" Width="97%" CssClass="clsComboBox"
                                    AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <table id="tdtls" runat="server" cellspacing="0" cellpadding="0" width="100%" border="1">
                                    <tr id="loc" runat="server">
                                        <td style="width: 50%; height: 26px" align="left">
                                            Select Building<font class="clsNote"> *</font>
                                            <asp:RequiredFieldValidator ID="rfvloc" runat="server" ValidationGroup="Val1" ControlToValidate="ddlloc"
                                                Display="NONE" ErrorMessage="Please Select Building" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                        <td style="width: 50%; height: 26px" align="left">
                                            <asp:DropDownList ID="ddlloc" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                Width="99%" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="Twr" runat="server">
                                        <td style="width: 50%; height: 26px" align="left">
                                            Select Tower<font class="clsNote"> *</font>
                                            <asp:RequiredFieldValidator ID="rfvtwr" runat="server" ValidationGroup="Val1" ControlToValidate="ddltwr"
                                                Display="NONE" ErrorMessage="Please Select Building" Enabled="true" InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                        <td style="width: 50%; height: 26px" align="left">
                                            <asp:DropDownList ID="ddltwr" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                Width="99%" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="flr" runat="Server">
                                        <td align="left" style="height: 26px; width: 50%">
                                            Floor<font class="clsNote"> *</font>
                                            <asp:RequiredFieldValidator ID="rfvfloor" runat="server" ControlToValidate="ddlfloor"
                                                Display="NONE" ErrorMessage="Please Select Floor" ValidationGroup="Val1" InitialValue="--Select--"
                                                Enabled="true"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlfloor" runat="server" TabIndex="9" CssClass="clsComboBox"
                                                AutoPostBack="true" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="wng" runat="server">
                                        <td align="left" style="height: 26px; width: 50%">
                                            Wing<font class="clsNote"> *</font>
                                            <asp:RequiredFieldValidator ID="rfvwing" runat="server" ControlToValidate="ddlwing"
                                                Display="NONE" ErrorMessage="Please Select Wing" ValidationGroup="Val1" InitialValue="--Select--"
                                                Enabled="true"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlwing" runat="server" TabIndex="10" CssClass="clsComboBox"
                                                AutoPostBack="true" Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr runat="Server" id="room">
                                        <td style="width: 50%; height: 26px" align="left">
                                            Location
                                            <font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Val1"
                                                ControlToValidate="ddlSpace" Display="NONE" ErrorMessage="Please Select SubLocation"
                                                Enabled="true" InitialValue="0"></asp:RequiredFieldValidator></td>
                                        <td style="width: 50%; height: 26px" align="left">
                                            <asp:DropDownList ID="ddlSpace" TabIndex="14" runat="server" CssClass="clsComboBox"
                                                Width="99%">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr id="tabcomm" runat="server">
                            <td colspan="4" style="height: 252px">
                                <table id="common" runat="server" cellpadding="1" cellspacing="0" width="100%" border="1">
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Service Category<font class="clsNote"> *</font>
                                            <asp:RequiredFieldValidator ID="rfvreq" runat="server" ControlToValidate="ddlReq"
                                                Display="NONE" ErrorMessage="Please Select Service request category" ValidationGroup="Val1"
                                                InitialValue="--Select--"></asp:RequiredFieldValidator></td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlReq" runat="server" TabIndex="11" CssClass="clsComboBox"
                                                Width="99%" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Service Type<font class="clsNote"> *</font>
                                            <asp:RequiredFieldValidator ID="rfvreqtype" runat="server" ControlToValidate="ddlreqtype"
                                                Display="NONE" ErrorMessage="Please Select Service request" ValidationGroup="Val1"
                                                InitialValue="--Select--" Enabled="true"></asp:RequiredFieldValidator></td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:DropDownList ID="ddlreqtype" runat="server" TabIndex="12" CssClass="clsComboBox"
                                                Width="99%" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Problem Description<font class="clsNote"> *</font>
                                            <asp:RequiredFieldValidator ID="rfvdesc" runat="server" ControlToValidate="txtProbDesc"
                                                ValidationGroup="Val1" Display="NONE" ErrorMessage="Problem Description Required"></asp:RequiredFieldValidator>
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:TextBox ID="txtProbDesc" runat="server" Width="97%" Rows="3" TabIndex="15" TextMode="MultiLine"
                                                MaxLength="10000"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            Convenient date when the personnel can visit to carry out the work<%--<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvconvdate" runat="server" ControlToValidate="txtconvdate"
                                                Display="NONE" ErrorMessage="Please Select Convenient date" ValidationGroup="Val1"
                                                Enabled="true"></asp:RequiredFieldValidator>--%></td>
                                        <td align="left" style="height: 26px; width: 50%;z-index:900;position:inherit;">
                                            <asp:TextBox ID="txtconvdate" runat="server" CssClass="clsTextField" Width="40%"
                                                ></asp:TextBox>
                                            <asp:Button ID="btnreload" CssClass="button" runat="server" Text="Reload" />
                                            <%-- <asp:Image ID="Image1" runat="server" Height="21px" ImageUrl="~/images/calen1.gif.PNG" />--%>
                                            <%--<cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd-MMM-yyyy"
                                                TargetControlID="txtconvdate" OnClientDateSelectionChanged="checkDate" PopupButtonID="txtconvdate">
                                            </cc1:CalendarExtender>--%>
                                            <%-- <asp:LinkButton ID="LinkButton2" runat="server">Refresh convinient time</asp:LinkButton>--%>
                                        </td>
                                    </tr>
                                    <tr id="tr1" runat="server">
                                        <td align="left" style="height: 26px; width: 50%">
                                            Convenient time when the personnel can visit to carry out the work
                                            <%--<font class="clsNote">
                                                *</font>
                                            <asp:RequiredFieldValidator ID="cfvHr" runat="server" ValidationGroup="Val1" ErrorMessage="Please Select Convenient time!"
                                                Display="None" ControlToValidate="cboHr" InitialValue="-HH--"></asp:RequiredFieldValidator>
                                           --%>
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%;">
                                            <asp:DropDownList ID="cboHr" runat="server" Width="48%" CssClass="clsComboBox">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">
                                            An email will be sent to EmailID.If you wish to receive SMS as well,Please Click
                                            Yes<font class="clsNote">*</font>
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:RadioButtonList AutoPostBack="True" OnSelectedIndexChanged="radlstactions_SelectedIndexChanged"
                                                ID="radlstactions" runat="server" CssClass="clsRadioButton">
                                                <asp:ListItem Value="0">Yes</asp:ListItem>
                                                <asp:ListItem Value="1" Selected="True">No</asp:ListItem>
                                            </asp:RadioButtonList>
                                            <%--    <asp:CheckBoxList ID="chklist" runat="server" TabIndex="16" RepeatDirection="Horizontal"
                                                AutoPostBack="True" OnSelectedIndexChanged="chklist_SelectedIndexChanged">
                                                <asp:ListItem Value="SMS">SMS</asp:ListItem>
                                              <asp:ListItem Value="Email">Email</asp:ListItem>
                                          </asp:CheckBoxList>--%>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="Mobile">
                                        <td align="left" style="height: 26px; width: 50%">
                                            Enter Mobile Number<font class="clsNote">*</font>
                                            <asp:RequiredFieldValidator ID="rfvSMSMobile" runat="server" ControlToValidate="txtSMSMobile"
                                                ValidationGroup="Val1" Display="NONE" ErrorMessage="Enter Mobile Number to send SMS"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="rgvmobile" runat="server" ErrorMessage="Please Enter Numerics Only"
                                                Display="None" ControlToValidate="txtSMSMobile" ValidationGroup="Val1" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                        </td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:TextBox ID="txtSMSMobile" runat="server" CssClass="clsTextField" MaxLength="10"
                                                Width="97%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="Email" visible="false">
                                        <td align="left" style="height: 26px; width: 50%">
                                            Enter Email ID<font class="clsNote">*</font>
                                            <%--<asp:RequiredFieldValidator ID="rfvSMSEmail" runat="server" ControlToValidate="txtSMSEmail"
                                                ValidationGroup="Val1" Display="NONE" ErrorMessage="Enter Email id "></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSMSEmail"
                                                Display="None" ErrorMessage="Please Ente Valid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                ValidationGroup="Val1"></asp:RegularExpressionValidator></td>
                                        <td align="left" style="height: 26px; width: 50%">
                                            <asp:TextBox ID="txtSMSEmail" runat="server" TextMode="MultiLine" Rows="3" MaxLength="1000"
                                                Width="97%"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr runat="server" id="btn">
                            <td colspan="6" align="center">
                                <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Submit" ValidationGroup="Val1"
                                    CausesValidation="true" TabIndex="17" />&nbsp;

                               
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="../../Images/table_right_mid_bg.gif">
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                <td background="../../images/table_bot_mid_bg.gif">
                    <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                <td>
                    <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
            </tr>
        </table>
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

