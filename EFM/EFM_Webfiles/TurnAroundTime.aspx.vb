﻿Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports MaintenanceRpt
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports Microsoft.Reporting.WebForms

Partial Class EFM_EFM_Webfiles_TurnAroundTime
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            bindGrid()
        End If
    End Sub

    Public Sub bindGrid()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("Uid")
        param(1) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(1).Value = ""
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("HDM_VIEWUSERMAINTENANCE_Report", param)
        Dim rds As New ReportDataSource()
        rds.Name = "TurnAroundTimeReqDS"
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/TurnAroundtimeReqReport.rdlc")
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        ReportViewer1.LocalReport.Refresh()

        ReportViewer1.LocalReport.EnableHyperlinks = True
    End Sub
    Protected Sub ReportViewer1_Drillthrough(sender As Object, e As DrillthroughEventArgs) Handles ReportViewer1.Drillthrough
        'Get OrderID that was clicked by 
        'user via e.Report.GetParameters()
        Dim DrillThroughValues As ReportParameterInfoCollection = e.Report.GetParameters()

        Dim thisConnectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString
        'Dim thatConnectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString

        'This is just to show you how to iterate 
        'through the collection if you have
        'multiple parameters values instead of a single parameter value.
        'To process multiple parameters values, 
        'concatenate d.Values[0] into a string with a delimiter.
        'Use the Split() method to  separate values 
        'into an array. Assign indivdual array element to
        'corresponding parameter array element.
        For Each d As ReportParameterInfo In DrillThroughValues
            Session("RPTParam") = d.Values(0).ToString().Trim()
        Next
        Dim localreport As LocalReport = DirectCast(e.Report, LocalReport)
        Dim Level1SearchValue As SqlParameter() = New SqlParameter(0) {}
        'Fill dataset for Level1.rdlc
        Dim thisConnection As New SqlConnection(thisConnectionString)
        Dim Level1DataSet As New System.Data.DataSet()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_MAINTENANCE_ACTIVITIES_report")
        sp.Command.AddParameter("@REQ_ID", Session("RPTParam"), DbType.String)
        Level1DataSet = sp.GetDataSet()

        If Level1DataSet.Tables(0).Rows.Count > 0 Then
            Dim level1datasource As New ReportDataSource("TurnAroundTimeReqDetailsDS", Level1DataSet.Tables(0))
            localreport.DataSources.Clear()
            localreport.DataSources.Add(level1datasource)
            localreport.Refresh()
            Dim rds1 As New ReportDataSource()
            rds1.Name = "TurnAroundTimeReqDetailsDS"
            'This refers to the dataset name in the RDLC file
            'rds.Value = ds.Tables(0)
            'ReportViewer2.Reset()
            'ReportViewer2.LocalReport.DataSources.Add(rds1)
            'ReportViewer2.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/HelpDesk_Mgmt/TurnAroundtimeReqSubReport.rdlc")
            'ReportViewer2.LocalReport.Refresh()
            'ReportViewer2.SizeToReportContent = True
            'ReportViewer2.Visible = True
            'ReportViewer2.LocalReport.EnableHyperlinks = True
        Else
        End If
    End Sub
End Class
