Imports System.Text
Imports System.Security.Cryptography
Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Partial Class ESP_ESP_Webfiles_frmEditProfile
    Inherits System.Web.UI.Page

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        UpdatePassword()
        UpdateProfile()
        UpdatePersonalProfile()
    End Sub
    Private Sub UpdatePassword()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_USER_PASSWORD")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@PWD", txtpwd.Text, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UpdateProfile()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_USERPROF_MODIFY")
        sp.Command.AddParameter("@id", Session("UID"), DbType.String)
        sp.Command.AddParameter("@AUR_FIRST_NAME", txtFrstName.Text, DbType.String)
        sp.Command.AddParameter("@AUR_MIDDLE_NAME", txtMdlName.Text, DbType.String)
        sp.Command.AddParameter("@AUR_LAST_NAME", txtLstName.Text, DbType.String)
        sp.Command.AddParameter("@AUR_EMAIL", txtEmail.Text, DbType.String)
        sp.Command.AddParameter("@AD_ID", txtITID.Text, DbType.String)
        sp.Command.AddParameter("@AUR_EXTENSION", txtExtn.Text, DbType.String)
        sp.Command.AddParameter("@AUR_DEP_ID", ddlDept.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
        sp.Command.AddParameter("@AUR_RES_NUMBER", txtRes.Text, DbType.String)
        'sp.Command.AddParameter("@AUR_GRADE", ddlGrade.SelectedItem.Value, DbType.String)
        If txtDOB.Text = "" Then
            sp.Command.AddParameter("@AUR_DOB", txtDOB.Text, DbType.Date)
        Else
            sp.Command.AddParameter("@AUR_DOB", txtDOB.Text, DbType.Date)
        End If
        If txtDOJ.Text = "" Then
            sp.Command.AddParameter("@AUR_DOJ", DBNull.Value, DbType.Date)
        Else
            sp.Command.AddParameter("@AUR_DOJ", txtDOJ.Text, DbType.Date)
        End If


        sp.Command.AddParameter("@AUR_REPORTING_TO", ddlRept.SelectedItem.Value, DbType.String)
        ' sp.Command.AddParameter("@AUR_TYPE", ddlEmpType.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub UpdatePersonalProfile()


        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_USER_PER_DET_MDFY")
        sp.Command.AddParameter("@id", Session("UID"), DbType.String)
        sp.Command.AddParameter("@AUR_FATHERS_NAME", txtFthrName.Text, DbType.String)
        sp.Command.AddParameter("@AUR_BLOOD_GROUP", txtBldGrp.Text, DbType.String)
        If txtDOA.Text = "" Then
            sp.Command.AddParameter("@AUR_ANNIVERSARY_DATE", DBNull.Value, DbType.Date)
        Else
            sp.Command.AddParameter("@AUR_ANNIVERSARY_DATE", txtDOA.Text, DbType.Date)
        End If

        sp.Command.AddParameter("@AUR_ADDRESS", txtAddr.Text, DbType.String)
        sp.Command.AddParameter("@AUR_EXP", txtExp.Text, DbType.String)
        sp.Command.AddParameter("@AUR_SKILLS", txtSkills.Text, DbType.String)
        sp.Command.AddParameter("@AUR_QUALIFICATION", txtQlfctn.Text, DbType.String)
        sp.Command.AddParameter("@AUR_PASSPORT_NO", txtPsptNo.Text, DbType.String)
        sp.Command.AddParameter("@AUR_PAN_CARD_NUMBER", txtPanNo.Text, DbType.String)
        sp.Command.AddParameter("@AUR_INSURANCE_NUMBER", txtInsNo.Text, DbType.String)
        sp.Command.AddParameter("@AUR_EMERGENCY_CONTACT_NO", txtEmerNo.Text, DbType.String)
        sp.Command.AddParameter("@AUR_SHIFT", ddlShift.SelectedItem.Value, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtITID.ReadOnly = True
            txtEmpID.ReadOnly = True
            txtDOB.Attributes.Add("onClick", "displayDatePicker('" + txtDOB.ClientID + "')")
            txtDOB.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            'txtDOJ.Attributes.Add("onClick", "displayDatePicker('" + txtDOJ.ClientID + "')")
            'txtDOJ.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txtDOA.Attributes.Add("onClick", "displayDatePicker('" + txtDOA.ClientID + "')")
            txtDOA.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            BindGrade()
            Get_Department()
            Get_Reporting()

            BindDetails()
            BindPersonalDetails()
        End If
    End Sub
    Private Sub BindGrade()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_EMPLOYEE_GRADE_GETDETAILS")
            ddlGrade.DataSource = sp.GetDataSet()
            ddlGrade.DataTextField = "GRADE_NAME"
            ddlGrade.DataValueField = "GRADE_CODE"
            ddlGrade.DataBind()
            ddlGrade.Items.Insert("0", New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Get_Reporting()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_USERS")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlRept.DataSource = sp.GetDataSet()
        ddlRept.DataTextField = "AUR_KNOWN_AS"
        ddlRept.DataValueField = "AUR_ID"
        ddlRept.DataBind()
        ddlRept.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub Get_Department()
        'Retreiving all the Departments from DEPARTMENT table
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_GET_DEPT")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlDept.DataSource = sp.GetDataSet()
        ddlDept.DataTextField = "DEP_NAME"
        ddlDept.DataValueField = "DEP_CODE"
        ddlDept.DataBind()
        ddlDept.Items.Insert(0, New ListItem("--Select--", "0"))

    End Sub
    Private Sub BindDetails()
        Try

            'Retrieving data from AMANTRA_USER for editing
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_USERPROF_EDIT")
            sp.Command.AddParameter("@id", Session("UID"), DbType.String)
            Dim ds3 As New DataSet
            ds3 = sp.GetDataSet()
            If ds3.Tables(0).Rows.Count > 0 Then
                txtFrstName.Text = ds3.Tables(0).Rows(0).Item("AUR_FIRST_NAME")
                txtMdlName.Text = ds3.Tables(0).Rows(0).Item("AUR_MIDDLE_NAME")
                txtLstName.Text = ds3.Tables(0).Rows(0).Item("AUR_LAST_NAME")
                txtEmail.Text = ds3.Tables(0).Rows(0).Item("AUR_EMAIL")
                txtExtn.Text = ds3.Tables(0).Rows(0).Item("AUR_EXTENSION")
                txtITID.Text = ds3.Tables(0).Rows(0).Item("AD_ID")
                txtRes.Text = ds3.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
                txtEmpID.Text = ds3.Tables(0).Rows(0).Item("AUR_ID")
                txtDOB.Text = ds3.Tables(0).Rows(0).Item("AUR_DOB")
                txtDOJ.Text = ds3.Tables(0).Rows(0).Item("AUR_DOJ")

                ddlDept.ClearSelection()
                ddlDept.Items.FindByValue(ds3.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True

                ddlStatus.ClearSelection()
                ddlStatus.Items.FindByValue(ds3.Tables(0).Rows(0).Item("AUR_STA_ID")).Selected = True

                ddlRept.ClearSelection()
                ddlRept.Items.FindByValue(ds3.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True

                ddlEmpType.ClearSelection()
                ddlEmpType.Items.FindByValue(ds3.Tables(0).Rows(0).Item("AUR_TYPE")).Selected = True

                ddlGrade.ClearSelection()
                ddlGrade.Items.FindByValue(ds3.Tables(0).Rows(0).Item("AUR_GRADE")).Selected = True
              

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPersonalDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AST_USER_PER_DET_EDIT")
        sp.Command.AddParameter("@id", Session("UID"), DbType.String)
        Dim ds3 As New DataSet
        ds3 = sp.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            txtFthrName.Text = ds3.Tables(0).Rows(0).Item("AUR_FATHERS_NAME")
            txtBldGrp.Text = ds3.Tables(0).Rows(0).Item("AUR_BLOOD_GROUP")
            txtDOA.Text = ds3.Tables(0).Rows(0).Item("AUR_ANNIVERSARY_DATE")
            txtAddr.Text = ds3.Tables(0).Rows(0).Item("AUR_ADDRESS")
            txtExp.Text = ds3.Tables(0).Rows(0).Item("AUR_EXP")
            txtSkills.Text = ds3.Tables(0).Rows(0).Item("AUR_SKILLS")
            txtQlfctn.Text = ds3.Tables(0).Rows(0).Item("AUR_QUALIFICATION")
            txtPsptNo.Text = ds3.Tables(0).Rows(0).Item("AUR_PASSPORT_NO")
            txtPanNo.Text = ds3.Tables(0).Rows(0).Item("AUR_PAN_CARD_NUMBER")
            txtInsNo.Text = ds3.Tables(0).Rows(0).Item("AUR_INSURANCE_NUMBER")
            txtEmerNo.Text = ds3.Tables(0).Rows(0).Item("AUR_EMERGENCY_CONTACT_NO")
            ddlShift.Items.Clear()
            BindShifts()
            ddlShift.ClearSelection()
            ddlShift.Items.FindByValue(ds3.Tables(0).Rows(0).Item("AUR_SHIFT")).Selected = True
        End If
    End Sub
    Private Sub BindShifts()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMANTRA_SHIFTS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)

        ddlShift.DataSource = sp.GetDataSet()
        ddlShift.DataTextField = "SHIFT_NAME"
        ddlShift.DataValueField = "SHIFT_ID"
        ddlShift.DataBind()
        ddlShift.Items.Insert(0, New ListItem("--Select Shift--", "--Select Shift--"))
    End Sub

    Protected Sub ddlDept_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDept.SelectedIndexChanged
        ddlShift.items.clear()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_AMANTRA_SHIFTS1")

        sp.Command.AddParameter("@DEP_ID", ddlDept.SelectedItem.Value, DbType.String)
        ddlShift.DataSource = sp.GetDataSet()
        ddlShift.DataTextField = "SHIFT_NAME"
        ddlShift.DataValueField = "SHIFT_ID"
        ddlShift.DataBind()
        ddlShift.Items.Insert(0, New ListItem("--Select Shift--", "--Select Shift--"))
    End Sub
End Class
