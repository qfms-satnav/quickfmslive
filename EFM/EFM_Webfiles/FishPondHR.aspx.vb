Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_FishPondHR
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            txtFishPondMsg.ReadOnly = True
            txtSuggestionType.ReadOnly = True
            Panel1.Visible = False
            BindApprovedMessages()

        End If
    End Sub
    Private Sub BindApprovedMessages()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_FISHPOND_GET_MESSAGES")
            gvapproved.DataSource = sp.GetDataSet()
            gvapproved.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_FISHPOND_HR_APPROVAL")
            gvitems.DataSource = sp.GetDataSet()
            gvitems.DataBind()
            For i As Integer = 0 To gvitems.Rows.Count - 1

                Dim lblstaid As LinkButton = CType(gvitems.Rows(i).FindControl("lblstaid"), LinkButton)
                If lblstaid.Text = 1 Then

                    gvitems.Columns(3).Visible = False

                Else
                    gvitems.Columns(3).Visible = True


                End If
            Next
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "Approve" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblsno As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblsno"), Label)
            Dim lblsuggestiontype As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblsuggestiontype"), Label)
            Dim lblremarks As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblremarks"), Label)
            Dim sno1 As Integer = lblsno.Text
            txtstore.Text = sno1
            txtSuggestionType.Text = lblsuggestiontype.Text
            txtFishPondMsg.Text = lblremarks.Text
            Panel1.Visible = True
        Else
            Panel1.Visible = False
            ClearFields()
        End If
    End Sub
    Private Sub DeleteFishPondMsg(ByVal sno As Integer)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_DEL_FISHPOND_EMPLOYEE")
            sp.Command.AddParameter("@SNO", sno, DbType.Int64)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub ClearFields()
        txtstore.Text = ""
        txtSuggestionType.Text = ""
        txtFishPondMsg.Text = ""
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ESP_FISHPOND_APPROVAL")
        sp.Command.AddParameter("@SNO", txtstore.Text, DbType.Int32)
        sp.Command.AddParameter("@HR_COMMENTS", txthrfishpondmsg.Text, DbType.String)
        sp.ExecuteScalar()
       
        Panel1.Visible = False
        ClearFields()
        BindGrid()
        BindApprovedMessages()
    End Sub

 

    Protected Sub gvapproved_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvapproved.PageIndexChanging
        gvapproved.PageIndex = e.NewPageIndex()
        BindApprovedMessages()
    End Sub

    Protected Sub gvapproved_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvapproved.RowCommand
        If e.CommandName = "Delete" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblsno1 As Label = DirectCast(gvapproved.Rows(rowIndex).FindControl("lblsno1"), Label)
            Dim lblsuggestiontype As Label = DirectCast(gvapproved.Rows(rowIndex).FindControl("lblsuggestiontype1"), Label)
            Dim lblremarks As Label = DirectCast(gvapproved.Rows(rowIndex).FindControl("lblremarks1"), Label)
            DeleteFishPondMsg(lblsno1.Text)
            BindGrid()
            BindApprovedMessages()
        End If
    End Sub

    Protected Sub gvapproved_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles gvapproved.RowDeleting
       
    End Sub
End Class
