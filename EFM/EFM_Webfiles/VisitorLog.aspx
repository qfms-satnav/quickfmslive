<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VisitorLog.aspx.vb" Inherits="ESP_ESP_Webfiles_VisitorLog" Title="Visitor Log" %>

<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div id="wrapper">
        <div id="page-wrapper" class="row">
            <div class="row form-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <fieldset>
                            <legend>Visitor Log
                            </legend>
                        </fieldset>
                        <form id="form1" class="form-horizontal well" runat="server">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label><br />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">From Date<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtfromdate" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">To Date<span style="color: red;">*</span></label>
                                            <div class="col-md-7">
                                                <div class='input-group date' id='todate'>
                                                    <asp:TextBox ID="txttoDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="row">
                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="GO" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                               <div class="col-md-12">                                 
                                        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                            CssClass="table table-condensed table-bordered table-hover table-striped" PageSize="10" EmptyDataText="No Visitor Log Found.">
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReqId" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_REQID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Visitor Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblname" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemail" runat="server" CssClass="bodyText" Text='<%#Eval("VISITOR_EMAIL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Mobile">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmobile" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_MOBILE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Whom to Meet">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblmeet" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_MEET") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="In-Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIntime" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_INTIME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Out-Time">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOuttime" runat="Server" CssClass="bodyText" Text='<%#Eval("VISITOR_OUTTIME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>                              
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
