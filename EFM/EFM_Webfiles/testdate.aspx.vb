﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class EFM_EFM_Webfiles_testdate
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            txtfromdate.Attributes.Add("onClick", "displayDatePicker('" + txtfromdate.ClientID + "')")
            txtfromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            txttoDate.Attributes.Add("onClick", "displayDatePicker('" + txttoDate.ClientID + "')")
            txttoDate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")

        End If


    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_VISITOR_LOG")

            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        Try
            If txtfromdate.Text <> "" And txttoDate.Text <> "" Then
                lblMsg.Text = ""
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_VISITOR_LOG1")
                sp.Command.AddParameter("@FROM_DATE", txtfromdate.Text, DbType.Date)
                sp.Command.AddParameter("@TO_DATE", txttoDate.Text, DbType.Date)
                gvItems.DataSource = sp.GetDataSet()
                gvItems.DataBind()
            Else
                lblMsg.Text = "Please Enter From and To Date"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
