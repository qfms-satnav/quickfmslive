Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class EFM_EFM_Webfiles_AssignUserSpace
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack() Then
            BindEmployee()
            'BindSpaceid()
            BindGrid()
            Get_Spacetype()
            btnsubmit.Visible = True
            btnmodify.Visible = False
            lblMsg.Text = ""
        End If
    End Sub
    Private Sub Get_Spacetype()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EFM_GET_SPACETYPE")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlType.DataSource = sp.GetDataSet()
        ddlType.DataTextField = "SPACE_LAYER"
        ddlType.DataValueField = "SPACE_TYPE"
        ddlType.DataBind()
        ddlType.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Sub BindEmployee()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_EMPLOYEE")
        Dim dummy As String = sp.Command.CommandSql
        ddlemp.DataSource = sp.GetDataSet()
        ddlemp.DataTextField = "AUR_KNOWN_AS"
        ddlemp.DataValueField = "AUR_ID"
        ddlemp.DataBind()
        ddlemp.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub BindSpaceid(ByVal space_type As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_SPACE_EMPLOYEE")
        sp.Command.AddParameter("@SPACE_TYPE", space_type, DbType.String)
        'Dim dummy As String = sp.Command.CommandSql
        liitems.DataSource = sp.GetDataSet()
        liitems.DataTextField = "SPC_NAME"
        liitems.DataValueField = "SPC_ID"
        liitems.DataBind()
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"bind_emp_space")
        Dim dummy As String = sp.Command.CommandSql
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If Page.IsValid = True Then
            Dim valid As Integer
            valid = ValidateEmployee()
            If valid = 0 Then
                Modify_User_Space()
                BindGrid()
                lblMsg.Text = "User Space Modified Succesfully"
            Else
                Assign_User_Space()
                BindGrid()
                lblMsg.Text = "User Space Added Succesfully"
            End If
            Cleardata()
            'BindSpaceid()
        End If
    End Sub
    Private Sub Cleardata()
        ddlemp.SelectedIndex = -1
        liitems.Items.Clear()
        ddlType.SelectedIndex = -1
    End Sub
    Private Function ValidateEmployee()
        Dim spacetype As Int32

        If ddlType.SelectedItem.Value = "OFFICE" Then
            spacetype = 1
        Else
            spacetype = 2
        End If

        Dim valid As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"EMPLOYEE_VALIDATE_SPACE")
        sp.Command.AddParameter("@AUR_ID", ddlemp.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SPACE_TYPE", spacetype, DbType.String)
        valid = sp.ExecuteScalar()
        Return valid
    End Function
    Private Sub Assign_User_Space()
        Dim spacetype As Int32

        'If ddlType.SelectedItem.Value = "OFFICE" Then
        '    spacetype = 1
        'Else
        '    spacetype = 2
        'End If

        For Each li As ListItem In liitems.Items
            If li.Selected = True Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADD_EMPLOYEE_SPACE")
                sp.Command.AddParameter("@AUR_ID", ddlemp.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@SPACE_TYPE", ddlType.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@SPACE_ID", li.Value, DbType.String)
                sp.ExecuteScalar()
            End If
        Next
    End Sub
    Private Sub Modify_User_Space()
        ' Dim spacetype As Int32

        'If ddlType.SelectedItem.Value = "OFFICE" Then
        '    spacetype = 1
        'Else
        '    spacetype = 2
        'End If

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"MODIFY_EMPLOYEE_SPACE")
        sp.Command.AddParameter("@AUR_ID", ddlemp.SelectedItem.Value, DbType.String)


        sp.Command.AddParameter("@SPACE_TYPE", ddlType.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
        Assign_User_Space()
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "Edit" Then
            lblMsg.Text = ""
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblempid As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblempid"), Label)
            Dim lblspctype As Label = DirectCast(gvitems.Rows(rowIndex).FindControl("lblspctype"), Label)
            BindDetails(lblempid.Text, lblspctype.Text)
        End If
    End Sub
    Private Sub BindDetails(ByVal emp As String, ByVal space As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_EMPLOYEE_SPACE_DETAILS")
        sp.Command.AddParameter("@AUR_ID", emp, DbType.String)
        sp.Command.AddParameter("@SPACE_TYPE", space, DbType.String)
        Dim ds As New DataSet()
        Dim dr As SqlDataReader
        dr = sp.GetReader()
        ddlemp.ClearSelection()
        ddlType.ClearSelection()
        BindSpaceid(space)
        liitems.ClearSelection()
        While dr.Read()
            ddlemp.Items.FindByValue(emp).Selected = True
            ddlType.Items.FindByValue(dr("SPACE_TYPE")).Selected = True
            liitems.Items.FindByValue(dr("SPACE_ID")).Selected = True
        End While
    End Sub

    Protected Sub gvitems_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs) Handles gvitems.RowEditing

    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedIndex > 0 Then
            BindSpaceid(ddlType.SelectedItem.Value)
        Else
            liitems.Items.Clear()
        End If
    End Sub

    Protected Sub btnsearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click

        Dim SEARCH As String = txtsearch.Text
        searchemployee(SEARCH)
       
    End Sub
    Private Sub searchemployee(ByVal SEARCH As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"bind_emp_space_search")
        sp.Command.AddParameter("@EMP_ID", SEARCH, DbType.String)
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
    End Sub

    Protected Sub gvitems_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gvitems.Sorting
        searchemployee("")
      

        Dim dsSortTable As DataSet = TryCast(gvitems.DataSource, DataSet)
        Dim dtSortTable As DataTable = dsSortTable.Tables(0)
        If dtSortTable IsNot Nothing Then
            Dim dvSortedView As New DataView(dtSortTable)
            dvSortedView.Sort = e.SortExpression + " " & getSortDirectionString()
            ViewState("sortExpression") = e.SortExpression
            gvitems.DataSource = dvSortedView
            gvitems.DataBind()
        End If



    End Sub

    Private Function getSortDirectionString() As String
        If ViewState("sortDirection") Is Nothing Then
            ViewState("sortDirection") = "ASC"
        Else
            If ViewState("sortDirection").ToString() = "ASC" Then
                ViewState("sortDirection") = "DESC"
                Return ViewState("sortDirection").ToString()
            End If
            If ViewState("sortDirection").ToString() = "DESC" Then
                ViewState("sortDirection") = "ASC"
                Return ViewState("sortDirection").ToString()
            End If
        End If
        Return ViewState("sortDirection").ToString()
    End Function

End Class
