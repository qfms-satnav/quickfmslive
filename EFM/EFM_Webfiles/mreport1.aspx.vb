Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO
Partial Class EFM_EFM_Webfiles_mreport1
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLOGOUT"))
        End If
        If Not IsPostBack() Then
            BindGrid()
            If gvitems.Rows.Count > 0 Then
                btnexport.Visible = True
            Else
                btnexport.Visible = False
            End If
        End If
    End Sub

    Private Sub BindGrid()
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@REQ", SqlDbType.NVarChar, 2000)
        param(0).Value = Request.QueryString("REQ")
        param(1) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        param(1).Value = Request.QueryString("FROMDATE")
        param(2) = New SqlParameter("@TODATE", SqlDbType.DateTime)
        param(2).Value = Request.QueryString("TODATE")
        param(3) = New SqlParameter("@FROMTIME", SqlDbType.NVarChar)
        param(3).Value = Request.QueryString("FROMTIME")
        param(4) = New SqlParameter("@TOTIME", SqlDbType.NVarChar)
        param(4).Value = Request.QueryString("TOTIME")
        Dim ds As New DataSet()
        ds = objsubsonic.GetSubSonicDataSet("HDM_GET_COLUMN_WISE_REPORT1", param)
        gvitems.DataSource = ds
        gvitems.DataBind()
    End Sub

    Protected Sub btnexport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnexport.Click
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@REQ", SqlDbType.NVarChar, 2000)
        param(0).Value = Request.QueryString("REQ")
        param(1) = New SqlParameter("@FROMDATE", SqlDbType.DateTime)
        param(1).Value = Request.QueryString("FROMDATE")
        param(2) = New SqlParameter("@TODATE", SqlDbType.DateTime)
        param(2).Value = Request.QueryString("TODATE")
        param(3) = New SqlParameter("@FROMTIME", SqlDbType.NVarChar)
        param(3).Value = Request.QueryString("FROMTIME")
        param(4) = New SqlParameter("@TOTIME", SqlDbType.NVarChar)
        param(4).Value = Request.QueryString("TOTIME")
        Dim ds As New DataSet()
        ds = objsubsonic.GetSubSonicDataSet("HDM_GET_COLUMN_WISE_REPORT1", param)
        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("Maintenance_Service_Report" & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddHHmmss") & ".xls", gv)
    End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Response.Redirect("MaintenanceReport1.aspx")
    End Sub

    Protected Sub gvitems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
