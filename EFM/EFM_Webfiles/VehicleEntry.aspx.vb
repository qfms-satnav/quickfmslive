Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_VehicleEntry
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_SP_GETVEHICLE_DETAILS")
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "PunchOut" Then
            Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim lblReqId As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblReqId"), Label)
            Dim SP As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMANTRA_PUNCH_OUT_VEHICLE")
            SP.Command.AddParameter("@REQ_ID", lblReqId.Text, DbType.String)
            SP.ExecuteScalar()
        End If
        BindGrid()
    End Sub
End Class
