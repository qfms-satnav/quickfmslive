<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="AssignUserSpace.aspx.vb" Inherits="EFM_EFM_Webfiles_AssignUserSpace"
    Title="Assign User Space" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="100%" align="center">
                            <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                                ForeColor="Black">User Space Master
             <hr align="center" width="60%" /></asp:Label>
                            &nbsp;
                    <br />
                        </td>
                    </tr>
                </table>
                <table width="85%" cellpadding="0" cellspacing="0" align="center" border="0">
                    <tr>
                        <td>
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                        <td width="100%" class="tableHEADER" align="left">&nbsp;<strong>User Space Master</strong></td>
                        <td style="width: 17px">
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                                ForeColor="" ValidationGroup="Val1" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            &nbsp; &nbsp; &nbsp;
                    <table id="tab" runat="Server" cellpadding="1" cellspacing="0" border="1" width="100%">
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">Select User <font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="rfvemp" runat="server" Display="None" ErrorMessage="Please Select Employee"
                                    ValidationGroup="Val1" ControlToValidate="ddlemp" InitialValue="--Select--">

                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:DropDownList ID="ddlemp" runat="server" CssClass="clsComboBox" Width="97%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="height: 26px; width: 50%">Select Space Type<font class="clsNote">*</font>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="None" ErrorMessage="Please Select Employee"
                                    ValidationGroup="Val1" ControlToValidate="ddlemp" InitialValue="--Select--">

                                </asp:RequiredFieldValidator>
                            </td>
                            <td align="left" style="height: 26px; width: 50%">
                                <asp:DropDownList ID="ddlType" runat="server" Width="97%" CssClass="clsComboBox" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" style="width: 50%">Select Space
                            </td>
                            <td align="left" style="width: 50%">
                                <asp:ListBox ID="liitems" runat="server" Width="97%" SelectionMode="Multiple" Rows="5"></asp:ListBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnsubmit" runat="server" CssClass="button" Text="Submit" CausesValidation="true"
                                    ValidationGroup="Val1" />
                                <asp:Button ID="btnmodify" runat="server" CssClass="button" Text="Modify" CausesValidation="true"
                                    ValidationGroup="Val1" />
                            </td>
                        </tr>
                    </table>
                            <asp:Panel ID="panl1" runat="Server" Width="100%">
                                <table runat="server" cellpadding="0" cellspacing="0" border="0">
                                    <tr>
                                        <td align="left" style="height: 26px; width: 50%">Search by Employee ID
                                        </td>
                                        <td align="center" style="height: 26px; width: 50%">
                                            <asp:TextBox ID="txtsearch" runat="server" CssClass="clsTextfield" Width="45%"></asp:TextBox></td>
                                        <td align="center" style="height: 26px; width: 50%">
                                            <asp:Button ID="btnsearch" runat="server" CssClass="button" Text="Search" />
                                        </td>

                                    </tr>
                                </table>

                                <asp:GridView ID="gvitems" runat="server" AllowSorting="true" AllowPaging="true"
                                    AutoGenerateColumns="false" Width="100%">
                                    <Columns>
                                        <asp:TemplateField visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblempid" runat="server" Text='<%#Eval("AUR_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle Width="30%" />
                                        </asp:TemplateField>
										<asp:TemplateField HeaderText="Staff/StudentId" SortExpression="EMPLOYEE_ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblempid1" runat="server" Text='<%#Eval("EMPLOYEE_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle Width="30%" />
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="Space ID">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspcid" runat="server" Text='<%#Eval("SPC_VIEW_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="40%" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Space Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblspctype" runat="server" Text='<%#Eval("SPACE_TYPE") %>'></asp:Label>
                                            </ItemTemplate>
                                               <ItemStyle Width="30%" />
                                        </asp:TemplateField>
                                     
                                        <asp:ButtonField CommandName="Edit" Text="Edit" />
                                    </Columns>
                                </asp:GridView>
                            </asp:Panel>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td background="../../images/table_bot_mid_bg.gif">
                            <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td>
                            <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>



    </asp:UpdatePanel>

</asp:Content>
