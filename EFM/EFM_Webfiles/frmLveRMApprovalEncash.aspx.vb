Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_frmLveRMApprovalEncash
    Inherits System.Web.UI.Page

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            Dim REQID As String = Request.QueryString("ReqID")
            APPROVE_LEAVE_ENCASH(REQID)
            UPDATE_LEAVE_BALANCE()
            MAIL_LVEENCASHAPPROVED(REQID)
            Response.Redirect("status.aspx?StaId=3&ReqId=" + REQID)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Try
            Dim REQID As String = Request.QueryString("ReqID")
            REJECT_LEAVE_ENCASH(REQID)
            MAIL_LVEENCASHREJECTED(REQID)

            Response.Redirect("status.aspx?StaId=3&ReqId=" + REQID)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub UPDATE_LEAVE_BALANCE()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_UPDATE_LEAVE_BALANCE")
            sp.Command.AddParameter("@AUR_ID", txtAssociateID.Text, DbType.String)
            sp.Command.AddParameter("@LEAVE_TYPE", ddlLeave.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@DEPT", ddlDep.SelectedItem.Value, DbType.String)
            sp.Command.AddParameter("@LEAVE_BALANCE", txtNoLeaves.Text, DbType.Decimal)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub APPROVE_LEAVE_ENCASH(ByVal REQID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_ENCASH_APPROVED")
            sp.Command.AddParameter("@REQID", REQID, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub REJECT_LEAVE_ENCASH(ByVal REQID As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_ENCASH_REJECTED")
            sp.Command.AddParameter("@REQID", REQID, DbType.String)
            sp.ExecuteScalar()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDep()
            Dept()
            BindRM()
            BindLeaveType()
            BindDetails()
            BindEncashDetails()
            BindLeaves()
            BindCompLeaves()
            ddlDep.Enabled = False
            ddlRM.Enabled = False
            ddlLeave.Enabled = False
            txtAssociateID.ReadOnly = True
            txtAssociateName.ReadOnly = True
            txtCompLeaves.ReadOnly = True
            txtContactNo.ReadOnly = True
            txtContactNo.Text = 30
            txtDesig.ReadOnly = True
            txtNoLeaves.ReadOnly = True
            txtPleaves.ReadOnly = True
            ddlEncash.Enabled = False
            txtEncash.ReadOnly = True
        End If
    End Sub
    Private Sub BindDep()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_DEP")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlDep.DataSource = sp.GetDataSet()
            ddlDep.DataTextField = "DEP_NAME"
            ddlDep.DataValueField = "DEP_CODE"
            ddlDep.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub Dept()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_BIND_USER_DEPT")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRM()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"BIND_EMPLOYEES_RM")
            'sp.Command.AddParameter("@dep_code", ddlDep.SelectedItem.Value, DbType.String)
            ddlRM.DataSource = sp.GetDataSet()
            ddlRM.DataTextField = "AUR_FIRST_NAME"
            ddlRM.DataValueField = "AUR_ID"
            ddlRM.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMANTRA_USER_GETASSOCIATE_DETAILS2")
            sp.Command.AddParameter("@REQ_ID", Request.QueryString("ReqId"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtAssociateName.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
                txtAssociateID.Text = ds.Tables(0).Rows(0).Item("AUR_NO")
                ddlDep.ClearSelection()
                ddlDep.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_DEP_ID")).Selected = True
                txtDesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
                'ddlRM.ClearSelection()
                ddlRM.Items.FindByValue(ds.Tables(0).Rows(0).Item("AUR_REPORTING_TO")).Selected = True

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLeaveType()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_TYPE_GETLEAVE_TYPE1")
            sp.Command.AddParameter("@REQ_ID", Request.QueryString("ReqId"), DbType.String)
            ddlLeave.DataSource = sp.GetDataSet()
            ddlLeave.DataTextField = "LVE_LEAVE_TYPE"
            ddlLeave.DataValueField = "LVE_LEAVE_ID"
            ddlLeave.DataBind()

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLeaves()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_BALANCE_LEAVES")
            sp.Command.AddParameter("@AUR_ID", txtAssociateID.Text, DbType.String)
            sp.Command.AddParameter("@DEP_ID", ddlDep.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            txtPleaves.Text = ds.Tables(0).Rows(0).Item("LEAVES")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindCompLeaves()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_GET_BALANCE_COMP_LEAVES")
            sp.Command.AddParameter("@AUR_ID", txtAssociateID.Text, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()
            txtCompLeaves.Text = ds.Tables(0).Rows(0).Item("COMPLEAVES")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindEncashDetails()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_ENCASH_GETDETAILS")
            sp.Command.AddParameter("@REQID", Request.QueryString("ReqID"), DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.tables(0).rows.count > 0 Then
                ddlEncash.Items.FindByValue(ds.Tables(0).Rows(0).Item("LVE_ENCASH_TYPE")).Selected = True
                ddlLeave.Items.FindByValue(ds.Tables(0).Rows(0).Item("LVE_LEAVETYPE")).Selected = True
                txtEncash.Text = ds.Tables(0).Rows(0).Item("LVE_ENCASH_DAYS")
                txtNoLeaves.Text = ds.Tables(0).Rows(0).Item("LVE_BALANCE")
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub MAIL_LVEENCASHAPPROVED(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays As String
        strSubj = "Leave Encashment Approved."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_ENCASH_MAIL_REQUEST1")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strReqId = ds.Tables(0).Rows(0).Item("LVE_REQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LVE_REQUESTED_ON")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LVE_ENCASH_DAYS")
        Dim strfrommail, strfromname As String

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "amantraadmin"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "amantraadmin"




        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of Approved Leave Encashment." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_ENCASH")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Leave Encashment Requisition and is pending for Approval." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 
        strSubj = " Approved Leave Encashment Requisition - by " & strAppEmpName & noofdays & " days."
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub
    Public Sub Send_Mail(ByVal MailTo As String, ByVal MailFrom As String, ByVal Id As String, ByVal subject As String, ByVal msg As String, ByVal strHrEmail As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMT_MAIL_ADD")
        sp.Command.AddParameter("@AMT_ID", Id, DbType.String)
        sp.Command.AddParameter("@AMT_MESSAGE", msg, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_TO", MailTo, DbType.String)
        sp.Command.AddParameter("@AMT_SUBJECT", subject, DbType.String)
        sp.Command.AddParameter("@AMT_FLAG", "Submitted", DbType.String)
        sp.Command.AddParameter("@AMT_TYPE", "Normal Mail", DbType.String)
        sp.Command.AddParameter("@AMT_FROM", MailFrom, DbType.String)
        sp.Command.AddParameter("@AMT_MAIL_CC", strHrEmail, DbType.String)
        sp.Command.AddParameter("@AMT_BDYFRMT", 0, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Sub MAIL_LVEENCASHREJECTED(ByVal REQID As String)
        Dim strSubj As String                              'To hold the subject of the mail
        Dim strMsg As String                               'To hold the user message of mail
        Dim strMsgAdm As String                            'TO hold the Admin message of mail
        Dim strMsgStr As String                            'To hold the sql query
        Dim strReqId As String                             'To hold the Requisition ID
        Dim strReqDate As String                           'To hold the requisition date
        Dim strEmpName As String                           'To hold the employee name
        Dim strEmpEmail As String                          'To hold the employee e-mail
        Dim strAppEmpName As String                        'To hold the approval authority name
        Dim strAppEmpEmail As String                       'To hold the approval authority email
        Dim strLeaveType As String                          'To hold the Leave Type
        Dim strHrEmail As String                              'To hold the HR Email
        Dim noofdays As String
        strSubj = "Leave Encashment Rejected."
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_LMS_LEAVE_ENCASH_MAIL_REQUEST2")
        sp.Command.AddParameter("@LMS_LEAVE_REQ", REQID, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        strReqId = ds.Tables(0).Rows(0).Item("LVE_REQ_ID")
        strReqDate = ds.Tables(0).Rows(0).Item("LVE_REQUESTED_ON")
        strEmpName = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strEmpEmail = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        noofdays = ds.Tables(0).Rows(0).Item("LVE_ENCASH_DAYS")
        Dim strfrommail, strfromname As String

        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USER_MAIL")
        sp1.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim ObjDR As SqlDataReader
        ObjDR = sp.GetReader()
        While ObjDR.Read
            strfrommail = "amantraadmin@satnavtech.com"
            strfromname = "amantraadmin"
        End While
        ObjDR.Close()
        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "amantraadmin"




        'Mail message for the Requisitioner. 


        strMsg = "Dear " & strEmpName & "," & "<br>" & "<br>" & _
"This is to inform you that following are the  Details of Leave Encashment Rejected." & "<br>" & _
"<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
"<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
"<td width=50% align=left>" & strEmpName & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
"<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"Thanking You, " & "<br><br>" & _
"Regards, " & "<br>" & strfromname & "." & "<br>"
        ' Response.Write(strMsg)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL")
        sp2.Command.AddParameter("@REQID", REQID, DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        'Mail message for the Admin. 
        strMsgAdm = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request by " & strEmpName & " for Leave Encashment Requisition and is pending for Approval." & "<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & strEmpName & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requisition ID: </U></td>" & _
                    "<td width=50% align=left>" & strReqId & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date of Requisition: </U></td>" & _
"<td width=50% align=left>" & strReqDate & "</td></tr>" & _
"<tr><td width=50% align=right ><U>No of Days: </U></td>" & _
"<td width=50% align=left>" & noofdays & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Date(s): </U></td>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strEmpName & "." & "<br>"
        ''"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'objCom.Dispose()
        'Mail  for the Requisitioner. 
        strSubj = " Rejected Leave Encashment Requisition - by " & strAppEmpName & noofdays & " days."
        If strEmpEmail <> "" Then
            Send_Mail(strEmpEmail, strfrommail, strReqId, strSubj, strMsg, strHrEmail)
        End If
        If strAppEmpEmail <> "" Then
            Send_Mail(strAppEmpEmail, strEmpEmail, strReqId, strSubj & " - " & strEmpName, strMsgAdm, strHrEmail)
        End If
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmConvReports.aspx")
    End Sub
End Class
