Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class ESP_ESP_Webfiles_HRApprovalRaiseRequest
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            lblMsg.Text = ""
            pnl1.Visible = False
            pnl2.Visible = False
            Dim id As String = Request.QueryString("id")
            BindDetails(id)
            lblHead.Text = "HR Approval for" & ddlIdType.SelectedItem.Text & "Request <br>"
            chkterms.Checked = True
            chkterms.Enabled = False
            chkbusterms.Checked = True
            chkbusterms.Enabled = False
            ddlIdType.Enabled = False
            txtAssociateFirstName.ReadOnly = True
            txtAssociateMiddleName.ReadOnly = True
            txtAssociatesurName.ReadOnly = True
            txtdoj.ReadOnly = True
            txtAssociateNo.ReadOnly = True
            txtdep.ReadOnly = True
            txtBG.ReadOnly = True
            txtphone.ReadOnly = True
            txtmobile.ReadOnly = True
            txtdesig.ReadOnly = True
            txtEmail.ReadOnly = True
            txtqty.ReadOnly = True
            txtassname.ReadOnly = True
        End If
    End Sub
    Private Sub BindDetails(ByVal id As String)
        Try
            Dim card_type As String = ""

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_GET_ID_REQUESTS")
            sp.Command.AddParameter("@REQ_ID", id, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            ddlIdType.Items.FindByValue(ds.Tables(2).Rows(0).Item("CARD_TYPE")).Selected = True
            card_type = ds.Tables(2).Rows(0).Item("CARD_TYPE")
            If card_type = "Associate" Then
                pnl1.Visible = True
                pnl2.Visible = False
            Else
                pnl1.Visible = False
                pnl2.Visible = True
            End If
            txtAssociateFirstName.Text = ds.Tables(1).Rows(0).Item("AUR_FIRST_NAME")
            txtAssociateMiddleName.Text = ds.Tables(1).Rows(0).Item("AUR_MIDDLE_NAME")
            txtAssociatesurName.Text = ds.Tables(1).Rows(0).Item("AUR_LAST_NAME")
            txtdoj.Text = ds.Tables(1).Rows(0).Item("AUR_DOJ")
            txtAssociateNo.Text = ds.Tables(1).Rows(0).Item("AUR_NO")
            txtdep.Text = ds.Tables(1).Rows(0).Item("AUR_DEP_ID")
            txtBG.Text = ds.Tables(1).Rows(0).Item("AUR_BLOOD_GROUP")
            txtphone.Text = ds.Tables(0).Rows(0).Item("AUR_EXTENSION")
            txtmobile.Text = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
            txtdesig.Text = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
            txtEmail.Text = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
            txtqty.Text = ds.Tables(2).Rows(0).Item("AUR_QTY")
            txtassname.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
        Approve_Idcard()
        MAIL_APPROVE()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks2.aspx?id=67")
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        Reject_IDCard()
        MAIL_REJECT()
        Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks2.aspx?id=68")
    End Sub

    Protected Sub lbtnterms_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnterms.Click
        Response.Write("<script language=javascript>javascript:window.open('Files/IDcards.pdf','Window','toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450')</script>")
    End Sub

    Protected Sub lbtnbus1terms_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnbus1terms.Click
        Response.Write("<script language=javascript>javascript:window.open('Files/BusinessCards.pdf','Window','toolbar=no,Scrollbars=Yes,resizable=yes,statusbar=yes,top=0,left=0,width=690,height=450')</script>")
    End Sub
    Private Sub Approve_idcard()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"APPROVE_ID_CARD")
        sp.Command.AddParameter("@REQUEST_ID", Request.QueryString("id"), DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub Reject_IDCard()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"REJECT_ID_CARD")
        sp.Command.AddParameter("@REQUEST_ID", Request.QueryString("id"), DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub MAIL_APPROVE()
        Dim strSubj As String = ""
        Dim Associate_FirstName As String = ""
        Dim Associate_MiddleName As String = ""
        Dim Associate_SurName As String = ""
        Dim Associate_DOJ As String = ""
        Dim Associate_No As String = ""
        Dim Associate_Department As String = ""
        Dim Associate_BG As String = ""
        Dim Associate_Phone As String = ""
        Dim Associate_Mobile As String = ""
        Dim Associate_Email As String = ""
        Dim Associate_Quantity As Integer = 0
        Dim Associate_Designation As String = ""
        Dim strfrommail As String = ""
        Dim strfromname As String = ""
        Dim id_type As String = ""
        Dim strMsg As String = ""
        Dim strAppEmpName As String = ""
        Dim strAppEmpEmail As String = ""
        Dim strHrEmail As String = ""
        Dim card_type As String = ""
        Dim Associate_Name As String = ""
        Dim StrHrName As String = ""
        Dim strMsg1 As String = ""

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_GET_ID_REQUESTS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()

        Associate_FirstName = ds.Tables(1).Rows(0).Item("AUR_FIRST_NAME")
        Associate_MiddleName = ds.Tables(1).Rows(0).Item("AUR_MIDDLE_NAME")
        Associate_SurName = ds.Tables(1).Rows(0).Item("AUR_LAST_NAME")
        Associate_DOJ = ds.Tables(1).Rows(0).Item("AUR_DOJ")
        Associate_No = ds.Tables(1).Rows(0).Item("AUR_NO")
        Associate_Department = ds.Tables(1).Rows(0).Item("AUR_DEP_ID")
        Associate_BG = ds.Tables(1).Rows(0).Item("AUR_BLOOD_GROUP")
        Associate_Phone = ds.Tables(0).Rows(0).Item("AUR_EXTENSION")
        Associate_Mobile = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
        Associate_Designation = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
        Associate_Email = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        Associate_Quantity = ds.Tables(2).Rows(0).Item("AUR_QTY")
        card_type = ds.Tables(2).Rows(0).Item("CARD_TYPE")
        Associate_Name = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")


        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "EmployeeServiceDesk"
        strSubj = "Request for" & ddlIdType.SelectedItem.Text & "<br>"

        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_IDCARD")
        sp2.Command.AddParameter("@REQID", Request.QueryString("id"), DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        StrHrName = DS2.Tables(0).Rows(0).Item("HR_NAME")
        If card_type = "Associate" Then
            strMsg1 = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                                   "This is to inform you that the Details of Your Approved" & ddlIdType.SelectedItem.Text & " Request  <br>" & _
                                "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor FirstName: </U></td>" & _
                                "<td width=50% align=left>" & Associate_FirstName & "</td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor Middle Name: </U></td>" & _
                                "<td width=50% align=left>" & Associate_MiddleName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor Sur Name </U></td>" & _
            "<td width=50% align=left>" & Associate_SurName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor ID: </U></td>" & _
            "<td width=50% align=left>" & Associate_No & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Date of Joining:</U></td>" & _
            "<td width=50% align=left> " & Associate_DOJ & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Blood Group </U></td>" & _
            "<td width=50% align=left>" & Associate_BG & "</td></tr>" & _
            "<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
             "Thanking You " & "<br><br>" & _
                    "Yours Sincerely, " & "<br>" & strfromname & "." & "<br>"

            strMsg = "Dear " & StrHrName & "," & "<br>" & "<br>" & _
                                   "This is to inform you that following request for" & ddlIdType.SelectedItem.Text & " by " & strAppEmpName & "has been Approved<br>" & _
                                "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor FirstName: </U></td>" & _
                                "<td width=50% align=left>" & Associate_FirstName & "</td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor Middle Name: </U></td>" & _
                                "<td width=50% align=left>" & Associate_MiddleName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor Sur Name </U></td>" & _
            "<td width=50% align=left>" & Associate_SurName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor ID: </U></td>" & _
            "<td width=50% align=left>" & Associate_No & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Date of Joining:</U></td>" & _
            "<td width=50% align=left> " & Associate_DOJ & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Blood Group </U></td>" & _
            "<td width=50% align=left>" & Associate_BG & "</td></tr>" & _
            "<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
             "Thanking You " & "<br><br>" & _
                    "Yours Sincerely, " & "<br>" & strAppEmpName & "." & "<br>"
        Else
            strMsg1 = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that the Details of Your Approved" & ddlIdType.SelectedItem.Text & " Request <br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Name & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Designation: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Designation & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Phone: </U></td>" & _
"<td width=50% align=left>" & Associate_Phone & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Mobile: </U></td>" & _
"<td width=50% align=left>" & Associate_Mobile & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Email: </U></td>" & _
"<td width=50% align=left>" & Associate_Email & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Quantity: </U></td>" & _
"<td width=50% align=left>" & Associate_Quantity & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strfromname & "." & "<br>"
            strMsg = "Dear " & StrHrName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request for" & ddlIdType.SelectedItem.Text & " by " & strAppEmpName & "has been Approved<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Name & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Designation: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Designation & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Phone: </U></td>" & _
"<td width=50% align=left>" & Associate_Phone & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Mobile: </U></td>" & _
"<td width=50% align=left>" & Associate_Mobile & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Email: </U></td>" & _
"<td width=50% align=left>" & Associate_Email & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Quantity: </U></td>" & _
"<td width=50% align=left>" & Associate_Quantity & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strAppEmpName & "." & "<br>"
        End If
        Response.Write(strMsg)
        '"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'If strAppEmpEmail <> "" Then
        '    Send_Mail(strAppEmpEmail,strHrEmail,  REQID, strSubj & " - " & strAppEmpName, strMsg1)
        'End If
        'If strHrEmail <> "" Then
        '    Send_Mail(strHrEmail, strAppEmpEmail, REQID, strSubj & " - " & strAppEmpName, strMsg)
        'End If
    End Sub
    Private Sub MAIL_REJECT()
        Dim strSubj As String = ""
        Dim Associate_FirstName As String = ""
        Dim Associate_MiddleName As String = ""
        Dim Associate_SurName As String = ""
        Dim Associate_DOJ As String = ""
        Dim Associate_No As String = ""
        Dim Associate_Department As String = ""
        Dim Associate_BG As String = ""
        Dim Associate_Phone As String = ""
        Dim Associate_Mobile As String = ""
        Dim Associate_Email As String = ""
        Dim Associate_Quantity As Integer = 0
        Dim Associate_Designation As String = ""
        Dim strfrommail As String = ""
        Dim strfromname As String = ""
        Dim id_type As String = ""
        Dim strMsg As String = ""
        Dim strAppEmpName As String = ""
        Dim strAppEmpEmail As String = ""
        Dim strHrEmail As String = ""
        Dim card_type As String = ""
        Dim Associate_Name As String = ""
        Dim StrHrName As String = ""
        Dim strMsg1 As String = ""

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AXIS_SP_GET_ID_REQUESTS")
        sp.Command.AddParameter("@REQ_ID", Request.QueryString("id"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()

        Associate_FirstName = ds.Tables(1).Rows(0).Item("AUR_FIRST_NAME")
        Associate_MiddleName = ds.Tables(1).Rows(0).Item("AUR_MIDDLE_NAME")
        Associate_SurName = ds.Tables(1).Rows(0).Item("AUR_LAST_NAME")
        Associate_DOJ = ds.Tables(1).Rows(0).Item("AUR_DOJ")
        Associate_No = ds.Tables(1).Rows(0).Item("AUR_NO")
        Associate_Department = ds.Tables(1).Rows(0).Item("AUR_DEP_ID")
        Associate_BG = ds.Tables(1).Rows(0).Item("AUR_BLOOD_GROUP")
        Associate_Phone = ds.Tables(0).Rows(0).Item("AUR_EXTENSION")
        Associate_Mobile = ds.Tables(0).Rows(0).Item("AUR_RES_NUMBER")
        Associate_Designation = ds.Tables(0).Rows(0).Item("AUR_DESGN_ID")
        Associate_Email = ds.Tables(0).Rows(0).Item("AUR_EMAIL")
        Associate_Quantity = ds.Tables(2).Rows(0).Item("AUR_QTY")
        card_type = ds.Tables(2).Rows(0).Item("CARD_TYPE")
        Associate_Name = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")


        strfrommail = "amantraadmin@satnavtech.com"
        strfromname = "EmployeeServiceDesk"
        strSubj = "Request for" & ddlIdType.SelectedItem.Text & "<br>"

        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"ADMIN_MAIL_IDCARD")
        sp2.Command.AddParameter("@REQID", Request.QueryString("id"), DbType.String)

        Dim DS2 As New DataSet()
        DS2 = sp2.GetDataSet()
        strAppEmpName = DS2.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        strAppEmpEmail = DS2.Tables(0).Rows(0).Item("AUR_EMAIL")
        strHrEmail = DS2.Tables(0).Rows(0).Item("HR_EMAIL")
        StrHrName = DS2.Tables(0).Rows(0).Item("HR_NAME")
        If card_type = "Associate" Then
            strMsg1 = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                                   "This is to inform you that the Details of Your Rejected" & ddlIdType.SelectedItem.Text & " Request  <br>" & _
                                "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor FirstName: </U></td>" & _
                                "<td width=50% align=left>" & Associate_FirstName & "</td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor Middle Name: </U></td>" & _
                                "<td width=50% align=left>" & Associate_MiddleName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor Sur Name </U></td>" & _
            "<td width=50% align=left>" & Associate_SurName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor ID: </U></td>" & _
            "<td width=50% align=left>" & Associate_No & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Date of Joining:</U></td>" & _
            "<td width=50% align=left> " & Associate_DOJ & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Blood Group </U></td>" & _
            "<td width=50% align=left>" & Associate_BG & "</td></tr>" & _
            "<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
             "Thanking You " & "<br><br>" & _
                    "Yours Sincerely, " & "<br>" & strfromname & "." & "<br>"

            strMsg = "Dear " & StrHrName & "," & "<br>" & "<br>" & _
                                   "This is to inform you that following request for" & ddlIdType.SelectedItem.Text & " by " & strAppEmpName & "has been Rejected<br>" & _
                                "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor FirstName: </U></td>" & _
                                "<td width=50% align=left>" & Associate_FirstName & "</td></tr>" & _
                                "<tr><td width=50% align=right ><U>Requestor Middle Name: </U></td>" & _
                                "<td width=50% align=left>" & Associate_MiddleName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor Sur Name </U></td>" & _
            "<td width=50% align=left>" & Associate_SurName & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Requestor ID: </U></td>" & _
            "<td width=50% align=left>" & Associate_No & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Date of Joining:</U></td>" & _
            "<td width=50% align=left> " & Associate_DOJ & "</td></tr>" & _
            "<tr><td width=50% align=right ><U>Blood Group </U></td>" & _
            "<td width=50% align=left>" & Associate_BG & "</td></tr>" & _
            "<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
             "Thanking You " & "<br><br>" & _
                    "Yours Sincerely, " & "<br>" & strAppEmpName & "." & "<br>"
        Else
            strMsg1 = "Dear " & strAppEmpName & "," & "<br>" & "<br>" & _
                    "This is to inform you that the Details of Your Rejected" & ddlIdType.SelectedItem.Text & " Request <br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Name & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Designation: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Designation & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Phone: </U></td>" & _
"<td width=50% align=left>" & Associate_Phone & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Mobile: </U></td>" & _
"<td width=50% align=left>" & Associate_Mobile & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Email: </U></td>" & _
"<td width=50% align=left>" & Associate_Email & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Quantity: </U></td>" & _
"<td width=50% align=left>" & Associate_Quantity & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strfromname & "." & "<br>"
            strMsg = "Dear " & StrHrName & "," & "<br>" & "<br>" & _
                    "This is to inform you that following request for" & ddlIdType.SelectedItem.Text & " by " & strAppEmpName & "has been Rejected<br>" & _
                    "<table width=50%><tr><td colspan=2><hr width=100%></td></tr>" & _
                    "<tr><td width=50% align=right ><U>Requestor Name: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Name & "</td></tr>" & _
                    "<tr><td width=50% align=right ><U>Designation: </U></td>" & _
                    "<td width=50% align=left>" & Associate_Designation & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Phone: </U></td>" & _
"<td width=50% align=left>" & Associate_Phone & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Mobile: </U></td>" & _
"<td width=50% align=left>" & Associate_Mobile & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Email: </U></td>" & _
"<td width=50% align=left>" & Associate_Email & "</td></tr>" & _
"<tr><td width=50% align=right ><U>Quantity: </U></td>" & _
"<td width=50% align=left>" & Associate_Quantity & "</td></tr>" & _
"<tr><td colspan=2><hr width=100%></td></tr></table>" & "<br>" & _
 "Thanking You " & "<br><br>" & _
        "Yours Sincerely, " & "<br>" & strAppEmpName & "." & "<br>"
        End If
        Response.Write(strMsg)
        '"Click here to <a href=""" & ConfigurationSettings.AppSettings("root") & "/EmployeeServices/LMS/LMS_webfiles/frmLMSLeaveAppDtls.aspx?rid=" & strReqId & """>Accept / Reject</a><br><br>" & _

        'If strAppEmpEmail <> "" Then
        '    Send_Mail(strAppEmpEmail,strHrEmail,  REQID, strSubj & " - " & strAppEmpName, strMsg1)
        'End If
        'If strHrEmail <> "" Then
        '    Send_Mail(strHrEmail, strAppEmpEmail, REQID, strSubj & " - " & strAppEmpName, strMsg)
        'End If
    End Sub
End Class
