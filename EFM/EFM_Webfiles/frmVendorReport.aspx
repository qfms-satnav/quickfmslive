﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmVendorReport.aspx.vb" Inherits="EFM_EFM_Webfiles_frmVendorReport" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript"></script>

    <div>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">Vendor Wise Report
             <hr align="center" width="60%" /></asp:Label>

                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" cellpadding="0" cellspacing="0" align="center" border="0">
            <tr>
                <td style="width: 10px">
                    <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    <strong>Vendor Wise Report</strong></td>
                <td style="width: 17px">
                    <img alt="" height="27" src="../../images/table_right_top_corner.gif" width="16" /></td>
            </tr>
            <tr>
                <td background="../../images/table_left_mid_bg.gif"></td>
                <td align="left">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
                    <br />

                    <asp:Label ID="lblMSG" runat="server" ForeColor="Red" ></asp:Label>
                     <br />
                    <table id="table2" cellspacing="0" cellpadding="5" width="100%" border="1" style="border-collapse: collapse" align="center">
                        <tr>
                            <td align="left" class="clsLabel" style="width: 50%; height: 26px">Select Vendor <font color="red">*</font>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please Select Vendor" InitialValue="--Select--" Display="None" ControlToValidate="ddlVendor"></asp:RequiredFieldValidator>
                            </td>
                            <td align="left" class="clsLabel" style="width: 50%; height: 26px">
                                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="clsComboBox"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left" class="clsLabel" style="width: 50%; height: 26px"></td>
                            <td align="left" class="clsLabel" style="width: 50%; height: 26px">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" class="button"/>
                                <asp:Button ID="btnExport" runat="server" Text="Export" class="button"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">
                                <asp:GridView ID="gvdetails" runat="server" AutoGenerateColumns="False" Width="98%" AllowPaging="true" PageSize="15">
                                    <Columns>
                                        <asp:BoundField DataField="ASSIGNED_TO" HeaderText="Vendor ID" />
                                        <asp:BoundField DataField="AUR_KNOWN_AS" HeaderText="Vendor Name" />
                                        <asp:BoundField DataField="SER_ID" HeaderText="Request Id" />
                                        <asp:BoundField DataField="CREATED_TIME" HeaderText="Raised Time" />
                                        <asp:BoundField DataField="ASSIGNED_TIME" HeaderText="Assigned Time" />
                                        <asp:BoundField DataField="CLOSING_TIME" HeaderText="Closed Time" />
                                        <asp:BoundField DataField="TAT_HOURS" HeaderText="Total Time Taken(Hrs)" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

