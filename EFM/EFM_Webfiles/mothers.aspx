<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="mothers.aspx.vb" Inherits="EFM_EFM_Webfiles_mothers" title="Maintenance Report for Others" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/flick/jquery-ui.css"
        type="text/css" media="all" />
    <link href="../../Source/css/ui.jqgrid.css" rel="stylesheet" type="text/css" />

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>

    <script src="../../Source/js/i18n/grid.locale-en.js" type="text/javascript" defer></script>

    <script src="../../Source/js/jquery.jqGrid.min.js" type="text/javascript" defer></script>


    <script src="../../EFM/EFM_Webfiles/ReportsJS/MaintenanceOthers.js" type="text/javascript" defer></script>

<script lang="javascript" type="text/javascript" defer>
    var qrStr = window.location.search;
    var spQrStr = qrStr.substring(1);
    var mySplitResult = spQrStr.split('&');
    var val ="";
    for(i = 0; i < mySplitResult.length; i++)
    {
    var index = mySplitResult[i].indexOf("=");
    var key = mySplitResult[i].substring(0,index);
    val = val +"%"+mySplitResult[i].substring(index+1);

    }
    val =  val.substr(1,val.length)
   
    
var myString =val;
var a=[];
var mySplitResult1 = myString.split("%");

for(i = 0; i < mySplitResult1.length; i++)
{
a[0]=mySplitResult1[0];
a[1]=mySplitResult1[1];hg
a[2]=mySplitResult1[2];
a[3]=mySplitResult1[3];
}


    function dg_reportc()
             {
    $("#UserGrid").setGridParam({url:'../../Generics_Handler/MaintenanceOthers.ashx?Mfromdate='+a[0]+'&Mtodate='+a[1]+'&Mfromtime='+a[2]+'&Mtotime='+a[3]});
		        dg_manage_stores(a[0],a[1],a[2],a[3]);
		        $("#UserGrid").trigger("reloadGrid");
		      }
   </script>
   
<body onload="dg_reportc()">

    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Maintenance Report for Others
             <hr align="center" width="60%" /></asp:Label>
                &nbsp;
                <br />
            </td>
        </tr>
    </table>
    <table width="85%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td colspan="3" align="left">
                <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <img alt="" height="27" src="../../images/table_left_top_corner.gif" width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Maintenance Report for Others</strong></td>
            <td>
                <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
        </tr>
        <tr>
            <td background="../../Images/table_left_mid_bg.gif">
                &nbsp;</td>
            <td align="left">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                    ForeColor="" ValidationGroup="Val1" />
                <br />
                <table id="t" runat="server" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                          
                            <td align="left">
                               <asp:Button ID="btnback" runat="server" CssClass="button" Text="Back" />
                            </td>
                            <td align="left">
                               
                            </td>
                        </tr>
                    </table>
               
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td>
                &nbsp;
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="CENTER" width="100%">
                <table id="Table2" runat="server" cellpadding="0" width="100%" cellspacing="0">
                </table>
                <table id="UserGrid" cellpadding="0" cellspacing="0" width="100%">
                </table>
                <div id="UserGridPager">
                </div>
            </td>
            <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
            <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                <img height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
        </tr>
    </table>
  </body> 
</asp:Content>

