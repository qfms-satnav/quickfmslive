﻿
Imports System.Data
Imports System.Data.SqlClient

Partial Class FAM_FAM_Webfiles_Level_1_Approval_Admin
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim FromLoc, ToLoc, FromLocCode, ToLocCode, REQ_ID As String
    Dim receiveAst, strremarks As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        'Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        'Dim host As String = HttpContext.Current.Request.Url.Host
        'Dim param(1) As SqlParameter
        'param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        'param(0).Value = Session("UID")
        'param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        'param(1).Value = path
        'Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
        '    If Session("UID") = "" Then
        '        Response.Redirect(Application("FMGLogout"))
        '    Else
        '        If sdr.HasRows Then
        '        Else
        '            Response.Redirect(Application("FMGLogout"))
        '            Response.Redirect(Application("FMGLogout"))
        '        End If
        '    End If
        'End Using
        If Not IsPostBack Then
            'Dim UID As String = ""
            'If Session("uid") = "" Then
            '    Response.Redirect(Application("FMGLogout"))
            'Else
            '    UID = Session("uid")
            'End If
            FillReqIds()
        End If
    End Sub



    Public Sub FillReqIds()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@COMPANYID", SqlDbType.VarChar)
        param(1).Value = Session("COMPANYID")
        ObjSubsonic.BindGridView(gvReqIds, "AM_GET_IT_INTER_MOVEMENT_REQS_AM_NOTE_L1", param)
    End Sub


    Protected Sub gvReqIds_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReqIds.PageIndexChanging
        gvReqIds.PageIndex = e.NewPageIndex
        FillReqIds()
    End Sub


    Protected Sub gvReqIds_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqIds.RowCommand
        If e.CommandName = "Details" Then
            'Response.Redirect("ITApprovalDtls.aspx?Req_id=" & e.CommandArgument)
            astmvmntNote.Visible = True
            hdnReqID.Value = e.CommandArgument
            getDetailsbyReqId(e.CommandArgument)
            BindLocations()
            'BindFromPersons()
            'BindToPersons()
            BindRequestAssets(hdnReqID.Value)
        End If
    End Sub


    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)
        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("AM_GET_INTER_MVMT_DETLS_BY_REQID", param)
        If dr.Read Then
            FromLoc = dr.Item("FromLoc")
            ToLoc = dr.Item("ToLoc")
            FromLocCode = dr.Item("FromLocCode")
            ToLocCode = dr.Item("ToLocCode")
            strremarks = dr.Item("MMR_COMMENTS")
            txtSendDate.Text = dr.Item("senddate")
            textadminrmks.Text = dr.Item("adminremarks")
        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If
        TOFROMPERSONS(strREQ_id)
    End Sub


    Private Sub TOFROMPERSONS(ByVal strREQ_id As String)
        'ddlModel.Items.Clear()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BRANCH_MANAGER_BYLOCATION")
        'sp.Command.AddParameter("@AUR_LOCATION", ddlDestLoc.SelectedItem.Value, DbType.String)
        ddlFromPerson.Text = ""
        ddlToPerson.Text = ""
        Dim raisedby As String
        Dim receivedby As String
        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.VarChar, 50)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("AST_MMT_MVMT_PERSONS_TO_FROM", param)
        If dr.Read Then
            ddlFromPerson.Text = dr.Item("raisedby")
            ddlToPerson.Text = dr.Item("receivedby")
        End If
        If dr.IsClosed = False Then
            dr.Close()


        End If



    End Sub



    Private Sub BindLocations()
        BindLocation()
        BindDestLoactions()
        ddlSLoc.Items.FindByValue(FromLocCode).Selected = True
        ddlDLoc.Items.FindByValue(ToLocCode).Selected = True
    End Sub


    Private Sub BindDestLoactions()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_LOCATIONS")
            sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlDLoc.DataSource = sp.GetDataSet()
            ddlDLoc.DataTextField = "LCM_NAME"
            ddlDLoc.DataValueField = "LCM_CODE"
            ddlDLoc.DataBind()
            ddlDLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlSLoc.DataSource = sp.GetDataSet()
            ddlSLoc.DataTextField = "LCM_NAME"
            ddlSLoc.DataValueField = "LCM_CODE"
            ddlSLoc.DataBind()
            ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "AM_GET_MVMT_ASTS", param)
    End Sub


    Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            getDetailsbyReqId(Request.QueryString("Req_id"))
        End If
    End Sub



    Protected Sub ddlDLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLoc.SelectedIndexChanged
        If ddlDLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlDLoc.SelectedItem.Value
        End If
    End Sub



    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click


        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)

            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = hdnReqID.Value
            param(1) = New SqlParameter("@STATUS", SqlDbType.NVarChar, 200)
            param(1).Value = "3020"
            param(2) = New SqlParameter("@AMN_REMARKS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            param(3).Value = Session("uid")
            param(4) = New SqlParameter("@ASSET_CODE", SqlDbType.Int)
            param(4).Value = lblAAS_AAT_CODE.Text
            param(5) = New SqlParameter("@DEST_LOC", SqlDbType.Int)
            param(5).Value = ddlDLoc.SelectedItem.Value

            ObjSubsonic.GetSubSonicExecute("UPDATE_MMT_MVMT_REQ_L1APPROVE", param)
        Next
        SendMail(hdnReqID.Value)




        Response.Redirect("frmAssetThanks.aspx?RID=AssetMovementNoteGenerated")

    End Sub


    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        astmvmntNote.Visible = False
    End Sub

    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReject.Click
        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)

            Dim param(5) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = hdnReqID.Value
            param(1) = New SqlParameter("@STATUS", SqlDbType.NVarChar, 200)
            param(1).Value = "3021"
            param(2) = New SqlParameter("@AMN_REMARKS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            'param(3) = New SqlParameter("@AMN_COMPANY_ID", SqlDbType.NVarChar, 200)
            'param(3).Value = Session("companyid")
            param(3) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
            param(3).Value = Session("uid")
            param(4) = New SqlParameter("@ASSET_CODE", SqlDbType.Int)
            param(4).Value = lblAAS_AAT_CODE.Text
            param(5) = New SqlParameter("@DEST_LOC", SqlDbType.Int)
            param(5).Value = ddlDLoc.SelectedItem.Value

            ObjSubsonic.GetSubSonicExecute("UPDATE_MMT_MVMT_REQ_L1APPROVE", param)
        Next

        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_it_reject"))
        '    getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)
        SendMail(Request.QueryString("Req_id"))
        ''Response.Redirect("frmAssetThanks.aspx?RID=intermvmtitrej")
        Response.Redirect("frmAssetThanks.aspx?RID=intermvmtitrej")


    End Sub


    Private Sub SendMail(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_MVMT_NOTE_GENERATE")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Execute()
    End Sub
    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "IntraMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = getoffsetdatetime(DateTime.Now)
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub
End Class
