﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ExportLeaseInvoice.aspx.vb" Inherits="FAM_FAM_Webfiles_ExportLeaseInvoice" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script src="../../BlurScripts/BlurJs/jquery.js"></script>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .widgets {
            background-color: white;
        }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Lease Extension Details</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <div class="row">
                        <div class="col-md-12">
                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                        </div>
                        <div class="col-md-12">
                            <input type="hidden" id="lblpoid" name="abcName" runat="server" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
   
</body>
</html>