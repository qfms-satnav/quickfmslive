Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common

Partial Class FAM_FAM_Webfiles_frmViewPODetails
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            pnlItems.Visible = True
            BindVendorDetails()
            BindRequisition()
        End If
    End Sub
    Private Sub BindVendorDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_VENDOR_GETALL_ACTIVE")
        ddlVendor.DataSource = sp.GetReader
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_Code"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function
    Private Sub BindRequisition()
        Dim ReqId As String = GetRequestId()
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEMPO_DETAILS_GetByPO")
            sp.Command.AddParameter("@PO", ReqId, DbType.String)
            gvItems.DataSource = sp.GetDataSet
            gvItems.DataBind()
            pnlItems.Visible = True
            BindOtherDetails(ReqId)
        End If
    End Sub
    Private Sub BindOtherDetails(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_GetBYPO")
        sp.Command.AddParameter("@PO", ReqId, DbType.String)
        Dim dr As SqlDataReader = sp.GetReader()
        If dr.Read() Then
            txtPORaisedBy.Text = dr("AIP_AUR_ID")
            'txtRemarks.Text=
            txtRequesterId.Text = dr("AIP_AUR_ID")
            txtRequesterName.Text = dr("AIP_AUR_ID")
            txtRequestorRemarks.Text = dr("AIP_PO_REMARKS")
            txtDeptId.Text = dr("AIP_DEPT")
            txtLocation.Text = dr("AIP_LOCATION")
            txtAdvance.Text = dr("AIPD_PLI")
            txtCommissioning.Text = dr("AIPD_PAY_COMMISSION")
            txtCst.Text = dr("AIPD_TAX_CST")
            txtDelivery.Text = dr("AIPD_PAY_ONDELIVERY")
            txtDOD.SelectedDate = CDate(dr("AIPD_EXPECTED_DATEOF_DELIVERY"))
            txtExchange.Text = dr("AIPD_EXCHANGE_CHARGES")
            txtFFIP.Text = dr("AIPD_TAX_FFIP")
            txtInstallation.Text = dr("AIPD_PAY_INSTALLATION")
            txtOctrai.Text = dr("AIPD_TAX_OCTRAI")
            txtOthers.Text = dr("AIPD_TAX_OTHERS")
            txtPayments.Text = dr("AIPD_PAY_OTHERS")
            txtRetention.Text = dr("AIPD_PAY_RETENTION")
            txtServiceTax.Text = dr("AIPD_TAX_SERVICETAX")
            txtTotalCost.Text = dr("AIPD_TOTALCOST")
            txtWst.Text = dr("AIPD_TAX_WST")

            Dim li As ListItem = ddlVendor.Items.FindByValue(Trim(CStr(dr("AIP_AVR_CODE"))))
            If Not li Is Nothing Then
                li.Selected = True
            End If
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim PONumber As String = GetRequestId()
        UpdateData(PONumber, 2)
        Response.Redirect("frmAssetThanks.aspx?PO=" + PONumber)
    End Sub
    Private Sub UpdateData(ByVal ReqId As String, ByVal StatusId As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_UpdateStatus")
        sp.Command.AddParameter("@AIP_PO_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim PONumber As String = GetRequestId()
        UpdatePOMaster(PONumber)
        UpdatePOMasterDetails(PONumber)
        UpdateData(PONumber, 2)
        Response.Redirect("frmAssetThanks.aspx?PO=" + PONumber)
    End Sub

    Private Sub UpdatePOMaster(ByVal PO As String)
        Dim FFIP As Double = 0
        Dim Advance As Double = 0
        Dim cst As Double = 0
        Dim OnDelivery As Double = 0
        Dim Wst As Double = 0
        Dim Installation As Double = 0
        Dim Octrai As Double = 0
        Dim Commissioning As Double = 0
        Dim ServiceTax As Double = 0
        Dim Retention As Double = 0
        Dim Others As Double = 0
        Dim Payments As Double = 0
        Dim TotalCost As Double = 0

        If Double.TryParse(txtFFIP.Text, FFIP) Then
        Else
            lblMsg.Text = "Invalid FFIP"
            Exit Sub
        End If
        If Double.TryParse(txtCst.Text, cst) Then
        Else
            lblMsg.Text = "Invalid CST/ST"
            Exit Sub
        End If
        If Double.TryParse(txtOctrai.Text, Octrai) Then
        Else
            lblMsg.Text = "Invalid Octroi"
            Exit Sub
        End If
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        If Double.TryParse(txtDelivery.Text, OnDelivery) Then
        Else
            lblMsg.Text = "Invalid On Delivery"
            Exit Sub
        End If
        If Double.TryParse(txtWst.Text, Wst) Then
        Else
            lblMsg.Text = "Invalid Wst"
            Exit Sub
        End If
        If Double.TryParse(txtInstallation.Text, Installation) Then
        Else
            lblMsg.Text = "Invalid Installation"
            Exit Sub
        End If
        If Double.TryParse(txtCommissioning.Text, Commissioning) Then
        Else
            lblMsg.Text = "Invalid Commissioning"
            Exit Sub
        End If
        If Double.TryParse(txtServiceTax.Text, ServiceTax) Then
        Else
            lblMsg.Text = "Invalid ServiceTax"
            Exit Sub
        End If
        If Double.TryParse(txtRetention.Text, Retention) Then
        Else
            lblMsg.Text = "Invalid Retention"
            Exit Sub
        End If
        If Double.TryParse(txtOthers.Text, Others) Then
        Else
            lblMsg.Text = "Invalid Others"
            Exit Sub
        End If
        If Double.TryParse(txtPayments.Text, Payments) Then
        Else
            lblMsg.Text = "Invalid Payments"
            Exit Sub
        End If

        Dim ExchangeCharges As Double = 0
        If Double.TryParse(txtPayments.Text, ExchangeCharges) Then
        Else
            lblMsg.Text = "Invalid Exchange Charges"
            Exit Sub
        End If

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_Update")
        sp.Command.AddParameter("@AIP_PO_ID", PO, DbType.String)
        sp.Command.AddParameter("@AIP_AUR_ID", txtRequesterId.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_REMARKS", txtRequestorRemarks.Text, DbType.String)
        sp.Command.AddParameter("@AIP_APP_AUTH", Session("UID"), DbType.String)
        sp.Command.AddParameter("@AIP_APP_DATE", txtDOD.SelectedDate, DbType.DateTime)
        sp.Command.AddParameter("@AIP_APP_REM", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@AIP_DEPT", txtDeptId.Text, DbType.String)
        sp.Command.AddParameter("@AIP_ITM_TYPE", "", DbType.String)
        sp.Command.AddParameter("@AIP_LOCATION", txtLocation.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_TYPE", "", DbType.String)
        sp.Command.AddParameter("@AIPD_VENDORName", ddlVendor.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@AIPD_TOTALCOST", Double.Parse(Trim(txtTotalCost.Text)), DbType.Double)
        sp.Command.AddParameter("@AIPD_PLI", Advance, DbType.String)
        sp.Command.AddParameter("@AIPD_TAX_FFIP", FFIP, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_CST", cst, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_WST", Wst, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_OCTRAI", Octrai, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_SERVICETAX", ServiceTax, DbType.Double)
        sp.Command.AddParameter("@AIPD_TAX_OTHERS", Others, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_ADVANCE", Advance, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_ONDELIVERY", OnDelivery, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_INSTALLATION", Installation, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_COMMISSION", Commissioning, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_RETENTION", Retention, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_OTHERS", Payments, DbType.Double)
        sp.Command.AddParameter("@AIPD_EXPECTED_DATEOF_DELIVERY", txtDOD.SelectedDate, DbType.DateTime)
        sp.Command.AddParameter("@AIPD_EXCHANGE_CHARGES", ExchangeCharges, DbType.Double)
        sp.Command.AddParameter("@AVR_Code", ddlVendor.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub UpdatePOMasterDetails(ByVal PO As String)
        For Each row As GridViewRow In gvItems.Rows
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim txtUnitPrice As TextBox = DirectCast(row.FindControl("txtUnitPrice"), TextBox)
            Dim unitprice As Double = 0
            Dim PurchaseQty As Integer = 0
            Double.TryParse(Trim(txtUnitPrice.Text), unitprice)
            Integer.TryParse(Trim(txtPurchaseQty.Text), PurchaseQty)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEMPO_DETAILS_Update")
            sp.Command.AddParameter("@AIPD_PO_ID", PO, DbType.String)
            sp.Command.AddParameter("@AIPD_AST_CODE", lblProductId.Text, DbType.String)
            sp.Command.AddParameter("@AIPD_QTY", CInt(txtQty.Text), DbType.Int32)
            sp.Command.AddParameter("@AIPD_ITMREQ_ID", Request("RID"), DbType.String)
            sp.Command.AddParameter("@AIPD_ACT_QTY", PurchaseQty, DbType.Int32)
            sp.Command.AddParameter("@AIPD_RATE", unitprice, DbType.Double)
            sp.ExecuteScalar()
        Next
    End Sub

    Protected Sub btnTotalCost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTotalCost.Click
        Dim ItemSubTotal As Double = 0
        Dim FFIP As Double = 0
        Dim Advance As Double = 0
        Dim cst As Double = 0
        Dim OnDelivery As Double = 0
        Dim Wst As Double = 0
        Dim Installation As Double = 0
        Dim Octrai As Double = 0
        Dim Commissioning As Double = 0
        Dim ServiceTax As Double = 0
        Dim Retention As Double = 0
        Dim Others As Double = 0
        Dim Payments As Double = 0
        Dim TotalCost As Double = 0

        For Each row As GridViewRow In gvItems.Rows
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim txtUnitPrice As TextBox = DirectCast(row.FindControl("txtUnitPrice"), TextBox)
            Dim unitprice As Double = 0
            Dim PurchaseQty As Double = 0
            Decimal.TryParse(Trim(txtUnitPrice.Text), unitprice)
            Decimal.TryParse(Trim(txtPurchaseQty.Text), PurchaseQty)
            ItemSubTotal += (PurchaseQty * unitprice)
        Next

        If Double.TryParse(txtFFIP.Text, FFIP) Then
        Else
            lblMsg.Text = "Invalid FFIP"
            Exit Sub
        End If
        If Double.TryParse(txtCst.Text, cst) Then
        Else
            lblMsg.Text = "Invalid CST/ST"
            Exit Sub
        End If
        If Double.TryParse(txtOctrai.Text, Octrai) Then
        Else
            lblMsg.Text = "Invalid Octroi"
            Exit Sub
        End If
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        If Double.TryParse(txtDelivery.Text, OnDelivery) Then
        Else
            lblMsg.Text = "Invalid On Delivery"
            Exit Sub
        End If
        If Double.TryParse(txtWst.Text, Wst) Then
        Else
            lblMsg.Text = "Invalid Wst"
            Exit Sub
        End If
        If Double.TryParse(txtInstallation.Text, Installation) Then
        Else
            lblMsg.Text = "Invalid Installation"
            Exit Sub
        End If
        If Double.TryParse(txtCommissioning.Text, Commissioning) Then
        Else
            lblMsg.Text = "Invalid Commissioning"
            Exit Sub
        End If
        If Double.TryParse(txtServiceTax.Text, ServiceTax) Then
        Else
            lblMsg.Text = "Invalid ServiceTax"
            Exit Sub
        End If
        If Double.TryParse(txtRetention.Text, Retention) Then
        Else
            lblMsg.Text = "Invalid Retention"
            Exit Sub
        End If
        If Double.TryParse(txtOthers.Text, Others) Then
        Else
            lblMsg.Text = "Invalid Others"
            Exit Sub
        End If
        If Double.TryParse(txtPayments.Text, Payments) Then
        Else
            lblMsg.Text = "Invalid Payments"
            Exit Sub
        End If

        TotalCost = ItemSubTotal + FFIP + cst + Octrai + Wst + ServiceTax + Others
        txtTotalCost.Text = FormatNumber(TotalCost, 2)
    End Sub
End Class
