Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class frmMapped_unMapped_Summary
    Inherits System.Web.UI.Page

    Dim ObjSubSonic As New clsSubSonicCommonFunctions


    Dim Prd_id As Integer
    Dim asset As String
    Dim strLocation As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim str As String
        str = Request.QueryString("Prd_id")
        Dim array(2) As String
        array = str.Split("~")
        asset = array(0)
        strLocation = array(1)
        array = asset.Split(",")
        Prd_id = array(0)
        If Not IsPostBack Then


            BindMappedAssets(Prd_id, strLocation)
            BindUnMappedAssets(Prd_id, strLocation)
        End If
    End Sub

    Private Sub BindMappedAssets(ByVal PrdId As Integer, ByVal LCM_Code As String)

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = PrdId
        param(1) = New SqlParameter("@LCM_Code", SqlDbType.NVarChar, 200)
        param(1).Value = LCM_Code
        ObjSubSonic.BindGridView(gvItems, "GET_MAPPED_ASTSLIST", param)

    End Sub

    Private Sub BindUnMappedAssets(ByVal PrdId As Integer, ByVal LCM_Code As String)

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = PrdId
        param(1) = New SqlParameter("@LCM_Code", SqlDbType.NVarChar, 200)
        param(1).Value = LCM_Code
        ObjSubSonic.BindGridView(gvUnmappedAst, "GET_UNMAPPED_ASTSLIST", param)

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindMappedAssets(Prd_id, strLocation)

    End Sub

    Protected Sub gvUnmappedAst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUnmappedAst.PageIndexChanging
        gvUnmappedAst.PageIndex = e.NewPageIndex
        BindUnMappedAssets(Prd_id, strLocation)
    End Sub
End Class

