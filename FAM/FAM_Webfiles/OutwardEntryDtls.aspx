<%@ Page Language="VB" AutoEventWireup="false" CodeFile="OutwardEntryDtls.aspx.vb"
    Inherits="FAM_FAM_Webfiles_OutwardEntryDtls" Title="Outward Entry Details" %>

<%@ Register Src="../../Controls/OutwardEntryDtls.ascx" TagName="OutwardEntryDtls" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Outward Entry Details" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Outward Entry Details</h3>
            </div>
            <div class="card">
                <%-- <div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="col-md-12 control-label" ForeColor="Red" ValidationGroup="Val1" />
                    <uc1:OutwardEntryDtls ID="OutwardEntryDtls1" runat="server" />
                </form>
            </div>
        </div>
        <%--  </div>
            </div>
        </div>--%>
    </div>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/jquery.min.js")%>' type="text/javascript"></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap.min.js")%>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-select.min.js")%>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/bootstrap-datepicker.js") %>' type="text/javascript" defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/ObjectKeys.js")%>' defer></script>
    <script src='<%= Page.ResolveUrl("~/BootStrapCSS/Scripts/wz_tooltip.js")%>' type="text/javascript" defer></script>
    <script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" temp_src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.1/modernizr.min.js" defer></script>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" temp_src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" temp_src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</body>
</html>
