<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddAssetSubCategory.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAddAssetSubCategory" Title="Add Sub Category" %>

<%@ Register Src="../../Controls/AddAssetSubCategory.ascx" TagName="AddAssetSubCategory" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
               <%-- <div ba-panel ba-panel-title="Asset Sub Type Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Asset Sub Category Master</h3>
                        </div>--%> <h3>Asset Subcategory Master</h3>
                      </div>
                        <div class="card">
                        <%--<div class="card-body" style="padding-right: 50px;">--%>
                            <form id="form1" runat="server" class="container">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel ID="CityPanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:ValidationSummary ID="VerticalValidations" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                        <uc1:AddAssetSubCategory ID="AddAssetSubCategory1" runat="server"></uc1:AddAssetSubCategory>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>
                    </div>
                </div>
          <%--  </div>
        </div>
    </div>--%>
<%--    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>--%>
</body>
</html>


