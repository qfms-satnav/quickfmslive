﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using Commerce.Common;
using System.Linq;


public partial class FAM_FAM_Webfiles_UpdatePO : System.Web.UI.Page
{
    string txtTotalCost = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            divven.Visible = false;
            gvItems.Visible = false;
            pnlItems.Visible = true;
            BindVendor2Details();
            BindGrid();
            //BindState();
        }
    }

    protected void rbActions_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMsg.Visible = false;
        //txtAdvance.Text = "0";
        //// txtFFIP.Text = "0";
        //txtCst.Text = "0";
        //txtsgst.Text = "0";
        //txtigst.Text = "0";
        //txtDelivery.Text = "0";
        //// txtWst.Text = "0";
        //txtInstallation.Text = "0";
        //// txtOctrai.Text = "0";
        //txtCommissioning.Text = "0";
        //// txtServiceTax.Text = "0";
        //txtRetention.Text = "0";
        //// txtOthers.Text = "0";
        //txtPayments.Text = "0";
        try
        {
            if (Rbtwithout.Checked)
            {
                divven.Visible = false;
                gvItems.Visible = false;
                gvvendorreqs.Visible = true;
                BindGrid();
                divvendors.Visible = true;
                BindVendor2Details();
            }
            else
            {
                divvendors.Visible = false;
                gvItems.Visible = false;
                divven.Visible = true;
                BindGrid();
                gvvendorreqs.Visible = true;
                BindVendorDetails();
            }
        }
        catch (Exception)
        {
            // Consider logging the exception or handling it as needed
        }
    }
    private void BindVendorDetails()
    {
        var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "AST_GETVENDOR_FORASSE");
        //Uncomment and adjust if you need to pass parameters to your stored procedure
         sp.Command.AddParameter("@REQ_ID", Request.QueryString["RID"], DbType.String);
        ddlVendor.DataSource = sp.GetReader();
        ddlVendor.DataTextField = "AVR_NAME";
        ddlVendor.DataValueField = "AVR_CODE";
        ddlVendor.DataBind();
        ddlVendor.Items.Insert(0, new ListItem("--Select--", "0"));
    }


    //protected void btnTotalCost_Click(object sender, EventArgs e)
    //{
    //    if (!Page.IsValid)
    //    {
    //        return;
    //    }

    //    double itemSubTotal = 0;
    //    double advance, cgst, sgst, igst, delivery, installation, commissioning, retention, payments;
    //    int chkCount = 0;

    //    foreach (GridViewRow row in gvItems.Rows)
    //    {
    //        CheckBox chkSelect1 = row.FindControl("chkSelect1") as CheckBox;
    //        TextBox txtUnitPrice = row.FindControl("txtUnitPrice") as TextBox;
    //        TextBox txtPurchaseQty = row.FindControl("txtPurchaseQty") as TextBox;

    //        double unitPrice = 0;
    //        double purchaseQty = 0;

    //        if (chkSelect1 != null && chkSelect1.Checked &&
    //            double.TryParse(txtUnitPrice.Text.Trim(), out unitPrice) &&
    //            double.TryParse(txtPurchaseQty.Text.Trim(), out purchaseQty))
    //        {
    //            itemSubTotal += (purchaseQty * unitPrice);
    //            chkCount++;
    //        }
    //    }

    //    if (chkCount == 0)
    //    {
    //        lblMsg.Visible = true;
    //        lblMsg.Text = "Select checkbox";
    //        return;
    //    }

    //    bool isCgstValid = double.TryParse(txtCst.Text, out cgst);
    //    bool isSgstValid = double.TryParse(txtsgst.Text, out sgst);
    //    bool isIgstValid = double.TryParse(txtigst.Text, out igst);
    //    bool isAdvanceValid = double.TryParse(txtAdvance.Text, out advance);
    //    bool isDeliveryValid = double.TryParse(txtDelivery.Text, out delivery);
    //    bool isInstallationValid = double.TryParse(txtInstallation.Text, out installation);
    //    bool isCommissioningValid = double.TryParse(txtCommissioning.Text, out commissioning);
    //    bool isRetentionValid = double.TryParse(txtRetention.Text, out retention);
    //    bool isPaymentsValid = double.TryParse(txtPayments.Text, out payments);

    //    if (!isCgstValid || !isSgstValid || !isIgstValid || !isAdvanceValid || !isDeliveryValid ||
    //        !isInstallationValid || !isCommissioningValid || !isRetentionValid || !isPaymentsValid)
    //    {
    //        lblMsg.Visible = true;
    //        lblMsg.Text = "Invalid input";
    //        return;
    //    }

    //    double totalCost = itemSubTotal + cgst + igst + sgst + advance + delivery + installation + commissioning + retention + payments;
    //    txtTotalCost.Text = totalCost.ToString("N2");
    //}


    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        bool checking = GridChecking();
        if (checking)
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Please select Unique Asset Req id";
            return;
        }
        if (!Page.IsValid)
        {
            return;
        }

        double itemSubTotal = 0;
        //double advance, cgst, sgst, igst, delivery, installation, commissioning, retention, payments;
        int chkCount = 0;

        foreach (GridViewRow row in gvItems.Rows)
        {
            CheckBox chkSelect1 = row.FindControl("chkSelect1") as CheckBox;
            TextBox txtUnitPrice = row.FindControl("txtUnitPrice") as TextBox;
            TextBox txtPurchaseQty = row.FindControl("txtPurchaseQty") as TextBox;

            double unitPrice = 0;
            double purchaseQty = 0;

            if (chkSelect1 != null && chkSelect1.Checked &&
                double.TryParse(txtUnitPrice.Text.Trim(), out unitPrice) &&
                double.TryParse(txtPurchaseQty.Text.Trim(), out purchaseQty))
            {
                itemSubTotal += (purchaseQty * unitPrice);
                chkCount++;
            }
        }
        if (chkCount == 0)
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Select checkbox";
            return;
        }
        if (txtPONum.Text == "")
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Please enter PO Number";
            return;
        }
        if (txtPODate.Value == "")
        {
            lblMsg.Visible = true;
            lblMsg.Text = "Select Date";
            return;
        }
        if (!NewPOID(txtPONum.Text))
        {
            lblMsg.Visible = true;
            lblMsg.Text = txtPONum.Text + " already exist, please enter new  PO Number";
            txtPONum.Text = "";
            return;            
        }
        lblMsg.Text = "";

        txtTotalCost = Convert.ToString(itemSubTotal);
        InsertPOMaster(txtPONum.Text);
        //string poNumber = GeneratePO();
        if (!string.IsNullOrEmpty(lblMsg.Text))
        {
            lblMsg.Visible = true;
            return;
        }
        else
        {
            InsertPOMasterDetails(txtPONum.Text);
            UpdateData(1014);
            SendMailGenPO(txtPONum.Text);
            Response.Redirect("frmAssetThanks.aspx?RID=" + Request.QueryString["RID"] + "&PO=" + txtPONum.Text);
        }

    }

    protected bool NewPOID(string PONum)
    {
        try
        {
            var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "CHK_PO_ID");
            sp.Command.AddParameter("@REQ_ID", PONum, DbType.String);
            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0 && Convert.ToInt32(ds.Tables[0].Rows[0][0]) == 1)
            {
                return true;
            }
            return false;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
    protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlVendor.SelectedIndex > 0)
        {
            BindRequisition();
            gvItems.Visible = true;
            gvvendorreqs.Visible = true;
            //txtTotalCost.Text = "";

            var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "GET_VENDOR_DETAILS");
            sp.Command.AddParameter("@VENDOR", ddlVendor.SelectedItem.Value, DbType.String);

            DataSet ds = sp.GetDataSet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                //txtAdd.Text = ds.Tables[0].Rows[0]["AVR_ADDR"].ToString();
                //txtNumber.Text = ds.Tables[0].Rows[0]["MOBILE"].ToString();
            }
        }
        else
        {
            gvItems.Visible = false;
            //var txtTotalCost = "";
        }
    }

    private void BindVendor2Details()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "AM_AST_GETVENDORS");
        // Uncomment and adjust the following line if you need to pass a parameter to your stored procedure
        //sp.Command.AddParameter("@REQ_ID", Request.QueryString["RID"], DbType.String);

        ddlVendor2.DataSource = sp.GetReader();
        ddlVendor2.DataTextField = "AVR_NAME";
        ddlVendor2.DataValueField = "AVR_CODE";
        ddlVendor2.DataBind();

        ddlVendor2.Items.Insert(0, new ListItem("--Select--", "0"));
    }
    private void BindGrid()
    {
        var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "AM_GET_GENERATE_PO_DETAILS");
        sp.Command.AddParameter("@dummy", 3022, DbType.Int32); // Adjust parameter values as necessary
        sp.Command.AddParameter("@Cuser", Session["uid"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);

        DataSet ds = sp.GetDataSet();
        gvvendorreqs.DataSource = ds;
        gvvendorreqs.DataBind();
    }
    //private void BindState()
    //{
    //    var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "AM_BIND_STATES");
    //    ddlState.DataSource = sp.GetReader();
    //    ddlState.DataTextField = "AM_ST_NAME";
    //    ddlState.DataValueField = "AM_ST_CODE";
    //    ddlState.DataBind();
    //    ddlState.Items.Insert(0, new ListItem("--Select--", "0"));
    //}
    private bool GridChecking()
    {
        List<string> alreadySeen = new List<string>();
        foreach (GridViewRow row in gvItems.Rows)
        {
            CheckBox chkSelect1 = row.FindControl("chkSelect1") as CheckBox;
            Label lblReqId = row.FindControl("lblReqId") as Label;
            if (chkSelect1 != null && chkSelect1.Checked)
            {
                alreadySeen.Add(lblReqId.Text);
            }
        }

        var duplicateCount = alreadySeen
            .GroupBy(item => item)
            .Where(group => group.Count() > 1)
            .Sum(group => group.Count());

        return duplicateCount > 1;
    }
    private string GeneratePO()
    {
        string po = "";
        var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "USP_GETMAX_AMG_ITEM_PO");
        int sno = Convert.ToInt32(sp.ExecuteScalar());

        if (Session["TENANT"].ToString() == "Aadhar.dbo")
        {
            po = "AHFL/" + GetCurrentFinancialYear() + "/0000" + sno.ToString();
        }
        else
        {
            po = "AST/PO/0000" + sno.ToString();
        }

        return po;
    }

    private string GetCurrentFinancialYear()
    {
        var currentYear = DateTime.Today.Year;
        var previousYear = DateTime.Today.Year - 1;
        var nextYear = DateTime.Today.Year + 1;
        var financialYear = "FY";

        if (DateTime.Today.Month > 3)
        {
            financialYear += currentYear.ToString() + "-" + nextYear.ToString();
        }
        else
        {
            financialYear += previousYear.ToString() + "-" + currentYear.ToString();
        }

        return financialYear;
    }
    private void InsertPOMaster(string po)
    {
        //double advance, cgst, sgst, igst, delivery, installation, commissioning, retention, payments, exchangeCharges;
        //if (!double.TryParse(txtCst.Text, out cgst))
        //{
        //    lblMsg.Text = "Invalid CGST";
        //    return;
        //}
        //if (!double.TryParse(txtsgst.Text, out sgst))
        //{
        //    lblMsg.Text = "Invalid SGST";
        //    return;
        //}
        //if (!double.TryParse(txtigst.Text, out igst))
        //{
        //    lblMsg.Text = "Invalid IGST";
        //    return;
        //}
        //if (!double.TryParse(txtAdvance.Text, out advance))
        //{
        //    lblMsg.Text = "Invalid Advance";
        //    return;
        //}
        //if (!double.TryParse(txtDelivery.Text, out delivery))
        //{
        //    lblMsg.Text = "Invalid On Delivery";
        //    return;
        //}
        //if (!double.TryParse(txtInstallation.Text, out installation))
        //{
        //    lblMsg.Text = "Invalid Installation";
        //    return;
        //}
        //if (!double.TryParse(txtCommissioning.Text, out commissioning))
        //{
        //    lblMsg.Text = "Invalid Commissioning";
        //    return;
        //}
        //if (!double.TryParse(txtRetention.Text, out retention))
        //{
        //    lblMsg.Text = "Invalid Retention";
        //    return;
        //}
        //if (!double.TryParse(txtPayments.Text, out payments))
        //{
        //    lblMsg.Text = "Invalid Payments";
        //    return;
        //}
        //if (!double.TryParse(txtExchange.Text, out exchangeCharges))
        //{
        //    lblMsg.Text = "Invalid Exchange Charges";
        //    return;
        //}
        string ClPONum;
        ClPONum = txtPONum.Text;
        DateTime PODt = Convert.ToDateTime( txtPODate.Value);

        var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "USP_AMG_ITEM_PO_Insert_new");
        sp.Command.AddParameter("@AIP_PO_ID", po, DbType.String);
        sp.Command.AddParameter("@AIP_APP_AUTH", Session["UID"].ToString(), DbType.String);
        //sp.Command.AddParameter("@AIP_APP_DATE", txtDOD.Text, DbType.DateTime);
        //sp.Command.AddParameter("@AIP_APP_REM", txtRemarks.Text, DbType.String);
        sp.Command.AddParameter("@AIP_ITM_TYPE", "", DbType.String);
        sp.Command.AddParameter("@AIP_PO_TYPE", "", DbType.String);
        sp.Command.AddParameter("@ClPONum", ClPONum, DbType.String);
        sp.Command.AddParameter("@PODate", PODt, DbType.DateTime);
        if (Rbtwith.Checked)
        {
            //sp.Command.AddParameter("@AIPD_VENDORName", ddlVendor.SelectedItem.Text, DbType.String);
            //sp.Command.AddParameter("@AVR_ID", ddlVendor.SelectedItem.Value, DbType.String);
        }
        else
        {
            sp.Command.AddParameter("@AIPD_VENDORName", ddlVendor2.SelectedItem.Text, DbType.String);
            sp.Command.AddParameter("@AVR_ID", ddlVendor2.SelectedItem.Value, DbType.String);
        }

        sp.Command.AddParameter("@AIPD_TOTALCOST", double.Parse(txtTotalCost.Trim()), DbType.Double);
        //sp.Command.AddParameter("@AIPD_PLI", advance, DbType.Double);
        //sp.Command.AddParameter("@AIPD_CGST", cgst, DbType.Double);
        //sp.Command.AddParameter("@AIPD_SGST", sgst, DbType.Double);
        //sp.Command.AddParameter("@AIPD_IGST", igst, DbType.Double);
        //sp.Command.AddParameter("@AIPD_PAY_ADVANCE", advance, DbType.Double);
        //sp.Command.AddParameter("@AIPD_PAY_ONDELIVERY", delivery, DbType.Double);
        //sp.Command.AddParameter("@AIPD_PAY_INSTALLATION", installation, DbType.Double);
        //sp.Command.AddParameter("@AIPD_PAY_COMMISSION", commissioning, DbType.Double);
        //sp.Command.AddParameter("@AIPD_PAY_RETENTION", retention, DbType.Double);
        //sp.Command.AddParameter("@AIPD_PAY_OTHERS", payments, DbType.Double);
        ////sp.Command.AddParameter("@AIPD_EXPECTED_DATEOF_DELIVERY", txtDOD.Text, DbType.DateTime);
        //sp.Command.AddParameter("@AIPD_EXCHANGE_CHARGES", exchangeCharges, DbType.Double);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);
        //sp.Command.AddParameter("@QREFID", txtQutref.Text, DbType.String);
        //sp.Command.AddParameter("@QDATE", txtQutdate.Text, DbType.String);
        //sp.Command.AddParameter("@STATE", ddlState.SelectedItem.Value, DbType.String);
        //sp.Command.AddParameter("@STATE_GST", txtStateGST.Text, DbType.String);
        //sp.Command.AddParameter("@STATE_ADDR", txtSAddress.Text, DbType.String);
        sp.ExecuteScalar();
    }
    private void InsertPOMasterDetails(string po)
    {
        foreach (GridViewRow row in gvItems.Rows)
        {
            var chkSelect1 = row.FindControl("chkSelect1") as CheckBox;
            var lblProductId = row.FindControl("lblProductId") as Label;
            var lblReqID = row.FindControl("lblReqID") as Label;
            var txtQty = row.FindControl("txtQty") as TextBox;
            var txtPurchaseQty = row.FindControl("txtPurchaseQty") as TextBox;
            var txtreqloc = row.FindControl("lblReqLoc") as Label;
            var txtUnitPrice = row.FindControl("txtUnitPrice") as TextBox;
            //var txtPONum = row.FindControl("txtPONum") as TextBox;
            //var txtPODate = row.FindControl("txtPODate") as TextBox;
            double unitPrice = 0;
            int purchaseQty = 0;

            if (chkSelect1 != null && chkSelect1.Checked
                && double.TryParse(txtUnitPrice.Text.Trim(), out unitPrice)
                && int.TryParse(txtPurchaseQty.Text.Trim(), out purchaseQty))
            {
                var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "USP_AMG_ITEMPO_DETAILS_Insert");
                sp.Command.AddParameter("@AIPD_PO_ID", po, DbType.String);
                sp.Command.AddParameter("@AIPD_AST_CODE", lblProductId.Text, DbType.String);
                sp.Command.AddParameter("@AIPD_QTY", Convert.ToInt32(txtQty.Text), DbType.Int32);
                sp.Command.AddParameter("@AIPD_ITMREQ_ID", lblReqID.Text, DbType.String);
                sp.Command.AddParameter("@AIPD_ACT_QTY", purchaseQty, DbType.Int32);
                sp.Command.AddParameter("@AIPD_RATE", unitPrice, DbType.Double);
                //sp.Command.AddParameter("@TERMS", txtTerms.Text, DbType.String);
                sp.Command.AddParameter("@PO_REQ_LOC", txtreqloc.Text, DbType.String);
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);
                sp.ExecuteScalar();
            }
        }
    }
    private void UpdateData(int statusId)
    {
        foreach (GridViewRow row in gvItems.Rows)
        {
            var chkSelect1 = row.FindControl("chkSelect1") as CheckBox;
            if (chkSelect1 != null && chkSelect1.Checked)
            {
                var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "USP_AssetRequisitionStatus_UpdateByReqIdPOApproved");
                sp.Command.AddParameter("@ReqId", (row.FindControl("lblReqId") as Label).Text, DbType.String);
                sp.Command.AddParameter("@StatusId", statusId, DbType.Int32);
                sp.Command.AddParameter("@ProductId", (row.FindControl("lblProductid") as Label).Text, DbType.String);
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);
                sp.ExecuteScalar();
            }
        }
    }
    public void SendMailGenPO(string poNum)
    {
        var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "SEND_MAIL_ASSET_REQUISITION_PO_GENERATE");
        sp.Command.AddParameter("@REQ_ID", poNum, DbType.String);
        sp.Execute();
    }
    private void BindRequisition()
    {
        DataTable dt = new DataTable();
        foreach (GridViewRow row in gvvendorreqs.Rows)
        {
            CheckBox chkSelect1 = row.FindControl("chkSelect") as CheckBox;
            Label lblReqID = row.FindControl("lblReqID") as Label;
            if (chkSelect1 != null && chkSelect1.Checked)
            {
                var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + "." + "USP_AssetRequisitionDetailsProcurement_GetDetailsByReqId");
                sp.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String);
                //sp.Command.AddParameter("@VEN_CODE", ddlVendor.SelectedItem.Value, DbType.String);
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"], DbType.String);
                DataSet ds = sp.GetDataSet();
                dt.Merge(ds.Tables[0]);
            }
        }
        gvItems.DataSource = dt;
        gvItems.DataBind();
        pnlItems.Visible = true;
    }
    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();

        foreach (GridViewRow row in gvvendorreqs.Rows)
        {
            CheckBox chkSelect = (CheckBox)row.FindControl("chkSelect");
            Label lblReqID = (Label)row.FindControl("lblReqID");

            if (chkSelect.Checked)
            {
                if (Rbtwith.Checked)
                {
                    // Assuming SubSonic.StoredProcedure is a part of your project or a third-party library you're using
                    // Note: This example assumes "Session["TENANT"]", "HttpContext.Current.Session["COMPANYID"]" are valid
                    // Replace "AM_USP_GetPODetailsBy_ReqId" with the correct stored procedure name
                    var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + ".AM_USP_GetPODetailsBy_ReqId");
                    sp.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String);
                    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);
                    ds = sp.GetDataSet();
                    dt.Merge(ds.Tables[0]);
                }
                else
                {
                    var sp = new SubSonic.StoredProcedure(Session["TENANT"].ToString() + ".AM_USP_Get_WITHOUTPODetailsBy_ReqId");
                    sp.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String);
                    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.String);
                    ds = sp.GetDataSet();
                    dt.Merge(ds.Tables[0]);
                }
            }
        }

        gvItems.Visible = true;
        gvItems.DataSource = dt;
        gvItems.DataBind();
        pnlItems.Visible = true;
    }
    //protected void ddlTax_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    // Assuming gst and gst1 are Panel or some other server controls that can be visible or hidden
    //    Panel gst = FindControl("gst") as Panel;
    //    Panel gst1 = FindControl("gst1") as Panel;

    //    if (ddlTax.SelectedValue == "Exclusive")
    //    {
    //        if (gst != null) gst.Visible = true;
    //        if (gst1 != null) gst1.Visible = true;
    //    }
    //    else
    //    {
    //        if (gst != null) gst.Visible = false;
    //        if (gst1 != null) gst1.Visible = false;
    //    }
    //}


}