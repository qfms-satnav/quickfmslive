﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        .ag-blue .ag-row {
            height: 75px !important;
        }
        .ag-cell{
            display:inline-block;
        }
    </style>
</head>
<body data-ng-controller="GlobalSearchController" class="amantra">
    <div class="animsition">
        <div class="al-content"">
            <div class="widgets">
                <h3>Global Search</h3>
            </div>
            <div class="card">
                <div class="row m-1 align-items-center">
                    <div class="col-md-3 col-sm-12 col-xs-12 form-inline">
                        <div class="form-group">
                            <label class="control-label">Site:</label>
                            <div isteven-multi-select data-input-model="Location" data-output-model="GlobalSearch.LCM_CODE" button-label="icon LCM_NAME"
                                item-label="icon LCM_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="QRCodeGenerator.LCM_CODE" name="LCM_CODE" style="display: none" required="" />
                            <span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.Location.$invalid" style="color: red">Please Select Location </span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="input-group">
                            <span class="input-group-prepend">
                                <span class="input-group-text bg-transparent text-light"><i class="fa fa-search"></i></span>
                            </span>
                            <input type="text" id="filtertxt" class="form-control" placeholder="Search with Asset ID, Name, PO, PR, Req ID, Site..." />
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12 col-xs-12 pull-right">
                        <input type="button" id="search" value="Search" class="btn btn-primary" data-ng-click="LoadData()" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div data-ag-grid="gridOptions" class="ag-blue"  style="height: 500px; width: auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        debugger;
        var tenant = '<%= Session("Tenant") %>';
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt", "cp.ngConfirm"]);

    </script>
    <script src="../../AssetManagement/JS/GlobalSearch.js"></script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../../BootStrapCSS/assets/js/Isteven-Multiselect/isteven-multi-select.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
    <script src="../../BootStrapCSS/Scripts/angular-confirm.js" defer></script>
    <script src="../../BlurScripts/BlurJs/jquery.qrcode.min.js"></script>
    <%--<script src="../../BlurScripts/BlurJs/qrcode.js"></script>--%>
</body>
</html>


