<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmFinalizePO.aspx.vb" Inherits="FAM_FAM_Webfiles_frmFinalizePO" Title="Finalize PO" %>

<%@ Register Src="../../Controls/FinalizePO.ascx" TagName="FinalizePO" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Finalize PO</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <uc1:FinalizePO ID="FinalizePO1" runat="server" />
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
