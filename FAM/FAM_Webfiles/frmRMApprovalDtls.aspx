<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmRMApprovalDtls.aspx.vb" Inherits="FAM_FAM_Webfiles_frmRMApprovalDtls" Title="RM Approval Details" %>

<%@ Register Src="../../Controls/RMApprovalDetailsl.ascx" TagName="RMApprovalDetailsl"
    TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                            <h4>Level1 Approval Details</h4>
                        </div>
                        <div class="card" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel ID="Assetpanel" runat="server">
                                    <ContentTemplate>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                            ForeColor="Red" ValidationGroup="Val1" />
                                        <uc1:RMApprovalDetailsl ID="RMApprovalDetailsl1" runat="server"></uc1:RMApprovalDetailsl>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



