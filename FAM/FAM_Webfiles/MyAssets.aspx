<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="MyAssets.aspx.vb" Inherits="FAM_FAM_Webfiles_MyAssets" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black"> My Assets
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong> My Assets</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                <table width="100%" cellpadding="2px">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <table width="100%" style="vertical-align: top;" cellpadding="0" cellspacing="0"
                                align="center" border="1">
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label12" runat="server" CssClass="bodytext" Text="Asset Code"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblAstCode" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label2" runat="server" CssClass="bodytext" Text="Asset Name"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblAstName" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label3" runat="server" CssClass="bodytext" Text="Location"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label4" runat="server" CssClass="bodytext" Text="Tower"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblTower" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label5" runat="server" CssClass="bodytext" Text="Floor"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblFloor" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label14" runat="server" CssClass="bodytext" Text="Asset Date"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblAstDate" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label6" runat="server" CssClass="bodytext" Text="Asset Allocated to Employee Date"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblAstAllocDt" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label7" runat="server" CssClass="bodytext" Text="Asset Surrender Date"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblAstSurDt" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label8" runat="server" CssClass="bodytext" Text="Surrender Request Id"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblSurReq_id" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label10" runat="server" CssClass="bodytext" Text="RM Approval Date"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblSREQ_RMAPPROVAL_DATE" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label13" runat="server" CssClass="bodytext" Text="Approved By (RM) "></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblRM_NAME" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        <asp:Label ID="label15" runat="server" CssClass="bodytext" Text="RM Remarks"></asp:Label>
                                    </td>
                                    <td align="left" width="50%">
                                        <asp:Label ID="lblRMRemarks" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" width="50%">
                                        &nbsp;
                                    </td>
                                    <td align="left" width="50%">
                                     
                                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="button" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</div>

</asp:Content>

