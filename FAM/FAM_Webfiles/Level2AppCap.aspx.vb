﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper
Partial Class FAM_FAM_Webfiles_Level2AppCap

    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim CatId As String
    Dim asstsubcat As String
    Dim asstbrand As String
    Dim astmodel As String
    Dim CachedInterGrid As DataTable
    Dim dtInterGrid As New DataTable
    Dim dtIntraGrid As New DataTable
    Dim CachedtIntraGrid As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)

        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Page)
        If scriptManager IsNot Nothing Then

            scriptManager.RegisterPostBackControl(grdDocs)

        End If


        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
                RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            BindGrid()
        End If
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAPT_LEVEL1_APPROVALS_GETBYAURID")
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub

    Private Sub SearchBindGrid()
        lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_LEVEL2_APPROVAL_SEARCH")
        sp.Command.AddParameter("@search_criteria", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        If (txtSearch.Text.Length > 0) Then
            SearchBindGrid()
        Else
            BindGrid()
        End If

    End Sub

    Protected Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "GetCaptApprovalDetails" Then
                hdnCANO.Value = e.CommandArgument
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                'Dim ReqID As String = DirectCast(row.FindControl("lblReqID"), LinkButton).Text.ToString()
                Session("REQ_ID") = e.CommandArgument
                GetCaptApprovalDetails(hdnCANO.Value)
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetCaptApprovalDetails(ByVal Reqid As String)
        'ol pg
        BindUsers()
        getassetcategory()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        '  ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        'getassetsubcategory()
        pnlItems.Visible = True
        ' BindRequisition()
        BindBasicReqDetails()
        BindGridSub()
        UpdatePanel.Visible = True
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        ' ddlAstCat.SelectedIndex = If(ddlAstCat.Items.Count > 1, 0, 0)
    End Sub

    Private Sub BindBasicReqDetails()
        Dim ReqId As String = Session("REQ_ID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AMG_ITEM_REQUISITION_GetByReqId_NP")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                'Dim RaisedBy As Integer = 0
                'Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim RaisedBy As String
                RaisedBy = dr("AIR_AUR_ID")

                Dim Raised As String
                Raised = dr("AUR_KNOWN_AS")

                BindUsers()
                ddlemp.ClearSelection()
                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If
                txtemp.Text = RaisedBy + Raised

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)
                'Dim CatId As String
                ddlAstCat.ClearSelection()
                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                '   ddlAstCat.Enabled = False


                getassetsubcategory(CatId)
                '   ddlAstSubCat.Enabled = False
                'Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")
                asstsubcat = dr("AIR_ITEM_SUBCAT")
                ddlAstSubCat.ClearSelection()
                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                'Dim asstbrand As String = dr("AIR_ITEM_BRD")
                asstbrand = dr("AIR_ITEM_BRD")
                ddlAstBrand.ClearSelection()
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                '  ddlAstBrand.Enabled = False


                getmakebycatsubcat()

                BindLocation1()

                Dim Loc As String = dr("AIR_REQ_LOC")
                ddlLocation.ClearSelection()
                ddlLocation.Items.FindByValue(Loc).Selected = True

                astmodel = dr("AIR_ITEM_MOD")
                ddlAstModel.ClearSelection()
                ddlAstModel.Items.FindByValue(astmodel).Selected = True

                txtRemarks.Text = dr("AIR_REMARKS")
                txtL1Remarks.Text = dr("AIR_APR1_REM")
                'txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")


                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                BindDocuments(ReqId)


            End If
        End If
    End Sub


    Private Sub BindLocation1()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
    End Sub
    Public Sub BindDocuments(ByVal ReqId As String)
        Dim dtDocs As New DataTable("Documents")
        Dim param(0) As SqlParameter
        'param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@AMG_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = ReqId
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("AST_Level1_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "Documents not available"
        End If
        dtDocs = Nothing
    End Sub
    'Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
    '    If e.CommandName = "Download" Then

    '            Dim ID = grdDocs.DataKeys(e.Item.ItemIndex)
    '            e.Item.BackColor = Drawing.Color.LightSteelBlue
    '            Dim filePath As String
    '            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
    '            filePath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & filePath
    '            Response.ContentType = "application/octet-stream"
    '            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "UploadFiles\", "") & """")
    '            Response.WriteFile(filePath)
    '            'Response.TransmitFile(Server.MapPath(filePath))
    '            Response.[End]()

    '    End If
    'End Sub
    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim ID = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & filePath
            Response.ContentType = "application/octet-stream"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "UploadFiles\", "") & """")
            Response.WriteFile(filePath)
            'Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        'ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, New ListItem("No Model", "No Model"))
        'ddlAstModel.Items.Insert(0, "--All--")
    End Sub

    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--Select--")
        'ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstBrand.Items.Insert(0, New ListItem("No Brand", "No Brand"))
    End Sub

    Dim dsCIRGrid As New DataSet()
    Private Sub BindGridSub()
        'Dim tickedcount = 0
        Dim ReqId As String = Session("REQ_ID") 'Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMGITEM_VIEW_CAPITAL_ASSET_REQUISITION")
        sp.Command.AddParameter("@AST_MD_CATID", CatId, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", asstsubcat, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", asstbrand, DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", astmodel, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQID", ReqId, DbType.String)
        dsCIRGrid = sp.GetDataSet
        gvCaptApprovals.DataSource = dsCIRGrid

        gvCaptApprovals.DataBind()

    End Sub

    Private Sub BindUsers()

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")

        param(1) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("COMPANYID")

        ObjSubsonic.Binddropdown(ddlEmp, "AM_AMT_bindUsers_SP", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(Session("UID"))
        If Not li Is Nothing Then
            li.Selected = True
        End If
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Session("REQ_ID")
        Return ReqId
    End Function

    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Dim ReqId As String = GetRequestId()
        Dim strASSET_LIST As New ArrayList
        UpdateData(ReqId, Trim(txtL2Remarks.Text))
        'DeleteRequistionItems(ReqId)
        Dim i As Integer = 0
        For Each row As GridViewRow In gvCaptApprovals.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtEstCost As TextBox = DirectCast(row.FindControl("txtEstCost"), TextBox)
            If chkSelect.Checked Then
                InsertDetails(ReqId, lblProductId.Text, CInt(Trim(txtQty.Text)), CInt(Trim(txtEstCost.Text)), i)

                'strASSET_LIST.Insert(i, lblProductId.Text & "," & lblProductname.Text & "," & txtQty.Text)
            End If
            i += 1
        Next

        Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId())
    End Sub

    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAP_AssetRequisition_APP_REJ_BY_L2")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@STATUS", 3015, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer, ByVal EstCost As Integer, ByVal id As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_updatebyrm")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.Command.AddParameter("@EST_COST", EstCost, DbType.Int32)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.Command.AddParameter("@ITEM_TYPE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@ITEM_MOD", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@delsno", id, DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        RejectData(GetRequestId, Trim(txtL2Remarks.Text))
        Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId())
    End Sub

    Private Sub RejectData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CAP_AssetRequisition_APP_REJ_BY_L2")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@STATUS", 1011, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@CompanyId", Session("CompanyId"), DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        lblMsg.Visible = False
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_LEVEL2_APPROVAL_SEARCH")
        sp.Command.AddParameter("@search_criteria", txtSearch.Text, Data.DbType.String)
        sp.Command.AddParameter("@AURID", Session("UID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
    End Sub


    'Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
    '    CachedInterGrid = CType(Cache("CdtInterGrid"), DataTable)
    '    CachedtIntraGrid = CType(Cache("CdtIntraGrid"), DataTable)
    '    '********* old code *******************

    '    'Dim ReqId As String = GetRequestId()
    '    ''DeleteRequistionItems(ReqId)
    '    'For Each row As GridViewRow In gvItems.Rows
    '    '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
    '    '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
    '    '    Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
    '    '    If txtStockQty.Text = "" Then
    '    '        txtStockQty.Text = 0
    '    '    End If
    '    '    Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
    '    '    If txtPurchaseQty.Text = "" Then
    '    '        txtPurchaseQty.Text = 0
    '    '    End If
    '    '    UpdateAssetRequisitionData(ReqId, CInt(Trim(lblProductId.Text)), CInt(Trim(txtStockQty.Text)), CInt(Trim(txtPurchaseQty.Text)))
    '    'Next
    '    'UpdateData(ReqId, 1010)
    '    'Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
    '    '*******************************************************

    '    '**************** modified on 29 aug 11 ******************
    '    '**************** modified on 12 may 2016 ****************

    '    Dim VT_Code As String
    '    Dim Ast_SubCat_Code As String
    '    Dim manufactuer_code As String
    '    Dim Ast_Md_code As String

    '    Dim ReqId As String = GetRequestId()
    '    Dim cnt As Integer = 0
    '    For Each row As GridViewRow In gvCaptApprovals.Rows
    '        Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
    '        Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
    '        Dim lblProductname As Label = DirectCast(row.FindControl("lblProductname"), Label)
    '        Dim lblStock As Label = DirectCast(row.FindControl("lblStock"), Label)
    '        Dim lblStockmsg As Label = DirectCast(row.FindControl("lblStockmsg"), Label)
    '        Dim lblApprQty As Label = DirectCast(row.FindControl("lblApprQty"), Label)
    '        Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
    '        Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
    '        Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)


    '        Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
    '        Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
    '        Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
    '        Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)


    '        VT_Code = lbl_vt_code.Text
    '        Ast_SubCat_Code = lbl_ast_subcat_code.Text
    '        manufactuer_code = lbl_manufactuer_code.Text
    '        Ast_Md_code = lbl_ast_md_code.Text

    '        If txtStockQty.Text = "" Then
    '            txtStockQty.Text = 0
    '        End If

    '        'If CInt(Trim(txtStockQty.Text)) > CInt(Trim(lblStock.Text)) Then
    '        '    lblMsg.Visible = True
    '        '    lblMsg.Text = "Asset stock not available."

    '        '    Exit Sub
    '        'End If
    '        If txtPurchaseQty.Text = "" Then
    '            txtPurchaseQty.Text = 0
    '        End If

    '        If chkSelect.Checked Then
    '            If Regex.IsMatch(txtPurchaseQty.Text, "^[0-9 ]+$") Then
    '                If CInt(Trim(txtPurchaseQty.Text)) > 0 Then
    '                    If CInt(Trim(txtPurchaseQty.Text)) <= CInt(txtQty.Text) Then
    '                        InsertAssetRequisitionData(ReqId, Trim(lblProductId.Text), CInt(Trim(txtQty.Text)), CInt(Trim(txtStockQty.Text)),
    '                                                   CInt(Trim(txtPurchaseQty.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code)
    '                        UpdateAssetRequisitionData(ReqId, Trim(lblProductId.Text), CInt(Trim(txtStockQty.Text)), CInt(Trim(txtPurchaseQty.Text)))
    '                    Else
    '                        lblMsg.Visible = True
    '                        lblMsg.Text = "Purchase qty. Should be lessthan than order qty."
    '                        Exit Sub
    '                    End If
    '                Else
    '                    lblMsg.Visible = True
    '                    lblMsg.Text = "Qty to purchase Should be greater than zero"
    '                    Exit Sub
    '                End If
    '            Else
    '                lblMsg.Visible = True
    '                lblMsg.Text = "Please enter Qty to purchase in numbers..."
    '                Exit Sub
    '            End If
    '        End If

    '        If txtPurchaseQty.Text = "" Then
    '            txtPurchaseQty.Text = 0
    '        End If
    '        cnt = cnt + 1
    '    Next

    '    If CachedtIntraGrid Is Nothing Then
    '    Else
    '        For I As Integer = 0 To CachedtIntraGrid.Rows.Count - 1
    '            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_AMT_ASSET_SPACE_STAT")
    '            sp1.Command.AddParameter("@LOC_ID", CachedtIntraGrid.Rows(I).Item("LOC_ID"), DbType.String)
    '            sp1.Command.AddParameter("@TWR_ID", "", DbType.String)
    '            sp1.Command.AddParameter("@FLR_ID", "", DbType.String)
    '            sp1.Command.AddParameter("@AAT_AST_CODE", CachedtIntraGrid.Rows(I).Item("AAT_AST_CODE"), DbType.String)
    '            sp1.ExecuteScalar()


    '            ' ALLOCATE_SPACEASSET
    '            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ALLOCATE_SPACEASSET_INTRA")
    '            sp.Command.AddParameter("@AAT_AST_CODE", CachedtIntraGrid.Rows(I).Item("AAT_AST_CODE"), DbType.String)
    '            sp.Command.AddParameter("@AAT_SPC_ID", "", DbType.String)
    '            sp.Command.AddParameter("@AAT_EMP_ID", CachedtIntraGrid.Rows(I).Item("AAT_EMP_ID"), DbType.String)
    '            sp.Command.AddParameter("@AAT_ITEM_REQUISITION", CachedtIntraGrid.Rows(I).Item("AAT_ITEM_REQUISITION"), DbType.String)
    '            sp.ExecuteScalar()

    '            '--------------- updating the asset tagged table to 1035 (Assigned)-------------
    '            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPDATE_ALLOCATE_ASSET")
    '            sp2.Command.AddParameter("@AAT_AST_CODE", CachedtIntraGrid.Rows(I).Item("AAT_AST_CODE"), DbType.String)
    '            sp2.Command.AddParameter("@AAT_EMP_ID", CachedtIntraGrid.Rows(I).Item("AAT_EMP_ID"), DbType.String)
    '            sp2.Command.AddParameter("@AAT_REQ_ID", CachedtIntraGrid.Rows(I).Item("AAT_REQ_ID"), DbType.String)
    '            sp2.ExecuteScalar()
    '        Next
    '    End If

    '    If CachedInterGrid Is Nothing Then
    '    ElseIf CachedInterGrid.Rows.Count > 0 Then
    '        'RAISE INTER MOVEMENT REQUISITION START
    '        Dim AstParid As String

    '        'PARENT
    '        Dim interreqid As String = "MMR" + "/" + ddlLocation.SelectedValue + "/" + Session("uid") + "/"
    '        Dim spParent As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ASSET_MVMT_REQUISITION")
    '        spParent.Command.AddParameter("@CREATEDBY", Session("uid"), DbType.String)
    '        spParent.Command.AddParameter("@REQID", ReqId, DbType.String)
    '        spParent.Command.AddParameter("@MMR_ITEM_REQUISITION", Request("RID"), DbType.String)
    '        spParent.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
    '        spParent.Command.AddParameter("@FROMLOC", ddlLocation.SelectedValue, DbType.String)
    '        AstParid = spParent.ExecuteScalar()



    '        If cnt = gvItems.Rows.Count Then
    '            CachedInterGrid = CType(Cache("CdtInterGrid"), DataTable)
    '            For I As Integer = 0 To CachedInterGrid.Rows.Count - 1
    '                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & ".AM_USP_MMT_INTRA_MVMT_REQ_Insert" & "")
    '                sp.Command.AddParameter("@MMR_REQ_ID", AstParid, DbType.String)
    '                'sp.Command.AddParameter("@MMR_AST_CODE", CachedInterGrid.Rows(I).Item("AST_MD_CODE"), DbType.String)
    '                'sp.Command.AddParameter("@MMR_FROMBDG_ID", CachedInterGrid.Rows(I).Item("LCM_CODE"), DbType.String)
    '                'sp.Command.AddParameter("@MMR_FROMFLR_ID", "", DbType.String)
    '                'sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
    '                'sp.Command.AddParameter("@MMR_RECVD_BY", Session("UID"), DbType.String)
    '                'sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
    '                'sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
    '                'sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1017, DbType.Int32)
    '                'sp.Command.AddParameter("@MMR_COMMENTS", "", DbType.String)
    '                'sp.Command.AddParameter("@MMR_FROMEMP_ID", 0, DbType.String)
    '                'sp.Command.AddParameter("@MMR_ITEM_REQUISITION", Request("RID"), DbType.String)
    '                sp.Command.AddParameter("@ASSETCODE", CachedInterGrid.Rows(I).Item("MMR_AST_CODE"), DbType.String)
    '                sp.Command.AddParameter("@RAISEDBY", Session("uid"), DbType.String)
    '                sp.Command.AddParameter("@COMMENT", "", DbType.String)
    '                sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
    '                sp.ExecuteScalar()
    '            Next




    '        Else
    '            Exit Sub
    '        End If

    '    End If
    '    Dim IntraCount As Integer
    '    Dim InterCount As Integer

    '    If CachedtIntraGrid Is Nothing Then
    '        IntraCount = 0
    '    Else
    '        IntraCount = CachedtIntraGrid.Rows.Count
    '    End If

    '    If CachedInterGrid Is Nothing Then
    '        InterCount = 0
    '    Else
    '        InterCount = CachedInterGrid.Rows.Count
    '    End If

    '    UpdateDataa(ReqId, 3015)
    '    Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId + "&IntraCount=" + Convert.ToString(IntraCount) + "&InterCount=" + Convert.ToString(InterCount))
    '    'Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
    'End Sub

    Private Sub UpdateDataa(ByVal ReqId As String, ByVal StatusId As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_UPDATE_BY_COORDINATOR_CHECK")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertAssetRequisitionData(ByVal strReqid As String, ByVal ProductId As String, ByVal ProductReqQty As Integer,
                                           ByVal StockQty As Integer, ByVal PurchaseQty As Integer, ByVal VT_CODE As String,
        ByVal AST_SUBCAT_CODE As String, ByVal manufactuer_code As String, ByVal AST_MD_CODE As String)

        Dim strPurReqId As String = ""
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strReqid
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("INSERT_PURCHASE_ITEMREQ", param)
        If ds.Tables(0).Rows.Count > 0 Then
            strPurReqId = ds.Tables(0).Rows(0).Item("PUR_REQ_ID")
        End If
        Dim paramItemDetails(8) As SqlParameter
        paramItemDetails(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
        paramItemDetails(0).Value = strPurReqId
        paramItemDetails(1) = New SqlParameter("@ProductId", SqlDbType.NVarChar, 200)
        paramItemDetails(1).Value = ProductId
        paramItemDetails(2) = New SqlParameter("@SQty", SqlDbType.NVarChar, 200)
        paramItemDetails(2).Value = StockQty
        paramItemDetails(3) = New SqlParameter("@PQty", SqlDbType.NVarChar, 200)
        paramItemDetails(3).Value = PurchaseQty
        paramItemDetails(4) = New SqlParameter("@ProductREQQty", SqlDbType.Int)
        paramItemDetails(4).Value = ProductReqQty

        paramItemDetails(5) = New SqlParameter("@AID_ITEM_TYPE", SqlDbType.NVarChar, 200)
        paramItemDetails(5).Value = VT_CODE

        paramItemDetails(6) = New SqlParameter("@AID_ITEM_SUBCAT", SqlDbType.NVarChar, 200)
        paramItemDetails(6).Value = AST_SUBCAT_CODE

        paramItemDetails(7) = New SqlParameter("@AID_ITEM_BRD", SqlDbType.NVarChar, 200)
        paramItemDetails(7).Value = manufactuer_code

        paramItemDetails(8) = New SqlParameter("@AID_ITEM_MOD", SqlDbType.NVarChar, 200)
        paramItemDetails(8).Value = AST_MD_CODE

        ObjSubsonic.GetSubSonicExecute("INSERT_PURCHASE_REQ_DETAILS", paramItemDetails)



        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_UpdateStockPurchaseQtyByReqIdAndProductId")
        'sp.Command.AddParameter("@ReqId", strPurReqId, DbType.String)
        'sp.Command.AddParameter("@ProductId", ProductId, DbType.Int32)
        'sp.Command.AddParameter("@SQty", StockQty, DbType.Int32)
        'sp.Command.AddParameter("@PQty", PurchaseQty, DbType.Int32)
        'sp.ExecuteScalar()
    End Sub


    Private Sub UpdateAssetRequisitionData(ByVal ReqId As String, ByVal ProductId As String, ByVal StockQty As Integer, ByVal PurchaseQty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_UpdateStockPurchaseQtyByReqIdAndProductId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@SQty", StockQty, DbType.Int32)
        sp.Command.AddParameter("@PQty", PurchaseQty, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.Command.AddParameter("@USERID", Session("UID"))
        sp.ExecuteScalar()
    End Sub

End Class


