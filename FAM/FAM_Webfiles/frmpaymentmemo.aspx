<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmpaymentmemo.aspx.vb" Inherits="FAM_FAM_Webfiles_frmpaymentmemo"
    Title="Payment Memo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black"> Asset Payment Memo
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Asset Payment Memo</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                <table border="0" width="100%" align="center">
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblmsg" CssClass="error" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Select PO Request<font class="clsNote">*</font></td>
                        <td>
                            <asp:DropDownList ID="ddlAsset" runat="server" CssClass="clsComboBox" Width="100%"
                                AutoPostBack="True">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td>
                            Payment<font class="clsNote">*</font></td>
                        <td>
                            <b>
                                <asp:Label ID="lblPayment" runat="server"></asp:Label></b> (INR)</td>
                    </tr>
                    <tr>
                        <td>
                            Payment paid till date<font class="clsNote">*</font></td>
                        <td>
                            <b>
                                <asp:Label ID="lblPaymentPaid" runat="server"></asp:Label></b> (INR)</td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <fieldset id="Fieldset1" visible="false" runat="server">
                                <legend>Previously Paid History</legend>
                                <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False" AllowPaging="True"
                                    EmptyDataText="No Assets Found" Width="100%">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Mode of Payment">
                                            <ItemTemplate>
                                            <asp:Label ID="lblID" runat="server" Visible="false" Text='<%# Eval("id") %>'></asp:Label>
                                                <asp:LinkButton ID="lnkPAYMENT_MODE" runat="server" Text='<%# Eval("PAYMENT_MODE") %>'></asp:LinkButton>
                                                <asp:Label ID="lblPAYMENT_MODE" runat="server" Text='<%# Eval("PAYMENT_MODE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Paid Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpaiddt" runat="server" Text='<%# Eval("PAID_DT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount (INR)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaidAmt" runat="server" Text='<%# Eval("PAYMENT") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mode Of Payment<font class="clsNote">*</font></td>
                        <td>
                            <asp:DropDownList ID="dllPayMode" runat="server" CssClass="clsComboBox" Width="100%"
                                AutoPostBack="True">
                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                <asp:ListItem Value="Cheque">Cheque</asp:ListItem>
                                <asp:ListItem Value="DD">Demand Draft</asp:ListItem>
                                <asp:ListItem Value="NEFT_RTGS">NEFT/RTGS</asp:ListItem>
                                <asp:ListItem Value="Cash">Cash</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr id="trDDChkDetails" runat="server" visible="false">
                        <td colspan="2">
                            <fieldset>
                                <legend>
                                    <asp:Label ID="lbltitle" runat="server"></asp:Label></legend>
                                <table border="0" width="100%" align="center">
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblNumber" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDDChqNumber" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblDt" runat="server"></asp:Label>
                                            <font class="clsNote">*</font></td>
                                        <td>
                                            <asp:TextBox ID="txtchqDt" runat="server"></asp:TextBox>
                                            <cc1:CalendarExtender TargetControlID="txtchqDt" ID="CalendarExtender1" runat="server">
                                            </cc1:CalendarExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Bank Name<font class="clsNote">*</font></td>
                                        <td>
                                            <asp:TextBox ID="txtBankName" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                            Branch Name<font class="clsNote">*</font></td>
                                        <td>
                                            <asp:TextBox ID="txtBranchName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr id="TRACNUMBER" runat="server" visible="false">
                                        <td>
                                            Account Number<font class="clsNote">*</font></td>
                                        <td>
                                            <asp:TextBox ID="txtAccountNumber" runat="server"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Balance Payment<font class="clsNote">*</font></td>
                        <td>
                            <b>
                                <asp:Label ID="lblBalPay" runat="server"></asp:Label></b> (INR)</td>
                    </tr>
                    <tr id="trpaid1" runat="server">
                        <td>
                            Payment Amount<font class="clsNote">*</font></td>
                        <td>
                            <asp:TextBox ID="txtPayAmt" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr id="trpaid2" runat="server">
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="clsButton" /></td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</asp:Content>
