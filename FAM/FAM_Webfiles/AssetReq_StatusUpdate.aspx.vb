Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_AssetReq_StatusUpdate
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strReqId As String
    Dim ReqStatus As String = ""
    Dim strRole As String = ""
    Dim strpage As String = ""


    Dim MMR_AST_CODE, MMR_FROMBDG_ID, MMR_FROMFLR_ID, LCM_CODE, LCM_NAME As String
    Dim TOEMP_ID As Integer


    Dim FBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor As String
    Dim receiveAst, strremarks As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strReqId = GetRequestId()
        ReqStatus = Request.QueryString("st")
        strRole = Request.QueryString("rl")
        strpage = Request.QueryString("pg")
        If Not IsPostBack Then
            If strpage.ToLower = "interit" Then
                pnlIntraMovementITApproval_Reject.Visible = False
                Panel1.Visible = False
                pnlInterMovementITApproval_Reject.Visible = True
                AssetInterMovementITApprovaldetails()
                ddlSLocInterMovementITAp_Rj.Enabled = False
                ddlSTowerInterMovementITAp_Rj.Enabled = False
                ddlSFloorInterMovementITAp_Rj.Enabled = False
                ddlEmpInterMovementITAp_Rj.Enabled = False
            ElseIf strpage.ToLower = "intrait" Then

                pnlInterMovementITApproval_Reject.Visible = False
                Panel1.Visible = False

                pnlIntraMovementITApproval_Reject.Visible = True
                AssetIntraMovementITApprovalDetails()
                getDetailsbyReqId(strReqId)
                BindLocations()
                BindRequestAssets(strReqId)

            Else
                Panel1.Visible = True
                pnlInterMovementITApproval_Reject.Visible = False
                pnlIntraMovementITApproval_Reject.Visible = False
                AssetRequisition()
            End If
        End If
    End Sub

    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItemIntraMovementITApproval_Reject, "GET_MVMT_ASTS", param)
    End Sub


    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)
        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("GET_INTRA_MVMT_REQID", param)
        If dr.Read Then
            FBDG_ID = dr.Item("FLOC_CODE")
            FTower = dr.Item("MMR_FROMBDG_ID")
            FFloor = dr.Item("MMR_FROMFLR_ID")
            TBDG_ID = dr.Item("FLOC_NAME")
            TTower = dr.Item("MMR_TOBDG_ID")
            TFloor = dr.Item("MMR_TOFLR_ID")
            receiveAst = dr.Item("MMR_RECVD_BY_name")
            strremarks = dr.Item("MMR_COMMENTS")
        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If
        txtPersonName.Text = receiveAst
    End Sub

    Private Sub BindLocations()
        '----------- Binding Location-----------------------
        ObjSubsonic.Binddropdown(ddlSLocIntraMovementITApproval_Reject, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")
        ObjSubsonic.Binddropdown(ddlDLocIntraMovementITApproval_Reject, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")

        '------------ From Building/Tower/Floor -----------------------------
        If FBDG_ID = "" Or FBDG_ID Is Nothing Then

        Else
            ddlSLocIntraMovementITApproval_Reject.Items.FindByValue(FBDG_ID).Selected = True

            '----------- Binding Tower -----------------------
            If ddlSLocIntraMovementITApproval_Reject.SelectedIndex > 0 Then
                Dim LocCode As String = ddlSLocIntraMovementITApproval_Reject.SelectedItem.Value
                BindTowersByLocation(LocCode, ddlSTowerIntraMovementITApproval_Reject)
            End If
            ddlSTowerIntraMovementITApproval_Reject.Items.FindByValue(FTower).Selected = True
            '-------------------------------------------------

            If ddlSTowerIntraMovementITApproval_Reject.SelectedIndex > 0 Then
                Dim TwrCode As String = ddlSTowerIntraMovementITApproval_Reject.SelectedItem.Value
                BindFloorsByTower(TwrCode, ddlSFloorIntraMovementITApproval_Reject)
            End If
            ddlSFloorIntraMovementITApproval_Reject.Items.FindByValue(FFloor).Selected = True

        End If
        '------------ From Building/Tower/Floor -------------------------
        If TBDG_ID = "" Or TBDG_ID Is Nothing Then

        Else
            ddlDLocIntraMovementITApproval_Reject.Items.FindByValue(TBDG_ID).Selected = True

            '----------- Binding Tower -----------------------
            If ddlDLocIntraMovementITApproval_Reject.SelectedIndex > 0 Then
                Dim LocCode As String = ddlDLocIntraMovementITApproval_Reject.SelectedItem.Value
                BindTowersByLocation(LocCode, ddlDTowerIntraMovementITApproval_Reject)
            End If
            ddlDTowerIntraMovementITApproval_Reject.Items.FindByValue(TTower).Selected = True
            '-------------------------------------------------

            If ddlDTowerIntraMovementITApproval_Reject.SelectedIndex > 0 Then
                Dim TwrCode As String = ddlDTowerIntraMovementITApproval_Reject.SelectedItem.Value
                BindFloorsByTower(TwrCode, ddlDFloorIntraMovementITApproval_Reject)
            End If
            ddlDFloorIntraMovementITApproval_Reject.Items.FindByValue(TFloor).Selected = True

        End If
    End Sub


    Private Sub BindFloorsByTower(ByVal TwrCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@TwrCode", DbType.String)
        param(0).Value = TwrCode
        ObjSubsonic.Binddropdown(ddl, "USP_FLOOR_GETBYTOWER", "FLR_NAME", "FLR_CODE", param)
    End Sub


    Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
        param(0).Value = LocCode
        ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    End Sub


    Private Sub AssetIntraMovementITApprovalDetails()
        If ReqStatus.ToLower = "ap" Then
            Label1.Text = "Intra Movement IT Approval details"
            Label2.Text = "Intra Movement IT Approval details"
            lblTitle.Text = "Intra Movement IT Approval details"
            btnSubmit.Text = "Approve"
        Else
            Label1.Text = "Intra Movement IT details -- Rejection "
            Label2.Text = "Intra Movement IT details -- Rejection "
            lblTitle.Text = "Intra Movement IT details -- Rejection "
            btnSubmit.Text = "Reject"
        End If
        BindDetails()
    End Sub

    Private Sub AssetInterMovementITApprovaldetails()
        If strRole.ToLower = "rm" And ReqStatus.ToLower = "ap" Then
            Label1.Text = "Inter Movement IT Approval details"
            Label2.Text = "Inter Movement IT Approval details"
            lblTitle.Text = "Inter Movement IT Approval details"
            btnSubmit.Text = "Approve"
        ElseIf strRole.ToLower = "rm" Then
            Label1.Text = "Inter Movement IT details -- Rejection "
            Label2.Text = "Inter Movement IT details -- Rejection "
            lblTitle.Text = "Inter Movement IT details -- Rejection "
            btnSubmit.Text = "Reject"
        End If
        BindDetails()
    End Sub

    Public Sub BindDetails()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Trim(Request.QueryString("RID"))

        Dim dr As SqlDataReader
        dr = ObjSubsonic.GetSubSonicDataReader("GET_REQID_InterMOVEMENTS_byREQ_ID", param)
        If dr.Read Then
            MMR_AST_CODE = dr.Item("MMR_AST_CODE")
            MMR_FROMBDG_ID = dr.Item("MMR_FROMBDG_ID")
            MMR_FROMFLR_ID = dr.Item("MMR_FROMFLR_ID")
            LCM_CODE = dr.Item("FLOC_CODE")
            LCM_NAME = dr.Item("FLOC_NAME")
            TOEMP_ID = dr.Item("TOEMP_ID")

            BindLocationsInterMovementITApproval(LCM_CODE)
            BindTowerInterMovementITApproval(MMR_FROMBDG_ID)
            BindFloorInterMovementITApproval(MMR_FROMFLR_ID)
            'ddlEmp.Items.Insert(0, New ListItem(dr("MMR_TOEMP_ID"), "0"))

            'ddlSLoc.Enabled = False
            'ddlSTower.Enabled = False
            'ddlSFloor.Enabled = False
            'ddlEmp.Enabled = False

        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If


    End Sub


    Private Sub BindLocationsInterMovementITApproval(ByVal LCM_CODE As String)
        ObjSubsonic.Binddropdown(ddlSLocInterMovementITAp_Rj, "USP_Location_GetAll", "LCM_NAME", "LCM_CODE")
        ddlSLocInterMovementITAp_Rj.Items.FindByValue(LCM_CODE).Selected = True
    End Sub
    Private Sub BindTowerInterMovementITApproval(ByVal AAS_BDG_ID As String)
        '----------- Binding Tower -----------------------
        If ddlSLocInterMovementITAp_Rj.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLocInterMovementITAp_Rj.SelectedItem.Value
            BindTowersByLocationInterMovementITApproval(LocCode, ddlSTowerInterMovementITAp_Rj)
        End If
        ddlSTowerInterMovementITAp_Rj.Items.FindByValue(AAS_BDG_ID).Selected = True
        '-------------------------------------------------
    End Sub

    Private Sub BindTowersByLocationInterMovementITApproval(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
        param(0).Value = LocCode
        ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    End Sub

    Public Sub BindFloorInterMovementITApproval(ByVal AAS_FLR_ID As String)
        If ddlSTowerInterMovementITAp_Rj.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlSTowerInterMovementITAp_Rj.SelectedItem.Value
            BindFloorsByTowerBindEmpInterMovementITAp_Rj(TwrCode, ddlSFloorInterMovementITAp_Rj)
        End If
        ddlSFloorInterMovementITAp_Rj.Items.FindByValue(AAS_FLR_ID).Selected = True
        BindEmpInterMovementITAp_Rj()
    End Sub

    Private Sub BindEmpInterMovementITAp_Rj()
        ObjSubsonic.Binddropdown(ddlEmpInterMovementITAp_Rj, "GET_USERS", "AUR_FIRST_NAME", "AUR_ID")
        ddlEmpInterMovementITAp_Rj.Items.FindByValue(TOEMP_ID).Selected = True
    End Sub

    Private Sub BindFloorsByTowerBindEmpInterMovementITAp_Rj(ByVal TwrCode As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@TwrCode", DbType.String)
        param(0).Value = TwrCode
        ObjSubsonic.Binddropdown(ddl, "USP_FLOOR_GETBYTOWER", "FLR_NAME", "FLR_CODE", param)
    End Sub

    Private Sub AssetRequisition()
        If strRole.ToLower = "rm" And ReqStatus.ToLower = "ap" Then
            Label1.Text = "Asset Request Approval"
            Label2.Text = "Asset Request Approval"
            lblTitle.Text = "Asset Request Approval"
            'lblRemarks.Text = "RM Remarks"
            btnSubmit.Text = "Approve"
            BindCategories()
            BindRequisition()
        ElseIf strRole.ToLower = "rm" Then
            Label1.Text = "Asset Request Rejection"
            Label2.Text = "Asset Request Rejection"
            lblTitle.Text = "Asset Request Rejection"
            'lblRemarks.Text = "RM Remarks"
            btnSubmit.Text = "Reject"
            BindCategories()
            BindRequisition()
        End If
    End Sub

    Private Sub BindCategories()
        GetChildRows("0")
        ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub
    Private Sub GetChildRows(ByVal i As String)
        Dim str As String = ""
        Dim id
        If i = "0" Then
            id = CType(i, Integer)
        Else
            Dim id1 As Array = Split(i, "~")
            str = id1(0).ToString & "  --"
            id = CType(id1(1), Integer)
        End If
        Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
        Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
        Dim ds As New DataSet
        da.Fill(ds)
        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim j As Integer = CType(dr("CategoryId"), Integer)
                If id = 0 Then
                    str = ""
                End If
                Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
                ddlAstCat.Items.Add(li)
                GetChildRows(str & "~" & j)
            Next
        End If
    End Sub
    Private Sub BindRequisition()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            lblmsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId
                Dim RaisedBy As Integer = 0
                Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)
                BindUsers(RaisedBy)
                Dim li As ListItem '= ddlEmp.Items.FindByValue(CStr(RaisedBy))
                'If Not li Is Nothing Then
                '    li.Selected = True
                'End If

                Dim CatId As Integer = 0
                Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)

                li = ddlAstCat.Items.FindByValue(CStr(CatId))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                ddlAstCat.Enabled = False
                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")

                txtStatus.Text = dr("STA_TITLE")

                'If String.IsNullOrEmpty(Trim(txtRemarks.Text)) Then
                '    tr3.Visible = False
                'Else
                '    tr3.Visible = True
                'End If



                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)


                BindGrid()

                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
                    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_GetDetailsByReqIdAndProductId")
                    sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
                    sp1.Command.AddParameter("@ProductId", CInt(lblProductId.Text), DbType.Int32)
                    Dim dr1 As SqlDataReader = sp1.GetReader
                    If dr1.Read() Then
                        chkSelect.Checked = True
                        txtQty.Text = dr1("AID_QTY")
                        If txtQty.Text = "NULL" Or txtQty.Text = "" Or txtQty.Text = 0 Then
                            row.Visible = False
                        Else
                            row.Visible = True
                        End If
                    Else
                        row.Visible = False
                    End If
                Next
            Else
                lblmsg.Text = "No such requisition found."
            End If
        End If
    End Sub
    Private Sub BindUsers(ByVal aur_id As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = aur_id
        ObjSubsonic.Binddropdown(ddlEmp, "AMT_BINDUSERS_SP_Raisedby", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(aur_id)
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub
    Private Sub BindGrid()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_REQUISITION", param)
    End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("rid")
        Return ReqId
    End Function
    
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If txtRemarks.Text = "" Then
            lblmsg.Text = "Remarks mandatory."
            lblmsg.Visible = True
            Exit Sub
        Else
            lblmsg.Text = ""
            lblmsg.Visible = False
        End If

        If strpage.ToLower = "interit" Then
            If ReqStatus.ToLower = "ap" Then

                lblmsg.Text = "Asset Inter movement Successfully approved."
                lblmsg.Visible = True
                InterMovement_IT_Approval()
            ElseIf ReqStatus.ToLower = "rj" Then
                lblmsg.Text = "Asset Inter movement Successfully rejected."
                lblmsg.Visible = True
                InterMovement_IT_Reject()
            End If
        ElseIf strpage.ToLower = "intrait" Then
            If ReqStatus.ToLower = "ap" Then
                IntraMovement_IT_Approval()
            ElseIf ReqStatus.ToLower = "rj" Then
                IntraMovement_IT_Reject()
            End If
        Else
            If strRole.ToLower = "rm" And ReqStatus.ToLower = "ap" Then
                UpdateAssetRequisitionStatus(GetRequestId, txtRemarks.Text, 1004)
            ElseIf strRole.ToLower = "rm" Then
                UpdateAssetRequisitionStatus(GetRequestId, txtRemarks.Text, 1005)
            End If

        End If
    End Sub

#Region "InterMovement"
    Private Sub InterMovement_IT_Approval()
        Dim strASSET_LIST As New ArrayList
        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
        param(1).Value = Request.QueryString("uid")
        param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
        param(3).Value = 1024
        ObjSubsonic.GetSubSonicExecute("UPDATE_INTER_MVMT_REQ_ITSTATUS", param)
        Dim param1(0) As SqlParameter
        param1(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param1(0).Value = Request.QueryString("RID")
        ObjSubsonic.GetSubSonicExecute("UPDATE_AMT_ASSET_SPACE_INTER_MVMT", param1)





        Dim param_Details(0) As SqlParameter
        param_Details(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param_Details(0).Value = Trim(Request.QueryString("RID"))

        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_REQID_InterMOVEMENTS_byREQ_ID", param_Details)
        If ds.Tables(0).Rows.Count > 0 Then
            MMR_AST_CODE = ds.Tables(0).Rows(0).Item("MMR_AST_CODE")
            MMR_FROMBDG_ID = ds.Tables(0).Rows(0).Item("MMR_FROMBDG_ID")
            MMR_FROMFLR_ID = ds.Tables(0).Rows(0).Item("MMR_FROMFLR_ID")
            LCM_CODE = ds.Tables(0).Rows(0).Item("FLOC_CODE")
            LCM_NAME = ds.Tables(0).Rows(0).Item("FLOC_NAME")
            TOEMP_ID = ds.Tables(0).Rows(0).Item("TOEMP_ID")
        End If
        Dim i As Integer = 0
        strASSET_LIST.Insert(i, MMR_AST_CODE & "," & LCM_NAME & "," & MMR_FROMBDG_ID & "," & MMR_FROMFLR_ID & "," & txtRemarks.Text & "," & ddlEmpInterMovementITAp_Rj.SelectedItem.Text)
        Dim MailTemplateId As Integer
        MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetInterMovementRequisition_it_appoval"))
        SendMail_InterMovementITApproval_Reject(Trim(Request.QueryString("RID")), strASSET_LIST, Request.QueryString("uid"), txtRemarks.Text, MailTemplateId, True)
    End Sub

    Private Sub InterMovement_IT_Reject()
        Dim strASSET_LIST As New ArrayList
        Dim param_Details(0) As SqlParameter
        param_Details(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param_Details(0).Value = Trim(strReqId)

        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("GET_REQID_InterMOVEMENTS_byREQ_ID", param_Details)
        If ds.Tables(0).Rows.Count > 0 Then
            MMR_AST_CODE = ds.Tables(0).Rows(0).Item("MMR_AST_CODE")
            MMR_FROMBDG_ID = ds.Tables(0).Rows(0).Item("MMR_FROMBDG_ID")
            MMR_FROMFLR_ID = ds.Tables(0).Rows(0).Item("MMR_FROMFLR_ID")
            LCM_CODE = ds.Tables(0).Rows(0).Item("FLOC_CODE")
            LCM_NAME = ds.Tables(0).Rows(0).Item("FLOC_NAME")
            TOEMP_ID = ds.Tables(0).Rows(0).Item("TOEMP_ID")
        End If




        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strReqId
        param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
        param(1).Value = Request.QueryString("uid")
        param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
        param(3).Value = 1025
        ObjSubsonic.GetSubSonicExecute("UPDATE_INTER_MVMT_REQ_ITSTATUS", param)



        Dim i As Integer = 0

        strASSET_LIST.Insert(i, MMR_AST_CODE & "," & LCM_NAME & "," & MMR_FROMBDG_ID & "," & MMR_FROMFLR_ID & "," & txtRemarks.Text & "," & ddlEmp.SelectedItem.Text)

        Dim MailTemplateId As Integer
        MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetInterMovementRequisition_it_reject"))
        SendMail_InterMovementITApproval_Reject(Trim(Request.QueryString("RID")), strASSET_LIST, Request.QueryString("uid"), txtRemarks.Text, MailTemplateId, True)
    End Sub

    Private Sub SendMail_InterMovementITApproval_Reject(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = objsubsonic.GetSubSonicDataSet("GET_ASSET_INTER_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If

            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty
            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = objsubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If
            Dim p1
            p1 = AstList.Item(0).ToString.Split(",")
            strAstList = "<table width='100%' border='1'>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(0) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(3) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Person To Receive Assets</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(5) & "</td></tr>"


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&rl=rm&ty=imr> Click to Approve </a></td></tr></table>"

            strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&rl=rm&ty=imr>  Click to Reject </a></td></tr></table>"

            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)
            body = body.Replace("@@Approve", strApproveList)
            body = body.Replace("@@Reject", strRejectList)

            Insert_AmtMail(body, strRR, strSubject, strRM)

        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "InterMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = getoffsetdatetime(DateTime.Now)
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub


#End Region

#Region "IntraMovement"

    Private Sub IntraMovement_IT_Approval()
        Dim strASSET_LIST As New ArrayList

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strReqId
        param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
        param(1).Value = Request.QueryString("uid")
        param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
        param(3).Value = 1017
        ObjSubsonic.GetSubSonicExecute("UPDATE_MMT_MVMT_REQ_APPROVE_reqId", param)

        For i As Integer = 0 To gvItemIntraMovementITApproval_Reject.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItemIntraMovementITApproval_Reject.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItemIntraMovementITApproval_Reject.Rows(i).FindControl("lblAAT_NAME"), Label)
            strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLocIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlSTowerIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlSFloorIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlDLocIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlDTowerIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlDFloorIntraMovementITApproval_Reject.SelectedItem.Text & "," & txtRemarks.Text)
        Next

        Dim MailTemplateId As Integer
        MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_it_approval"))
        'getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)
        SendMail_IntraMovement(strReqId, strASSET_LIST, Request.QueryString("uid"), txtRemarks.Text, MailTemplateId, True)



        MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_Outwardentrydetails_approve_reject"))
        'getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)
        SendMail_IntraMovement(strReqId, strASSET_LIST, Request.QueryString("uid"), txtRemarks.Text, MailTemplateId, True)





        lblmsg.Text = "Asset Intra movement Successfully approved."
        lblmsg.Visible = True
    End Sub

    Private Sub SendMail_IntraMovement(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = objsubsonic.GetSubSonicDataSet("GET_ASSET_INTRA_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty


            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = objsubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If


            Dim p1
            p1 = AstList.Item(0).ToString.Split(",")
            strAstList = "<table width='100%' border='1'>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(0) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(3) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(4) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(5) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>To Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(6) & "</td></tr>"



            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Person To Receive Assets</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & txtPersonName.Text & "</td></tr>"


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&rl=rm&ty=imr> Click to Approve </a></td></tr></table>"

            strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&rl=rm&ty=imr>  Click to Reject </a></td></tr></table>"

            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)
            body = body.Replace("@@Approve", strApproveList)
            body = body.Replace("@@Reject", strRejectList)



            '1. IT Request Approved copy goes to request raised person.
            '2. One copy for IT ADMIN to approve.

            'If App_Rej_status = True Then
            Insert_AmtMail(body, strRR, strSubject, strRM)
            'Else
            'Insert_AmtMail(body, strRM, strSubject, "")
            'End If






        Catch ex As Exception

            Throw (ex)
        End Try


    End Sub


    Private Sub IntraMovement_IT_Reject()
        Dim strASSET_LIST As New ArrayList
        For i As Integer = 0 To gvItemIntraMovementITApproval_Reject.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
            strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLocIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlSTowerIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlSFloorIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlDLocIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlDTowerIntraMovementITApproval_Reject.SelectedItem.Text & "," & ddlDFloorIntraMovementITApproval_Reject.SelectedItem.Text & "," & txtRemarks.Text)
        Next

        Dim param(3) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strReqId
        param(1) = New SqlParameter("@MMR_APPROVED_BY", SqlDbType.NVarChar, 200)
        param(1).Value = Request.QueryString("uid")
        param(2) = New SqlParameter("@MMR_Approved_COMMENTS", SqlDbType.NVarChar, 200)
        param(2).Value = txtRemarks.Text
        param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
        param(3).Value = 1018

        ObjSubsonic.GetSubSonicExecute("UPDATE_MMT_MVMT_REQ_APPROVE_reqId", param)
        lblmsg.Text = "Asset Intra movement Successfully rejected."
        lblmsg.Visible = True
        Dim MailTemplateId As Integer
        MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_it_reject"))
        'getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)
        SendMail_IntraMovement(strReqId, strASSET_LIST, Request.QueryString("uid"), txtRemarks.Text, MailTemplateId, True)


    End Sub

#End Region



    Private Sub UpdateAssetRequisitionStatus(ByVal strReq As String, ByVal strRemarks As String, ByVal strStatus As Integer)
        Dim param(2) As SqlParameter
        param(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
        param(0).Value = strReq
        param(1) = New SqlParameter("@Remarks", SqlDbType.NVarChar, 200)
        param(1).Value = strRemarks
        param(2) = New SqlParameter("@StatusId", SqlDbType.NVarChar, 200)
        param(2).Value = strStatus
        ObjSubsonic.GetSubSonicExecute("AssetRequisition_UpdateByRM_Directly", param)
        lblmsg.Text = "Successfully updated."
        lblmsg.Visible = True
    End Sub
End Class
