﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmEditPODetails.aspx.vb" Inherits="FAM_FAM_Webfiles_frmEditPODetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }
        function ChildClick(CheckBox) {
            console.log(CheckBox);
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect1";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {

                    return delrecord();

                }
            }
            alert("Please select atleast one checkbox");
            return false;

        }
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Finalize PO" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Edit PO</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 50px;">--%>
                <form id="form1" runat="server">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <%--<div class="clearfix ">--%>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="255px" placeholder="Search By Request Id/Property Name"></asp:TextBox>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 20px">
                            <asp:Button ID="btnsrch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
                        </div>
                        <%--</div>--%>
                    </div>



                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-12 pull-right">

                            <%--<div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">--%>
                            <div class="row" style="width: 100%; min-height: 20px; max-height: 500px">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <asp:GridView ID="gvItems" runat="server" AllowSorting="False" AutoGenerateColumns="false" AllowPaging="True" PageSize="5"
                                        EmptyDataText="No Finalize PO(s) Found." CssClass="table GridStyle" GridLines="none">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                        ToolTip="Click to check all" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                <ItemStyle Width="50px" HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <%--<asp:BoundField DataField="AIP_PO_ID" HeaderText="PO Id" ItemStyle-HorizontalAlign="left" />--%>
                                            <asp:TemplateField HeaderText="PO ID" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>

                                                    <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIP_PO_ID", "~/FAM/FAM_WebFiles/EditPoDetails.aspx?RID={0}")%>'
                                                        Text='<%# Eval("AIP_PO_ID")%> '>
                                                    </asp:HyperLink>
                                                    <asp:Label ID="lblPoId" runat="server" Text='<%#Eval("AIP_PO_ID")%>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="AUR_NAME" HeaderText="PO Generated By" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="AIP_PO_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />

                                            <asp:TemplateField HeaderText="Advance" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%#Eval("AIPD_PLI")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="AIP_STA_ID" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--</div>--%>
                    <br />
                    <div class="panel-heading">
                        <h3 class="panel-title">Approved PO</h3>
                    </div>
                    <div class="row">
                        <%-- <div class="clearfix">--%>

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <asp:TextBox ID="txtSearch2" runat="server" CssClass="form-control" Width="255px" placeholder="Search By PO Id"></asp:TextBox>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 20px">
                            <asp:Button ID="btnsrch2" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
                        </div>
                        <%-- </div>--%>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-12 pull-right">

                            <%--<div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">--%>
                            <div class="row" style="width: 100%; min-height: 20px; max-height: 1200px">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <asp:GridView ID="FinalizeGrid" runat="server" AllowSorting="False" AutoGenerateColumns="false" AllowPaging="True" PageSize="5"
                                        EmptyDataText="No Finalize PO(s) Found." CssClass="table GridStyle" GridLines="none">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="PO ID" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIP_PO_ID", "~/FAM/FAM_WebFiles/ExportPO.aspx?rid=" + Eval("AIP_PO_ID"))%>'
                                                        Text='<%# Eval("AIP_PO_ID")%> '>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="AIPD_VENDORNAME" HeaderText="Vender Name" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="AIP_PO_DATE" HeaderText="Date" ItemStyle-HorizontalAlign="left" />
                                            <%-- <asp:BoundField DataField="brand" HeaderText="Brand" ItemStyle-HorizontalAlign="left" />--%>
                                            <%--<asp:BoundField DataField="model" HeaderText="Model" ItemStyle-HorizontalAlign="left" />--%>
                                            <asp:BoundField DataField="AIPD_TOTALCOST" HeaderText="Cost" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="location" HeaderText="Location" ItemStyle-HorizontalAlign="left" />
                                            <asp:BoundField DataField="category" HeaderText="Main Category" ItemStyle-HorizontalAlign="left" />
                                            <%--<asp:BoundField DataField="subcategory" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" />--%>
                                            <asp:BoundField DataField="AIP_STA_ID" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                            <asp:TemplateField HeaderText="Edit PO" ItemStyle-HorizontalAlign="left">
                                                <ItemTemplate>

                                                    <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIP_PO_ID", "~/FAM/FAM_WebFiles/EditPoDetails.aspx?RID={0}")%>'
                                                        Text='EDIT '>
                                                    </asp:HyperLink>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%--</div>--%>
                </form>
            </div>
        </div>
        <%--</div>
    </div>--%>
        <%--  </div>--%>
    </div>
    <%-- </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        function delrecord() {
            var txt;
            var r = confirm("Are you sure want to submit ?");
            if (r == true) {
                return true;
            } else {
                return false;
            }
        }
        //var id = '<%= Session("req")%>';
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "/FAM/FAM_Webfiles/ExportPO.aspx?rid=" + id);
            $("#myModal").modal('show');
            return false;
        }
        function refreshSelectpicker() {

            <%--$("#<%=ddlState.ClientID%>").selectpicker();
            $("#<%=ddlVendor.ClientID%>").selectpicker();--%>
            $("#<%=gvItems.ClientID %>").selectpicker();
            $('#<%=gvItems.ClientID %>').find('[id$="ddlAssetCat"]').selectpicker();
            $('#<%=gvItems.ClientID %>').find('[id$="ddlAssetsubcat"]').selectpicker();
            $('#<%=gvItems.ClientID %>').find('[id$="ddlAssetCode"]').selectpicker();
            $('#<%=gvItems.ClientID %>').find('[id$="ddlproductname"]').selectpicker();
        }

        refreshSelectpicker();
    </script>
</body>
</html>

