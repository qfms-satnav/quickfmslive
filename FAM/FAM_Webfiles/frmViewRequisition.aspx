<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewRequisition.aspx.vb" Inherits="FAM_FAM_Webfiles_frmViewRequisition" Title="View Requisition" %>

<%@ Register Src="../../Controls/ViewReq.ascx" TagName="ViewReq" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>View Capital Asset Requisition</h3>
            </div>
            <div class="card" style="padding-right: 10px;">
                <form id="form1" class="form-horizontal" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                        ForeColor="Red" ValidationGroup="Val1" />
                    <uc1:ViewReq ID="ViewReq" runat="server" />
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>




