<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Test.aspx.vb" Inherits="Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="~/Controls/ucTest.ascx" TagName="ucTest" TagPrefix="uc" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>TEST</title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
    <%--gird--%>
    <link rel="stylesheet" href="style/print.css" type="text/css" media="print" charset="utf-8" />
    <link rel="stylesheet" href="themes/redmond/jquery-ui-1.8.14.custom.css" type="text/css"
        media="screen" />
    <link rel="stylesheet" href="themes/ui.jqgrid.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="style/facebox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="style/colorbox/colorbox.css" media="screen" />
    <link rel="stylesheet" href="style/tooltip.css" type="text/css" media="all" />
    <link rel="stylesheet" href="style/roi.css" type="text/css" media="all" />
    <%--end--%>

    <script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js" type="text/javascript" defer></script>

    <script type="text/javascript" src="lib/jquery/jquery-ui-1.8.2.custom.min.js" defer></script>

    <script type="text/javascript" src="lib/jquery/facebox/facebox.js" defer></script>

    <script type="text/javascript" src="lib/jquery/colorbox/jquery.colorbox.js" defer></script>

    <script type="text/javascript" src="lib/page.js" defer></script>

    <script type="text/javascript" src="lib/roi_uam.js" defer></script>

    <script type="text/javascript" src="lib/utilities.js" defer></script>

    <%--gird--%>
    <link rel="stylesheet" href="themes/ui.jqgrid.css" type="text/css" media="screen" />

    <script type="text/javascript" src="lib/UserManager.js" defer></script>

    <script type="text/javascript" src="js/i18n/grid.locale-en.js" defer></script>

    <script type="text/javascript" src="js/jquery.jqGrid.src.js" defer></script>

    <script type="text/javascript" src="js/jquery.jqGrid.min.js" defer></script>

    <script type="text/javascript" defer>
			var roleid;
			var aclid ='<%=Session("aclid") %>'; 
			var aclLevel = '<%=Session("aclLevel") %>';
			var cmid ='<%= Session("cid") %>';
			var uid ='<%= Session("ud") %>';
			var roleid ='<%= Session("rd") %>';	
			//var cityArray = <?=$citylist?>;
			
			function getData()
			{
				var dat = '<%= Session("data") %>';
				return dat;
			}
			
			function disableleftaccordion()
			{		
				if(aclid!=null && aclid!="")
				{
					// alert (aclid + ' level: ' + aclLevel);
					var spAclId = aclid.split('!');
					var spAclLevel = aclLevel.split('!');
					for(var i=0;i<spAclId.length; i++)
					{		// 				
							if(!((spAclId[i] == "clientmapaccess") ||  (spAclId[i] == "userlicense" ) || (spAclId[i] == "usermapaccess" ) || (spAclId[i] == "clientadmin" ) || (spAclId[i] == "mapdatalayer" )))
							{ 
								var aclObj = document.getElementById(spAclId[i]);								
								if (aclObj != null)
								{
									aclObj.style.display='block';
								}
						 }
					}
				}		
			}
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <uc:ucTest ID="uctest" runat="server" />
        </div>
    </form>
</body>
</html>
