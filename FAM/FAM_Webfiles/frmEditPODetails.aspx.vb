﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports System.Drawing
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports System.IO
Partial Class FAM_FAM_Webfiles_frmEditPODetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindGrid()
            BindFinalizeGrid()
            Dim ReqId As String = GetRequestId()
            Dim Message As String = ""
            If LCase(ReqId) = "outwardapp" Then
                Message = "Updated Po Successfully"
                lblMsg.Text = Message
            Else
                lblMsg.Text = ""
            End If
        End If
    End Sub

    Private Sub BindGrid()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_GetByStatusId")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_GetByStatusId_BYAURID_FORUPDATE")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()

    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnsrch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsrch.Click
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_GetByStatusId_Search")
            sp1.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)
            sp1.Command.AddParameter("@SEARCH", txtSearch.Text, DbType.String)
            Dim ds As New DataSet
            ds = sp1.GetDataSet
            gvItems.DataSource = ds
            gvItems.DataBind()
            Session("reqDetails") = ds
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


    End Sub
    Protected Sub btnsrch2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsrch2.Click
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_GETFINALIZEPO_SEARCH")
            sp1.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)
            sp1.Command.AddParameter("@SEARCH", txtSearch2.Text, DbType.String)
            Dim ds As New DataSet
            ds = sp1.GetDataSet
            FinalizeGrid.DataSource = ds
            FinalizeGrid.DataBind()
            'Session("reqDetailsS") = ds
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function
    Public Sub send_mail_Finalize_PO(ByVal POnum As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_FINALIZE")
        sp.Command.AddParameter("@REQ_ID", POnum, DbType.String)
        sp.Execute()
    End Sub
    Private Sub BindFinalizeGrid()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_PO_GetByStatusId")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_GETFINALIZEPO")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), Data.DbType.String)
        FinalizeGrid.DataSource = sp.GetDataSet
        FinalizeGrid.DataBind()

    End Sub
    Protected Sub FinalizeGrid_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles FinalizeGrid.PageIndexChanging
        FinalizeGrid.PageIndex = e.NewPageIndex
        BindFinalizeGrid()
    End Sub
End Class
