<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmpaymentmemodtls.aspx.vb"
    Inherits="FAM_FAM_Webfiles_frmpaymentmemodtls" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title> Payment Memo Details</title>
</head>
<body>
    <form id="form1" runat="server">
    
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="100%" align="center">
                    <asp:Label ID="lblHeadTitle" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                        ForeColor="Black">  
                    </asp:Label>
                    <hr align="center" width="60%" />
                    <br />
                </td>
            </tr>
        </table>
        <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
            border="0">
            <tr>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                        width="9" /></td>
                <td width="100%" class="tableHEADER" align="left">
                    &nbsp;<strong>
                        <asp:Label ID="lblHeadTitle1" runat="server"></asp:Label></strong>
                </td>
                <td>
                    <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                        width="16" /></td>
            </tr>
            <tr>
                <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                    &nbsp;</td>
                <td align="left">
                    <table width="100%" style="vertical-align: top;" cellpadding="0" cellspacing="0"
                        align="center" border="0">
                        <tr id="trDDChkDetails" runat="server" visible="false">
                            <td colspan="2">
                                <fieldset>
                                    <legend>
                                        <asp:Label ID="lbltitle" runat="server"></asp:Label></legend>
                                    <table border="0" width="100%" align="center">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lblNumber" runat="server"></asp:Label>
                                            </td>
                                            <td>
                                                <b><asp:Label ID="lblDDChqNumber" runat="server"></asp:Label></b>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDt" runat="server"></asp:Label>
                                                <font class="clsNote">*</font></td>
                                            <td>
                                                <b><asp:Label ID="lblchqDt" runat="server"></asp:Label></b>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Bank Name<font class="clsNote">*</font></td>
                                            <td>
                                               <b> <asp:Label ID="lblBankName" runat="server"></asp:Label></b>
                                            </td>
                                            <td>
                                                Branch Name<font class="clsNote">*</font></td>
                                            <td>
                                                <b><asp:Label ID="lblBranchName" runat="server"></asp:Label></b>
                                            </td>
                                        </tr>
                                        <tr id="TRACNUMBER" runat="server" visible="false">
                                            <td>
                                                Account Number<font class="clsNote">*</font></td>
                                            <td>
                                               <b><asp:Label ID="lblAccountNumber" runat="server"></asp:Label></b> 
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
                <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                    height: 100%;">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 10px; height: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                        width="9" /></td>
                <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                        width="25" /></td>
                <td style="height: 17px; width: 17px;">
                    <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                        width="16" /></td>
            </tr>
        </table>
    </form>
</body>
</html>
