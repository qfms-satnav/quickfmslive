<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AssetDetails.aspx.vb" Inherits="FAM_FAM_Webfiles_AssetDetails" %>

<%@ Register Src="../../Controls/AstDetails.ascx" TagName="AstDetails" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Asset Details</title>
 <link rel="stylesheet" type="text/css" href="<%=Page.ResolveUrl("~/js/Default.css")%>"  />
<script type="text/javascript" src="<%=Page.ResolveUrl("~/popup.js")%>"> </script>
<script type="text/javascript" src="<%=Page.ResolveUrl("~/tabcontent.js")%>"> </script>
<script type="text/javascript" src="<%=Page.ResolveUrl("~/js/jquery.js")%>"> </script>
<link rel="stylesheet" type="text/css" href="<%=Page.ResolveUrl("~/tabcontent.css")%>" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:AstDetails ID="AstDetails1" runat="server" />
    
    </div>
    </form>
</body>
</html>
