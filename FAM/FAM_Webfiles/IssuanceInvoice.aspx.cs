﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Microsoft.Reporting.WebForms;
using System.Globalization;


public partial class FAM_FAM_Webfiles_IssuanceInvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] == "")
            Response.Redirect(Application["FMGLogout"].ToString());
        if (!IsPostBack)
        {
            string rid =  Request.QueryString["rid"];
            //string rid = "10";
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ISSUANCE_INVOICE");
            sp.Command.Parameters.Add("@REQID",rid,DbType.String);
            DataSet ds = sp.GetDataSet();
            
            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("AssetIssuanceInvoiceDS", ds.Tables[0]));
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("AssetIssuanceInvoiceDetailsDS", ds.Tables[1]));
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/IssuanceInvoice.rdlc");
            ReportViewer1.LocalReport.Refresh();

            CultureInfo ci = new CultureInfo(Session["userculture"].ToString());
            NumberFormatInfo nfi = ci.NumberFormat;
            ReportParameter p2 = new ReportParameter("CurrencyParam", nfi.CurrencySymbol);

            ReportViewer1.LocalReport.SetParameters(p2);

            ReportViewer1.SizeToReportContent = true;
            ReportViewer1.Visible = true;
            ReportViewer1.ShowPrintButton = true;
        }      
    }
}