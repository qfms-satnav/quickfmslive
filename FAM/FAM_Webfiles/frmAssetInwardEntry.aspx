<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetInwardEntry.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAssetInwardEntry" Title="Asset Inward Entry" %>

<%@ Register Src="../../Controls/AssetInwardEntry.ascx" TagName="AssetInwardEntry" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
     <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };



    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Finalize PO" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                            <h3 class="panel-title">Asset Inward Entry</h3>
                        </div>
                          <div class="card">
                        <%--<div class="panel-body" style="padding-right: 50px;">--%>
                            <form id="form1" runat="server">
                                  <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                  <asp:updatepanel runat="server">
                                      <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                <uc1:AssetInwardEntry ID="AssetInwardEntry1" runat="server"></uc1:AssetInwardEntry>
                                                  </ContentTemplate>
                               </asp:updatepanel>
                            </form>
                        </div>
                    </div>
               <%-- </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
