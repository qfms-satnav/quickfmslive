﻿Imports System.Collections.Generic
Imports System.Collections.ObjectModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.Script.Serialization
Imports clsSubSonicCommonFunctions
Imports System.IO
Partial Class FAM_FAM_Webfiles_Exports_SearchunMappedAssetStatus
    Inherits System.Web.UI.Page
    Dim objsubsonic As New clsSubSonicCommonFunctions
    Dim export As New Export
    Dim ds As DataSet

    Private Sub ExportToExcel()
        Dim param(0) As SqlParameter

        param(0) = New SqlParameter("@ASTCODE", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("ct")
        ds = objsubsonic.GetSubSonicDataSet("GET_AST_DETAILS_SEARCH", param)


        'ds.Tables(0).Columns(1).ColumnName = "PRODUCT NAME"
        'ds.Tables(0).Columns(2).ColumnName = "ASSET CODE"

        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        export.Export("Search_unMapped_Asset_Status.xls", gv)

    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            ExportToExcel()
        End If
    End Sub
End Class
