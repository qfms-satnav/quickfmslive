<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAMGVendorModify.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmAMGVendorModify" %>

<%@ Register Src="../../Controls/AMGVendorModify.ascx" TagName="AMGVendorModify" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div ba-panel ba-panel-title="Asset Brand Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Vendor Modification Details</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>
                    <%--<asp:Label ID="lblHeader" runat="server" />--%>
                                Vendor Modification Details
                </h3>
            </div>
            <div class="card">

                <form id="form1" runat="server">
                    <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel ID="CityPanel1" runat="server">
                                    <ContentTemplate>--%>
                    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                    <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                    <uc1:AMGVendorModify ID="AMGVendorModify1" runat="server" />
                    <%-- </ContentTemplate>
                                </asp:UpdatePanel>--%>
                </form>
            </div>
        </div>
    </div>
    <%-- </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

