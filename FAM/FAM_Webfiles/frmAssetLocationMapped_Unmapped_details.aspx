<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetLocationMapped_Unmapped_details.aspx.vb" Inherits="frmAssetLocationMapped_Unmapped_details" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
 <title>Asset Location (Mapped and Unmapped Assets)</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table width="100%" cellpadding="2">
                <tr>
                    <td align="left" valign="top">
                        <fieldset>
                            <legend>Mapped Assets </legend>
                            <fieldset>
                                    <legend>Mapped Assets </legend>
                                    <asp:GridView ID="gvMappedAssets" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        EmptyDataText="No Asset(s) Found." Width="100%">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Emp Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAUR_FIRST_NAME" runat="server" Text='<%#Eval("AUR_FIRST_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Emp Email">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAUR_EMAIL" runat="server" Text='<%#Eval("AUR_EMAIL") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RM Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRMNAME" runat="server" Text='<%#Eval("RMNAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="RM Email">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRMEMAIL" runat="server" Text='<%#Eval("RMEMAIL") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLCM_NAME" runat="server" Text='<%#Eval("LCM_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tower">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTWR_NAME" runat="server" Text='<%#Eval("TWR_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Floor">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFLR_NAME" runat="server" Text='<%#Eval("FLR_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Department">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAUR_DEP_ID" runat="server" Text='<%#Eval("AUR_DEP_ID") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Space ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAUR_LAST_NAME" runat="server" Text='<%#Eval("AUR_LAST_NAME") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </fieldset>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <fieldset>
                            <legend>Unmapped Assets</legend>
                            <asp:GridView ID="gvUnmappedAst" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                EmptyDataText="No Asset(s) Found." Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText="AssetName">
                                        <ItemTemplate>
                                            <asp:Label ID="lblname" runat="server" Text='<%#Eval("PRODUCTNAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AssetCode">
                                        <ItemTemplate>
                                            <asp:Label ID="lblcode" runat="server" Text='<%#Eval("AAS_AAT_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </fieldset>
                    </td>
                </tr>
            </table>
    </div>
    </form>
</body>
</html>
