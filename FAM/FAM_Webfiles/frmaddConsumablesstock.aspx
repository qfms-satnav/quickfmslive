<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmaddConsumablesstock.aspx.vb" Inherits="FAM_FAM_Webfiles_frmaddConsumablesstock"
    Title="Add Asset Stock " %>

<%@ Register Src="~/Controls/AddStock.ascx" TagName="AddStock"
    TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('.date').datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Asset Brand Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Add Consumable Asset </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 50px;">--%>
                <h3>Add Consumable Asset </h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                   <%-- <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CityPanel1" runat="server">
                        <ContentTemplate>--%>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="red" ValidationGroup="Val1" DisplayMode="List" />
                            <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                            <uc1:AddStock ID="AddStock1" runat="server" />
                       <%-- </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </form>
            </div>
        </div>
    </div>
    <%-- </div>
        </div>
    </div>--%>
    <%--<script src="../../BootStrapCSS/assets/js/AunglarJs-1.8.3/angular.min.js"></script>--%>
    <script src="../../BootStrapCSS/Scripts/jquery.min.js"></script>
    <script src="../../BootStrapCSS/assets/js/Bootstrap-DatePicker-V1.9.0/bootstrap-datepicker.js"></script>
    <%--<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>--%>
</body>
</html>
