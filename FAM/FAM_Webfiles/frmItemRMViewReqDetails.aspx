<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmItemRMViewReqDetails.aspx.vb" Inherits="FAM_FAM_Webfiles_ItemRMViewReqDetails" Title="RM Approval" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }
         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Consumuable Requisition Approval 1</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" ValidationGroup="Val1" />
                                 <asp:updatepanel runat="server">
                                      <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">                  
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Requisition Id</label>
                                            <div class="col-md-12">
                                                <asp:Label ID="lblReqId" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Raised By</label>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Asset Category</label>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlAstCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Asset Sub Category</label>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Asset Brand/Make</label>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Asset Model</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                                                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Location</label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                                ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <div  style="height: 41px;">
                                            <h3 class="panel-title">Item Lists</h3>
                                        </div>
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No request(s) found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:BoundField DataField="AST_MD_id" HeaderText="Item Code" ItemStyle-HorizontalAlign="left" Visible="false">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AST_MD_id" HeaderText="Item Code" ItemStyle-HorizontalAlign="left" Visible="false">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AST_MD_CODE" HeaderText="Item Code" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Item Name" ItemStyle-HorizontalAlign="left" Visible="TRUE">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AST_MD_NAME" runat="server" Text='<%#Eval("AST_MD_NAME") %>'> </asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Category" HeaderText="Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="manufacturer" HeaderText="Brand Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_QTY") %>'></asp:TextBox>
                                                        <asp:Label ID="lblunits" runat="server" Text='<%#Eval("UNITS")%>'></asp:Label>
                                                        <asp:Label ID="lblProductid" runat="server" Text='<%#Eval("AST_MD_id") %>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblMinOrdQty" runat="server" Text='<%#Eval("AST_MD_MINQTY") %>' Visible="false"> </asp:Label>
                                                        <asp:Label ID="lblqty" runat="server" Text='<%#Eval("AID_QTY") %>' Visible="false"> </asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" ItemStyle-HorizontalAlign="Center" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                            ToolTip="Click to check all" />

                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReq_status" runat="server" Visible="false" Text='<%# Eval("AIR_STA_ID") %>'></asp:Label>
                                                        <asp:CheckBox ID="chkSelect" Checked='<%#Bind("ticked") %>' runat="server" onclick="javascript:ChildClick(this);"></asp:CheckBox>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Remarks<span style="color: red;">*</span> </label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="lblRem"
                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="lblRem" runat="server" CssClass="form-control" MaxLength="100" Height="30%" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">L1 Remarks<span style="color: red;">*</span></label>
                                             <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRM"
                ErrorMessage="Please Enter Valid Remarks" Display="None"
                ValidationGroup="Val1">
            </asp:RegularExpressionValidator>
                                            <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRM"
                                                Display="None" ErrorMessage="Please Enter L1 Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtRM" runat="server" CssClass="form-control" MaxLength="100"
                                                    Height="30%" TextMode="MultiLine"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 20px">
                                        <div class="form-group">
                                            <asp:Button ID="btnsubmit" runat="server" Text="Approve" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1"/>
                                            <asp:Button ID="btnCancel" runat="server" Text="Reject" CssClass="btn btn-primary custom-button-color" />
                                            <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />

                                        </div>
                                    </div>
                                </div>
</ContentTemplate>
                                     </asp:updatepanel>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>

    <script defer>
        function refreshSelectpicker() {
            $("#<%=ddlastCat.ClientID%>").selectpicker();
             $("#<%=ddlastsubCat.ClientID%>").selectpicker();
             $("#<%=ddlAstBrand.ClientID%>").selectpicker();
             $("#<%=ddlAstModel.ClientID%>").selectpicker();
             $("#<%=ddlAstCat.ClientID%>").selectpicker();

        }
        refreshSelectpicker();


    </script>
</html>

