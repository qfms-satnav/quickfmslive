﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;

public partial class FAM_FAM_Webfiles_AssetTransferReport : System.Web.UI.Page
{
    private DataSet ds;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            LoadReport();
        }


    }
    public void LoadReport()

    {
        var reqid = (Request.QueryString["rid"]);
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "[AM_ASSET_TRANSFOR_REQUISITION]");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@Reqid", reqid, DbType.String);
        ds = sp.GetDataSet();
        //ds.Tables.Add(ds.Tables[0]);
        // ds.Tables.Add(dt2);
        string imagePath = BindLogo();
        ReportDataSource rds = new ReportDataSource();
        rds.Name = "AssetTransfer";
        rds.Value = ds.Tables[0];
        ReportDataSource rds1 = new ReportDataSource();
        rds1.Name = "AssetTransferSending";
        rds1.Value = ds.Tables[1];
        ReportDataSource rds2 = new ReportDataSource();
        rds2.Name = "AssetTransferReceiving";
        rds2.Value = ds.Tables[2];
        ReportViewer1.Reset();

        ReportViewer1.LocalReport.DataSources.Add(rds);
        ReportViewer1.LocalReport.DataSources.Add(rds1);
        ReportViewer1.LocalReport.DataSources.Add(rds2);
        ReportViewer1.LocalReport.EnableExternalImages = true;
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetTransformReport.rdlc");

        ReportParameter rp1 = new ReportParameter("Imagepath", imagePath);
        ReportViewer1.LocalReport.SetParameters(rp1);
        ReportViewer1.SizeToReportContent = true;

        ReportViewer1.Visible = true;
        ReportViewer1.LocalReport.DisplayName = "Asset Transfer Report";
        ReportViewer1.LocalReport.Refresh();
    }
    public string BindLogo()
    {
        // Dim imagePath As String
        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "Update_Get_LogoImage");
        sp3.Command.AddParameter("@type", "2", DbType.String);
        sp3.Command.AddParameter("@AUR_ID", Session["uid"], DbType.String);
        sp3.Command.AddParameter("@Tenant", Session["TENANT"], DbType.String);
        DataSet ds3 = sp3.GetDataSet();
        if (ds3.Tables[0].Rows.Count > 0)
            return ds3.Tables[0].Rows[0]["ImagePath"].ToString();
        else
            return "https://live.quickfms.com/BootStrapCSS/images/yourlogo.png";
    }
}