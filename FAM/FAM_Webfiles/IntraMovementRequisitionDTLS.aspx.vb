Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_IntraMovementRequisitionDTLS
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim MMR_AST_CODE, MMR_FROMBDG_ID, MMR_FROMFLR_ID, MMR_TOFLR_ID, SLCM_CODE, DLCM_CODE, LCM_NAME As String
    Dim TOEMP_ID As Integer
    Dim strLCM_CODE As String
    Dim strTWR_CODE As String
    Dim strfloor As String
    Dim StrEmp As String
    Dim Strast As String
    Dim strastcd As New ArrayList
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDetails()
            'BindLocations()
        End If
    End Sub

    Public Sub BindDetails()
        Dim icnt As Integer = 0
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Trim(Request.QueryString("Req_id"))

        Dim ds As New DataSet
        ds = objsubsonic.GetSubSonicDataSet("ASSET_GET_INTRAREQUISITIONS_DTLS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                MMR_AST_CODE = ds.Tables(0).Rows(i).Item("MMR_AST_CODE")
                MMR_FROMBDG_ID = ds.Tables(0).Rows(i).Item("MMR_FROMBDG_ID")
                MMR_FROMFLR_ID = ds.Tables(0).Rows(i).Item("MMR_FROMFLR_ID")
                DLCM_CODE = ds.Tables(0).Rows(i).Item("FLOC_CODE")
                SLCM_CODE = ds.Tables(0).Rows(i).Item("SLOC_CODE")
                LCM_NAME = ds.Tables(0).Rows(i).Item("FLOC_NAME")
                TOEMP_ID = ds.Tables(0).Rows(i).Item("TOEMP_ID")

                MMR_TOFLR_ID = ds.Tables(0).Rows(i).Item("MMR_TOFLR_ID")
                BindLocation(SLCM_CODE, ddlSLoc)
                BindLocation(DLCM_CODE, ddlDLoc)
                BindTower(SLCM_CODE, MMR_FROMBDG_ID, ddlSTower)
                BindTower(DLCM_CODE, MMR_FROMBDG_ID, ddlDTower)
                'BindLocation(LCM_CODE) 
                'BindTower(MMR_FROMBDG_ID)
                'BindFloor(MMR_FROMFLR_ID)

                BindFloor(MMR_FROMBDG_ID, MMR_FROMFLR_ID, ddlSFloor)
                BindFloor(MMR_FROMBDG_ID, MMR_TOFLR_ID, ddlDFloor)


                Session("Flag") = 1
                BindAssetGrid()
                

                Emp_Loadddl(ddlEmp)
                ddlEmp.ClearSelection()
                ddlEmp.Items.FindByValue(TOEMP_ID).Selected = True
                strastcd.Insert(icnt, MMR_AST_CODE)
                icnt = icnt + 1
            Next
        End If
        'For i As Integer = 0 To strastcd.Count - 1
        '    CheckAssetWhichisAlreadySelected(strastcd(i))
        'Next

        'Emp_Loadddl(ddlEmp, dr("TOEMP_ID"))
        'ddlAsset.Items.FindByValue(LTrim(RTrim(dr("MMR_AST_CODE")))).Selected = True
 


    End Sub

    'Private Sub CheckAssetWhichisAlreadySelected(ByVal astcode As String)
    '    For i As Integer = 0 To gvItems.Rows.Count - 1
    '        Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
    '        Dim chkSelect As CheckBox = CType(gvItems.Rows(i).FindControl("chkSelect"), CheckBox)
    '        If Trim(lblAAS_AAT_CODE.Text) = Trim(astcode) Then
    '            chkSelect.Checked = True
    '        End If
    '    Next
    'End Sub
     
    Private Sub BindAssetGrid()
        Dim Location As String = ""
        Dim Tower As String = ""
        Dim Floor As String = ""

        If ddlSLoc.SelectedIndex > 0 Then
            Location = ddlSLoc.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a location."
            Exit Sub
        End If

        If ddlSTower.SelectedIndex > 0 Then
            Tower = ddlSTower.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a tower."
            Exit Sub
        End If

        If ddlSFloor.SelectedIndex > 0 Then
            Floor = ddlSFloor.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a floor."
            Exit Sub
        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_ALLASTS")
        sp.Command.AddParameter("@LCM_CODE", Location, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", Tower, DbType.String)
        sp.Command.AddParameter("@FLR_CODE", Floor, DbType.String)
        'sp.Command.AddParameter("@ASSET_CODE", txtstore1.Text, DbType.String)
        'sp.Command.AddParameter("@ASSET_CODE", "", DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()

        For j As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(j).FindControl("lblAAS_AAT_CODE"), Label)
            Dim chkSelect As CheckBox = CType(gvItems.Rows(j).FindControl("chkSelect"), CheckBox)
            If lblAAS_AAT_CODE.Text = MMR_AST_CODE Then
                chkSelect.Checked = True

            End If
        Next


    End Sub
    Public Sub BindFloor(ByVal AAS_FLR_ID As String)
        If ddlSTower.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlSTower.SelectedItem.Value
            BindFloorsByTower(TwrCode, ddlSFloor)
        End If
        ddlSFloor.Items.FindByValue(AAS_FLR_ID).Selected = True
    End Sub
    Private Sub BindTower(ByVal AAS_BDG_ID As String)
        '----------- Binding Tower -----------------------
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            BindTowersByLocation(LocCode, ddlSTower)
        End If
        ddlSTower.Items.FindByValue(AAS_BDG_ID).Selected = True
        '-------------------------------------------------
    End Sub

    Private Sub BindLocation(ByVal strlcm_code As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.Int)
        param(0).Value = 1
        ObjSubsonic.Binddropdown(ddl, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddl.Items.FindByValue(strlcm_code).Selected = True
    End Sub

    Private Sub BindTower(ByVal strLocCode As String, ByVal strtwr_code As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
        param(0).Value = Trim(strLocCode)
        ObjSubsonic.Binddropdown(ddl, "USP_Tower_GetByLocation", "TWR_NAME", "twr_CODE", param)
        ddl.Items.FindByValue(strtwr_code).Selected = True
    End Sub


    Private Sub BindFloor(ByVal strTWR_CODE As String, ByVal strflr_code As String, ByRef ddl As DropDownList)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@TwrCode", SqlDbType.NVarChar, 200)
        param(0).Value = Trim(strTWR_CODE)
        ObjSubsonic.Binddropdown(ddl, "USP_Floor_GetByTower", "FLR_NAME", "FLR_Code", param)
        ddl.Items.FindByValue(strflr_code).Selected = True

    End Sub

    Private Sub BindLocation(ByVal strlcm_code As String)
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            ddlSLoc.DataSource = sp.GetDataSet()
            ddlSLoc.DataTextField = "LCM_NAME"
            ddlSLoc.DataValueField = "LCM_CODE"
            ddlSLoc.DataBind()
            ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))

            ddlSLoc.Items.FindByValue(strlcm_code).Selected = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub BindEmp()
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("uid")
        ObjSubsonic.Binddropdown(ddlEmp, "GET_ALL_AMANTRA_USER", "AUR_FIRST_NAME", "AUR_ID", param)
    End Sub

    Private Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        Dim dr As SqlDataReader = sp.GetReader
        ddlSLoc.DataSource = dr
        ddlSLoc.DataTextField = "LCM_NAME"
        ddlSLoc.DataValueField = "LCM_CODE"
        ddlSLoc.DataBind()
        ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        For Each li As ListItem In ddlSLoc.Items
            ddlDLoc.Items.Add(li)
        Next
    End Sub

    Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            BindTowersByLocation(LocCode, ddlSTower)
        End If
    End Sub
    Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Tower_GetByLocation")
        sp.Command.AddParameter("@LocId", Trim(LocCode), DbType.String)
        ddl.DataSource = sp.GetReader
        ddl.DataTextField = "TWR_NAME"
        ddl.DataValueField = "TWR_Code"
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Protected Sub ddlSTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTower.SelectedIndexChanged
        If ddlSTower.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlSTower.SelectedItem.Value
            BindFloorsByTower(TwrCode, ddlSFloor)
        End If
    End Sub
    Private Sub BindFloorsByTower(ByVal TwrCode As String, ByRef ddl As DropDownList)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Floor_GetByTower")
        sp.Command.AddParameter("@TwrCode", Trim(TwrCode), DbType.String)
        ddl.DataSource = sp.GetReader
        ddl.DataTextField = "FLR_NAME"
        ddl.DataValueField = "FLR_Code"
        ddl.DataBind()
        ddl.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    
 
    Protected Sub ddlDLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLoc.SelectedIndexChanged
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            BindTowersByLocation(LocCode, ddlDTower)
        End If
    End Sub

    Protected Sub ddlDTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDTower.SelectedIndexChanged
        If ddlDTower.SelectedIndex > 0 Then
            Dim TwrCode As String = ddlDTower.SelectedItem.Value
            BindFloorsByTower(TwrCode, ddlDFloor)
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim Location As String = ""
        Dim Tower As String = ""
        Dim Floor As String = ""
        Dim DLocation As String = ""
        Dim DTower As String = ""
        Dim DFloor As String = ""

        If ddlSLoc.SelectedIndex > 0 Then
            Location = ddlSLoc.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a location."
            Exit Sub
        End If

        If ddlSTower.SelectedIndex > 0 Then
            Tower = ddlSTower.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a tower."
            Exit Sub
        End If

        If ddlSFloor.SelectedIndex > 0 Then
            Floor = ddlSFloor.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a floor."
            Exit Sub
        End If

        If ddlDLoc.SelectedIndex > 0 Then
            DLocation = ddlDLoc.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a location."
            Exit Sub
        End If

        If ddlDTower.SelectedIndex > 0 Then
            DTower = ddlDTower.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a tower."
            Exit Sub
        End If

        If ddlDFloor.SelectedIndex > 0 Then
            DFloor = ddlDFloor.SelectedItem.Value
        Else
            lblMsg.Text = "Please select a floor."
            Exit Sub
        End If

        Dim chkCount As Integer = 0

        For Each gvRow As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(gvRow.FindControl("chkSelect"), CheckBox)
            If chkSelect.Checked Then
                chkCount += 1
            End If
        Next
        If chkCount = 0 Then
            lblMsg.Text = "Please select at least one asset."
            Exit Sub
        End If


        '-------------- Source Building and Tower should not be equal to  Destination Building and Tower -----------------

        If ddlSLoc.SelectedItem.Value = ddlDLoc.SelectedItem.Value And ddlSTower.SelectedItem.Value = ddlDTower.SelectedItem.Value And ddlSFloor.SelectedItem.Value = ddlDFloor.SelectedItem.Value Then
            lblMsg.Text = "Source Building, Tower and Floor should not be equal to Destination Building, Tower and Floor."
            Exit Sub
        End If

        '-----------------------------------------------------------------------------------------------------------------
        Dim strASSET_LIST As New ArrayList
        Dim PersonName As String = ddlEmp.SelectedItem.Value
        Dim Remarks As String = Trim(txtRemarks.Text)
        Dim ReqId As String = ""
        Dim count As Integer = 0
        Dim Message As String = String.Empty
        Dim TotalSelectedProduct As New ArrayList
        Dim cnt As Integer = 0
        ReqId = Request.QueryString("Req_id")
        Dim mcnt As Integer = 0
        For i As Integer = 0 To gvItems.Rows.Count - 1

            Dim lblAAS_AAT_CODE As Label = DirectCast(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim chbx As CheckBox = DirectCast(gvItems.Rows(i).FindControl("chkSelect"), CheckBox)
            If chbx.Checked = True Then

                cnt = 1
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_MVMT_REQ_update")
                sp.Command.AddParameter("@MMR_REQ_ID", ReqId, DbType.String)
                sp.Command.AddParameter("@MMR_AST_CODE", lblAAS_AAT_CODE.Text, DbType.String)
                sp.Command.AddParameter("@MMR_FROMBDG_ID", Tower, DbType.String)
                sp.Command.AddParameter("@MMR_FROMFLR_ID", Floor, DbType.String)
                sp.Command.AddParameter("@MMR_TOBDG_ID", DTower, DbType.String)
                sp.Command.AddParameter("@MMR_TOFLR_ID", DFloor, DbType.String)
                sp.Command.AddParameter("@MMR_RAISEDBY", Session("UID"), DbType.String)
                sp.Command.AddParameter("@MMR_RECVD_BY", PersonName, DbType.String)
                sp.Command.AddParameter("@MMR_MVMT_TYPE", "", DbType.String)
                sp.Command.AddParameter("@MMR_MVMT_PURPOSE", "", DbType.String)
                sp.Command.AddParameter("@MMR_LRSTATUS_ID", 1, DbType.Int32)
                sp.Command.AddParameter("@MMR_COMMENTS", Remarks, DbType.String)
                ' sp.Command.AddParameter("@MMR_ITEM_REQUISITION", txtstore.Text, DbType.String)
                sp.Command.AddParameter("@MMR_ITEM_REQUISITION", "", DbType.String)
                sp.ExecuteScalar()

                Dim strValue As String = ""
                strValue = lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlSTower.SelectedItem.Text & "," & ddlSFloor.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & ddlDTower.SelectedItem.Text & "," & ddlDFloor.SelectedItem.Text & "," & txtRemarks.Text & "," & ddlEmp.SelectedItem.Text

                strASSET_LIST.Insert(mcnt, strValue)
                mcnt += 1
            End If

        Next



        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_employee"))
        'getRequestDetails(ReqId, strASSET_LIST, MailTemplateId, False)


        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_it_approval_reject"))
        'getRequestDetails(ReqId, strASSET_LIST, MailTemplateId, True)
 

        If cnt = 1 Then
            Response.Redirect("frmAssetThanks.aspx?RID=intramovement")
        End If
        

    End Sub



    Private Sub getRequestDetails(ByVal ReqId As String, ByVal strAst As ArrayList, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
       
        SendMail(ReqId, strAst, Session("uid"), txtRemarks.Text, MailStatus, App_Rej_status)
    End Sub


    Private Sub SendMail(ByVal strReqId As String, ByVal AstList As ArrayList, ByVal Req_raised As String, ByVal strRemarks As String, ByVal MailStatus As Integer, ByVal App_Rej_status As Boolean)
        Dim st As String = ""
        'lblMsg.Text = st
        Try
            Dim to_mail As String = String.Empty
            Dim body As String = String.Empty
            Dim strCC As String = String.Empty
            Dim strEmail As String = String.Empty
            Dim strRM As String = String.Empty
            Dim strKnownas As String = String.Empty
            Dim strFMG As String = String.Empty
            Dim strBUHead As String = String.Empty
            Dim strRR As String = String.Empty
            Dim strUKnownas As String = String.Empty
            Dim strSubject As String = String.Empty
            Dim dsGET_ASSET_REQ_EMAILS As New DataSet
            Dim strRMAur_id As String
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = strReqId

            dsGET_ASSET_REQ_EMAILS = objsubsonic.GetSubSonicDataSet("GET_ASSET_INTRA_REQ_EMAILS", param)

            If dsGET_ASSET_REQ_EMAILS.Tables(0).Rows.Count > 0 Then
                strRR = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("EMAIL")
                strUKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RAISED_AUR_KNOWN_AS")
                strRM = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_EMAIL")
                strRMAur_id = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_ID")
                strKnownas = dsGET_ASSET_REQ_EMAILS.Tables(0).Rows(0).Item("RM_AUR_KNOWN_AS")
            End If


            '----------- Get Mail Content from MailMaster Table ---------------------
            Dim strAstList As String = String.Empty


            Dim dsMail_content As New DataSet
            Dim paramMail_content(0) As SqlParameter
            paramMail_content(0) = New SqlParameter("@Mail_id", SqlDbType.Int)
            paramMail_content(0).Value = MailStatus
            dsMail_content = objsubsonic.GetSubSonicDataSet("GET_MAILMASTER_ID", paramMail_content)
            If dsMail_content.Tables(0).Rows.Count > 0 Then
                body = dsMail_content.Tables(0).Rows(0).Item("MAIL_BODY")
                strSubject = dsMail_content.Tables(0).Rows(0).Item("MAIL_SUBJECT")
            End If

           


            Dim p1
            p1 = AstList.Item(0).ToString.Split(",")
            strAstList = "<table width='100%' border='1'>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Asset Code</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(0) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Location</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(1) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Tower</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(2) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>From Floor</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(3) & "</td></tr>"
            strAstList = strAstList & "<tr><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>Person To Receive Assets</td><td  style='background-color:#fff0e0;color:#00008B;font-family:Bookman Old Style;font-size:10.5pt;padding-left:5px'>" & p1(5) & "</td></tr>"


            Dim strApproveList As String = String.Empty
            Dim strRejectList As String = String.Empty

            Dim AppLink As String = String.Empty
            AppLink = ConfigurationManager.AppSettings("AppLink").ToString


            strApproveList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=ap&rl=rm&ty=imr> Click to Approve </a></td></tr></table>"

            strRejectList = "<table align='center' width='100%'><tr><td style='font-family:Bookman Old Style;font-size:10.5pt;color:Blue' align='Center'></td><a href=" & AppLink & "rid=" & strReqId & "&uid=" & strRMAur_id & "&st=rj&rl=rm&ty=imr>  Click to Reject </a></td></tr></table>"

            body = body.Replace("@@RAISED_USER", strUKnownas)
            body = body.Replace("@@REQ_ID", strReqId)
            body = body.Replace("@@Astlist", strAstList)
            body = body.Replace("@@REMARKS", strRemarks)
            body = body.Replace("@@RM", strKnownas)
            body = body.Replace("@@Approve", strApproveList)
            body = body.Replace("@@Reject", strRejectList)



            '1. IT Request Approved copy goes to request raised person.
            '2. One copy for IT ADMIN to approve.

            If App_Rej_status = False Then
                Insert_AmtMail(body, strRR, strSubject, strRM)
            Else
                Insert_AmtMail(body, strRM, strSubject, "")
            End If






        Catch ex As Exception

            Throw (ex)
        End Try


    End Sub

    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "IntraMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = getoffsetdatetime(DateTime.Now)
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub




    Private Function GenerateRequestId(ByVal TowerId As String, ByVal FloorId As String, ByVal DTowerId As String, ByVal DFloorId As String, ByVal AssetCode As String) As String
        Dim ReqId As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_MMT_MVMT_REQ_GetMaxMMR_ID")
        Dim SNO As String = CStr(sp.ExecuteScalar())
        ' ReqId = "MMR/" + TowerId + "/" + FloorId + "/" + AssetCode + "/" + DTowerId + "/" + DFloorId + "/" + SNO
        ReqId = "MMR/" + TowerId + "/" + FloorId + "/" + DTowerId + "/" + DFloorId + "/" + SNO
        Return ReqId
    End Function
     

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        Dim d As Integer = gvItems.PageCount
        Dim texts As String() = New String(gvItems.PageSize - 1) {}

        Dim count As Integer = 0
        For Each row As GridViewRow In gvItems.Rows

            Dim lblAAS_AAT_CODE As Label = DirectCast(row.FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = DirectCast(row.FindControl("lblAAT_NAME"), Label)
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)

            If chkSelect.Checked = True Then
                texts(count) = lblAAS_AAT_CODE.Text & "," & lblAAT_NAME.Text & "," & chkSelect.Checked
            Else
                texts(count) = ""
            End If
            count += 1
        Next

        Session("pageIntra" + gvItems.PageIndex.ToString) = texts
        gvItems.PageIndex = e.NewPageIndex
        BindAssetGrid()
    End Sub

    Protected Sub gvItems_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvItems.PreRender
        If Session("Flag") = 1 Then
            For i As Integer = 0 To gvItems.PageCount - 1
                Session("pageIntra" + i.ToString) = Nothing
            Next
            Session("Flag") = 0
        Else

            If Session("pageIntra" + gvItems.PageIndex.ToString) IsNot Nothing Then
                Dim texts As String() = DirectCast(Session("pageIntra" + CStr(gvItems.PageIndex)), String())
                For i As Integer = 0 To gvItems.Rows.Count - 1
                    Dim lblAAS_AAT_CODE As Label = DirectCast(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
                    Dim lblAAT_NAME As Label = DirectCast(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
                    Dim chkSelect As CheckBox = DirectCast(gvItems.Rows(i).FindControl("chkSelect"), CheckBox)

                    If texts(i).ToString = "" Then
                    Else
                        Dim prdValue As String() = texts(i).Split(",")
                        chkSelect.Checked = prdValue(2)
                    End If
 
                Next
            End If
        End If

    End Sub
 
    Private Sub BindCategories()
        GetChildRows("0")
        
    End Sub
    Private Sub GetChildRows(ByVal i As String)
        Dim str As String = ""
        Dim id
        If i = "0" Then
            id = CType(i, Integer)
        Else
            Dim id1 As Array = Split(i, "~")
            str = id1(0).ToString & "  --"
            id = CType(id1(1), Integer)
        End If


        Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
        Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
        Dim ds As New DataSet
        da.Fill(ds)



        If ds.Tables(0).Rows.Count > 0 Then
            For Each dr As DataRow In ds.Tables(0).Rows
                Dim j As Integer = CType(dr("CategoryId"), Integer)
                If id = 0 Then
                    str = ""
                End If
                Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
                'ddlAstCat.Items.Add(li)
                GetChildRows(str & "~" & j)
            Next
        End If
    End Sub
  

    Protected Sub ddlDFloor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDFloor.SelectedIndexChanged
        Emp_Loadddl(ddlEmp)
    End Sub

    Private Sub Emp_Loadddl(ByVal ddlEmp As DropDownList)
        Try
            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@loc", DbType.String)
            param(0).Value = ddlDLoc.SelectedItem.Value
            param(1) = New SqlParameter("@tow", DbType.String)
            param(1).Value = ddlDTower.SelectedItem.Value
            param(2) = New SqlParameter("@flr", DbType.String)
            param(2).Value = ddlDFloor.SelectedItem.Value
            ObjSubsonic.Binddropdown(ddlEmp, "GET_LOCATION_EMPLOYEES", "AUR_FIRST_NAME", "AUR_ID", param)

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub


End Class
