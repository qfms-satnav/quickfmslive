<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="frmGetDipreciationCost.aspx.vb" Inherits="FAM_FAM_Webfiles_frmGetDipreciationCost"
    Title="Get Depreciation Cost" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black">Get Depreciation Cost 
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>Get Depreciation Cost</strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                <table width="100%" cellpadding="2px">
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Label ID="lblMsg" runat="server" ForeColor="red"></asp:Label>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label2" runat="server" CssClass="bodytext" Text="Select Location"></asp:Label></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlLocation" AutoPostBack="true" CssClass="bodytext" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label3" runat="server" CssClass="bodytext" Text="Select Tower"></asp:Label></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTower" AutoPostBack="true" CssClass="bodytext" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Label4" runat="server" CssClass="bodytext" Text="Select Floor"></asp:Label></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlFloor" AutoPostBack="true" CssClass="bodytext" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                            <asp:Button ID="btnView" runat="server" Text="View" CssClass="button" />
                            <br />
                            <br />
                            Asset Value = (Cost - Salvage Value) / Life (no. of days)
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                        </td>
                        <td align="left">
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                            <fieldset id="Fieldset1" runat="server">
                                <legend>Asset List</legend>
                                <asp:GridView ID="gvAstList" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" EmptyDataText="No Assets Found" Width="100%">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Asset Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAAT_CODE" runat="server" Text='<%#Eval("AAT_CODE")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset ">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAIM_NAME" runat="server" Text='<%#Eval("AIM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Cost">
                                            <ItemTemplate>
                                                <asp:Label ID="lblRETAILPRICE" runat="server"  ></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Salvage Value">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsalvage" runat="server" Text='0'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Asset life (no. of days)">
                                            <ItemTemplate>
                                                <asp:Label ID="lbllife" runat="server" Text='0'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Asset Depreciation Cost">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAstDepCost" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" colspan="2">
                        </td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</asp:Content>
