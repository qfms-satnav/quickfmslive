<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmItemReq.aspx.vb" Inherits="FAM_FAM_Webfiles_ItemReq" Title="Item Requistion" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" contast_md_codeent="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItems.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }
        <%--function HeaderClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            //Checked/Unchecked all the checkBoxes in side the GridView.
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = CheckBox.checked;
            //Reset Counter
            Counter = CheckBox.checked ? TotalChkBx : 0;
        }--%>

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkItem";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Consumable Item Requisition</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                        ForeColor="Red" ValidationGroup="Val1" />
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                <asp:Label ID="Label2" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Location </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                            ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Asset Category </label>
                                        <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                                            Display="None" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                                            InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Asset Sub Category </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstSubCat"
                                            Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Asset Brand/Make </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAstBrand"
                                            Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Asset Model </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                                            Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="true" CssClass="btn btn-primary custom-button-color" Text="Submit" />
                                        <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>
                            <div id="pnlItems" runat="server">
                                <div class="clearfix">
                                    <div class="" style="height: 41px;">
                                        <h3 class="panel-title">Item List</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search By Any.."></asp:TextBox>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                        <asp:Button ID="Button1" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" OnClientClick="Button1_Click" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                                            EmptyDataText="No Asset(s) Found." CssClass="table GridStyle" GridLines="None" DataKeyNames="AST_MD_CODE">
                                            <Columns>
                                                <asp:BoundField DataField="AST_MD_CODE" HeaderText="Item Code" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Category" HeaderText="Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="manufacturer" HeaderText="Brand Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Item Name" ItemStyle-HorizontalAlign="left" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="AST_MD_CODE" runat="server" Text='<%#Eval("AST_MD_CODE") %>'> </asp:Label>
                                                        <asp:Label ID="AST_MD_NAME" runat="server" Text='<%#Eval("AST_MD_NAME") %>'> </asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="AST_MD_TOTAVBL" HeaderText="Availability" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Required Qty" ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="form-inline"   ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" runat="server" CssClass="form-control mr-1" Text='<%# Bind("Req_Qty") %>' Width="50px" MaxLength="10"></asp:TextBox>
                                                        <asp:Label ID="lblunits" runat="server" Text='<%#Eval("UNITS")%>'></asp:Label>
                                                        <asp:Label ID="lblITEMCODE" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblMinOrdQty" runat="server" Text='<%#Eval("AST_MD_MINQTY") %>' Visible="false"> </asp:Label>
                                                        <asp:Label ID="lblCat" Text='<%#Eval("AST_MD_CATID") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblSubCat" Text='<%#Eval("AST_MD_SUBCATID") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblBrand" Text='<%#Eval("AST_MD_BRDID") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbltotal" Text='<%#Eval("AST_MD_TOTAVBL") %>' runat="server" Visible="false"></asp:Label>

                                                        <asp:Label ID="lbl_vt_code" Text='<%#Eval("VT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_ast_subcat_code" Text='<%#Eval("AST_SUBCAT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_manufactuer_code" Text='<%#Eval("manufactuer_code") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_ast_md_code" Text='<%#Eval("AST_MD_CODE") %>' runat="server" Visible="false"></asp:Label>



                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll" ItemStyle-HorizontalAlign="Center" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkItem', this.checked);"
                                                            ToolTip="Click to check all" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkItem" Checked='<%#Bind("ticked") %>' runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <br />

                                <div id="GrdHistHeading" runat="server" visible="false" style="height: 41px;">
                                    <h3 class="panel-title">Previous Request Details</h3>
                                </div>

                                <div class="row">
                                    <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
                                        <asp:GridView ID="ItemReqHist" runat="server" AllowPaging="False" AutoGenerateColumns="False"
                                            EmptyDataText="No Asset(s) Found." CssClass="table GridStyle" GridLines="None">
                                            <Columns>
                                                <asp:BoundField DataField="AID_REQ_ID" HeaderText="Request Id" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                                <asp:BoundField DataField="AID_ORD_QTY" HeaderText="Qty Ordered" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                                <asp:BoundField DataField="VT_TYPE" HeaderText="Type" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                                <asp:BoundField DataField="manufacturer" HeaderText="Brand" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                                <asp:BoundField DataField="AST_MD_NAME" HeaderText="Model" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                                <asp:BoundField DataField="REQUESTED_BY" HeaderText="Requested by" ItemStyle-HorizontalAlign="left"></asp:BoundField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>

                            </div>
                            <div class="row" id="remarksAndSubmitBtn" runat="server">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Remarks </label>
                                        <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRem"
                                            ErrorMessage="Please Enter Valid Remarks" Display="None"
                                            ValidationGroup="Val1">
                                        </asp:RegularExpressionValidator>
                                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRem"
                                            Display="None" ErrorMessage="Please Enter the Remarks" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtRem" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 20px">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" runat="server" CausesValidation="true" Text="Submit Request" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();" ValidationGroup="Val2" />
                                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        function refreshSelectpicker() {
            $("#<%=ddlastCat.ClientID%>").selectpicker();
            $("#<%=ddlastsubCat.ClientID%>").selectpicker();
            $("#<%=ddlAstBrand.ClientID%>").selectpicker();
            $("#<%=ddlAstModel.ClientID%>").selectpicker();
            $("#<%=ddlAstCat.ClientID%>").selectpicker();
            $("#<%=ddlLocation.ClientID%>").selectpicker();

        }
        refreshSelectpicker();


    </script>
</body>
</html>


