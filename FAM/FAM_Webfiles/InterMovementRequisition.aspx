<%@ Page Language="VB" AutoEventWireup="false" CodeFile="InterMovementRequisition.aspx.vb" Inherits="FAM_FAM_Webfiles_InterMovementRequisition1"
    Title="InterMovement Request" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
      <%--<link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
        <style>
       .thick-pink-line {
           width: 100%;
           height: 10px;
           background-color: #FFC0CB;
           margin: 40px 0;
       }
   </style>
    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.grdAssets.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=grdAssets.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Asset Reconciliation" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                            <h3 class="panel-title">Inter Movement Requisition</h3>
                        </div>
                        <div class="card">
<%--                        <div class="panel-body" style="padding-right: 10px;">--%>
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                                                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-12 control-label">Asset Category<span style="color: red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="ddlAssetCategory"
                                                                Display="none" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="selectpicker" data-live-search="true"
                                                                ToolTip="Select Asset Category" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-12 control-label">Asset Sub Category<span style="color: red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="ddlAstSubCat"
                                                                Display="none" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1">
                                                            </asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true"
                                                                AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">

                                                    <label class="col-md-12 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                                                    <div class="col-md-7">
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlAstBrand"
                                                            Display="none" ErrorMessage="Please Select Asset Brand/Make" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true"
                                                            AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-12 control-label">Asset Model<span style="color: red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="ddlModel"
                                                                Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlModel" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-12 control-label">Location <span style="color: red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <asp:RequiredFieldValidator ID="cvLocation" runat="server" ControlToValidate="ddlLocation"
                                                                Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1"
                                                                InitialValue="-1" Enabled="true"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-12 control-label">Destination Location to receive asset<span style="color: red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <asp:RequiredFieldValidator ID="rfcdestloc" runat="server" ControlToValidate="ddlDestLoc"
                                                                Display="None" ErrorMessage="Please Select Destination Location To Receive Asset" ValidationGroup="Val1" InitialValue="-1"
                                                                Enabled="true"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlDestLoc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%--<div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                         <label class="col-md-12 control-label">Receiving Person</label>
                                                        <div class="col-md-7">
                                                            <asp:TextBox ID="Txtmanager" runat="server" MaxLength="70" CssClass="form-control" Width="200px" data-live-search="true" Enabled="False" Visible="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>--%>

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-12 control-label">Receiver<span style="color: red;">*</span></label>
                                                        <div class="col-md-7">
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlreceving"
                                                                Display="None" ErrorMessage="Please Select Receiving Person" ValidationGroup="Val1"
                                                                InitialValue="-1" Enabled="true"></asp:RequiredFieldValidator>
                                                             <asp:DropDownList ID="ddlreceving" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>

                                                          <%--  <asp:DropDownList ID="ddlreceving" runat="server" DataTextField="AUR_KNOWN_AS"
                                                                DataValueField="AUR_KNOWN_AS" CssClass="selectpicker" data-live-search="true" AutoPostBack="True"
                                                                AppendDataBoundItems="true" TabIndex="3" Font-Size="small" EnableViewState="true">
                                                            </asp:DropDownList>--%>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        

                                    

                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-2"></div>
                                                        <div class="col-md-10">
                                                            <asp:Button ID="btnSubmit" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" CausesValidation="true" ValidationGroup="Val1" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       
                                            </div>
                                        <br />
                                        <br />
                                        <div class="thick-pink-line"></div>
                                        <br />
                                        <br />
                                        <div class="row">
                                            <div class="col-md-12 text-left">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <br />
                                                            <asp:TextBox ID="txtSearch" runat="server" placeholder="Search By AssetName or Asset Serial No..." CssClass="form-control"></asp:TextBox>

                                                        </div>
                                                        <div class="col-md-4">
                                                            <br />
                                                            <asp:Button ID="Button1" CssClass="btn btn-primary custom-button-color" runat="server" Text="Search"
                                                                CausesValidation="true" TabIndex="2" />
                                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 500px">
                                                <asp:GridView ID="grdAssets" runat="server" EmptyDataText="No Assets Found."
                                                    CssClass="table GridStyle" GridLines="none"
                                                    DataKeyNames="AAT_CODE" AutoGenerateColumns="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Asset Id" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAstCode" runat="server" Text='<%#Eval("AAT_CODE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAssetname" runat="server" Text='<%#Eval("AAT_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Asset Serial No" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAssetSerial" runat="server" Text='<%#Eval("AAT_AST_SERIALNO")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Asset Category" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAstCategory" runat="server" Text='<%#Eval("VT_TYPE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Asset SubCategory" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSubcategory" runat="server" Text='<%#Eval("AST_SUBCAT_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Brand" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBrand" runat="server" Text='<%#Eval("manufacturer")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Brand Make" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTEmp" runat="server" Text='<%#Eval("AST_MD_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Comments" ItemStyle-HorizontalAlign="left">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txtcomments" runat="server"></asp:TextBox>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkAll" runat="server" ItemStyle-HorizontalAlign="center" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                                    ToolTip="Click to check all" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkSelect" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                                            </ItemTemplate>
                                                            <HeaderStyle Width="50px" HorizontalAlign="Center" />
                                                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="col-md-3 col-sm-12 col-xs-12" id="txtremrks" visible="false" runat="server">
                                            <div class="form-group">

                                                <label class="col-md-12 control-label">Remarks<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                                                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val2"
                                                    Enabled="true"></asp:RequiredFieldValidator>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" CssClass="form-control" Rows="3" maxlenght="5"></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                                <div class="form-group">
                                                    <asp:Button ID="btnAstReq" runat="server" Text="Submit" Visible="false" OnClientClick="javascript:return validateCheckBoxesMyReq();"
                                                        CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>
                    </div>
                <%--</div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        function refreshSelectpicker() {
            $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
            $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
            $("#<%=ddlAstBrand.ClientID%>").selectpicker();
            $("#<%=ddlModel.ClientID%>").selectpicker();
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlDestLoc.ClientID%>").selectpicker();
            $("#<%=ddlreceving.ClientID%>").selectpicker();



        }
        refreshSelectpicker();
    </script>
</body>
</html>
