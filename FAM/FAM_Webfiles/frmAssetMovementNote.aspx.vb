Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_frmAssetMovementNote
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim FromLoc, ToLoc, FromLocCode, ToLocCode, REQ_ID As String
    Dim receiveAst, strremarks As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            Dim UID As String = ""
            If Session("uid") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                UID = Session("uid")
            End If
            FillReqIds()
        End If
    End Sub

    Public Sub FillReqIds()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@COMPANYID", SqlDbType.VarChar)
        param(1).Value = Session("COMPANYID")
        ObjSubsonic.BindGridView(gvReqIds, "AM_GET_IT_INTER_MOVEMENT_REQS_AM_NOTE", param)
    End Sub
    Protected Sub gvReqIds_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReqIds.PageIndexChanging
        gvReqIds.PageIndex = e.NewPageIndex
        FillReqIds()
    End Sub

    Protected Sub gvReqIds_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqIds.RowCommand
        If e.CommandName = "Details" Then
            'Response.Redirect("ITApprovalDtls.aspx?Req_id=" & e.CommandArgument)
            astmvmntNote.Visible = True
            hdnReqID.Value = e.CommandArgument
            getDetailsbyReqId(e.CommandArgument)
            BindLocations()
            'BindFromPersons()
            'BindToPersons()
            BindRequestAssets(hdnReqID.Value)
        End If
    End Sub

    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)
        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("AM_GET_INTER_MVMT_DETLS_BY_REQID", param)
        If dr.Read Then
            FromLoc = dr.Item("FromLoc")
            ToLoc = dr.Item("ToLoc")
            FromLocCode = dr.Item("FromLocCode")
            ToLocCode = dr.Item("ToLocCode")
            strremarks = dr.Item("MMR_COMMENTS")

        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If
        TOFROMPERSONS(strREQ_id)
    End Sub
    Private Sub TOFROMPERSONS(ByVal strREQ_id As String)
        'ddlModel.Items.Clear()
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_BRANCH_MANAGER_BYLOCATION")
        'sp.Command.AddParameter("@AUR_LOCATION", ddlDestLoc.SelectedItem.Value, DbType.String)
        ddlFromPerson.Text = ""
        ddlToPerson.Text = ""
        Dim raisedby As String
        Dim receivedby As String
        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.VarChar, 50)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("AST_MMT_MVMT_PERSONS_TO_FROM", param)
        If dr.Read Then
            ddlFromPerson.Text = dr.Item("raisedby")
            ddlToPerson.Text = dr.Item("receivedby")

            hdnRaisedBy.Value = dr.Item("raised_id")
            hdnReceivedBy.Value = dr.Item("received_id")
        End If
        If dr.IsClosed = False Then
            dr.Close()


        End If



    End Sub
    Private Sub BindLocations()
        BindLocation()
        BindDestLoactions()
        ddlSLoc.Items.FindByValue(FromLocCode).Selected = True
        ddlDLoc.Items.FindByValue(ToLocCode).Selected = True
    End Sub
    Private Sub BindDestLoactions()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_LOCATIONS")
            sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlDLoc.DataSource = sp.GetDataSet()
            ddlDLoc.DataTextField = "LCM_NAME"
            ddlDLoc.DataValueField = "LCM_CODE"
            ddlDLoc.DataBind()
            ddlDLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

#Region "Below code used before Auto complete to bind the drop downs"
    'Private Sub BindFromPersons()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USERS")
    '        sp.Command.AddParameter("@COMPANYID", Session("companyid"), DbType.String)
    '        ddlFromPerson.DataSource = sp.GetDataSet()
    '        ddlFromPerson.DataTextField = "AUR_FIRST_NAME"
    '        ddlFromPerson.DataValueField = "AUR_ID"
    '        ddlFromPerson.DataBind()
    '        ddlFromPerson.Items.Insert(0, New ListItem("--Select--", "0"))
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    'Private Sub BindToPersons()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USERS")
    '        sp.Command.AddParameter("@COMPANYID", Session("companyid"), DbType.String)
    '        ddlToPerson.DataSource = sp.GetDataSet()
    '        ddlToPerson.DataTextField = "AUR_FIRST_NAME"
    '        ddlToPerson.DataValueField = "AUR_ID"
    '        ddlToPerson.DataBind()
    '        ddlToPerson.Items.Insert(0, New ListItem("--Select--", "0"))
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
#End Region

    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlSLoc.DataSource = sp.GetDataSet()
            ddlSLoc.DataTextField = "LCM_NAME"
            ddlSLoc.DataValueField = "LCM_CODE"
            ddlSLoc.DataBind()
            ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "AM_GET_MVMT_ASTS", param)
    End Sub


    Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
        If ddlSLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlSLoc.SelectedItem.Value
            getDetailsbyReqId(Request.QueryString("Req_id"))
        End If
    End Sub



    Protected Sub ddlDLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLoc.SelectedIndexChanged
        If ddlDLoc.SelectedIndex > 0 Then
            Dim LocCode As String = ddlDLoc.SelectedItem.Value
        End If
    End Sub



    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If rbReturn.SelectedValue = "1" Then
            If txtReturnDate.Text.ToString() = "" Then
                lblMsg.Text = "Please Select returnable date"
                lblMsg.Visible = True
                Exit Sub
            Else
                lblMsg.Text = False
            End If
        End If
        Dim param(11) As SqlParameter
        param(0) = New SqlParameter("@AMN_MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = hdnReqID.Value
        param(1) = New SqlParameter("@AMN_FROM_LOC", SqlDbType.NVarChar, 200)
        param(1).Value = ddlSLoc.Text
        param(2) = New SqlParameter("@AMN_TO_LOC", SqlDbType.NVarChar, 200)
        param(2).Value = ddlDLoc.Text
        param(3) = New SqlParameter("@AMN_PERSON_SENDING", SqlDbType.NVarChar, 200)
        param(3).Value = hdnRaisedBy.Value
        'param(3).Value = Split(ddlFromPerson.Text, "/")(0)
        param(4) = New SqlParameter("@AMN_PERSON_RECEIVING", SqlDbType.NVarChar, 200)
        param(4).Value = hdnReceivedBy.Value
        'param(4).Value = Split(ddlToPerson.Text, "/")(0)
        param(5) = New SqlParameter("@AMN_SENDING_DATE", SqlDbType.NVarChar, 200)
        param(5).Value = txtSendDate.Text
        param(6) = New SqlParameter("@AMN_RECEIVING_DATE", SqlDbType.NVarChar, 200)
        param(6).Value = txtReceiveDate.Text
        param(7) = New SqlParameter("@AMN_RETURN_DATE", SqlDbType.NVarChar, 200)
        param(7).Value = txtReturnDate.Text
        param(8) = New SqlParameter("@AMN_RETURNABLE", SqlDbType.NVarChar, 200)
        param(8).Value = rbReturn.SelectedValue

        param(9) = New SqlParameter("@AMN_REMARKS", SqlDbType.NVarChar, 200)
        param(9).Value = txtRemarks.Text

        param(10) = New SqlParameter("@AMN_COMPANY_ID", SqlDbType.NVarChar, 200)
        param(10).Value = Session("companyid")

        param(11) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(11).Value = Session("uid")

        ObjSubsonic.GetSubSonicExecute("AM_INSERT_ASSET_MOVEMENT_NOTE_DETAILS", param)

        'SendMail(hdnReqID.Value)




        Response.Redirect("frmAssetThanks.aspx?RID=AssetMovementNoteGenerated")

    End Sub

    Private Sub SendMail(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_MVMT_NOTE_GENERATE")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Execute()
    End Sub
    Private Sub Insert_AmtMail(ByVal strBody As String, ByVal strEMAIL As String, ByVal strSubject As String, ByVal strCC As String)
        Dim paramMail(7) As SqlParameter
        paramMail(0) = New SqlParameter("@VC_ID", SqlDbType.NVarChar, 50)
        paramMail(0).Value = "IntraMovement"
        paramMail(1) = New SqlParameter("@VC_MSG", SqlDbType.NVarChar, 50)
        paramMail(1).Value = strBody
        paramMail(2) = New SqlParameter("@vc_mail", SqlDbType.NVarChar, 50)
        paramMail(2).Value = strEMAIL
        paramMail(3) = New SqlParameter("@VC_SUB", SqlDbType.NVarChar, 50)
        paramMail(3).Value = strSubject
        paramMail(4) = New SqlParameter("@DT_MAILTIME", SqlDbType.NVarChar, 50)
        paramMail(4).Value = getoffsetdatetime(DateTime.Now)
        paramMail(5) = New SqlParameter("@VC_FLAG", SqlDbType.NVarChar, 50)
        paramMail(5).Value = "Request Submitted"
        paramMail(6) = New SqlParameter("@VC_TYPE", SqlDbType.NVarChar, 50)
        paramMail(6).Value = "Normal Mail"
        paramMail(7) = New SqlParameter("@VC_MAIL_CC", SqlDbType.NVarChar, 50)
        paramMail(7).Value = strCC
        ObjSubsonic.GetSubSonicExecute("USP_SPACE_INSERT_AMTMAIL", paramMail)
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        astmvmntNote.Visible = False
    End Sub



End Class
