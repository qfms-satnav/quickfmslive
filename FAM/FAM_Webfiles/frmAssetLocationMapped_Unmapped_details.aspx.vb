Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Partial Class frmAssetLocationMapped_Unmapped_details
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions

    Dim Prd_id As Integer
    Dim strLocation As String


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        'Meta tags
       
        '************************


    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim str As String
        str = Request.QueryString("Prd_id")
        Dim array(2) As String
        array = str.Split("~")
        Prd_id = array(0)
        strLocation = array(1)
        If Not IsPostBack Then
            BindMappedAssets(Prd_id, strLocation)
            BindUnMappedAssets(Prd_id, strLocation)
        End If
    End Sub

    Private Sub BindMappedAssets(ByVal PrdId As Integer, ByVal LCM_Code As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = PrdId
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = LCM_Code
        ObjSubSonic.BindGridView(gvMappedAssets, "GET_MAPPED_ASTSLIST_LOCATION", param)
    End Sub

    Private Sub BindUnMappedAssets(ByVal PrdId As Integer, ByVal LCM_Code As String)

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@PRD_ID", SqlDbType.Int)
        param(0).Value = PrdId
        param(1) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(1).Value = LCM_Code
        ObjSubSonic.BindGridView(gvUnmappedAst, "GET_UNMAPPED_ASTSLIST_LOCATION", param)

    End Sub

    Protected Sub gvMappedAssets_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvMappedAssets.PageIndexChanging
        gvMappedAssets.PageIndex = e.NewPageIndex
        BindMappedAssets(Prd_id, strLocation)

    End Sub

    Protected Sub gvUnmappedAst_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvUnmappedAst.PageIndexChanging
        gvUnmappedAst.PageIndex = e.NewPageIndex
        BindUnMappedAssets(Prd_id, strLocation)
    End Sub
End Class
