<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddAssetModel.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAddAssetModel" Title="Add Asset Model" %>

<%@ Register Src="../../Controls/AddAssetModel.ascx" TagName="AddAssetModel" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Asset Brand Master" ba-panel-class="with-scroll">--%>
                <%--<div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Asset Model Master</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                <h3>Asset Model Master</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CityPanel1" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="VerticalValidations" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                            <uc1:AddAssetModel ID="AddAssetModel1" runat="server"></uc1:AddAssetModel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%-- </div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



