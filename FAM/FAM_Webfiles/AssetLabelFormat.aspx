﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="AssetLabelFormat.aspx.vb" Inherits="FAM_FAM_Webfiles_AssetLabelFormat" %>

<!DOCTYPE html>

<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Asset Brand Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Asset Label Format</h3>
            </div>
            <div class="card">
                <%--<div class="card-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-12 control-label">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class=" btn-default">
                                <asp:TextBox ID="txtcmpny" runat="server" CssClass="form-control"></asp:TextBox>
                            </label>
                        </div>
                        <div class="col-md-1">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckCity" runat="server" Text="City" />
                            </label>
                        </div>
                        <div class="col-md-2">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckLoc" runat="server" Text="Location" />

                            </label>
                        </div>
                        <div class="col-md-2">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckCat" runat="server" Text="Category" />

                            </label>
                        </div>
                        <div class="col-md-2">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckSubcat" runat="server" Text="SubCategory" />

                            </label>
                        </div>
                        <div class="col-md-1">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckBrand" runat="server" Text="Brand" />

                            </label>
                        </div>
                        <div class="col-md-1">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckModel" runat="server" Text="Model" />

                            </label>
                        </div>
                    </div>

                    <%--<asp:CheckBox ID="CheckLoc" runat="server" Text="Location" />
                            <asp:CheckBox ID="CheckCat" runat="server" Text="Category" />--%>
                    <%--<asp:CheckBox ID="CheckSubcat" runat="server" Text="SubCategory" />
                            <asp:CheckBox ID="CheckBrand" runat="server" Text="Brand" />
                            <asp:CheckBox ID="CheckModel" runat="server" Text="Model" />--%>

                    <br />
                    <br />
                    <div class="row" style="padding-left: 20px">
                        <asp:Label ID="Label1" runat="server" Text="Label Format :"></asp:Label>
                        <b>
                            <asp:Label ID="LabelFormat" runat="server" Visible="false"></asp:Label></b>

                    </div>

                    <div class="row" id="submitbtn" runat="server">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <asp:Button ID="btnView" runat="server" CssClass="btn btn-default btn-primary" Text="View" OnClick="btnView_Click" />
                                <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-default btn-primary" Text="Submit" />
                                <asp:Button ID="btnBack" runat="server" CssClass="btn btn-default btn-primary" Text="Back" PostBackUrl="~/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx" />
                            </div>
                        </div>
                    </div>

                    <div class="panel-heading" style="height: 41px;">
                        <h3 class="panel-title">Asset Numbering Format</h3>
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <div class="form-group">

                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                <asp:Label ID="Label2" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <label class="col-md-12 control-label">
                                        <asp:Label ID="lblMsg1" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class=" btn-default">
                                <asp:RequiredFieldValidator ID="cmpny1" runat="server" ControlToValidate="txtcmpny1"
                                    Display="None" ErrorMessage="Please Enter Value in Textbox" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtcmpny1" runat="server" CssClass="form-control"></asp:TextBox>
                            </label>
                        </div>
                        <div class="col-md-1">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckCity1" runat="server" Text="City" />
                            </label>
                        </div>
                        <div class="col-md-2">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckLocation" runat="server" Text="Location" />

                            </label>
                        </div>
                        <div class="col-md-2">
                            <label class=" btn-default">
                                <asp:CheckBox ID="CheckCategory" runat="server" Text="Category" />
                            </label>
                        </div>
                        <div class="col-md-2">
                            <label class=" btn-default">
                                <asp:RadioButton ID="CheckSubCategory" GroupName="label" runat="server" Text="SubCategory" />

                            </label>
                        </div>
                        <div class="col-md-1">
                            <label class=" btn-default">
                                <asp:RadioButton ID="CheckBrand1" GroupName="label" runat="server" Text="Brand" />

                            </label>
                        </div>
                        <div class="col-md-1">
                            <label class=" btn-default">
                                <asp:RadioButton ID="CheckModel1" GroupName="label" runat="server" Text="Model" />

                            </label>
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="row" style="padding-left: 20px">
                        <asp:Label ID="Label6" runat="server" Text="Label Format :"></asp:Label>
                        <b>
                            <asp:Label ID="LabelFormat1" runat="server" Visible="false"></asp:Label></b>

                    </div>

                    <div class="row" id="Div2" runat="server">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <asp:Button ID="btnView1" runat="server" CssClass="btn btn-default btn-primary" Text="View" ValidationGroup="Val1" OnClick="btnView1_Click" />
                                <asp:Button ID="btnSubmit1" runat="server" CssClass="btn btn-default btn-primary" Text="Submit" ValidationGroup="Val1" />
                                <asp:Button ID="btnBack1" runat="server" CssClass="btn btn-default btn-primary" Text="Back" PostBackUrl="~/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%--</div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
