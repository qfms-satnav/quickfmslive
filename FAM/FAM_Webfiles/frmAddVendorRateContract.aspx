<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddVendorRateContract.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAddVendorRateContract" Title="Vendor Rate Contract Master" %>

<%@ Register Src="../../Controls/AddVendorRateContract.ascx" TagName="AddVendorRateContract" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function setup(id) {
            $('.date').datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Asset Brand Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Vendor Rate Contract Master</h3>
                        </div>--%>
                <h3>Vendor Rate Contract Master</h3>
            </div>
            <div class="card">
                <%--<div class="card-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CityPanel1" runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="VerticalValidations" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                            <uc1:AddVendorRateContract ID="AddVendorRateContract1" runat="server"></uc1:AddVendorRateContract>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%--</div>--%>
    <%-- </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

