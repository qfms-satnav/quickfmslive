﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_frmConsumableIssuance
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim ReqId As String
    Dim AST_CODE As String



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If


        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else

            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                'divitemlist.Visible = False
                gvitemslist.Visible = False
                ddllocation2.Visible = False
                BindUsers(UID)
                BindLocation()
                BindmovedLocation()
                BindCatergories()
                BindSubCatergories()
                BindAssetBrand()
                btnsubmit1.Visible = False
                divide.Visible = False

                'Panel1.Visible = False
                Session("AST_CODE") = ""
                Session("RID") = ""
                'GetallItemstotbl()

            End If
        End If
    End Sub
    Private Sub BindUsers(ByVal aur_id As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AMT_BINDUSERS_SP")
        sp.Command.AddParameter("@AUR_ID", aur_id, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        ddlEmp.DataSource = sp.GetDataSet()
        ddlEmp.DataTextField = "NAME"
        ddlEmp.DataValueField = "AUR_ID"
        ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, "--Select--")
        Dim li As ListItem = ddlEmp.Items.FindByValue(aur_id)
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub
    Private Sub BindLocation()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_LOCTION_ISSUANCES")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddllocation.DataSource = sp.GetDataSet()
        ddllocation.DataTextField = "LCM_NAME"
        ddllocation.DataValueField = "LCM_CODE"
        ddllocation.DataBind()
        ddllocation.Items.Insert(0, "--Select--")

    End Sub
    Private Sub BindmovedLocation()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_LOCTION_ISSUANCES")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddllocation1.DataSource = sp.GetDataSet()
        ddllocation1.DataTextField = "LCM_NAME"
        ddllocation1.DataValueField = "LCM_CODE"
        ddllocation1.DataBind()
        ddllocation1.Items.Insert(0, "--Select--")

    End Sub
    Private Sub BindCatergories()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESSCON_CON")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
        ddlAstCat.Items.Insert(1, "--All--")
    End Sub
    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
        lblMsg.Text = ""
        btnsubmit.Visible = False
        btnsubmit1.Visible = False
        If ddlAstCat.SelectedIndex > 0 Then

            BindSubCatergories()
        Else
        End If
    End Sub
    Private Sub BindSubCatergories()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, "--Select--")
        ddlAstSubCat.Items.Insert(1, "--All--")

    End Sub
    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        lblMsg.Text = ""
        btnsubmit.Visible = False
        btnsubmit1.Visible = False
        If ddlAstCat.SelectedIndex > 0 Then
            BindAssetBrand()
        End If

    End Sub
    Private Sub BindAssetBrand()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(1, "--All--")

    End Sub
    'Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
    '    lblMsg.Text = ""
    '    GetallItemstotbl()
    '    gvItems.Visible = True
    '    btnsubmit.Visible = True
    '    gvitemslist.Visible = False
    '    btnsubmit1.Visible = False
    '    Panel1.Visible = False

    'End Sub
    Public Sub GetallItemstotbl()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MODEL_LISTFORGRID")
        sp.Command.AddParameter("@LOCTN_CODE", ddllocation.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedValue, DbType.String)
        gvItems.DataSource = sp.GetDataSet()

        gvItems.DataBind()
        'gvItems.Visible = True
        'exportpanel1.Visible = True
    End Sub
    Protected Sub btnsubmit_Click(sender As Object, e As EventArgs) Handles btnsubmit.Click
        Getallselectedmodels()
        'Panel1.Visible = False
        If (gvitemslist.Rows.Count = 0) Then
            btnsubmit1.Enabled = False
        Else
            btnsubmit1.Enabled = True
        End If
    End Sub
    Protected Sub Validate()
        Dim exist As Boolean = False
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblitem As Label = DirectCast(row.FindControl("lblITEMCODE"), Label)

            If chkSelect.Checked = True Then
                exist = True
                gvItems.Visible = False
                gvitemslist.Visible = True
                btnsubmit1.Visible = True
                btnsubmit2.Visible = True
                ddllocation2.Visible = True
                divide.Visible = True
                lblMsg.Text = ""
                txtReqID.Visible = False
                'ElseIf chkSelect.Checked = False Then

                '    lblMsg.Text = "Please select items for issuance"
                'Else
                '    gvItems.Visible = True

            End If
            If exist = False Then
                lblMsg.Text = "Please select items for issuance"
                gvitemslist.Visible = False
                ddllocation2.Visible = False
            End If
        Next
    End Sub
    Public Sub Getallselectedmodels()

        Dim model As String = ""
        Dim uncheck As Boolean = False
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblitem As Label = DirectCast(row.FindControl("lblITEMCODE"), Label)
            Dim lblmdid As Label = DirectCast(row.FindControl("lblmdid"), Label)
            If chkSelect.Checked = True Then

                model = model + lblmdid.Text + "/"
                gvItems.Visible = False
                gvitemslist.Visible = True
                'ddllocation2.Visible = True
                btnsubmit.Visible = False


                txtReqID.Visible = False
                lblMsg.Text = ""
                uncheck = True
            ElseIf chkSelect.Checked = False Then
                lblMsg.Text = "Please select items for issuance"
            Else
                gvItems.Visible = True
                gvitemslist.Visible = True
                'ddllocation2.Visible = True
                txtReqID.Visible = False
            End If
        Next

        lblMsg.Text = IIf(uncheck = False, "Please select items for issuance", "")

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_CONSUMABLES_FOR_ASSET_ISSUANCE")
        sp.Command.AddParameter("@MDID", model, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@LOC_CODE", ddllocation.SelectedValue, DbType.String)
        ViewState("dt") = sp.GetDataSet().Tables(0)
        If ViewState("dt").Rows.Count = 0 Then
            lblMsg.Text = "No Asset(s) Found"
            Exit Sub
        End If
        AST_CODE = ViewState("dt").Rows(0)("AAC_CODE").ToString()
        gvitemslist.Visible = True
        ddllocation2.Visible = True
        btnsubmit1.Visible = True
        Session("AST_CODE") = AST_CODE
        BindGrid()

    End Sub
    Protected Sub BindGrid()
        gvitemslist.DataSource = TryCast(ViewState("dt"), DataTable)
        gvitemslist.DataBind()
        If gvItems.Rows.Count = 0 Then
            btnsubmit.Visible = False
            txtReqID.Visible = False
        End If
    End Sub
    Protected Sub Cleardata()

        ddllocation.ClearSelection()
        ddllocation1.ClearSelection()
        ddlAstCat.ClearSelection()
        ddlAstSubCat.ClearSelection()
        ddlAstBrand.ClearSelection()
        gvItems.Visible = False
        gvitemslist.Visible = False
        ddllocation2.Visible = False
        btnsubmit.Visible = False
        btnsubmit1.Visible = False
        txtReqID.Visible = False
        lblMsg.Text = " "
        Session("AST_CODE") = ""

    End Sub
    Protected Sub bindissuance()

        Dim reqid As String
        Dim ds As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_INSERT_ITEMS_ISSUED")
        sp.Command.AddParameter("@LOCATION", ddllocation1.SelectedValue, DbType.String)
        sp.Command.AddParameter("@USERID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        ds = sp.GetDataSet()
        reqid = ds.Tables(0).Rows(0)(0).ToString()
        txtReqID.Text = reqid
        If reqid <> "" Then
            For Each row As GridViewRow In gvitemslist.Rows
                Dim lblmdid As Label = DirectCast(row.FindControl("lblmdid"), Label)
                Dim lblprice As Label = DirectCast(row.FindControl("lblprice"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
                Dim lbltotal As Label = DirectCast(row.FindControl("lbltotal"), Label)
                Dim txtdiscnt As TextBox = DirectCast(row.FindControl("txtdiscnt"), TextBox)
                Dim lblitem As Label = DirectCast(row.FindControl("lblITEMCODE"), Label)

                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_INSERT_ITEM_ISSUED_DETAILS")
                sp1.Command.AddParameter("@MODID", lblmdid.Text, DbType.String)
                sp1.Command.AddParameter("@PRICE", lblprice.Text, DbType.String)
                sp1.Command.AddParameter("@QTY", txtQty.Text, DbType.String)
                sp1.Command.AddParameter("@TOTAVBL", lbltotal.Text, DbType.String)
                sp1.Command.AddParameter("@DISCOUNT", txtdiscnt.Text, DbType.String)
                sp1.Command.AddParameter("@LOCATION", ddllocation1.SelectedValue, DbType.String)
                sp1.Command.AddParameter("@USERID", Session("uid"), DbType.String)
                sp1.Command.AddParameter("@REQID", reqid, DbType.String)
                sp1.Command.AddParameter("@ISSUESTAT", 1, DbType.Int32)

                sp1.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
                sp1.Command.AddParameter("@AST_CODE", Session("AST_CODE"), DbType.String)
                sp1.ExecuteScalar()
            Next
        End If

        'modalcontentframe.Src = "IssuanceInvoice.aspx?reqid=" & reqid
        'btnsubmit1.OnClientClick = "return showPopWin('" & reqid & "');"
        Session("AST_CODE") = ""

    End Sub


    Protected Sub btnsubmit1_Click(sender As Object, e As EventArgs) Handles btnsubmit1.Click
        Dim count As Integer = 0
        For Each row As GridViewRow In gvitemslist.Rows
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtdst As TextBox = DirectCast(row.FindControl("txtdiscnt"), TextBox)

            If String.IsNullOrEmpty(txtQty.Text) Then
                lblMsg.Text = "Please enter quantity in numerics"
                Exit Sub
            Else
                count = count + 1
            End If
        Next

        If (gvitemslist.Rows.Count = count) Then
            lblMsg.Text = ""
            bindissuance()
            Cleardata()
            If (rdbpo.Checked) Then
                'Panel1.Visible = True
                'ModalPopupExtender1.Show()
                btnsubmit.Visible = False
                Cleardata()
                lblMsg.Visible = True
                txtReqID.Visible = False
                divide.Visible = True
                lblMsg.Text = "Assets Issued Successfully"

            Else
                'ModalPopupExtender1.Hide()
                btnsubmit1.Visible = False
                'Panel1.Visible = False
                lblMsg.Visible = True
                txtReqID.Visible = False
                divide.Visible = False
                Cleardata()
                lblMsg.Text = "Assets Issued Successfully"

            End If
        Else
            lblMsg.Text = "Please enter quantity in numerics"
        End If

    End Sub
    Protected Sub txtQty_TextChanged(sender As Object, e As EventArgs)

        Dim currentRow As GridViewRow = DirectCast(DirectCast(sender, TextBox).Parent.Parent, GridViewRow)
        Dim txtqty As TextBox = DirectCast(currentRow.FindControl("txtQty"), TextBox)
        Dim txtdst As TextBox = DirectCast(currentRow.FindControl("txtdiscnt"), TextBox)
        Dim lbltotprice As Label = DirectCast(currentRow.FindControl("lbltlprice"), Label)
        Dim lblprice As Label = DirectCast(currentRow.FindControl("lblprice"), Label)

        If txtdst.Text = Nothing Then
            txtdst.Text = 0
        End If
        If txtqty.Text = Nothing Then
            txtqty.Text = 0
        End If

        Dim discount As Double
        Dim total As Integer = txtqty.Text * lblprice.Text

        discount = (total / 100) * txtdst.Text
        Dim disctot As Double = (total - discount)
        lbltotprice.Text = disctot.ToString("C")


    End Sub
    Protected Sub txtdiscnt_TextChanged(sender As Object, e As EventArgs)


        Dim currentRow As GridViewRow = DirectCast(DirectCast(sender, TextBox).Parent.Parent, GridViewRow)
        Dim txtqty As TextBox = DirectCast(currentRow.FindControl("txtQty"), TextBox)
        Dim txtdst As TextBox = DirectCast(currentRow.FindControl("txtdiscnt"), TextBox)
        Dim lbltotprice As Label = DirectCast(currentRow.FindControl("lbltlprice"), Label)
        Dim lblprice As Label = DirectCast(currentRow.FindControl("lblprice"), Label)

        If txtdst.Text = Nothing Then
            txtdst.Text = 0
        End If
        If txtqty.Text = Nothing Then
            txtqty.Text = 0
        End If

        Dim discount As Double
        Dim total As Integer = txtqty.Text * lblprice.Text

        discount = (total / 100) * txtdst.Text
        Dim disctot As Double = (total - discount)
        lbltotprice.Text = disctot.ToString("C")
        'Dim count As Int32 = Convert.ToInt32(txt.Text)
        'txt.Text = Convert.ToString(count + 10)


    End Sub
    Protected Sub gvitemslist_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvitemslist.RowDeleting
        Dim index As Integer = Convert.ToInt32(e.RowIndex)
        Dim dt As DataTable = TryCast(ViewState("dt"), DataTable)
        dt.Rows(index).Delete()
        ViewState("dt") = dt
        BindGrid()
    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        GetallItemstotbl()
    End Sub
    Protected Sub gvitemslist_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitemslist.PageIndexChanging
        gvitemslist.PageIndex = e.NewPageIndex
        Getallselectedmodels()
    End Sub
    Protected Sub ddllocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllocation.SelectedIndexChanged
        lblMsg.Text = ""
        btnsubmit.Visible = False
        txtReqID.Visible = False
        'btnsubmit1.Visible = False

        'Panel1.Visible = False
    End Sub
    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        lblMsg.Text = ""
        GetallItemstotbl()
        gvItems.Visible = True
        btnsubmit.Visible = True
        gvitemslist.Visible = False
        ddllocation2.Visible = False
        btnsubmit1.Visible = False
        txtReqID.Visible = False
        'Panel1.Visible = False
    End Sub
    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        Cleardata()
    End Sub

End Class
