<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAddAssetBrand.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAddAssetBrand" Title="Add Asset Brand" %>

<%@ Register Src="../../Controls/AddAssetBrand.ascx" TagName="AddAssetBrand" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Asset Brand Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Asset Brand Master</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                             <h3>Asset Brand Master</h3>
                      </div>
                        <div class="card">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel ID="CityPanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:ValidationSummary ID="VerticalValidations" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                        <uc1:AddAssetBrand ID="AddAssetBrand1" runat="server"></uc1:AddAssetBrand>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>
                    </div>
                </div>
            <%--</div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



