<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetThanks.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAssetThanks" Title="Asset Requisition" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
</head>
<body>

    <div id="pnlMain" runat="server" width="100%" groupingtext="Asset Requisition Status">
        <div class="animsition">
            <div class="al-content">
                <div class="widgets">
                    <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                        <div class="panel">
                            <div class="panel-heading" style="height: 41px;">
                                <h3 class="panel-title">Asset Management</h3>
                            </div>
                            <div class="panel-body" style="padding-right: 10px;">
                                <form id="form1" runat="server">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group text-center">
                                                <div class="row">
                                                    <asp:Label ID="lblHead" runat="server" ForeColor="#209e91"  CssClass="col-md-12 control-label"></asp:Label>
                                                    <asp:Label ID="lblMsg" runat="server" ForeColor="#209e91"  CssClass="col-md-12 control-label"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



