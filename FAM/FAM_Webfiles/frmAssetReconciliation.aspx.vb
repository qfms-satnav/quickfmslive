﻿Imports System.Data
Imports System.Data.SqlClient
Imports clsReports
Imports System.Configuration.ConfigurationManager
Imports Microsoft.Reporting.WebForms
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Imports System.IO
Imports System.Threading
Imports System.Data.OleDb

Partial Class FAM_FAM_Webfiles_frmAssetReconciliation
    Inherits System.Web.UI.Page
    Dim obj As New clsReports
    Dim objMasters As clsMasters
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(AppSettings("FMGLogout"))
        End If
        If Not Page.IsPostBack Then

            BindCity()
            ' BindLocation(ddlCity.SelectedItem.Value)
            ' BindTower(ddlLocation.SelectedItem.Value)
            'BindFloor()
            btnrecon.Visible = False
            head1.Visible = False
            head2.Visible = False
            Session("DataSet") = ""
            
        End If
    End Sub
    Public Sub BindCity()
        ObjSubsonic.Binddropdown(ddlCity, "USP_GETACTIVECITIES", "CTY_NAME", "CTY_CODE")

    End Sub


    Private Sub BindLocation(ByVal loc As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.Items.Insert(0, New ListItem("--All--", "ALL"))
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)

    End Sub

    'Public Sub BindLocation(ByVal MCity As String)

    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@CTY_ID", SqlDbType.NVarChar, 200)
    '    param(0).Value = MCity
    '    ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCATIONBY_CTY", "LCM_NAME", "LCM_CODE", param)
    'End Sub
    Public Sub BindTower(ByVal loc As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@LCM_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = loc
        ObjSubsonic.Binddropdown(ddlTower, "AST_GET_TOWERBYLOC", "TWR_NAME", "TWR_CODE", param)

    End Sub
    Public Sub BindFloor(ByVal loc As String, ByVal tower As String)
        

        ' ObjSubsonic.Binddropdown(ddlFloor, "AST_GET_FLOORBYLOCTOWER", "FLR_NAME", "FLR_CODE", param)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_FLOORBYLOCTOWER")
        sp.Command.AddParameter("@LCM_CODE", loc, DbType.String)
        sp.Command.AddParameter("@TWR_CODE", tower, DbType.String)
        ddlFloor.DataSource = sp.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))

        ' ObjSubsonic.Binddropdown(ddlFloor, "AST_GET_FLOORBYLOCTOWER", "FLR_NAME", "FLR_CODE")

    End Sub
    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged

        head1.Visible = False
        head2.Visible = False
        btnrecon.Visible = False
        btnExport.Visible = False
        ddlLocation.Items.Clear()
        ddlTower.ClearSelection()
        ddlFloor.ClearSelection()

        BindLocation(ddlCity.SelectedItem.Value)

        If ddlLocation.Items.Count = 0 Then
            lblMsg.Text = "No Locations Available for Selected City"
            lblMsg.Visible = True
        Else
            lblMsg.Text = ""
        End If

    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged

        head1.Visible = False
        head2.Visible = False
        btnrecon.Visible = False
        btnExport.Visible = False
        '   ddlLocation.Items.Clear()
        ddlTower.ClearSelection()
        ddlFloor.ClearSelection()

        BindTower(ddlLocation.SelectedItem.Value)

        If ddlTower.Items.Count = 0 Then
            lblMsg.Text = "No Towers Available for Selected Location"
            lblMsg.Visible = True
        Else
            lblMsg.Text = ""
        End If

    End Sub


    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged

        head1.Visible = False
        head2.Visible = False
        btnrecon.Visible = False
        btnExport.Visible = False
        '   ddlLocation.Items.Clear()
        ' ddlTower.ClearSelection()
        ddlFloor.ClearSelection()

        BindFloor(ddlLocation.SelectedItem.Value, ddlTower.SelectedItem.Value)

        If ddlTower.Items.Count = 0 Then
            lblMsg.Text = "No Towers Available for Selected Location"
            lblMsg.Visible = True
        Else
            lblMsg.Text = ""
        End If

    End Sub


    
    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            Dim connectionstring As String = ""
            If fpBrowseDoc.HasFile Then
                lblMsg.Visible = False
                Dim fs As System.IO.FileStream
                Dim strFileType As String = Path.GetExtension(fpBrowseDoc.FileName).ToLower()
                Dim fname As String = fpBrowseDoc.PostedFile.FileName
                Dim s As String() = (fname.ToString()).Split(".")
                Dim filename As String = s(0).ToString() & getoffsetdatetime(DateTime.Now).ToString("yyyyMMddhhmmss") & "." & s(1).ToString()
                Dim filepath As String = Replace(Request.PhysicalApplicationPath.ToString + "UploadFiles\", "\", "\\") & filename

                Try
                    fs = System.IO.File.Open(filepath, IO.FileMode.OpenOrCreate, IO.FileAccess.Read, IO.FileShare.None)
                    fs.Close()
                Catch ex As System.IO.IOException
                End Try
                fpBrowseDoc.SaveAs(Request.PhysicalApplicationPath.ToString + "UploadFiles\" + filename)
                'Connection String to Excel Workbook
                'If strFileType.Trim() = ".xls" Then
                '    connectionstring = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & filepath & ";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=2"""
                'ElseIf strFileType.Trim() = ".xlsx" Then
                '    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                'End If
                If strFileType.Trim() = ".xls" Or strFileType.Trim() = ".xlsx" Then
                    connectionstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & filepath & ";Extended Properties=""Excel 12.0;HDR=Yes;IMEX=2"""
                Else
                    lblMsg.Visible = True
                    lblMsg.Text = "Upload excel files only..."
                End If
                Dim con As New System.Data.OleDb.OleDbConnection(connectionstring)
                con.Open()
                Dim dt As New System.Data.DataTable()
                dt = con.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, Nothing)
                Dim listSheet As New List(Of String)
                Dim drSheet As DataRow
                For Each drSheet In dt.Rows
                    listSheet.Add(drSheet("TABLE_NAME").ToString())
                Next
                Dim str As String = ""
                Dim sheetname As String = ""
                Dim msheet As String = ""
                Dim mfilename As String = ""
                ' msheet = s(count - 1)
                msheet = listSheet(0).ToString()
                mfilename = msheet
                ' MessageBox.Show(s(count - 1))
                If dt IsNot Nothing OrElse dt.Rows.Count > 0 Then
                    ' Create Query to get Data from sheet. '
                    sheetname = mfilename 'dt.Rows(0)("table_name").ToString()
                    str = "Select * from [" & sheetname & "]"
                    'str = "Select * from [sheet1$]"
                End If
                Dim snocnt As Integer = 1
                Dim locationid_cnt As Integer = 1
                Dim category_codecnt As Integer = 1
                Dim category_namecnt As Integer = 1
                Dim status_cnt As Integer = 1
                Dim remarks_cnt As Integer = 1
                'Dim dmn_idcnt As Integer = 1

                Dim cmd As New OleDbCommand(str, con)
                Dim ds As New DataSet
                Dim da As New OleDbDataAdapter
                da.SelectCommand = cmd
                Dim sb As New StringBuilder
                Dim sb1 As New StringBuilder
                da.Fill(ds, sheetname.Replace("$", ""))
                If con.State = ConnectionState.Open Then
                    con.Close()
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If LCase(ds.Tables(0).Columns(0).ToString) <> "sno" Then
                            snocnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be SNO"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(1).ToString) <> "asset_code" Then
                            locationid_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset_Code"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(2).ToString) <> "asset_name" Then

                            category_codecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Asset_Name"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(3).ToString) <> "location" Then
                            category_namecnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Location"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(4).ToString) <> "vendor" Then
                            status_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Vendor"
                            Exit Sub
                        ElseIf LCase(ds.Tables(0).Columns(5).ToString) <> "cost" Then
                            remarks_cnt = 0
                            lblMsg.Visible = True
                            lblMsg.Text = "Column name should be Cost"
                            Exit Sub

                        End If
                    Next
                Next
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    'Start inseting excel data

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "INSERT_BARCODE_ASSETS")

                    sp.Command.AddParameter("@AST_CODE", ds.Tables(0).Rows(i).Item("ASSET_CODE").ToString, DbType.String)
                    sp.Command.AddParameter("@AST_NAME", ds.Tables(0).Rows(i).Item("ASSET_NAME").ToString, DbType.String)
                    sp.Command.AddParameter("@AST_COST", ds.Tables(0).Rows(i).Item("COST").ToString, DbType.Double)
                    sp.Command.AddParameter("@AST_VENDOR", ds.Tables(0).Rows(i).Item("VENDOR").ToString, DbType.String)
                    sp.Command.AddParameter("@LCM_CODE", ds.Tables(0).Rows(i).Item("LOCATION").ToString, DbType.String)
                    sp.ExecuteScalar()

                Next
                lblMsg.Visible = True
                lblMsg.Text = "Data uploaded successfully..."
                BindGrid1()
                'head1.Visible = True
                btnrecon.Visible = True
            Else
                lblMsg.Visible = True
                lblMsg.Text = "Please choose file..."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindGrid1()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "BIND_BARCODE_ASSETS")
        sp1.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        GridView1.DataSource = sp1.GetDataSet()
        GridView1.DataBind()

    End Sub
    Private Sub BindGrid2()
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETS_NOTIN_BARCODE")
        sp2.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)

        Session("DataSet") = sp2.GetDataSet()
        GridView2.DataSource = Session("DataSet")
        GridView2.DataBind()

    End Sub
    Protected Sub btnrecon_Click(sender As Object, e As EventArgs) Handles btnrecon.Click
        lblMsg.Visible = False
        head2.Visible = True
        BindGrid2()
        btnExport.Visible = True
    End Sub

    Protected Sub GridView1_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView1.PageIndexChanging
        GridView1.PageIndex = e.NewPageIndex
        BindGrid1()
    End Sub

    Protected Sub GridView2_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles GridView2.PageIndexChanging
        GridView2.PageIndex = e.NewPageIndex
        GridView2.DataSource = Session("DataSet")
        GridView2.DataBind()

    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click

        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ASSETS_NOTIN_BARCODE")
        sp3.Command.AddParameter("@LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        GridView2.DataSource = sp3.GetDataSet()
        GridView2.DataBind()

        Dim ds As New DataSet
        ds = sp3.GetDataSet()

        Dim gv As New GridView
        gv.DataSource = ds
        gv.DataBind()
        Export("ReconciliationAssets.xls", gv)
        'Export("ReconciliationAssets.xls", GridView2)
    End Sub
#Region "Export"
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub
#End Region

End Class
