Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions

Partial Class FAM_FAM_Webfiles_frmAssetRequisitionDetails_Procure
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Page)
        If scriptManager IsNot Nothing Then

            scriptManager.RegisterPostBackControl(grdDocs)

        End If
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                BindUsers(UID)
                BindCategories()
                pnlItems.Visible = True
                BindRequisition()
                rolebased()
            End If
        End If
    End Sub

    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function
    Private Sub rolebased()
        Dim User As String
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        Dim ds As DataSet
        ds = sp.GetDataSet

        For Each dr As DataRow In ds.Tables(0).Rows
            User = dr("URL_USR_ID")

        Next
        If User = Session("UID") Then
            'BindGrid(1012)
            pnlItems.Visible = False
            Div4.Visible = False
            Div5.Visible = False
            pnll.Visible = True
            pnl.Visible = False
        Else
            'BindGrid(1008)
            pnll.Visible = False
            pnl.Visible = True
            Div4.Visible = False
            Div5.Visible = False
        End If
    End Sub

    Private Sub BindRequisition()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_REQUISITION_GetByReqIdd")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId
                Dim RaisedBy As Integer = 0
                Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)
                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(dr("AIR_AUR_ID")))
                If Not li Is Nothing Then
                    li.Selected = True
                End If
                Dim CatId As String
                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))
                If Not li Is Nothing Then
                    li.Selected = True
                End If
                getassetsubcategory(CatId)
                Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")
                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True
                getbrandbycatsubcat(CatId, asstsubcat)

                Dim asstbrand As String = dr("AIR_ITEM_BRD")
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True

                getmakebycatsubcat()
                Dim asstmodel As String = dr("AIR_ITEM_MOD")
                ddlAstModel.Items.FindByValue(asstmodel).Selected = True

                BindLocation()
                Dim req_loc As String = dr("AIR_REQ_LOC")
                ddlLocation.Items.FindByValue(req_loc).Selected = True


                Dim rm As String
                Dim rmname As String
                Dim coordinater As String
                rm = dr("RM_ID")
                rmname = dr("RMAPPROVER")
                coordinater = dr("Coordinator")
                rmid.Text = rm + rmname
                Dim requester As String
                Dim reqqid As String
                Dim adm As String
                Dim admname As String
                reqqid = dr("AIR_AUR_ID")
                requester = dr("requester")
                adm = dr("ADM_ID")
                admname = dr("ADMAPPROVER")
                admid.Text = adm + admname

                'Coordinator


                CoordinateCRMid.Text = dr("AIR_APRCOORD_BY") + coordinater


                reqqer.Text = reqqid + requester
                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                proc_usr.Text = rm + rmname
                proc_rem.Text = dr("AIR_RM_REMARKS")

                txtCoordinateCheckRemarks.Text = dr("AIR_APRCOORD_REM")
                txtStatus.Text = dr("STA_TITLE")
                txtRemarks.ReadOnly = True
                txtRMRemarks.ReadOnly = True
                txtAdminRemarks.ReadOnly = True
                txtCoordinateCheckRemarks.ReadOnly = True
                txtStatus.ReadOnly = True
                proc_rem.ReadOnly = True
                If String.IsNullOrEmpty(Trim(txtRemarks.Text)) Then
                    trRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    trRMRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                    trAdminRemarks.Visible = False
                End If
                If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                    proc_rem.Visible = False
                End If
                BindDocuments(ReqId)
                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)
                'If RaisedBy = GetCurrentUser() Then
                '    If StatusId = 1001 Or StatusId = 1002 Then
                '        btnSubmit.Enabled = True
                '        btnCancel.Enabled = True
                '    Else
                '        btnSubmit.Enabled = False
                '        btnCancel.Enabled = False
                '    End If
                'Else
                '    btnSubmit.Enabled = False
                '    btnCancel.Enabled = False
                'End If
                Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
                sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
                gvItems.DataSource = sp1.GetDataSet
                gvItems.DataBind()
                pnlItems.Visible = True
            Else
                lblMsg.Text = "No such requisition found."
            End If
        End If
    End Sub

    Public Sub BindDocuments(ByVal ReqId As String)
        Dim dtDocs As New DataTable("Documents")
        Dim param(0) As SqlParameter
        'param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@AMG_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = ReqId
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("AST_Level1_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub


    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim ID = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & filePath
            Response.ContentType = "application/octet-stream"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "UploadFiles\", "") & """")
            Response.WriteFile(filePath)
            'Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub


    Private Sub BindUsers(ByVal aur_id As String)
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@req_id", SqlDbType.NVarChar, 200)
        param(0).Value = Request.QueryString("RID")
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(1).Value = aur_id
        ObjSubsonic.Binddropdown(ddlEmp, "AMT_BINDUSERS_SP_Raisedby", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(aur_id)
        If Not li Is Nothing Then
            li.Selected = True
        End If
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
        'Dim li As ListItem = ddlEmp.Items.FindByValue(Session("UID"))
        'If Not li Is Nothing Then
        '    li.Selected = True
        'End If
    End Sub

    Private Sub BindCategories()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
        'GetChildRows("0")
        ''ddlAstCat.DataSource = CategoryController.CategoryList
        ''ddlAstCat.DataTextField = "CategoryName"
        ''ddlAstCat.DataValueField = "CategoryID"
        ''ddlAstCat.DataBind()
        ''For Each li As ListItem In ddlAstCat.Items
        ''Next
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    'Private Sub GetChildRows(ByVal i As String)
    '    Dim str As String = ""
    '    Dim id
    '    If i = "0" Then
    '        id = CType(i, Integer)
    '    Else
    '        Dim id1 As Array = Split(i, "~")
    '        str = id1(0).ToString & "  --"
    '        id = CType(id1(1), Integer)
    '    End If
    '    Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
    '    Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
    '    Dim ds As New DataSet
    '    da.Fill(ds)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each dr As DataRow In ds.Tables(0).Rows
    '            Dim j As Integer = CType(dr("CategoryId"), Integer)
    '            If id = 0 Then
    '                str = ""
    '            End If
    '            Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
    '            ddlAstCat.Items.Add(li)
    '            GetChildRows(str & "~" & j)
    '        Next
    '    End If
    'End Sub
    'Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
    '    If ddlAstCat.SelectedIndex > 0 Then
    '        Dim CatId As Integer = 0
    '        Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
    '        gvItems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
    '        gvItems.DataBind()
    '        pnlItems.Visible = True
    '    End If
    'End Sub
    Protected Sub ddlAstSubCatSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged

        getbrandbycatsubcat(ddlAstCat.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged

        getmakebycatsubcat()
    End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Dim user2 As String
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim User As String
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        'Dim ds As DataSet
        'ds = sp.GetDataSet
        'For Each dr As DataRow In ds.Tables(0).Rows
        '    user = dr("URL_USR_ID")
        'Next
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        Dim ds As DataSet
        ds = sp.GetDataSet
        For Each dr As DataRow In ds.Tables(0).Rows
            User = dr("URL_USR_ID")
        Next
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument2]")
        Dim ds2 As DataSet
        ds2 = sp2.GetDataSet
        For Each dr As DataRow In ds2.Tables(0).Rows
            user2 = dr("URL_USR_ID")
        Next

        If User = Session("UID") Then
            RoleData()
        ElseIf user2 = Session("UID") Then
            RoleData()

        Else
            RoleNormalData()
        End If

        'Dim ds As DataSet
        'Dim lblReqID As Label
        'Dim FlagStock As Boolean = False
        'Dim FlagPurchase As Boolean = False
        'For Each row As GridViewRow In gvItems.Rows
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
        '    lblReqID = DirectCast(row.FindControl("lblReqID"), Label)
        '    If chkSelect.Checked Then
        '        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqId")
        '        sp1.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
        '        ds = sp1.GetDataSet
        '        For Each dr As DataRow In ds.Tables(0).Rows
        '            Dim lblProductId As String = dr("AST_MD_CODE")
        '            Dim txtQty As String = dr("AID_QTY")
        '            Dim txtStockQty As String = dr("AID_MVM_QTY")
        '            Dim txtPurchaseQty As String = dr("AID_ORD_QTY")
        '            If CInt(Trim(txtPurchaseQty)) > 0 Then
        '                FlagPurchase = True
        '            End If
        '            If CInt(Trim(txtStockQty)) > 0 Then
        '                FlagStock = True
        '            End If
        '            UpdateAssetRequisitionData(lblReqID.Text, Trim(lblProductId), CInt(Trim(txtStockQty)), CInt(Trim(txtPurchaseQty)))
        '        Next
        '        Dim StatusId As Integer = 0
        '        If FlagPurchase = True And FlagStock = True Then
        '            StatusId = 1016
        '        ElseIf FlagPurchase = True Then
        '            StatusId = 1012
        '        ElseIf FlagStock = True Then
        '            StatusId = 1015
        '        End If
        '        If StatusId > 0 Then
        '            UpdateData(lblReqID.Text, StatusId)
        '        End If
        '        send_mail_PO(lblReqID.Text, 1)
        '    End If
        'Next
        'Response.Redirect("frmAssetThanks.aspx?RID=" + lblReqID.Text)
    End Sub
    Private Sub RoleData()
        Dim ReqId As String = GetRequestId()
        Dim FlagStock As Boolean = False
        Dim FlagPurchase As Boolean = False
        'DeleteRequistionItems(ReqId)
        For Each row As GridViewRow In gvItems.Rows
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            If CInt(Trim(txtPurchaseQty.Text)) > 0 Then
                FlagPurchase = True
            End If
            If CInt(Trim(txtStockQty.Text)) > 0 Then
                FlagStock = True
            End If
            UpdateAssetRequisitionData(ReqId, Trim(lblProductId.Text), CInt(Trim(txtStockQty.Text)), CInt(Trim(txtPurchaseQty.Text)))
        Next
        Dim StatusId As Integer = 0
        If FlagPurchase = True And FlagStock = True Then
            StatusId = 1016
        ElseIf FlagPurchase = True Then
            StatusId = 3022
        ElseIf FlagStock = True Then
            StatusId = 1015
        End If
        If StatusId > 0 Then
            UpdateData(ReqId, StatusId)
        End If
        send_mail_PO(ReqId, 1)
        Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
    End Sub

    Private Sub RoleNormalData()
        Dim ReqId As String = GetRequestId()
        Dim FlagStock As Boolean = False
        Dim FlagPurchase As Boolean = False
        'DeleteRequistionItems(ReqId)
        For Each row As GridViewRow In gvItems.Rows
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            If CInt(Trim(txtPurchaseQty.Text)) > 0 Then
                FlagPurchase = True
            End If
            If CInt(Trim(txtStockQty.Text)) > 0 Then
                FlagStock = True
            End If
            UpdateAssetRequisitionData(ReqId, Trim(lblProductId.Text), CInt(Trim(txtStockQty.Text)), CInt(Trim(txtPurchaseQty.Text)))
        Next
        Dim StatusId As Integer = 0
        If FlagPurchase = True And FlagStock = True Then
            StatusId = 1016
        ElseIf FlagPurchase = True Then
            StatusId = 1012
        ElseIf FlagStock = True Then
            StatusId = 1015
        End If
        If StatusId > 0 Then
            UpdateData(ReqId, StatusId)
        End If
        send_mail_PO(ReqId, 1)
        Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
    End Sub
    Private Sub UpdateData(ByVal ReqId As String, ByVal StatusId As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionStatus_UpdateByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
        sp.Command.AddParameter("@UID", Session("UID"), DbType.String)
        'sp.Command.AddParameter("@REM", txtRMRemarks.Text, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@REM", txtapprovetorem.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub

    'Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As Integer, ByVal Qty As Integer)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_AddNew")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    sp.Command.AddParameter("@ProductId", ProductId, DbType.Int32)
    '    sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
    '    sp.ExecuteScalar()
    'End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim User As String
        Dim ReqId As String = GetRequestId()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[Rol_based_procument]")
        Dim ds As DataSet
        ds = sp.GetDataSet
        For Each dr As DataRow In ds.Tables(0).Rows
            User = dr("URL_USR_ID")
        Next
        If User = Session("UID") Then
            UpdateData(ReqId, 3023)
        Else
            UpdateData(ReqId, 1013)
        End If
        send_mail_PO(ReqId, 2)
        Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId())
    End Sub


    Public Sub send_mail_PO(ByVal ReqId As String, ByVal mode As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_APPROVE")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", mode, DbType.Int32)
        sp.Execute()
    End Sub

    Public Sub send_mail_PO(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_Reject")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Execute()
    End Sub

    'Private Sub CancelData(ByVal ReqId As String, ByVal Remarks As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisition_UpdateByReqId")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
    '    sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
    '    sp.Command.AddParameter("@CatId", CInt(ddlAstCat.SelectedItem.Value), DbType.Int32)
    '    sp.Command.AddParameter("@StatusId", 1003, DbType.Int32)
    '    sp.ExecuteScalar()
    'End Sub
    'Private Sub DeleteRequistionItems(ByVal ReqId As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_DeleteByReqId")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    sp.ExecuteScalar()
    'End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "UpdateRecord" Then
            Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
            Dim ReqId As String = GetRequestId()
            Dim ProductId As Integer = 0
            Integer.TryParse(e.CommandArgument, ProductId)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            UpdateAssetRequisitionData(ReqId, ProductId, CInt(Trim(txtStockQty.Text)), CInt(Trim(txtPurchaseQty.Text)))
        End If
    End Sub

    Private Sub UpdateAssetRequisitionData(ByVal ReqId As String, ByVal ProductId As String, ByVal StockQty As Integer, ByVal PurchaseQty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_UpdateStockPurchaseQtyByReqIdAndProductId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@SQty", StockQty, DbType.Int32)
        sp.Command.AddParameter("@PQty", PurchaseQty, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@USERID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@CatIdd", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_MODD", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_SUBCATT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRDD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmApproveToProcure.aspx")
    End Sub

    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ' ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        'ddlAstModel.Items.Insert(0, "--All--")
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        'ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 1, 0)
    End Sub

End Class
