<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmAssetRequisitionDetails.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAssetRequisitionDetails"
    Title="View/Modify Asset Requisition" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

        var TotalChkBx;
        var Counter;
        window.onload = function () {
            //Get total no. of CheckBoxes in side the GridView.
            TotalChkBx = parseInt('<%= Me.gvItems.Rows.Count%>');
            //Get total no. of checked CheckBoxes in side the GridView.
            Counter = 0;
        }


        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {
                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;
        }

         <%--check box validation--%>
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    return true;
                }
            }
            alert("Please select atleast one checkbox");
            return false;
        }
    </script>
</head>
<body>
    <%--<div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View/Modify Asset Requisition
                        </legend>
                    </fieldset>--%>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>View/Modify Asset Requisition</h3>
            </div>
            <div class="card" style="padding-right: 10px;">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                ForeColor="Red" ValidationGroup="Val1" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Requisition Id</label>
                                        <asp:TextBox ID="lblReqId" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Raised By</label>
                                        <asp:DropDownList ID="ddlEmp" runat="server" CssClass="selectpicker" data-live-search="true" Enabled="false"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Asset Category</label>
                                        <asp:DropDownList ID="ddlAstCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Asset Subcategory</label>
                                        <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Asset Brand/Make</label>
                                        <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Asset Model </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstBrand"
                                            Display="none" ErrorMessage="Please Select Asset Model" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlAstModel" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Location </label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please Select Location" InitialValue="--Select--"
                                            ControlToValidate="ddlLocation" Display="None" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true">
                                        </asp:TextBox>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSearch" runat="server" CausesValidation="true" CssClass="btn btn-primary" Text="Search" />
                                        <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary" Text="Clear" CausesValidation="False" />
                                    </div>
                                </div>
                            </div>

                            <div id="pnlItems" runat="server">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">

                                            <div class="panel-heading" style="height: 41px;">
                                                <h3 class="panel-title">Assets list</h3>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12" style="overflow: auto; width: 100%; min-height: 20px; max-height: 320px">
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                            EmptyDataText="No Asset(s) FounD." CssClass="table GridStyle" GridLines="None">
                                            <Columns>
                                                <asp:BoundField DataField="AST_MD_id" HeaderText="Code" ItemStyle-HorizontalAlign="left" Visible="FALSE" />
                                                <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblproductname" Text='<%#Eval("AST_MD_NAME") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Category" HeaderText="Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="manufacturer" HeaderText="Brand Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Quantity" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" class="form-control" runat="server" Width="50%" MaxLength="2" Text='<%#Eval("AID_QTY") %>'></asp:TextBox>
                                                        <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("AST_MD_CODE")%>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_vt_code" Text='<%#Eval("VT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_ast_subcat_code" Text='<%#Eval("AST_SUBCAT_CODE") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_manufactuer_code" Text='<%#Eval("manufactuer_code") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbl_ast_md_code" Text='<%#Eval("AST_MD_CODE") %>' runat="server" Visible="false"></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Estimated Cost" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtEstCost" class="form-control" runat="server" Width="50px" MaxLength="10" Text='<%#Eval("AID_EST_COST") %>'>0</asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="chkAll"  runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect', this.checked);"
                                                            ToolTip="Click to check all" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReq_status" runat="server" Visible="false" Text='<%# Eval("AIR_STA_ID") %>'></asp:Label>
                                                        <asp:CheckBox ID="chkSelect" Checked='<%#Bind("ticked") %>' runat="server" onclick="javascript:ChildClick(this);"></asp:CheckBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div id="remarksAndActionButtons" class="row" runat="server">
                                <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Status</label>
                                            <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true">
                                            </asp:TextBox>
                                        </div>
                                    </div>--%>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Requestor Remarks<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"
                                            Enabled="true"></asp:RequiredFieldValidator>
                                        <asp:TextBox ID="txtRemarks" runat="server" Height="50px" TextMode="MultiLine" CssClass="form-control">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div id="tr1" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Reporting Manager Remarks:</label>
                                        <asp:TextBox ID="txtRMRemarks" runat="server" Height="50px" TextMode="MultiLine" CssClass="form-control"
                                            ReadOnly="True">
                                        </asp:TextBox>
                                    </div>
                                </div>
                                <div id="tr2" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Skip Level Reporting Manager Remarks:</label>
                                        <asp:TextBox ID="txtAdminRemarks" runat="server" Height="50px" TextMode="MultiLine" CssClass="form-control"
                                            ReadOnly="True">
                                        </asp:TextBox>
                                    </div>
                                </div>



                                <div id="tr3" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Coordinate Check Remarks</label>
                                        <asp:TextBox ID="coordrem" runat="server" Height="50px" TextMode="MultiLine" CssClass="form-control"
                                            ReadOnly="True">
                                        </asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group">
                                        <%--<strong>Documents</strong>--%>
                                        <label class="col-md-12 control-label">Uploaded Documents</label>
                                        <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                            <br/>
                             <br/>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div id="tblGridDocs" runat="server">
                                            <asp:DataGrid ID="grdDocs" runat="server" CssClass="table GridStyle" GridLines="None" DataKeyField="ID"
                                                EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                <Columns>
                                                    <asp:BoundColumn Visible="False" DataField="ID" HeaderText="ID"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="AMG_FILEPATH" HeaderText="Document Name">
                                                        <HeaderStyle></HeaderStyle>
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="AMG_CREATED_DT" HeaderText="Document Date">
                                                        <HeaderStyle></HeaderStyle>
                                                    </asp:BoundColumn>
                                                    <asp:ButtonColumn Text="Download" CommandName="Download">
                                                        <HeaderStyle></HeaderStyle>
                                                    </asp:ButtonColumn>
                                                    <asp:ButtonColumn Text="Delete" CommandName="Delete">
                                                        <HeaderStyle></HeaderStyle>
                                                    </asp:ButtonColumn>
                                                </Columns>
                                                <HeaderStyle ForeColor="white" BackColor="Black" />
                                                <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" Text="Update" runat="server" ValidationGroup="Val1" CssClass="btn btn-primary" OnClientClick="javascript:return validateCheckBoxesMyReq();" />
                                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CssClass="btn btn-primary" />
                                        <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary" OnClick="btnBack_Click" />
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        function refreshSelectpicker() {
            $("#<%=ddlAstCat.ClientID%>").selectpicker();
            $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
            $("#<%=ddlAstBrand.ClientID%>").selectpicker();
            $("#<%=ddlLocation.ClientID%>").selectpicker();
            $("#<%=ddlAstModel.ClientID%>").selectpicker();
            $("#<%=ddlEmp.ClientID%>").selectpicker();
        }
        refreshSelectpicker();
    </script>
</body>
</html>
