<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmRMApproval.aspx.vb" Inherits="FAM_FAM_Webfiles_frmRMApproval" Title="RM Approval" %>

<%@ Register Src="../../Controls/RMApproval.ascx" TagName="RMApproval" TagPrefix="uc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Level1 Approval</h3>
            </div>
            <div class="card" style="padding-right: 10px;">
                <form id="form1" runat="server">
                    <uc1:RMApproval ID="RMApproval1" runat="server" />
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>




