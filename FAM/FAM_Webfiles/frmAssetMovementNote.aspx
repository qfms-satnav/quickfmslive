<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetMovementNote.aspx.vb" Inherits="FAM_FAM_Webfiles_frmAssetMovementNote" Title="Asset Movement Note" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="../../Controls/ITApproval.ascx" TagName="ITApproval" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--  <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                startDate: new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
        function refreshSelectpicker() {
            $("#<%=ddlDLoc.ClientID%>").selectpicker();
            $("#<%=ddlSLoc.ClientID%>").selectpicker();
        }
        refreshSelectpicker();
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="IT Approval" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Asset Movement Note</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="CountryPanel1" runat="server">
                        <ContentTemplate>
                            <asp:HiddenField ID="hdnReqID" runat="server" />
                            <div class="row" style="margin-top: 10px">
                                <div class="col-md-12">
                                    <fieldset>
                                        <legend>Inter Movement Requests </legend>
                                        <asp:GridView ID="gvReqIds" runat="server" EmptyDataText="No Intra Movement Requests Found." AllowPaging="true"
                                            PageSize="10" CssClass="table GridStyle" GridLines="none" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblMMR_REQ_ID" runat="server" Text='<%#Eval("MMR_REQ_ID") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="From Location" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSTWR_NAME" runat="server" Text='<%#Eval("FROMLOC")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="To Location" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSFLR_NAME" runat="server" Text='<%#Eval("TOLOC")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Request Date" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDATE" runat="server" Text='<%#Eval("MMR_MVMT_DATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Details/Approve" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkApproval" runat="server" CommandArgument='<%#Eval("MMR_REQ_ID") %>'
                                                            CommandName="Details">Details/Approve</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </fieldset>
                                </div>
                            </div>
                            <br />

                            <div class="row">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                    <div class="form-group">

                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                                        <asp:Label ID="lblMsg" runat="server" ForeColor="Red" CssClass="col-md-12 control-label"></asp:Label>

                                    </div>
                                </div>
                            </div>
                            <br />

                            <div id="astmvmntNote" runat="server" visible="false">
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-12 control-label">From Location</label>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlSLoc" Enabled="false" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-12 control-label">To Location</label>
                                                <div class="col-md-7">
                                                    <asp:DropDownList ID="ddlDLoc" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="True" Enabled="False">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-12 control-label">Sender</label>
                                                <div class="col-md-7">
                                                    <asp:RequiredFieldValidator ID="rfPersonSending" runat="server" ControlToValidate="ddlFromPerson"
                                                        Display="None" ErrorMessage="Please Select Person Sending" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <%--<asp:DropDownList ID="ddlFromPerson" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>--%>
                                                    <asp:TextBox ID="ddlFromPerson" runat="server" CssClass="form-control" Width="200px" Enabled="False"></asp:TextBox>
                                                    <asp:HiddenField ID="hdnRaisedBy" runat="server" />
                                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="ddlFromPerson" MinimumPrefixLength="2" EnableCaching="false"
                                                        CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetDetails" ServicePath="~/Autocompletetype.asmx">
                                                    </asp:AutoCompleteExtender>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-12 control-label">Receiver</label>
                                                <div class="col-md-7">
                                                    <asp:RequiredFieldValidator ID="rfPersonReceiving" runat="server" ControlToValidate="ddlToPerson"
                                                        Display="None" ErrorMessage="Please Select Person Receiving" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <%--<asp:DropDownList ID="ddlToPerson" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>--%>
                                                    <asp:TextBox ID="ddlToPerson" runat="server" CssClass="form-control" Width="200px" Enabled="False"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnReceivedBy" runat="server" />
                                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="ddlToPerson" MinimumPrefixLength="2" EnableCaching="false"
                                                        CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetDetails" ServicePath="~/Autocompletetype.asmx">
                                                    </asp:AutoCompleteExtender>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Sending Date <span style="color: red;">*</span></label>
                                            <div class="input-group date" style="width: 150px" id='fromdate'>
                                                <asp:RequiredFieldValidator ID="rfSendDate" runat="server" ControlToValidate="txtSendDate"
                                                    Display="None" ErrorMessage="Please Select Sending Date" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:TextBox runat="server" type="text" class="form-control" placeholder="mm/dd/yyyy" ID="txtSendDate"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Receiving Date</label>
                                            <div class="input-group date" style="width: 150px" id='todate'>
                                                <asp:RequiredFieldValidator ID="rfRecvDate" runat="server" ControlToValidate="txtReceiveDate"
                                                    Display="None" ErrorMessage="Please Select Receiving Date" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:TextBox runat="server" type="text" class="form-control" placeholder="mm/dd/yyyy" ID="txtReceiveDate"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Returnable Date<span style="color: red;">*</span> </label>
                                            <div class="input-group date" style="width: 150px" id='retDate'>
                                                <asp:TextBox runat="server" type="text" class="form-control" placeholder="mm/dd/yyyy" ID="txtReturnDate"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('retDate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Is Returnable<span style="color: red;">*</span>  </label>
                                            <div class="input-group date" style="width: 150px" id='Div1'>
                                                <asp:RadioButtonList ID="rbReturn" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table">
                                                    <asp:ListItem Text="Yes" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="No" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-12 control-label">Transfer note<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                                                    Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="txtRemarks" runat="server" TextMode="multiline" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvItems" runat="server" EmptyDataText="No Assets Found." AllowPaging="true"
                                            PageSize="10" CssClass="table GridStyle" GridLines="none" AutoGenerateColumns="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAAS_AAT_CODE" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAAT_NAME" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <br />
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Close" />
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" CausesValidation="true" ValidationGroup="Val1" Text="Submit" />
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </form>
            </div>
        </div>

    </div>
    </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

