﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports System.Drawing
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports System.IO
Partial Class FAM_FAM_Webfiles_EditPoDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        txtDOD.Attributes.Add("readonly", "readonly")
        RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        If Not IsPostBack Then
            pnlItems.Visible = True
            Session("req") = Request.QueryString("rid")
            BindRequisition()
            BindVendorDetails()
            BindState()
        End If
    End Sub
    Private Sub BindVendorDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GETVENDORS")
        ddlVendor.DataSource = sp.GetReader
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.FindByValue(de).Selected = True
        'ddlVendor.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub
    Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
        If ddlVendor.SelectedIndex > 0 Then

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_VENDOR_DETAILS")
            sp.Command.AddParameter("@VENDOR", ddlVendor.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                txtAdd.Text = ds.Tables(0).Rows(0).Item("AVR_ADDR")
                txtNumber.Text = ds.Tables(0).Rows(0).Item("MOBILE")
            End If

        Else

        End If
    End Sub
    Private Sub BindState()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_BIND_STATES")
        ddlState.DataSource = sp.GetReader
        ddlState.DataTextField = "AM_ST_NAME"
        ddlState.DataValueField = "AM_ST_CODE"
        ddlState.DataBind()
        If state <> "" Then
            ddlState.Items.FindByValue(state).Selected = True
        End If

        ddlState.Items.Insert(0, "--Select--")
    End Sub

    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function
    Dim ch, AssetCode, subcat, cat As String
    Private Sub BindRequisition()
        Dim ReqId As String = GetRequestId()
        'If String.IsNullOrEmpty(ReqId) Then
        '    lblMsg.Text = "No such requisition found."
        'Else
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AMG_ITEMPO_DETAILS_GetByPO")
        sp.Command.AddParameter("@PO", ReqId, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        Dim dr As SqlDataReader = sp.GetReader()
        'ds = sp.GetDataSet
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        Dim ds1 As New DataSet
        Dim spSts As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_MAKE_FOR_ASSETGRID")
        spSts.Command.AddParameter("@AUR_ID", Session("uid"), Data.DbType.String)
        spSts.Command.AddParameter("@PO", ReqId, DbType.String)
        ds1 = spSts.GetDataSet()

        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim ddlAssetCat As DropDownList = CType(gvItems.Rows(i).FindControl("ddlAssetCat"), DropDownList)
            Dim ddlAssetsubcat As DropDownList = CType(gvItems.Rows(i).FindControl("ddlAssetsubcat"), DropDownList)
            Dim ddlstat As DropDownList = CType(gvItems.Rows(i).FindControl("ddlproductname"), DropDownList)
            Dim ddlAssetCode As DropDownList = CType(gvItems.Rows(i).FindControl("ddlAssetCode"), DropDownList)
            ddlAssetCat.DataSource = ds1.Tables(1)
            ddlAssetCat.DataTextField = "VT_TYPE"
            ddlAssetCat.DataValueField = "VT_CODE"
            ddlAssetCat.DataBind()
            ddlAssetsubcat.DataSource = ds1.Tables(2)
            ddlAssetsubcat.DataTextField = "AST_SUBCAT_NAME"
            ddlAssetsubcat.DataValueField = "AST_SUBCAT_CODE"
            ddlAssetsubcat.DataBind()
            ddlstat.DataSource = ds1.Tables(4)
            ddlstat.DataTextField = "AST_MD_NAME"
            ddlstat.DataValueField = "AST_MD_CODE"
            ddlstat.DataBind()
            ddlAssetCode.DataSource = ds1.Tables(3)
            ddlAssetCode.DataTextField = "manufacturer"
            ddlAssetCode.DataValueField = "manufactuer_code"
            ddlAssetCode.DataBind()
            If dr.Read() Then
                ch = dr("productname")
                AssetCode = dr("brand")
                subcat = dr("AST_SUBCAT_name")
                cat = dr("VT_TYPE")
            End If
            ddlAssetCat.Items.Insert(0, "--Select--")
            ddlAssetsubcat.Items.Insert(0, "--Select--")
            ddlstat.Items.Insert(0, "--Select--")
            ddlAssetCode.Items.Insert(0, "--Select--")
            ddlAssetCat.Items.FindByText(cat).Selected = True
            ddlAssetsubcat.Items.FindByText(subcat).Selected = True
            ddlstat.Items.FindByText(ch).Selected = True
            ddlAssetCode.Items.FindByText(AssetCode).Selected = True
        Next
        pnlItems.Visible = True
        BindOtherDetails(ReqId)
        'End If
    End Sub

    Dim de, state As String
    Private Sub BindOtherDetails(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_GetBYPO")
        sp.Command.AddParameter("@PO", ReqId, DbType.String)
        Dim dr As SqlDataReader = sp.GetReader()
        If dr.Read() Then
            'txtPORaisedBy.Text = dr("AIP_AUR_ID")
            'txtRemarks.Text = dr("AIP_PO_REMARKS")
            'txtRequesterId.Text = dr("AIP_AUR_ID")
            'txtRequesterName.Text = dr("AIP_AUR_ID")
            'txtRequestorRemarks.Text = dr("AIP_PO_REMARKS")
            'txtDeptId.Text = dr("AIP_DEPT")
            'txtLocation.Text = dr("AIP_LOCATION")
            txtAdvance.Text = dr("AIPD_PLI")
            txtCommissioning.Text = dr("AIPD_PAY_COMMISSION")
            'txtCst.Text = dr("AIPD_TAX_CST")
            txtDelivery.Text = dr("AIPD_PAY_ONDELIVERY")
            txtDOD.Text = CDate(dr("AIPD_EXPECTED_DATEOF_DELIVERY"))
            txtExchange.Text = dr("AIPD_EXCHANGE_CHARGES")
            'txtFFIP.Text = dr("AIPD_TAX_FFIP")
            txtInstallation.Text = dr("AIPD_PAY_INSTALLATION")
            ' txtOctrai.Text = dr("AIPD_TAX_OCTRAI")
            ' txtOthers.Text = dr("AIPD_TAX_OTHERS")
            txtPayments.Text = dr("AIPD_PAY_OTHERS")
            txtRetention.Text = dr("AIPD_PAY_RETENTION")
            'txtServiceTax.Text = dr("AIPD_TAX_SERVICETAX")
            txtTotalCost.Text = dr("AIPD_TOTALCOST")
            ' txtWst.Text = dr("AIPD_TAX_WST")
            'ddlVendor.SelectedValue = dr("AIPD_VENDORNAME")
            de = dr("AIP_AVR_CODE")
            'txtVenCode.Text = dr("AIP_AVR_CODE")
            txtCst.Text = dr("AIPD_CGST")
            txtsgst.Text = dr("AIPD_SGST")
            txtigst.Text = dr("AIPD_IGST")
            txtAdd.Text = dr("AIPD_DADDRESS")
            txtNumber.Text = dr("AIPD_CONTACT")
            txtQref.Text = dr("AIPD_QREF")
            txtQdate.Text = dr("AIPD_QDATE")
            txtTerms.Text = dr("TERMS")
            state = dr("STATE_NAME")
            txtSGSTN.Text = dr("STATE_GST")
            txtSAddress.Text = dr("STATE_Address")
            txtPodetailsId.Text = dr("podetails_id")
            txtupremarks.Text = dr("aip_po_up_remarks")
            'txtfinremarks.Text = dr("aip_po_fin_rem")

            'Dim li As ListItem = ddlVendor.Items.FindByValue(Trim(CStr(dr("AIP_AVR_CODE"))))
            'If Not li Is Nothing Then
            '    li.Selected = True
            'End If
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim PONumber As String = GetRequestId()
        Dim Advance As Integer = 0
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        UpdateData(PONumber, 2, txtAdvance.Text)
        ''send_mail_Finalize_PO(PONumber)
        Response.Redirect("frmAssetThanks.aspx?PO=" + PONumber)
    End Sub
    Protected Sub ddlState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlState.SelectedIndexChanged
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_STATE_DETAILS")
        sp.Command.AddParameter("@STATE", ddlState.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            txtSGSTN.Text = ds.Tables(0).Rows(0).Item("AM_ST_GSTN")
            txtSAddress.Text = ds.Tables(0).Rows(0).Item("AM_ST_ADDRESS")
        End If
    End Sub
    Private Sub UpdateData(ByVal ReqId As String, ByVal StatusId As Integer, ByVal Advance As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_UpdateStatusFinal")
        sp.Command.AddParameter("@AIP_PO_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
        sp.Command.AddParameter("@AIP_PO_REMARKS", txtDelivery.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_Installation", txtInstallation.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_Commission", txtCommissioning.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_Retention", txtRetention.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_Payments", txtPayments.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_Cst", txtCst.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_sgst", txtsgst.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_igst", txtigst.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_TotalCost", txtTotalCost.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_Exchange", txtExchange.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_REMARKS", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@ADVANCE", Advance, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Sub UpdateData1(ByVal ReqId As String, ByVal StatusId As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_UpdateStatusFinal")
        sp.Command.AddParameter("@AIP_PO_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
        sp.Command.AddParameter("@AIP_PO_REMARKS", txtDelivery.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_Installation", txtInstallation.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_Commission", txtCommissioning.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_Retention", txtRetention.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_Payments", txtPayments.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_Cst", txtCst.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_sgst", txtsgst.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_igst", txtigst.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_TotalCost", txtTotalCost.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_Exchange", txtExchange.Text, DbType.String)
        sp.Command.AddParameter("@ADVANCE", txtAdvance.Text, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        sp.ExecuteScalar()
    End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Protected Sub ddlAssetCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(ddl.Parent.Parent, GridViewRow)
        Dim idx As Integer = row.RowIndex
        Dim st As String = ddl.SelectedItem.Value
        Dim ddl2 As DropDownList = row.FindControl("ddlAssetsubcat")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", st, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        'If ddl IsNot Nothing Then
        ddl2.DataSource = sp.GetDataSet()
        ddl2.DataTextField = "AST_SUBCAT_NAME"
        ddl2.DataValueField = "AST_SUBCAT_CODE"
        ddl2.DataBind()
        ddl2.Items.Insert(0, "--Select--")
    End Sub
    Protected Sub ddlAssetsubcat_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(ddl.Parent.Parent, GridViewRow)
        Dim ddl1 As DropDownList = row.FindControl("ddlAssetCat")
        Dim idx As Integer = row.RowIndex
        Dim st As String = ddl.SelectedItem.Value
        Dim ddlAssetCat As String = ddl1.SelectedItem.Value
        Dim ddl2 As DropDownList = row.FindControl("ddlAssetCode")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAssetCat, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", st, DbType.String)
        ''If ddl IsNot Nothing Then
        ddl2.DataSource = sp.GetDataSet()
        ddl2.DataTextField = "manufacturer"
        ddl2.DataValueField = "manufactuer_code"
        ddl2.DataBind()
        ddl2.Items.Insert(0, "--Select--")
    End Sub
    Protected Sub ddlAssetCode_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(ddl.Parent.Parent, GridViewRow)
        Dim ddl1 As DropDownList = row.FindControl("ddlAssetCat")
        Dim ddl3 As DropDownList = row.FindControl("ddlAssetsubcat")
        Dim idx As Integer = row.RowIndex
        Dim st As String = ddl.SelectedItem.Value
        Dim ddlAssetCat As String = ddl1.SelectedItem.Value
        Dim ddlastbrd As String = ddl3.SelectedItem.Value
        Dim ddl2 As DropDownList = row.FindControl("ddlproductname")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCat, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlastbrd, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", st, DbType.String)
        ddl2.DataSource = sp.GetDataSet()
        ddl2.DataTextField = "AST_MD_NAME"
        ddl2.DataValueField = "AST_MD_CODE"
        ddl2.DataBind()
        ddl2.Items.Insert(0, "--Select--")
    End Sub
    Protected Sub ddlproductname_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim ddl As DropDownList = CType(sender, DropDownList)
        Dim row As GridViewRow = CType(ddl.Parent.Parent, GridViewRow)
        Dim ddl1 As DropDownList = row.FindControl("ddlAssetCat")
        Dim ddl3 As DropDownList = row.FindControl("ddlAssetsubcat")
        Dim ddl4 As DropDownList = row.FindControl("ddlAssetCode")
        Dim ddl6 As Label = row.FindControl("requistionid")
        Dim ddl5 As String
        Dim idx As Integer = row.RowIndex
        Dim st As String = ddl.SelectedItem.Value
        Dim ddlAssetCat As String = ddl1.SelectedItem.Value
        Dim ddlastbrd As String = ddl3.SelectedItem.Value
        Dim ddlastbrnd As String = ddl4.SelectedItem.Value
        Dim REQID As String = ddl6.Text
        Dim ddl2 As DropDownList = row.FindControl("ddlproductname")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GetCOST_WITHOUTPODetailsBy_ReqId")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCat, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlastbrd, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlastbrnd, DbType.String)
        sp.Command.AddParameter("@AST_MD_MODID", st, DbType.String)
        sp.Command.AddParameter("@REQID", REQID, DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)

        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then

            Dim txtUnitPrice As TextBox = row.FindControl("txtUnitPrice")
            txtUnitPrice.Text = ds.Tables(0).Rows(0)("AIPD_RATE").ToString()
            'txtUnitPrice = ds.Tables(0).Rows(0).Item("AIPD_RATE")

        End If


    End Sub
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If Not Page.IsValid Then
            Exit Sub
        End If
        Dim PONumber As String = GetRequestId()
        UpdatePOMaster(PONumber)
        UpdatePOMasterDetails(PONumber)
        'UpdateData1(PONumber, 2)
        'Response.Redirect("frmAssetThanks.aspx?PO=" + PONumber)
        Dim Advance As Integer = 0
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        'Dim PONumber As String = GetRequestId()
        'UpdateData(PONumber, 2, txtAdvance.Text)
        send_mail_Finalize_PO(PONumber)
        Response.Redirect("frmEditPODetails.aspx?RID=outwardapp&MReqId=" + Request.QueryString("REQ_ID"))
        'Response.Redirect("frmEditPODetails.aspx")
    End Sub

    Public Sub send_mail_Finalize_PO(ByVal POnum As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_UPDATED")
        sp.Command.AddParameter("@REQ_ID", POnum, DbType.String)
        sp.Execute()
    End Sub


    Private Sub UpdatePOMaster(ByVal PO As String)
        Dim FFIP As Double = 0
        Dim Advance As Double = 0
        Dim cst As Double = 0
        Dim OnDelivery As Double = 0
        Dim Wst As Double = 0
        Dim CGST As Double = 0
        Dim IGST As Double = 0
        Dim SGST As Double = 0
        Dim Installation As Double = 0
        Dim Octrai As Double = 0
        Dim Commissioning As Double = 0
        Dim ServiceTax As Double = 0
        Dim Retention As Double = 0
        Dim Others As Double = 0
        Dim Payments As Double = 0
        Dim TotalCost As Double = 0

        'If Double.TryParse(txtFFIP.Text, FFIP) Then
        'Else
        '    lblMsg.Text = "Invalid FFIP"
        '    Exit Sub
        'End If
        'If Double.TryParse(txtCst.Text, cst) Then
        'Else
        '    lblMsg.Text = "Invalid CST/ST"
        '    Exit Sub
        'End If
        'If Double.TryParse(txtOctrai.Text, Octrai) Then
        'Else
        '    lblMsg.Text = "Invalid Octroi"
        '    Exit Sub
        'End If
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        If Double.TryParse(txtDelivery.Text, OnDelivery) Then
        Else
            lblMsg.Text = "Invalid On Delivery"
            Exit Sub
        End If
        'If Double.TryParse(txtWst.Text, Wst) Then
        'Else
        '    lblMsg.Text = "Invalid Wst"
        '    Exit Sub
        'End If
        If Double.TryParse(txtInstallation.Text, Installation) Then
        Else
            lblMsg.Text = "Invalid Installation"
            Exit Sub
        End If
        If Double.TryParse(txtCommissioning.Text, Commissioning) Then
        Else
            lblMsg.Text = "Invalid Commissioning"
            Exit Sub
        End If
        'If Double.TryParse(txtServiceTax.Text, ServiceTax) Then
        'Else
        '    lblMsg.Text = "Invalid ServiceTax"
        '    Exit Sub
        'End If
        If Double.TryParse(txtRetention.Text, Retention) Then
        Else
            lblMsg.Text = "Invalid Retention"
            Exit Sub
        End If
        'If Double.TryParse(txtOthers.Text, Others) Then
        'Else
        '    lblMsg.Text = "Invalid Others"
        '    Exit Sub
        'End If
        If Double.TryParse(txtPayments.Text, Payments) Then
        Else
            lblMsg.Text = "Invalid Payments"
            Exit Sub
        End If
        If Double.TryParse(txtigst.Text, IGST) Then
        Else
            lblMsg.Text = "Invalid IGST"
            Exit Sub
        End If
        If Double.TryParse(txtsgst.Text, SGST) Then
        Else
            lblMsg.Text = "Invalid SGST"
            Exit Sub
        End If
        If Double.TryParse(txtCst.Text, CGST) Then
        Else
            lblMsg.Text = "Invalid CGST"
            Exit Sub
        End If
        Dim ExchangeCharges As Double = 0
        If Double.TryParse(txtExchange.Text, ExchangeCharges) Then
        Else
            lblMsg.Text = "Invalid Exchange Charges"
            Exit Sub
        End If
        Dim VenCode As String = ddlVendor.DataValueField

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_UPDATE_DETAILS")
        sp.Command.AddParameter("@AIP_PO_ID", PO, DbType.String)
        sp.Command.AddParameter("@StatusId", 4, DbType.Int32)
        'sp.Command.AddParameter("@AIP_AUR_ID", txtRequesterId.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_PO_REMARKS", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@AIP_APP_AUTH", Session("UID"), DbType.String)
        'sp.Command.AddParameter("@AIP_APP_DATE", txtDOD.Text, DbType.DateTime)
        sp.Command.AddParameter("@AIP_APP_REM", txtRemarks.Text, DbType.String)
        'sp.Command.AddParameter("@AIP_DEPT", txtDeptId.Text, DbType.String)
        sp.Command.AddParameter("@AIP_ITM_TYPE", "", DbType.String)
        'sp.Command.AddParameter("@AIP_LOCATION", txtLocation.Text, DbType.String)
        sp.Command.AddParameter("@AIP_PO_TYPE", "", DbType.String)
        'sp.Command.AddParameter("@AIPD_VENDORName", ddlVendor.SelectedValue, DbType.String)
        Dim a As String = Double.Parse(Trim(txtTotalCost.Text))
        sp.Command.AddParameter("@AIPD_TOTALCOST", Double.Parse(Trim(txtTotalCost.Text)), DbType.Double)
        sp.Command.AddParameter("@AIP_PO_Cst", CGST, DbType.Double)
        sp.Command.AddParameter("@AIP_PO_sgst", SGST, DbType.Double)
        sp.Command.AddParameter("@AIP_PO_igst", IGST, DbType.Double)
        'sp.Command.AddParameter("@AIPD_PLI", Advance, DbType.String)
        'sp.Command.AddParameter("@AIPD_TAX_FFIP", FFIP, DbType.Double)
        'sp.Command.AddParameter("@AIPD_TAX_CST", cst, DbType.Double)
        'sp.Command.AddParameter("@AIPD_TAX_WST", Wst, DbType.Double)
        'sp.Command.AddParameter("@AIPD_TAX_OCTRAI", Octrai, DbType.Double)
        'sp.Command.AddParameter("@AIPD_TAX_SERVICETAX", ServiceTax, DbType.Double)
        'sp.Command.AddParameter("@AIPD_TAX_OTHERS", Others, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_ADVANCE", Advance, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_ONDELIVERY", OnDelivery, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_INSTALLATION", Installation, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_COMMISSION", Commissioning, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_RETENTION", Retention, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_OTHERS", Payments, DbType.Double)
        sp.Command.AddParameter("@AIPD_EXPECTED_DATEOF_DELIVERY", txtDOD.Text, DbType.String)
        sp.Command.AddParameter("@AIPD_EXCHANGE_CHARGES", ExchangeCharges, DbType.Double)
        sp.Command.AddParameter("@AVR_Code", ddlVendor.SelectedValue, DbType.String)
        sp.Command.AddParameter("@aip_state", ddlState.SelectedValue, DbType.String)
        sp.Command.AddParameter("@AIP_STATE_GST", txtSGSTN.Text, DbType.String)
        sp.Command.AddParameter("@AIP_STATE_ADDR", txtSAddress.Text, DbType.String)
        sp.Command.AddParameter("@aip_conditions", txtTerms.Text, DbType.String)
        sp.Command.AddParameter("@AIPD_QREF", txtQref.Text, DbType.String)
        sp.Command.AddParameter("@AIPD_QDATE", txtQdate.Text, DbType.String)
        sp.Command.AddParameter("@AIP_APP_UPREM", txtupremarks.Text, DbType.String)
        'sp.Command.AddParameter("@PoDetailSno", txtPodetailsId.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub UpdatePOMasterDetails(ByVal PO As String)
        For Each row As GridViewRow In gvItems.Rows
            Dim CHECKED As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim txtLCM As TextBox = DirectCast(row.FindControl("lcmname"), TextBox)
            Dim podetailsid As Label = DirectCast(row.FindControl("podetailsid"), Label)
            Dim reqid As Label = DirectCast(row.FindControl("requistionid"), Label)
            Dim lblProductId As DropDownList = DirectCast(row.FindControl("ddlproductname"), DropDownList)
            Dim lblAssetCat As DropDownList = DirectCast(row.FindControl("ddlAssetCat"), DropDownList)
            Dim lblAssetsubcat As DropDownList = DirectCast(row.FindControl("ddlAssetsubcat"), DropDownList)
            Dim lblAssetCode As DropDownList = DirectCast(row.FindControl("ddlAssetCode"), DropDownList)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim txtUnitPrice As TextBox = DirectCast(row.FindControl("txtUnitPrice"), TextBox)
            Dim unitprice As Double = 0
            Dim PurchaseQty As Integer = 0
            Double.TryParse(Trim(txtUnitPrice.Text), unitprice)
            Integer.TryParse(Trim(txtPurchaseQty.Text), PurchaseQty)
            If lblAssetsubcat.SelectedValue = "--Select--" Then
                lblMsg.Visible = True
                lblMsg.Text = "Please Select Subcategory"
            End If


            If lblAssetCode.SelectedValue = "--Select--" Then
                lblMsg.Visible = True
                lblMsg.Text = "Please Select Brand"
            End If

            If lblProductId.SelectedValue = "--Select--" Then
                lblMsg.Visible = True
                lblMsg.Text = "Please Select Model"
            End If

            If CHECKED.Checked Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEMPO_DETAILS_Update")
                sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
                sp.Command.AddParameter("@AIPD_PO_ID", PO, DbType.String)
                sp.Command.AddParameter("@AIPD_AST_CODE", lblProductId.Text, DbType.String)
                sp.Command.AddParameter("@AIR_LOC", txtLCM.Text, DbType.String)
                sp.Command.AddParameter("@AIR_ITEM_CODE", lblAssetCat.Text, DbType.String)
                sp.Command.AddParameter("@AIR_ITEM_SUBCAT", lblAssetsubcat.Text, DbType.String)
                sp.Command.AddParameter("@AIR_ITEM_BRD", lblAssetCode.Text, DbType.String)
                sp.Command.AddParameter("@AIR_PO_Sno", podetailsid.Text, DbType.String)
                sp.Command.AddParameter("@AIR_Req_Sno", reqid.Text, DbType.String)
                sp.Command.AddParameter("@AIPD_QTY", CInt(txtQty.Text), DbType.Int32)
                sp.Command.AddParameter("@AIPD_ITMREQ_ID", Request("RID"), DbType.String)
                sp.Command.AddParameter("@AIPD_ACT_QTY", PurchaseQty, DbType.Int32)
                sp.Command.AddParameter("@AIPD_RATE", unitprice, DbType.Double)
                sp.ExecuteScalar()
            End If

        Next
    End Sub

    Protected Sub btnTotalCost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTotalCost.Click
        Dim ItemSubTotal As Double = 0
        Dim FFIP As Double = 0
        Dim Advance As Double = 0
        Dim CGST As Double = 0
        Dim OnDelivery As Double = 0
        Dim IGST As Double = 0
        Dim Installation As Double = 0
        Dim Octrai As Double = 0
        Dim Commissioning As Double = 0
        Dim ServiceTax As Double = 0
        Dim Retention As Double = 0
        Dim SGST As Double = 0
        Dim Payments As Double = 0
        Dim TotalCost As Double = 0

        For Each row As GridViewRow In gvItems.Rows
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim txtUnitPrice As TextBox = DirectCast(row.FindControl("txtUnitPrice"), TextBox)
            Dim unitprice As Double = 0
            Dim PurchaseQty As Double = 0
            Decimal.TryParse(Trim(txtUnitPrice.Text), unitprice)
            Decimal.TryParse(Trim(txtPurchaseQty.Text), PurchaseQty)
            ItemSubTotal += (PurchaseQty * unitprice)
        Next

        'If Double.TryParse(txtFFIP.Text, FFIP) Then
        'Else
        '    lblMsg.Text = "Invalid FFIP"
        '    Exit Sub
        'End If
        'If Double.TryParse(txtCst.Text, cst) Then
        'Else
        '    lblMsg.Text = "Invalid CST/ST"
        '    Exit Sub
        'End If
        'If Double.TryParse(txtOctrai.Text, Octrai) Then
        'Else
        '    lblMsg.Text = "Invalid Octroi"
        '    Exit Sub
        'End If
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        If Double.TryParse(txtDelivery.Text, OnDelivery) Then
        Else
            lblMsg.Text = "Invalid On Delivery"
            Exit Sub
        End If
        If Double.TryParse(txtigst.Text, IGST) Then
        Else
            lblMsg.Text = "Invalid IGST"
            Exit Sub
        End If
        If Double.TryParse(txtInstallation.Text, Installation) Then
        Else
            lblMsg.Text = "Invalid Installation"
            Exit Sub
        End If
        If Double.TryParse(txtCommissioning.Text, Commissioning) Then
        Else
            lblMsg.Text = "Invalid Commissioning"
            Exit Sub
        End If
        If Double.TryParse(txtCst.Text, CGST) Then
        Else
            lblMsg.Text = "Invalid CGST"
            Exit Sub
        End If
        If Double.TryParse(txtRetention.Text, Retention) Then
        Else
            lblMsg.Text = "Invalid Retention"
            Exit Sub
        End If
        If Double.TryParse(txtsgst.Text, SGST) Then
        Else
            lblMsg.Text = "Invalid SGST"
            Exit Sub
        End If
        If Double.TryParse(txtPayments.Text, Payments) Then
        Else
            lblMsg.Text = "Invalid Payments"
            Exit Sub
        End If

        TotalCost = ItemSubTotal + Payments + Retention + Commissioning + Installation + OnDelivery + Advance + IGST + SGST + CGST
        txtTotalCost.Text = FormatNumber(TotalCost, 2)
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmEditPODetails.aspx")
    End Sub

End Class
