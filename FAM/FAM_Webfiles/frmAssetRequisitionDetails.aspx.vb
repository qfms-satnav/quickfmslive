Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions
Partial Class FAM_FAM_Webfiles_frmAssetRequisitionDetails
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim CatId As String
    Dim asstsubcat As String
    Dim asstbrand As String
    Dim astmodel As String
    Dim ReqId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Page)
        If scriptManager IsNot Nothing Then

            scriptManager.RegisterPostBackControl(grdDocs)

        End If
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
            ' RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        Else
            If Not IsPostBack Then
                Dim UID As String = Session("uid")
                BindUsers()
                getassetcategory()
                ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
                '  ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
                'getassetsubcategory()
                pnlItems.Visible = True
                ' BindRequisition()
                ReqId = Request("RID")
                BindBasicReqDetails()
                BindGrid()
            End If
        End If
    End Sub
    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function
    Private Sub BindRequisition()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AMG_ITEM_REQUISITION_GetByReqId")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                'Dim RaisedBy As Integer = 0
                'Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim RaisedBy As String
                RaisedBy = dr("AIR_AUR_ID")

                BindUsers()

                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)
                Dim CatId As String

                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                '   ddlAstCat.Enabled = False


                getassetsubcategory(CatId)
                '   ddlAstSubCat.Enabled = False
                Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")

                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                Dim asstbrand As String = dr("AIR_ITEM_BRD")
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                '  ddlAstBrand.Enabled = False


                getmakebycatsubcat()

                BindLocation()


                Dim Loc As String = dr("AIR_REQ_LOC")
                ddlLocation.Items.FindByValue(Loc).Selected = True

                txtRemarks.Text = dr("AIR_REMARKS")
                txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                txtStatus.Text = dr("STA_TITLE")
                coordrem.Text = dr("Coordinate")
                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                    tr1.Visible = False
                Else
                    tr1.Visible = True

                End If
                If String.IsNullOrEmpty(Trim(coordrem.Text)) Then
                    tr2.Visible = False
                Else
                    tr2.Visible = True
                End If
                If String.IsNullOrEmpty(Trim(coordrem.Text)) Then
                    tr3.Visible = False
                Else
                    tr3.Visible = True
                End If

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                'If RaisedBy = GetCurrentUser() Then
                If StatusId = 1001 Or StatusId = 1002 Then
                    btnSubmit.Enabled = True
                    btnCancel.Enabled = True
                Else
                    btnSubmit.Enabled = False
                    btnCancel.Enabled = False
                    txtRemarks.Enabled = False
                    gvItems.Enabled = False
                End If
                'Else
                '    btnSubmit.Enabled = False
                '    btnCancel.Enabled = False
                '    txtRemarks.Enabled = False
                '    gvItems.Enabled = False
                'End If

                '  BindGrid()
                For Each row As GridViewRow In gvItems.Rows
                    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
                    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetails_GetDetailsByReqIdAndProductId")
                    sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
                    sp1.Command.AddParameter("@ProductId", CInt(lblProductId.Text), DbType.Int32)
                    Dim dr1 As SqlDataReader = sp1.GetReader
                    If dr1.Read() Then
                        chkSelect.Checked = True
                        'txtQty.Text = dr1("AID_QTY")
                        If txtQty.Text = "NULL" Or txtQty.Text = "" Or txtQty.Text = 0 Then
                            row.Visible = False
                        Else
                            row.Visible = True
                        End If
                    Else
                        row.Visible = False
                    End If
                    '
                Next
            Else
                lblMsg.Text = "No such requisition found."
            End If
        End If
    End Sub

    Private Sub BindBasicReqDetails()

        If String.IsNullOrEmpty(ReqId) Then
            lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AMG_ITEM_REQUISITION_GetByReqId_NP")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then
                lblReqId.Text = ReqId

                'Dim RaisedBy As Integer = 0
                'Integer.TryParse(dr("AIR_AUR_ID"), RaisedBy)

                Dim RaisedBy As String
                RaisedBy = dr("AIR_AUR_ID")

                BindUsers()

                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                'Dim CatId As Integer = 0
                'Integer.TryParse(dr("AIR_ITEM_TYPE"), CatId)
                'Dim CatId As String

                CatId = dr("AIR_ITEM_TYPE")
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = False


                getassetsubcategory(CatId)
                '   ddlAstSubCat.Enabled = False
                'Dim asstsubcat As String = dr("AIR_ITEM_SUBCAT")
                asstsubcat = dr("AIR_ITEM_SUBCAT")

                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                'Dim asstbrand As String = dr("AIR_ITEM_BRD")
                asstbrand = dr("AIR_ITEM_BRD")
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                '  ddlAstBrand.Enabled = False


                getmakebycatsubcat()

                BindLocation1()
                BindDocuments(ReqId)

                Dim Loc As String = dr("AIR_REQ_LOC")
                ddlLocation.Items.FindByValue(Loc).Selected = True

                Dim StatusId As Integer = 0
                Integer.TryParse(dr("AIR_STA_ID"), StatusId)

                astmodel = dr("AIR_ITEM_MOD")
                ddlAstModel.Items.FindByValue(astmodel).Selected = True


                'txtRemarks.Text = dr("AIR_REMARKS")
                'txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                'txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")
                'proc_usr.Text = rm + rmname
                'txtCoordinateCheckRemarks.Text = dr("AIR_APRCOORD_REM")

                txtRemarks.Text = dr("AIR_REMARKS")

                If StatusId = 1005 Then
                    txtRMRemarks.Text = dr("AIR_RM_REMARKS")
                End If
                If StatusId <> 1005 Then
                    txtRMRemarks.Text = dr("AIR_APR1_REM")
                End If

                txtAdminRemarks.Text = dr("AIR_ADM_REMARKS")

                txtStatus.Text = dr("STA_TITLE")

                coordrem.Text = dr("AIR_APRCOORD_REM")

                If String.IsNullOrEmpty(Trim(txtRMRemarks.Text)) Then
                        tr1.Visible = False
                    Else
                        tr1.Visible = True

                    End If
                    If String.IsNullOrEmpty(Trim(txtAdminRemarks.Text)) Then
                        tr2.Visible = False
                    Else
                        tr2.Visible = True
                    End If
                If String.IsNullOrEmpty(Trim(coordrem.Text)) Then
                    tr3.Visible = False
                Else
                    tr3.Visible = True
                End If


                'If RaisedBy = GetCurrentUser() Then
                If StatusId = 1001 Or StatusId = 1002 Or StatusId = 1005 Then
                        btnSubmit.Enabled = True
                        btnCancel.Enabled = True
                    Else
                        btnSubmit.Enabled = False
                        btnCancel.Enabled = False
                        txtRemarks.Enabled = False
                        gvItems.Enabled = False
                    End If
                End If
            End If
    End Sub

    Public Sub BindDocuments(ByVal ReqId As String)
        Dim dtDocs As New DataTable("Documents")
        Dim param(0) As SqlParameter
        'param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@AMG_REQID", SqlDbType.NVarChar, 50)
        param(0).Value = ReqId
        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("AST_Level1_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            lblDocsMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub


    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim ID = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & filePath
            Response.ContentType = "application/octet-stream"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "UploadFiles\", "") & """")
            Response.WriteFile(filePath)
            'Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub

    Private Sub grdDocs_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.DeleteCommand
        If grdDocs.EditItemIndex = -1 Then
            Dim Bid As Integer
            Bid = grdDocs.DataKeys(e.Item.ItemIndex)
            'Param = New SqlParameter(0) {}
            Dim param(0) As SqlParameter
            param(0) = New SqlParameter("@ID", SqlDbType.NVarChar, 50)
            param(0).Value = Bid
            ObjSubsonic.GetSubSonicDataSet("AST_VIEW_DELETE_DOCS", param)
            ReqId = Request("RID")
            BindDocuments(ReqId)
        End If
    End Sub
    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--Select--")
        'ddlAstBrand.Items.Insert(0, New ListItem("No Brand", "No Brand"))
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    'Private Sub BindGrid()
    '    'Dim CatId As Integer = 0
    '    'Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
    '    'gvItems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
    '    'gvItems.DataBind()




    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@ReqId", SqlDbType.NVarChar, 200)
    '    param(0).Value = Request.QueryString("RID")
    '    'ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_REQUISITION", param)


    '    ObjSubsonic.BindGridView(gvItems, "GET_AMGITEM_VIEW_CAPITAL_ASSET_REQUISITION", param)

    '    pnlItems.Visible = True
    'End Sub

    Dim dsCIRGrid As New DataSet()
    Private Sub BindGrid()
        'Dim tickedcount = 0
        Dim ReqId As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMGITEM_VIEW_CAPITAL_ASSET_REQUISITION")
        sp.Command.AddParameter("@AST_MD_CATID", CatId, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", asstsubcat, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", IIf(asstbrand = "No Brand", "ALL", asstbrand), DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", IIf(astmodel = "No Model", "ALL", astmodel), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@REQID", ReqId, DbType.String)
        dsCIRGrid = sp.GetDataSet
        gvItems.DataSource = dsCIRGrid

        gvItems.DataBind()

    End Sub
    Private Sub BindUsers()

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = Session("UID")

        param(1) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(1).Value = Session("COMPANYID")

        ObjSubsonic.Binddropdown(ddlEmp, "AM_AMT_bindUsers_SP", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(Session("UID"))
        If Not li Is Nothing Then
            li.Selected = True
        End If
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"AMT_GetUserDtls_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        'ddlEmp.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS_VIEWSTK")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        ' ddlAstCat.SelectedIndex = If(ddlAstCat.Items.Count > 1, 0, 0)
    End Sub


    'Private Sub GetChildRows(ByVal i As String)
    '    Dim str As String = ""
    '    Dim id
    '    If i = "0" Then
    '        id = CType(i, Integer)
    '    Else
    '        Dim id1 As Array = Split(i, "~")
    '        str = id1(0).ToString & "  --"
    '        id = CType(id1(1), Integer)
    '    End If
    '    Dim objConn As New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString)
    '    Dim da As New SqlDataAdapter("select CategoryId,CategoryName,ParentId FROM CSK_Store_Category WHERE ParentId = " & id, objConn)
    '    Dim ds As New DataSet
    '    da.Fill(ds)
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        For Each dr As DataRow In ds.Tables(0).Rows
    '            Dim j As Integer = CType(dr("CategoryId"), Integer)
    '            If id = 0 Then
    '                str = ""
    '            End If
    '            Dim li As ListItem = New ListItem(str & dr("CategoryName").ToString, dr("CategoryId"))
    '            ddlAstCat.Items.Add(li)
    '            GetChildRows(str & "~" & j)
    '        Next
    '    End If
    'End Sub
    'Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
    '    If ddlAstCat.SelectedIndex > 0 Then
    '        Dim CatId As Integer = 0
    '        Integer.TryParse(ddlAstCat.SelectedItem.Value, CatId)
    '        gvItems.DataSource = ProductController.GetByCategoryID_DataSet(CatId)
    '        gvItems.DataBind()
    '        pnlItems.Visible = True
    '    End If
    'End Sub
    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'Dim ReqId As String = GetRequestId()
        ''UpdateData(ReqId, Trim(txtRemarks.Text))
        'UpdateData(ReqId, Trim(txtRemarks.Text), ddlAstCat.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value, ddlAstBrand.SelectedItem.Value, ddlAstModel.SelectedItem.Value)
        '' DeleteRequistionItems(ReqId)
        'Dim VT_Code As String
        'Dim Ast_SubCat_Code As String
        'Dim manufactuer_code As String
        'Dim Ast_Md_code As String

        'For Each row As GridViewRow In gvItems.Rows
        '    Dim count As Integer = 0
        '    Dim chkSelect As CheckBox = DirectCast(row.FindControl("chk"), CheckBox)
        '    Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
        '    Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)



        '    Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
        '    Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
        '    Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
        '    Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)

        '    VT_Code = lbl_vt_code.Text
        '    Ast_SubCat_Code = lbl_ast_subcat_code.Text
        '    manufactuer_code = lbl_manufactuer_code.Text
        '    Ast_Md_code = lbl_ast_md_code.Text

        '    If chkSelect.Checked Then
        '        If String.IsNullOrEmpty(txtQty.Text) Then
        '            lblMsg.Text = "Please enter Quantity for Selected Checkbox"
        '            Exit Sub
        '        ElseIf IsNumeric(txtQty.Text) > 0 Then
        '            count = count + 1
        '            If count = 0 Then
        '                lblMsg.Text = "Sorry! Request Has not been Raised You haven't selected any Products and make Quantity more than Zero"
        '                Exit Sub
        '            End If
        '            'Else
        '            '    lblMsg.Text = "Please enter Quantity in Numerics Only"
        '        End If

        '        InsertDetails(ReqId, CInt(lblProductId.Text), CInt(Trim(txtQty.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code)
        '    Else
        '        lblMsg.Text = "Sorry! Request Has not been Raised You haven't selected any Products and make Quantity more than Zero"
        '        Exit Sub
        '    End If

        'Next
        Validate(Request.QueryString("RID"))
       
    End Sub
    Private Sub UpdateData(ByVal ReqId As String, ByVal Remarks As String, ByVal astcat As String, ByVal subcat As String, ByVal brand As String, ByVal model As String, ByVal Company_Id As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AssetRequisition_UpdateByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@StatusId", 1002, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@model", model, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Company_Id, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub InsertDetails(ByVal ReqId As String, ByVal ProductId As String, ByVal Qty As Integer, ByVal EstCost As Integer, ByVal VT_CODE As String,
        ByVal AST_SUBCAT_CODE As String, ByVal manufactuer_code As String, ByVal AST_MD_CODE As String, ByVal Company_Id As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AssetRequisitionDetails_update")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@Qty", Qty, DbType.Int32)
        sp.Command.AddParameter("@EST_COST", EstCost, DbType.Int32)
        sp.Command.AddParameter("@ITEM_TYPE", VT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_SUBCAT", AST_SUBCAT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_BRD", manufactuer_code, DbType.String)
        sp.Command.AddParameter("@ITEM_MOD", AST_MD_CODE, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Company_Id, DbType.String)

        sp.ExecuteScalar()
    End Sub

    Private Sub Mail_modify(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_Mail_Modify")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@StatusId", 1002, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        CancelData(GetRequestId, Trim(txtRemarks.Text))
        Mail_cancel(GetRequestId, Trim(txtRemarks.Text))
        Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId())
    End Sub
    Private Sub CancelData(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AssetRequisition_CancelByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@StatusId", 1003, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@model", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), Data.DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub Mail_cancel(ByVal ReqId As String, ByVal Remarks As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_Mail_Cancel")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp.Command.AddParameter("@Remarks", Remarks, DbType.String)
        sp.Command.AddParameter("@CatId", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@StatusId", 1003, DbType.Int32)
        sp.Command.AddParameter("@AIR_ITEM_SUBCAT", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AIR_ITEM_BRD", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Sub DeleteRequistionItems(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_AssetRequisitionDetails_DeleteByReqId")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("frmViewRequisition.aspx")
    End Sub
    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 0, 0)
    End Sub


    Private Sub BindLocation1()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        'ddlAstModel.Items.Insert(0, New ListItem("No Model", "No Model"))
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        'ddlAstModel.Items.Insert(0, "--All--")
    End Sub


    Private Sub BindGridOnSearchClick()
        Dim ReqId As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AMGITEM_VIEW_CAPITAL_ASSET_REQUISITION")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_MODEL_ID", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@REQID", ReqId, DbType.String)
        gvItems.DataSource = sp.GetDataSet
        gvItems.DataBind()
        If gvItems.Rows.Count > 0 Then
            pnlItems.Visible = True
            remarksAndActionButtons.Visible = True
        End If

    End Sub

   

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        BindGridOnSearchClick()

    End Sub

    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
        pnlItems.Visible = False
        remarksAndActionButtons.Visible = False
        getassetsubcategory()
    End Sub
    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        pnlItems.Visible = False
        getbrandbycatsubcat()
        remarksAndActionButtons.Visible = False
    End Sub
    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        pnlItems.Visible = False
        getmakebycatsubcat()
        remarksAndActionButtons.Visible = False
    End Sub

    Private Sub getbrandbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        pnlItems.Visible = False
    End Sub

    Private Sub Validate(ByVal ReqId As String)

        If Not Page.IsValid Then
            Exit Sub
        End If

        Dim count As Integer = 0
        Dim Message As String = String.Empty

        Dim VT_Code As String
        Dim Ast_SubCat_Code As String
        Dim manufactuer_code As String
        Dim Ast_Md_code As String
        Dim Company_Id As Integer = Session("COMPANYID")

        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtEstCost As TextBox = DirectCast(row.FindControl("txtEstCost"), TextBox)

            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtQty.Text) Then
                    lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                    Exit Sub
                ElseIf IsNumeric(txtQty.Text) = True Then
                    count = count + 1
                    If count = 0 Then
                        lblMsg.Text = "Sorry! Request Has not been Raised You haven't selected any Products and make Quantity more than Zero"
                        Exit Sub
                    End If
                Else
                    lblMsg.Text = "Please enter Quantity in Numerics Only"
                    Exit Sub
                End If
            End If

            If chkSelect.Checked Then
                If String.IsNullOrEmpty(txtEstCost.Text) Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Please enter Estimated Cost for Selected Checkbox, if it is not required keep zero"
                    Exit Sub
                ElseIf IsNumeric(txtEstCost.Text) = False Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Please enter Estimated Cost in numbers..."
                    Exit Sub
                End If
            End If

        Next
        If count > 0 Then
            UpdateData(ReqId, Trim(txtRemarks.Text), ddlAstCat.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value, ddlAstBrand.SelectedItem.Value, ddlAstModel.SelectedItem.Value, Company_Id)
            For Each row As GridViewRow In gvItems.Rows

                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
                Dim txtEstCost As TextBox = DirectCast(row.FindControl("txtEstCost"), TextBox)
                Dim AST_MD_NAME As Label = DirectCast(row.FindControl("AST_MD_NAME"), Label)

                Dim lblMinOrdQty As Label = DirectCast(row.FindControl("lblMinOrdQty"), Label)


                Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
                Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
                Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
                Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)

                VT_Code = lbl_vt_code.Text
                Ast_SubCat_Code = lbl_ast_subcat_code.Text
                manufactuer_code = lbl_manufactuer_code.Text
                Ast_Md_code = lbl_ast_md_code.Text

                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtQty.Text) Then
                        lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                        Exit Sub
                    ElseIf IsNumeric(txtQty.Text) = True Then

                        InsertDetails(ReqId, lblProductid.Text, CInt(Trim(txtQty.Text)), CInt(Trim(txtEstCost.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code, Company_Id)

                    End If



                End If
            Next
            Mail_modify(ReqId, Trim(txtRemarks.Text))
            Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)

        ElseIf count = 0 Then
            lblMsg.Text = "Sorry! Request Has not been Raised You haven't selected any Products and make Quantity more than Zero"
        Else
            lblMsg.Text = "Please enter Quantity for Selected Checkbox"
        End If
    End Sub
    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()
        pnlItems.Visible = False
        remarksAndActionButtons.Visible = False
        getassetsubcategory()
        ddlAstBrand.Items.Clear()
        ddlAstModel.Items.Clear()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
End Class
