﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditPoDetails.aspx.vb" Inherits="FAM_FAM_Webfiles_EditPoDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>


    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                startDate: new Date(),
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }
        function ChildClick(CheckBox) {
            //Get target base & child control.
            var TargetBaseControl = document.getElementById('<%= Me.gvItems.ClientID%>');
            var TargetChildControl = "chkSelect1";
            //Get all the control of the type INPUT in the base control.
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            // check to see if all other checkboxes are checked
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0) {

                    // Whoops, there is an unchecked checkbox, make sure
                    // that the header checkbox is unchecked
                    if (!Inputs[n].checked) {
                        Inputs[0].checked = false;
                        return;
                    }
                }
            // If we reach here, ALL GridView checkboxes are checked
            Inputs[0].checked = true;

        }
        function validateCheckBoxesMyReq() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");

            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {

                    return delrecord();

                }
            }
            alert("Please select atleast one checkbox");
            return false;

        }
     <%-- function reqid() {
            var gridView = document.getElementById("<%=gvItems.ClientID%>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    var row = checkBoxes[i].parentNode.parentNode;
                    message = row.cells[2].innerHTML;
                   
                    return alert(message);

                }
            }
        }--%>
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Finalize PO" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Edit PO</h3>
            </div>
            <div class="card">
                <%-- <div class="panel-body" style="padding-right: 50px;">--%>
                <form id="form1" runat="server">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                    <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div id="pnlItems" runat="server">
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <div style="margin-top: 10px; overflow-x: auto; width: auto;">
                                            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                EmptyDataText="No Asset(s) Found." CssClass="table GridStyle" GridLines="none">
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="left">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="chkAll" runat="server" onclick="javascript:CheckAllDataGridCheckBoxes('chkSelect1', this.checked);"
                                                                ToolTip="Click to check all" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect1" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);"></asp:CheckBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="AIPD_ITMREQ_ID" HeaderText="Requisition Id" ItemStyle-HorizontalAlign="left" />
                                                    <%--<asp:BoundField DataField="sku" HeaderText="Requisition Serialno." ItemStyle-HorizontalAlign="left" />--%>
                                                    <%--<asp:BoundField DataField="LCM_NAME" HeaderText="Location" ItemStyle-HorizontalAlign="left" />--%>
                                                    <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="lcmname" runat="server" Style="width: auto" Text='<%#Eval("LCM_NAME") %>'
                                                                ReadOnly="false" CssClass="form-control"></asp:TextBox>
                                                            <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="server" TargetControlID="lcmname" MinimumPrefixLength="2" EnableCaching="false"
                                                                CompletionSetCount="10" CompletionInterval="10" ServiceMethod="GetLocations" ServicePath="~/Autocompletetype.asmx">
                                                            </asp:AutoCompleteExtender>
                                                            <%--<asp:Label ID="lblProductId" runat="server" Text='<%#Eval("ProductId") %>' Visible="false"></asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Category" ItemStyle-HorizontalAlign="left" Visible="true">
                                                        <ItemTemplate>
                                                            <%--<asp:UpdatePanel runat="server" ID="UpdatePanel8" UpdateMode="Conditional">
                                                                    <ContentTemplate>--%>
                                                            <asp:DropDownList ID="ddlAssetCat" runat="server" ReadOnly="false" CssClass="selectpicker" data-live-search="true" AutoPostBack="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlAssetCat_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <%--<asp:Label ID="lblproductname" runat="server" Text='<%#Bind("productname") %>' Visible="false"></asp:Label>--%>
                                                            <%--</ContentTemplate>
                                                                </asp:UpdatePanel>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sub Category" ItemStyle-HorizontalAlign="left" Visible="true">
                                                        <ItemTemplate>
                                                            <%-- <asp:UpdatePanel runat="server" ID="UpdatePanel9" UpdateMode="Conditional" ClientIDMode="Static">
                                                                    <ContentTemplate>--%>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAssetsubcat"
                                                                Display="Dynamic" ErrorMessage="Please Select Sub Category !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlAssetsubcat" runat="server" ReadOnly="false" InitialValue="--Select--" CssClass="selectpicker" ClientIDMode="Static" data-live-search="FALSE" AutoPostBack="true" OnSelectedIndexChanged="ddlAssetsubcat_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <%--<asp:Label ID="lblproductname" runat="server" Text='<%#Bind("productname") %>' Visible="false"></asp:Label>--%>
                                                            <%--</ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="ddlAssetCat" EventName="SelectedIndexChanged" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- <asp:TemplateField HeaderText="Location" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:DropDownList ID="ddlAssetloc" runat="server" ReadOnly="false" CssClass="form-control">
                                                            </asp:DropDownList>
                                                            <%--<asp:Label ID="lblproductname" runat="server" Text='<%#Bind("productname") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    <asp:TemplateField HeaderText="Brand" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <%--<asp:UpdatePanel runat="server" ID="UpdatePanel7" UpdateMode="Conditional" ChildrenAsTriggers="False">
                                                                    <ContentTemplate>--%>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlAssetCode"
                                                                Display="Dynamic" ErrorMessage="Please Select Brand !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlAssetCode" runat="server" ReadOnly="false" InitialValue="--Select--" CssClass="selectpicker" ClientIDMode="Static" data-live-search="FALSE" AutoPostBack="true" OnSelectedIndexChanged="ddlAssetCode_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <%--<asp:Label ID="lblproductname" runat="server" Text='<%#Bind("productname") %>' Visible="false"></asp:Label>--%>
                                                            <%--                                                                    </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="ddlAssetCat" EventName="SelectedIndexChanged" />
                                                                        <asp:AsyncPostBackTrigger ControlID="ddlAssetsubcat" EventName="SelectedIndexChanged" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Model Name" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <%-- <asp:UpdatePanel runat="server" ID="UpdatePanel5" UpdateMode="Conditional">
                                                                    <ContentTemplate>--%>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlproductname"
                                                                Display="Dynamic" ErrorMessage="Please Select Model !" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlproductname" runat="server" ReadOnly="false" InitialValue="--Select--" CssClass="selectpicker" ClientIDMode="Static" data-live-search="FALSE" AutoPostBack="true" OnSelectedIndexChanged="ddlproductname_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <%-- </ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:AsyncPostBackTrigger ControlID="ddlproductname" EventName="SelectedIndexChanged" />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                            <%--<asp:Label ID="lblproductname" runat="server" Text='<%#Bind("productname") %>' Visible="false"></asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="requistionid" ItemStyle-HorizontalAlign="left" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="podetailsid" runat="server" Text='<%#Bind("poid") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="requistionid" runat="server" Text='<%#Bind("reqid") %>' Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Quantity" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtQty" runat="server" MaxLength="10" Text='<%#Eval("AIPD_QTY") %>'
                                                                ReadOnly="FALSE" CssClass="form-control"></asp:TextBox>
                                                            <%--<asp:Label ID="lblProductId" runat="server" Text='<%#Eval("ProductId") %>' Visible="false"></asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Per Unit Price" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:UpdatePanel runat="server" ID="UpdatePanel6" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:TextBox ID="txtUnitPrice" runat="server" CssClass="form-control" MaxLength="10" Style="width: auto" Text='<%#formatnumber(Eval("AIPD_RATE"),2) %>'></asp:TextBox>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ddlproductname" EventName="SelectedIndexChanged" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Qty To Purchase" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtPurchaseQty" runat="server" CssClass="form-control" MaxLength="10" Text='<%#Eval("AIPD_ACT_QTY") %>'></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row ">
                                            <%--<div class="col-md-3">
                                                </div>
                                                <label class="col-md-3 control-label"><strong>TAX DETAILS</strong></label>
                                                <div class="col-md-3">
                                                </div>--%>
                                            <div class="col-md-3">
                                                <strong>PAYMENT TERMS</strong>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Advance</label>
                                                <asp:RequiredFieldValidator ID="rfvadvance" runat="server" Display="Dynamic"
                                                    ErrorMessage="Advance" ValidationGroup="Val1" ControlToValidate="txtAdvance" ReadOnly="true"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtAdvance" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">On Delivery</label>
                                                <asp:RequiredFieldValidator ID="rfvdelivery" runat="server" Display="Dynamic" ErrorMessage="On Delivery"
                                                    ValidationGroup="Val1" ControlToValidate="txtDelivery"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtDelivery" runat="server" CssClass="form-control"> </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Installation</label>
                                                <asp:RequiredFieldValidator ID="rfvinstallation" runat="server" Display="Dynamic" ErrorMessage="Installation"
                                                    ValidationGroup="Val1" ControlToValidate="txtInstallation"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtInstallation" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Commissioning</label>
                                                <asp:RequiredFieldValidator ID="rfvcomm" runat="server" Display="Dynamic" ErrorMessage="Commissioning"
                                                    ValidationGroup="Val1" ControlToValidate="txtCommissioning"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtCommissioning" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Retention</label>
                                                <asp:RequiredFieldValidator ID="rfvretention" runat="server" Display="Dynamic" ErrorMessage="Retention"
                                                    ValidationGroup="Val1" ControlToValidate="txtRetention"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtRetention" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">Payments</label>
                                                <asp:RequiredFieldValidator ID="rfvpayments" runat="server" Display="Dynamic" ErrorMessage="Payments"
                                                    ValidationGroup="Val1" ControlToValidate="txtPayments"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtPayments" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="row ">
                                            <div class="col-md-3">
                                                <strong>Tax Details</strong>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">CGST</label>
                                                <asp:RequiredFieldValidator ID="rfvcst" runat="server" Display="Dynamic" ErrorMessage="CGST"
                                                    ValidationGroup="Val1" ControlToValidate="txtCst"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtCst" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">SGST/UTGST</label>
                                                <asp:RequiredFieldValidator ID="rfvsgst" runat="server" Display="Dynamic" ErrorMessage="SGST/UTGST"
                                                    ValidationGroup="Val1" ControlToValidate="txtsgst"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtsgst" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="col-md-5 control-label">IGST</label>
                                                <asp:RequiredFieldValidator ID="rfvigst" runat="server" Display="Dynamic" ErrorMessage="IGST"
                                                    ValidationGroup="Val1" ControlToValidate="txtigst"></asp:RequiredFieldValidator>
                                                <div class="col-md-7">
                                                    <asp:TextBox ID="txtigst" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <%--<div class="row text-right">--%>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <%--  <div class="col-md-12 col-sm-12 col-xs-12">--%>
                                <div class="form-group">
                                    <div class="pull-right">
                                        <asp:Button ID="btnTotalCost" runat="server" Text="Get Total Cost" Visible="true" CssClass="btn btn-primary custom-button-color" />
                                    </div>
                                </div>
                                <%-- </div>--%>
                            </div>
                            <%--</div>--%>
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Total Cost</label>
                                            <asp:RequiredFieldValidator ID="rfvcose" runat="server" Display="Dynamic" ErrorMessage="Total Cost"
                                                ValidationGroup="Val1" ControlToValidate="txtTotalCost"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtTotalCost" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Vendor Name</label>
                                            <div class="col-md-7">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel0" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtPodetailsId" runat="server" ReadOnly="True" Visible="false"></asp:TextBox>
                                                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="selectpicker" data-live-search="true"
                                                            AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <%--<asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control" ReadOnly="True"></asp:TextBox>--%>
                                                <%--<asp:TextBox ID="txtVenCode" runat="server" CssClass="form-control" Visible="false" ReadOnly="True"></asp:TextBox>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Expected Date of Delivery</label>
                                            <%--  <asp:RequiredFieldValidator ID="rfvdate"  runat="server" Display="Dynamic" ErrorMessage="Expected Date"
                                                        ValidationGroup="Val1" ControlToValidate="txtDOD"></asp:RequiredFieldValidator>--%>
                                            <div class="col-md-7">
                                                <%--<ew:CalendarPopup ID="txtDOD" runat="server">
                                                        </ew:CalendarPopup>--%>
                                                <div class='input-group date' id='fromdate'>
                                                    <asp:TextBox ID="txtDOD" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Exchange Charges</label>
                                            <asp:RequiredFieldValidator ID="rfvcharges" runat="server" Display="Dynamic" ErrorMessage="Extra Charges"
                                                ValidationGroup="Val1" ControlToValidate="txtExchange"></asp:RequiredFieldValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtExchange" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Address<span style="color: red;"></span></label>
                                            <div class="col-md-7">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel12" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtAdd" Height="50px" runat="server" ReadOnly="True" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlVendor" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">

                                            <label class="col-md-5 control-label">Contact Number<span style="color: red;"></span></label>
                                            <div class="col-md-7">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtNumber" Height="35px" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlVendor" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <%--<div class="col-md-12 col-sm-6 col-xs-12">--%>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Quotation Reference<span style="color: red;"></span></label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtQref" Height="35px" runat="server" ReadOnly="false" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- </div>--%>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Quotation Date<span style="color: red;"></span></label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtQdate" Height="35px" runat="server" ReadOnly="false" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">State</label>
                                            <div class="col-md-7">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel10" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <%-- <asp:TextBox ID="txtState" Height="35px" runat="server" ReadOnly="True" CssClass="form-control"> </asp:TextBox>--%>
                                                        <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="True" CssClass="form-control selectpicker" data-live-search="true" ToolTip="Select State">
                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">State GSTN</label>
                                            <div class="col-md-7">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtSGSTN" Height="35px" runat="server" ReadOnly="True" CssClass="form-control"> </asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlState" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Billing Address</label>
                                            <div class="col-md-7">
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:TextBox ID="txtSAddress" Height="50px" runat="server" ReadOnly="True" CssClass="form-control" TextMode="multiline"> </asp:TextBox>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlState" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Terms and Conditions</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtTerms" Height="50px" runat="server" ReadOnly="false" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Rejected Remarks</label>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtupremarks" Height="75px" Width="250px" runat="server" ReadOnly="true" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                            <asp:RegularExpressionValidator ID="RegExpRemarks" runat="server" ControlToValidate="txtRemarks"
                                                ErrorMessage="Please Enter Valid Remarks" Display="None"
                                                ValidationGroup="Val1">
                                            </asp:RegularExpressionValidator>
                                            <div class="col-md-7">
                                                <asp:TextBox ID="txtRemarks" Height="75px" Width="250px" runat="server" ReadOnly="false" CssClass="form-control" TextMode="multiline"> </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%-- <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="row">
                                                    <label class="col-md-5 control-label">Remarks</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="txtfinremarks" Height="75px" Width="250px" runat="server" ReadOnly="false" CssClass="form-control" TextMode="multiline"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>--%>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnSubmit" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" Visible="false" />
                                        <input type='button' value='Export' class="btn btn-primary custom-button-color" onclick="showPopWin()" />
                                        <asp:Button ID="btnUpdate" Text="Update" runat="server" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq();" OnClick="btnUpdate_Click" ValidationGroup="Val1" />
                                        <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%--            </div>--%>
    <div class="modal fade" id="myModal" runat="server" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg" style="width:800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title" id="H1">PO Details</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%-- </div>--%>
    <%--  </div>--%>
    <%-- </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript" defer>
        function delrecord() {
            var txt;
            var r = confirm("Are you sure want to submit ?");
            if (r == true) {
                return true;
            } else {
                return false;
            }
        }
        var id = '<%= Session("req")%>';


        function showPopWin() {

            $("#modalcontentframe").attr("src", "/FAM/FAM_Webfiles/ExportPO.aspx?rid=" + id);
            $("#myModal").modal('show');
            return false;
        }
        function refreshSelectpicker() {

            $("#<%=ddlState.ClientID%>").selectpicker();
            $("#<%=ddlVendor.ClientID%>").selectpicker();
            $("#<%=gvItems.ClientID %>").selectpicker();
            $('#<%=gvItems.ClientID %>').find('[id$="ddlAssetCat"]').selectpicker();
            $('#<%=gvItems.ClientID %>').find('[id$="ddlAssetsubcat"]').selectpicker();
            $('#<%=gvItems.ClientID %>').find('[id$="ddlAssetCode"]').selectpicker();
            $('#<%=gvItems.ClientID %>').find('[id$="ddlproductname"]').selectpicker();
        }

        refreshSelectpicker();
    </script>
</body>
</html>

