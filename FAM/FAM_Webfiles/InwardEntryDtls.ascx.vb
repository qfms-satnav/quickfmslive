Imports System.Data
Imports System.Data.SqlClient
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization

Imports clsSubSonicCommonFunctions
Partial Class Controls_InwardEntryDtls
    Inherits System.Web.UI.UserControl
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim strReqId As String
    Dim strstat As String


    Dim FBDG_ID, SBDG_ID, FTower, FFloor, TBDG_ID, TTower, TFloor As String
    Dim receiveAst, strremarks As String
    Dim FromLoc, ToLoc, FromLocCode, ToLocCode, ReceivingPerson As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        If Not IsPostBack Then
            strReqId = Request.QueryString("Req_id")
            strstat = Request.QueryString("stat")
            getDetailsbyReqId(strReqId)
            BindLocations()
            'BindRequestAssets(strReqId)
            BindInterRequestAssets(strReqId)
        End If
    End Sub

    Public Sub getDetailsbyReqId(ByVal strREQ_id As String)

        Dim dr As SqlDataReader
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
        param(0).Value = strREQ_id
        dr = ObjSubsonic.GetSubSonicDataReader("AM_GET_INTER_MVMT_DETLS_BY_REQID", param)
        If dr.Read Then
            'FBDG_ID = dr.Item("SLOC_CODE")
            'SBDG_ID = dr.Item("FLOC_CODE")
            'FTower = dr.Item("MMR_FROMBDG_ID")
            'FFloor = dr.Item("MMR_FROMFLR_ID")
            'TBDG_ID = dr.Item("FLOC_NAME")
            FromLoc = dr.Item("FromLoc")
            ToLoc = dr.Item("ToLoc")
            FromLocCode = dr.Item("FromLocCode")
            ToLocCode = dr.Item("ToLocCode")
            '  receiveAst = dr.Item("MMR_RECVD_BY_name")
            strremarks = dr.Item("MMR_COMMENTS")
            ReceivingPerson = dr.Item("ReceivingPerson")
            txtPersonName.Text = ReceivingPerson
            txtSendingDate.Text = dr.Item("senddate")
        End If
        If dr.IsClosed = False Then
            dr.Close()
        End If
        txtPersonName.Text = ReceivingPerson


    End Sub
    Private Sub BindDestLoactions()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "MN_GET_ALL_LOCATIONS")
            sp.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlDLoc.DataSource = sp.GetDataSet()
            ddlDLoc.DataTextField = "LCM_NAME"
            ddlDLoc.DataValueField = "LCM_CODE"
            ddlDLoc.DataBind()
            ddlDLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindLocation()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LOCTION")
            sp.Command.AddParameter("@dummy", 1, DbType.Int32)
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlSLoc.DataSource = sp.GetDataSet()
            ddlSLoc.DataTextField = "LCM_NAME"
            ddlSLoc.DataValueField = "LCM_CODE"
            ddlSLoc.DataBind()
            ddlSLoc.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindLocations()
        BindLocation()
        BindDestLoactions()
        ddlSLoc.Items.FindByValue(FromLocCode).Selected = True
        ddlDLoc.Items.FindByValue(ToLocCode).Selected = True
    End Sub

    Public Sub BindInterRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "AM_GET_MVMT_ASTS", param)
    End Sub

    Public Sub BindRequestAssets(ByVal strReqId As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 50)
        param(0).Value = strReqId
        ObjSubsonic.BindGridView(gvItems, "GET_MVMT_ASTS", param)
    End Sub


    'Protected Sub ddlSLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSLoc.SelectedIndexChanged
    '    If ddlSLoc.SelectedIndex > 0 Then
    '        Dim LocCode As String = ddlSLoc.SelectedItem.Value
    '        BindTowersByLocation(LocCode, ddlSTower)
    '        getDetailsbyReqId(Request.QueryString("Req_id"))
    '    End If
    'End Sub

    'Private Sub BindTowersByLocation(ByVal LocCode As String, ByRef ddl As DropDownList)
    '    Dim param(0) As SqlParameter
    '    param(0) = New SqlParameter("@LocId", SqlDbType.NVarChar, 200)
    '    param(0).Value = LocCode
    '    ObjSubsonic.Binddropdown(ddl, "USP_TOWER_GETBYLOCATION", "TWR_NAME", "TWR_CODE", param)
    'End Sub

    'Protected Sub ddlDLoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDLoc.SelectedIndexChanged
    '    If ddlDLoc.SelectedIndex > 0 Then
    '        Dim LocCode As String = ddlDLoc.SelectedItem.Value
    '        BindTowersByLocation(LocCode, ddlDTower)
    '    End If
    'End Sub

    'Protected Sub ddlSTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSTower.SelectedIndexChanged
    '    If ddlSTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlSTower.SelectedItem.Value
    '        Dim LocCode As String = ddlSLoc.SelectedItem.Value
    '        BindFloorsByTower(TwrCode, LocCode, ddlSFloor)
    '    End If
    'End Sub
    'Private Sub BindFloorsByTower(ByVal TwrCode As String, ByVal LocCode As String, ByRef ddl As DropDownList)
    '    Dim param(1) As SqlParameter
    '    param(0) = New SqlParameter("@FLR_LOC_ID", DbType.String)
    '    param(0).Value = Loccode
    '    param(1) = New SqlParameter("@FLR_TWR_ID", DbType.String)
    '    param(1).Value = TwrCode
    '    ObjSubsonic.Binddropdown(ddl, "GET_FLOOR_BYLOCTWR", "FLR_NAME", "FLR_CODE", param)
    'End Sub

    'Protected Sub ddlDTower_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDTower.SelectedIndexChanged
    '    If ddlDTower.SelectedIndex > 0 Then
    '        Dim TwrCode As String = ddlDTower.SelectedItem.Value
    '        Dim LocCode As String = ddlDLoc.SelectedItem.Value
    '        BindFloorsByTower(TwrCode, LocCode, ddlDFloor)
    '    End If
    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim strASSET_LIST As New ArrayList
        If Request.QueryString("stat") = "INTRA" Then

            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_INWARDAPPROVED_BY", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_INWARD_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1022
            param(4) = New SqlParameter("@AMN_RECEIVING_DATE", SqlDbType.NVarChar, 200)
            param(4).Value = txtReceiveDate.Text
            ObjSubsonic.GetSubSonicExecute("UPDATE_MVMT_INWARDSTATUS", param)

            Dim param1(0) As SqlParameter
            param1(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param1(0).Value = Request.QueryString("Req_id")
            ObjSubsonic.GetSubSonicExecute("UPDATE_AMT_ASSET_SPACE", param1)

        Else
            Dim param(4) As SqlParameter
            param(0) = New SqlParameter("@MMR_REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = Request.QueryString("Req_id")
            param(1) = New SqlParameter("@MMR_INWARDAPPROVED_BY", SqlDbType.NVarChar, 200)
            param(1).Value = Session("uid")
            param(2) = New SqlParameter("@MMR_INWARD_COMMENTS", SqlDbType.NVarChar, 200)
            param(2).Value = txtRemarks.Text
            param(3) = New SqlParameter("@STATUS", SqlDbType.Int)
            param(3).Value = 1022
            param(4) = New SqlParameter("@AMN_RECEIVING_DATE", SqlDbType.NVarChar, 200)
            param(4).Value = txtReceiveDate.Text
            ObjSubsonic.GetSubSonicExecute("UPDATE_INTER_MVMT_INWARDSTATUS", param)

            Dim param1(0) As SqlParameter
            param1(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param1(0).Value = Request.QueryString("Req_id")
            ObjSubsonic.GetSubSonicExecute("UPDATE_INTER_AMT_ASSET_SPACE", param1)

        End If


        'lblMsg.Text = "Request succesfully approved."
        For i As Integer = 0 To gvItems.Rows.Count - 1
            Dim lblAAS_AAT_CODE As Label = CType(gvItems.Rows(i).FindControl("lblAAS_AAT_CODE"), Label)
            Dim lblAAT_NAME As Label = CType(gvItems.Rows(i).FindControl("lblAAT_NAME"), Label)
            'strASSET_LIST.Insert(i, lblAAS_AAT_CODE.Text & "," & ddlSLoc.SelectedItem.Text & "," & ddlSTower.SelectedItem.Text & "," & ddlSFloor.SelectedItem.Text & "," & ddlDLoc.SelectedItem.Text & "," & ddlDTower.SelectedItem.Text & "," & ddlDFloor.SelectedItem.Text & "," & txtRemarks.Text)
        Next

        'Dim MailTemplateId As Integer
        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_inwardentrydetails"))
        'getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)




        'MailTemplateId = CInt(ConfigurationManager.AppSettings("AssetIntraMovementRequisition_Outwardentrydetails_approve_reject"))
        'getRequestDetails(Trim(Request.QueryString("Req_id")), strASSET_LIST, MailTemplateId, True)


        'send_mail(Request.QueryString("Req_id"))
        SendMail(Request.QueryString("Req_id"), "INWARD")
        DownloadTrnsform()
        Response.Redirect("frmAssetThanks.aspx?RID=inwardapp")

    End Sub

    Public Sub send_mail(ByVal reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_MOVEMENT_REQUEST_INWARD")
        sp.Command.AddParameter("@REQ_ID", reqid, DbType.String)
        sp.Execute()
    End Sub

    Private Sub SendMail(ByVal ReqId As String, ByVal Mode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_SEND_MAIL_ASSET_INTER_MVMT_REQUISITION")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Command.AddParameter("@MODE", Mode, DbType.String)
        sp.Execute()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Response.Redirect("InwardEntry.aspx")
    End Sub
    Private Sub DownloadTrnsform()
        Try
            Dim viewer As ReportViewer = New ReportViewer()
            ''Dim param() As SqlParameter
            Dim ObjSubSonic As New clsSubSonicCommonFunctions

            Dim reqid = Request.QueryString("Req_id")
            Dim reqid1 = Request.QueryString("Req_id")
            Dim sp As SubSonic.StoredProcedure = New SubSonic.StoredProcedure(Session("TENANT") & "." & "[AM_ASSET_TRANSFOR_REQUISITION]")
            sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session("UID").ToString(), DbType.String)
            sp.Command.AddParameter("@Reqid", reqid, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet()

            Dim rds As ReportDataSource = New ReportDataSource()
            rds.Name = "AssetTransfer"
            rds.Value = ds.Tables(0)
            Dim rds1 As ReportDataSource = New ReportDataSource()
            rds1.Name = "AssetTransferSending"
            rds1.Value = ds.Tables(1)
            Dim rds2 As ReportDataSource = New ReportDataSource()
            rds2.Name = "AssetTransferReceiving"
            rds2.Value = ds.Tables(2)
            viewer.Reset()
            viewer.LocalReport.DataSources.Add(rds)
            viewer.LocalReport.DataSources.Add(rds1)
            viewer.LocalReport.DataSources.Add(rds2)
            viewer.LocalReport.EnableExternalImages = True
            viewer.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetTransformReport.rdlc")

            Dim ci As New CultureInfo(Session("userculture").ToString())
            Dim nfi As NumberFormatInfo = ci.NumberFormat
            'Dim p1 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())
            'Dim p2 As New ReportParameter("ImageVal", BindLogo())
            viewer.LocalReport.EnableExternalImages = True
            Dim imagePath As String = BindLogo()
            Dim parameter As New ReportParameter("Imagepath", imagePath)
            viewer.LocalReport.SetParameters(parameter)
            viewer.LocalReport.Refresh()

            viewer.ProcessingMode = ProcessingMode.Local
            ''viewer.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetDisposalApprvdReport.rdlc")

            reqid = reqid.Substring(reqid.Length - 1)
            Dim FileName As String = "AssetTransfer Report" & reqid & ".pdf" 'DateTime.Now.ToString("ddMMyyyyhhmmss") & ".pdf"
            ''Dim FileName As String = "Finalize PO" & DateTime.Now.ToString("ddMMyyyyhhmmss") & ".pdf"
            Dim extension As String
            Dim FileFullpath As String
            Dim encoding As String
            Dim mimeType As String
            Dim streams As String()
            Dim warnings As Warning()
            Dim contentType As String = "application/pdf"
            Dim mybytes As Byte() = viewer.LocalReport.Render("PDF", Nothing, extension, encoding, mimeType, streams, warnings)
            FileFullpath = Server.MapPath("~/AssetDisposal_PDF/") & FileName
            Using fs As FileStream = File.Create(Server.MapPath("~/AssetDisposal_PDF/") & FileName)
                fs.Write(mybytes, 0, mybytes.Length)
            End Using

            Response.ContentType = contentType
            Response.AddHeader("Content-Disposition", "attachment; filename=" & FileName)
            ''Response.WriteFile(Server.MapPath("~/FinalizePO_pdfs/" & FileName))
            ''Response.Flush()
            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "sp_get_Transform_mails")
            sp3.Command.AddParameter("@MMR_REQ_ID", reqid1, DbType.String)
            Dim ds3 As DataSet = sp3.GetDataSet()
            If (ds3.Tables(0).Rows.Count > 0) Then
                SendMail(FileFullpath, ds3.Tables(0).Rows(0)("TOMAIL").ToString(), ds3.Tables(0).Rows(0)("CCMAIL").ToString())
            End If
        Catch ex As Exception
            ex.ToString()

        End Try
    End Sub



    Public Function BindLogo() As String
        'Dim imagePath As String
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
        sp3.Command.AddParameter("@type", "2", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp3.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
        Dim ds3 As DataSet = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            Return "https://live.quickfms.com/BootStrapCSS/images/" & ds3.Tables(0).Rows(0).Item("IMAGENAME")

        Else
            Return "https://live.quickfms.com/BootStrapCSS/images/yourlogo.png"
        End If
        'Return New Uri(Server.MapPath(imagePath)).AbsoluteUri
    End Function

    Public Shared Sub SendMail(ByVal FileFullpath As String, ByVal TOMAIL As String, ByVal CCMAIL As String)

        Dim smtp As SmtpClient = New SmtpClient()
        Dim fromaddr As MailAddress = New MailAddress(ConfigurationManager.AppSettings("from"))
        smtp.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("mailid"), ConfigurationManager.AppSettings("password"))
        smtp.Host = ConfigurationManager.AppSettings("Host")
        smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings("Port"))
        smtp.EnableSsl = True
        Dim mailMessage As MailMessage = New MailMessage()
        mailMessage.From = fromaddr
        mailMessage.Subject = "Asset Transfer Report"
        Dim str As String = "<!DOCTYPE html><html><body><p>Dear Sir/Madam,</p></br><p>Asset is received, please find the attachement. </p></br><p>Kindly do not hesitate to contact at undersigned, in case of any clarification/discrepancies.</p><div>Best Regards,</div><div>Rupesh Kairamkonda</div><div>Dy. Manager Administration (Procurment)</div><div>Tel: (91-22) 3950 9900 | Mob: +91 98333 97314</div><div>Email: rupesh.kairamkonda@aadharhousing.com</div><p>This is an auto-generated E-mail, hence do not reply to this E-mail id. </p></body></html>"
        mailMessage.Body = str
        mailMessage.IsBodyHtml = True
        'Dim TOemailarr As String = TOMAIL
        mailMessage.[To].Add(TOMAIL)
        'Dim TOemailarr As String() = ConfigurationManager.AppSettings("GLOBALTO").ToString().Split(";"c)

        'For Each [to] As String In TOemailarr
        '    mailMessage.[To].Add([to])
        'Next
        'Dim Bccemailarr As String = CCMAIL
        'Dim Bccemailarr As String() = ConfigurationManager.AppSettings("GLOBALBCC").ToString().Split(";"c)

        'For Each bcc As String In Bccemailarr
        mailMessage.CC.Add(CCMAIL)
        'Next

        Using attachment As Attachment = New Attachment(FileFullpath, MediaTypeNames.Application.Octet)

            Try
                Dim disposition As Mime.ContentDisposition = attachment.ContentDisposition
                disposition.CreationDate = File.GetCreationTime(FileFullpath)
                disposition.ModificationDate = File.GetLastWriteTime(FileFullpath)
                disposition.ReadDate = File.GetLastAccessTime(FileFullpath)
                disposition.FileName = Path.GetFileName(FileFullpath)
                disposition.Size = New FileInfo(FileFullpath).Length
                disposition.DispositionType = DispositionTypeNames.Attachment
                mailMessage.Attachments.Add(attachment)
            Catch ex As Exception
            End Try

            If mailMessage.[To].Count() > 0 Then smtp.Send(mailMessage)
        End Using
    End Sub

End Class
