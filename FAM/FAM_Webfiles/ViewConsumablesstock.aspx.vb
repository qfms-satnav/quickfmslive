Imports System.Data
Imports System.Data.SqlClient
Imports clsSubSonicCommonFunctions
Imports System.IO
Imports System.Web
Imports System
Imports System.Web.UI.WebControls
Imports System.Web.UI

Partial Class FAM_FAM_Webfiles_ViewConsumablesstock
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            divGrid.Visible = True
            BindLocation()
            getassetcategory()
            ddlastsubCat.Items.Insert(0, New ListItem("--All--", "All"))
            ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
            ddlAstModel.Items.Insert(0, "--All--")
            fillgrid()

        End If
    End Sub





    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GETCHECK_CONSUMABLES")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        'sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        ddlastCat.DataSource = sp.GetDataSet()
        ddlastCat.DataTextField = "VT_TYPE"
        ddlastCat.DataValueField = "VT_CODE"
        ddlastCat.DataBind()
        ddlastCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        'ddlLocation.Items.Insert(0, New ListItem("--All--", ""))
        ddlLocation.Items.Remove("--Select--")
        'ddlLocation.Items.Remove("--All--")
    End Sub

    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()

        ddlAstBrand.ClearSelection()
        ddlAstModel.ClearSelection()
        btnExport.Visible = False
        divGrid.Visible = False
        gvCusomableAstStock.DataSource = Nothing
        gvCusomableAstStock.DataBind()
    End Sub

    Protected Sub gvCusomableAstStock_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvCusomableAstStock.RowCommand
        If e.CommandName = "Edit" Then
            Response.Redirect("UpdateConsumablesstock.aspx?sid=" & e.CommandArgument)
        End If
    End Sub
    Private Sub fillgrid()
        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlLocation.SelectedItem.Value
        param(1) = New SqlParameter("@AST_SUB_CAT", SqlDbType.NVarChar, 200)
        param(1).Value = ddlastsubCat.SelectedItem.Value
        param(2) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlastCat.SelectedItem.Value
        param(3) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(3).Value = Session("UID")
        param(4) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 100)
        param(4).Value = Session("COMPANYID")
        

        ObjSubsonic.BindGridView(gvCusomableAstStock, "AM_AST_GET_CONSUMBLESBYLOC_NP", param)
        If gvCusomableAstStock.Rows.Count = 0 Then
            btnExport.Visible = False
            gvCusomableAstStock.DataSource = Nothing
            gvCusomableAstStock.DataBind()
        Else
            btnExport.Visible = True
        End If
    End Sub

    Protected Sub gvCusomableAstStock_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvCusomableAstStock.PageIndexChanging
        gvCusomableAstStock.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

    Protected Sub ddlastCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlastCat.SelectedIndexChanged
        getsubcategorybycat(ddlastCat.SelectedItem.Value)
    End Sub

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        ' ddlAstSubCat.Enabled = True
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        ddlastsubCat.DataSource = sp.GetDataSet()
        ddlastsubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlastsubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlastsubCat.DataBind()
        ' ddlastsubCat.Items.Insert(0, "--Select--")
        ddlastsubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        divGrid.Visible = True
        fillgrid()

    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim gv As New GridView

        Dim param(4) As SqlParameter
        param(0) = New SqlParameter("@LOC_CODE", SqlDbType.NVarChar, 200)
        param(0).Value = ddlLocation.SelectedItem.Value
        param(1) = New SqlParameter("@AST_SUB_CAT", SqlDbType.NVarChar, 200)
        param(1).Value = ddlastsubCat.SelectedItem.Value
        param(2) = New SqlParameter("@AST_CODE", SqlDbType.NVarChar, 200)
        param(2).Value = ddlastCat.SelectedItem.Value
        param(3) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(3).Value = Session("UID")
        param(4) = New SqlParameter("@COMPANYID", SqlDbType.Int, 10)
        param(4).Value = Session("COMPANYID")
        gv.DataSource = ObjSubsonic.GetSubSonicDataSet("AM_AST_GET_CONSUMBLESBYLOC_NP", param)
        gv.DataBind()
        Export("TotalConsumableStock.xls", gv)
    End Sub
    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()
    End Sub
    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

    Private Sub getbrandbycatsubcat(ByVal astcatcode As String, ByVal astsubcatcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlastsubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--All--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))

    End Sub
    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlastsubCat.SelectedIndexChanged
        If ddlastsubCat.SelectedIndex > 0 Then
            getbrandbycatsubcat(ddlastCat.SelectedItem.Value, ddlastsubCat.SelectedItem.Value)
        End If

    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        If ddlAstBrand.SelectedIndex > 0 Then
            getmakebycatsubcat()
        End If

    End Sub

    Private Sub getmodel()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MODELDRP")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_CODE"
        ddlAstModel.DataValueField = "AST_MD_ID"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, "--All--")
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlastCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlastsubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, "--All--")

    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_VIEW_STOCK_FILTER_GRID")
            sp2.Command.AddParameter("@SEARCH", txtSearch.Text, DbType.String)
            sp2.Command.AddParameter("@AUR_ID", HttpContext.Current.Session("Uid"), DbType.String)
            sp2.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
            Session("dataset") = sp2.GetDataSet()
            gvCusomableAstStock.DataSource = Session("dataset")
            gvCusomableAstStock.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
End Class
