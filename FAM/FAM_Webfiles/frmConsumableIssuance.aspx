﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmConsumableIssuance.aspx.vb" Inherits="FAM_FAM_Webfiles_frmConsumableIssuance" %>

<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc2" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
      <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>

<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Create Plan" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Consumable Issuance</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            
                            <form id="form1" runat="server">
                                 
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                                    ForeColor="Red" ValidationGroup="Val1" />
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                         <asp:TextBox ID="txtReqID" runat="server" CssClass="form-control" BorderColor="White" ></asp:TextBox>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                            <asp:Label ID="Label2" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Employee Id<span style="color: red;">*</span></label>
                                                    <div class="col-md-12">
                                                        <asp:DropDownList ID="ddlEmp" runat="server" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Asset Available Location<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddllocation"
                                                        Display="None" ErrorMessage="Please Select Location" ValidationGroup="Val1"
                                                        InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                    <div class="col-md-12">
                                                        <asp:DropDownList ID="ddllocation" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Asset Category<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvastcat" runat="server" ControlToValidate="ddlAstCat"
                                                        Display="None" ErrorMessage="Please Select Asset Category" ValidationGroup="Val1"
                                                        InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                    <div class="col-md-12">
                                                        <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Asset Subcategory<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAstSubCat"
                                                        Display="None" ErrorMessage="Please Select Asset Sub Category" ValidationGroup="Val1"
                                                        InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                    <div class="col-md-12">
                                                        <asp:DropDownList ID="ddlAstSubCat" runat="server" AutoPostBack="True" CssClass="selectpicker" data-live-search="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        

                                        
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Asset Brand/Make<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlAstBrand"
                                                        Display="None" ErrorMessage="Please Select Asset Brand" ValidationGroup="Val1"
                                                        InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                    <div class="col-md-12">
                                                        <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Invoice If Needed</label>
                                                    <div class="col-md-12">
                                                        <input type="checkbox" id="rdbpo" value="Invoice" runat="server" />
                                                    </div>
                                                </div>
                                            </div>
                                       
                                      <br />
                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <div class="form-group">
                                                    <asp:Button ID="btnSearch" runat="server" CausesValidation="true" CssClass="btn btn-primary custom-button-color" Text="Search" ValidationGroup="Val1" />
                                                    <asp:Button ID="btnclear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Clear" CausesValidation="False" />
                                                </div>
                                            </div>
                                        </div>

                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                            EmptyDataText="No Asset(s) Found." CssClass="table table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:BoundField DataField="CAT" HeaderText="Category Name" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="SUBCAT" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="BRAND" HeaderText="Brand" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="MDNAME" HeaderText="Model Name" ItemStyle-HorizontalAlign="left" />
                                                <asp:BoundField DataField="UNITS" HeaderText="Units" ItemStyle-HorizontalAlign="left" />
                                                <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="left">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server"></asp:CheckBox>
                                                        <asp:Label ID="lblmdid" Visible="false" Text='<%#Eval("AST_MD_ID")%>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>

                                        <div class="row">
                                            <div class="col-md-12 text-right">
                                                <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn-primary custom-button-color" Visible="false" ValidationGroup="Val1" />
                                            </div>
                                        </div>
                                       
                                        <asp:GridView ID="gvitemslist" runat="server" AutoGenerateColumns="false" EmptyDataText="No Assets Found"
                                            CssClass="table table-condensed table-bordered table-hover table-striped" OnRowDeleting="gvitemslist_RowDeleting">
                                            <Columns>
                                                <asp:BoundField DataField="AST_MD_NAME" HeaderText="Item Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AST_SUBCAT_NAME" HeaderText="Sub Category Name" ItemStyle-HorizontalAlign="left">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AMG_VENCAN_RT" HeaderText="Price" ItemStyle-HorizontalAlign="left" DataFormatString="{0:c2}">
                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AAC_CON_TOTAVBL" HeaderText="Total Available" ItemStyle-HorizontalAlign="left">

                                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                                </asp:BoundField>

                                                <asp:TemplateField HeaderText="Quantity Issued" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtQty" AutoPostBack="True" OnTextChanged="txtQty_TextChanged" runat="server" MaxLength="10"></asp:TextBox>
                                                        <cc2:FilteredTextBoxExtender ID="txtQty_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtQty">
                                                        </cc2:FilteredTextBoxExtender>
                                                        <asp:Label ID="lblITEMCODE" runat="server" Text='<%#Eval("AST_MD_NAME")%>' Visible="false"></asp:Label>
                                                        <asp:Label ID="lblprice" Text='<%#Eval("AMG_VENCAN_RT")%>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblBrand" Text='<%#Eval("AST_MD_BRDID") %>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbltotal" Text='<%#Eval("AAC_CON_TOTAVBL")%>' runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lblmdid" Visible="false" Text='<%#Eval("AST_MD_ID")%>' runat="server"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Discount(%)" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtdiscnt" AutoPostBack="True" OnTextChanged="txtdiscnt_TextChanged" runat="server" MaxLength="10" Text="0"></asp:TextBox>
                                                        <cc2:FilteredTextBoxExtender ID="txtdiscnt_FilteredTextBoxExtender" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="txtdiscnt">
                                                        </cc2:FilteredTextBoxExtender>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Total Price" ItemStyle-HorizontalAlign="left" ItemStyle-VerticalAlign="Middle">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltlprice" runat="server" MaxLength="10"></asp:Label>
                                                    </ItemTemplate>

                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle"></ItemStyle>
                                                </asp:TemplateField>

                                                <asp:ButtonField Text="Delete" CommandName="Delete" />

                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                            <br />
                                                 <div id="ddllocation2" class="col-md-3 col-sm-12 col-xs-12" runat="server">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Asset Issuance Location<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddllocation1"
                                                    Display="None" ErrorMessage="Please Select Issuance Location" ValidationGroup="Val1"
                                                    InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                <div class="col-md-12">
                                                    <asp:DropDownList ID="ddllocation1" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" id="divid" style="display: block">
                                            <div class="col-md-12 text-right">
                                                <asp:Button ID="btnsubmit1" runat="server"  Text="Save" CssClass="btn btn-primary custom-button-color" Visible="false" ValidationGroup="Val1" />
                                                <%--<input type="button"  ID="btnsubmit2" runat="server"  value="Show Invoice" onclick="showPopWin('<%=txtReqID.Text%>');" />--%>
                                               
                                            </div>
                                        </div>
                                             <div class="row" id="divide" style="display: block" runat="server">
                                            <div class="col-md-12 text-right">
                                                <a   href="#"  class="btn btn-primary" id="showInvoiceButton"  onclick="showPopWin('<%=txtReqID.Text%>')" style="border: 1px solid gray">
                                                    <asp:Label ID="btnsubmit2" runat="server" Text="Show Invoice"></asp:Label>
                                                </a>
                                            </div>
                                        </div>

                                        <%--                                <cc2:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1" TargetControlID="btnsubmit1"
                                    BackgroundCssClass="modalBackground" CancelControlID="close">
                                </cc2:ModalPopupExtender>--%>

                                        <%-- <asp:Panel ID="Panel1" runat="server">
                                    <div id="modelcontainer">
                                        <iframe id="modalcontentframe" runat="server" width="900px" height="500px" style="border: none"></iframe>
                                    </div>
                                    <input type="button" id="close" value="Close" onclick="CloseDialog()" />
                                </asp:Panel>--%>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="InvoiceModel" runat="server" tabindex='-1' data-backdrop="false">
        <div class="modal-dialog modal-lg" style="width: 800px;">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title" id="H1">PO Details</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="500px" style="border: none"></iframe>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script defer>
   
 
   
    function showPopWin(id) {
       
      <%--  var rid = document.getElementById('<%=HiddenField.ClientID %>').Value;
        var id = '<%= Session("RID")%>';--%>
        /*  var reqId = session["rid"];*/
        if ($("#rdbpo").prop('checked') == true) {
            $("#modalcontentframe").attr("src", "IssuanceInvoice.aspx?rid=" +id+"");
            $("#InvoiceModel").modal('show');

            return true;
        }
    }

    
        $(document).ready(function() {
            $('#InvoiceModel').on('hidden.bs.modal', function () {
                $('#showInvoiceButton').hide(); 
            });
    });

    function CloseDialog() {
        $("#<%=btnsubmit1.ClientID %>").css('visibility', 'hidden');
        
    }
    function refreshSelectpicker() {

        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAstBrand.ClientID%>").selectpicker();
        $("#<%=ddllocation.ClientID%>").selectpicker();
        $("#<%=ddlAstCat.ClientID%>").selectpicker();
        $("#<%=ddlEmp.ClientID%>").selectpicker();
        $("#<%=ddllocation1.ClientID%>").selectpicker();

    }
    refreshSelectpicker();
</script>

<style type="text/css">
    .modalBackground {
        background-color: Black;
        filter: alpha(opacity=90);
        opacity: 0.8;
    }

    .modalPopup {
        background-color: #FFFFFF;
        border-width: 3px;
        border-style: solid;
        border-color: black;
        padding-top: 10px;
        padding-left: 10px;
        width: 300px;
        height: 140px;
    }

    .table-striped 
    {}
</style>
