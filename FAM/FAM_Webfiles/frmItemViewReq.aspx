<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmItemViewReq.aspx.vb" Inherits="FAM_FAM_Webfiles_frmItemViewReq" Title="View Item Requsition" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">View Consumuable Requisition</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search By Any.."></asp:TextBox>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" />
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-12">
                            <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                EmptyDataText="No Requests Found." CssClass="table GridStyle" GridLines="None">
                                <Columns>

                                    <asp:TemplateField HeaderText="Requisition ID" ItemStyle-HorizontalAlign="left">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("AIR_REQ_ID","~/FAM/FAM_WebFiles/frmItemViewReqDetails.aspx?RID={0}") %>'
                                                Text='<%# Eval("AIR_REQ_ID")%> '></asp:HyperLink>
                                            <asp:Label ID="lblStatusId" runat="server" Text='<%#Eval("AIR_STA_ID") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AUR_NAME" HeaderText="Requested By" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField DataField="AIR_REQ_DATE" HeaderText="Requested Date" ItemStyle-HorizontalAlign="left" />
                                    <asp:BoundField DataField="STA_TITLE" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
                                </Columns>
                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                <PagerStyle CssClass="pagination-ys" />
                            </asp:GridView>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

