<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false"
    CodeFile="AssetReq_StatusUpdate.aspx.vb" Inherits="FAM_FAM_Webfiles_AssetReq_StatusUpdate"
    Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="100%" align="center">
                <asp:Label ID="Label1" runat="server" CssClass="clsHead" Width="86%" Font-Underline="False"
                    ForeColor="Black"> 
                </asp:Label>
                <hr align="center" width="60%" />
                <br />
            </td>
        </tr>
    </table>
    <table width="95%" style="vertical-align: top;" cellpadding="0" cellspacing="0" align="center"
        border="0">
        <tr>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_left_top_corner.gif")%>"
                    width="9" /></td>
            <td width="100%" class="tableHEADER" align="left">
                &nbsp;<strong>
                    <asp:Label ID="Label2" runat="server"></asp:Label></strong>
            </td>
            <td>
                <img alt="" height="27" src="<%=Page.ResolveUrl("~/images/table_right_top_corner.gif")%>"
                    width="16" /></td>
        </tr>
        <tr>
            <td background="<%=Page.ResolveUrl("~/Images/table_left_mid_bg.gif")%>">
                &nbsp;</td>
            <td align="left">
                <table id="table1" cellspacing="2" cellpadding="2" width="100%" border="1" style="border-collapse: collapse">
                    <tr>
                        <td colspan="2" valign="top" align="left">
                            <asp:Label ID="lblmsg" ForeColor="red" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" valign="top" align="left">
                            <asp:Panel ID="Panel1" Visible="false" runat="server">
                                <fieldset id="RmRequisitionDetails" runat="server">
                                    <legend>
                                        <asp:Label ID="lblTitle" runat="server"></asp:Label></legend>
                                    <table cellspacing="0" cellpadding="0" width="100%" border="1" style="border-collapse: collapse">
                                        <tr>
                                            <td align="left">
                                                Requisition Id:</td>
                                            <td align="left">
                                                <asp:Label ID="lblReqId" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Raised By:</td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlEmp" runat="server" Width="275px" Enabled="false">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Asset Category:</td>
                                            <td align="left">
                                                <asp:DropDownList ID="ddlAstCat" runat="server" AutoPostBack="True" Width="275px">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="left" valign="top">
                                                <asp:Panel ID="pnlItems" runat="server" Width="100%">
                                                    Assets List
                                                    <asp:GridView ID="gvItems" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                        EmptyDataText="No Asset(s) Found." Width="100%">
                                                        <Columns>
                                                            <asp:BoundField DataField="sku" HeaderText="sku" ItemStyle-HorizontalAlign="left" />
                                                            <asp:BoundField DataField="productname" HeaderText="Name" ItemStyle-HorizontalAlign="left" />
                                                            <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="left">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtQty" runat="server" Width="50px" MaxLength="10" Enabled="false"></asp:TextBox>
                                                                    <asp:Label ID="lblProductId" runat="server" Text='<%#Eval("ProductId") %>' Visible="false"></asp:Label>
                                                                    <asp:Label ID="lblProductName" runat="server" Text='<%#Eval("Productname") %>' Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center">
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkSelect" runat="server" Enabled="false"></asp:CheckBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Status:</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtStatus" runat="server" Width="275px" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr3" runat="Server">
                                            <td align="left">
                                                Requestor Remarks:</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtRRemarks" runat="server" TextMode="MultiLine" Width="400px" ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr1" runat="Server">
                                            <td align="left">
                                                RM Remarks:</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtRMRemarks" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr id="tr2" runat="server" visible="false">
                                            <td align="left">
                                                Admin Remarks:</td>
                                            <td align="left">
                                                <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" Width="400px"
                                                    ReadOnly="True"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </asp:Panel>
                            <asp:Panel ID="pnlInterMovementITApproval_Reject" Visible="false" runat="server">
                                <table width="100%" cellpadding="2">
                                    <tr>
                                        <td style="height: 26px">
                                            From Location</td>
                                        <td style="height: 26px">
                                            <asp:DropDownList ID="ddlSLocInterMovementITAp_Rj" runat="server" Width="275px" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From Tower</td>
                                        <td>
                                            <asp:DropDownList ID="ddlSTowerInterMovementITAp_Rj" runat="server" Width="275px"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From Floor</td>
                                        <td>
                                            <asp:DropDownList ID="ddlSFloorInterMovementITAp_Rj" runat="server" Width="275px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Person To Receive Assets</td>
                                        <td>
                                            <asp:DropDownList ID="ddlEmpInterMovementITAp_Rj" runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlIntraMovementITApproval_Reject" Visible="false" runat="server">
                                <table width="100%" cellpadding="2px">
                                    <tr>
                                        <td>
                                            From Location</td>
                                        <td>
                                            <asp:DropDownList ID="ddlSLocIntraMovementITApproval_Reject" Enabled="false" runat="server"
                                                Width="275px" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From Tower</td>
                                        <td>
                                            <asp:DropDownList ID="ddlSTowerIntraMovementITApproval_Reject" Enabled="false" runat="server"
                                                Width="275px" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            From Floor</td>
                                        <td>
                                            <asp:DropDownList ID="ddlSFloorIntraMovementITApproval_Reject" Enabled="false" runat="server"
                                                Width="275px">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:GridView ID="gvItemIntraMovementITApproval_Reject" runat="server" EmptyDataText="No Assets Found."
                                                AllowPaging="true" PageSize="10" Width="100%" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAAS_AAT_CODE" runat="server" Text='<%#Eval("AAT_CODE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Asset Name" ItemStyle-HorizontalAlign="left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAAT_NAME" runat="server" Text='<%#Eval("AAT_NAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="height: 26px">
                                            To Location</td>
                                        <td style="height: 26px">
                                            <asp:DropDownList ID="ddlDLocIntraMovementITApproval_Reject" runat="server" Width="275px"
                                                AutoPostBack="True" Enabled="False">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            To Tower</td>
                                        <td>
                                            <asp:DropDownList ID="ddlDTowerIntraMovementITApproval_Reject" runat="server" Width="275px"
                                                AutoPostBack="True" Enabled="False">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            To Floor</td>
                                        <td>
                                            <asp:DropDownList ID="ddlDFloorIntraMovementITApproval_Reject" runat="server" Width="275px"
                                                Enabled="False">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Person To Receive Assets</td>
                                        <td>
                                            <asp:TextBox ID="txtPersonName" runat="server" MaxLength="50" Width="275px" Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr id="tr4" runat="Server">
                        <td align="left">
                            Remarks:</td>
                        <td align="left">
                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="400px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" CssClass="button" />
                        </td>
                    </tr>
                </table>
            </td>
            <td background="<%=Page.ResolveUrl("~/Images/table_right_mid_bg.gif")%>" style="width: 10px;
                height: 100%;">
                &nbsp;</td>
        </tr>
        <tr>
            <td style="width: 10px; height: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_left_bot_corner.gif")%>"
                    width="9" /></td>
            <td style="height: 17px" background="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_bot_mid_bg.gif")%>"
                    width="25" /></td>
            <td style="height: 17px; width: 17px;">
                <img alt="" height="17" src="<%=Page.ResolveUrl("~/Images/table_right_bot_corner.gif")%>"
                    width="16" /></td>
        </tr>
    </table>
</asp:Content>
