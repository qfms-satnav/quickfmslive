<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSpaceAssetMapping.aspx.vb" Inherits="FAM_FAM_Webfiles_frmSpaceAssetMapping" Title="Space Asset Mapping" %>

<%@ Register Src="../../Controls/SpaceAssetMapping.ascx" TagName="SpaceAssetMapping" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>

    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Asset Mapping</h3>
            </div>
            <div class="card">
                <%-- <div class="panel-body" style="padding-right: 50px;">--%>
                <form id="form1" runat="server">
                    <%--<asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
                                 <asp:updatepanel runat="server">
                                      <ContentTemplate>--%>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                    <uc1:SpaceAssetMapping ID="SpaceAssetMapping1" runat="server" />
                    <%--</ContentTemplate>
                                     </asp:updatepanel>--%>
                </form>
            </div>
        </div>
    </div>
</body>
</html>

