
Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Commerce.Common
Imports Commerce.Promotions
Partial Class FAM_FAM_Webfiles_AssetDetails
    Inherits System.Web.UI.Page

    Protected productID As Integer
    Protected productSku, ProductName As String
    Protected productGUID As Guid

    Public product As New Commerce.Common.Product
    Protected discount As ProductDiscount
    Private Getproduct As New Commerce.Common.Product
    Dim ds As New DataSet
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load

        '###############################################################################
        '  Page Validators - these must be implemented or they will be redirected
        '###############################################################################
        Try

            Dim sProductGUID As String = Utility.GetParameter("guid")
            productID = Utility.GetIntParameter("id")
            productSku = Utility.GetParameter("n")
            ProductName = Utility.GetParameter("prdname")

            If sProductGUID <> String.Empty Then
                productGUID = New Guid(sProductGUID)
                product = ProductController.GetProductDeepByGUID(productGUID)
            ElseIf productID <> 0 Then
                product = ProductController.GetProductDeep(productID)
            ElseIf productSku <> String.Empty Then
                product = ProductController.GetProductDeep(productSku)
            ElseIf ProductName <> String.Empty Then

                ProductName = Server.HtmlDecode(ProductName)

                ds = ProductController.GetProductDeepProductName(ProductName)
                If ds.Tables(0).Rows.Count > 0 Then
                    productID = ds.Tables(0).Rows(0).Item("ProductId")
                    product = ProductController.GetProductDeep(productID)

                End If



            End If

            If Not sProductGUID Is Nothing Then
                Getproduct = ProductController.GetProductSEOTagsById(product.ProductGUID).Item(0)
            End If

            'BindSEOTags(Getproduct.ProductPageTitle, Getproduct.ProductPageKeywords, Getproduct.ProductPageMetaDescription)

            'make sure we have a product
            TestCondition.IsTrue(product.IsLoaded, "Invalid url/product id")

            'set the page variables
            productID = product.ProductID
            productSku = product.Sku
        Catch ex As Exception
            'Throw ex
            'ExceptionPolicy.HandleException(ex, "Application Exception");
            'Response.Redirect(Page.ResolveUrl("~/ExceptionPage.aspx"), false);
        End Try
        '##############################################################################
        'load the product ratings
        BindProductInfo()
        'BindRelatedProducts()

    End Sub
 

    
    Private Sub BindProductInfo()
        Try
            AstDetails1.product = product

            'set the ratings
            'this was passed in 

            'BundleDisplay1.product = product
            'ReviewDisplay1.product = product
            'FeedBackDisplay1.ProductID = product.ProductID
            'FeedBackDisplay1.ProductName = product.ProductName
            'FeedBackDisplay1.InitialRating = Convert.ToDouble(product.Rating)
            'ProductDescriptorDisplay1.DescriptorList = product.Descriptors
            'Me.Title = product.ProductName
            'response.write(Me.Title)
            'If Me.Title = "" Or Me.Title Is Nothing Then
            '    Me.Title = ConfigurationManager.AppSettings("HomePageTitle")
            'End If
        Catch ex As Exception

        End Try

    End Sub

    
    
End Class