Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common

Partial Class FAM_FAM_Webfiles_frmAssetRequisitionDetails_PO
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        'TermsandConditions =
        '"  1) Goods should be Accompanied by Challan ub triplicate, and MUST bear our purchase order no. & Sr. No. " + Environment.NewLine +
        '"  2) The quality of the supplies is to be approved by us and our Approval or rejection will be final." + Environment.NewLine +
        '"  3) Payments should be made without prejudice to our rights against partly executive order."



        txtDOD.Attributes.Add("readonly", "readonly")
        RegExpNumber.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        reqnum.ValidationExpression = User_Validation.GetValidationExpressionForAddress.VAL_EXPR()
        If Not IsPostBack Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_PO_TERMS_CONDITIONS")
            txtTerms.Text = sp.ExecuteScalar

            pnlItems.Visible = True
            'BindRequisition()
            BindVendor2Details()
            BindGrid()
            BindState()
            divven.Visible = False
            gvItems.Visible = False
        End If
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Rbtwith.CheckedChanged, Rbtwithout.CheckedChanged
        lblMsg.Visible = False
        txtAdvance.Text = "0"
        'txtFFIP.Text = "0"
        txtCst.Text = "0"
        txtsgst.Text = "0"
        txtigst.Text = "0"
        txtDelivery.Text = "0"
        'txtWst.Text = "0"
        txtInstallation.Text = "0"
        'txtOctrai.Text = "0"
        txtCommissioning.Text = "0"
        'txtServiceTax.Text = "0"
        txtRetention.Text = "0"
        ' txtOthers.Text = "0"
        txtPayments.Text = "0"
        Try
            'If RbtEmployee.Checked = True Then
            'ddlLocation.Items.Clear()
            'BindLocation()
            'BindAssets()
            If Rbtwithout.Checked = True Then
                divven.Visible = False
                gvItems.Visible = False
                gvvendorreqs.Visible = True
                BindGrid()
                divvendors.Visible = True
                BindVendor2Details()
            Else
                divvendors.Visible = False
                gvItems.Visible = False
                divven.Visible = True
                BindGrid()
                gvvendorreqs.Visible = True
                BindVendorDetails()
            End If


            'LoadGridData()
            'End If
        Catch ex As Exception

        End Try

    End Sub

    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_GENERATE_PO_DETAILS")
        sp.Command.AddParameter("@dummy", 3022, Data.DbType.Int32) '--1012
        sp.Command.AddParameter("@Cuser", Session("uid"), Data.DbType.String)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
        'gvvendorreqs.DataSource = sp.GetDataSet.Tables(0).ToString()
        Dim ds As New DataSet()
        ds = sp.GetDataSet()
        gvvendorreqs.DataSource = ds
        gvvendorreqs.DataBind()


    End Sub

    Private Sub BindVendorDetails()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GETVENDOR_FORASSE")
        'sp.Command.AddParameter("@REQ_ID", Request.QueryString("RID"), DbType.String)
        ddlVendor.DataSource = sp.GetReader
        ddlVendor.DataTextField = "AVR_NAME"
        ddlVendor.DataValueField = "AVR_CODE"
        ddlVendor.DataBind()
        ddlVendor.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindState()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_BIND_STATES")
        ddlState.DataSource = sp.GetReader
        ddlState.DataTextField = "AM_ST_NAME"
        ddlState.DataValueField = "AM_ST_CODE"
        ddlState.DataBind()
        ddlState.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Sub BindVendor2Details()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GETVENDORS")
        'sp.Command.AddParameter("@REQ_ID", Request.QueryString("RID"), DbType.String)
        ddlVendor2.DataSource = sp.GetReader
        ddlVendor2.DataTextField = "AVR_NAME"
        ddlVendor2.DataValueField = "AVR_CODE"
        ddlVendor2.DataBind()
        ddlVendor2.Items.Insert(0, New ListItem("--Select--", "0"))
    End Sub

    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function

    Private Sub BindRequisition()
        'Dim ReqId As String = Request("RID")

        'Dim duplicates As List(Of String) = New List(Of String)()
        Dim ds As DataSet
        Dim dt As New DataTable
        For Each row As GridViewRow In gvvendorreqs.Rows
            Dim chkSelect1 As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblReqID As Label = TryCast(row.FindControl("lblReqID"), Label)
            If chkSelect1.Checked = True Then
                'lblReqID.Checked = True Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionDetailsProcurement_GetDetailsByReqId")
                'Else
                sp.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                sp.Command.AddParameter("@VEN_CODE", ddlVendor.SelectedItem.Value, DbType.String)
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                ds = sp.GetDataSet
                dt.Merge(ds.Tables(0))
                'lblReqID.Visible = True
            End If
            'End If
        Next
        gvItems.DataSource = dt
        gvItems.DataBind()
        pnlItems.Visible = True

    End Sub

    'Private Sub BindUserDetails(ByVal ReqId As String)
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_ITEM_REQUISITION_USERDETAILS_GETBYREQID")
    '    sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
    '    Dim dr As SqlDataReader = sp.GetReader()
    '    If dr.Read() Then
    '        txtPORaisedBy.Text = dr("AUR_KNOWN_AS")
    '        'txtRemarks.Text=
    '        txtRequesterId.Text = dr("AUR_ID")
    '        txtRequesterName.Text = dr("AUR_KNOWN_AS")
    '        txtRequestorRemarks.Text = dr("AIR_REMARKS")
    '        txtDeptId.Text = dr("AUR_DEP_ID")
    '        txtLocation.Text = dr("AUR_LOCATION")
    '    End If
    'End Sub

    Protected Sub btnTotalCost_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTotalCost.Click
        If Not Page.IsValid Then
            Exit Sub
        End If

        Dim ItemSubTotal As Double = 0
        Dim FFIP As Double = 0
        Dim Advance As Double = 0
        Dim cgst As Double = 0
        Dim igst As Double = 0
        Dim sgst As Double = 0
        Dim OnDelivery As Double = 0
        Dim Wst As Double = 0
        Dim Installation As Double = 0
        Dim Octrai As Double = 0
        Dim Commissioning As Double = 0
        Dim ServiceTax As Double = 0
        Dim Retention As Double = 0
        Dim Others As Double = 0
        Dim Payments As Double = 0
        Dim TotalCost As Double = 0
        Dim ChkCount As Integer = 0
        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect1 As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim txtPrice As TextBox = DirectCast(row.FindControl("txtPrice"), TextBox)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim txtUnitPrice As TextBox = DirectCast(row.FindControl("txtUnitPrice"), TextBox)
            Dim unitprice As Double = 0
            Dim PurchaseQty As Double = 0
            Decimal.TryParse(Trim(txtUnitPrice.Text), unitprice)
            Decimal.TryParse(Trim(txtPurchaseQty.Text), PurchaseQty)
            If chkSelect1.Checked Then
                'If unitprice = 0 Then
                '    lblMsg.Visible = True
                '    'lblMsg.Text = "Please enter unit price for selected checkbox"
                '    lblMsg.Text = "Unit price shouldn't be zero for selected checkbox"
                '    Exit Sub
                'End If
                ItemSubTotal += (PurchaseQty * unitprice)
                ChkCount = ChkCount + 1
                lblMsg.Visible = False
            End If
        Next
        If ChkCount = 0 Then
            lblMsg.Visible = True
            lblMsg.Text = "Select checkbox"
            Exit Sub
        End If

        'If Double.TryParse(txtFFIP.Text, FFIP) Then
        'Else
        '    lblMsg.Text = "Invalid Tax"
        '    Exit Sub
        'End If
        If Double.TryParse(txtCst.Text, cgst) Then
        Else
            lblMsg.Text = "Invalid CGST"
            Exit Sub
        End If
        If Double.TryParse(txtsgst.Text, sgst) Then
        Else
            lblMsg.Text = "Invalid SGST/UTGST"
            Exit Sub
        End If
        If Double.TryParse(txtigst.Text, igst) Then
        Else
            lblMsg.Text = "Invalid IGST"
            Exit Sub
        End If

        'If Double.TryParse(txtOctrai.Text, Octrai) Then
        'Else
        '    lblMsg.Text = "Invalid Octroi"
        '    Exit Sub
        'End If
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        If Double.TryParse(txtDelivery.Text, OnDelivery) Then
        Else
            lblMsg.Text = "Invalid On Delivery"
            Exit Sub
        End If
        'If Double.TryParse(txtWst.Text, Wst) Then
        'Else
        '    lblMsg.Text = "Invalid VAT"
        '    Exit Sub
        'End If
        If Double.TryParse(txtInstallation.Text, Installation) Then
        Else
            lblMsg.Text = "Invalid Installation"
            Exit Sub
        End If
        If Double.TryParse(txtCommissioning.Text, Commissioning) Then
        Else
            lblMsg.Text = "Invalid Commissioning"
            Exit Sub
        End If
        'If Double.TryParse(txtServiceTax.Text, ServiceTax) Then
        'Else
        '    lblMsg.Text = "Invalid ServiceTax"
        '    Exit Sub
        'End If
        If Double.TryParse(txtRetention.Text, Retention) Then
        Else
            lblMsg.Text = "Invalid Retention"
            Exit Sub
        End If
        'If Double.TryParse(txtOthers.Text, Others) Then
        'Else
        '    lblMsg.Text = "Invalid Others"
        '    Exit Sub
        'End If
        If Double.TryParse(txtPayments.Text, Payments) Then
        Else
            lblMsg.Text = "Invalid Payments"
            Exit Sub
        End If
        TotalCost = ItemSubTotal + cgst + igst + sgst
        txtTotalCost.Text = FormatNumber(TotalCost, 2)
        'If TotalCost = 0 Then
        '    lblMsg.Visible = True
        '    lblMsg.Text = "Please enter unit price to calculate the total cost..."
        '    Exit Sub
        'Else
        '    lblMsg.Visible = False
        'End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        'Dim Checking As Boolean = GridChecking()
        'If Checking Then
        '    lblMsg.Visible = True
        '    lblMsg.Text = "Please select Unique Asset Req ID"
        '    Exit Sub
        'End If

        Dim PONumber As String = GeneratePO()
        InsertPOMaster(PONumber)
        If lblMsg.Text <> "" Then
            lblMsg.Visible = True
            Exit Sub
        Else
            InsertPOMasterDetails(PONumber)
            UpdateData(1014)
            send_mail_gen_PO(PONumber)
            Response.Redirect("frmAssetThanks.aspx?RID=" + Request("RID") + "&PO=" + PONumber)
        End If
    End Sub



    'Private Function GridChecking() As Boolean

    '    Dim alreadySeen As List(Of String) = New List(Of String)()
    '    For Each row As GridViewRow In gvItems.Rows
    '        Dim chkSelect1 As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
    '        Dim lblReqId As Label = DirectCast(row.FindControl("lblReqId"), Label)
    '        If chkSelect1.Checked Then
    '            alreadySeen.Add(lblReqId.Text)
    '        End If
    '    Next
    '    Dim count = alreadySeen.GroupBy(Function(item) item).Where(Function(item) item.Count() > 1).Sum(Function(item) item.Count())
    '    'Dim duplicates = alreadySeen.GroupBy(Function(x) x).Where(Function(g) g.Count() > 1).[Select](Function(g) g.Key)
    '    If count > 1 Then
    '        Return True
    '    Else
    '        Return False
    '    End If

    'End Function
















    Private Function GeneratePO() As String


        Dim Po As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GETMAX_AMG_ITEM_PO")
        Dim Sno As Integer = CInt(sp.ExecuteScalar)
        If Session("TENANT") = "Aadhar.dbo" Then
            'Return "AHFL/PO/0000" + CStr(Sno)
            Return "AHFL/" + GetCurrentFinancialYear() + "/0000" + CStr(Sno)
        Else
            Return "AST/PO/0000" + CStr(Sno)

        End If

        'Response.Redirect("frmAssetThanks.aspx?RID=" + GetRequestId() + "&PO=" + Po)
    End Function
    Public Shared Function GetCurrentFinancialYear() As String
        Dim CurrentYear As Integer = DateTime.Today.Year
        Dim PreviousYear As Integer = DateTime.Today.Year - 1
        Dim NextYear As Integer = DateTime.Today.Year + 1
        Dim PreYear As String = PreviousYear.ToString()
        Dim NexYear As String = NextYear.ToString()
        Dim CurYear As String = CurrentYear.ToString()
        Dim FinYear As String = "FY"
        If DateTime.Today.Month > 3 Then
            FinYear = Convert.ToString((FinYear & CurYear) + "-") & NexYear
        Else
            FinYear = Convert.ToString((FinYear & PreYear) + "-") & CurYear
        End If
        Return FinYear.Trim()
    End Function

    Private Sub InsertPOMaster(ByVal PO As String)
        Dim FFIP As Double = 0
        Dim Advance As Double = 0
        Dim cgst As Double = 0
        Dim igst As Double = 0
        Dim sgst As Double = 0
        Dim OnDelivery As Double = 0
        Dim Wst As Double = 0
        Dim Installation As Double = 0
        Dim Octrai As Double = 0
        Dim Commissioning As Double = 0
        Dim ServiceTax As Double = 0
        Dim Retention As Double = 0
        Dim Others As Double = 0
        Dim Payments As Double = 0
        Dim TotalCost As Double = 0
        'If Double.TryParse(txtFFIP.Text, FFIP) Then
        'Else
        '    lblMsg.Text = "Invalid FFIP"
        '    Exit Sub
        'End If
        If Double.TryParse(txtCst.Text, cgst) Then
        Else
            lblMsg.Text = "Invalid cgst"
            Exit Sub
        End If
        If Double.TryParse(txtsgst.Text, sgst) Then
        Else
            lblMsg.Text = "Invalid SGST"
            Exit Sub
        End If
        If Double.TryParse(txtigst.Text, igst) Then
        Else
            lblMsg.Text = "Invalid IGST"
            Exit Sub
        End If
        'If Double.TryParse(txtOctrai.Text, Octrai) Then
        'Else
        '    lblMsg.Text = "Invalid Octroi"
        '    Exit Sub
        'End If
        If Double.TryParse(txtAdvance.Text, Advance) Then
        Else
            lblMsg.Text = "Invalid Advance"
            Exit Sub
        End If
        If Double.TryParse(txtDelivery.Text, OnDelivery) Then
        Else
            lblMsg.Text = "Invalid On Delivery"
            Exit Sub
        End If
        'If Double.TryParse(txtWst.Text, Wst) Then
        'Else
        '    lblMsg.Text = "Invalid Wst"
        '    Exit Sub
        'End If
        If Double.TryParse(txtInstallation.Text, Installation) Then
        Else
            lblMsg.Text = "Invalid Installation"
            Exit Sub
        End If
        If Double.TryParse(txtCommissioning.Text, Commissioning) Then
        Else
            lblMsg.Text = "Invalid Commissioning"
            Exit Sub
        End If
        'If Double.TryParse(txtServiceTax.Text, ServiceTax) Then
        'Else
        '    lblMsg.Text = "Invalid ServiceTax"
        '    Exit Sub
        'End If
        If Double.TryParse(txtRetention.Text, Retention) Then
        Else
            lblMsg.Text = "Invalid Retention"
            Exit Sub
        End If
        'If Double.TryParse(txtOthers.Text, Others) Then
        'Else
        '    lblMsg.Text = "Invalid Others"
        '    Exit Sub
        'End If
        If Double.TryParse(txtPayments.Text, Payments) Then
        Else
            lblMsg.Text = "Invalid Payments"
            Exit Sub
        End If
        Dim ExchangeCharges As Double = 0
        If Double.TryParse(txtExchange.Text, ExchangeCharges) Then
            lblMsg.Text = ""
        Else
            lblMsg.Text = "Invalid Exchange Charges"
            Exit Sub
        End If
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEM_PO_Insert")
        sp.Command.AddParameter("@AIP_PO_ID", PO, DbType.String)
        sp.Command.AddParameter("@AIP_APP_AUTH", Session("UID"), DbType.String)
        sp.Command.AddParameter("@AIP_APP_DATE", txtDOD.Text, DbType.DateTime)
        sp.Command.AddParameter("@AIP_APP_REM", txtRemarks.Text, DbType.String)
        sp.Command.AddParameter("@AIP_ITM_TYPE", "", DbType.String)
        sp.Command.AddParameter("@AIP_PO_TYPE", "", DbType.String)
        If Rbtwith.Checked = True Then
            sp.Command.AddParameter("@AIPD_VENDORName", ddlVendor.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@AVR_ID", ddlVendor.SelectedItem.Value, DbType.String)
        Else
            sp.Command.AddParameter("@AIPD_VENDORName", ddlVendor2.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@AVR_ID", ddlVendor2.SelectedItem.Value, DbType.String)
        End If

        sp.Command.AddParameter("@AIPD_TOTALCOST", Double.Parse(Trim(txtTotalCost.Text)), DbType.Double)
        sp.Command.AddParameter("@AIPD_PLI", Advance, DbType.String)
        sp.Command.AddParameter("@AIPD_CGST", cgst, DbType.Double)
        sp.Command.AddParameter("@AIPD_SGST", sgst, DbType.Double)
        sp.Command.AddParameter("@AIPD_IGST", igst, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_ADVANCE", Advance, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_ONDELIVERY", OnDelivery, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_INSTALLATION", Installation, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_COMMISSION", Commissioning, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_RETENTION", Retention, DbType.Double)
        sp.Command.AddParameter("@AIPD_PAY_OTHERS", Payments, DbType.Double)
        sp.Command.AddParameter("@AIPD_EXPECTED_DATEOF_DELIVERY", txtDOD.Text, DbType.DateTime)
        sp.Command.AddParameter("@AIPD_EXCHANGE_CHARGES", ExchangeCharges, DbType.Double)
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@QREFID", txtQutref.Text, DbType.String)
        sp.Command.AddParameter("@QDATE", txtQutdate.Text, DbType.String)
        sp.Command.AddParameter("@STATE", ddlState.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@STATE_GST", txtStateGST.Text, DbType.String)
        sp.Command.AddParameter("@STATE_ADDR", txtSAddress.Text, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub InsertPOMasterDetails(ByVal PO As String)
        For Each row As GridViewRow In gvItems.Rows
            Dim txtStockQty As TextBox = DirectCast(row.FindControl("txtStockQty"), TextBox)
            Dim lblProductId As Label = DirectCast(row.FindControl("lblProductId"), Label)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim txtPurchaseQty As TextBox = DirectCast(row.FindControl("txtPurchaseQty"), TextBox)
            Dim txtreqloc As Label = DirectCast(row.FindControl("lblReqLoc"), Label)
            Dim txtUnitPrice As TextBox = DirectCast(row.FindControl("txtUnitPrice"), TextBox)
            Dim lblReqId As Label = DirectCast(row.FindControl("lblReqId"), Label)
            Dim chkSelect1 As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
            Dim unitprice As Double = 0
            Dim PurchaseQty As Integer = 0
            Double.TryParse(Trim(txtUnitPrice.Text), unitprice)
            Integer.TryParse(Trim(txtPurchaseQty.Text), PurchaseQty)
            If chkSelect1.Checked Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AMG_ITEMPO_DETAILS_Insert")
                sp.Command.AddParameter("@AIPD_PO_ID", PO, DbType.String)
                sp.Command.AddParameter("@AIPD_AST_CODE", lblProductId.Text, DbType.String)
                sp.Command.AddParameter("@AIPD_QTY", CInt(txtQty.Text), DbType.Int32)
                sp.Command.AddParameter("@AIPD_ITMREQ_ID", lblReqId.Text, DbType.String)
                sp.Command.AddParameter("@AIPD_ACT_QTY", PurchaseQty, DbType.Int32)
                sp.Command.AddParameter("@AIPD_RATE", txtUnitPrice.Text, DbType.Double)
                sp.Command.AddParameter("@TERMS", txtTerms.Text, DbType.String)
                sp.Command.AddParameter("@PO_REQ_LOC", txtreqloc.Text, DbType.String)
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                sp.ExecuteScalar()
            End If
        Next
    End Sub

    Private Sub UpdateData(ByVal StatusId As Integer)
        For Each row As GridViewRow In gvItems.Rows
            Dim lblReqId As Label = DirectCast(row.FindControl("lblReqId"), Label)
            Dim lblProductid As Label = DirectCast(row.FindControl("lblProductid"), Label)
            Dim chkSelect1 As CheckBox = DirectCast(row.FindControl("chkSelect1"), CheckBox)
            If chkSelect1.Checked Then
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_AssetRequisitionStatus_UpdateByReqIdPOApproved")
                sp.Command.AddParameter("@ReqId", lblReqId.Text, DbType.String)
                sp.Command.AddParameter("@StatusId", StatusId, DbType.Int32)
                sp.Command.AddParameter("@ProductId", lblProductid.Text, DbType.String)
                sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                sp.ExecuteScalar()
            End If
        Next
    End Sub

    Public Sub send_mail_gen_PO(ByVal POnum As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO_GENERATE")
        sp.Command.AddParameter("@REQ_ID", POnum, DbType.String)
        sp.Execute()
    End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

    Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendor.SelectedIndexChanged
        If ddlVendor.SelectedIndex > 0 Then
            BindRequisition()
            gvItems.Visible = True
            gvvendorreqs.Visible = True
            txtTotalCost.Text = ""

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_VENDOR_DETAILS")
            sp.Command.AddParameter("@VENDOR", ddlVendor.SelectedItem.Value, DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            If ds.Tables(0).Rows.Count > 0 Then
                txtAdd.Text = ds.Tables(0).Rows(0).Item("AVR_ADDR")
                txtNumber.Text = ds.Tables(0).Rows(0).Item("MOBILE")
            End If

        Else
            gvItems.Visible = False
            txtTotalCost.Text = ""
        End If
    End Sub

    Protected Sub chkSelect_CheckedChanged(sender As Object, e As EventArgs)

        Dim ds As DataSet
        Dim dt As New DataTable
        For Each row As GridViewRow In gvvendorreqs.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim lblReqID As Label = TryCast(row.FindControl("lblReqID"), Label)
            If chkSelect.Checked Then
                If Rbtwith.Checked = True Then
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GetPODetailsBy_ReqId")
                    sp.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                    ds = sp.GetDataSet
                    dt.Merge(ds.Tables(0))
                Else
                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_Get_WITHOUTPODetailsBy_ReqId")
                    sp.Command.AddParameter("@ReqId", lblReqID.Text, DbType.String)
                    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session("COMPANYID"), DbType.String)
                    ds = sp.GetDataSet
                    dt.Merge(ds.Tables(0))
                End If
            End If
        Next
        gvItems.Visible = True
        gvItems.DataSource = dt
        gvItems.DataBind()
        pnlItems.Visible = True

    End Sub

    Protected Sub ddlTax_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTax.SelectedIndexChanged
        If ddlTax.SelectedValue = "Exclusive" Then
            gst.Visible = True
            gst1.Visible = True
        Else
            gst.Visible = False
            gst1.Visible = False
        End If
    End Sub

    Protected Sub ddlVendor2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendor2.SelectedIndexChanged

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_VENDOR_DETAILS")
        sp.Command.AddParameter("@VENDOR", ddlVendor2.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            txtAdd.Text = ds.Tables(0).Rows(0).Item("AVR_ADDR")
            txtNumber.Text = ds.Tables(0).Rows(0).Item("MOBILE")
        End If
    End Sub

    Protected Sub ddlState_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlState.SelectedIndexChanged
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_STATE_DETAILS")
        sp.Command.AddParameter("@STATE", ddlState.SelectedItem.Value, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            txtStateGST.Text = ds.Tables(0).Rows(0).Item("AM_ST_GSTN")
            txtSAddress.Text = ds.Tables(0).Rows(0).Item("AM_ST_ADDRESS")
        End If
    End Sub
End Class
