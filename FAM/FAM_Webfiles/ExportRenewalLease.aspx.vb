﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Partial Class FAM_FAM_Webfiles_ExportRenewalLease
    Inherits System.Web.UI.Page
    Dim digit(19) As String
    Dim tys(9) As String
    Dim other(4) As String
    Dim Words As String
    Dim mNos(99) As String
    Public param() As SqlParameter
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Protected Function GetUrl(ByVal imagepath As String) As String

        Dim splits As String() = Request.Url.AbsoluteUri.Split("/"c)
        If splits.Length >= 2 Then
            Dim url As String = splits(0) & "//"
            For i As Integer = 2 To i = 6
                url += splits(i)
                url += "/"
            Next
            Return url + imagepath
        End If
        Return imagepath
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not Page.IsPostBack Then
            Dim rid As String
            rid = Request.QueryString("rid")
            lblpoids.Value = rid
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@REQ_ID", SqlDbType.NVarChar, 200)
            param(0).Value = rid
            Dim ds As New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("LEASE_EXTENSION_RDLC", param)
            Dim rds As New ReportDataSource()
            rds.Name = "LeaseExtnsion"
            rds.Value = ds.Tables(0)
            ReportViewer1.Reset()
            ReportViewer1.LocalReport.DataSources.Add(rds)
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/LeaseRenewalExport.rdlc")
            Dim ci As New CultureInfo(Session("userculture").ToString())
            Dim nfi As NumberFormatInfo = ci.NumberFormat
            ReportViewer1.LocalReport.EnableExternalImages = True
            Dim imagePath As String = BindLogo()
            'Dim parameter As New ReportParameter("ImagePath", imagePath)
            'ReportViewer1.LocalReport.SetParameters(parameter)
            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.SizeToReportContent = True
            ReportViewer1.Visible = True
        End If

    End Sub

    Public Function BindLogo() As String
        'Dim imagePath As String
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Update_Get_LogoImage")
        sp3.Command.AddParameter("@type", "2", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp3.Command.AddParameter("@Tenant", Session("TENANT"), DbType.String)
        Dim ds3 As DataSet = sp3.GetDataSet()
        If ds3.Tables(0).Rows.Count > 0 Then
            Return "https://live.quickfms.com/BootStrapCSS/images/" & ds3.Tables(0).Rows(0).Item("IMAGENAME")

        Else
            Return "https://live.quickfms.com/BootStrapCSS/images/yourlogo.png"
        End If
        'Return New Uri(Server.MapPath(imagePath)).AbsoluteUri
    End Function


End Class

