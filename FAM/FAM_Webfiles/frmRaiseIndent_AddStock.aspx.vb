﻿Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions

Partial Class FAM_FAM_Webfiles_frmRaiseIndent_AddStock
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Dim ReqId As String
    Dim CatId As String
    Dim asstsubcat As String
    Dim asstbrand As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        RegExpRemarks.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            lblMsg.Text = ""
            If Not IsPostBack Then

                Dim UID As String = Session("uid")
                'Get_Location()
                Dim ReqId As String = Request("RID")
                If String.IsNullOrEmpty(ReqId) Then
                    btnsubmit.Visible = True
                    pnlItems.Visible = False
                    BindUsers(UID)
                    'getassetcategory()
                    BindCategories()
                    getassetsubcategory()
                    BindLocation()
                    ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
                    ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))

                Else
                    BindUsers(UID)
                    '  getassetcategory()
                    BindCategories()
                    BindBasicReqDetails()
                End If
                fillgrid()
                gvItems.Visible = True
                pnlItems.Visible = True
                remarksAndSubmitBtn.Visible = True
            End If
        End If
    End Sub

    Private Sub BindUsers(ByVal aur_id As String)

        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = aur_id
        param(1) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 10)
        param(1).Value = Session("COMPANYID")
        ObjSubsonic.Binddropdown(ddlEmp, "AM_AMT_BINDUSERS_SP", "NAME", "AUR_ID", param)

        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_bindUsers_SP")
        'ddlEmp.DataSource = sp.GetReader
        'ddlEmp.DataTextField = "Name"
        'ddlEmp.DataValueField = "AUR_ID"
        'ddlEmp.DataBind()
        Dim li As ListItem = ddlEmp.Items.FindByValue(aur_id)
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub

    Private Sub BindCategories()
        'GetChildRows("0")
        'ddlAstCat.Items.Insert(0, New ListItem("--Select--", "0"))
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETCATEGORIESS")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ' ddlAstCat.Items.Insert(0, "--Select--")
    End Sub

    Private Sub getassetsubcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
        pnlItems.Visible = False
        remarksAndSubmitBtn.Visible = False
    End Sub

    Private Sub getbrandbycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", ddlAstSubCat.SelectedItem.Value, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        'ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        pnlItems.Visible = False
        remarksAndSubmitBtn.Visible = False
    End Sub
   
    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_GET_CONSUMABLES_FOR_ASSETGRID_PO")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_MODID", ddlAstModel.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_LOC_ID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        gvItems.DataSource = sp.GetDataSet()
        gvItems.DataBind()
        If gvItems.Rows.Count = 0 Then
            'remarksAndSubmitBtn.Visible = False
            remarksAndSubmitBtn.Style.Add("display", "none")
        Else
            remarksAndSubmitBtn.Style.Add("display", "visible")
        End If

    End Sub

    Protected Sub ddlAstCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstCat.SelectedIndexChanged
        getassetsubcategory()
        
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        If ddlAstCat.SelectedIndex > 0 Then
            getbrandbycatsubcat()
        End If

    End Sub

    
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        ReqId = GenerateRequestId()
        Validate(ReqId)
    End Sub

    Private Function GenerateRequestId() As String
        Dim ReqId As String = ""
        'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_AMG_CONSUMABLE_REQUISITION_GETMAX_AIR_SNO")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AMG_ITEM_REQUISITION_GetMaxAIR_SNO")
        Dim Id As Integer = CInt(sp.ExecuteScalar())
        Return Session("UID") + "/CONS/REQ/" + CStr(Id) + "_pur"
    End Function

    Private Sub Validate(ByVal ReqId As String)
        Dim count As Integer = 0
        Dim Message As String = String.Empty
        Dim cat1 As String = String.Empty
        Dim subcat1 As String = String.Empty
        Dim brand1 As String = String.Empty
        Dim Model1 As String = String.Empty

        Dim VT_Code As String
        Dim Ast_SubCat_Code As String
        Dim manufactuer_code As String
        Dim Ast_Md_code As String

        For Each row As GridViewRow In gvItems.Rows
            Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
            Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
            Dim lbltotal As Label = DirectCast(row.FindControl("lbltotal"), Label)
            Dim lblCat As Label = DirectCast(row.FindControl("lblCat"), Label)
            Dim lblSubCat As Label = DirectCast(row.FindControl("lblSubCat"), Label)
            Dim lblBrand As Label = DirectCast(row.FindControl("lblBrand"), Label)
            Dim lblITemName As Label = DirectCast(row.FindControl("AST_MD_CODE"), Label)

            cat1 = lblCat.Text
            subcat1 = lblSubCat.Text
            brand1 = lblBrand.Text
            Model1 = lblITemName.Text

            If chkSelect.Checked Then
                count = count + 1
                Exit For
            End If
        Next
        If count > 0 Then
            'InsertData(ReqId, txtRem.Text, cat1, subcat1, brand1, Model1)
            InsertData(ReqId, txtRem.Text, ddlAstCat.SelectedValue, ddlAstSubCat.SelectedValue, ddlAstBrand.SelectedValue, ddlAstModel.SelectedValue, ddlLocation.SelectedValue)
            For Each row As GridViewRow In gvItems.Rows
                Dim chkSelect As CheckBox = DirectCast(row.FindControl("chkSelect"), CheckBox)
                Dim lblITEMCODE As Label = DirectCast(row.FindControl("lblITEMCODE"), Label)
                Dim txtQty As TextBox = DirectCast(row.FindControl("txtQty"), TextBox)
                Dim lblCat As Label = DirectCast(row.FindControl("lblCat"), Label)
                Dim lblSubCat As Label = DirectCast(row.FindControl("lblSubCat"), Label)
                Dim lblBrand As Label = DirectCast(row.FindControl("lblBrand"), Label)
                Dim lblMinOrdQty As Label = DirectCast(row.FindControl("lblMinOrdQty"), Label)
                Dim lblMinStkQty As Label = DirectCast(row.FindControl("lblMinStkQty"), Label)
                'Dim lblITEMCODE As Label = DirectCast(row.FindControl("lblITEMCODE"), Label)
                Dim AST_MD_NAME As Label = DirectCast(row.FindControl("AST_MD_NAME"), Label)
                Dim lbltotal As Label = DirectCast(row.FindControl("lbltotal"), Label)


                Dim lbl_vt_code As Label = DirectCast(row.FindControl("lbl_vt_code"), Label)
                Dim lbl_ast_subcat_code As Label = DirectCast(row.FindControl("lbl_ast_subcat_code"), Label)
                Dim lbl_manufactuer_code As Label = DirectCast(row.FindControl("lbl_manufactuer_code"), Label)
                Dim lbl_ast_md_code As Label = DirectCast(row.FindControl("lbl_ast_md_code"), Label)

                VT_Code = lbl_vt_code.Text
                Ast_SubCat_Code = lbl_ast_subcat_code.Text
                manufactuer_code = lbl_manufactuer_code.Text
                Ast_Md_code = lbl_ast_md_code.Text


                If chkSelect.Checked Then
                    If String.IsNullOrEmpty(txtQty.Text) Then
                        lblMsg.Text = "Please enter Quantity for Selected Checkbox"
                        Exit Sub
                    ElseIf IsNumeric(txtQty.Text) = True Then
                        If CInt(lblMinOrdQty.Text) <= CInt(txtQty.Text) Then
                            count = count + 1
                            If count > 0 Then
                                InsertDetails(ReqId, lblITEMCODE.Text, CInt(Trim(txtQty.Text)), VT_Code, Ast_SubCat_Code, manufactuer_code, Ast_Md_code)
                                UpdateAssetRequisitionData(ReqId, Trim(lblITEMCODE.Text), CInt(Trim(lbltotal.Text)), CInt(Trim(txtQty.Text)))
                            End If
                        Else
                            lblMsg.Text = "Minimum Qty. should be  " + lblMinOrdQty.Text + " for " + AST_MD_NAME.Text
                            Dim param(0) As SqlParameter
                            param(0) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
                            param(0).Value = ReqId
                            ObjSubsonic.GetSubSonicExecute("AM_AST_ROLLBACK_REQUISITION", param)
                            Exit Sub
                        End If
                    Else
                        lblMsg.Text = "Please Enter Quantity in Numerics!"
                        Exit Sub
                    End If

                End If
            Next
            send_mail_po(ReqId)
            Response.Redirect("frmAssetThanks.aspx?RID=" + ReqId)
        ElseIf count = 0 Then
            lblMsg.Text = "Sorry! Request Has not been Raised You haven't select any Products and make Quantity more than Zero"
        Else
            lblMsg.Text = "Please enter Quantity for Selected Checkbox"
        End If
    End Sub

    Private Sub InsertData(ByVal ReqId As String, ByVal remarks As String, ByVal cat1 As String, ByVal subcat1 As String, ByVal brand1 As String, ByVal Model1 As String, ByVal Loc As String)
        'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AST_ITEMREQUISTION_ADDNEW")
        'sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
        'sp1.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        'sp1.Command.AddParameter("@Remarks", remarks, DbType.String)

        'sp1.ExecuteScalar()
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_CONSUMABLES_AddNew")
        sp1.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp1.Command.AddParameter("@AurId", Session("UID"), DbType.String)
        sp1.Command.AddParameter("@Remarks", remarks, DbType.String)
        sp1.Command.AddParameter("@CatId", cat1, DbType.String)
        sp1.Command.AddParameter("@AIR_ITEM_SUBCAT", subcat1, DbType.String)
        sp1.Command.AddParameter("@AIR_ITEM_BRD", brand1, DbType.String)
        sp1.Command.AddParameter("@AIR_ITEM_MOD", Model1, DbType.String)
        sp1.Command.AddParameter("@AIR_REQ_LOC", Loc, DbType.String)
        sp1.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp1.ExecuteScalar()
    End Sub

    Private Sub InsertDetails(ByVal ReqId As String, ByVal item As String, ByVal qty As Integer, ByVal VT_CODE As String,
        ByVal AST_SUBCAT_CODE As String, ByVal manufactuer_code As String, ByVal AST_MD_CODE As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AssetRequisitionDetails_AddNew")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@productid", item, DbType.String)
        sp.Command.AddParameter("@Qty", qty, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@ITEM_TYPE", VT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_SUBCAT", AST_SUBCAT_CODE, DbType.String)
        sp.Command.AddParameter("@ITEM_BRD", manufactuer_code, DbType.String)
        sp.Command.AddParameter("@ITEM_MOD", AST_MD_CODE, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Private Sub UpdateAssetRequisitionData(ByVal ReqId As String, ByVal ProductId As String, ByVal StockQty As Integer, ByVal PurchaseQty As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AssetRequisitionDetails_UpdateStockPurchaseQty")
        sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
        sp.Command.AddParameter("@ProductId", ProductId, DbType.String)
        sp.Command.AddParameter("@SQty", StockQty, DbType.Int32)
        sp.Command.AddParameter("@PQty", PurchaseQty, DbType.Int32)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.ExecuteScalar()
    End Sub

    Public Sub send_mail_po(ByVal ReqId As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SEND_MAIL_ASSET_REQUISITION_PO")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        sp.Execute()
    End Sub

    Protected Sub gvItem_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub
    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAstCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlAstModel.DataSource = sp.GetDataSet()
        ddlAstModel.DataTextField = "AST_MD_NAME"
        ddlAstModel.DataValueField = "AST_MD_CODE"
        ddlAstModel.DataBind()
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
        End Sub

    Private Sub BindLocation()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        ddlLocation.Items.Remove("--Select--")
        ddlLocation.SelectedIndex = If(ddlLocation.Items.Count > 1, 0, 0)
    End Sub

    Private Sub BindLocation1()
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@dummy", SqlDbType.NVarChar, 100)
        param(0).Value = "1"
        param(1) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 100)
        param(1).Value = Session("UID")
        ObjSubsonic.Binddropdown(ddlLocation, "GET_LOCTION", "LCM_NAME", "LCM_CODE", param)
        'ddlLocation.Items.Remove("--Select--")

    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        ' If ddlAstCat.SelectedIndex > 0 Then
        getmakebycatsubcat()
        '  End If
    End Sub


    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        gvItems.Visible = True
        pnlItems.Visible = True
        remarksAndSubmitBtn.Visible = True
        fillgrid()
    End Sub
    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GETCHECK_CONSUMABLES")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlAstCat.DataSource = sp.GetDataSet()
        ddlAstCat.DataTextField = "VT_TYPE"
        ddlAstCat.DataValueField = "VT_CODE"
        ddlAstCat.DataBind()
        ddlAstCat.Items.Insert(0, "--Select--")
        ' ddlAstCat.SelectedIndex = If(ddlAstCat.Items.Count > 1, 0, 0)
    End Sub

    Private Sub BindBasicReqDetails()
        Dim ReqId As String = Request("RID")
        If String.IsNullOrEmpty(ReqId) Then
            'lblMsg.Text = "No such requisition found."
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_AMG_ITEM_REQUISITION_GetByReqId_NP")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            Dim dr As SqlDataReader = sp.GetReader()
            If dr.Read() Then

                Dim RaisedBy As String
                RaisedBy = dr("AIR_AUR_ID")
                ddlEmp.ClearSelection()
                Dim li As ListItem = ddlEmp.Items.FindByValue(CStr(RaisedBy))
                If Not li Is Nothing Then
                    li.Selected = True
                End If

                CatId = dr("AIR_ITEM_TYPE")
                ddlAstCat.ClearSelection()
                li = ddlAstCat.Items.FindByValue(CStr(CatId))

                If Not li Is Nothing Then
                    li.Selected = True
                End If
                ddlAstCat.Enabled = True


                getassetsubcategory(CatId)

                asstsubcat = dr("AIR_ITEM_SUBCAT")
                ddlAstSubCat.ClearSelection()
                ddlAstSubCat.Items.FindByValue(asstsubcat).Selected = True

                getbrandbycatsubcat(CatId, asstsubcat)

                asstbrand = dr("AIR_ITEM_BRD")

                getmakebycatsubcat()
                ddlAstBrand.ClearSelection()
                ddlAstBrand.Items.FindByValue(asstbrand).Selected = True
                ddlAstBrand.Enabled = True


                Dim astmodel = dr("AIR_ITEM_MOD")
                ddlAstModel.ClearSelection()
                ddlAstModel.Items.FindByValue(astmodel).Selected = True

                BindLocation1()

                Dim LOC = dr("AIR_REQ_LOC")
                ddlLocation.ClearSelection()
                ddlLocation.Items.FindByValue(LOC).Selected = True

            End If
        End If
    End Sub
    Private Sub getassetsubcategory(ByVal assetcode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_SUBCATBYVENDORS")
        sp.Command.AddParameter("@VT_CODE", assetcode, DbType.String)
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub
    Private Sub getbrandbycatsubcat(ByVal assetcode As String, ByVal assetsubcat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", assetcode, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", assetsubcat, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ' ddlAstBrand.Items.Insert(0, "--Select--")
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

    Protected Sub btnclear_Click(sender As Object, e As EventArgs) Handles btnclear.Click
        cleardata()
    End Sub
    Public Sub cleardata()
        ddlastCat.ClearSelection()
        ddlastsubCat.ClearSelection()
        ddlLocation.ClearSelection()
        pnlItems.Visible = False
        remarksAndSubmitBtn.Visible = False
        getassetsubcategory()
        ddlAstBrand.Items.Clear()
        ddlAstModel.Items.Clear()
        ddlAstBrand.Items.Insert(0, New ListItem("--All--", "All"))
        ddlAstModel.Items.Insert(0, New ListItem("--All--", "All"))
    End Sub

End Class
