<%@ Page EnableEventValidation="false" Language="VB" MaintainScrollPositionOnPostback="true" AutoEventWireup="false" CodeFile="frmAddAsset.aspx.vb" Inherits="FAM_Masters_Mas_WebFiles_frmAddAsset" Title="Add Asset" %>

<%@ Register Src="../../../Controls/AddAsset.ascx" TagName="AddAsset" TagPrefix="uc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Add Capital Asset</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <uc1:AddAsset ID="AddAsset1" runat="server" />
                </form>
            </div>
        </div>
    </div>
</body>
</html>
