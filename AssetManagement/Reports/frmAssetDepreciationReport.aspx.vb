﻿Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports Microsoft.Reporting.WebForms
Imports System.Globalization
Imports System.ComponentModel

Partial Class AssetManagement_Reports_frmAssetDepreciationReport
    Inherits System.Web.UI.Page

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindLocation()
            getassetcategory()
            FillCompanies()
            TryCast(ddlAssetCategory, IPostBackDataHandler).RaisePostDataChangedEvent()
            BindGridView(Nothing)
        End If
    End Sub

    Protected Sub ddllocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllocation.SelectedIndexChanged

    End Sub

    Private Sub FillCompanies()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COMPANIES")
        ddlCompany.DataSource = sp.GetReader
        ddlCompany.DataTextField = "CNP_NAME"
        ddlCompany.DataValueField = "CNP_ID"
        ddlCompany.DataBind()
    End Sub

    Public Sub BindGridView(ByVal gv As GridView)
        Dim AstCat As String = ""
        If ddlAssetCategory.SelectedValue = "--All--" Then
            AstCat = ""
        Else
            AstCat = ddlAssetCategory.SelectedValue
        End If

        Dim AstSubCat As String = ""
        If ddlAstSubCat.SelectedValue = "--All--" Then
            AstSubCat = ""
        Else
            AstSubCat = ddlAstSubCat.SelectedValue
        End If

        Dim AstBrand As String = ""
        If ddlAstBrand.SelectedValue = "--All--" Then
            AstBrand = ""
        Else
            AstBrand = ddlAstBrand.SelectedValue
        End If

        Dim AstModel As String = ""
        If ddlModel.SelectedValue = "--All--" Then
            AstModel = ""
        Else
            AstModel = ddlModel.SelectedValue
        End If

        Dim Location As String = ""
        If ddllocation.SelectedValue = "--All--" Then
            Location = ""
        Else
            Location = ddllocation.SelectedValue
        End If
        Dim frmDate As String = FromDate.Text


        Dim toDate As String = ""
        toDate = txtToDate.Text

        Dim param(8) As SqlParameter

        param(0) = New SqlParameter("@Category", SqlDbType.NVarChar, 200)
        param(0).Value = AstCat
        param(1) = New SqlParameter("@SubCategory", SqlDbType.NVarChar, 200)
        param(1).Value = AstSubCat
        param(2) = New SqlParameter("@Brand", SqlDbType.NVarChar, 200)
        param(2).Value = AstBrand
        param(3) = New SqlParameter("@Model", SqlDbType.NVarChar, 200)
        param(3).Value = AstModel
        param(4) = New SqlParameter("@LOCCODE", SqlDbType.NVarChar, 200)
        param(4).Value = Location
        param(5) = New SqlParameter("@FromDate", SqlDbType.DateTime, 200)
        param(5).Value = FromDate.Text
        param(6) = New SqlParameter("@ToDate", SqlDbType.DateTime, 200)
        param(6).Value = txtToDate.Text
        param(7) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(7).Value = Session("Uid").ToString
        param(8) = New SqlParameter("@COMPANY", SqlDbType.NVarChar, 5)
        param(8).Value = ddlCompany.SelectedValue

        Dim ds As New DataSet
        ds = ObjSubsonic.GetSubSonicDataSet("AM_GET_DEPRECIATION_REPORT", param)

        Dim rds As New ReportDataSource()
        rds.Name = "DepreciationReportDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/DepreciationReport.rdlc")

        Dim ci As New CultureInfo(Session("userculture").ToString())
        Dim nfi As NumberFormatInfo = ci.NumberFormat
        Dim p2 As New ReportParameter("CurrencyParam", nfi.CurrencySymbol())

        ReportViewer1.LocalReport.SetParameters(p2)
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        ReportViewer1.LocalReport.EnableHyperlinks = True
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESSALL")
        sp.Command.AddParameter("@dummy", 1, DbType.String)
        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        'ddlAssetCategory.Items.Insert(0, "--All--")
        ddlAssetCategory.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub getsubcategorybycat(ByVal categorycode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"))
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        TryCast(ddlAstBrand, IPostBackDataHandler).RaisePostDataChangedEvent()
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub BindLocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation") '@USER_ID
        sp.Command.AddParameter("@USER_ID", Session("Uid"), DbType.String)
        ddllocation.DataSource = sp.GetReader
        ddllocation.DataTextField = "LCM_NAME"
        ddllocation.DataValueField = "LCM_CODE"
        ddllocation.DataBind()
        ddllocation.Items.Insert(0, New ListItem("--All--", "ALL"))
    End Sub

    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
        TryCast(ddlAstSubCat, IPostBackDataHandler).RaisePostDataChangedEvent()
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        getmakebycatsubcat()
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        BindGridView(Nothing)
    End Sub

End Class
