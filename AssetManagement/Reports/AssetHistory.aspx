﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssetHistory.aspx.cs" Inherits="AssetManagement_Reports_ViewPODetails" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%----%>
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .word {
            color: #4813CA;
        }

        .pdf {
            color: #FF0023;
        }

        .excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }


        .with-nav-tabs.panel-primary .nav-tabs > .open > a,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
            background-color: #3071a9;
            border-color: transparent;
        }

        .with-nav-tabs.panel-primary .nav-tabs > li.active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
            color: #428bca;
            background-color: #fff;
            border-color: #428bca;
            border-bottom-color: transparent;
        }
    </style>
</head>

<body data-ng-controller="AssetHistoryController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Asset History</h3>
            </div>
            <div class="card">
                <form id="form1" name="CustomizedReport" data-valid-submit="LoadData()" novalidate>

                    <div class="row">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="FromDate" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                                <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please Select From Date</span>--%>
                            </div>

                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="ToDate" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please Select To Date</span>--%>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.UGC_NAME.$invalid}">
                                <label class="control-label">Status</label>
                                <div isteven-multi-select data-input-model="Status" data-output-model="CustomizedReport.Status" data-button-label="icon STA_TITLE"
                                    data-item-label="icon STA_TITLE maker" data-on-click="GetStatus()" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="CustomizedReport.Status[0]" name="STA_TITLE" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.UGC_NAME.$invalid" style="color: red">Please select status</span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 10px;">
                            <input type="button" value="Search" class='btn btn-primary custom-button-color' data-ng-click="SearchA()" />

                            <%-- <input type="submit" value="Search" class="btn btn-primary custom-button-color" />--%>
                        </div>
                    </div>
                    <div class="row" style="padding-left: 18px">
                        <div class="col-md-12 pull-right">
                            <a data-ng-click="GenReport('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" data-ng-show="DocTypeVisible==0" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>
                    </div>
                    <div id="Tabular">
                        <div class="row" style="padding-bottom: 30px">
                            <div class="col-md-12">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%;" />

                                <%--<input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </div>



                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
    </script>

    <script src="../../SMViews/Utility.js"></script>
    <%--<script src="../../SMViews/Utility.min.js" defer></script>--%>
    <%--<script src="../JS/ASTCustomizedReport.min.js"></script>--%>
    <%--<script src="../JS/AssetHistory.js" defer></script>--%>
    <script src="../JS/AssetHistory.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js"></script>
    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {

            $('#FromDate').datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });


        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
            //$('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            //$('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));

            $('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));
        });

    </script>

</body>
</html>
