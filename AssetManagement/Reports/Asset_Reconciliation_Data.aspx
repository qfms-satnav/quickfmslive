﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
</head>
<body data-ng-controller="Asset_Reconciliation_Controller">
    <div class="container-fluid">
        <div class="p-3 mb-2 bg-body-secondary rounded-3">
            <div class="card ">
                <div class="card-body">
                    <h3 class="card-title">Asset Reconciliation</h3>
                    <hr class="border border-primary border-3 opacity-75">
                    <form id="form1" name="Asset_Reconciliation" data-valid-submit="SubmitData()" novalidate>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <div class="form-group" data-ng-class="{'has-error': Asset_Reconciliation.$submitted && Asset_Reconciliation.LCM_NAME.$invalid}">
                                        <label for="txtcode">Location <span style="color: red;"></span></label>
                                        <div isteven-multi-select data-input-model="Loclist" data-output-model="AssetReccon.Loclist" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                            data-on-item-click="LocChanged()" data-on-select-all="LocChangeAll()" data-on-select-none="LocSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="AssetReccon.Loclist[0]" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="Asset_Reconciliation.$submitted && Asset_Reconciliation.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Asset Category<span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="categorylist" data-output-model="AssetReccon.selectedcat" data-button-label="icon CAT_NAME" data-item-label="icon CAT_NAME maker"
                                        data-on-item-click="CatChanged()" data-on-select-all="catSelectAll()" data-on-select-none="catSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="AssetReccon.selectedcat[0]" name="CAT_NAME" style="display: none" />
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">Asset Type<span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="AssetType" data-output-model="AssetReccon.AssetType" data-button-label="icon TYP_NAME" data-item-label="icon TYP_NAME maker"
                                        data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="AssetReccon.AssetType[0]" name="TYP_NAME" style="display: none" />
                                </div>
                            </div>
                             <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Reconcilation Month </label>
                                <div>
                                    <input type="month" id="start" name="start" data-ng-model="AssetReccon.Recon_date" class="form-control " data-live-search="true"
                                        required="required" min="2021" value="06">
                                </div>
                                <input type="text" data-ng-model="AssetReccon.Recon_date" name="Recon_date" style="display: none" required="" />
<%--                                <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.ExpenseMonth.$invalid" style="color: red">Please Select Expense Month </span>--%>
                            </div>
                        </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 text-right search_button_wrap">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <input type="file" id="fileUpl" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <input type="button" value="Upload Asset" data-ng-click="UploadFile()" class="btn btn-primary custom-button-color" />
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <a id="AssetRecon" href="Asset_Recon_Data.xlsx">Click here to view the template</a>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                    </form>
                    <div class="row">
                        <div class="col-xs-12  text-right">
                                                <a data-ng-click="GenReport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                            </div>
                                            <br /><br />
                        <div class="col-xs-12">
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 510px; width: auto"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../../BootStrapCSS/Scripts/leaflet/qrcode.js" defer></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/qr_generator.js" defer></script>
    <script src="../../BlurScripts/BlurJs/jquery.qrcode.min.js" defer></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
     <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <script src="../JS/Asset_Reconciliation.js" defer></script>
</body>
</html>
