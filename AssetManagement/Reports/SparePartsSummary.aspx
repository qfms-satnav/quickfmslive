﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SparePartsSummary.aspx.cs" Inherits="AssetManagement_Reports_SparePartsSummary" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

    </script>
    <%--    <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };
    </script>--%>
    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>


    <style>
        .grid-align {
            text-align: left;
        }


        a:hover {
            cursor: pointer;
        }



        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #fff;
        }

        .ag-body-viewport-wrapper.ag-layout-normal {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #dbdeed;
        }

        .ag-blue .ag-header-cell-label {
            color: black;
        }

        .bxheader {
            width: 100%;
            background-color: black;
            padding: 5px 10px;
            color: #fff;
        }

        .ag-blue .ag-row-odd {
            background-color: #fff;
        }

        .cell-fail {
            text-align: left;
            font-weight: bold;
            background-color: red;
        }

        .cell-pass {
            text-align: left;
            font-weight: bold;
            background-color: #4caf50;
        }

        .cell-less {
            text-align: left;
            font-weight: bold;
            background-color: orange;
        }

        .panel-title {
            color: black;
            font-weight: bold;
        }

        /* .ag-header-cell-resize {
            height: 100%;
            width: 62px;
        }*/
    </style>
</head>
<body data-ng-controller="SparePartsSummaryController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="SpareParts Summary" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Spare Parts Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" name="SparePartsSummary" data-valid-submit="LoadData()" novalidate>
                                <div class="clearfix" style="height: 50px;">

                                    <div class="row">

                                        <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Site</label>
                                                <div isteven-multi-select data-input-model="Locations" data-output-model="SparePartsSummary.Locations" button-label="icon LCM_NAME" data-is-disabled="EnableStatus==0"
                                                    item-label="icon LCM_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1"
                                                    data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()">
                                                </div>
                                                <input type="text" data-ng-model="SparePartsSummary.LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="SparePartsSummary.$submitted && SparePartsSummary.Location.$invalid" style="color: red">Please Select Locaton </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': SparePartsSummary.$submitted && SparePartsSummary.VT_TYPE.$invalid}">
                                               
                                                <label class="control-label">Equipment</label>
                                                <div isteven-multi-select data-input-model="Type" data-output-model="SparePartsSummary.Type" data-button-label="icon SUBC_NAME"
                                                    data-item-label="icon SUBC_NAME maker" data-on-click="GetAssetDetails()" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="SparePartsSummary.Type[0]" name="SUBC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="SparePartsSummary.$submitted && SparePartsSummary.VT_TYPE.$invalid" style="color: red">Please select Equipment</span>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="control-label">Site</label>
                                                <div isteven-multi-select data-input-model="Locations" data-output-model="SparePartsSummary.Locations" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                    data-on-item-click="GetAssetDetails()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                </div>

                                                <input type="text" data-ng-model="SparePartsSummary.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="SparePartsSummary.$submitted && SpareCurrentInventory.Location.$invalid" style="color: red">Please select location </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.SUBC_NAME.$invalid}">

                                                <label class="control-label">Equipment</label>
                                                <span style="color: red;">*</span>
                                                <div isteven-multi-select data-input-model="Type" data-output-model="AssetSpareParts.Type" data-button-label="icon SUBC_NAME"
                                                    data-item-label="icon SUBC_NAME maker" data-on-click="GetAssetDetails()" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                    data-tick-property="ticked" selection-mode="single" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="AssetSpareParts.Type[0]" name="SUBC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.SUBC_NAME.$invalid" style="color: red">Please select equipment</span>
                                            </div>
                                        </div>
                                        <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': SparePartsSummary.$submitted && SparePartsSummary.SUBC_NAME.$invalid}">

                                                <label class="control-label">Equipment</label>
                                                <div isteven-multi-select data-input-model="Type" data-output-model="SparePartsSummary.Type" data-button-label="icon SUBC_NAME"
                                                    data-item-label="icon SUBC_NAME" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"selection-mode="single"
                                                    data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="SparePartsSummary.Type[0]" name="SUBC_NAME" style="display: none" required="" />
                                                <span class="error" data-ng-show="SparePartsSummary.$submitted && SparePartsSummary.SUBC_NAME.$invalid" style="color: red">Please select Equipment</span>
                                            </div>
                                        </div>--%>
                                        <div class="col-md-2 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Select Range</label>
                                                <br />
                                               <%-- <select style="width: 100%; height: 30px;" class="form-control" name="Range" id="Range" data-input-model="Range" ng-change="fn_setdate()">
                                                    <option value="0">Select</option>
                                                    <option value="30">Last 30 Days</option>
                                                    <option value="60">Last 60 Days</option>
                                                    <option value="90">Last 90 Days</option>
                                                </select>--%>
                                                 <select data-ng-model="SparePartsSummary.Range" style="width: 100%;  height: 30px;" class="form-control" name="Range" id="Range" data-ng-change="fn_setdate()">
                                                    <option value="0">Select</option>
                                                    <option value="30">Last 30 Days</option>
                                                    <option value="60">Last 60 Days</option>
                                                    <option value="90">Last 90 Days</option>
                                                  </select>  
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">From Date</label>
                                                <div class="input-group date" id='fromdate'>
                                                    <input type="text" class="form-control" data-ng-model="SparePartsSummary.FromDate" id="FromDate" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                                    </span>
                                                </div>
                                                <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please Select From Date</span>--%>
                                            </div>

                                        </div>
                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label for="txtcode">To Date</label>
                                                <div class="input-group date" id='todate'>
                                                    <input type="text" class="form-control" data-ng-model="SparePartsSummary.ToDate" id="ToDate" name="ToDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                                    </span>
                                                </div>
                                                <%--<span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please Select To Date</span>--%>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">

                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">                                               
                                                <br />
                                                <input type="Button" value="Search" class='btn btn-primary custom-button-color' data-ng-click="SearchA()" />
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <span class="btn btn-info">Total Inventory
                            <br>
                            <span class="badge ng-binding">{{SparePartsSummary.Stock || 0}} </span>
                        </span>
                                             
                                            </div>
                                        </div>
                                         <div class="col-md-2 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                 <span class="btn btn-success">Total Cost
                            <br>
                            <span class="badge ng-binding">{{SparePartsSummary.TotCost || 0}} </span>
                        </span>
                                            
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row" style="margin-bottom:10px">
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                           <div class="form-group">

                                            <%--<a data-ng-click="GenReport(OpexFormat,'doc')"><i id="word" data-bs-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-middle"></i></a>--%>
                                            <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                                            <%--<a data-ng-click="GenReport(OpexFormat,'pdf')"><i id="pdf" data-bs-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>
                                        </div>
                                         </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">

                           
                                        <div class="bxheader">
                                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%; display: none;" />
                                            Site Wise Data
                                        </div>
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 250px; width: auto"></div>
                                
                                        <%--  <div class="bxheader">Spare Consumption Cost</div>
                                        <div data-ag-grid="gridOptions3" class="ag-blue" style="height: 310px; width: auto"></div>--%>
                                    </div>
                                </div>
                                <div class="row" style="margin-bottom:10px; margin-top:10px">
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">

                                            <%--<a data-ng-click="GenReport(OpexFormat,'doc')"><i id="word" data-bs-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-middle"></i></a>--%>
                                            <a data-ng-click="GenReport2(OpexFormat,'xls')"><i id="excel2" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                                            <%--<a data-ng-click="GenReport(OpexFormat,'pdf')"><i id="pdf" data-bs-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>
                                        </div>
                                       </div>
                                </div>
                                <div class="row">
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                     <div class="bxheader">Spare Consumption Part</div>
                                        <div data-ag-grid="gridOptions2" class="ag-blue" style="height: 210px; width: auto"></div>
                                </div>
                                    </div>

                                <div class="row" style="margin-bottom:10px; margin-top:10px">
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">

                                            <%--<a data-ng-click="GenReport(OpexFormat,'doc')"><i id="word" data-bs-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-middle"></i></a>--%>
                                            <a data-ng-click="GenReport3(OpexFormat,'xls')"><i id="excel3" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                                            <%--<a data-ng-click="GenReport(OpexFormat,'pdf')"><i id="pdf" data-bs-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>
                                        </div>
                                       </div>
                                </div>
                                <div class="row">
                                     <div class="col-md-12 col-sm-12 col-xs-12">
                                     <div class="bxheader">Site (Total Inventory/Total Cost)</div>
                                        <div data-ag-grid="gridOptions3" class="ag-blue" style="height: 210px; width: auto"></div>
                                </div>
                                    </div>
                                <div style="margin-top:10px;">
                     <a class="btn btn-danger homeButton" href="#" onclick="fnDetail('../../MaintenanceManagement/Views/MaintainanceDashboard.aspx')" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-home" aria-hidden="true"></i>HOME</a>
                                 &nbsp;<a class="btn btn-danger homeButton" href="#" onclick="goBack();" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>
                   </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>



    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/SparePartsSummary.js"></script>
    <script defer type="text/javascript">
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            ////$('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            ////$('#ToDate').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));
            $('#FromDate').datepicker('setDate', new Date(moment().startOf('month').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().endOf('month').format('MM/DD/YYYY')));

        }

        //function fn_setdate() {
        //    if ($('#Range').val() > 0) {
        //        $('#FromDate').val(BackdaysDateOfMonthDDMMYYYY($('#Range').val()));
        //        $('#ToDate').val(CurrentDateOfMonthDDMMYYYY());

        //    }
        //}

        //function BackdaysDateOfMonthDDMMYYYY(range) {          
        //    var curDate = new Date();
        //    var yesterday = new Date();
        //    yesterday.setDate(curDate.getDate() - parseInt(range));          
        //    var _dd = yesterday.getDate();
        //    if (_dd.length == 1)
        //        _dd = "0" + _dd;
        //    return ((yesterday.getMonth() + 1) + "/" + _dd + "/" + yesterday.getFullYear());
        //}
        //function CurrentDateOfMonthDDMMYYYY() {
        //    var curDate = new Date();
        //    var _dd = curDate.getDate();
        //    if (_dd.length == 1)
        //        _dd = "0" + _dd;
        //    return ((curDate.getMonth() + 1) + "/" + _dd + "/" + curDate.getFullYear());
        //}
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
            //$('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            //$('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        });
        function fnDetail(url) {
            //progress(0, 'Loading...', true);
            window.location = url;
        }
        function goBack() {
            //window.history.back();
            var strBackUrl = sessionStorage.getItem("strBackUrl");
            window.location = strBackUrl;
        }
    </script>
</body>
</html>

