﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssetModel.aspx.cs" Inherits="AssetManagement_Reports_AssetModel" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <%--<link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
<link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <%--<link href="Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />--%>
</head>
<body data-ng-controller="AssetModelController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Asset Model Master</h3>
            </div>
            <div class="card">
                <form name="assetmodel" id="form1" ng-submit="Save()" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetmodel.$submitted && assetmodel.AST_MD_CODE.$invalid}">
                                <label>Asset Model Code<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                    <input name="AST_MD_CODE" id="ModelCode" class="form-control" data-ng-model="model.AST_MD_CODE" ng-disabled="isEditMode" required=""/>
                                    <span class="error" data-ng-show="assetmodel.$submitted && assetmodel.AST_MD_CODE.$invalid">Asset Model Code is required.</span>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetmodel.$submitted && assetmodel.AST_MD_NAME.$invalid}">
                                <label>Asset Model Name<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">
                                    <input name="AST_MD_NAME" id="ModelName" class="form-control" data-ng-model="model.AST_MD_NAME" required=""/>
                                    <span class="error" data-ng-show="assetmodel.$submitted && assetmodel.AST_MD_NAME.$invalid">Asset Model Name is required.</span>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetmodel.$submitted && assetmodel.VT_TYPE.$invalid}">
                                <div>

                                    <label>Asset Category<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="AssetMainCategoryList" data-output-model="AssetMainCategoryList.VT_CODE" button-label="icon VT_TYPE"
                                        data-item-label="icon VT_TYPE" data-tick-property="ticked" data-on-item-click="onBindSubcategory()" data-is-disabled="EnableStatus==0"
                                        data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="AssetMainCategoryList.VT_CODE" name="VT_TYPE" style="display: none" required="" />
                                    <span class="error" data-ng-show="assetmodel.$submitted && assetmodel.VT_TYPE.$invalid" style="color: red">Please Select Asset Category </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetmodel.$submitted && assetmodel.AST_SUBCAT_NAME.$invalid}">
                                <label>Asset Subcategory<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AssetSubCategoryList" data-output-model="AssetSubCategoryList.AST_SUBCAT_CODE" button-label="icon AST_SUBCAT_NAME"
                                    data-item-label="icon AST_SUBCAT_NAME" data-tick-property="ticked" data-on-item-click="onBindAssetBrand()" ng-disabled="disabled" data-is-disabled="EnableStatus==0"
                                    data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetSubCategoryList.AST_SUBCAT_CODE" name="AST_SUBCAT_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="assetmodel.$submitted && assetmodel.AST_SUBCAT_NAME.$invalid" style="color: red">Please Select Asset Subcategory </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetmodel.$submitted && assetmodel.manufacturer.$invalid}">
                                <%--<div>--%>
                                <%--<input id="manufacturer" type="hidden" name="manufacturer" data-ng-model="AssetBrandList.manufacturer" autofocus class="form-control" />--%>
                                <label>Asset Brand/Make<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AssetBrandList" data-output-model="AssetBrandList.manufactuer_code" button-label="icon manufacturer"
                                    data-item-label="icon manufacturer" data-tick-property="ticked" ng-disabled="disabled" data-is-disabled="EnableStatus==0"
                                    data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetBrandList.manufactuer_code" name="manufacturer" style="display: none" required="" />
                                <span class="error" data-ng-show="assetmodel.$submitted && assetmodel.manufacturer.$invalid" style="color: red">Please Select Asset Brand/Make</span>
                                <%--</div>--%>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetmodel.$submitted && assetmodel.AST_MD_STAID.$invalid}">
                                <label>Status<span style="color: red;">*</span></label>
                                <div>
                                    <select id="AST_MD_STAID" name="AST_MD_STAID" data-ng-model="model.AST_MD_STAID" class="form-control" data-live-search="true" required="">
                                        <option value="">--Select--</option>
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>

                                    </select>
                                    <span class="error" data-ng-show="assetmodel.$submitted && assetmodel.AST_MD_STAID.$invalid" style="color: red">Please Select status</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                    <textarea name="AST_MD_REM" rows="2" cols="20" id="ModelRemarks" class="form-control" data-ng-model="model.AST_MD_REM" style="height: 30%;">
                                    </textarea>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 22px">
                            <div class="form-group">

                                <input type="submit" name="ModelSubmit" value="{{buttonText}}" class="btn btn-default btn-primary" />
                                <input type="submit" value="Modify" class="btn btn-default btn-primary" data-ng-show="ActionStatus==1" />
                                <a class="btn btn-default btn-primary" href="javascript:history.back()">Back</a>

                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 510px; width: auto;"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <%--<script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>--%>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <!-- AngularJS and other JS libraries -->
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
        var CompanySessionId = '<%= Session["TENANT"]%>';

    </script>
    <%--    <script src="path/to/bootstrap-select.min.js"></script>--%>


    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/AssetModel.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
</body>
</html>
