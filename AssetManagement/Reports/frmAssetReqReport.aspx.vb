Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports Microsoft.Reporting.WebForms

Partial Class frmAssetREQreport
    Inherits System.Web.UI.Page
    Dim ObjSubsonic As New clsSubSonicCommonFunctions
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        Try
            If Not IsPostBack Then
                'BindGrid()
                BindLocation()
                getassetcategory()
                FillCompanies()
                TryCast(ddlAssetCategory, IPostBackDataHandler).RaisePostDataChangedEvent()
                bindgridview()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    'Private Sub BindGrid()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_ASSETREQREPORT_DETAILS")
    '        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)

    Private Sub FillCompanies()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_COMPANIES")
        ddlCompany.DataSource = sp.GetReader
        ddlCompany.DataTextField = "CNP_NAME"
        ddlCompany.DataValueField = "CNP_ID"
        ddlCompany.DataBind()
    End Sub

    Public Sub bindgridview()
        Dim AstCat As String = ""
        If ddlAssetCategory.SelectedValue = "--All--" Then
            AstCat = ""
        Else
            AstCat = ddlAssetCategory.SelectedValue
        End If

        Dim AstSubCat As String = ""
        If ddlAstSubCat.SelectedValue = "--All--" Then
            AstSubCat = ""
        Else
            AstSubCat = ddlAstSubCat.SelectedValue
        End If

        Dim AstBrand As String = ""
        If ddlAstBrand.SelectedValue = "--All--" Then
            AstBrand = ""
        Else
            AstBrand = ddlAstBrand.SelectedValue
        End If

        Dim AstModel As String = ""
        If ddlModel.SelectedValue = "--All--" Then
            AstModel = ""
        Else
            AstModel = ddlModel.SelectedValue
        End If

        Dim Location As String = ""
        If ddlLocation.SelectedValue = "--All--" Then
            Location = ""
        Else
            Location = ddlLocation.SelectedValue
        End If
        Dim frmDate As String = FromDate.Text

        Dim toDate As String = ""
        toDate = txtToDate.Text

        Dim param(8) As SqlParameter

        param(0) = New SqlParameter("@Category", SqlDbType.NVarChar, 200)
        param(0).Value = AstCat
        param(1) = New SqlParameter("@SubCategory", SqlDbType.NVarChar, 200)
        param(1).Value = AstSubCat
        param(2) = New SqlParameter("@Brand", SqlDbType.NVarChar, 200)
        param(2).Value = AstBrand
        param(3) = New SqlParameter("@Model", SqlDbType.NVarChar, 200)
        param(3).Value = AstModel
        param(4) = New SqlParameter("@Location", SqlDbType.NVarChar, 200)
        param(4).Value = Location
        param(5) = New SqlParameter("@FromDate", SqlDbType.DateTime, 200)
        param(5).Value = FromDate.Text
        param(6) = New SqlParameter("@ToDate", SqlDbType.DateTime, 200)
        param(6).Value = txtToDate.Text
        param(7) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(7).Value = Session("Uid").ToString
        param(8) = New SqlParameter("@COMPANYID", SqlDbType.NVarChar, 200)
        param(8).Value = ddlCompany.SelectedValue

        Dim ds As New DataSet
        'ds = sp.GetDataSet()
        ds = ObjSubsonic.GetSubSonicDataSet("AST_GET_VIEW_ITEMREQUISITION_GRID_REPORT", param)

        Dim rds As New ReportDataSource()
        rds.Name = "AssetReqRptDS"
        'This refers to the dataset name in the RDLC file
        rds.Value = ds.Tables(0)
        ReportViewer1.Reset()
        'ReportViewer1.LocalReport.DataSources.Clear()
        ReportViewer1.LocalReport.DataSources.Add(rds)
        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetReqReport.rdlc")
        ReportViewer1.LocalReport.Refresh()
        ReportViewer1.SizeToReportContent = True
        ReportViewer1.Visible = True
        ReportViewer1.LocalReport.EnableHyperlinks = True
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        bindgridview()
    End Sub

    Private Sub getassetcategory()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_GET_ASSETCATEGORIESSALL")
        sp.Command.AddParameter("@dummy", 1, DbType.String)

        ddlAssetCategory.DataSource = sp.GetDataSet()
        ddlAssetCategory.DataTextField = "VT_TYPE"
        ddlAssetCategory.DataValueField = "VT_CODE"
        ddlAssetCategory.DataBind()
        ddlAssetCategory.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub
    Private Sub getsubcategorybycat(ByVal categorycode As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_USP_GET_ASSETSUBCATBYASSET")
        sp.Command.AddParameter("@AST_CAT_CODE", categorycode, DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"))
        ddlAstSubCat.DataSource = sp.GetDataSet()
        ddlAstSubCat.DataTextField = "AST_SUBCAT_NAME"
        ddlAstSubCat.DataValueField = "AST_SUBCAT_CODE"
        ddlAstSubCat.DataBind()
        ddlAstSubCat.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub getbrandbycatsubcat(ByVal category As String, ByVal subcategory As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCAT")
        sp.Command.AddParameter("@MANUFACTURER_TYPE_CODE", category, DbType.String)
        sp.Command.AddParameter("@manufacturer_type_subcode", subcategory, DbType.String)
        ddlAstBrand.DataSource = sp.GetDataSet()
        ddlAstBrand.DataTextField = "manufacturer"
        ddlAstBrand.DataValueField = "manufactuer_code"
        ddlAstBrand.DataBind()
        ddlAstBrand.Items.Insert(0, New ListItem("--ALL--", "ALL"))
        TryCast(ddlAstBrand, IPostBackDataHandler).RaisePostDataChangedEvent()
    End Sub

    Private Sub getmakebycatsubcat()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_AST_GET_MAKEBYCATSUBCATVEND")
        sp.Command.AddParameter("@AST_MD_CATID", ddlAssetCategory.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_SUBCATID", ddlAstSubCat.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@AST_MD_BRDID", ddlAstBrand.SelectedItem.Value, DbType.String)
        ddlModel.DataSource = sp.GetDataSet()
        ddlModel.DataTextField = "AST_MD_NAME"
        ddlModel.DataValueField = "AST_MD_CODE"
        ddlModel.DataBind()
        ddlModel.Items.Insert(0, New ListItem("--ALL--", "ALL"))
    End Sub

    Private Sub BindLocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "usp_getActiveLocation") '@USER_ID
        sp.Command.AddParameter("@USER_ID", Session("Uid"), DbType.String)
        ddlLocation.DataSource = sp.GetReader
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--All--", "ALL"))
    End Sub
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub
    'Private Sub BindGrid()
    '    Try
    '        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_ASSETREQREPORT_DETAILS")
    '        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)

    '        Dim ds As New DataSet
    '        ds = sp.GetDataSet()

    '        Dim rds As New ReportDataSource()
    '        rds.Name = "AssetReqRptDS"
    '        'This refers to the dataset name in the RDLC file
    '        rds.Value = ds.Tables(0)
    '        ReportViewer1.Reset()
    '        ReportViewer1.LocalReport.DataSources.Clear()
    '        ReportViewer1.LocalReport.DataSources.Add(rds)
    '        ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Asset_Mgmt/AssetReqReport.rdlc")
    '        ReportViewer1.LocalReport.Refresh()
    '        ReportViewer1.SizeToReportContent = True
    '        ReportViewer1.Visible = True
    '        ReportViewer1.LocalReport.EnableHyperlinks = True
    '    Catch ex As Exception
    '        Response.Write(ex.Message)
    '    End Try
    'End Sub

    Protected Sub ReportViewer1_Drillthrough(sender As Object, e As DrillthroughEventArgs) Handles ReportViewer1.Drillthrough
        'Get OrderID that was clicked by 
        'user via e.Report.GetParameters()
        Dim DrillThroughValues As ReportParameterInfoCollection = e.Report.GetParameters()

        Dim thisConnectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString
        'Dim thatConnectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString

        'This is just to show you how to iterate 
        'through the collection if you have
        'multiple parameters values instead of a single parameter value.
        'To process multiple parameters values, 
        'concatenate d.Values[0] into a string with a delimiter.
        'Use the Split() method to  separate values 
        'into an array. Assign indivdual array element to
        'corresponding parameter array element.
        For Each d As ReportParameterInfo In DrillThroughValues
            Session("RPTParam") = d.Values(0).ToString().Trim()
        Next
        Dim localreport As LocalReport = DirectCast(e.Report, LocalReport)
        Dim Level1SearchValue As SqlParameter() = New SqlParameter(0) {}
        'Fill dataset for Level1.rdlc
        Dim thisConnection As New SqlConnection(thisConnectionString)
        Dim Level1DataSet As New System.Data.DataSet()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_GET_STATDTLS")
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        sp.Command.AddParameter("@status", Session("RPTParam").Trim())
        Level1DataSet = sp.GetDataSet()
        'Level1SearchValue(0) = New SqlParameter("@status", Session("RPTParam").Trim())
        'Level1DataSet = SqlHelper.ExecuteDataset(thisConnection, "GET_STATDTLS", Level1SearchValue)

        If Level1DataSet.Tables(0).Rows.Count > 0 Then
            Dim level1datasource As New ReportDataSource("AssetReqSubReportDS", Level1DataSet.Tables(0))
            localreport.DataSources.Clear()
            localreport.DataSources.Add(level1datasource)
            localreport.Refresh()
        End If
    End Sub
    Protected Sub ddlAssetCategory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAssetCategory.SelectedIndexChanged
        ddlAstBrand.Items.Clear()
        ddlModel.Items.Clear()
        getsubcategorybycat(ddlAssetCategory.SelectedItem.Value)
        TryCast(ddlAstSubCat, IPostBackDataHandler).RaisePostDataChangedEvent()
    End Sub

    Protected Sub ddlAstSubCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstSubCat.SelectedIndexChanged
        ddlModel.Items.Clear()
        getbrandbycatsubcat(ddlAssetCategory.SelectedItem.Value, ddlAstSubCat.SelectedItem.Value)
    End Sub

    Protected Sub ddlAstBrand_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAstBrand.SelectedIndexChanged
        getmakebycatsubcat()
    End Sub
End Class
