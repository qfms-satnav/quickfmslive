﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmSearchAvailabeStock.aspx.cs" Inherits="AssetManagement_Reports_frmSearchAvailabeStock" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }
        /*.grid-align
        {
            text-align: center;
        }

        a:hover
        {
            cursor: pointer;
        }

        .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }*/

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        /* .ag-header-cell-filtered
        {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button
        {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .search_button_wrap {
            margin-top: 50px;
        }*/
        .ag-blue .ag-cell:first-of-type {
            justify-content: left;
        }
    </style>
</head>
<body data-ng-controller="CustomizedReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Customizable Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Asset Availability Report</h3>
            </div>
            <div class="card">
                <%-- <div class="card-body" style="padding-right: 10px; height: 100vh">--%>
                <form id="form1" name="CustomizedReport" novalidate>
                    <div class="clearfix row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CAT_NAME.$invalid}">
                                <label for="txtcode">Asset Category<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="categorylist" data-output-model="Customized.selectedcat" data-button-label="icon CAT_NAME" data-item-label="icon CAT_NAME maker"
                                    data-on-item-click="CatChanged()" data-on-select-all="catSelectAll()" data-on-select-none="catSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedcat" name="CAT_NAME" style="display: none" required="" />
                                 <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CAT_NAME.$invalid" style="color: red">Please select Asset Category </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.AST_SUBCAT_NAME.$invalid}">
                                <label for="txtcode">   Asset Sub Category<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="SubCatlist" data-output-model="Customized.selectedsubcat" data-button-label="icon AST_SUBCAT_NAME" data-item-label="icon AST_SUBCAT_NAME maker"
                                    data-on-item-click="SubCatChanged()" data-on-select-all="subcatSelectAll()" data-on-select-none="" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedsubcat" name="AST_SUBCAT_NAME" style="display: none" required="" />
                                 <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.AST_SUBCAT_NAME.$invalid" style="color: red">Please select Asset Sub Category </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.BRND_NAME.$invalid}">
                                <label for="txtcode">Asset Brand/Make <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Brandlist" data-output-model="Customized.selectedBrands" data-button-label="icon BRND_NAME" data-item-label="icon BRND_NAME maker"
                                    data-on-item-click="BrandChanged()" data-on-select-all="BrandSelectAll()" data-on-select-none="BrandSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedBrands" name="BRND_NAME" style="display: none" required="" />
                                 <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.BRND_NAME.$invalid" style="color: red">Please select Asset Brand </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.MD_NAME.$invalid}">
                                <label for="txtcode">Asset Model <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Modellist" data-output-model="Customized.selectedModels" data-button-label="icon MD_NAME" data-item-label="icon MD_NAME maker"
                                    data-on-item-click="MdlChanged()" data-on-select-all="MdlChangeAll()" data-on-select-none="MdlSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.selectedModels" name="MD_NAME" style="display: none" required="" />
                                 <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.MD_NAME.$invalid" style="color: red">Please select Asset Model </span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid}">
                                    <label for="txtcode">Location <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Loclist" data-output-model="Customized.selectedLoc" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                        data-on-item-click="LocChanged()" data-on-select-all="LocChangeAll()" data-on-select-none="LocSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="Customized.selectedLoc" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid" style="color: red">Please select location </span>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                            <div class="form-group">
                                <label class="control-label">Company</label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="Customized.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="Customized.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Company.$invalid" style="color: red">Please select company </span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-right search_button_wrap">
                            <input type="submit" value="Search" data-ng-click="SubmitData(0)" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>


                    <div class="row" style="padding-right: 18px">
                        <div class="col-md-12 col-sm-6 col-xs-12 pull-right" data-ng-show="GridVisiblity2" style="padding-top: 15px">
                             <a data-ng-click="GenReport()"><i id="excel" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                        </div>
                    </div>

                   
                    <div id="Tabular" data-ng-show="GridVisiblity">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="selectpicker form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <%-- </div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="../../Scripts/jspdf.min.js" defer></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js"></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
    </script>

    <%--<script src="../../SMViews/Utility.js"></script>--%>
    <script src="../../SMViews/Utility.min.js" defer></script>
    <%--<script src="../JS/ASTCustomizedReport.min.js"></script>--%>
    <script src="../JS/frmSearchAvailableStock.js"></script>
</body>
</html>