﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="DayWiseConsumableReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Day Wise Consumable Report</h3>
            </div>
            <div class="card">
                <form id="frmDayWise" name="frmDayWise" data-valid-submit="SubmitData()" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDayWise.$submitted && frmDayWise.CTY_NAME.$invalid}">
                                <label class="col-md-12 control-label" for="txtcode">City <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="cityLists" data-output-model="cityLists.CTY_CODE"
                                    data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME maker"
                                    data-on-item-click="CityChanged()" data-on-select-all="Selectallcities()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="cityLists.CTY_CODE" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDayWise.$submitted && frmDayWise.CTY_NAME.$invalid" style="color: red">Please Select Location</span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDayWise.$submitted && frmDayWise.LCM_NAME.$invalid}">
                                <label class="col-md-12 control-label" for="txtcode">Location <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Loclist" data-output-model="Loclist.LCM_CODE"
                                    data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                    data-on-select-all="LocSelectAll()" data-on-select-none="LocSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Loclist.LCM_CODE" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDayWise.$submitted && frmDayWise.LCM_NAME.$invalid" style="color: red">Please Select Location</span>

                            </div>
                        </div>



                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDayWise.$submitted && frmDayWise.VT_TYPE.$invalid}">
                                <label class="col-md-12 control-label" for="txtcode">Asset Category<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="categorylist" data-output-model="categorylist.VT_CODE"
                                    data-button-label="icon VT_TYPE" data-item-label="icon VT_TYPE maker"
                                    data-on-item-click="CatChanged()" data-on-select-all="catSelectAll()" data-on-select-none="catSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="categorylist.VT_CODE" name="VT_TYPE" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDayWise.$submitted && frmDayWise.VT_TYPE.$invalid" style="color: red">Please Select Category </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDayWise.$submitted && frmDayWise.AST_SUBCAT_NAME.$invalid}">
                                <label class="col-md-12 control-label" for="txtcode">Asset Subcategory<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="SubCatlist" data-output-model="SubCatlist.AST_SUBCAT_CODE"
                                    data-button-label="icon AST_SUBCAT_NAME" data-item-label="icon AST_SUBCAT_NAME maker"
                                    data-on-item-click="SubCatChanged()" data-on-select-all="subcatSelectAll()" data-on-select-none="subcatSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SubCatlist.AST_SUBCAT_CODE" name="AST_SUBCAT_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDayWise.$submitted && frmDayWise.AST_SUBCAT_NAME.$invalid" style="color: red">Please Select Asset Subcategory </span>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDayWise.$submitted && frmDayWise.manufacturer.$invalid}">
                                <label class="col-md-12 control-label" for="txtcode">Asset Brand/Make<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Brandlist" data-output-model="Brandlist.manufactuer_code"
                                    data-button-label="icon manufacturer" data-item-label="icon manufacturer maker"
                                    data-on-item-click="BrandChanged()" data-on-select-all="BrandSelectAll()" data-on-select-none="BrandSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Brandlist.manufactuer_code" name="manufacturer" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDayWise.$submitted && frmDayWise.manufacturer.$invalid" style="color: red">Please Select Asset Brand</span>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDayWise.$submitted && frmDayWise.AST_MD_NAME.$invalid}">
                                <label class="col-md-12 control-label" for="txtcode">Asset Model <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Modellist" data-output-model="Modellist.AST_MD_CODE"
                                    data-button-label="icon AST_MD_NAME" data-item-label="icon AST_MD_NAME maker"
                                    data-on-item-click="GetReqId()" data-on-select-all="MdlChangeAll()" data-on-select-none="MdlSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Modellist.AST_MD_CODE" name="AST_MD_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDayWise.$submitted && frmDayWise.AST_MD_NAME.$invalid" style="color: red">Please Select Asset Model</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">From Date</label>
                                <div class="input-group date" id="fromdate">

                                    <input type="text" class="form-control" data-ng-model="Consumable.fromdate" id="FromDate" name="FromDate" placeholder="MM/DD/YYYY" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">To Date</label>
                                <div class="input-group date" id="todate">

                                    <input type="text" class="form-control" data-ng-model="Consumable.todate" id="ToDate" name="ToDate" placeholder="MM/DD/YYYY" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="button" data-ng-click="SearchData()" value="Search" class="btn btn-primary custom-button-color" />
                            <input type="button" value="Clear" data-ng-click="clear()" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                    <br />
                    <div class="row" data-ng-show="GridVisiblity">
                        <div class="col-xs-11  text-right">
                            <a data-ng-click="GenReport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                        </div>
                        <br />
                        <br />
                        <div class="col-xs-12">
                            <input type="text" class="selectpicker form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 350px; width: auto"></div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/DayWiseConsumableReport.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
</body>
</html>
