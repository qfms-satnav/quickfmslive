﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssetMapping.aspx.cs" Inherits="AssetManagement_Reports_AssetMapping" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        }
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

       /* .form-control {
            display: inline-block;
        }
*/
        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
        .pd-down{
            margin-bottom:18rem;
        }
        .ag-paging-button{   
            background: #d1d1d1;
        }
         #btFirst,#btLast{
            display:none !important;
        }
    </style>
</head>
<body data-ng-controller="AssetMappingController" class="amantra">

    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Asset Mapping</h3>
            </div>
            <div class="card">
                <form id="frmassetmapping" name="frmassetmapping"  data-valid-submit="SubmitData()"  novalidate>
                    <div class="clearfix row">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label class="btn btn-default pull-right">
                                    <input id="RbtSpace" name="AssetType" type="radio" value="2" ng-model="assetmapping.AssetType" />
                                    Space Asset Mapping
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="btn btn-default" style="margin-left: 25px">
                                    <input id="RbtEmployee" name="AssetType" type="radio" value="1" ng-model="assetmapping.AssetType" />
                                    Employee Asset Mapping
                                </label>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />

                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group"data-ng-class="{'has-error': frmassetmapping.$submitted && frmassetmapping.CAT_NAME.$invalid}">
                                    <label class="col-md-12 control-label" for="txtcode">Asset Category<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="categorylist" data-output-model="assetmapping.selectedcat"
                                        data-button-label="icon CAT_NAME" data-item-label="icon CAT_NAME maker"
                                        data-on-item-click="CatChanged()" data-on-select-all="catSelectAll()" data-on-select-none="catSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="assetmapping.selectedcat" name="CAT_NAME" style="display: none" required="" />
                                  <span class="error" data-ng-show="frmassetmapping.$submitted && frmassetmapping.CAT_NAME.$invalid" style="color: red">Please Select Category </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group"data-ng-class="{'has-error': frmassetmapping.$submitted && frmassetmapping.AST_SUBCAT_NAME.$invalid}">
                                    <label class="col-md-12 control-label" for="txtcode">Asset Subcategory<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="SubCatlist" data-output-model="assetmapping.selectedsubcat"
                                        data-button-label="icon AST_SUBCAT_NAME" data-item-label="icon AST_SUBCAT_NAME maker"
                                        data-on-item-click="SubCatChanged()" data-on-select-all="subcatSelectAll()" data-on-select-none="subcatSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="assetmapping.selectedsubcat" name="AST_SUBCAT_NAME" style="display: none" required="" />
                                 <span class="error" data-ng-show="frmassetmapping.$submitted && frmassetmapping.AST_SUBCAT_NAME.$invalid" style="color: red">Please Select Asset Subcategory </span>

                                </div>
                            </div>
                                    
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group"data-ng-class="{'has-error': frmassetmapping.$submitted && frmassetmapping.BRND_NAME.$invalid}">
                                    <label class="col-md-12 control-label" for="txtcode">Asset Brand/Make<span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="Brandlist" data-output-model="assetmapping.selectedBrands"
                                        data-button-label="icon BRND_NAME" data-item-label="icon BRND_NAME maker"
                                        data-on-item-click="BrandChanged()" data-on-select-all="BrandSelectAll()" data-on-select-none="BrandSelectNone()" 
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="assetmapping.selectedBrands" name="BRND_NAME" style="display: none" required="" />
                                  <span class="error" data-ng-show="frmassetmapping.$submitted && frmassetmapping.BRND_NAME.$invalid" style="color: red">Please Select Asset Brand</span>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmassetmapping.$submitted && frmassetmapping.MD_NAME.$invalid}">
                                    <label class="col-md-12 control-label"  for="txtcode">Asset Model <span style="color: red;"></span></label>
                                    <div isteven-multi-select data-input-model="Modellist" data-output-model="assetmapping.selectedModels" data-button-label="icon MD_NAME" data-item-label="icon MD_NAME maker"
                                        data-on-item-click="GetReqId()" data-on-select-all="MdlChangeAll()" data-on-select-none="MdlSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="assetmapping.selectedModels" name="MD_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmassetmapping.$submitted && frmassetmapping.MD_NAME.$invalid" style="color: red">Please Select Asset Model</span>
                                </div>
                            </div>
                        </div>
                        <br />
                        <br />
                        <br />

                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmassetmapping.$submitted && frmassetmapping.LCM_NAME.$invalid}">
                                    <label class="col-md-12 control-label" for="txtcode">Location <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Loclist" data-output-model="assetmapping.selectedLoc"
                                        data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME maker"
                                         data-on-item-click="GetEmployees()"data-on-select-all="GetEmployees()" data-on-select-none="LocSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="assetmapping.selectedLoc" name="LCM_NAME" style="display: none" required="" />
                                     <span class="error" data-ng-show="frmassetmapping.$submitted && frmassetmapping.LCM_NAME.$invalid" style="color: red">Please Select Location</span>

                                    </div>
                            </div>
                            <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Asset Type</label>
                                    <select id="Request_Type" name="Request_Type" required="" data-ng-model="assetmapping.Request_Type" class="selectpicker">
                                        <option value="All" selected>All</option>
                                        <option value="MAPPED">Mapped</option>
                                        <option value="UNMAPPED">Unmapped</option>
                                    </select>
                                </div>
                            </div>--%>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmassetmapping.$submitted && frmassetmapping.Request_Type.$invalid}">
                                    <label class="control-label">Asset Type</label>
                                    <select id="Request_Type" name="Request_Type"  data-ng-model="assetmapping.Request_Type" class="selectpicker">
                                        <option value="All" selected>All</option>
                                        <option value="MAPPED">Mapped</option>
                                        <option value="UNMAPPED">Unmapped</option>
                                    </select>
                                    <span class="error" data-ng-show="frmassetmapping.$submitted && frmassetmapping.Request_Type.$invalid" style="color: red">Please select Asset Type</span>
                                </div>
                            </div>
                               
                              <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Request ID<span style="color: red;">*</span></label>
                                        <div isteven-multi-select
                                            data-input-model="ReqIdlist"
                                            data-output-model="assetmapping.selectedReqId"
                                            data-button-label="icon AIR_REQ_TS"
                                            data-item-label="icon AIR_REQ_TS maker"
                                            data-on-item-click="ReqChanged()"
                                            data-on-select-all="ReqSelectAll()"
                                            data-on-select-none="ReqSelectNone()"
                                            data-tick-property="ticked"
                                            data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="assetmapping.selectedReqId" name="AIR_REQ_TS" style="display: none" required />
                                    </div>
                                </div>--%>
                        </div>
                        <div class="clearfix">
                            <div class="col-md-12 col-sm-12 col-xs-12 text-right search_button_wrap">
                                <input type="submit" value="Search" data-ng-click="submit()" class="btn btn-primary custom-button-color" />
                                <%--<input type="submit" value="Search"  class="btn btn-primary custom-button-color" />--%>
                                <input type="button" value="Clear" data-ng-click="clear()" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                         </div>
                    <%-- </div>--%>
                </form>
                        <%--     <div class="clearfix">
                            <div class="row">
                                <div class="col-md-12 text-left">
                                    <div class="form-group">
                                      <div class="row">
                                            <div class="col-md-4">
                                                <br />
                                                <input name="AssetMapping1$txtSearch" type="text" id="AssetMapping1_txtSearch" class="form-control" placeholder="Search By Any..." />
                                            </div>
                                            <div class="col-md-4">
                                                <br />
                                                <input type="submit" name="Search" value="Search" id="Search" class="btn btn-primary custom-button-color" />
                                            </div>
                                        
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>--%>

                        <%--  </div>--%>
                        <%--<div class="row">
                           <div class="col-md-12">
                                        <div class="box-footer text-left">
                                        </div>
                               
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" /> 
                               
                             
                               </div>
                                   <div class="col-md-3 col-sm-6 col-xs-12  ">
                                    <div class="form-group">
                                                    <label class="control-label">Asset Employee<span style="color: red;">*</span></label>
                                                    <div isteven-multi-select data-input-model="Emplist" data-output-model="assetmapping.selectedemp"
                                                        data-button-label="icon AUR_FIRST_NAME" data-item-label="icon AUR_FIRST_NAME maker"
                                                        data-on-item-click="empChanged()" data-on-select-all="empSelectAll()" data-on-select-none="empSelectNone()"
                                                        data-tick-property="ticked" data-max-labels="1">
                                                    </div>
                                                    <input type="text" data-ng-model="assetmapping.selectedemp" name="AUR_ID" style="display: none" required />
                                                </div>
                                            </div>
                             </div>--%>
                      <%--  <div class="container">--%>
                            <div class="row">
                                <!-- Filter input on the left side -->
                                <div class="col-md-12" data-ng-hide="isHidden">
                                    <br />
                                      <br />
                                    <div class="d-flex">
                                    <input type="text" class="form-control" id="filtertxt" data-ng-model="assetmapping.Search"placeholder="Filter by any..."  style="width: 20%" />
                                    <button type="button" class="btn btn-primary" style="margin-left: 10px;" data-ng-click="SubmitData()">Search</button>
                                </div>
</div>
                                
                           
                            </div>
                     <%--   </div>--%>
                        <div data-ng-hide="isHidden">
                        <div class="row" data-ng-hide="griddata1==true">
                            <div class="col-md-12">
                                <div data-ag-grid="gridOptions" style="height: 250px; width: auto" class="ag-blue"></div>
                            </div>
                        </div>
                            <br />
                           
                        <div class="clearfix" >
                            <div class="row">
                                  <div class="col-md-3 text-left" ng-show="assetmapping.AssetType == '1'">
                                    <div class="form-group">
                                        <label class="control-label">Asset Employee<span style="color: red;">*</span></label>
                                        <div isteven-multi-select
                                            data-input-model="Emplist"
                                            data-output-model="assetmapping.selectedemp"
                                            data-button-label="icon AUR_FIRST_NAME"
                                            data-ng-click="togglePad($event)"
                                            data-item-label="icon AUR_FIRST_NAME maker"
                                            data-on-item-click="empChanged()"
                                            data-on-select-all="empSelectAll()"
                                            data-on-select-none="empSelectNone()"
                                            data-tick-property="ticked"
                                            data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="assetmapping.selectedemp" name="AUR_ID" style="display: none"  />
                                        <span class="error" data-ng-if="isVisible==true" style="color: red">Please Select Employee </span>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div>
                                        <label>Remarks<span style="color: red; width: auto">*</span></label>
                                        <div >
                                            <textarea class="form-control" id="Remarks" name="Remarks" data-ng-model="assetmapping.Remarks" > </textarea>

<%--                                            <span class="error" data-ng-show="frmassetmapping.$submitted && frmassetmapping.Remarks.$invalid" style="color: red">Please enter Remarks </span>--%>
                                        </div>
                                    </div>
                                </div>
                                 <br />
                             <br />
                                <div id="btnPad" class="box-footer text-right" ng-class="{'pd-down': pad}">
                                    <input type="submit" value="Submit" data-ng-click="SaveData()" class="btn btn-primary custom-button-color"/>
                                </div>
                            </div>
                        </div>
 </div>
                   
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <!-- AngularJS and other JS libraries -->
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
        var CompanySessionId = '<%= Session["COMPANYID"]%>'
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/AssetMapping.js"></script>


</body>
</html>
