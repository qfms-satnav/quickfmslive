﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="content-type" content="text/plain; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    --%>
    <%--<link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <%-- <style>
      
        canvas {
            width:70px !important;
            height:70px !important;
        }
    </style>--%>
    <style>
        #exportthis.ag-blue .ag-row {
            height: 75px !important;
            line-height: 80px !important;
        }

        #exportthis .ag-cell {
            display: inline-block;
            padding:0px 10px;
        }
    </style>
</head>
<body data-ng-controller="QRCodeGeneratorController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">QR Code Generator</h3>
            </div>
            <div class="card">
                <%-- <div class="panel-body" style="padding-right: 10px;">--%>
                <form id="frmQRCodeGenerator" name="QRCodeGenerator" >
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Location</label>
                                <div isteven-multi-select data-input-model="Location" data-output-model="QRCodeGenerator.LCM_NAME" button-label="icon LCM_NAME"
                                    item-label="icon LCM_NAME" tick-property="ticked" data-on-select-all="locselectall()" data-on-select-none="" data-max-labels="1" data-on-item-click="BINDASSET()">
                                </div>
                                <input type="text" data-ng-model="QRCodeGenerator.LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.Location.$invalid" style="color: red">Please select location </span>
                            </div>
                        </div>
                        <%--<div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Company</label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="QRCodeGenerator.CNP_NAME" button-label="icon CNP_NAME"
                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="QRCodeGenerator.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.Company.$invalid" style="color: red">Please select company </span>
                            </div>
                        </div>--%>
                      <%--  <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Asset Name<span style="color: red;"></span></label>
                                <div isteven-multi-select data-input-model="AssetName" data-output-model="AssetName.AAT_NAME" button-label="icon AAT_NAME"
                                    item-label="icon AAT_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="AssetName.AAT_NAME" name="AAT_NAME" style="display: none" />
                                <%--                                <span class="error" data-ng-show="QRCodeGenerator.$submitted && QRCodeGenerator.AAT_NAME.$invalid" style="color: red">Please select Asset Name </span>
                            </div>
                        </div>--%>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">From Date</label>
                                <div class="input-group date" id="fromdate">

                                    <input type="text" class="form-control" data-ng-model="QRCodeGenerator.fromdate" id="FromDate" name="FromDate" placeholder="MM/DD/YYYY" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                                <%--<span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.fromdate.$invalid" style="color: red">Please select From Date </span>--%>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">To Date</label>
                                <div class="input-group date" id="todate">
                                    <input type="text" class="form-control" data-ng-model="QRCodeGenerator.todate" id="ToDate" name="ToDate" placeholder="MM/DD/YYYY" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <%--                                <span class="error" data-ng-show="frmQRCodeGenerator.$submitted && frmQRCodeGenerator.todate.$invalid" style="color: red">Please select to Date </span>--%>
                            </div>
                        </div>
                         <div class="col-xs-4">
                              <div class="input-group col-md-6">
                                <span class="input-group-prepend">
                                    <span class="input-group-text bg-transparent text-light"><i class="fa fa-search"></i></span>
                                </span>
                                <input type="text" id="filtertxtAsset" class="form-control" placeholder="Filter by any..." />
                            </div>
                                <div data-ag-grid="gridOptionsAsset" class="ag-blue" style="height: 210px; width: auto"></div>
                            </div>
                        <div class="col-md-2 col-sm-6 col-xs-12" style="margin-top: 20px;">
                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="Search()" />
                        </div>
                    </div>
                    <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Print Type</label>
                                <div>
                                    <label>
                                        <input type="radio" name="printType" value="Label_Print" ng-model="QRCodeGenerator.printType" >
                                        Label Print
                                    </label>
                                </div>
                                <div>
                                    <label>
                                        <input type="radio" name="printType" value="A4_Print" ng-model="QRCodeGenerator.printType" >
                                        General Print
                                    </label>
                                </div>
                             
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="box-footer text-right">
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="LoadData()" />
                                </div>
                            </div>
                        </div>
                    </div>--%>
                    <%-- <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Print Type</label>
                                <div style="display: flex; align-items: center; flex-wrap: nowrap;">
                                    <div style="flex-grow: 0; margin-right: 10px;">
                                        <!-- Reduce margin as needed -->
                                        <label>
                                            <input type="radio" name="printType" value="Label_Print" ng-model="QRCodeGenerator.printType">
                                            Label Print
                                        </label>
                                        <label>
                                            <input type="radio" name="printType" value="Single_Print" ng-model="QRCodeGenerator.printType">
                                            Single Print
                                        </label>
                                        <label>
                                            <input type="radio" name="printType" value="A4_Print" ng-model="QRCodeGenerator.printType">
                                            General Print
                                        </label>
                                    </div>
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="LoadData()" style="margin-right: 20px; flex-grow: 0;" />
                                    <!-- Ensure no additional spacing -->
                                    <a data-ng-click="GenReport('pdf')">
                                        <i id="pdf" data-toggle="tooltip" title="Export to PDF" class="fa fa-file-pdf-o fa-2x"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>--%>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-footer pull-right" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <a data-ng-click="GenReport('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <%-- <a data-ng-click="exportToExcel('#exportthis')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>--%>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div>
                        <div class="row">
                            <div class="col-md-5">
                                <input type="text" class="selectpicker form-control" id="filtertxt" placeholder="Filter by any..." />
                            </div>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-5" data-ng-show="PrinttypeVisible === 1">
                                <label>Print Type:</label>
                                <label>
                                    <input type="radio" name="printType" value="Label_Print" ng-model="QRCodeGenerator.printType">
                                    Label Print
                                </label>
                                <label>
                                    <input type="radio" name="printType" value="Single_Print" ng-model="QRCodeGenerator.printType">
                                    Single Print
                                </label>
                                <label>
                                    <input type="radio" name="printType" value="A4_Print" ng-model="QRCodeGenerator.printType">
                                    General Print
                                </label>
                                <a data-ng-click="GenReport('pdf')">
                                    <i id="pdf" data-toggle="tooltip" title="Export to PDF" class="fa fa-file-pdf-o fa-2x"></i>
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12" style="overflow-x: scroll;">
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto" id="exportthis"></div>
                            </div>
                        </div>
                        <div id="dvTable" style="">
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySessionId = '<%= Session["COMPANYID"]%>';
        var UID = '<%= Session["UID"]%>';
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
        $('#FromDate').datepicker('setDate', new Date(moment().startOf('month').format('MM/DD/YYYY')));
        $('#ToDate').datepicker('setDate', new Date(moment().endOf('month').format('MM/DD/YYYY')));
    </script>

    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/QRCodeGenerator.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/FileSaver.min.js"></script>
    <%--    <script src="../../BootStrapCSS/Scripts/leaflet/jspdf.js"></script>--%>
    <%-- <script src="../../BootStrapCSS/Scripts/leaflet/qr_generator.js"></script>--%>
    <script src="../../BootStrapCSS/Scripts/leaflet/qrcode.js"></script>
    <script src="../../BlurScripts/BlurJs/jquery.qrcode.min.js"></script>
</body>
</html>

