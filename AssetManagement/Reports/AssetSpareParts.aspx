﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssetSpareParts.aspx.cs" Inherits="FAM_FAM_Webfiles_AssetSpareParts" %>--%>
<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
    <head id="Head" runat="server">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Facility Management Services::a-mantra</title>
        <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

        <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
        <link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
        <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
        <link href="../../Scripts/DropDownCheckBoxList/angucomplete.css" rel="stylesheet" />
        <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />
        <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
        <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
        <script type="text/javascript">
            function maxLength(s, args) {
                if (args.Value.length >= 500)
                    args.IsValid = false;
            }
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                    && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }
        </script>
        <style>
            .grid-align {
                text-align: left;
            }

            a:hover {
                cursor: pointer;
            }

            .ag-header-cell-filtered {
                background-color: #4682B4;
            }

            .modal-header-primary {
                color: #1D1C1C;
                padding: 9px 15px;
            }

            #word {
                color: #4813CA;
            }

            #pdf {
                color: #FF0023;
            }

            #excel {
                color: #2AE214;
            }

            .ag-header-cell-filtered {
                background-color: #4682B4;
            }

            .ag-body-viewport-wrapper.ag-layout-normal {
                overflow-x: scroll;
                overflow-y: scroll;
            }

            .ag-header-cell-menu-button {
                opacity: 1 !important;
                transition: opacity 0.5s, border 0.2s;
            }

            .ag-header-cell {
                background-color: #dbdeed;
            }

            .ag-blue .ag-header-cell-label {
                color: black;
            }

            .bxheader {
                width: 100%;
                background-color: black;
                padding: 5px 10px;
            }

            .ag-blue .ag-row-odd {
                background-color: #fff;
            }

            .panel-title {
                color: black;
                font-weight: bold;
            }

            .site-form {
                display: grid;
                margin: 0 15px 0px 0px;
            }

                .site-form select {
                    padding: 5px 0px;
                }

            .custom-button-color {
                margin: 0px 5px;
            }
        </style>
        <style>
            /*canvas {
            width: 26px !important;
            height: 26px !important;
            }*/
        </style>
    </head>
    <body data-ng-controller="AssetSparePartsController" class="amantra">
        <div class="animsition">
            <div class="al-content">
                <div class="widgets">
                    <div ba-panel ba-panel-title="Asset SpareParts" ba-panel-class="with-scroll">
                        <div class="panel" style="height: 500px;">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <ul class="nav nav-tabs ">
                                        <li class="active">
                                            <a href="#tab_default_1" data-bs-toggle="tab">
                                            Add Spare Parts </a>
                                        </li>
                                        <li>
                                            <a href="#tab_default_2" data-bs-toggle="tab">
                                            Excel Upload </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="panel-body" style="padding-right: 10px;" >
                                <form id="form1" name="AssetSpareParts" <%--data-valid-submit="LoadData()"--%> data-valid-submit="Save()" novalidate>
                                    <div class="tab-content">
                                        <div class="tab-pane " id="tab_default_2">
                                            <div class="row">
                                                <div class="clearfix">
                                                    <div class="col-md-3 col-sm-4 col-xs-6">
                                                        <%--<div class="form-group" onmouseover="Tip('choose file to upload')" onmouseout="UnTip()">--%>
                                                        <div class="form-group">
                                                            <label>Upload File:</label>
                                                            <input type="file" name="ExUpload" id="File3" required="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />
                                                            <%--    <a data-ng-click="ExcelDownload()">Click Here To Download Template</a>--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 col-sm-4 col-xs-6" style="display: flex; align-items: end;">
                                                        <div class="form-group site-form" data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.LCM_NAME.$invalid}">
                                                          <label class="control-label">Site</label>
                                                           <%--<div isteven-multi-select data-input-model="Location" data-output-model="AssetSpareParts.Location" data-button-label="icon LCM_NAME"
                                                                data-item-label="icon LCM_NAME maker" data-on-item-click="GetAssetDetails()"
                                                                data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                            </div>--%>
                                                            <select name="Location" id="drplocation" ng-change="changeevt()"  ng-model="AssetSpareParts.Location">
                                                                <option value="">--Select User--</option>
                                                                <option ng-repeat="option in Location" value="{{option.LCM_CODE}}">{{option.LCM_NAME}}</option>
                                                            </select>
                                                            <input type="text" data-ng-model="AssetSpareParts.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.CTY_NAME.$invalid" style="color: red">Please select a location</span>
                                                        </div>
                                                        <input type="submit" value="Excel Down" class='btn btn-primary custom-button-color' data-ng-click="ExcelDownload()" id="exeldown" />
                                                        <input type="submit" value="Excel Upload" class='btn btn-primary custom-button-color' data-ng-click="UploadFile()" id="exelUpload" />
                                                    </div>
                                                    


                                                </div>
                                                <div style="padding-top:15px;">
                                                    <div style="font-size:24px; color:red; margin-bottom:10px;">Note*</div>
                                                      <p style="padding-top:15px;">1.Please select a location</p>
                                                        <p>2.New Spare to be added as new line item where Spare ID to be entered as 0</p>
                                                        <p>3.Do not update location name and location code</p>
                                                        <p>4.Do not enter one location data into another</p>
                                                        <%--<p>5.Spare Name ,Spare Description, Cost, Quantity, Measure, Vendor are mandatory columns.</p>--%>
 
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane active" id="tab_default_1">
                                            <div class="row">
                                                <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group" data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.CTY_NAME.$invalid}">
                                                        <label class="control-label">City</label>
                                                        <div isteven-multi-select data-input-model="City" data-output-model="AssetSpareParts.City" data-button-label="icon CTY_NAME"
                                                            data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()"
                                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                        </div>
                                                        <input type="text" data-ng-model="AssetSpareParts.City[0]" name="CTY_NAME" style="display: none" required="" />
                                                        <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.CTY_NAME.$invalid" style="color: red">Please select city </span>
                                                    </div>
                                                    </div>--%>
                                                <div class="col-md-3 col-sm-6 col-xs-12" <%--style="display:none"--%>>
                                                    <div class="form-group" data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.ZONE_NAME.$invalid}">
                                                        <label class="control-label">Zone</label>
                                                        <div isteven-multi-select data-input-model="Zone" data-output-model="AssetSpareParts.Zone" data-button-label="icon ZONE_NAME"
                                                            data-item-label="icon ZONE_NAME maker" 
                                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                        </div>
                                                        <%--<input type="text" data-ng-model="AssetSpareParts.Zone[0]" name="ZONE_NAME" style="display: none" required="" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.ZONE_NAME.$invalid" style="color: red">Please select zone </span>--%>
                                                    </div>
                                                    <%-- <div class="form-group">
                                                        <label class="control-label">Zone</label>
                                                        
                                                            <div data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.ZONE_NAME.$invalid}">
                                                                <select id="ZONE_NAME" name="ZONE_NAME" data-ng-model="AssetSpareParts.ZONE_NAME" class="form-control" data-live-search="true" required>
                                                                    <option value="">--Select--</option>
                                                                    <option data-ng-repeat="Spc in Zone" value="{{Spc.ZONE_ID}}">{{Spc.ZONE_NAME}}</option>
                                                                </select>
                                                                <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.ZONE_NAME.$invalid" style="color: red">Please Select valid ZONE Name </span>
                                                            </div>
                                                        
                                                        </div>--%>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group" data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.LCM_NAME.$invalid}">
                                                        <label class="control-label">Site</label>
                                                        <div isteven-multi-select data-input-model="Location" data-output-model="AssetSpareParts.Location" data-button-label="icon LCM_NAME"
                                                            data-item-label="icon LCM_NAME maker" 
                                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                        </div>
                                                        <input type="text" data-ng-model="AssetSpareParts.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                                        <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.CTY_NAME.$invalid" style="color: red">Please select a location</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12" style="display:none">
                                                    <div class="form-group" data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.TWR_CODE.$invalid}">
                                                        <label for="txtcode">Tower</label>
                                                        <div isteven-multi-select data-input-model="Tower" data-output-model="AssetSpareParts.Tower" data-button-label="icon TWR_CODE"
                                                            data-item-label="icon TWR_CODE maker" data-tick-property="ticked" data-on-select-none="SelectNone()" data-max-labels="1">
                                                        </div>
                                                        <input type="text" data-ng-model="AssetSpareParts.TWR_CODE" name="TWR_CODE" style="display: none" required="" />
                                                        <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.TWR_CODE.$invalid" style="color: red">Please select tower</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group" data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.AVR_NAME.$invalid}">
                                                        <label class="control-label">Vendor</label>
                                                        <div isteven-multi-select data-input-model="Vendor" data-output-model="AssetSpareParts.AVR_NAME" data-button-label="icon AVR_NAME"
                                                            data-item-label="icon AVR_NAME maker" data-on-item-click="GetVendor()" 
                                                            data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                                        </div>
                                                        <%--data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"--%>
                                                        <input type="text" data-ng-model="AssetSpareParts.Vendor[0]" name="AVR_NAME" style="display: none" required="" />
                                                        <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AVR_NAME.$invalid" style="color: red">Please select vendor </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12" style="display:none">
                                                    <div class="form-group" data-ng-class="{'has-error': AssetSpareParts.$submitted && AssetSpareParts.SUBC_NAME.$invalid}">
                                                        <label class="control-label">Equipment</label>
                                                        <span style="color: red;">*</span>
                                                        <div isteven-multi-select data-input-model="Type" data-output-model="AssetSpareParts.Type" data-button-label="icon SUBC_NAME"
                                                            data-item-label="icon SUBC_NAME maker" data-on-click="GetAssetDetails()" data-on-select-all="SelectAll()" data-on-select-none="SelectNone()"
                                                            data-tick-property="ticked" selection-mode="single" data-max-labels="1">
                                                        </div>
                                                        <input type="text" data-ng-model="AssetSpareParts.Type[0]" name="SUBC_NAME" style="display: none" required="" />
                                                        <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.SUBC_NAME.$invalid" style="color: red">Please select category</span>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group" style="margin-top: 19px;">    
                                                        <input type="Button" value="Search" class='btn btn-primary custom-button-color' data-ng-click="SearchA()" id="btnsearch" />
                                                        <button type="button" class="btn btn-primary" data-ng-click="fn_showHide()"  id="dispButton">Add Item</button>
                                                        <button type="button" class="btn btn-primary" data-ng-click="fn_Back()" id="dispBackButton" style="display:none;">Back</button>
                                                       <%-- onclick="fn_showHide();"--%>
                                                       <%-- onclick="fn_Back();"--%>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-6 col-xs-12">
                                                    <div class="form-group" style="margin-top: 19px;">
                                                        <%--<a data-ng-click="GenReport('doc')"><i id="word" data-bs-toggle="tooltip" <%--data-ng-show="DocTypeVisible==0"title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>--%>
                                                        <%--<a data-ng-click="exportToExcel('#exportthis')"><i id="excel" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>--%>
                                                        <a data-ng-click="GenReport('pdf')"><i id="pdf" data-bs-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix" style="height: 50px;display:none;" id="dispPage">
                                                </br>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>New Vendor Name</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="NEW_VENDOR_NAME" type="text" name="NEW_VENDOR_NAME" autofocus class="form-control"
                                                                required="" data-ng-model="AssetSpareParts1.NEW_VENDOR_NAME" data-ng-disabled="AssetSpareParts.AVR_NAME[0].AVR_NAME != 'OTHER'" />&nbsp;
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.NEW_VENDOR_NAME.$invalid" style="color: red">Please enter new vendor</span>
                                                        </div>
                                                    </div>
                                                  

                                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="!Edit">
                                                        <div class="form-group">
                                                            <label>EQUIPMENT </label>
                                                            <span style="color: red;">*</span>
                                                        </div>
                                                        <div angucomplete-alt
                                                            id="sub"
                                                            placeholder="Search SubCategory"
                                                            pause="500"
                                                            selected-object="selectedEmp"
                                                            remote-url="../../../api/AssetSpareParts/GetSpareSub"
                                                            remote-url-request-formatter="remoteUrlRequestFn"
                                                            remote-url-data-field="items"
                                                            title-field="NAME"
                                                            minlength="2"
                                                            input-class="form-control"
                                                            match-class="highlight"
                                                            data-ng-model="AssetSpareParts1.AAS_SUB_NAME"
                                                            required>
                                                        </div>
                                                        <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts1.AAS_SUB_NAME.$invalid" style="color: red">Please select Spare Sub Category </span>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="Edit">
                                                        <div class="form-group">
                                                            <label>Subcategory</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="AAS_SUB_NAME" type="text" name="AAS_SUB_NAME" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" autofocus class="form-control"
                                                                required="" data-ng-model="AssetSpareParts1.AAS_SUB_NAME" />&nbsp;
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts1.AAS_SUB_NAME.$invalid" style="color: red">Please select spare subcategory </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="Edit == false">
                                                        <div class="form-group">
                                                            <label>Brand</label>
                                                        </div>
                                                        <div angucomplete-alt
                                                            id="Brand"
                                                            placeholder="Search Brand"
                                                            pause="500"
                                                            selected-object="selectedBrand"
                                                            remote-url="../../../api/AssetSpareParts/GetSpareBrand"
                                                            remote-url-request-formatter="remoteUrlRequestFn"
                                                            remote-url-data-field="items"
                                                            title-field="NAME"
                                                            minlength="2"
                                                            input-class="form-control"
                                                            match-class="highlight"
                                                            data-ng-model="AssetSpareParts1.AAS_AAB_NAME"
                                                            required>
                                                        </div>
                                                        <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts1.AAS_AAB_NAME.$invalid" style="color: red">Please select spare brand </span>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="Edit ==true">
                                                        <div class="form-group">
                                                            <label>Brand{{AssetSpareParts1.AAS_AAB_NAME}}</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="AAS_AAB_NAME" type="text" name="AAS_AAB_NAME" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" autofocus class="form-control"
                                                                required="" data-ng-model="AssetSpareParts1.AAS_AAB_NAME" />&nbsp;
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts1.AAS_AAB_NAME.$invalid" style="color: red">Please select spare brand</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="Edit == false">
                                                        <div class="form-group">
                                                            <label>Model</label>
                                                        </div>
                                                        <div angucomplete-alt
                                                            id="Model"
                                                            placeholder="Search Model"
                                                            pause="500"
                                                            selected-object="selectedModel"
                                                            remote-url="../../../api/AssetSpareParts/GetSpareModel"
                                                            remote-url-request-formatter="remoteUrlRequestFn"
                                                            remote-url-data-field="items"
                                                            title-field="NAME"
                                                            minlength="2"
                                                            input-class="form-control"
                                                            match-class="highlight"
                                                            data-ng-model="AssetSpareParts1.AAS_MODEL_NAME"
                                                            required>
                                                        </div>
                                                        <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts1.AAS_MODEL_NAME.$invalid" style="color: red">Please select spare model</span>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12" data-ng-if="Edit ==true">
                                                        <div class="form-group">
                                                            <label>Model</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="AAS_MODEL_NAME" type="text" name="AAS_MODEL_NAME" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" autofocus class="form-control"
                                                                required="" data-ng-model="AssetSpareParts1.AAS_MODEL_NAME" />&nbsp;
                                                        </div>
                                                    </div>
                                                </div>
                                                </br>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Part Name</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="AAS_SPAREPART_NAME" type="text" name="AAS_SPAREPART_NAME" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" autofocus class="form-control"
                                                                required="" data-ng-model="AssetSpareParts1.AAS_SPAREPART_NAME" />&nbsp;
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_SPAREPART_NAME.$invalid" style="color: red">Please select part name </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Part Description</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="AAS_SPAREPART_DES" type="text" name="AAS_SPAREPART_DES" data-ng-model="AssetSpareParts1.AAS_SPAREPART_DES" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" autofocus class="form-control" required="required" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_SPAREPART_NAME.$invalid" style="color: red">Please select part description </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Available Quantity</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="AAS_SPAREPART_AVBQUANTITY" type="text" name="AAS_SPAREPART_AVBQUANTITY" maxlength="25" data-ng-model="AssetSpareParts1.AAS_SPAREPART_AVBQUANTITY" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" autofocus class="form-control" required="required" onkeypress="return isNumberKey(event)" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_SPAREPART_NAME.$invalid" style="color: red">Please select available quantity </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Cost per unit</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="AAS_SPAREPART_COST" type="text" name="AAS_SPAREPART_COST" maxlength="25" data-ng-model="AssetSpareParts1.AAS_SPAREPART_COST" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" autofocus class="form-control" required="required" onkeypress="return isNumberKey(event)" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_SPAREPART_NAME.$invalid" style="color: red">Please select cost per unit</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Minimum Stock Quantity</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="Stock Quantity" type="text" name="AAS_SPAREPART_MINQUANTITY" maxlength="25" data-ng-model="AssetSpareParts1.AAS_SPAREPART_MINQUANTITY" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" autofocus class="form-control" required="required" onkeypress="return isNumberKey(event)" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_SPAREPART_NAME.$invalid" style="color: red">Please select stock quantity</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Storage Area</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="Storage Area" type="text" name="AAS_RACK_NO" maxlength="25" data-ng-model="AssetSpareParts1.AAS_RACK_NO" autofocus class="form-control" required="required" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_RACK_NO.$invalid" style="color: red">Please enter storage area </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Lead Time</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="ASS_LEAD_TIME" type="text" name="ASS_LEAD_TIME" maxlength="25" data-ng-model="AssetSpareParts1.ASS_LEAD_TIME" autofocus class="form-control" required="required" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.ASS_LEAD_TIME.$invalid" style="color: red">Please enter lead time</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Priority</label>
                                                            <br/>
                                                            <select style="width: 100%; height: 30px;" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-maxlength ng-touched" data-ng-model="AssetSpareParts1.AAS_VED" name="AAS_VED" id="veds">
                                                                <%-- <option value="Vital">Vital</option>
                                                                    <option value="Essential">Essential</option>
                                                                    <option value="Desirabel">Desirabel</option>  --%>
                                                                <option value="P0">P0</option>
                                                                <option value="P1">P1</option>
                                                                <option value="P2">P2</option>
                                                            </select>
                                                            <input type="text"  name="AAS_VED" style="display: none" required />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_VED.$invalid" style="color: red">Please select VED </span>
                                                        </div>
                                                    </div>
                                                      
                                                    <div class="col-md-3 col-sm-12 col-xs-12" style="display: none">
                                                        <div class="form-group">
                                                            <label>FSN</label>
                                                            <br/>
                                                            <select data-ng-model="AssetSpareParts1.AAS_FSN" style="width: 100%;  height: 30px;" class="form-control ng-pristine ng-invalid ng-invalid-required ng-valid-maxlength ng-touched" name="AAS_FSN" id="fsndata">
                                                                <option value="Fast Moving">Fast Moving</option>
                                                                <option value="Slow Moving">Slow Moving</option>
                                                                <option value="Non Moving">Non Moving</option>
                                                                <input type="text" name="AAS_FSN" style="display: none" required="" />
                                                            </select>
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_FSN.$invalid" style="color: red">Please enter storage area </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                </br>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Measure (UOM)</label>
                                                            <span style="color: red;">*</span>
                                                            <input id="AAS_SPAREPART_QUANTITY" type="text" name="AAS_SPAREPART_QUANTITY" maxlength="25" data-ng-model="AssetSpareParts1.AAS_SPAREPART_QUANTITY" autofocus class="form-control" required="required" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_SPAREPART_QUANTITY.$invalid" style="color: red">Please enter unit of measure (UOM)</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Part No.</label>
                                                              <span style="color: red;">*</span>
                                                            <input id="AAS_SPAREPART_NUMBER" type="text" name="AAS_SPAREPART_NUMBER" maxlength="25" data-ng-model="AssetSpareParts1.AAS_SPAREPART_NUMBER" autofocus class="form-control" required="required" />
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_SPAREPART_NUMBER.$invalid" style="color: red">Please enter part number.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks</label>
                                                            <span style="color: red;">*</span>
                                                            <textarea name="AAS_REMARKS" id="AAS_REMARKS" class="form-control" data-ng-model="AssetSpareParts1.AAS_REMARKS" cols="40" rows="5" style="height: 60px;"></textarea>
                                                            <span class="error" data-ng-show="AssetSpareParts.$submitted && AssetSpareParts.AAS_REMARKS.$invalid" style="color: red">Please enter remarks </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                       
                                                    </div>
                                                   
                                                    
                                                </div>
                                                </br>
                                                <div class="row">
                                                    <%--<div class="col-md-3 col-sm-12 col-xs-12" style="width: 100px;"  data-ng-show="hide">
                                                        <div class="form-group">
                                                            <label class="control-label">
                                                                UploadImage 
                                                            </label>
                                                            <div style="width: 100px">
                                                                <input type="file" id="file" onchange="angular.element(this).scope().fileNameChanged(this,'F')" data-ng-model="pgVar.FilePathArr[0].FilePath" ngf-multiple="false" name="file" accept=".png,.jpg,.xlsx,.pdf,.docx">
                                                            </div>
                                                        </div>
                                                        </div>--%> 
                                                    <div class="form-group" class="col-md-3 col-sm-12 col-xs-12">
                                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" style="margin-left: 15px;" />
                                                        <input type="submit" value="Update" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1"  />
                                                        <input type="reset" value="Clear" class='btn btn-primary custom-button-color' data-ng-show="hide" data-ng-click="ClearData()" />
                                                        <%--<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-target="#exampleModal">Add Item</button>--%>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                         
                                            <div class="row" id="dispGrid">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div style="height: 325px; width: inherit" data-ng-show="Checing">
                                                        <div class="bxheader">
                                                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />
                                                        </div>
                                                        <div data-ag-grid="gridOptions" style="height: 310px; width:auto;" cellClass: 'cell-wrap-text' class="ag-blue"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                 </form>
                            </div>
                        </div>
                    </div>
                    <div style="margin-top:15px;padding-left:20px;">
                        <a class="btn btn-danger homeButton" href="#" onclick="fnDetail('../../MaintenanceManagement/Views/MaintainanceDashboard.aspx')" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-home" aria-hidden="true"></i>HOME</a>
                        <a class="btn btn-danger homeButton" href="#" onclick="goBack();" role="button" style="float:right;color:#fff;;background-color: #6c757d;border-color: #6c757d;"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</a>
                    </div>
                </div>
            </div>
        </div>
        <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
        <script src="../../Scripts/jspdf.min.js"></script>
        <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
        <script src="../../Scripts/Lodash/lodash.min.js"></script>
        <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
        <script src="../../Scripts/moment.min.js"></script>
        <script>
            var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
            var CompanySession = '<%= Session["COMPANYID"]%>';
        </script>
        <script type="text/javascript">

            function fnDetail(url) {
                //progress(0, 'Loading...', true);
                window.location = url;
            }
            function goBack() {
                //window.history.back();           
                var strBackUrl = sessionStorage.getItem("strBackUrl");
                //sessionStorage.setItem("strBackUrl", "/AssetManagement/Reports/AssetSpareParts.aspx");
                window.location = strBackUrl;
            }
            function setDateVals() {
                $('#FromDate').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    todayHighlight: true
                });
                $('#ToDate').datepicker({
                    format: 'mm/dd/yyyy',
                    autoclose: true,
                    todayHighlight: true
                });
                $('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
                $('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
            }
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                //sessionStorage.setItem("strBackUrl", "/AssetManagement/Reports/AssetSpareParts.aspx");
                setDateVals();
            });

        </script>
        <script defer>
            var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "angucomplete-alt"]);
        </script>
        <script src="../../SMViews/Utility.js" defer></script>
        <script src="../JS/AssetSpareParts.js"></script>
        <script src="../../Scripts/DropDownCheckBoxList/angucomplete-alt.min.js" defer></script>
        <script src="../../BootStrapCSS/Scripts/leaflet/FileSaver.min.js"></script>
        <script src="../../BootStrapCSS/Scripts/leaflet/jspdf.js"></script>
        <script src="../../BootStrapCSS/Scripts/leaflet/qr_generator.js"></script>
        <script src="../../BootStrapCSS/Scripts/leaflet/qrcode.js"></script>
        <script src="../../BlurScripts/BlurJs/jquery.qrcode.min.js"></script>
        <script src="../../BootStrapCSS/Scripts/leaflet/FileSaver.min.js"></script>
        <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js"></script> 
    </body>
</html>