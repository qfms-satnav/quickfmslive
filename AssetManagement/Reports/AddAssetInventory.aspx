﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .thick-pink-line {
            width: 100%;
            height: 10px;
            background-color: #FFC0CB;
            margin: 40px 0;
        }
    </style>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
        #btFirst,#btLast{
            display:none !important;
        }
    </style>


</head>
<body data-ng-controller="AddAssetInventoryController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Add Capital Asset </h3>
            </div>
            <div class="card">
                <form role="form" id="frmAddInventory" name="frmAddInventory" data-valid-submit="SubmitData()" novalidate>
                    <div class="row">
                        <label class="col-md-3 control-label">Upload Document(Only Excel)<span style="color: red;">*</span></label>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <br />
                            <input type="file" id="fileUpl" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <input type="button" value="Upload File" data-ng-click="UploadExcel()" class="btn btn-primary custom-button-color" />
                           <br />
                            <asp:HyperLink ID="hyp" runat="server" Text=" Click here to View the Template" NavigateUrl="~/Masters/Mas_Webfiles/CapitalAsset.xlsx"></asp:HyperLink>

                            <span style="color: red" data-ng-show="frmAddInventory.$submitted && frmAddInventory.fileUpl.$invalid">Please select File</span>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="row" data-ng-hide="griddata1==true">
                            <div class="col-md-12">
                                <a data-ng-click="GenReport()"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>

                                <br />
                                <br />
                                <div data-ag-grid="gridOptions1" style="height: 250px; width: auto" class="ag-blue"></div>
                            </div>
                        </div>
                    </div>
                    <div class="thick-pink-line"></div>
                    <div class="row">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.Assetmaincatcode.$invalid}">
                                <div>
                                    <%--                                    <input id="Assetmaincatname" type="hidden" name="Assetmaincatcode" data-ng-model="AssetMainCategoryList.Assetmaincatcode" autofocus class="form-control" />--%>
                                    <label>Asset Category<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="AssetMainCategoryList" data-output-model="AssetMainCategoryList.Assetmaincatcode" button-label="icon Assetmaincatname""
                                        data-item-label="icon Assetmaincatname" data-tick-property="ticked" data-on-item-click="onBindSubcategory()"
                                        data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="AssetMainCategoryList.Assetmaincatcode" name="Assetmaincatname" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.Assetmaincatname.$invalid" style="color: red">Please Select Asset Category </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.Assetsubncatname.$invalid}">
                                <label>Asset Sub Category<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AssetSubCategoryList" data-output-model="AssetSubCategoryList.Assetsubncatcode" button-label="icon Assetsubncatname"  
                                    data-item-label="icon Assetsubncatname" data-tick-property="ticked" data-on-item-click="onBindAssetBrand()"
                                    data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetSubCategoryList.Assetsubncatcode" name="Assetsubncatname" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.Assetsubncatname.$invalid" style="color: red">Please Select Asset Sub Category </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div>
                                    <input id="BrandName" type="hidden" name="BrandName" data-ng-model="AssetBrandList.BrandName" autofocus class="form-control" />
                                    <label>Asset Brand/Make<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="AssetBrandList" data-output-model="AssetBrandList.BrandCode" button-label="icon BrandName"  
                                        data-item-label="icon BrandName" data-tick-property="ticked" data-on-item-click="onBindAssetModel()"
                                        data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="AssetBrandList.BrandCode" name="BrandName" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.BrandName.$invalid" style="color: red">Please Select Asset Brand </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.MdName.$invalid}">
                                <label>Asset Model<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AssetModelList" data-output-model="AssetModelList.MdCode" button-label="icon MdName" 
                                   data-on-item-click="AstMdlchanged()" data-item-label="icon MdName" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetModelList.MdCode" name="MdName" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.MdName.$invalid" style="color: red">Please Select Model </span>
                            </div>
                        </div>

                    </div>
                    <div class="row">

                        <div class="col-md-3 col-sm-12 col-xs-12" ng-disabled="disabled">
                            <div class="form-group">
                                <%-- <label>Location<span style="color: red;">*</span></label>--%>
                                <div>
                                    <input id="LCMNAME" type="hidden" name="LCMNAME" data-ng-model="AssetlOCATIONList.LCMNAME" autofocus class="form-control" />
                                    <label>Location<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="AssetlOCATIONList" data-output-model="AssetlOCATIONList.lCMCODE" button-label="icon LCMNAME"  
                                        data-item-label="icon LCMNAME" data-tick-property="ticked" data-on-item-click="onBindAssettower()"
                                        data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="AssetlOCATIONList.lCMCODE" name="LCMNAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.LCMNAME.$invalid" style="color: red">Please Select Location </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.twr_name.$invalid}">
                                <label>Tower<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AssetTowerList" data-output-model="AssetTowerList.twr_CODE" button-label="icon twr_name" 
                                    data-item-label="icon twr_name" data-tick-property="ticked" data-on-item-click="onBindAssetFLOOR()" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetTowerList.twr_CODE" name="twr_name" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.twr_name.$invalid" style="color: red">Please Select Tower </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <%-- <label>Location<span style="color: red;">*</span></label>--%>
                                <div>
                                    <input id="flr_name" type="hidden" name="flr_name" data-ng-model="AssetFloorList.flr_name" autofocus class="form-control" />
                                    <label>Floor<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="AssetFloorList" data-output-model="AssetFloorList.flr_code" button-label="icon flr_name" 
                                        data-item-label="icon flr_name" data-tick-property="ticked"
                                        data-on-item-click="onFlrChanged()" data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="AssetFloorList.flr_code" name="flr_name" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.flr_name.$invalid" style="color: red">Please Select Floor </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Auto Generated Asset ID</label>
                                <input id="AAT_CODE" type="text" name="AAT_CODE" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="AddInventory.AAT_CODE" class="form-control" required="required" readonly />&nbsp;
                                <%--                                <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.AAT_CODE.$invalid" style="color: red">Please e Auto Generated Asset Id </span>--%>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Asset ID<span style="color: red;">*</span></label>
                                <input id="AAT_NAME" type="text" name="AAT_NAME" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="AddInventory.AAT_NAME" class="form-control" required="required" />&nbsp;
                         <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.AAT_NAME.$invalid" style="color: red">Please Enter Asset Id </span>
                                <span class="error"  data-ng-show="frmAddInventory.$submitted && frmAddInventory.AAT_NAME.$error.pattern" style="color: red"></span>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.vendorname.$invalid}">
                                <label>Vendor<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AssetVENDORList" data-output-model="AssetVENDORList.vendorCode" button-label="icon vendorname"
                                    data-item-label="icon vendorname" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetVENDORList.vendorCode" name="vendorname" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.vendorname.$invalid" style="color: red">Please Select Vendor </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Rate Contract(Y/N)<span style="color: red;"></span></label>
                                <div class="col-md-9 control-label">
                                    <input type="radio" name="rateContract" ng-change="updateUploadDisabled()" value="1" ng-model="AddInventory.rateContract" />&nbsp Yes &nbsp&nbsp&nbsp&nbsp
                                <input type="radio" name="rateContract" ng-change="updateUploadDisabled()" value="0" ng-model="AddInventory.rateContract" />&nbsp No
                             
                                   <%-- <input type="radio" id="rateContractYes" name="rateContract" value="1" checked>
                                <label for="rateContractYes">Yes &nbsp;</label>
                                <input type="radio" id="rateContractNo" name="rateContract" value="0">
                                <label for="rateContractNo">No</label>--%>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.AAT_COST.$invalid}">
                                <label>Asset Price (In Cost)<span style="color: red;">*</span></label>
                                <%-- <div data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.AAT_COST.$invalid}">--%>
                                <input id="AAT_COST" type="number" name="AAT_COST" maxlength="25" data-ng-model="AddInventory.AAT_COST" class="form-control" required=""  ng-pattern="/^\d+(\.\d{1,2})?$/"/>
                         <span class="error"   data-ng-show="frmAddInventory.$submitted && frmAddInventory.AAT_COST.$invalid" style="color: red">Please enter Asset Price </span>
                           <span class="error"  data-ng-show="frmAddInventory.$submitted && frmAddInventory.AAT_COST.$error.pattern" style="color: red">
                    Please enter a valid numeric Asset Price.
                </span>
                                </div>
                        </div>
                        <%--</div>--%>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Count</label>
                                <div data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.Count.$invalid}">
                                    <input id="Count" type="text" name="Count" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="AddInventory.Count" class="form-control" required="required" />&nbsp;
                            <%--   <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.BILL_INVOICE.$invalid" style="color: red">Please enter valid bill invoice </span>--%>
                                </div>
                            </div>
                        </div>
                        <%--    <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Count<span style="color: red;">*</span></label>
                                <div data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.BILL_INVOICE.$invalid}">
                                    <input id="BILL_INVOICE" type="text" name="BILL_INVOICE" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="SaveExpUtility.BILL_INVOICE" class="form-control" required="required" />&nbsp;
   <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.BILL_INVOICE.$invalid" style="color: red">Please enter valid bill invoice </span>
                                </div>
                            </div>
                        </div>--%>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Asset Serial No</label>
                                <div>
                                    <input id="AST_SNO" type="text" name="AST_SNO" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="AddInventory.AST_SNO" class="form-control" />&nbsp;
                                    <%--   <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.AST_SNO.$invalid" style="color: red">Please enter valid bill invoice </span>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.Assetname.$invalid}">
                                <label>Asset Type<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AssetTYPEList" data-output-model="AssetTYPEList.Assettype" button-label="icon Assetname"
                                    data-item-label="icon Assetname" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetTYPEList.Assettype" name="Assetname" style="display: none" required="" />
                                <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.Assetname.$invalid" style="color: red">Please Select Asset Type </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Life Span(In Years)</label>
                                <div>
                                    <input id="LIFE_SPAN" type="text" name="LIFE_SPAN" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="AddInventory.LIFE_SPAN" class="form-control" required="required" />&nbsp;
                                    <%--   <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.BILL_INVOICE.$invalid" style="color: red">Please enter valid bill invoice </span>--%>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>PO Number</label>
                                <div>
                                    <input id="PO_NUM" type="text" name="PO_NUM" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="AddInventory.PO_NUM" class="form-control" required="required" />&nbsp;
                                    <%--   <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.PO_NUM.$invalid" style="color: red">Please enter valid bill invoice </span>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Warranty Date Up To</label>
                                <div class="input-group date" id='WAR_DT'>
                                    <input type="text" class="form-control" data-ng-model="AddInventory.WAR_DT" id="WAR_DT" name="WAR_DT" required="" placeholder="dd-mm-yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('WAR_DT')"></i>
                                    </span>
                                </div>
                            </div>
                            <%--                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.FromDate.$invalid" style="color: red;">Please select Bill Date</span>--%>
                        </div>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Purchase Date</label>
                                <div class="input-group date" id='PUR_DT'>
                                    <input type="text" class="form-control" data-ng-model="AddInventory.PUR_DT" id="PUR_DT" name="PUR_DT" required="" placeholder="dd-mm-yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('PUR_DT')"></i>
                                    </span>
                                </div>
                            </div>
                            <%--                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.FromDate.$invalid" style="color: red;">Please select Bill Date</span>--%>
                        </div>



                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Mfg. Date</label>
                                <div class="input-group date" id='MFG_DT'>
                                    <input type="text" class="form-control" data-ng-model="AddInventory.MFG_DT" id="MFG_DT" name="MFG_DT" required="" placeholder="dd-mm-yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('MFG_DT')"></i>
                                    </span>
                                </div>
                            </div>
                            <%--                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.FromDate.$invalid" style="color: red;">Please select Bill Date</span>--%>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Depreciation(in %)</label>
                                <div>
                                    <input id="Depreciation" type="text" name="Depreciation" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="AddInventory.Depreciation" class="form-control" />&nbsp;
                                    <%--                               <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.Depreciation.$invalid" style="color: red">Please enter valid bill invoice </span>--%>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>CostCenter</label>
                                <div isteven-multi-select data-input-model="CostcenterList" data-output-model="CostcenterList.Cost_Center_Code" button-label="icon Cost_Center_Code"
                                    data-item-label="icon Cost_Center_Code" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CostcenterList.Cost_Center_Code" name="Cost_Center_Code" style="display: none" />
                                <%--                        <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.Cost_Center_Code.$invalid" style="color: red">Please Select CostCenter </span>--%>
                            </div>
                        </div>
                        <div class="form-group col-sm-3 col-xs-6">
                            <div class="form-group">
                                <label class="control-label" for="txtcode">Status</label>
                                <div>
                                    <select id="status" name="status" data-ng-model="AddInventory.STATUS" class="form-control">
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Asset For</label>
                                <div isteven-multi-select data-input-model="AssetCompanyList" data-output-model="AssetCompanyList.AssettCompany" button-label="icon AssettCompany"
                                    data-item-label="icon AssettCompany" data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetCompanyList.AssettCompany" name="AssettCompany" style="display: none" required="" />
                                <%--                        <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.AssettCompany.$invalid" style="color: red">Please Select Asset For </span>--%>
                            </div>
                        </div>

                    </div>

                    <div class="row">


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Invoice Number</label>
                                <div>
                                    <input id="INV_NUM" type="text" name="INV_NUM" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="AddInventory.INV_NUM" class="form-control" />&nbsp;
                                    <%--                                 <span class="error" data-ng-show="frmAddInventory.$submitted && frmAddInventory.INV_NUM.$invalid" style="color: red">Please enter valid bill invoice </span>--%>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" id="Div5">
                                <label>Upload Asset Image</label>
                                <div style="width: 100px">
                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                    <input type="file" name="file" onchange="angular.element(this).scope().fileUpload(this,'AAT_IMG')" multiple="multiple" data-ng-model="AddInventory.AAT_IMG" id="file3" required=""
                                        accept=".png,.jpg,.xlsx,.pdf,.docx">
                                    <%--<span ng-if="data.PocPhoto != undefined">
                                                        <button ng-click="cancelSelectedFile(data.PocPhoto,'POC_Photo','file1')" " class="btn btn-link"><i class="bi bi-trash"></i></button>
                                                    </span>--%>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Description<span style="color: red; width: auto">*</span></label>
                                <div data-ng-class="{'has-error': frmAddInventory.$submitted && frmAddInventory.Description.$invalid}">
                                    <textarea class="form-control" id="Description" name="Descrpition" data-ng-model="AddInventory.Descrpition" required="required"> </textarea>

                                    <span class="error"  data-ng-if="isVisible==true" data-ng-show="frmAddInventory.$submitted && frmAddInventory.Description.$invalid" style="color: red">Please enter Description </span>
                                                                        <span class="error"  data-ng-if="isVisible==true"  style="color: red">Please enter Description </span>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>AMC Included(Y/N)</label>
                                <div class="col-md-9 control-label">
                                    <input type="radio" name="AMC" ng-change="updateUploadDisabled()" value="1" ng-model="AddInventory.AMC" />&nbsp Yes &nbsp&nbsp&nbsp&nbsp
                                <input type="radio" name="AMC" ng-change="updateUploadDisabled()" value="0" ng-model="AddInventory.AMC" />&nbsp No
                             
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12" ng-show="AddInventory.AMC !== '0'">
                            <div class="form-group">
                                <label for="txtcode">AMC Date Up To</label>
                                <div class="input-group date" id='AMC_DT'>
                                    <input type="text" class="form-control" data-ng-model="AddInventory.AMC_DT" id="AMC_DT" name="AMC_DT" required="" placeholder="dd-mm-yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('AMC_DT')"></i>
                                    </span>
                                </div>
                            </div>
                            <%--                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.AMC_DT.$invalid" style="color: red;">Please select Bill Date</span>--%>
                        </div>







                        <div class="col-md-3 col-sm-12 col-xs-12" ng-show="AddInventory.AMC !== '0'">
                            <div class="form-group" id="Div5">
                                <label>Upload AMC Document(s)</label>
                                <div style="width: 100px">
                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                    <input type="file" name="file1" onchange="angular.element(this).scope().fileUpload(this,'AMC_DOC')" multiple="multiple" data-ng-model="AddInventory.AMC_DOC" id="file1" required=""
                                        accept=".png,.jpg,.xlsx,.pdf,.docx">
                                    <%--<span ng-if="data.PocPhoto != undefined">
                                                        <button ng-click="cancelSelectedFile(data.PocPhoto,'POC_Photo','file1')" " class="btn btn-link"><i class="bi bi-trash"></i></button>
                                                    </span>--%>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Insurance Included(Y/N)</label>
                                <div class="col-md-9 control-label">
                                    <input type="radio" name="Insurance" ng-change="updateUploadDisabled()" value="1" ng-model="AddInventory.Insurance" />&nbsp Yes &nbsp&nbsp&nbsp&nbsp
                                <input type="radio" name="Insurance" ng-change="updateUploadDisabled()" value="0" ng-model="AddInventory.Insurance" />&nbsp No
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12" ng-show="AddInventory.Insurance !== '0'">
                            <div class="form-group">
                                <label for="txtcode">Insurance Date Up To</label>
                                <div class="input-group date" id='INSU_DT'>
                                    <input type="text" class="form-control" data-ng-model="AddInventory.INSU_DT" id="Text2" name="INSU_DT" required="" placeholder="dd-mm-yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('INSU_DT')"></i>
                                    </span>
                                </div>
                            </div>
                            <%--                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.FromDate.$invalid" style="color: red;">Please select Bill Date</span>--%>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12" ng-show="AddInventory.Insurance !== '0'">
                            <div class="form-group" id="Div5">
                                <label>Upload Insurance Documents</label>
                                <div style="width: 100px">
                                    <i class="fa fa-folder-open-o fa-lg"></i>
                                    <input type="file" name="file2" onchange="angular.element(this).scope().fileUpload(this,'INS_DOC')" multiple="multiple" data-ng-model="AddInventory.INS_DOC" id="file2" required=""
                                        accept=".png,.jpg,.xlsx,.pdf,.docx">
                                    <%--<span ng-if="data.PocPhoto != undefined">
                                                        <button ng-click="cancelSelectedFile(data.PocPhoto,'POC_Photo','file1')" " class="btn btn-link"><i class="bi bi-trash"></i></button>
                                                    </span>--%>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary" data-ng-hide="save==true" data-ng-click="submit()">Add</button>

                        <button id="Save" data-ng-hide="modify==true" class="btn btn-primary custom-button-color" data-ng-click="Modify()">Modify</button>
                        <button id="backbtn" class="btn btn-primary" data-ng-click="back()">Back</button>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box-footer text-left">
                            </div>
                            <div class="d-flex">
                            <input type="text" class="form-control" id="filtertxt" data-ng-model="AddInventory.Search"placeholder="Filter by any..." style="width: 20%" />
                            
                                <button type="button" class="btn btn-primary" style="margin-left: 10px;" data-ng-click="Bindgrid()">Search</button>
                            </div>
                                <div data-ag-grid="gridOptions" style="height: 250px; width: auto" class="ag-blue"></div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
        var CompanySessionId = '<%= Session["TENANT"]%>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/AddAssetInventory.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
</body>
</html>
