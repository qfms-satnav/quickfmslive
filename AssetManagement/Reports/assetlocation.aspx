﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="assetlocation.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_assetlocation" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Ast Loc Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Capital Asset Allocation Report </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ValidationGroup="Val1" ForeColor="Red" />
                                     <asp:updatepanel runat="server">
                                      <ContentTemplate>
                                <div class="row">
                                    <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Category</label>
                                            <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select Asset Category" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Sub Category</label>
                                            <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select Asset Sub Category" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Asset Brand/Make</label>
                                            <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select Asset Brand/Make" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>
                                                Asset Model</label>
                                            <asp:DropDownList ID="ddlModel" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select Asset Model">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Location</label>
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                                ToolTip="Select Asset Model">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Duration</label><br />
                                            <select id="ddlRange" class="form-control selectpicker with-search" onchange="rangeChanged()">
                                                <option value="">Select Range</option>
                                                <option value="TODAY">Today</option>
                                                <option value="YESTERDAY">Yesterday</option>
                                                <option value="7">Last 7 Days</option>
                                                <option value="30">Last 30 Days</option>
                                                <option value="THISMONTH">This Month</option>
                                                <option value="LASTMONTH">Last Month</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>From Date</label>
                                            <div class='input-group date' id='Div1'>
                                                <asp:TextBox ID="FromDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>To Date</label>
                                            <div class='input-group date' id='Div4'>
                                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" placeholder="mm/dd/yyyy" MaxLength="10"> </asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <label>Company</label>
                                            <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" ToolTip="Select company">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button ID="Button1" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row table table table-condensed table-responsive">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                        </div>
                                    </div>
                                </div>
                                          </ContentTemplate>
                                         </asp:updatepanel>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

<script src="../../Scripts/moment.min.js" defer></script>
<script type="text/javascript" defer>
    $(document).ready(function () {
        $("#ddlRange").val('');
        rangeChanged();
    });

    function rangeChanged() {
        var selVal = $("#ddlRange").val();
        switch (selVal) {
            case 'TODAY':
                $("#FromDate").val(moment().format('MM/DD/YYYY'));
                $("#txtToDate").val(moment().format('MM/DD/YYYY'));
                break;
            case 'YESTERDAY':
                $("#FromDate").val(moment().subtract(1, 'days').format('MM/DD/YYYY'));
                $("#txtToDate").val(moment().subtract(1, 'days').format('MM/DD/YYYY'));
                break;
            case '7':
                $("#FromDate").val(moment().subtract(6, 'days').format('MM/DD/YYYY'));
                $("#txtToDate").val(moment().format('MM/DD/YYYY'));
                break;
            case '30':
                $("#FromDate").val(moment().subtract(29, 'days').format('MM/DD/YYYY'));
                $("#txtToDate").val(moment().format('MM/DD/YYYY'));
                break;
            case 'THISMONTH':
                $("#FromDate").val(moment().startOf('month').format('MM/DD/YYYY'));
                $("#txtToDate").val(moment().endOf('month').format('MM/DD/YYYY'));
                break;
            case 'LASTMONTH':
                $("#FromDate").val(moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY'));
                $("#txtToDate").val(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY'));
                break;
            case '':
                $("#FromDate").val('');
                $("#txtToDate").val('');
                break;
        }
    }

    $("#ddlRange").change(function () {
        rangeChanged();
    });
</script>
<script>
    function refreshSelectpicker() {
        $("#<%=ddlAssetCategory.ClientID%>").selectpicker();
        $("#<%=ddlAstSubCat.ClientID%>").selectpicker();
        $("#<%=ddlAstBrand.ClientID%>").selectpicker();
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%=ddlModel.ClientID%>").selectpicker();
        $("#<%=ddlCompany.ClientID%>").selectpicker();

    }
    refreshSelectpicker();
    </script>