﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QRCoder;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;


public partial class AssetManagement_Reports_AssetQRCodeGeneratorReport : System.Web.UI.Page
{
    clsSubSonicCommonFunctions ObjSubsonic = new clsSubSonicCommonFunctions();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


           
            DataSet ds = new DataSet();
            ds = ds = (DataSet)ObjSubsonic.GetSubSonicDataSet("ASSET_QR_CODE_READER");
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "AssetQRCodeRpt";        
            rds.Value = ds.Tables[0];          
            ReportViewer1.Reset();       

            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/DrillDownReports/DDL_RDLC/AssetQRCodeReport.rdlc");
          
            ReportViewer1.Visible = true;
            ReportViewer1.LocalReport.EnableExternalImages = true;
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.LocalReport.EnableHyperlinks = true;
         
        }
    }
  

   
}