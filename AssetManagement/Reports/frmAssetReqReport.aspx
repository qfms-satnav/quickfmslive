<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmAssetREQreport.aspx.vb" MaintainScrollPositionOnPostback="true"
    Inherits="frmAssetREQreport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        .my_popup {
           fade :1;
        }
    </style>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Ast Mvmt Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Capital Asset Requisition Report </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="VerticalValidations" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                                
                                                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="row">
                                    <asp:Label ID="Label1" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Asset Category</label>
                                <asp:DropDownList ID="ddlAssetCategory" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" ToolTip="Select Asset Category" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Asset Subcategory</label>
                                <asp:DropDownList ID="ddlAstSubCat" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" ToolTip="Select Asset Sub Category" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Asset Brand/Make</label>
                                <asp:DropDownList ID="ddlAstBrand" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true"
                                    ToolTip="Select Asset Brand/Make" AutoPostBack="True" OnSelectedIndexChanged="ddlAstBrand_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Asset Model</label>
                                <asp:DropDownList ID="ddlModel" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" ToolTip="Select Asset Model">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Location</label>
                                <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" ToolTip="Select Asset Model">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Duration</label>
                                <br />
                                <select id="ddlRange" class="form-control selectpicker with-search" onChange="rangeChanged()">
                                    <option value="">Select Range</option>
                                    <option value="TODAY">Today</option>
                                    <option value="YESTERDAY">Yesterday</option>
                                    <option value="7">Last 7 Days</option>
                                    <option value="30">Last 30 Days</option>
                                    <option value="THISMONTH">This Month</option>
                                    <option value="LASTMONTH">Last Month</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>From Date</label>
                                <div class='input-group date' id='Div1'>
                                    <asp:TextBox ID="FromDate" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy" MaxLength="10"> </asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('Div1')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>To Date</label>
                                <div class='input-group date' id='Div4'>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" placeholder="dd/mm/yyyy" MaxLength="10"> </asp:TextBox>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('Div4')"></span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Company</label>
                                <asp:DropDownList ID="ddlCompany" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" ToolTip="Select company">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <br />
                                <asp:Button ID="btnSubmit" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" />
                            </div>
                        </div>
                    </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="overflow:scroll">
                                            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Width="100%"></rsweb:ReportViewer>
                                              <br />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%-- Modal popup block --%>

    <div class="modal fade my_popup" id="myModal6" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                     <button type="button" class="close2" data-dismiss="modal" aria-label="Close2"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Asset Requisition Details</h4>
                </div>
                <div class="modal-body">
                    <iframe id="modalcontentframe6" style="border: none;min-height:380px;width:100%"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>
        function showPopWin6(id) {
            $("#myModal6").modal('show');
            $("#modalcontentframe6").attr("src", "/AssetManagement/Reports/frmAssetReqReportDtls.aspx?reqid=" + id);
            //$("#modalcontentframe").load("frmAssetReqReportDtls.aspx?reqid=" + id, function (responseTxt, statusTxt, xhr) {
            //    $("#myModal1").modal('show');
            //});
        }
        //$(".close2").click(function () {
        //    window.location = "/AssetManagement/Reports/frmAssetReqReport.aspx"
        //});
    </script>

    <%-- Modal popup block--%>
</body>
</html>
