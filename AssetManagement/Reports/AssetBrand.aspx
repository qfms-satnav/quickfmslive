﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AssetBrand.aspx.cs" Inherits="AssetManagement_Reports_AssetBrand" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <%--<link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
<link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <%--<link href="Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />--%>
</head>
<body data-ng-controller="AssetBrandController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Asset Brand Master</h3>
            </div>
            <div class="card">
                <form name="assetbrand" id="form1" ng-submit="Save()" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetbrand.$submitted && assetbrand.manufactuer_code.$invalid}">
                                <label>Asset Brand/Make Code<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                    <input name="manufactuer_code" id="BrandCode" class="form-control selectpicker" data-ng-model="brand.manufactuer_code" ng-disabled="isEditMode" required="" />
                                    <span class="error" data-ng-show="assetbrand.$submitted && assetbrand.manufactuer_code.$invalid">Asset Brand Code is required.</span>
                                </div>


                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetbrand.$submitted && assetbrand.manufacturer.$invalid}">
                                <label>Asset Brand/Make Name<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter name in alphabets,numbers')" onmouseout="UnTip()">
                                    <input name="manufacturer" id="BrandName" class="form-control selectpicker" data-ng-model="brand.manufacturer" required=" " />
                                    <span class="error" data-ng-show="assetbrand.$submitted && assetbrand.manufacturer.$invalid">Asset Brand Name is required.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetbrand.$submitted && assetbrand.VT_TYPE.$invalid}">
                                <div>

                                    <label>Asset Category<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="AssetMainCategoryList" data-output-model="AssetMainCategoryList.VT_CODE" button-label="icon VT_TYPE" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon VT_TYPE" data-tick-property="ticked" data-on-item-click="onBindSubcategory()" 
                                        data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="AssetMainCategoryList.VT_CODE" name="VT_TYPE" style="display: none" required="" />
                                    <span class="error" data-ng-show="assetbrand.$submitted && assetbrand.VT_TYPE.$invalid" style="color: red">Please Select Asset Category </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetbrand.$submitted && assetbrand.AST_SUBCAT_NAME.$invalid}">
                                <label>Asset Subcategory<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="AssetSubCategoryList" data-output-model="AssetSubCategoryList.AST_SUBCAT_CODE" button-label="icon AST_SUBCAT_NAME" ng-disabled="disabled" data-is-disabled="EnableStatus==0"
                                    data-item-label="icon AST_SUBCAT_NAME" data-tick-property="ticked"
                                    data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="AssetSubCategoryList.AST_SUBCAT_CODE" name="AST_SUBCAT_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="assetbrand.$submitted && assetbrand.AST_SUBCAT_NAME.$invalid" style="color: red">Please Select Asset Subcategory </span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="text-align: left">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': assetbrand.$submitted && assetbrand.MANUFACTUER_STATUS.$invalid}" >
                                <label>Status<span style="color: red;">*</span></label>
                              
                                <select id="MANUFACTUER_STATUS" name="MANUFACTUER_STATUS" data-ng-model="brand.MANUFACTUER_STATUS" class="form-control" data-live-search="true" required="">
                                    <option value="">--Select--</option>
                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>

                                </select>
                                <span class="error" data-ng-show="assetbrand.$submitted && assetbrand.MANUFACTUER_STATUS.$invalid" style="color: red">Please Select status</span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                    <textarea name="MANU_REM" id="BrandRemarks" class="form-control" style="height: 30%;" data-ng-model="brand.MANU_REM">
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 23px">
                            <div class="form-group">

                                <input type="submit" name="BrandSubmit" value="{{buttonText}}" class="btn btn-default btn-primary" />
                                <input type="submit" value="Modify" class="btn btn-default btn-primary" data-ng-show="ActionStatus==1" />
                                <a class="btn btn-default btn-primary" href="javascript:history.back()">Back</a>

                            </div>
                        </div>
                    </div>

                </form>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">

                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 510px; width: auto;"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <%--<script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>--%>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <!-- AngularJS and other JS libraries -->
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
        var CompanySessionId = '<%= Session["TENANT"]%>';

    </script>
    <%--    <script src="path/to/bootstrap-select.min.js"></script>--%>


    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/AssetBrand.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>

</body>
</html>
