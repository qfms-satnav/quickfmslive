﻿app.service("AssetSparePartsService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetZone = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/GetZone', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetAccestData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/GetAccestData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetAssetDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/GetAssetDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.updateData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/updateData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetVendor = function (data) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/GetVendor', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetSpareSub = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/GetSpareSub', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetSpareBrand = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/GetSpareBrand', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetSpareModel = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/GetSpareModel', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/SaveData', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetSpareDumpDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetSpareParts/DownloadTemplate?loc=' + data + ' ')
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('AssetSparePartsController', ['$scope', '$q', 'UtilityService', 'AssetSparePartsService', '$filter', '$timeout', "$rootScope",
    function ($scope, $q, UtilityService, AssetSparePartsService, $filter, $timeout, $rootScope) {

        $scope.QRCodeGenerator = {};
        $scope.LoadInfo = [];
        $scope.AssetSpareParts = {};
        $scope.AssetSpareParts1 = {};
        $scope.Type = [];
        $scope.SUBC_NAME = [];
        $scope.search = [];
        $scope.Location = [];
        $scope.Country = [];
        //$scope.City = [];
        $scope.Zone = [];
        $scope.Tower = [];
        $scope.AAS_MODEL_NAME = [];
        $scope.SPARE_SUB_CATEGORY = [];
        $scope.SPARE_MODEL = [];
        $scope.SPARE_BRAND = [];
        $scope.AAS_AAB_NAME = [];
        $scope.AAS_SPAREPART_COST = [];
        $scope.AAS_SPAREPART_NAME = [];
        $scope.AVR_NAME = [];
        $scope.AAS_SPAREPART_AVBQUANTITY = [];
        $scope.AAS_SPAREPART_MINQUANTITY = [];
        $scope.AAS_AAT_CODE = [];
        $scope.AAS_SPAREPART_ID = [];
        $scope.AAS_SPAREPART_DES = [];
        $scope.AAS_ASP_CODE = [];
        $scope.AAS_ID = [];
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.Edit = false;
        //UtilityService.getCountires(2).then(function (response) {
        //    if (response.data != null) {
        //        $scope.Country = response.data;
        //        angular.forEach($scope.Country, function (value, key) {
        //            value.ticked = true;
        //        });
        //        AssetSparePartsService.GetZone().then(function (response) {
        //            if (response != null) {
        //                //$scope.City = response.data;
        //                $scope.Zone = response;
        //                //angular.forEach($scope.City, function (value, key) {                       
        //                angular.forEach($scope.Zone, function (value, key) {
        //                    value.ticked = true;
        //                });
        //                UtilityService.getLocations(2).then(function (response) {
        //                    if (response.data != null) {
        //                        $scope.Location = response.data;
        //                        angular.forEach($scope.Location, function (value, key) {
        //                            value.ticked = true;
        //                        });
        //                        UtilityService.getTowers(2).then(function (response) {
        //                            if (response.data != null) {
        //                                $scope.Tower = response.data;
        //                                angular.forEach($scope.Tower, function (value, key) {
        //                                    value.ticked = true;
        //                                });
        //                                AssetSparePartsService.GetVendor().then(function (response) {

        //                                    if (response.data != null) {
        //                                        $scope.Vendor = response.data;
        //                                        angular.forEach($scope.Vendor, function (value, key) {
        //                                            var a = _.find($scope.Vendor);
        //                                            a.ticked = true;

        //                                        });
        //                                    }
        //                                });
        //                            }
        //                        });
        //                    }
        //                });
        //            }
        //        });
        //    }
        //});




        function validatinFn() {
            //var bool = false;
            //if ($scope.AssetSpareParts.City.length <= 0 && $scope.AssetSpareParts.City[0].CTY_CODE.length <= 0) {           
            //if ($scope.AssetSpareParts.Zone.length <= 0 && $scope.AssetSpareParts.Zone[0].ZONE_ID.length <= 0) {
            //    return false;
            //}
            if ($scope.AssetSpareParts.Location.length <= 0 && $scope.AssetSpareParts.Location[0].LCM_CODE.length <= 0) {
                return false;
            }
            //if ($scope.AssetSpareParts.Tower.length <= 0 && $scope.AssetSpareParts.Tower[0].TWR_CODE <= 0) {
            //    return false;
            //}
            if ($scope.AssetSpareParts1.AAS_SPAREPART_NAME.length == 0) {
                return false;
            }
            if ($scope.AssetSpareParts1.AAS_SPAREPART_DES.length == 0) {
                return false;
            }
            if ($scope.AssetSpareParts1.AAS_SPAREPART_AVBQUANTITY.length == 0) {
                return false;
            }
            if ($scope.AssetSpareParts1.AAS_SPAREPART_MINQUANTITY.length == 0) {
                return false;
            }
            if ($scope.AssetSpareParts1.AAS_SPAREPART_COST.length == 0) {
                return false;
            }
            if ($scope.AssetSpareParts1.AAS_RACK_NO.length == 0) {
                return false;
            }
            if ($scope.AssetSpareParts1.ASS_LEAD_TIME.length == 0) {
                return false;
            }
            return true;
        }

        $scope.fn_showHide = function () {
            $('#dispPage').show();
            $('#dispGrid').hide();
            $('#dispButton').hide();
            $('#btnsearch').hide();
            $('#dispBackButton').show();
        }
        $scope.fn_Back = function () {
            $('#dispPage').hide();
            $('#dispGrid').show();
            $('#dispButton').show();
            $('#btnsearch').show();
            $('#dispBackButton').hide();
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.Edit = false;
            $scope.ClearData();
        }

        $scope.SaveA = function () {

            if (validatinFn()) {

                var obj = {
                    AAS_CTY_CODE: '',//$scope.AssetSpareParts.City[0].CTY_CODE,
                    AAS_LOC_ID: $scope.AssetSpareParts.Location[0].LCM_CODE,
                    AAS_TOWER_CODE: '',//$scope.AssetSpareParts.Tower[0].TWR_CODE,
                    AAS_AAB_NAME: $scope.AssetSpareParts1.AAS_AAB_NAME,
                    AAS_MODEL_NAME: $scope.AssetSpareParts1.AAS_MODEL_NAME,
                    AAS_SUB_NAME: $scope.AssetSpareParts1.AAS_SUB_NAME,
                    AAS_SPAREPART_COST: $scope.AssetSpareParts1.AAS_SPAREPART_COST,
                    AAS_SPAREPART_NAME: $scope.AssetSpareParts1.AAS_SPAREPART_NAME,
                    AAS_SPAREPART_DES: $scope.AssetSpareParts1.AAS_SPAREPART_DES,
                    AAS_AVR_CODE: $scope.AssetSpareParts.AVR_NAME[0].AVR_NAME == "OTHER" ? $scope.AssetSpareParts1.NEW_VENDOR_NAME : $scope.AssetSpareParts.AVR_NAME[0].AVR_NAME,
                    AAS_SPAREPART_AVBQUANTITY: $scope.AssetSpareParts1.AAS_SPAREPART_AVBQUANTITY,
                    AAS_SPAREPART_MINQUANTITY: $scope.AssetSpareParts1.AAS_SPAREPART_MINQUANTITY,
                    Type: '',//$scope.AssetSpareParts.Type[0].SUBC_NAME,
                    AAS_RACK_NO: $scope.AssetSpareParts1.AAS_RACK_NO,
                    AAS_VED: $scope.AssetSpareParts1.AAS_VED,
                    AAS_FSN: '',//$scope.AssetSpareParts1.AAS_FSN,
                    AAS_AAT_CODE: '',
                    ASS_LEAD_TIME: $scope.AssetSpareParts1.ASS_LEAD_TIME,
                    AAS_SPAREPART_QUANTITY: $scope.AssetSpareParts1.AAS_SPAREPART_QUANTITY,
                    AAS_REMARKS: $scope.AssetSpareParts1.AAS_REMARKS,
                    AAS_SPAREPART_NUMBER: $scope.AssetSpareParts1.AAS_SPAREPART_NUMBER
                    //AAS_AVR_CODE: $scope.AssetSpareParts.Vendor[0].AVR_NAME
                }

                AssetSparePartsService.SaveData(obj).then(function (response) {
                    if (response != null) {
                        showNotification('success', 8, 'bottom-right', 'Data Saved Successfully');
                        //$scope.LoadData();
                        $timeout($scope.LoadData, 3000);
                        //$timeout($scope.SearchA, 3000);
                        $scope.ClearData();

                        $('#dispPage').hide();
                        $('#dispGrid').show();
                        $('#dispButton').show();
                        $('#btnsearch').show();
                        $('#dispBackButton').hide();
                    }
                    else
                        showNotification('error', 8, 'bottom-right', response.Message);
                }, function (response) {
                    progress(0, '', false);

                });
            }

        }

        $scope.Update = function () {
            var obj = {
                AAS_CTY_CODE: '',
                AAS_ID: $scope.AssetSpareParts1.AAS_ID,
                AAS_LOC_ID: '',
                AAS_TOWER_CODE: '',
                AAS_AAB_NAME: $scope.AssetSpareParts1.AAS_AAB_NAME,
                AAS_MODEL_NAME: $scope.AssetSpareParts1.AAS_MODEL_NAME,
                AAS_AVR_CODE: $scope.AssetSpareParts.AVR_NAME[0].AVR_NAME == "OTHER" ? $scope.AssetSpareParts1.NEW_VENDOR_NAME : $scope.AssetSpareParts.AVR_NAME[0].AVR_NAME,
                AAS_SUB_NAME: $scope.AssetSpareParts1.AAS_SUB_NAME,
                AAS_SPAREPART_COST: $scope.AssetSpareParts1.AAS_SPAREPART_COST,
                AAS_SPAREPART_NAME: $scope.AssetSpareParts1.AAS_SPAREPART_NAME,
                AAS_SPAREPART_DES: $scope.AssetSpareParts1.AAS_SPAREPART_DES,
                AAS_SPAREPART_AVBQUANTITY: $scope.AssetSpareParts1.AAS_SPAREPART_AVBQUANTITY,
                AAS_SPAREPART_MINQUANTITY: $scope.AssetSpareParts1.AAS_SPAREPART_MINQUANTITY,
                AAS_RACK_NO: $scope.AssetSpareParts1.AAS_RACK_NO,
                AAS_VED: $scope.AssetSpareParts1.AAS_VED,
                AAS_FSN: $scope.AssetSpareParts1.AAS_FSN,
                ASS_LEAD_TIME: $scope.AssetSpareParts1.ASS_LEAD_TIME,
                AAS_SPAREPART_QUANTITY: $scope.AssetSpareParts1.AAS_SPAREPART_QUANTITY,
                AAS_AAT_CODE: '',
                AAS_REMARKS: $scope.AssetSpareParts1.AAS_REMARKS,
                AAS_SPAREPART_NUMBER: $scope.AssetSpareParts1.AAS_SPAREPART_NUMBER
            }

            AssetSparePartsService.updateData(obj).then(function (response) {
                if (response.data != null) {
                    //$scope.Update = response.data;
                    //angular.forEach($scope.Update, function (value, key) {
                    //    var a = _.find($scope.Update);
                    //    a.ticked = true;
                    //}); 
                    $scope.LoadData();
                    $scope.ClearData();
                    $('#dispPage').hide();
                    $('#dispGrid').show();
                    $('#dispButton').show();
                    $('#btnsearch').show();
                    $('#dispBackButton').hide();
                    $scope.ActionStatus = 0;
                    $scope.IsInEdit = false;
                    $scope.Edit = false;
                    showNotification('success', 8, 'bottom-right', 'Data update Successfully');
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
        }

        $scope.getCitiesbyCny = function () {
            UtilityService.getCitiesbyCny($scope.AssetSpareParts.Country, 2).then(function (response) {
                $scope.City = response.data;
            }, function (error) {
                console.log(error);
            });
        }
        $scope.cnySelectAll = function () {
            $scope.AssetSpareParts.Country = $scope.Country;
            $scope.getCitiesbyCny();
        }

        $scope.locSelectAll = function () {
            $scope.AssetSpareParts.Locations = $scope.Locations;
            $scope.Locations();
        }

        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };

        /*Auto Complete Free Text*/
        $rootScope.$on('automComChng', function (event, item, idx) {

            if (idx == 'sub') {
                $scope.AssetSpareParts.SPARE_SUB_CATEGORY = item;
            }
            else if (idx == 'Brand') {
                $scope.AssetSpareParts.SPARE_BRAND = item;
            }
            else if (idx == 'Model') {
                $scope.AssetSpareParts.SPARE_MODEL = item;
            }
        });

        $scope.selectedEmp = function (selected, typ) {

            if (selected) {
                $scope.selectedEmployee = selected.originalObject;
                if (typ != 'E')
                    $scope.$broadcast('angucomplete-alt:changeInput', 'sub', $scope.selectedEmployee.VT_TYPE);
                if (selected.originalObject.NAME != null && selected.originalObject.NAME != undefined)
                    $scope.AssetSpareParts.SPARE_SUB_CATEGORY = selected.originalObject.NAME;
            }
        };

        $scope.selectedBrand = function (selected) {
            if (selected) {
                $scope.selectedEmployee = selected.originalObject;
                $scope.$broadcast('angucomplete-alt:changeInput', 'Brand', $scope.selectedEmployee.VT_TYPE);
                if (selected.originalObject.NAME != null && selected.originalObject.NAME != undefined)
                    $scope.AssetSpareParts.SPARE_BRAND = selected.originalObject.NAME;
            }
        };

        $scope.selectedModel = function (selected) {
            if (selected) {
                $scope.selectedEmployee = selected.originalObject;
                $scope.$broadcast('angucomplete-alt:changeInput', 'Model', $scope.selectedEmployee.VT_TYPE);
                if (selected.originalObject.NAME != null && selected.originalObject.NAME != undefined)
                    $scope.AssetSpareParts.SPARE_MODEL = selected.originalObject.NAME;
            }
        };

        $scope.LoadData = function () {
            progress(0, 'Loading...', true);
            //alert('ss');
            //$scope.GetLCMZone();

        }
        $timeout($scope.LoadData, 1000);

        $scope.EditFunction = function (data) {
            $('#dispPage').show();
            $('#dispGrid').hide();
            $('#dispButton').hide();
            $('#btnsearch').hide();
            $('#dispBackButton').show();
            $scope.AssetSpareParts = {};
            $scope.remoteUrlRequestFn(data.AAS_SUB_NAME);
            $scope.remoteUrlRequestFn(data.AAS_MODEL_NAME);
            $scope.remoteUrlRequestFn(data.AAS_AAB_NAME);
            angular.forEach($scope.Zone, function (value, key) {
                value.ticked = false;
                if (value.ZONE_ID.toString() == data.LCM_ZONE) {
                    value.ticked = true;
                }
            });
            angular.forEach($scope.Location, function (value, key) {
                value.ticked = false;
                if (value.LCM_CODE.toString() == data.AAS_LOC_ID) {
                    value.ticked = true;
                }
            });
            //angular.forEach($scope.Vendor, function (value, key) {
            //    value.ticked = false;
            //    if (value.AVR_NAME.toString() == data.AAS_AVR_CODE) {
            //        value.ticked = true;
            //    }
            //});

            $scope.Vendor = [];
            var obj = {
                LCM_NAME: _.filter($scope.Location, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),

            }
            AssetSparePartsService.GetVendor(obj).then(function (response) {
                $scope.Vendor = response.data;
                angular.forEach($scope.Vendor, function (value, key) {
                value.ticked = false;
                if (value.AVR_NAME.toString() == data.AAS_AVR_CODE) {
                    value.ticked = true;
                }
            });
            });

            $scope.EditCategory = data;
            angular.copy(data, $scope.AssetSpareParts1);
            angular.copy(data, $scope.AssetSpareParts);
            $scope.ActionStatus = 1;
            $scope.IsInEdit = true;
            $scope.Edit = true;

        }
        var columnDefs = [
            { headerName: "Edit", width: 40, height: 150, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
            { headerName: "QR Code", field: "ASS_QR_CODE", width: 150, height: 150, cellRenderer: deltaIndicator },
            { headerName: "Vendor", field: "AAS_AVR_CODE", width: 150, cellClass: 'grid-align' },
            { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Priority", field: "AAS_VED", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "QFMS - Part Number", field: "AAS_ASP_CODE", width: 150, cellClass: 'grid-align' },
            { headerName: "Part Name", field: "AAS_SPAREPART_NAME", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Spare Part No", field: "AAS_SPAREPART_NUMBER", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Part Specification", field: "AAS_SPAREPART_DES", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Model", field: "AAS_MODEL_NAME", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Supplier", field: "AAS_AAB_NAME", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Storage Location", field: "AAS_RACK_NO", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "MSQ(Min Stock QTY)", field: "AAS_SPAREPART_MINQUANTITY", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "ASQ (Actual Stock QTY)", field: "AAS_SPAREPART_AVBQUANTITY", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Measure (UOM)", field: "AAS_SPAREPART_QUANTITY", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Safety Stock (MSQ-ASQ)", field: "SAFEQTY", width: 100, cellClass: 'grid-align' },
            { headerName: "Unit Cost", field: "AAS_SPAREPART_COST", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Total Cost", field: "AAS_SPAREPART_TOTCOST", cellClass: "grid-align", width: 150, height: 150 },            
            { headerName: "Lead Time", field: "ASS_LEAD_TIME", cellClass: "grid-align", width: 150, height: 150 },
            //{ headerName: "FSN", field: "AAS_FSN", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Zone", field: "LCM_ZONE", cellClass: "grid-align", width: 150, height: 150 },
            //{ headerName: "City", field: "AAS_CTY_CODE", cellClass: "grid-align", width: 150, height: 150 },
            //{ headerName: "Tower", field: "AAS_TOWER_CODE", cellClass: "grid-align", width: 150, height: 150 },
            { headerName: "Assid", field: "AAS_SPAREPART_ID", cellClass: "grid-align", hide: "true", width: 150, height: 150 },
            { headerName: "Category", field: "AAS_SUB_NAME", cellClass: "grid-align", width: 150, height: 150 },
            //{ headerName: "Sub Category", field: "AAS_SUB_NAME", cellClass: "grid-align", width: 150, height: 150 },                  
           
            { headerName: "Remarks", field: "AAS_REMARKS", cellClass: "grid-align", width: 150, height: 150 },
        ];
        $scope.gridOptions = {
            columnDefs: '',
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,

            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            },
            rowHeight: 80
        };

        function deltaIndicator(params) {

            var element = document.createElement("span");
            var DivElement = document.createElement("div");
            $(DivElement).qrcode(
                {
                    width: 70,
                    height: 70,
                    text: params.data.AAS_SPAREPART_NAME + ',' + params.data.AAS_SPAREPART_ID
                });
            element.appendChild(DivElement);
            element.appendChild(document.createTextNode(params.value));
            return element;
        }
        $scope.SearchA = function () {
            var obj2 = {
                LCM_NAME: _.filter($scope.Location, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                //AAS_AAT_CODE: _.filter($scope.Type, function (o) { return o.ticked == true; }).map(function (x) { return x.SUBC_CODE; }).join(',')
                AAS_AVR_CODE: _.filter($scope.Vendor, function (o) { return o.ticked == true; }).map(function (x) { return x.AVR_NAME; }).join(',')
            }
            AssetSparePartsService.GetAccestData(obj2).then(function (response) {
                if (response != null) {
                    $scope.gridata = response.data;
                    $scope.LoadInfo = $scope.gridata;
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);



                }
                else {
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                }
                progress(0, 'Loading...', false);
            })
        }

        $scope.GetAssetDetails = function () {
            var obj = {
                LCM_NAME: _.filter($scope.Location, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),

            }
            AssetSparePartsService.GetVendor(obj).then(function (response) {
                $scope.Vendor = response.data;
                angular.forEach($scope.Vendor, function (value, key) {
                    var a = _.find($scope.Vendor);
                    a.ticked = true;

                });
                $scope.SearchA();
            });
            //AssetSparePartsService.GetAssetDetails(obj).then(function (response) {

            //    if (response.data != null) {
            //        $scope.Type = response.data;
            //    }
            //    else
            //        showNotification('error', 8, 'bottom-right', response.Message);
            //}, function (response) {
            //    progress(0, '', false);
            //});
        }

        $scope.LoadData = function () {
            var searchval = $("#filtertxt").val();
            var dataObj = {
                SearchValue: searchval
            };
            AssetSparePartsService.GetZone().then(function (response) {
                $scope.Zone = response;
                //UtilityService.getLocations(2).then(function (response) {
                UtilityService.getLocationsInventory(2).then(function (response) {
                    $scope.Location = response.data;
                    angular.forEach($scope.Location, function (value, key) {
                        var a = _.find($scope.Location);
                        a.ticked = true;

                    });
                    $scope.GetAssetDetails();
                    //UtilityService.getTowers(2).then(function (response) {
                    // $scope.Tower = response.data;
                    //AssetSparePartsService.GetVendor().then(function (response) {
                    //    $scope.Vendor = response.data;
                    //    angular.forEach($scope.Vendor, function (value, key) {
                    //        var a = _.find($scope.Vendor);
                    //        a.ticked = true;

                    //    });
                    //    $scope.SearchA();
                    //var obj = {
                    //    LCM_NAME: _.filter($scope.Location, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),

                    //}
                    //AssetSparePartsService.GetAssetDetails(obj).then(function (response) {
                    //    $scope.Type = response.data;

                    //})
                    //})
                    //})
                })
            })



            //$scope.GenReport = function (Type) {               
            //progress(0, 'Loading...', true);
            //}
            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
            progress(0, '', false);
        }
        $scope.GenerateFilterPdf = function () {
            ;

        }
        //$scope.GenReport = function (data) {
        //    ;
        //    if (data == 'pdf') {
        //        var codes = {};
        //        const CodeVales = [];
        //        angular.forEach($scope.LoadInfo, function (val) {
        //            CodeVales.push({ "AAS_SPAREPART_NAME": val.AAS_SPAREPART_NAME, "AAS_SPAREPART_ID": val.AAS_SPAREPART_ID });

        //        }); 
        //        qr_generate(CodeVales);
        //    }
        //}

        $scope.ExcelDownload = function () {
            var obj = {
                LCM_CODE: $('#drplocation :selected').text()
            }
            debugger
            if ($('#drplocation :selected').text() == "") {
                showNotification('error', 8, 'bottom-right', 'Please Select Location');
                return null;
            }
            AssetSparePartsService.GetSpareDumpDetails($scope.AssetSpareParts.Location.toString()).then(function (response) {

                const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
                const EXCEL_EXTENSION = '.xlsx';
                progress(0, 'Loading...', true);
                debugger
                if (response != null) {
                    var fileName = 'SpareDumpDetails.xlsx';
                    var bytes = Base64ToBytes(response.data.data);
                    var blob = new Blob([bytes], { type: "application/octetstream" });

                    //Check the Browser type and download the File.
                    var isIE = false || !!document.documentMode;
                    if (isIE) {
                        window.navigator.msSaveBlob(blob, fileName);
                        progress(0, 'Loading...', false);
                    } else {
                        progress(0, 'Loading...', false);
                        var url = window.URL || window.webkitURL;
                        link = url.createObjectURL(blob);
                        var a = $("<a />");
                        a.attr("download", fileName);
                        a.attr("href", link);
                        $("body").append(a);
                        a[0].click();
                        $("body").remove(a);

                    }

                    const worksheet = XLSX.utils.json_to_sheet(response.data.data);
                    const workbook = {
                        Sheets: { 'SpareDump': worksheet },
                        SheetNames: ['SpareDump']
                    };
                    const excelBuffer = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
                    const exceldata = new Blob([excelBuffer], { type: EXCEL_TYPE });
                    saveAs(exceldata, "SpareDump_" + moment(new Date()).format('YYYY - MM - DD HH: MM: SS') + EXCEL_EXTENSION);
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', 'Downloaded..!');

                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                
                progress(0, response.Message, false);
            });
        }
        function Base64ToBytes(base64) {
            var s = window.atob(base64);
            var bytes = new Uint8Array(s.length);
            for (var i = 0; i < s.length; i++) {
                bytes[i] = s.charCodeAt(i);
            }
            return bytes;
        };

        $scope.GenReport = function (data) {
            progress(0, 'Loading...', true);
            setTimeout(function () {
                progress(0, 'Loading...', false);
                if (data == 'pdf') {
                    const songs = [];
                    angular.forEach($scope.LoadInfo, function (val) {
                        songs.push({ "AAT_CODE": val.AAS_SPAREPART_NAME + ',' + val.AAS_SPAREPART_ID, "AAT_NAME": val.AAS_SPAREPART_DES });
                    });
                    qr_generate(songs);
                    progress(0, 'Loading...', false);
                }
            }, 3000);
        }

        $timeout($scope.LoadData, 1000);
        $scope.ClearData = function () {

            //$scope.AssetSpareParts = {};
            $scope.AssetSpareParts1 = {};

            $scope.$broadcast('angucomplete-alt:clearInput');

        }
        //$scope.changeevt = function () {
        //    alert("Country Name: " + );
        //};

        $scope.UploadFile = function () {

            if ($('#File3', $('#form1')).val()) {
                progress(0, 'Loading...', true);
                var filetype = $('#File3', $('#form1')).val().substring($('#File3', $('#form1')).val().lastIndexOf(".") + 1);
                if (filetype == "xlsx" || filetype == "xls") {
                    if (!window.FormData) {
                        redirect(); // if IE8
                    }
                    else {

                        var formData = new FormData();
                        var UplFile = $('#File3')[0];
                        var CurrObj = $scope.SaveExpUtility;
                        formData.append("ExUpload", UplFile.files[0]);
                        formData.append("CurrObj", JSON.stringify(CurrObj));
                        formData.append("CurrLoc", JSON.stringify($scope.AssetSpareParts.Location));
                        $.ajax({
                            url: UtilityService.path + "/api/AssetSpareParts/UploadExcel",
                            type: "POST",
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                if (response.data != null) {
                                    $scope.$apply(function () {
                                        $scope.gridata = response.data;
                                        $scope.gridUploadExcel.api.setRowData([]);
                                        $scope.gridUploadExcel.api.setRowData($scope.gridata);
                                        $scope.gridUploadExcel.api.refreshHeader();
                                        $scope.tempAssetsobj = response.data;
                                        progress(0, '', false);
                                    });
                                    $scope.LoadData();
                                    progress(0, '', false);
                                    showNotification('success', 8, 'bottom-right', "Data uploaded Successfully.");

                                }
                                else {

                                    progress(0, '', false);
                                    showNotification('error', 8, 'bottom-right', response.Message);
                                }
                            }
                        });
                    }

                    $("#File3").val('');
                }
                else
                    progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Please Upload only Excel File(s)');
            }
            else
                showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');

        }

    }]);