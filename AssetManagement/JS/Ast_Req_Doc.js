﻿app.service("AssetService", function ($http, $q, UtilityService) {

  
    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetReport/GetData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


});


app.controller('AssetReportController', function ($scope, $q, $http, AssetService, UtilityService, $timeout, $filter) {
     $scope.RequisistionID = [];
   


    var columnDefs = [
        { headerName: "REQUISITION ID", field: "AIR_REQ_ID", width: 190, cellClass: 'grid-align' },
        {
            headerName: "DATE", field: "AIR_REQ_DATE", width: 190, cellClass: 'grid-align', cellRenderer: (data) =>  {
                return moment(data.AIR_REQ_DATE).format('MM/DD/YYYY' ) }},
        //{ headerName: "DOCUMENT", field: "Document", template: '<a data-ng-click="Download(FILENAME)">' + Document+'</a>' ,width: 280, cellClass: 'grid-align' }
       
        { headerName: "UPDATED BY", field: "AUR_FIRST_NAME", width: 200, cellClass: 'grid-align' },
        {  headerName: "DOCUMENT", field: "Document", width: 280, cellClass: 'grid-align', cellRenderer: function (params) {

                if (params.data.Document != null) {
                    return "<a href='../../UploadFiles/"  + params.data.Document + "' Download='" + params.data.Document + "'>" + params.data.Document + "</a>";
                }
                else {
                   return "No Document";
                }
                
            }
           
        }
            
    ]
   
  

    $scope.gridOptions = {
        columnDefs: columnDefs,
       
    }

  
    $scope.LoadData = function () {
        
        progress(0, 'Loading...', true);
        var params = {
            AID_REQ_ID: $scope.Asset.RequisistionID
        };
        AssetService.GetGriddata(params).then(function (response) {
            if (response.griddata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                $scope.LoadInfo = response.griddata;
                $scope.gridOptions.api.setRowData(response.griddata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 600);

            }
        }, function (error) {
            console.log(error);
        });
    }
 


     
    // $scope.Download = function (FILENAME) {
    //var a = document.createElement('a');
    //     a.href = '../../ UploadFiles / ' + companyid + ' /' + FILENAME;
    //a.target = '_blank';
    //     a.download = FILENAME ;
    //document.body.appendChild(a);
    //a.click();
    // }
});













