﻿app.service("AssetReconsilationReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetBarcodeAsset = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetReconsilationReport/GetBarcodeAsset', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }
    this.GetAssetReconName = function (data) {
        return handleAPIRequest($http.post(UtilityService.path + '/api/AssetReconsilationReport/AssetNameRecon', data));
    };
}]);
app.controller('AssetReconsilationReportController', function ($scope, UtilityService, $filter, $timeout, AssetReconsilationReportService, $http) {
    $scope.Asset_Reconciliation = {};
    $scope.AssetName = [];
    $scope.AssetNamesBind = [];
    $scope.AssetReccon = {
        fromdate: moment().subtract(30, 'days').format('MM/DD/YYYY'),
        todate: moment().format('MM/DD/YYYY')


    };
  

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        
        UtilityService.getLocations(1).then(function (response) {
            if (response.data != null) {
                $scope.Loclist = response.data;
                UtilityService.GetCategories(1).then(function (response) {
                    if (response.data != null) {
                        $scope.categorylist = response.data;
                        $scope.gridOptionsAsset.api.setRowData([]);
                        //angular.forEach($scope.AssetType, function (response) {
                        //    if (response.data != null) {
                        //        $scope.AssetType = response.data;
                        //    }
                        //})
                        //$scope.SubmitData();
                        $scope.gridOptions.api.setRowData([]);
                        progress(0, 'Loading...', false);
                    }
                });
            }
           
        });
       // progress(0, 'Loading...', true);
    }

    $scope.AssetType = [{
        TYP_NAME: 'Matching Asset', TYP_CODE: 1, ticked: false, ID: 1
    },
    { TYP_NAME: 'Non Matching Asset', TYP_CODE: 2, ticked: false, ID: 2 },
    { TYP_NAME: 'All', TYP_CODE: 3, ticked: false, ID: 3 }];



    var columnDefsSpace = [
        /*   { headerName: 'Asset ID', field: 'Asset_Code', width: 150, height: 150 },*/
        { headerName: 'Asset Name', field: 'Asset_Name', width: 150, height: 150 },
        { headerName: 'Location Name', field: 'Location_Name', width: 150, height: 150 },
        { headerName: 'Asset Type', field: 'AST_TYPE', width: 150, height: 150 },
        { headerName: 'Asset Scanned By', field: 'SCANNED_BY', width: 150, height: 150 },
        { headerName: 'Asset Scanned Date', field: 'SCANNED_DT', width: 150, height: 150 },
        { headerName: 'Status', field: 'Status', width: 150, height: 150 },
        { headerName: 'Description', field: 'DESCRIPTION', width: 150, height: 150 }
    ];
    var columnDefs = [
        { headerName: "Sno", width: 30, cellClass: "grid-align", filter: 'set', template: '<input type="checkbox" value="Update" ng-model="data.ticked"  >', headerCellRenderer: headerCellRendererFunc },
        { headerName: 'Asset Name', field: 'AAT_NAME', width: 150}
    ];
    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    var filter = $('#filtertxtAsset').value;
                    if (filter != '' && filter != undefined) {
                        angular.forEach($scope.AllGridData, function (value, key) {
                            value.ticked = true;
                        });
                    }
                    else {
                        angular.forEach($scope.gridOptionsAsset.rowData, function (value, key) {
                            value.ticked = true;
                        });
                    }
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptionsAsset.rowData, function (value, key) {
                        value.ticked = false;
                    });
                });
            }
        });
        return eHeader;
    }
    $scope.gridOptions = {
        columnDefs: columnDefsSpace,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        enableColResize: true,
    };
    $scope.gridOptionsAsset = {
        columnDefs: columnDefs,
        onReady: function () {
            $scope.gridOptionsAsset.api.sizeColumnsToFit()
        },
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple'
    };

    function onFilterChangedAsset(value) {
        $scope.AllGridData = $scope.AssetNameS;
        if (value) {
            $scope.AllGridData = $filter('filter')($scope.AllGridData, function (item) {
                const filterValues = value.split(',').map(v => v.trim());
                return item.AAT_NAME && filterValues.some(val => item.AAT_NAME.includes(val));
            });
            $scope.gridOptionsAsset.api.setRowData($scope.AllGridData);
        } else {
            $scope.AllGridData = $scope.AssetNameS;
            $scope.gridOptionsAsset.api.setRowData($scope.AllGridData);
        }
    }

    $("#filtertxtAsset").keydown(function () {
        onFilterChangedAsset($(this).val());
    }).keyup(function () {
        onFilterChangedAsset($(this).val());
    }).bind('paste', function () {
        onFilterChangedAsset($(this).val());
    })

    $scope.SubmitData = function () {
        progress(0, 'Loading...', true);
        var assetName = _.filter($scope.gridOptionsAsset.rowData, { ticked: true });
        if (assetName.length == 0) {
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', "Please select Asset");
            return;
        }
        //var assetName = $scope.AssetName && $scope.AssetName.AAT_NAME ? $scope.AssetName.AAT_NAME : null;
        var obj = {
            LocationLstAsts: $scope.AssetReccon.Loclist,
            Types: $scope.AssetReccon.selectedcat,
            ReqOpt: $scope.AssetReccon.AssetType[0].TYP_CODE,
           /* ReconDate: moment($scope.AssetReccon.Recon_date, 'DD/MM/YYYY').add(5, 'hours').add(30, 'minutes')*/
            FromDate: $scope.AssetReccon.fromdate,
            ToDate: $scope.AssetReccon.todate,
            AssetData: assetName
        };

        AssetReconsilationReportService.GetBarcodeAsset(obj).then(function (res) {
            progress(0, 'Loading...', false);

            if (res.Message === 'Ok') {
                $scope.gridOptions.api.setRowData(res.data);
            } else {
                $scope.gridOptions.api.setRowData([]);
            }
        });
    }
    $scope.onbindAssetNames = function () {
        //debugger;
        //var tickedLCMCodes = $scope.AssetReccon.Loclist
        //    .filter(function (item) {
        //        return item.ticked === true;
        //    })
        //    .map(function (item) {
        //        return item.LCM_CODE;
        //    });
        //progress(0, 'Loading...', true);
        //// Filter AssetNameS based on the ticked LCM_CODE values
        //$scope.AssetName = $filter('filter')($scope.AssetNameS, function (asset) {
        //    return tickedLCMCodes.includes(asset.AAT_LOC_ID);
        //});
        //progress(0, 'Loading...', false);
        angular.forEach($scope.categorylist, function (val) {
            val.ticked = false;
        })
        $scope.AssetReccon.selectedcat = [];
        $scope.gridOptionsAsset.api.setRowData([]);
    };
    $scope.LocChangeAll = function () {
        //AssetReconsilationReportService.GetAssetReconName().then(function (obj) {
        //    $scope.AssetNameS = obj;
        //    AssetNamesBind = obj;
        //    progress(0, 'Loading...', false);
        //});
        //$scope.AssetName = AssetNamesBind;
        angular.forEach($scope.categorylist, function (val) {
            val.ticked = false;
        })
        $scope.AssetReccon.selectedcat = [];
        $scope.gridOptionsAsset.api.setRowData([]);
    };

    $scope.CatChanged = function () {
        var data = {
            LocationLstAsts: $scope.AssetReccon.Loclist,
            Types: $scope.AssetReccon.selectedcat
        }
        AssetReconsilationReportService.GetAssetReconName(data).then(function (obj) {
            $scope.AssetNameS = obj;
            setTimeout(function () {
                $scope.gridOptionsAsset.api.setRowData($scope.AssetNameS);
            }, 100)
            progress(0, 'Loading...', false);
        });
    };
    $scope.catSelectAll = function () {
        var data = {
            LocationLstAsts: $scope.AssetReccon.Loclist,
            Types: $scope.categorylist
        }
        AssetReconsilationReportService.GetAssetReconName(data).then(function (obj) {
            $scope.AssetNameS = obj;
            angular.forEach($scope.AssetNameS, function (value, key) {
                value.ticked = false;
            });
            $scope.gridOptionsAsset.api.setRowData($scope.AssetNameS);
            progress(0, 'Loading...', false);
        });
    };

    function fitToColumn(arrayOfArray) {
        const keys = Object.keys(arrayOfArray[0]);
        if (arrayOfArray.length > 100000) {
            return keys.map(function (val) {
                return { wch: 20 };
            })
        }
        else{
            return keys.map(function (key) {
                const columnValues = arrayOfArray.map(function (obj) {
                    return obj[key] ? obj[key].toString().length : 0;
                });
                return { wch: Math.max.apply(null, columnValues) };
            });
        }
    }

    $scope.GenReport = function () {
        $scope.datavalue = [];
            angular.forEach($scope.gridOptions.rowData, function (value, key) {
                $scope.datavalue.push({ 'Asset Name': value.Asset_Name, 'Location Name': value.Location_Name, 'Asset Type': value.AST_TYPE, 'Asset Scanned By': value.SCANNED_BY, 'Asset Scanned Date': value.SCANNED_DT, 'Status': value.Status, 'Description': value.DESCRIPTION });
            });

            var ws = XLSX.utils.json_to_sheet($scope.datavalue);
            ws['!cols'] = fitToColumn($scope.datavalue);
            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "AssetReconsilationReport");
            XLSX.writeFile(wb, "AssetReconsilationReport.xlsx");
    }


    setTimeout(function () {
        $scope.LoadData()
    }, 700);


})