﻿app.service("GlobalSearchService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/GlobalSearch/GetCustomizedDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});
app.controller('GlobalSearchController', function ($scope, $q, $http, GlobalSearchService, UtilityService, $timeout, $filter, $window, $document) {
    $scope.GlobalSearch = {};
    $scope.LoadInfo = [];

    UtilityService.GetLocationsall(1).then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
            angular.forEach($scope.Location, function (item, index) {
                if (index == 0) {
                    item.ticked = true
                }
            })
        }
    });

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    var columnDefs = [
        { headerName: "Asset ID", field: "AAT_ID", cellClass: "grid-align", width: 70 },
        { headerName: "Status", field: "STA_ID", cellClass: "grid-align", width: 70 },
        { headerName: "Site", field: "AAT_LOC", cellClass: "grid-align", width: 100 },
        { headerName: "Asset Label", field: "AAT_NAME", cellClass: "grid-align", width: 250 },
        { headerName: 'QR Code', field: 'QrCode', cellRenderer: deltaIndicator, width: 80, height: 75 },
        { headerName: "PO ID", field: "AAT_PO_NUMBER", cellClass: "grid-align", width: 150 },
        { headerName: "Vendor", field: "AAT_AVR_CODE", cellClass: "grid-align", width: 100 },
        //{ headerName: "Current Status", field: "AAT_AST_STATUS", cellClass: "grid-align", width: 100 },
        { headerName: "AMC Start Date", field: "AMN_FROM_DATE", cellClass: "grid-align", width: 150 },
        { headerName: "AMC End Date", field: "AMN_TO_DATE", cellClass: "grid-align", width: 150 },
        { headerName: "PPM Start Date", field: "PVD_PLANSCHD_DT", cellClass: "grid-align", width: 150 },
        //{ headerName: "PPM End Date", field: "PVM_PLAN_FDATE", cellClass: "grid-align", width: 150},
        { headerName: "PPM Status", field: "STA_TITLE", cellClass: "grid-align", width: 150 },
        {
            headerName: "Image", field: "AAT_ASSET_IMAGE", cellClass: "grid-align", width: 150, cellRenderer: function (data) {
                if (data.data.AAT_ASSET_IMAGE != null && data.data.AAT_ASSET_IMAGE != "") {
                    return "<a download href='../../../UploadFiles/" + tenant + "/ASSET_IMG/" + data.data.AAT_ASSET_IMAGE + "'>Download</a>"
                }
                else {
                    return "Image Not Available"
                }
            }
        }
    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableSorting: true,
        angularCompileRows: true,
        rowData: null,
        enableFilter: true,
        cellClass: 'grid-align',
        showToolPanel: true,
        enableColResize: true,
        //angularCompileRows: true,
        //enableScrollbars: false,
        //rowSelection: 'single',
        //rowModelType: 'virtual',
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
        rowHeight: 75
    };

    function deltaIndicator(params) {
        var element = document.createElement("span");
        var DivElement = document.createElement("div");
        $(DivElement).qrcode(
            {
                width: 70,
                height: 70,
                text: params.data.AAT_CODE
            });
        element.appendChild(DivElement);
        element.appendChild(document.createTextNode(params.value));
        return element;
    }

    //$("#btLast").hide();
    //$("#btFirst").hide();
    //$("#btNext").on('click', function () {
    //    $scope.LoadData();
    //});

    $scope.LoadData = function () {
        
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                progress(0, 'Loading...', true);
                var params = {
                    LCM_CODE: $scope.GlobalSearch.LCM_CODE[0].LCM_CODE,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 20,
                    SearchValue: searchval
                };
                GlobalSearchService.GetGriddata(params).then(function (response) {
                    if (response.griddata == null) {
                        //$("#btNext").attr("disabled", true);
                        $scope.gridOptions.api.setRowData([]);
                        progress(0, 'Loading...', false);
                    }
                    else {
                        $scope.LoadInfo = response.griddata;
                        $scope.gridOptions.api.setRowData([]);
                        $scope.gridOptions.api.setRowData(response.griddata);
                        setTimeout(function () {
                            progress(0, 'Loading...', false);
                        }, 600);

                    }
                }, function (error) {
                    console.log(error);
                });

            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    }
    //setTimeout(function () { $scope.LoadData(); }, 1000);
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
    }
    $scope.GenReport = function (data) {
        if (data == 'pdf') {
            var codes = {};
            const songs = [];
            angular.forEach($scope.LoadInfo, function (val) {
                songs.push(val.AAT_CODE);
            });
            qr_generate(songs);
        }
    }
    //$("#btNext").on('click', function () {
    //    $scope.LoadData();
    //});

});
