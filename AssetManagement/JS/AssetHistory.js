﻿app.service("AssetHistoryService", function ($http, $q, UtilityService) {

    //this.GetGriddata = function (Customized) {
    //    deferred = $q.defer();
    //    return $http.post(UtilityService.path + '/api/AssetHistory/AssetHistoryData', Customized)
    //        .then(function (response) {
    //            deferred.resolve(response.data);
    //            return deferred.promise;
    //        }, function (response) {
    //            deferred.reject(response);
    //            return deferred.promise;
    //        });
    //};
    this.GetStatus = function (data) {
        deferred = $q.defer();

        return $http.post(UtilityService.path + '/api/AssetHistory/GetStatus', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //this.SearchData = function (data) {
    //    deferred = $q.defer();
    //    return $http.post(UtilityService.path + '/api/AssetHistory/SearchData', data)
    //        .then(function (response) {
    //            deferred.resolve(response.data);
    //            return deferred.promise;
    //        }, function (response) {
    //            deferred.reject(response);
    //            return deferred.promise;
    //        });
    //};
    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetHistory/SearchData', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


});

app.controller('AssetHistoryController', function ($scope, $q, $http, AssetHistoryService, UtilityService, $timeout, $filter) {

    $scope.CustomizedReport = {};
    $scope.Status = [];
    $scope.ToDate = [];
    $scope.FromDate = [];


    $scope.gridOptions = {
        columnDefs: [],
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    }


    $scope.LoadData = function () {

        AssetHistoryService.GetStatus().then(function (response) {

            if (response.data != null) {
                $scope.Status = response.data;

                angular.forEach($scope.Status, function (value, key) {
                    var a = _.find($scope.Status);
                    a.ticked = true;
                });

                var STS1 = { STA_ID: 5, STA_TITLE: "Approved PO" };
                var STS2 = { STA_ID: 3, STA_TITLE: "Rejected PO" };
                var STS3 = { STA_ID: 4, STA_TITLE: "Finalize Updated PO" };

                response.data.push(STS1, STS2, STS3);


                setTimeout(function () { $scope.SearchA(); }, 1000);

                //$scope.SearchA();
                //$timeout($scope.SearchA(), 200);
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }), function (response) {
            progress(0, '', false);
        }

    };

    $scope.SearchA = function () {


        var STS = _.filter($scope.CustomizedReport.Status, function (o) { return o.ticked == true; }).map(function (x) { return x.STA_ID; }).join(',');

        var dataObj = {

            STlist: STS,
            FromDate: $scope.CustomizedReport.FromDate.$modelValue,
            ToDate: $scope.CustomizedReport.ToDate.$modelValue
        };
        AssetHistoryService.GetGriddata(dataObj).then(function (response) {

            $scope.gridata = response.griddata;

            if ($scope.gridata == null) {
                $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData([]);

            }
            else {

                $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData(response.griddata);
            }

        }, function (response) {
            progress(0, '', false);

        });

    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        var Customized = {};
        $scope.GenerateFilterExcel();

    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var ws = XLSX.utils.json_to_sheet($scope.gridOptions.rowData);
        var columnHeaders = Object.keys($scope.gridOptions.rowData[0]);
        /* Iterate over the column headers */
        columnHeaders.forEach(function (header, columnIndex) {
            var Length = [];
            var headerLength = header.length + 2; // Start with the length of the header
            /* Iterate over the rows to find the maximum length of cell content */
            $scope.gridOptions.rowData.forEach(function (row) {
                var cellValue = row[header] ? row[header].toString() : '';
                var maxLength = Math.max(headerLength, cellValue.length + 2); // Add 2 for padding
                Length.push(maxLength);
            });

            let maximumValue = Math.max(...Length);
            var columnPixels = maximumValue;
            /* Set the column width in the worksheet */
            ws['!cols'] = ws['!cols'] || [];
            ws['!cols'][columnIndex] = { width: columnPixels };

        });
        /* add to workbook */
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Asset_History");
        /* write workbook and force a download */
        XLSX.writeFile(wb, "Asset_History.xlsx");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $timeout($scope.LoadData()

        , 100);

    $("#filtertxt").change(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keydown(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).keyup(function () {
        onReq_SelSpacesFilterChanged($(this).val());
    }).bind('paste', function () {
        onReq_SelSpacesFilterChanged($(this).val());
    });
    function onReq_SelSpacesFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }


});