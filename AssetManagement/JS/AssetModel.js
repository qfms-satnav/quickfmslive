﻿app.service("AssetModelService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.SaveAssetModel = function (obj) {
        debugger;
        deferred = $q.defer();
        $http.post('../../api/AssetModel/SaveAssetModel', obj)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error);
            });
        return deferred.promise;
    };


    this.ModifyAssetModel = function (assetbrand) {
        deferred = $q.defer();
        $http.post(UtilityService.path + '/api/AssetModel/ModifyAssetModel', assetbrand)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error);
            });
        return deferred.promise;
    };


    this.GetBRNGrid = function () {
        debugger;
        deferred = $q.defer();
        return $http.get('../../api/AssetModel/GetGridData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    };


    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }


    this.GetAssetMainCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/AssetModel/AssetMainCategoryModel'));
    };
    this.GetAssetSubCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/AssetModel/AssetSubCategoryModel'));
    };
    this.GetAssetBrand = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/AssetModel/AssetBrandModel'));
    };

}]);
app.controller('AssetModelController', ['$scope', 'AssetModelService', '$filter', '$timeout', function ($scope, AssetModelService, $filter, $timeout) {
    $scope.assetmodel = [];
    $scope.model = {};
    $scope.isEditMode = false;
    $scope.buttonText = "Add";
    $scope.AssetMainCategoryList = [];
    $scope.AssetSubCategoryList = [];
    $scope.AssetBrandList = [];
    $scope.EnableStatus = 1;
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];

    $scope.PageLoad = function () {
        debugger;
        AssetModelService.GetAssetMainCategory().then(function (Zdata) {
            $scope.AssetMainCategoryList = Zdata;
            AssetModelService.GetAssetSubCategory().then(function (Vdata) {

                $scope.AssetSubCategoryListS = Vdata;
                //$scope.AssetSubCategoryList = $scope.AssetSubCategoryListS;
                AssetModelService.GetAssetBrand().then(function (Adata) {

                    $scope.AssetBrandLists = Adata;
                    //$scope.AssetBrandList = $scope.AssetBrandLists;
                }, function (error) {
                    console.error('Error fetching AssetBrand:', error);
                });
            }, function (error) {
                console.error('Error fetching AssetSubCategory:', error);
            });
        }, function (error) {
            console.error('Error fetching AssetMainCategory:', error);
        });
    }
    $scope.onBindSubcategory = function () {


        $scope.AssetSubCategoryList = $filter('filter')($scope.AssetSubCategoryListS, { VT_CODE: $scope.AssetMainCategoryList.VT_CODE[0].VT_CODE });
    }
    $scope.onBindAssetBrand = function () {


        $scope.AssetBrandList = $filter('filter')($scope.AssetBrandLists,
            {
                AST_SUBCAT_CODE: $scope.AssetSubCategoryList.AST_SUBCAT_CODE[0].AST_SUBCAT_CODE,
                VT_CODE: $scope.AssetMainCategoryList.VT_CODE[0].VT_CODE
            });

    }



    $scope.LoadData = function () {

        AssetModelService.GetBRNGrid().then(function (data) {

            $scope.gridata = data;
            $scope.gridOptions.api.setRowData($scope.gridata);
            /*}*/
        }).catch(error =>
            console.error('Error loading grid data:', error)
        );
    };

    columnDefs = [
        { headerName: "Model Name", field: "AST_MD_NAME", width: 270 },
        { headerName: "Model Code", field: "AST_MD_CODE", width: 270 },
        { headerName: "Asset Brand/Make", field: "manufacturer", width: 270 },
        { headerName: "Asset Subcategory", field: "AST_SUBCAT_NAME", width: 270 },
        { headerName: "Asset Category", field: "VT_TYPE", width: 270 },
        { headerName: "Status", field: "AST_MD_STAID", template: "{{ShowStatus(data.AST_MD_STAID)}}", width: 270, cellClass: 'grid-align' },

        { headerName: "Edit", width: 150, template: '<a ng-click="EditData(data)"> <i class="Edit">Edit</i> </a>', cellClass: 'grid-align', suppressMenu: true }

    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableSorting: true,
        angularCompileRows: true,
        enableColResize: true,
        onGridReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };


    $scope.Save = function () {

        var obj = {
            AST_MD_CODE: $scope.model.AST_MD_CODE,
            AST_MD_NAME: $scope.model.AST_MD_NAME,
            AST_MD_CATID: $scope.AssetMainCategoryList.VT_CODE[0].VT_CODE,
            AST_MD_SUBCATID: $scope.AssetSubCategoryList.AST_SUBCAT_CODE[0].AST_SUBCAT_CODE,
            AST_MD_BRDID: $scope.AssetBrandList.manufactuer_code[0].manufactuer_code,
            AST_MD_STAID: $scope.model.AST_MD_STAID,
            AST_MD_REM: $scope.model.AST_MD_REM
        };

        if ($scope.isEditMode) {
            var mdl = {
                AST_MD_CODE: $scope.model.AST_MD_CODE,
                AST_MD_NAME: $scope.model.AST_MD_NAME,
                AST_MD_CATID: $scope.AssetMainCategoryList.VT_CODE[0].VT_CODE,
                AST_MD_SUBCATID: $scope.AssetSubCategoryListS.AST_SUBCAT_CODE[0].AST_SUBCAT_CODE,
                AST_MD_BRDID: $scope.AssetBrandLists.manufactuer_code[0].manufactuer_code,
                AST_MD_STAID: $scope.model.AST_MD_STAID,
                AST_MD_REM: $scope.model.AST_MD_REM
            };
            AssetModelService.ModifyAssetModel(mdl).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Asset Model Successfully Modified";
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.isEditMode = false;
                $scope.buttonText = "Add";
                $scope.LoadData();
                //$scope.PageLoad();
                $scope.resetForm();
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data.Message || "An error occurred.";
                showNotification('error', 8, 'bottom-right', $scope.Success);
                console.log(error);
            });

        }
        else {
            AssetModelService.SaveAssetModel(obj).then(function (response) {

                if (response.Message == "Asset Model already exists.") {
                    $scope.ShowMessage = true;
                    $scope.error = "Asset Model Code is in use; try another";
                    showNotification('error', 8, 'bottom-right', $scope.error);
                    $scope.assetmodel.$submitted = false;
                    $scope.LoadData();
                    //$scope.resetForm();
                    //$scope.PageLoad();
                } else {
                    $scope.Success = "New Asset Model Successfully Added";
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', $scope.Success);
                    $scope.isEditMode = false;
                    $scope.LoadData();
                    $scope.PageLoad();
                    $scope.resetForm();
                }
                //$scope.MANUFACTUER_STATUS.value = 1;
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data.Message || "An error occurred.";
                showNotification('error', 8, 'bottom-right', $scope.Success);
                console.log(error);
            });
        }
    };

    $scope.ShowStatus = function (value) {
        /* $('#AST_MD_STAID').selectpicker('refresh');*/
        return $scope.StaDet[value == 0 ? 1 : 0].Name;

    }


    $scope.EditData = function (data) {

        $scope.resetForm();
        $scope.model = {};
        $scope.EnableStatus = 0;


        $scope.model.AST_MD_REM = data.AST_MD_REM;
        $scope.AssetSubCategoryList = $scope.AssetSubCategoryListS;
        $scope.AssetBrandList = $scope.AssetBrandLists;

        $scope.model = angular.copy(data);

        var cat = _.find($scope.AssetMainCategoryList, { VT_CODE: data.VT_CODE });
        if (cat != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cat.ticked = true;

                });
            }, 100)
        }

        var subcat = _.find($scope.AssetSubCategoryListS, { AST_SUBCAT_CODE: data.AST_SUBCAT_CODE });
        if (subcat != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    subcat.ticked = true;

                });
            }, 100)
        }


        var brd = _.find($scope.AssetBrandLists, { manufactuer_code: data.manufactuer_code });
        if (brd != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    brd.ticked = true;

                });
            }, 100)
        }

        $scope.model.AST_MD_STAID = data.AST_MD_STAID;

        $scope.isEditMode = true;
        $scope.buttonText = "Modify";

    }

    $scope.resetForm = function () {
        $scope.assetmodel.$submitted = false;
        $scope.model = {};
        $scope.EnableStatus = 1;

        $scope.isEditMode = false;
        $scope.buttonText = "Add";


        angular.forEach($scope.AssetMainCategoryList, function (Zdata) {
            Zdata.ticked = false;
        });
        angular.forEach($scope.AssetSubCategoryList, function (Vdata) {
            Vdata.ticked = false;
        });
        angular.forEach($scope.AssetBrandList, function (Adata) {
            Adata.ticked = false;
        });
    };
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.LoadData();
    $scope.PageLoad();


}]);