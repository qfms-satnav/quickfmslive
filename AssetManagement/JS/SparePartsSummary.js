﻿app.service("SparePartsSummaryService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {



    this.GetData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SparePartsSummary/GetData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetAssetDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SparePartsSummary/GetAssetDetails', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SearchData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SparePartsSummary/SearchData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SearchToTInventoryToTCost = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SparePartsSummary/SearchToTInventoryToTCost', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.SearchToTInventoryToTCostReport = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SparePartsSummary/SearchToTInventoryToTCostReport', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('SparePartsSummaryController', ['$scope', '$q', '$http', 'SparePartsSummaryService', 'UtilityService', '$timeout', '$filter',

    function ($scope, $q, $http, SparePartsSummaryService, UtilityService, $timeout, $filter) {

        $scope.SparePartsSummary = {};
        $scope.gridata = [];
        $scope.Priority = [];
        $scope.Locations = [];


       

        var columnDefs = [
            //{ headerName: "Ticket Id", field: "ASP_MAINT_ID", width:150, cellClass: 'grid-align', hide: true, },
            ////{ headerName: "Request Id", field: "ASP_REQ_ID", width: 200, cellClass: 'grid-align' },
            ////{ headerName: "Requested By", field: "ASP_AUR_ID", width: 150, cellClass: 'grid-align', cellRenderer: tooltip},
            //{ headerName: "Sites", field: "LCM_NAME", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            //{ headerName: "Priority", field: "AAS_VED", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            //{ headerName: "Total Quantity", field: "AAS_SPAREPART_AVBQUANTITY", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            ////{ headerName: "Requested Date", field: "ASP_REQ_DATE", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            ////{ headerName: "Validated By", field: "ASP_APR1_BY", width: 150, cellClass: 'grid-align', cellRenderer: tooltip},
            ////{ headerName: "Request Type", field: "ASP_FLAG_TYPE", width: 150, cellClass: 'grid-align' },
            //{ headerName: "Equipment", field: "AAS_AAT_CODE", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            ////{ headerName: "Part Number", field: "AAS_SPAREPART_NAME", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            ////{ headerName: "Part Name", field: "AAS_SPAREPART_NAME", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            ////{ headerName: "Part Descrpition", field: "AAS_SPAREPART_DES", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },            
            //{ headerName: "Utilized Quantity", field: "AAS_SPAREPART_MINQUANTITY", width: 150, cellClass: 'grid-align' },
            //{ headerName: "Unit Cost", field: "AAS_SPAREPART_TOTCOST", width: 150, cellClass: 'grid-align' },
            //{ headerName: "Total Cost", field: "AAS_SPAREPART_TOTCOST", width: 150, cellClass: 'grid-align' },, rowSpan: params => params.data.Sites === 'Whitefield FC' ? 2 : 1

            { headerName: "Sites", field: "Sites", width: 200, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Priorities", field: "Priorities", width: 200, cellClass: 'grid-align' },
            { headerName: "Total Quantity", field: "TotalQty", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Good Stock", field: "GoodStock", width: 150, cellClass: 'grid-align', headerClass: 'cell-pass', cellRenderer: tooltip, filter: 'set', template: '<a href="/BreakdownManagement/Views/SpareCurrentInventory.aspx?LCMName={{data.Sites}}&Stock=GoodStock" style="cursor: pointer;">{{data.GoodStock}}</a>'},
            { headerName: "Low Stock", field: "LowStock", width: 150, cellClass: 'grid-align', headerClass: 'cell-less', cellRenderer: tooltip, filter: 'set', template: '<a href="/BreakdownManagement/Views/SpareCurrentInventory.aspx?LCMName={{data.Sites}}&Stock=LowStock" style="cursor: pointer;">{{data.LowStock}}</a>' },
            { headerName: "Out Stock", field: "OutStock", width: 150, cellClass: 'grid-align', headerClass: 'cell-fail', cellRenderer: tooltip, filter: 'set', template: '<a href="/BreakdownManagement/Views/SpareCurrentInventory.aspx?LCMName={{data.Sites}}&Stock=OutStock" style="cursor: pointer;">{{data.OutStock}}</a>' },
        ];

        const cellClassRules = {
            "cell-pass": params => params.value > 0,
            "cell-less": params => params.value == 0,
            "cell-fail": params => params.value < 0
        };
        var columnDefs2 = [
            { headerName: "Sites", field: "LCM_NAME", width: 150, cellClass: 'grid-align', cellRenderer: tooltip, filter: 'set', template: '<a ng-click="goToConsumeSpare(data.LCM_NAME)" style="cursor: pointer;">{{data.LCM_NAME}}</a>' },
            { headerName: "Part No", field: "AAS_SPAREPART_NUMBER", width: 100, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Part Name", field: "SPARE_NAME", width: 150, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Part Descrpition", field: "DESCRIPTION", width: 200, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Actual Stock", field: "AVAILQTY", width: 120, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Min Stock", field: "MINIQTY", width: 100, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Count Of Consumption", field: "CountQty", width: 170, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Total Consumption", field: "TotalConsumption", width: 170, cellClassRules: cellClassRules, cellRenderer: tooltip },
            { headerName: "Total Consumption Cost", field: "Total_Consumption_Cost", width: 170, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Consumed By", field: "CONSUMEDBY", width: 170, cellClass: 'grid-align', cellRenderer: tooltip }
        ];

        var columnDefs3 = [
            //{ headerName: "Sites Code", field: "SitesCode", width: 200, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Sites", field: "Sites", width: 600, cellClass: 'grid-align', cellRenderer: tooltip },           
            { headerName: "Total Inventory", field: "TotalInventory", width: 200, cellClass: 'grid-align', cellRenderer: tooltip },
            { headerName: "Total Cost", field: "TotalCost", width: 200, cellClass: 'grid-align' },
            //{ headerName: "Total Consumption", field: "SAFEQTY", width: 220, cellClass: 'grid-align', cellRenderer: tooltip }

        ];

        function tooltip(params) {
            return `<label title="${params.value}" >${params.value}</label> `
        }

        $scope.gridOptions = {
            columnDefs: columnDefs,
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            //groupHideGroupColumns: true,
            //suppressHorizontalScroll: false,
            //enableCellSelection: false,
            //groupDefaultExpanded: 1,
            //groupColumnDef: {
            //    headerName: "Ticket Id",
            //    cellRenderer: {
            //        renderer: "group"
            //    }
            //},
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            }
        };

        $scope.gridOptions2 = {
            columnDefs: columnDefs2,
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            onReady: function () {
                $scope.gridOptions2.api.sizeColumnsToFit();
            }
        };
        $scope.gridOptions3 = {
            columnDefs: columnDefs3,
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            onReady: function () {
                $scope.gridOptions3.api.sizeColumnsToFit();
            }
        };
        $scope.fn_setdate = function () {
            if ($scope.SparePartsSummary.Range > 0) {
                $scope.SparePartsSummary.FromDate = $scope.BackdaysDateOfMonthDDMMYYYY($scope.SparePartsSummary.Range);
                $scope.SparePartsSummary.ToDate = $scope.CurrentDateOfMonthDDMMYYYY();
            }
        }
        $scope.BackdaysDateOfMonthDDMMYYYY = function (range) {
            var curDate = new Date();
            var yesterday = new Date();
            yesterday.setDate(curDate.getDate() - parseInt(range));
            var _dd = yesterday.getDate();
            if (_dd.length == 1)
                _dd = "0" + _dd;
            return ((yesterday.getMonth() + 1) + "/" + _dd + "/" + yesterday.getFullYear());
        }
        $scope.CurrentDateOfMonthDDMMYYYY = function () {
            var curDate = new Date();
            var _dd = curDate.getDate();
            if (_dd.length == 1)
                _dd = "0" + _dd;
            return ((curDate.getMonth() + 1) + "/" + _dd + "/" + curDate.getFullYear());
        }

        $scope.SearchTotalInventoryTotalCost = function () {
            var obj = {              
                LCM_NAME: _.filter($scope.SparePartsSummary.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),                
            }

            SparePartsSummaryService.SearchToTInventoryToTCost(obj).then(function (response) {              
                if (response.data != null) {
                    $scope.SparePartsSummary.Stock = parseFloat(response.data[0].AVAILQTY).toFixed(2);
                    $scope.SparePartsSummary.TotCost = parseFloat(response.data[0].AAS_SPAREPART_TOTCOST).toFixed(2);
                }
                else {
                    showNotification('error', 8, 'bottom-right', response.Message);
                    $scope.SparePartsSummary.Stock = 0;
                    $scope.SparePartsSummary.TotCost = 0;
                }
            }, function (response) {
                progress(0, '', false);

            });
            // $scope.LoadData();

        }

        $scope.SearchToTInventoryToTCostReport = function () {
            var obj = {
                LCM_NAME: _.filter($scope.SparePartsSummary.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
            }

            SparePartsSummaryService.SearchToTInventoryToTCostReport(obj).then(function (response) {
                if (response.data != null) {
                    $scope.gridOptions3.api.setColumnDefs(columnDefs3);
                    $scope.gridOptions3.api.setRowData(response.data);
                }
                else {
                    showNotification('error', 8, 'bottom-right', response.Message);
                    $scope.gridOptions3.api.setColumnDefs(columnDefs3);
                    $scope.gridOptions3.api.setRowData([]);
                }
                $scope.SearchTotalInventoryTotalCost();
            }, function (response) {
                progress(0, '', false);

            });
            // $scope.LoadData();

        }

        $scope.SearchA = function () {

            progress(0, 'Loading...', true);
            var obj = {
                //LCM_NAME: _.filter($scope.SparePartsSummary.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_NAME; }).join(','),
                LCM_NAME: _.filter($scope.SparePartsSummary.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
                //AAS_AAT_CODE: _.filter($scope.Type, function (o) { return o.ticked == true; }).map(function (x) { return x.SUBC_CODE; }).join(','),
                SUBC_NAME: _.filter($scope.Type, function (o) { return o.ticked == true; }).map(function (x) { return x.SUBC_CODE; }).join(','),
                FromDate: $scope.SparePartsSummary.FromDate,
                ToDate: $scope.SparePartsSummary.ToDate
            }

            SparePartsSummaryService.SearchData(obj).then(function (response) {

                if (response.data != null) {
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData(response.data);
                }
                else {
                    showNotification('error', 8, 'bottom-right', response.Message);
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData([]);
                }

                if (response.data2 != null) {
                    $scope.gridOptions2.api.setColumnDefs(columnDefs2);
                    $scope.gridOptions2.api.setRowData(response.data2);

                }
                else {
                    showNotification('error', 8, 'bottom-right', response.Message);
                    $scope.gridOptions2.api.setColumnDefs(columnDefs2);
                    $scope.gridOptions2.api.setRowData([]);
                }               
                $scope.SearchToTInventoryToTCostReport();
                //if (response.data3 != null) {
                //    $scope.gridOptions3.api.setColumnDefs(columnDefs3);
                //    $scope.gridOptions3.api.setRowData(response.data3);

                //}
                //else {
                //    showNotification('error', 8, 'bottom-right', response.Message);
                //    $scope.gridOptions3.api.setColumnDefs(columnDefs3);
                //    $scope.gridOptions3.api.setRowData([]);
                //}
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);

            }, function (response) {
                progress(0, '', false);

            });
            // $scope.LoadData();

        }

        $scope.LoadData = function () {

            //SparePartsSummaryService.GetData().then(function (data)
            //{
            //    if (data.data == null) {

            //        $scope.gridOptions.api.setColumnDefs(data.columnDefs);
            //        $scope.gridOptions.api.setRowData([]);
            //        progress(0, '', false);
            //        showNotification('error', 8, 'bottom-right', 'No Records Found');
            //    }
            //    else {
            //        showNotification('', 8, 'bottom-right', '');
            //        $scope.gridOptions.api.setColumnDefs(data.columnDefs);
            //        $scope.gridOptions.api.setRowData(data.data);
            //        setTimeout(function () {
            //            progress(0, 'Loading...', false);
            //        }, 1000);
            //    }
            //    progress(0, 'Loading...', false);
            //})
        }




        //UtilityService.getLocations(2).then(function (response) {
        UtilityService.getLocationsInventory(2).then(function (response) {
            if (response.data != null) {
                response.data.push({ LCM_CODE: 'All', LCM_NAME: 'All' });
                $scope.Locations = response.data;
                angular.forEach($scope.Locations, function (value, key) {
                    //if (key == '0') {
                    if (value.LCM_CODE == 'All') {
                        value.ticked = true;
                    }

                })
                $scope.GetAssetDetails();
            }
        });

        $scope.GetAssetDetails = function () {
            var objs = {
                LCM_NAME: _.filter($scope.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(',')
            }
            SparePartsSummaryService.GetAssetDetails(objs).then(function (response) {

                if (response.data != null) {
                    response.data.push({ SUBC_CODE: 'All', SUBC_NAME: 'All' });
                    $scope.Type = response.data;
                    angular.forEach($scope.Type, function (value, key) {
                        //var a = _.find($scope.Type);
                        //a.ticked = true;
                        if (value.SUBC_CODE == 'All') {
                            value.ticked = true;
                        }
                    });
                    $scope.SparePartsSummary.Range = "90";
                    $scope.fn_setdate();
                    $scope.SearchA();
                }
                else
                    showNotification('error', 8, 'bottom-right', response.Message);
            }, function (response) {
                progress(0, '', false);
            });
        }

        $scope.goToConsumeSpare = function(LCMName){
            if (LCMName != 'TOTAL') {
                location.href = '/BreakdownManagement/Views/ConsumeSpare.aspx?LCMName=' + LCMName + ' ';
            }
        }
        //setTimeout(function () { $scope.LoadData(); }, 1000);
        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);
            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "SpareSummary_Report.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }
        $scope.GenerateFilterExcel2 = function () {
            progress(0, 'Loading...', true);
            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "SpareSummary_Report2.csv"
            };
            $scope.gridOptions2.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }
        $scope.GenerateFilterExcel3 = function () {
            progress(0, 'Loading...', true);
            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "SpareSummary_Report3.csv"
            };
            $scope.gridOptions3.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }
        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel();
        }
        $scope.GenReport2 = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel2();
        }
        $scope.GenReport3 = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel3();
        }
        $("#filtertxt").change(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).keydown(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).keyup(function () {
            onReq_SelSpacesFilterChanged($(this).val());
        }).bind('paste', function () {
            onReq_SelSpacesFilterChanged($(this).val());
        });
        function onReq_SelSpacesFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
        }


    }]);
