﻿app.service("AddAssetInventoryService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }

    this.GetAssetMainCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/AssetMainCategory'));
    };
    this.GetAssetSubCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/AssetSubCategory'));
    };
    this.GetAssetBrand = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/AssetBrand'));
    };
    this.GetAssetModel = function () {

        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/AssetModel'));
    };
    this.GetAssetlOCATION = function () {
        
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/AssetLOCATION'));
    };
    this.GetAssettower = function () {
        
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/Assettower'));
    };
    this.GetAssetfloor = function () {
        
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/Assetfloor'));
    };
    this.GetCC = function () {
        
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/GetCostcenter'));
    };
    this.GetAssetVENDOR = function () {
        
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/AssetVendor'));
    };
    //this.GetGridData = function (paginationParams) {
    //    
    //    return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/GetData', paginationParams ));
    //};
    this.GetGridData = function (obj) {
        
        return handleAPIRequest($http.post(UtilityService.path + '/api/AddAssetInventory/GetData', obj));
    };
    this.GetAssetTYPE = function () {
        
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/Assettype'));
    };
    this.BindCompany = function () {
        
        return handleAPIRequest($http.get(UtilityService.path + '/api/AddAssetInventory/GetCompany'));
    };
    this.GetDetailsbyID = function (obj) {
        
        return handleAPIRequest($http.post(UtilityService.path + '/api/AddAssetInventory/GetDetailsById', obj));
    };
    this.SaveAssetDetails = function (obj) {
        
        return handleAPIRequest($http.post(UtilityService.path + '/api/AddAssetInventory/SaveRecord', obj));
    };
    this.ModifyAssetDetails = function (obj) {
        
        return handleAPIRequest($http.post(UtilityService.path + '/api/AddAssetInventory/ModifyAssetDetails', obj));
    };
    this.GetAstLabel = function (obj) {
        
        return handleAPIRequest($http.post(UtilityService.path + '/api/AddAssetInventory/GetAssetLable', obj));
    };
}]);
app.controller('AddAssetInventoryController', ['$scope', '$q', 'AddAssetInventoryService', 'UtilityService', '$timeout', '$http', '$filter', function ($scope, $q, AddAssetInventoryService, UtilityService, $timeout, $http, $filter) {
    $scope.AssetMainCategoryList = [];
    $scope.AssetSubCategoryList = [];
    $scope.AssetBrandList = [];
    $scope.AssetModelList = [];
    $scope.AssetlOCATIONList = [];
    $scope.AssetTowerList = [];
    $scope.AssetFloorList = [];
    $scope.CostcenterList = [];
    $scope.AssetVENDORList = [];
    $scope.AssetTYPEList = [];
    $scope.AssetCompanyList = [];
    var selectedFiles = { Files: [] };
    var selecteddata = {};
    var Documents = [];
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.AddInventory = {};
    $scope.AddInventory.STATUS = "1";
    $scope.AddInventory.rateContract = "1";
    $scope.AddInventory.AMC = "1";
    $scope.AddInventory.Insurance = "1";
    $scope.AddInventory.Count = "1";
    $scope.AddInventory.AAT_COST = 0;
    $scope.AAT_ID = "";
    $scope.griddata1 = false;
    $scope.save = false;
    $scope.modify = false;
    $scope.EnableStatus = 1;
    $scope.isVisible = false
   
    /* $scope.isValidate = false*/



    $scope.Bindgrid = function () {
        
        var dataSource = {

            rowCount: null,
           
            getRows: function (params) {
                var params = {
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    Search: $scope.AddInventory.Search


                };
                progress(0, 'Loading...', true);
                AddAssetInventoryService.GetGridData(params).then(function (data) {
                    $scope.gridata = data.Table;
                    if ($scope.gridata == null || $scope.gridata.length=='0') {
                        //$("#btNext").attr("disabled", true);
                        var nextButton = document.querySelector('#btNext');
                        nextButton.disabled = true;
                        
                        //nextButton.setAttribute('disabled', 'disabled');
                        
                        $scope.gridOptions.api.setRowData([]);

                    }
                    else {

                        $scope.griddata = data.Table;
                        $scope.gridOptions.api.setRowData($scope.griddata);
                        /*nextButton.removeAttribute('disabled');*/
                        setTimeout(function () {
                            progress(0, 'Loading...', false);
                        }, 600);
                    }
                    progress(0, 'Loading...', false);
                })
               
            }
        };
        setTimeout(function () {
            $scope.gridOptions.api.setDatasource(dataSource);
            
        }, 300)
       
    }
    $scope.Pageload = function () {
        
        $scope.modify = true;
        $scope.save = false;
        $scope.griddata1 = true;
        
        AddAssetInventoryService.GetAssetMainCategory().then(function (Zdata) {
            $scope.AssetMainCategoryList = Zdata;
            AddAssetInventoryService.GetAssetSubCategory().then(function (Vdata) {
                $scope.AssetSubCategoryListS = Vdata;
                //$scope.AssetSubCategoryList = $scope.AssetSubCategoryListS;
                AddAssetInventoryService.GetAssetBrand().then(function (Adata) {
                    $scope.AssetBrandLists = Adata;
                    //$scope.AssetBrandList = $scope.AssetBrandLists;
                    AddAssetInventoryService.GetAssetModel().then(function (Mdata) {
                        $scope.AssetModelLists = Mdata;
                        //$scope.AssetModelList = $scope.AssetModelLists;
                    }, function (error) {
                        console.error('Error fetching Asset Model:', error);
                    });

                }, function (error) {
                    console.error('Error fetching AssetSubCategory:', error);
                });
            }, function (error) {
                console.error('Error fetching AssetSubCategory:', error);
            });
        }, function (error) {
            console.error('Error fetching AssetMainCategory:', error);
        });

        AddAssetInventoryService.GetAssetlOCATION().then(function (Ldata) {
            
            $scope.AssetlOCATIONList = Ldata;
            AddAssetInventoryService.GetAssettower().then(function (tdata) {
                $scope.AssetTowerLists = tdata;
                
                AddAssetInventoryService.GetAssetfloor().then(function (fdata) {

                    $scope.AssetFloorLists = fdata;
                   
                }, function (error) {
                    console.error('Error fetching AssetSubCategory:', error);
                });
            }, function (error) {
                console.error('Error fetching AssetSubCategory:', error);
            });
        }, function (error) {
            console.error('Error fetching AssetSubCategory:', error);
        });
        
        
        AddAssetInventoryService.GetCC().then(function (cdata) {
            
            $scope.CostcenterList = cdata;
        }, function (error) {
            console.error('Error fetching Costcenter:', error);
        });
        AddAssetInventoryService.GetAssetVENDOR().then(function (Vendata) {
            $scope.AssetVENDORList = Vendata;
        }, function (error) {
            console.error('Error fetching AssetSubCategory:', error);
        });
        AddAssetInventoryService.GetAssetTYPE().then(function (Adata) {
            $scope.AssetTYPEList = Adata;
        }, function (error) {
            console.error('Error fetching AssetSubCategory:', error);
        });
       
        $scope.Bindgrid();
        
        AddAssetInventoryService.BindCompany().then(function (Adata) {
            $scope.AssetCompanyList = Adata;
            $scope.AssetCompanyList[0].ticked = true;
            $scope.AssetCompanyList.AssettCompany = $scope.AssetCompanyList[0];
        }, function (error) {
            console.error('Error fetching AssetSubCategory:', error);
        });
        
    };

    $scope.onBindSubcategory = function () {
        $scope.AssetSubCategoryList = [];
        //$scope.AssetSubCategoryList = $filter('filter')($scope.AssetSubCategoryListS, { Assetmaincatcode: $scope.AssetMainCategoryList.Assetmaincatcode[0].Assetmaincatcode });
        $scope.AssetSubCategoryListS.forEach(function (value) {
            if (value.Assetmaincatcode == $scope.AssetMainCategoryList.Assetmaincatcode[0].Assetmaincatcode)
                $scope.AssetSubCategoryList.push(value)
        })
    }
    $scope.onBindAssetBrand = function () {
        angular.forEach($scope.AssetMainCategoryList, function (Zdata) {
            Zdata.ticked = false;
        });
        $scope.AssetMainCategoryList.Assetmaincatcode = [];
        angular.forEach($scope.AssetMainCategoryList, function (value, key) {
            var cat = $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetmaincatcode;
            if (cat != undefined && value.Assetmaincatcode == cat) {
                value.ticked = true;
                $scope.AssetMainCategoryList.Assetmaincatcode.push(value);
            }
        });

        $scope.AssetBrandList = [];
        $scope.AssetBrandLists.forEach(function (value) {
            if (value.Assetsubncatcode == $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetsubncatcode && value.Assetcatcode == $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetmaincatcode)
                $scope.AssetBrandList.push(value)
        })

       
        //$scope.AssetBrandList = $filter('filter')($scope.AssetBrandLists, { Assetsubncatcode: $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetsubncatcode, Assetcatcode: $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetmaincatcode });
    }
    $scope.onBindAssetModel = function () {
        angular.forEach($scope.AssetMainCategoryList, function (Zdata) {
            Zdata.ticked = false;
        });
        angular.forEach($scope.AssetSubCategoryList, function (Vdata) {
            Vdata.ticked = false;
        });
        $scope.AssetMainCategoryList.Assetmaincatcode = [];
        $scope.AssetSubCategoryList.Assetsubncatcode = [];

        angular.forEach($scope.AssetMainCategoryList, function (value, key) {
            var cate = $scope.AssetBrandList.BrandCode[0].Assetcatcode;
            if (cate != undefined && value.Assetmaincatcode == cate) {
                value.ticked = true;
                $scope.AssetMainCategoryList.Assetmaincatcode.push(value);
            }
        });
        angular.forEach($scope.AssetSubCategoryList, function (value, key) {
            var subcat = $scope.AssetBrandList.BrandCode[0].Assetsubncatcode;
            var cate = $scope.AssetBrandList.BrandCode[0].Assetcatcode;
            if (subcat != undefined && value.Assetsubncatcode == subcat && cate != undefined && value.Assetmaincatcode == cate) {
                value.ticked = true;
                $scope.AssetSubCategoryList.Assetsubncatcode.push(value);
            }
        });
        $scope.AssetModelList = [];
        $scope.AssetModelLists.forEach(function (value) {
            if (value.MdBrandCode == $scope.AssetBrandList.BrandCode[0].BrandCode && value.Mdsubncatcode == $scope.AssetBrandList.BrandCode[0].Assetsubncatcode && value.Mdcatcode == $scope.AssetBrandList.BrandCode[0].Assetcatcode)
                $scope.AssetModelList.push(value)
        })

        //$scope.AssetModelList = $filter('filter')($scope.AssetModelLists, { MdBrandCode: $scope.AssetBrandList.BrandCode[0].BrandCode, Mdsubncatcode: $scope.AssetBrandList.BrandCode[0].Assetsubncatcode, Mdcatcode: $scope.AssetBrandList.BrandCode[0].Assetcatcode });

    }

    $scope.AstMdlchanged = function () {
        angular.forEach($scope.AssetMainCategoryList, function (Zdata) {
            Zdata.ticked = false;
        });
        angular.forEach($scope.AssetSubCategoryList, function (Vdata) {
            Vdata.ticked = false;
        });
        angular.forEach($scope.AssetBrandList, function (Adata) {
            Adata.ticked = false;
        });
        $scope.AssetMainCategoryList.Assetmaincatcode = [];
        $scope.AssetSubCategoryList.Assetsubncatcode = [];
        $scope.AssetBrandList.BrandCode = [];
     

        angular.forEach($scope.AssetMainCategoryList, function (value, key) {
            var cate = $scope.AssetModelList.MdCode[0].Mdcatcode;
            if (cate != undefined && value.Assetmaincatcode == cate) {
                value.ticked = true;
                $scope.AssetMainCategoryList.Assetmaincatcode.push(value);
            }
        });
        angular.forEach($scope.AssetSubCategoryList, function (value, key) {
            var subcat = $scope.AssetModelList.MdCode[0].Mdsubncatcode;
            var cate = $scope.AssetModelList.MdCode[0].Mdcatcode;
            if (subcat != undefined && value.Assetsubncatcode == subcat && cate != undefined && value.Assetmaincatcode == cate) {
                value.ticked = true;
                $scope.AssetSubCategoryList.Assetsubncatcode.push(value);
            }
        });
        angular.forEach($scope.AssetBrandList, function (value, key) {
            var brnd = $scope.AssetModelList.MdCode[0].MdBrandCode;
            if (brnd != undefined && value.BrandCode == brnd) {
                value.ticked = true;
                $scope.AssetBrandList.BrandCode.push(value);
            }
        });


    }

    $scope.onBindAssettower = function () {
        
        $scope.GetAssetLabel();

        $scope.AssetTowerList = [];
        $scope.AssetTowerLists.forEach(function (value) {
            if (value.lCMCODE == $scope.AssetlOCATIONList.lCMCODE[0].lCMCODE)
                $scope.AssetTowerList.push(value)
        })

        //$scope.AssetTowerList = $filter('filter')($scope.AssetTowerLists, { lCMCODE: $scope.AssetlOCATIONList.lCMCODE[0].lCMCODE });

    }
    $scope.onBindAssetFLOOR = function () {
        angular.forEach($scope.AssetlOCATIONList, function (Ldata) {
            Ldata.ticked = false;
        });
        $scope.AssetlOCATIONList.lCMCODE = [];
        angular.forEach($scope.AssetlOCATIONList, function (value, key) {
            var cate = $scope.AssetTowerList.twr_CODE[0].lCMCODE;
            if (cate != undefined && value.lCMCODE == cate) {
                value.ticked = true;
                $scope.AssetlOCATIONList.lCMCODE.push(value);
            }
        });

        $scope.AssetFloorList = [];
        $scope.AssetFloorLists.forEach(function (value) {
            if (value.lCM_CODE == $scope.AssetTowerList.twr_CODE[0].lCMCODE && value.twr_CODE == $scope.AssetTowerList.twr_CODE[0].twr_CODE)
                $scope.AssetFloorList.push(value)
        })
        

        //$scope.AssetFloorList = $filter('filter')($scope.AssetFloorLists, { lCM_CODE: $scope.AssetTowerList.twr_CODE[0].lCMCODE,twr_CODE: $scope.AssetTowerList.twr_CODE[0].twr_CODE });

    }

    $scope.onFlrChanged = function () {
        angular.forEach($scope.AssetlOCATIONList, function (Ldata) {
            Ldata.ticked = false;
        });

        angular.forEach($scope.AssetTowerList, function (tdata) {
            tdata.ticked = false;
        });

        $scope.AssetlOCATIONList.lCMCODE = [];
        $scope.AssetTowerList.twr_CODE = [];
        angular.forEach($scope.AssetlOCATIONList, function (value, key) {
            var lcm = $scope.AssetFloorList.flr_code[0].lCM_CODE;
            if (lcm != undefined && value.lCMCODE == lcm) {
                value.ticked = true;
                $scope.AssetlOCATIONList.lCMCODE.push(value);
            }
        });

        angular.forEach($scope.AssetTowerList, function (value, key) {
            var twr = $scope.AssetFloorList.flr_code[0].twr_CODE;
            if (twr != undefined && value.twr_CODE == twr) {
                value.ticked = true;
                $scope.AssetTowerList.twr_CODE.push(value);
            }
        });




    }


   
  
    var columnDefs = [

        { headerName: "Serial No", field: "AAT_ID", cellClass: 'grid-align' },
        { headerName: "Asset Name", field: "AAT_NAME",  cellClass: 'grid-align' },
        { headerName: "Location", field: "AST_LOC",  cellClass: 'grid-align' },
        { headerName: "Category", field: "AST_CAT",  cellClass: 'grid-align' },
        { headerName: "Sub Category", field: "AST_SUB_CAT",  cellClass: 'grid-align' },
        { headerName: "Brand", field: "AST_BRD",  cellClass: 'grid-align' },
        { headerName: "Model Name", field: "AST_MODEL_NAME",  cellClass: 'grid-align' },
        { headerName: "Asset Cost", field: "AAT_COST",  cellClass: 'grid-align' },
        { headerName: "Added By", field: "AUR_KNOWN_AS",  cellClass: 'grid-align' },
        { headerName: "Edit", template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true }

    ];


    var columnDef = [
        { headerName: "Remarks", field: "Remarks",  cellClass: 'grid-align' },
        { headerName: "Asset SNO", field: "AssetSNO",  cellClass: 'grid-align' },
        { headerName: "Asset Vendor", field: "AssetVendor",  cellClass: 'grid-align' },
        { headerName: "Category", field: "AssetCategory",  cellClass: 'grid-align' },
        { headerName: "SubCategory", field: "AssetSubCategory",  cellClass: 'grid-align' },
        { headerName: "Brand", field: "AssetBrand",  cellClass: 'grid-align' },
        { headerName: "Model", field: "AssetModel",  cellClass: 'grid-align' },
        { headerName: "Location", field: "AssetLocation", cellClass: 'grid-align' },
        { headerName: "Asset Cost", field: "AssetCost", cellClass: 'grid-align' },
        { headerName: "Description", field: "AssetDescription", cellClass: 'grid-align' },

    ];

    $scope.gridOptions1 = {
        columnDefs: columnDef,
        rowData: null,
        enableColResize: true,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: false,

        enableFilter: true,
        
        onReady: function () {
            $scope.gridOptions1.api.sizeColumnsToFit()
        }
    };
    function onFilterChanged(value) {
        $scope.gridOptions1.api.setQuickFilter(value);
    }






    if ($scope.AAT_ID == '') {
        $scope.GetAssetLabel = function () {
            var obj = {
                AST_CAT: $scope.AssetMainCategoryList.Assetmaincatcode[0].Assetmaincatcode,
                AST_SUB_CAT: $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetsubncatcode,
                AST_BRD: $scope.AssetBrandList.BrandCode[0].BrandCode,
                AST_MODEL: $scope.AssetModelList.MdCode[0].MdCode,
                AST_LOC: $scope.AssetlOCATIONList.lCMCODE[0].lCMCODE,

            }
            AddAssetInventoryService.GetAstLabel(obj).then(function (response) {
                $scope.AddInventory.AAT_CODE = response.Table[0].AAT_NAME;
            })

        }
    }
    $scope.disabled = false;
    $scope.EditFunction = function (data) {
        $scope.save = true;
        $scope.modify = false;
        
        $scope.griddata1 = true;
        $scope.clear();
        $scope.AAT_ID = data.AAT_ID;
        var obj = {
            AAT_ID: data.AAT_ID
        }
        $scope.EnableStatus = 0;
        $scope.AssetSubCategoryList = $scope.AssetSubCategoryListS;
        $scope.AssetBrandList = $scope.AssetBrandLists;
        $scope.AssetModelList = $scope.AssetModelLists;
        $scope.AssetTowerList = $scope.AssetTowerLists;
        $scope.AssetFloorList = $scope.AssetFloorLists;
        AddAssetInventoryService.GetDetailsbyID(obj).then(function (response) {
            $scope.AddInventory = response.Table;

            _.forEach($scope.AssetMainCategoryList, function (n) {
                if (n.Assetmaincatcode === response.Table[0].AAT_AAG_CODE) {
                    n.ticked = true;
                }
            });
            _.forEach($scope.AssetSubCategoryListS, function (s) {
                if (s.Assetsubncatcode === response.Table[0].AAT_SUB_CODE && s.Assetmaincatcode === response.Table[0].AAT_AAG_CODE) {
                    s.ticked = true;
                }
            });
            _.forEach($scope.AssetBrandLists, function (l) {
                if (l.BrandCode === response.Table[0].AAT_AAB_CODE) {
                    l.ticked = true;
                }
            });
            _.forEach($scope.AssetModelLists, function (m) {
                if (m.MdCode === response.Table[0].AAT_MODEL_NAME) {
                    m.ticked = true;
                }
            });
            _.forEach($scope.AssetlOCATIONList, function (k) {
                if (k.lCMCODE === response.Table[0].AAT_LOC_ID) {
                    k.ticked = true;
                }
            });

            _.forEach($scope.AssetTowerLists, function (e) {
                if (e.twr_CODE === response.Table[0].AAT_TOWER_CODE) {
                    e.ticked = true;
                }
            });
            _.forEach($scope.AssetFloorLists, function (b) {
                if (b.flr_code === response.Table[0].AAT_FLOOR_CODE) {
                    b.ticked = true;
                }
            });
            _.forEach($scope.AssetVENDORList, function (h) {
                if (h.vendorCode === response.Table[0].AAT_AVR_CODE) {
                    h.ticked = true;
                }
            });
            _.forEach($scope.CostcenterList, function (i) {
                if (i.Cost_Center_Code === response.Table[0].AAT_LOB) {
                    i.ticked = true;

                }
            });
            _.forEach($scope.AssetTYPEList, function (r) {
                if (r.Assettype == response.Table[0].AAT_AST_STATUS) {
                    r.ticked = true;

                }
            });
            $scope.AddInventory.AAT_CODE = response.Table[0].AAT_CODE;
            $scope.AddInventory.AAT_NAME = response.Table[0].AAT_NAME;
            $scope.AddInventory.rateContract = response.Table[0].AAT_RATE_CONTRACT.toString();
            $scope.AddInventory.AAT_COST = response.Table[0].AAT_AST_COST;
            /* $scope.AddInventory.Count = response.Table[0].AAT_CODE;*/
            $scope.AddInventory.AST_SNO = response.Table[0].AAT_AST_SERIALNO;
            $scope.AddInventory.LIFE_SPAN = response.Table[0].AAT_LIFE_SPAN;
            $scope.AddInventory.PO_NUM = response.Table[0].AAT_PO_NUMBER;
            $scope.AddInventory.WAR_DT = response.Table[0].AAT_WRNTY_DATE;
            $scope.AddInventory.PUR_DT = response.Table[0].PURCHASEDDATE;
            $scope.AddInventory.MFG_DT = response.Table[0].AAT_MFG_DATE;
            $scope.AddInventory.Depreciation = response.Table[0].AAT_DEPRICIATION;
            $scope.AddInventory.STATUS = response.Table[0].AAT_STA_ID.toString();
            $scope.AddInventory.INV_NUM = response.Table[0].AAT_INVOICE_NUM;
            $scope.AddInventory.Descrpition = response.Table[0].AAT_DESCRIPTION;
            $scope.AddInventory.AMC = response.Table[0].AAT_AMC_REQD.toString();
            $scope.AddInventory.AMC_DT = response.Table[0].AAT_AMC_DATE;
            $scope.AddInventory.Insurance = response.Table[0].AAT_INS_REQD.toString();
            $scope.AddInventory.INSU_DT = response.Table[0].AAT_INS_DATE;
            $scope.AddInventory.Count = "1";

        })
    };

    //$scope.gridOptions = {
    //    columnDefs: columnDefs,
    //    rowData: null,
    //    enableColResize: true,
    //    enableSorting: true,
    //    cellClass: 'grid-align',
    //    angularCompileRows: true,
    //    enableCellSelection: false,
    //    suppressHorizontalScroll: false,
    //    enableFilter: true,

    //    onReady: function () {
    //        $scope.gridOptions1.api.sizeColumnsToFit()
    //    }
    //};
    

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        //showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    
    $scope.Pageload();

   
   /* $scope.initializeGrid();*/
   
    
    //function onFilterChanged(value) {
    //    $scope.gridOptions.api.setQuickFilter(value);
    //}

    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.fileUpload = function (x, type) {

        var types = type;

        //angular.forEach(x.files, function (file) {
        //    var fileObj = {
        //        FileName: file.name,
        //        FileSize: file.size,
        //        FileType: types,
        //        Type: type,
        //        data: file
        //    };
        var now = moment();
        var dateTimePrefix = now.format('YYYYMMDDHHmmss');

        angular.forEach(x.files, function (file) {
            // Create the new filename with date-time prefix
            var originalFileName = file.name;
            var fileExtension = originalFileName.split('.').pop();
            var baseName = originalFileName.substring(0, originalFileName.lastIndexOf('.'));
            var newFileName = dateTimePrefix + '_' + baseName + '.' + fileExtension;

            var fileObj = {
                FileName: newFileName,
                FileSize: file.size,
                FileType: types,
                Type: type,
                data: file
            };
            var fileName = newFileName
            var Filetype = types
            switch (type) {
                case 'AAT_IMG':
                    $scope.$apply(function () {
                        $scope.AddInventory.AAT_IMG = fileName;
                    });
                    break;
                case 'AMC_DOC':
                    $scope.$apply(function () {
                        $scope.AddInventory.AMC_DOC = fileName;
                    });
                    break;
                case 'INS_DOC':
                    $scope.$apply(function () {
                        $scope.AddInventory.INS_DOC = fileName;
                    });
                    break;

                default:
                    break;
            }
            selecteddata[Filetype] = fileName;
            selectedFiles.Files.push(fileObj);
            Documents.push(fileObj);

        });


    };
    $scope.saveImg = function () {
        var frmData = new FormData();
        angular.forEach(selectedFiles.Files, function (fileObj) {
            var file = new Blob([fileObj.data]);
            frmData.append(fileObj.FileName, file, 'file');
            /*frmData.append(fileObj.Type, file, 'Type');*/
        });
      
        $http.post(UtilityService.path + '/api/AddAssetInventory/UploadFiles', frmData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined },
            processData: false
        }).then(function (response) { // Handle success response from the server
            console.log('File uploaded successfully:', response.data);
        }).catch(function (error) { // Handle errors
            console.error('File upload failed:', error);
        });
    }

   


    $scope.submit = function () {
      
        if ($scope.AddInventory.Descrpition == '' || $scope.AddInventory.Descrpition == null) {
            $scope.isVisible = true;

            return;
        
        }
        if ($scope.AddInventory.AAT_NAME == '' || $scope.AddInventory.AAT_NAME == null) {
            $scope.isValidate = true;
            return;
        }
        $scope.AddInventory.Search = "";
        //if ($scope.AddInventory.AAT_COST == '' || $scope.AddInventory.AAT_COST == null) {
        //    $scope.isValidate = true;

        //    return;

        //}


        //$scope.isValidate = false;
        $scope.isVisible = false;
            $scope.SubmitData();
           
        $scope.griddata1 = true;
    }

    $scope.SubmitData = function () {
        var tickedItem = $scope.AssetModelList.find(item => item.ticked === true);
        var obj = {
            AST_CAT: $scope.AssetMainCategoryList.Assetmaincatcode[0].Assetmaincatcode,
            AST_SUB_CAT: $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetsubncatcode,
            AST_BRD: $scope.AssetBrandList.BrandCode[0].BrandCode,
            AST_MODEL: tickedItem.MdCode,
            AST_MODEL_NAME: tickedItem.MdName,
            AST_LOC: $scope.AssetlOCATIONList.lCMCODE[0].lCMCODE,
            AST_TOWER: $scope.AssetTowerList.twr_CODE[0].twr_CODE,
            AST_FLR: $scope.AssetFloorList.flr_code[0].flr_code,
            AAT_CODE: $scope.AddInventory.AAT_CODE,
            AAT_NAME: $scope.AddInventory.AAT_NAME,
            AST_VENDOR: $scope.AssetVENDORList.vendorCode[0].vendorCode,
            AST_RT_CNT: $scope.AddInventory.rateContract,
            AAT_COST: $scope.AddInventory.AAT_COST,
            AAT_CNT: $scope.AddInventory.Count,
            AAT_SNO: $scope.AddInventory.AST_SNO,
            AAT_TYPE: $scope.AssetTYPEList.Assettype[0].Assettype,
            AAT_LF_SP: $scope.AddInventory.LIFE_SPAN,
            AAT_PO_NO: $scope.AddInventory.PO_NUM,
            AAT_WAR_DT: $scope.AddInventory.WAR_DT,
            AAT_PUR_DT: $scope.AddInventory.PUR_DT,
            AAT_MFG_DT: $scope.AddInventory.MFG_DT,
            AAT_DEP: $scope.AddInventory.Depreciation,
             AAT_LOB: $scope.CostcenterList.Cost_Center_Code.length != 0 ? $scope.CostcenterList.Cost_Center_Code[0].Cost_Center_Code : "",
            AAT_COM: $scope.AssetCompanyList.AssettCompany[0].AssettCompany,
            AAT_INV: $scope.AddInventory.INV_NUM,
            AAT_DES: $scope.AddInventory.Descrpition,
            AAT_AMT_DT: $scope.AddInventory.AMC_DT,
            AAT_INS_DT: $scope.AddInventory.INSU_DT,
            AAT_STATUS: $scope.AddInventory.STATUS,
            AAT_AMT_INC: $scope.AddInventory.AMC,
            AAT_INS_INC: $scope.AddInventory.Insurance,
            AAT_INC_DOC: $scope.AddInventory.INS_DOC,
            AAT_AMC_DOC: $scope.AddInventory.AMC_DOC,
            AAT_AST_IMG: $scope.AddInventory.AAT_IMG


        };
        $scope.saveImg();
        AddAssetInventoryService.SaveAssetDetails(obj).then(function (response) {
            if (response.Message == "Details are Inserted Successfully") {
                $scope.frmAddInventory.$submitted = false;
                $scope.ShowMessage = true;
                $scope.Success = "Asset Added Successfully";
                //$scope.clear();
                /*$scope.Pageload();*/
                showNotification('success', 8, 'bottom-right', $scope.Success);
                var obj1 = {
                    AST_CAT: $scope.AssetMainCategoryList.Assetmaincatcode[0].Assetmaincatname,
                    AST_SUB_CAT: $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetsubncatname,
                    AST_BRD: $scope.AssetBrandList.BrandCode[0].BrandName,
                    AST_MODEL_NAME: $scope.AssetModelList.MdCode[0].MdName,
                    AST_LOC: $scope.AssetlOCATIONList.lCMCODE[0].LCMNAME,
                    AAT_COST: $scope.AddInventory.AAT_COST,
                    AAT_NAME: $scope.AddInventory.AAT_NAME
                }
                var data = $scope.gridOptions.rowData;
                data = data.filter(x => x.AAT_ID != response.data[0].AST_ID);
                obj1.AAT_ID = response.data[0].AST_ID;
                obj1.AUR_KNOWN_AS = response.data[0].AUR_NAME;
                data.unshift(obj1);
                $scope.gridOptions.api.setRowData(data);
                $scope.clear();
               /* $scope.gridOptions.api.setRowData($scope.gridOptions.rowData);*/

                //AddAssetInventoryService.GetGridData().then(function (response) {
                //    $scope.griddata = response.Table;
                //    $scope.gridOptions.api.setRowData($scope.griddata);
                //    setTimeout(function () {
                //        progress(0, 'Loading...', false);
                //        $scope.$apply(function () {
                //            $scope.ShowMessage = false;
                //        });
                //    }, 600);
                //})
            }
            else if (response.data == "Asset Already Exist") {
                $scope.frmAddInventory.$submitted = false;
                $scope.ShowMessage = true;
                $scope.error = "Asset Name Already Exist";
               
                showNotification('error', 8, 'bottom-right', $scope.error);

                AddAssetInventoryService.GetGridData().then(function (response) {
                    $scope.griddata = response.Table;
                    $scope.gridOptions.api.setRowData($scope.griddata);
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                        $scope.$apply(function () {
                            $scope.ShowMessage = false;
                        });
                    }, 600);
                })
            }

            else {
                showNotification('error', 8, 'bottom-right', response.data);
            }

        });

    }
    $scope.clear = function () {
        $scope.AddInventory = {};
        $scope.selectedFiles = {};
        angular.forEach($scope.AssetMainCategoryList, function (Zdata) {
            Zdata.ticked = false;
        });
        angular.forEach($scope.AssetSubCategoryList, function (Vdata) {
            Vdata.ticked = false;
        });
        angular.forEach($scope.CostcenterList, function (cdata) {
            cdata.ticked = false;
        });
        angular.forEach($scope.AssetVENDORList, function (Vendata) {
            Vendata.ticked = false;
        });
        angular.forEach($scope.AssetTYPEList, function (Adata) {
            Adata.ticked = false;
        });
        angular.forEach($scope.AssetBrandList, function (Adata) {
            Adata.ticked = false;
        });
        angular.forEach($scope.AssetModelList, function (Mdata) {
            Mdata.ticked = false;
        });
        angular.forEach($scope.AssetlOCATIONList, function (Ldata) {
            Ldata.ticked = false;
        });
        angular.forEach($scope.AssetTowerList, function (tdata) {
            tdata.ticked = false;
        });
        angular.forEach($scope.AssetFloorList, function (fdata) {
            fdata.ticked = false;
        });
        angular.forEach($scope.AssetFloorLists, function (fdata) {
            fdata.ticked = false;
        });
        angular.forEach($scope.AssetModelLists, function (Ldata) {
            Ldata.ticked = false;
        });
        angular.forEach($scope.AssetTowerLists, function (tdata) {
            tdata.ticked = false;
        });
        angular.forEach($scope.AssetBrandLists, function (fdata) {
            fdata.ticked = false;
        });
        angular.forEach($scope.AssetSubCategoryListS, function (Ldata) {
            Ldata.ticked = false;
        });
        
        $scope.AddInventory.STATUS = "1";
        $scope.AddInventory.rateContract = "1";
        $scope.AddInventory.AMC = "1";
        $scope.AddInventory.Insurance = "1";
        $scope.AddInventory.Count = "1";
        $timeout(function () {
            var fileInput = angular.element('input[type="file"]');
            fileInput.val(null);
            $scope.AddInventory.INS_DOC = null;
            $scope.AddInventory.INS_DOC_FILENAME = '';
        }, 0);
    }

    $scope.Modify = function () {
        $scope.frmAddInventory.$submitted = true;
        $scope.update();
        $scope.griddata1 = true;
        $scope.AddInventory.Search = "";
    }
    $scope.update = function () {
        var tickedItem = $scope.AssetModelList.find(item => item.ticked === true);
        var obj = {
            AAT_ID: $scope.AAT_ID,
            AST_CAT: $scope.AssetMainCategoryList.Assetmaincatcode[0].Assetmaincatcode,
            AST_SUB_CAT: $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetsubncatcode,
            AST_BRD: $scope.AssetBrandList.BrandCode[0].BrandCode,
            AST_MODEL: tickedItem.MdCode,
            AST_MODEL_NAME: tickedItem.MdName,
            AST_LOC: $scope.AssetlOCATIONList.lCMCODE[0].lCMCODE,
            AST_TOWER: $scope.AssetTowerList.twr_CODE[0].twr_CODE,
            AST_FLR: $scope.AssetFloorList.flr_code[0].flr_code,
            AAT_CODE: $scope.AddInventory.AAT_CODE,
            AAT_NAME: $scope.AddInventory.AAT_NAME,
            AST_VENDOR: $scope.AssetVENDORList.vendorCode[0].vendorCode,
            AST_RT_CNT: $scope.AddInventory.rateContract,
            AAT_COST: $scope.AddInventory.AAT_COST,
            AAT_CNT: $scope.AddInventory.Count,
            AAT_SNO: $scope.AddInventory.AST_SNO,
            AAT_TYPE: $scope.AssetTYPEList.Assettype[0].Assettype,
            AAT_LF_SP: $scope.AddInventory.LIFE_SPAN,
            AAT_PO_NO: $scope.AddInventory.PO_NUM,
            AAT_WAR_DT: $scope.AddInventory.WAR_DT,
            AAT_PUR_DT: $scope.AddInventory.PUR_DT,
            AAT_MFG_DT: $scope.AddInventory.MFG_DT,
            AAT_DEP: $scope.AddInventory.Depreciation,
            AAT_LOB: $scope.CostcenterList.Cost_Center_Code.length != 0 ? $scope.CostcenterList.Cost_Center_Code[0].Cost_Center_Code : "",
            AAT_COM: $scope.AssetCompanyList.AssettCompany[0].AssettCompany,
            AAT_INV: $scope.AddInventory.INV_NUM,
            AAT_DES: $scope.AddInventory.Descrpition,
            AAT_AMT_DT: $scope.AddInventory.AMC_DT,
            AAT_INS_DT: $scope.AddInventory.INSU_DT,
            AAT_STATUS: $scope.AddInventory.STATUS,
            AAT_AMT_INC: $scope.AddInventory.AMC,
            AAT_INS_INC: $scope.AddInventory.Insurance,
            AAT_INC_DOC: $scope.AddInventory.INS_DOC,
            AAT_AMC_DOC: $scope.AddInventory.AMC_DOC,
            AAT_AST_IMG: $scope.AddInventory.AAT_IMG


        };
        $scope.saveImg();
        AddAssetInventoryService.ModifyAssetDetails(obj).then(function (response) {
            if (response.Message == "Details are Modified Successfully") {
                $scope.frmAddInventory.$submitted = false;
                $scope.ShowMessage = true;
                $scope.Success = "Asset Modified Successfully";
                $scope.save = false;
                $scope.modify = true;
                /*$scope.Pageload();*/
                showNotification('success', 8, 'bottom-right', $scope.Success);
                

                var obj1 = {
                    AST_CAT: $scope.AssetMainCategoryList.Assetmaincatcode[0].Assetmaincatname,
                    AST_SUB_CAT: $scope.AssetSubCategoryList.Assetsubncatcode[0].Assetsubncatname,
                    AST_BRD: $scope.AssetBrandList.BrandCode[0].BrandName,
                    AST_MODEL_NAME: $scope.AssetModelList.MdCode[0].MdName,
                    AST_LOC: $scope.AssetlOCATIONList.lCMCODE[0].LCMNAME,
                    AAT_COST: $scope.AddInventory.AAT_COST,
                    AAT_NAME: $scope.AddInventory.AAT_NAME
                }
                var data = $scope.gridOptions.rowData;
                data = data.filter(x => x.AAT_ID != response.data[0].AST_ID);
                obj1.AAT_ID = response.data[0].AST_ID;
                obj1.AUR_KNOWN_AS = response.data[0].AUR_NAME;
                data.unshift(obj1);
                $scope.gridOptions.api.setRowData(data);
                $scope.clear();
                //AddAssetInventoryService.GetGridData().then(function (response) {
                //    $scope.griddata = response.Table;
                //    $scope.gridOptions.api.setRowData($scope.griddata);
                //    setTimeout(function () {
                //        progress(0, 'Loading...', false);
                //        $scope.$apply(function () {
                //            $scope.ShowMessage = false;
                //        });
                //    }, 600);
                //})
                //setTimeout(function () {
                //    
                //}, 700);
            }
            else {
                showNotification('error', 8, 'bottom-right', response.data);
            }

        });

    }

   

    $scope.UploadExcel = function () {
        var fileInput = $('#fileUpl')[0];

        if (fileInput && fileInput.files.length > 0) {
            var file = fileInput.files[0];
            var filetype = file.name.split('.').pop().toLowerCase();

            if (filetype === "xlsx" || filetype === "xls") {
                if (!window.FormData) {
                    redirect(); // if IE8
                    return;
                }
                var filePath = window.origin + "/UploadFiles/";
                var UplFile = $('#fileUpl')[0];
                var formData = new FormData();
                formData.append("ExUpload", file);

                formData.append("UplFile", UplFile.files[0]);
                var reader = new FileReader();
                reader.onload = function (e) {
                    var data = e.target.result;
                    var workbook = XLSX.read(data, { type: 'binary' });
                    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
                    var rowCount = XLSX.utils.decode_range(worksheet['!ref']).e.r + 1;

                    if (rowCount > 500) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Upload limit exceeded. Please upload a file with up to 500 rows.');
                        return;
                    }
                progress(0, 'Loading...', true);

                $.ajax({
                    url: UtilityService.path + "/api/AddAssetInventory/UploadExcel",
                    type: "POST",
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        if (response.data1) {
                            $scope.$apply(function () {
                                $scope.gridata1 = response.data1;
                                $scope.gridOptions1.api.setRowData([]);
                                $scope.gridOptions1.api.setRowData($scope.gridata1);
                                $scope.griddata1 = false
                                $scope.gridOptions1.api.refreshHeader();
                                $scope.tempAssetsobj = response.data1;
                                $scope.Bindgrid();
                                $('#fileUpl').val('');
                                progress(0, '', false);
                            });
                        } else {
                            progress(0, '', false);
                            showNotification('error', 8, 'bottom-right', response.Message);
                        }
                    },
                    error: function (xhr, status, error) {
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'An error occurred: ' + error);
                    }
                });
                };
                reader.readAsBinaryString(UplFile.files[0]);

            } else {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Please Upload only Excel File(s)');
            }
        } else {
            showNotification('error', 8, 'bottom-right', 'Please Select File To Upload');
        }
    }

    $scope.back = function () {
        window.location = "/FAM/Masters/Mas_WebFiles/frmAssetMasters.aspx";
    }

   

    function fitToColumn(arrayOfArray) {
        // get maximum character of each column
        const keys = Object.keys(arrayOfArray[0]);
        return keys.map(function (key) {
            const columnValues = arrayOfArray.map(function (obj) {
                return obj[key] ? obj[key].toString().length : 0;
            });

            return { wch: Math.max.apply(null, columnValues) };
        });
    }

    $scope.GenReport = function () {
        //$scope.GenReport() = function () {
        $scope.datavalue = [];
        angular.forEach($scope.gridOptions1.rowData, function (value, key) {
            $scope.datavalue.push({ 'Remarks': value.Remarks, 'Asset SNO': value.AssetSNO, 'Asset Vendor': value.AssetVendor, 'Category': value.AssetCategory, 'SubCategory': value.AssetSubCategory, 'Brand': value.AssetBrand, 'Model': value.AssetModel, 'Location': value.AssetLocation,'Asset Cost': value.AssetCost, 'Description': value.AssetDescription});
        });

        var ws = XLSX.utils.json_to_sheet($scope.datavalue);
        ws['!cols'] = fitToColumn($scope.datavalue);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "AssetReport");
        XLSX.writeFile(wb, "AssetReport.xlsx");
    }






}]);
