﻿app.service("CustomizedReportService", function ($http, $q, UtilityService) {
    debugger;
    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ASTCustomizedReport/GetCustomizedDetailsMapped', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});

app.controller('CustomizedReportController', function ($scope, $q, $http, CustomizedReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.categorylist = [];
    $scope.SubCatlist = [];
    $scope.Brandlist = [];
    $scope.Modellist = [];
    $scope.Loclist = [];
    // $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];

    $scope.Cols = [

        { COL: "City", value: "Cty_Name", ticked: false },
        { COL: "Location", value: "LCM_NAME", ticked: false },
        { COL: "Total Assets", value: "AAT_CODE_Count" },//, aggFunc: 'sum', suppressMenu: true, ticked: false
        { COL: "Available Assets", value: "AvailableAssets", ticked: false },
        { COL: "Mapped Assets", value: "MappedAssets", ticked: false },

    ];

    $scope.columnDefs = [
        { headerName: "City", field: "Cty_Name", width: 150, cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 150, },
        { headerName: "Asset Category", field: "CATEGORY", cellClass: 'grid-align', width: 150, },
        { headerName: "Sub Category", field: "SUB_CATEGORY", cellClass: 'grid-align', width: 150, },
        { headerName: "Asset Unit Cost", field: "AAT_AST_COST", cellClass: 'grid-align', width: 150, },
        { headerName: "Asset Value", field: "Total_AAT_AST_COST", cellClass: 'grid-align', width: 150, },
        { headerName: "Total Assets", field: "AAT_CODE_Count", cellClass: 'grid-align', width: 150, aggFunc: 'sum', suppressMenu: true, },
        { headerName: "Mapped Assets", field: "MappedAssets", cellClass: 'grid-align', width: 150, suppressMenu: true },
        { headerName: "Available Assets", field: "AvailableAssets", width: 150, cellClass: 'grid-align' },

    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Asset Name", field: "AAT_NAME",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };
    function groupAggFunction(rows) {
        var sums = {
            Asset_count: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.Asset_count += (data.Asset_count);
        });
        return sums;
    }
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        UtilityService.getLocations(1).then(function (response) {
            if (response.data != null) {
                $scope.Loclist = response.data;
                //angular.forEach($scope.Loclist, function (value, key) {
                //    value.ticked = true;
                //})
            }
        });

        UtilityService.GetCategories(1).then(function (response) {
            if (response.data != null) {
                $scope.categorylist = response.data;
                $scope.categorylistsource = response.data;
                //angular.forEach($scope.categorylist, function (value, key) {
                //    value.ticked = true;
                //})
            }
        });

        UtilityService.GetSubCategories(1).then(function (response) {
            if (response.data != null) {
                $scope.SubCatlist = response.data;
                $scope.SubCatlistsource = response.data;
                //angular.forEach($scope.SubCatlist, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });

        UtilityService.GetBrands(1).then(function (response) {
            if (response.data != null) {
                $scope.Brandlist = response.data;
                $scope.Brandlistsource = response.data;
                //angular.forEach($scope.Brandlist, function (value, key) {
                //    value.ticked = true;
                //})
            }
        });

        UtilityService.GetModels(1).then(function (response) {
            if (response.data != null) {
                $scope.Modellist = response.data;
                $scope.Modellistsource = response.data;
                //angular.forEach($scope.Modellist, function (value, key) {
                //    value.ticked = true;
                //})
            }
        });

        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.Customized.CNP_NAME = parseInt(CompanySessionId);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                    a.ticked = true;
                });
                if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }
            progress(0, 'Loading...', false);
        });

        //$scope.SubmitData(1);
        //setTimeout(function () {
        //    $scope.SubmitData(1, 'All');
        //}, 1000);
        progress(0, '', false);

    }, function (error) {
        console.log(error);
    }


    $scope.CatChanged = function () {
        $scope.SubCatlist = [];
        _.forEach($scope.SubCatlistsource, function (n, key) {
            _.forEach($scope.Customized.selectedcat, function (n2, key2) {
                if (n.CAT_CODE === n2.CAT_CODE) {
                    $scope.SubCatlist.push(n);
                }
            });
        });
        //console.log($scope.SubCatlist);
        //UtilityService.GetSubCategoryByCategory($scope.Customized.selectedcat, 1).then(function (response) {
        //    if (response.data != null) {
        //        $scope.SubCatlist = response.data;


        //    }
        //});
    }
    $scope.SubCatChanged = function () {
        $scope.Brandlist = [];
        angular.forEach($scope.Brandlistsource, function (n, key) {
            angular.forEach($scope.Customized.selectedsubcat, function (n2, key2) {
                if (n.BRND_SUBCODE === n2.AST_SUBCAT_CODE && n.CAT_CODE === n2.CAT_CODE) {
                    $scope.Brandlist.push(n);
                }
            });
        });

        //console.log($scope.Customized.selectedsubcat);
        //UtilityService.GetBrandBySubCategory($scope.Customized.selectedsubcat, 1).then(function (response) {
        //    if (response.data != null) {
        //        $scope.Brandlist = response.data;
        //        console.log($scope.Brandlist);
        //    }
        //});
    }
    $scope.BrandChanged = function () {

        $scope.Modellist = [];
        angular.forEach($scope.Modellistsource, function (n, key) {
            angular.forEach($scope.Customized.selectedBrands, function (n2, key2) {
                if (n.BRND_CODE == n2.BRND_CODE && n.BRND_SUBCODE === n2.AST_SUBCAT_CODE && n.CAT_CODE === n2.CAT_CODE) {
                    $scope.Modellist.push(n);
                }
            });
        });


    }

    $scope.catSelectAll = function () {
        $scope.Customized.selectedcat = $scope.categorylist;
        $scope.CatChanged();
    }
    $scope.subcatSelectAll = function () {
        $scope.Customized.selectedsubcat = $scope.SubCatlist;
        $scope.SubCatChanged();
    }
    $scope.BrandSelectAll = function () {
        $scope.Customized.selectedBrands = $scope.Brandlist;
        $scope.BrandChanged();
    }
    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    $scope.SubmitData = function (sta) {
        progress(0, 'Loading...', true);
        if (sta == 1) {
            ReqType = $scope.Customized.Request_Type;
            var params = {
                Categorylst: $scope.Customized.selectedcat,
                Subcatlst: $scope.Customized.selectedsubcat,
                Modellst: $scope.Customized.selectedModels,
                Brandlst: $scope.Customized.selectedBrands,
                loclst: $scope.Customized.selectedLoc,
                CNP_NAME: $scope.Customized.CNP_NAME[0].CNP_ID,
                Request_Type: $scope.Customized.Request_Type
            };
        } else {
            var params = {
                Categorylst: $scope.Customized.selectedcat,
                Subcatlst: $scope.Customized.selectedsubcat,
                Modellst: $scope.Customized.selectedModels,
                Brandlst: $scope.Customized.selectedBrands,
                loclst: $scope.Customized.selectedLoc,
                CNP_NAME: $scope.Customized.CNP_NAME[0].CNP_ID,
                Request_Type: $scope.Customized.Request_Type
            };
        }
        CustomizedReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            progress(0, 'Loading...', true);
            $scope.GridVisiblity = true;
            $scope.GridVisiblity2 = true;
            if ($scope.gridata == null) {
                // $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            else {
                showNotification('', 8, 'bottom-right', '');
                $scope.GridVisiblity = true;
                $scope.GridVisiblity2 = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000);

                var cols = [];
                var unticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == false;
                });

                var ticked = _.filter($scope.Cols, function (item) {
                    return item.ticked == true;
                });
                //console.log(ticked);
                for (i = 0; i < unticked.length; i++) {
                    cols[i] = unticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                cols = [];
                for (i = 0; i < ticked.length; i++) {
                    // console.log(ticked[i]);
                    cols[i] = ticked[i].value;
                }
                $scope.gridOptions.columnApi.setColumnsVisible(cols, true);


            }

            progress(0, '', false);
        })
    }, function (error) {
        console.log(error);
    }
    $scope.GenReport = function () {
        debugger;
        //$scope.GenReport() = function () {
        $scope.datavalue = [];
        angular.forEach($scope.gridOptions.rowData, function (value, key) {
            $scope.datavalue.push({
                'City': value.Cty_Name,
                'Location': value.LCM_NAME,
                'Asset Category': value.CATEGORY,
                'Sub Category': value.SUB_CATEGORY,
                'Asset Unit Cost': value.AAT_AST_COST,
                'Asset Value': value.Total_AAT_AST_COST,
                'Total Assets': value.AAT_CODE_Count,
                'Mapped Assets': value.MappedAssets,
                'Available Assets': value.AvailableAssets

            });
        });


        var ws = XLSX.utils.json_to_sheet($scope.datavalue);
        var maxLengths = [];
        $scope.datavalue.forEach(function (row) {
            Object.keys(row).forEach(function (key, index) {

                var length = row[key].toString().length;
                maxLengths[index] = maxLengths[index] || 0;
                if (length > maxLengths[index]) {
                    maxLengths[index] = length;
                }
            });
        });

        ws['!cols'] = maxLengths.map(function (length) {
            return { width: Math.min(110, Math.max(25, length * 1.5)) }; // Adjust these values as needed
        });

        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Asset Availability Report");
        XLSX.writeFile(wb, "AssetAvailabilityReport.xlsx");
    }


    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });
    $timeout($scope.LoadData, 100);
    $scope.Customized.Request_Type = "All";
});
