﻿
app.directive('istevenMultiSelect', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.on('click', function () {
                // Add a small delay to ensure the dropdown is opened
                setTimeout(function () {
                    // Call your refresh function here
                    scope.refreshLoclist(scope, element, attrs);
                }, 100);
            });
        }
    };
});
app.service("AssetMappingService", function ($q, $http, UtilityService) {
    this.GetGriddata = function (params) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetMapping/GetGriddata', params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetEmployees = function (obj) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetMapping/GetEmployees', obj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetReqIds = function (obj) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetMapping/GetReqIds', obj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveRecord = function (Obj) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetMapping/SaveData', Obj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getTowerByLocation = function (obj) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetMapping/GetTowers', obj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getFloorByTower = function (obj) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetMapping/GetFloor', obj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getspaceByFloor = function (obj) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/AssetMapping/GetSpaceIds', obj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}
);

app.controller('AssetMappingController', function ($scope, $q, $http, AssetMappingService, UtilityService, $timeout, $document) {



    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.categorylist = [];
    $scope.SubCatlist = [];
    $scope.Brandlist = [];
    $scope.Modellist = [];
    $scope.Loclist = [];
    $scope.Emplist = [];
    $scope.Twrlist = [];
    $scope.spclist = [];
    $scope.LCM_CODE = [];
    $scope.assetmapping = {};
    $scope.maincolumns = [];
    $scope.isHidden = true;
    $scope.ReqIdlist = [];
    $scope.isVisible = false
    $scope.assetmapping.Request_Type = "All"
    $scope.pad = false;

    $scope.togglePad = function (event) {
        $scope.pad = !$scope.pad;
        event.stopPropagation();
    };

    $document.on('click', function () {
        $scope.$apply(function () {
            $scope.pad = false;
        });
    });


    $scope.LoadData = function () {

        $scope.assetmapping.AssetType = "2";

        progress(0, 'Loading...', true);
        UtilityService.getLocations(1).then(function (response) {
            if (response.data != null) {
                $scope.Loclist = response.data;
                maincolumns = $scope.gridOptions.columnDefs;

            }

        });


        UtilityService.GetCategories(1).then(function (response) {
            if (response.data != null) {
                $scope.categorylist = response.data;

                /*$scope.categorylistsource = response.data;*/
                UtilityService.GetSubCategories(1).then(function (response) {

                    if (response.data != null) {
                        $scope.SubCatlistS = response.data;
                        //$scope.SubCatlistS[0].ticked = true;
                        /*    $scope.SubCatlistsource = response.data;*/
                        UtilityService.GetBrands(1).then(function (response) {

                            if (response.data != null) {
                                $scope.BrandlistS = response.data;
                                /* $scope.Brandlistsource = response.data;*/
                                UtilityService.GetModels(1).then(function (response) {

                                    if (response.data != null) {
                                        $scope.ModellistS = response.data;



                                    }
                                });

                            }
                        });

                    }
                });



            }
        });




        progress(0, '', false);

    }, function (error) {
        console.log(error);
    }

    //$scope.ClearData = function () {
    //    $scope.assetmapping = {
    //        selectedcat: [],
    //        selectedsubcat: [],
    //        selectedbrand: [],
    //        selectedmodel: [],
    //        selectedLoc: [],
    //        Request_Type: 'All',

    //    }
    //};
    $scope.CatChanged = function () {
        $scope.SubCatlist = [];
        _.forEach($scope.SubCatlistS, function (n, key) {
            _.forEach($scope.assetmapping.selectedcat, function (n2, key2) {
                if (n.CAT_CODE === n2.CAT_CODE) {
                    $scope.SubCatlist.push(n);
                }
            });
        });
    }


    $scope.SubCatChanged = function () {

        $scope.Brandlist = [];
        angular.forEach($scope.BrandlistS, function (n, key) {
            angular.forEach($scope.assetmapping.selectedsubcat, function (n2, key2) {
                if (n.BRND_SUBCODE === n2.AST_SUBCAT_CODE && n.CAT_CODE === n2.CAT_CODE) {
                    $scope.Brandlist.push(n);
                }
            });
        });


    }

    $scope.BrandChanged = function () {

        $scope.Modellist = [];
        angular.forEach($scope.ModellistS, function (n, key) {
            angular.forEach($scope.assetmapping.selectedBrands, function (n2, key2) {
                if (n.BRND_CODE == n2.BRND_CODE && n.BRND_SUBCODE === n2.AST_SUBCAT_CODE && n.CAT_CODE === n2.CAT_CODE) {
                    $scope.Modellist.push(n);
                }
            });
        });


    }
    $scope.catSelectNone = function () {
        angular.forEach($scope.SubCatlist, function (Vdata) {
            Vdata.ticked = false;
        });

        angular.forEach($scope.Brandlist, function (Mdata) {
            Mdata.ticked = false;
        });
        angular.forEach($scope.Modellist, function (Ldata) {
            Ldata.ticked = false;
        });
    }
    $scope.subcatSelectNone = function () {

        angular.forEach($scope.Brandlist, function (Mdata) {
            Mdata.ticked = false;
        });
        angular.forEach($scope.Modellist, function (Ldata) {
            Ldata.ticked = false;
        });
    }
    $scope.BrandSelectNone = function () {

        angular.forEach($scope.Modellist, function (Ldata) {
            Ldata.ticked = false;
        });
    }
    $scope.submit = function () {
        $scope.frmassetmapping.$submitted = true;
        if ($scope.frmassetmapping.$valid) {
            $scope.assetmapping.Search = "";
            $scope.SubmitData();
        }
    }

    $scope.SubmitData = function () {

        /* progress(0, 'Loading...', true);*/
        var dataSource = {

            rowCount: null,
            getRows: function (params) {
                var params = {
                    Categorylst: $scope.assetmapping.selectedcat,
                    Subcatlst: $scope.assetmapping.selectedsubcat,
                    Modellst: $scope.assetmapping.selectedModels,
                    Brandlst: $scope.assetmapping.selectedBrands,
                    loclst: $scope.assetmapping.selectedLoc,
                    Request_Type: $scope.assetmapping.Request_Type,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    Search: $scope.assetmapping.Search
                };

                progress(0, 'Loading...', true);
                AssetMappingService.GetGriddata(params).then(function (data) {

                    if (data.data != null) {
                        $scope.gridata = data.data;
                        progress(0, 'Loading...', true);


                        if ($scope.assetmapping.AssetType === "1") {

                            var updatedColumnDefs;
                            var excludedField = ['TWR_CODE', 'FLR_CODE', 'SPC_ID'];
                            updatedColumnDefs = maincolumns.filter(function (column) {
                                return !excludedField.includes(column.field);
                            });
                            $scope.gridOptions.api.setColumnDefs(updatedColumnDefs);
                            $scope.gridOptions.api.setRowData($scope.gridata);
                            $scope.gridOptions.api.sizeColumnsToFit();
                            setTimeout(function () {
                                progress(0, 'Loading...', false);
                            }, 1000);
                            $scope.isHidden = false;
                        }
                        else if ($scope.assetmapping.AssetType === "2") {
                            //var curColumnDefs = $scope.gridOptions.columnDefs;
                            var modifiedColumnDefs;
                            var excludedFields = ['AAT_EMP_ID'];
                            modifiedColumnDefs = maincolumns.filter(function (column) {
                                return !excludedFields.includes(column.field);
                            });
                            $scope.gridOptions.api.setColumnDefs(modifiedColumnDefs);
                            $scope.gridOptions.api.setRowData($scope.gridata);
                            $scope.gridOptions.api.sizeColumnsToFit();
                            setTimeout(function () {
                                progress(0, 'Loading...', false);
                            }, 1000);
                            $scope.isHidden = false;
                        }

                    }

                    if (data.data == null) {

                        $scope.gridOptions.api.setRowData([]);
                        progress(0, '', false);
                        $scope.isHidden = false;
                        showNotification('error', 8, 'bottom-right', 'No Records Found');
                    }
                    progress(0, 'Loading...', false);
                })
            }
        };
        setTimeout(function () {
            $scope.gridOptions.api.setDatasource(dataSource);

        }, 300)

    };

    $scope.GetEmployees = function () {
        var obj = {
            loclst: $scope.assetmapping.selectedLoc
        }
        AssetMappingService.GetEmployees(obj).then(function (response) {
            if (response.data != null) {
                $scope.Emplist = response.data;
            }
        });
    }
    var columnDefs = [
        { headerName: "Asset ID", field: "Asset_Name", cellClass: 'grid-align' },
        { headerName: "Asset Code", hide: true, field: "Asset_Code", cellClass: 'grid-align' },
        { headerName: "Asset Serial No", field: "Serial_Number", cellClass: 'grid-align' },
        { headerName: "Asset Name", field: "AAT_DESC", cellClass: 'grid-align' },
        { headerName: "Employee Name", field: "AAT_EMP_ID", cellClass: 'grid-align', },
        { headerName: "Location", field: "Location Name", cellClass: 'grid-align' },
        { headerName: "LCM_CODE", hide: true, field: "LCM_CODE", cellClass: 'grid-align' },
        {
            headerName: "Tower", field: "TWR_CODE", cellClass: "grid-align", filter: 'set', suppressMenu: true, suppressSorting: true, cellRenderer: customEditor
        },

        {
            headerName: "Floor", field: "FLR_CODE", cellClass: "grid-align", filter: 'set', suppressMenu: true, suppressSorting: true, cellRenderer: customEditor
        },
        {
            headerName: "Space id", field: "SPC_ID", cellClass: "grid-align", filter: 'set', suppressMenu: true, suppressSorting: true, cellRenderer: customEditor
        },

        {
            headerName: "Select All", field: "ticked", template: "<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)' />",
            cellClass: 'grid-align', headerCellRenderer: headerCellRendererFunc, suppressMenu: true
        },




    ];


    function customEditor(params) {
        var eCell = document.createElement('span');
        var index = params.rowIndex;

        if (params.column.colId === "TWR_CODE") {

            var name = 'Tower' + index;
            eCell.innerHTML = '<div isteven-multi-select ' +
                'data-input-model="data.Twrlist" ' +
                'data-output-model= "data.TWR_NAME" ' +
                'data-button-label="TWR_NAME" ' +
                'data-item-label="TWR_NAME" ' +
                'data-tick-property="ticked" ' +
                'data-max-labels="1" ' +
                'data-selection-mode="single"' +
                'style="width:200px"></div>';

            return eCell;
        }
        if (params.column.colId === "FLR_CODE") {

            var name = 'Floor' + index;
            eCell.innerHTML = '<div isteven-multi-select ' +
                'data-input-model="data.Fwrlist" ' +
                'data-output-model= "data.FLR_NAME" ' +
                'data-button-label="FLR_NAME" ' +
                'data-item-label="FLR_NAME" ' +
                'data-tick-property="ticked" ' +
                'data-max-labels="1" ' +
                'data-selection-mode="single"' +
                'style="width:200px"></div>';

            return eCell;
        }
        if (params.column.colId === "SPC_ID") {

            var name = 'Space id' + index;
            eCell.innerHTML = '<div isteven-multi-select ' +
                'data-input-model="data.spclist" ' +
                'data-output-model= "data.SPC_ID" ' +
                'data-button-label="SPC_ID" ' +
                'data-item-label="SPC_ID" ' +
                'data-tick-property="ticked" ' +
                'data-max-labels="1" ' +
                'data-selection-mode="single"' +
                'style="width:200px"></div>';

            return eCell;
        }
    }
    $scope.selectedRows = [];

    $scope.chkChanged = function (data) {

        if (data.ticked) {
            var obj = {
                LCM_CODE: data.LCM_CODE
            }
            AssetMappingService.getTowerByLocation(obj)
                .then(function (response) {
                    if (response.data != null) {
                        data.Twrlist = response.data;
                        data.Twrlist[0].ticked = true;
                        var obj = {
                            TWR_CODE: data.Twrlist[0].TWR_CODE
                        }
                        AssetMappingService.getFloorByTower(obj)
                            .then(function (response) {
                                if (response.data != null) {
                                    data.Fwrlist = response.data;
                                    data.Fwrlist[0].ticked = true;
                                    var obj = {

                                        FLR_CODE: data.Fwrlist[0].FLR_CODE,
                                        TWR_CODE: data.Fwrlist[0].TWR_CODE,
                                        LCM_CODE: data.Fwrlist[0].LCM_CODE
                                    }
                                    AssetMappingService.getspaceByFloor(obj)
                                        .then(function (response) {
                                            if (response.data != null) {
                                                data.spclist = response.data;
                                                data.spclist[0].ticked = true;
                                            }

                                        });
                                }

                            });


                    }
                });
            $scope.selectedRows.push(data);
            if ($scope.gridOptions.rowData.length == $scope.selectedRows.length) {
                var HeadChk = document.getElementById("HeadCheck");
                HeadChk.checked = true;
            }
        }

        else {
            $scope.selectedRows = _.reject($scope.selectedRows, function (d) {
                var HeadChk = document.getElementById("HeadCheck");
                HeadChk.checked = false;
                return d.RR_SNO == data.RR_SNO;
            });
        }
    }
    function headerCellRendererFunc(params) {

        var cb = document.createElement('input');
        var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        cb.setAttribute('id', 'HeadCheck');
        var eHeader = document.createElement('label');
        var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    if ($scope.gridOptions.api.isAnyFilterPresent()) {
                        $scope.gridOptions.api.forEachNodeAfterFilter(function (node, index) {
                            node.data.ticked = true;
                            $scope.selectedRows.push(node.data);
                        });
                    }
                    else {
                        angular.forEach($scope.gridOptions.rowData, function (value, key) {
                            value.ticked = true;
                            $scope.selectedRows.push(value);

                        });
                    }

                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptions.rowData, function (value, key) {
                        value.ticked = false;
                    });

                });
            }
        });
        return eHeader;
    }





    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableColResize: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableFilter: true,
        groupHideGroupColumns: true,
        onGridReady: function (event) {
            event.api.sizeColumnsToFit();
        },
        onGridSizeChanged: function (event) {
            event.api.sizeColumnsToFit();
        }
    };
    //function onFilterChanged(value) {
    //    $scope.gridOptions.api.setQuickFilter(value);
    //}




    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.empChanged = function () {
        var anyTicked = false;
        angular.forEach($scope.Emplist, function (value, key) {
            if (value.ticked) {
                anyTicked = true;
                return;
            }
        });
        if (!anyTicked) {
            $scope.isVisible = true;
            return;
        }
        else {
            $scope.isVisible = false;
        }
    }
    $scope.empSelectNone = function () {
        var anyTicked = false;
        angular.forEach($scope.Emplist, function (value, key) {
            if (value.ticked) {
                anyTicked = true;
                return;
            }
        });
        if (!anyTicked) {
            $scope.isVisible = true;
            return;
        }
        else {
            $scope.isVisible = false;
        }
    }

    $scope.empSelectAll = function () {
        var anyTicked = false;
        angular.forEach($scope.Emplist, function (value, key) {
            if (value.ticked) {
                anyTicked = true;
                return;
            }
        });
        if (!anyTicked) {
            $scope.isVisible = true;
            return;
        }
        else {
            $scope.isVisible = false;
        }
    }



    $scope.SaveData = function () {


        $scope.selectedAsset = [];

        if ($scope.assetmapping.AssetType === "1") {
            angular.forEach($scope.gridOptions.rowData, function (Value, Key) {
                if (Value.ticked) {
                    $scope.selAssetObj = {};
                    $scope.selAssetObj.Asset_Code = Value.Asset_Code;
                    $scope.selAssetObj.LCM_CODE = Value.LCM_CODE;
                    $scope.selectedAsset.push($scope.selAssetObj);
                }
            });

            //var anyTicked = $scope.Emplist.some(function (value) {
            //    return value.ticked;
            //});

            //$scope.isVisible = !anyTicked;
            var anyTicked = false;
            angular.forEach($scope.Emplist, function (value, key) {
                if (value.ticked) {
                    anyTicked = true;
                    return;
                }
            });

            if (!anyTicked) {
                $scope.isVisible = true;
                return;
            }
            else {
                $scope.isVisible = false;
            }
        }
        else if ($scope.assetmapping.AssetType === "2") {
            angular.forEach($scope.gridOptions.rowData, function (Value, Key) {
                if (Value.ticked) {
                    $scope.selAssetObj = {};
                    $scope.selAssetObj.Asset_Code = Value.Asset_Code;
                    $scope.selAssetObj.LCM_CODE = Value.LCM_CODE;
                    $scope.selAssetObj.TWR_CODE = Value.TWR_NAME[0].TWR_CODE;
                    $scope.selAssetObj.FLR_CODE = Value.FLR_NAME[0].FLR_CODE;
                    $scope.selAssetObj.SPC_ID = Value.SPC_ID[0].SPC_ID;
                    $scope.selectedAsset.push($scope.selAssetObj);
                }
            });
        }

        if ($scope.selectedAsset.length === 0) {
            showNotification('error', 8, 'bottom-right', 'Please select at least one checkbox');
            return;
        }


        if ($scope.assetmapping.Remarks === "" || $scope.assetmapping.Remarks === undefined) {
            showNotification('error', 8, 'bottom-right', 'Please Enter Remarks');
            return;
        }
        /* progress(0, 'Loading...', true);*/
        var Obj = { Mappedlst: $scope.selectedAsset, Emplst: $scope.assetmapping.selectedemp, Remarks: $scope.assetmapping.Remarks };
        AssetMappingService.SaveRecord(Obj).then(function (response) {

            if (response != null) {

                $scope.frmassetmapping.$submitted = false;

                /*progress(0, '', false);*/
                $scope.ShowMessage = true;
                $scope.Success = "Asset Mapped Successfully";
                /*progress(0, '', false);*/

                showNotification('success', 8, 'bottom-right', $scope.Success);
                angular.forEach($scope.Emplist, function (Edata) {
                    Edata.ticked = false;
                });
                $scope.assetmapping.Remarks = ""
                $scope.isHidden = true;
                scope.isVisible = false


                $scope.assetmapping.AssetType = "2";
                $scope.SubmitData();

            }

            else {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Something Went Wrong');
                console.log('Something Went Wrong')
                $scope.clear();
                $scope.assetmapping.AssetType = "2";

            }


        }, function (response) {
            progress(0, '', false);
        });


    }

    $scope.clear = function () {

        angular.forEach($scope.Loclist, function (ldata) {
            ldata.ticked = false;
        });
        angular.forEach($scope.SubCatlist, function (Vdata) {
            Vdata.ticked = false;
        });
        $scope.assetmapping.Request_Type = "All"
        angular.forEach($scope.categorylist, function (Adata) {
            Adata.ticked = false;
        });
        angular.forEach($scope.Brandlist, function (Mdata) {
            Mdata.ticked = false;
        });
        angular.forEach($scope.Modellist, function (Ldata) {
            Ldata.ticked = false;
        });
        angular.forEach($scope.Emplist, function (Edata) {
            Edata.ticked = false;
        });


    }
    $timeout($scope.LoadData, 100);
    $scope.catSelectAll = function () {

        $scope.assetmapping.selectedcat = $scope.categorylist;
        $scope.CatChanged();
    }
    $scope.subcatSelectAll = function () {

        $scope.assetmapping.selectedsubcat = $scope.SubCatlist;
        $scope.SubCatChanged();
    }
    $scope.BrandSelectAll = function () {

        $scope.assetmapping.selectedBrands = $scope.Brandlist;
        $scope.BrandChanged();
    }


    $scope.GetReqId = function () {
        var obj = {
            Categorylst: $scope.assetmapping.selectedcat,
            Subcatlst: $scope.assetmapping.selectedsubcat,
            Modellst: $scope.assetmapping.selectedModels,
            Brandlst: $scope.assetmapping.selectedBrands,
            loclst: $scope.assetmapping.selectedLoc,
        }
        AssetMappingService.GetReqIds(obj).then(function (response) {
            if (response.data != null) {
                $scope.ReqIdlist = response.data;
            }
        });
    }


    //$scope.$on('dropdown-opened', function (val) {
    //    $scope.onDropdownOpen(val);
    //});
    $scope.refreshLoclist = function (scope, element, attrs) {
        if (attrs.buttonLabel == "TWR_NAME") {
            var obj = {
                LCM_CODE: scope.data.LCM_CODE
            }
            AssetMappingService.getTowerByLocation(obj)
                .then(function (response) {
                    if (response.data != null) {
                        scope.data.Twrlist = response.data;

                    }

                });
        }
        if (attrs.buttonLabel == "FLR_NAME") {
            var obj = {
                TWR_CODE: scope.data.TWR_NAME[0].TWR_CODE
            }
            AssetMappingService.getFloorByTower(obj)
                .then(function (response) {
                    if (response.data != null) {
                        scope.data.Fwrlist = response.data;

                    }

                });
        }
        if (attrs.buttonLabel == "SPC_ID") {
            var obj = {

                FLR_CODE: scope.data.FLR_NAME[0].FLR_CODE,
                TWR_CODE: scope.data.FLR_NAME[0].TWR_CODE,
                LCM_CODE: scope.data.FLR_NAME[0].LCM_CODE
            }
            AssetMappingService.getspaceByFloor(obj)
                .then(function (response) {
                    if (response.data != null) {
                        scope.data.spclist = response.data;

                    }

                });
        }


    }




});
