﻿app.service("DayWiseConsumableReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }
    this.GetAssetCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/DayWiseConsumableReport/AssetMainCategory'));
    };
    this.GetAssetSubCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/DayWiseConsumableReport/AssetSubCategory'));
    };
    this.GetAssetBrandBySub = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/DayWiseConsumableReport/AssetBrand'));
    };
    this.GetModelByMake = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/DayWiseConsumableReport/AssetModel'));
    };
    this.GetAssetLocations = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/DayWiseConsumableReport/AssetLOCATION'));
    };
    this.GetAssetCities = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/DayWiseConsumableReport/AssetCITY'));
    };
    this.GetGriddata = function (params) {

        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/DayWiseConsumableReport/DayWiseConsReport', params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('DayWiseConsumableReportController', ['$scope', '$q', 'DayWiseConsumableReportService', 'UtilityService', '$timeout', '$http', '$filter', function ($scope, $q, DayWiseConsumableReportService, UtilityService, $timeout, $http, $filter) {

    $scope.Consumable = {};
    $scope.categorylist = [];
    $scope.SubCatlist = [];
    $scope.Brandlist = [];
    $scope.Modellist = [];
    $scope.Loclist = [];
    $scope.cityLists = [];
    $scope.Consumable.fromdate = moment().subtract(30, 'days').format('MM/DD/YYYY');
    $scope.Consumable.todate = moment().format('MM/DD/YYYY');
    $scope.PageLoad = function () {
        DayWiseConsumableReportService.GetAssetLocations().then(function (data) {
            $scope.LoclistS = data;

            DayWiseConsumableReportService.GetAssetCities().then(function (Cdata) {
                $scope.cityLists = Cdata;
            }, function (error) {
                console.error('Error fetching cities:', error);
            });
        }, function (error) {
            console.error('Error fetching locations:', error);
        });
        DayWiseConsumableReportService.GetAssetCategory().then(function (Zdata) {

            $scope.categorylist = Zdata;
            DayWiseConsumableReportService.GetAssetSubCategory().then(function (Vdata) {

                $scope.SubCatlistS = Vdata;
                DayWiseConsumableReportService.GetAssetBrandBySub().then(function (Adata) {

                    $scope.BrandlistS = Adata;

                    DayWiseConsumableReportService.GetModelByMake().then(function (Mdata) {
                        $scope.ModellistS = Mdata;
                    }, function (error) {
                        console.error('Error fetching AssetModel:', error);
                    });
                }, function (error) {
                    console.error('Error fetching AssetBrand:', error);
                });
            }, function (error) {
                console.error('Error fetching AssetSubCategory:', error);
            });
        }, function (error) {
            console.error('Error fetching AssetMainCategory:', error);
        });
    }
    $scope.PageLoad();
    $scope.CityChanged = function () {
        $scope.Loclist = [];

        var selectedCityCodes = $scope.cityLists
            .filter(city => city.ticked)
            .map(city => city.CTY_CODE);

        $scope.Loclist = $scope.LoclistS.filter(location =>
            selectedCityCodes.includes(location.CTY_CODE)
        );
    };


    $scope.CatChanged = function () {
        $scope.SubCatlist = [];
        _.forEach($scope.SubCatlistS, function (n, key) {
            _.forEach($scope.categorylist.VT_CODE, function (n2, key2) {
                if (n.VT_CODE === n2.VT_CODE) {
                    $scope.SubCatlist.push(n);
                }
            });
        });
    }

    $scope.SubCatChanged = function () {

        $scope.Brandlist = [];
        angular.forEach($scope.BrandlistS, function (n, key) {
            angular.forEach($scope.SubCatlist.AST_SUBCAT_CODE, function (n2, key2) {
                if (n.AST_SUBCAT_CODE === n2.AST_SUBCAT_CODE && n.VT_CODE === n2.VT_CODE) {
                    $scope.Brandlist.push(n);
                }
            });
        });
        $scope.BrandChanged();

    }

    $scope.BrandChanged = function () {

        $scope.Modellist = [];
        angular.forEach($scope.ModellistS, function (n, key) {
            angular.forEach($scope.Brandlist.manufactuer_code, function (n2, key2) {
                if (n.manufactuer_code == n2.manufactuer_code && n.AST_SUBCAT_CODE === n2.AST_SUBCAT_CODE && n.VT_CODE === n2.VT_CODE) {
                    $scope.Modellist.push(n);
                }
            });
        });


    }

    $scope.catSelectAll = function () {
        $scope.categorylist.VT_CODE = angular.copy($scope.categorylist);
        $scope.SubCatlist = $filter('filter')($scope.SubCatlistS, function (subCat) {
            return $scope.categorylist.some(function (category) {
                return subCat.VT_CODE === category.VT_CODE;
            });
        });

        $scope.Brandlist = $filter('filter')($scope.BrandlistS, function (brand) {
            return $scope.SubCatlist.some(function (subCat) {
                return brand.AST_SUBCAT_CODE === subCat.AST_SUBCAT_CODE;
            });
        });
    };

    $scope.subcatSelectAll = function () {
        $scope.SubCatlist.AST_SUBCAT_CODE = angular.copy($scope.SubCatlist);
        $scope.Brandlist = $filter('filter')($scope.BrandlistS, function (brand) {
            return $scope.SubCatlist.some(function (subCat) {
                return brand.AST_SUBCAT_CODE === subCat.AST_SUBCAT_CODE;
            });
        });

        $scope.Modellist = $filter('filter')($scope.ModellistS, function (model) {
            return $scope.Brandlist.some(function (brand) {
                return model.manufactuer_code === brand.manufactuer_code;
            });
        });
    };

    $scope.BrandSelectAll = function () {
        $scope.Brandlist.manufactuer_code = angular.copy($scope.Brandlist);
        $scope.Modellist = $filter('filter')($scope.ModellistS, function (model) {
            return $scope.Brandlist.some(function (brand) {
                return model.manufactuer_code === brand.manufactuer_code;
            });
        });
    };
    $scope.Selectallcities = function () {
        $scope.selectedCityCodes = $scope.cityLists;
        $scope.CityChanged();
    };
    $scope.catSelectNone = function () {
        $scope.frmDayWise.$submitted = false;
        angular.forEach($scope.SubCatlist, function (Vdata) {
            Vdata.ticked = false;
        });

        angular.forEach($scope.Brandlist, function (Adata) {
            Adata.ticked = false;
        });
        angular.forEach($scope.Modellist, function (Mdata) {
            Mdata.ticked = false;
        });
    }
    $scope.subcatSelectNone = function () {

        angular.forEach($scope.Brandlist, function (Adata) {
            Adata.ticked = false;
        });
        angular.forEach($scope.Modellist, function (Mdata) {
            Mdata.ticked = false;
        });
    }
    $scope.BrandSelectNone = function () {

        angular.forEach($scope.Modellist, function (Mdata) {
            Mdata.ticked = false;
        });
    }
    $scope.SearchData = function () {
        progress(0, 'Loading...', true);
        $scope.frmDayWise.$submitted = true;
        var obj = {
            AssetMainCategoryList: $scope.categorylist.VT_CODE,
            AssetSUbCategoryList: $scope.SubCatlist.AST_SUBCAT_CODE,
            AssetBrandList: $scope.Brandlist.manufactuer_code,
            AssetModelList: $scope.Modellist.AST_MD_CODE,
            AssetLOCList: $scope.Loclist.LCM_CODE,
            cityLists: $scope.cityLists.CTY_CODE,
            Fromdate: $scope.Consumable.fromdate,
            Todate: $scope.Consumable.todate
        };
        DayWiseConsumableReportService.GetGriddata(obj).then(function (data) {
            debugger;
            if (data.griddata != null) {
                $scope.gridOptions.api.setColumnDefs(data.Coldef);
                $scope.gridOptions.api.setRowData(data.griddata);
                $scope.GridVisiblity = true;
                progress(0, 'Loading...', true);
            } else {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Records Found');
            }
            progress(0, 'Loading...', false);
        }, function (error) {
            console.log(error);
        });
    };

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $scope.gridOptions = {
        columnDefs: [],
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    }

    $scope.GenReport = function () {
        progress(0, 'Loading...', true);

        var Filterparams1 = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Day_Wise_Consumable_Report.csv"
        };

        $scope.gridOptions.api.exportDataAsCsv(Filterparams1);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.clear = function () {
        $scope.frmDayWise.$submitted = false;
        angular.forEach($scope.categorylist, function (Zdata) {
            Zdata.ticked = false;
        });
        angular.forEach($scope.SubCatlist, function (Vdata) {
            Vdata.ticked = false;
        });
        angular.forEach($scope.Brandlist, function (Adata) {
            Adata.ticked = false;
        });
        angular.forEach($scope.Modellist, function (Mdata) {
            Mdata.ticked = false;
        });
        angular.forEach($scope.Loclist, function (data) {
            data.ticked = false;
        });
        angular.forEach($scope.cityLists, function (Cdata) {
            Cdata.ticked = false;
        });
    };
}]);