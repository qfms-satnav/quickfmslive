﻿app.service("Asset_ReconciliationService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetBarcodeAsset = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Asset_Reconciliation/GetBarcodeAsset', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('Asset_Reconciliation_Controller', function ($scope, UtilityService, $timeout, Asset_ReconciliationService, $http) {

    $scope.LoadData = function () {
        //progress(0, 'Loading...', true);
        UtilityService.getLocations(1).then(function (response) {
            if (response.data != null) {
                $scope.Loclist = response.data;
                UtilityService.GetCategories(1).then(function (response) {
                    if (response.data != null) {
                        $scope.categorylist = response.data;
                        angular.forEach($scope.AssetType, function (value, key) {
                            if (value.ID == 1)
                                value.ticked = true;
                        })
                        $scope.SubmitData();
                    }
                });
            }
            //progress(0, 'Loading...', false);
        });
    }

    $scope.AssetType = [{
        TYP_NAME: 'Matching Asset', TYP_CODE: 1, ticked:false,ID:1},
        { TYP_NAME: 'Non Matching Asset', TYP_CODE: 2, ticked: false,ID: 2}];


    var columnDefsSpace = [
        { headerName: 'Asset ID', field: 'Asset_Code', width: 150, height: 150 },
        { headerName: 'Asset Name', field: 'Asset_Name', width: 150, height: 150 },
        { headerName: 'Location Name', field: 'Location_Name', width: 150, height: 150 },
        { headerName: 'Status', field: 'Status', width: 150, height: 150 },
        { headerName: 'QR Code', field: 'QrCode', width: 150, height: 150, cellRenderer: AssetQR },
        { headerName: 'Asset Type', field: 'TAG_NAME', width: 150, height: 150 }
    ];

    $scope.gridOptions = {
        columnDefs: columnDefsSpace,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        enableColResize: true,
        rowHeight: 80
    };

    function AssetQR(params) {

        var elementt = document.createElement("span");
        var DivElementt = document.createElement("div");
        $(DivElementt).qrcode(
            {
                width: 70,
                height: 70,
                text: params.data.Asset_Code,
                label: params.data.Asset_Code
            });
        elementt.appendChild(DivElementt);
        //elementt.appendChild(document.createTextNode(params.value));
        return elementt;
    }

    $scope.SubmitData = function () {
        var obj = {
            LocationLstAsts: $scope.AssetReccon.Loclist,
            Types: $scope.AssetReccon.selectedcat,
            ReqOpt: $scope.AssetReccon.AssetType[0].TYP_CODE,
            ReconDate: moment($scope.AssetReccon.Recon_date, 'DD/MM/YYYY').add(5, 'hours').add(30, 'minutes')
        };
        Asset_ReconciliationService.GetBarcodeAsset(obj).then(function (res) {
            if (res.Message == 'Ok') {
                $scope.gridOptions.api.setRowData(res.data);
            } else {
                $scope.gridOptions.api.setRowData([]);
            }
        });
    }

    $scope.UploadFile = function () {
        var ext = $('#fileUpl').val().split('.').pop().toLowerCase();
        if (ext == "xlsx" || ext == "xls") {
            //progress(0, '', true);
            var formData = new FormData();
            var UplFile = $('#fileUpl')[0];
            formData.append("UplFile", UplFile.files[0]);
            $http.post(UtilityService.path + '/api/Asset_Reconciliation/UploadAsset_Rec', formData,
                {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
                }).then(function (data, status) {
                    $('#fileUpl').val('');
                    showNotification('success', 8, 'bottom-right', data.data.Message);
                }), function (error, data, status) {
                    $('#fileUpl').val('');
                    showNotification('error', 8, 'bottom-right', 'Something went Wrong,Please try again');
                };
        } else {
            $('#fileUpl').val('');
            showNotification('error', 8, 'bottom-right', 'Upload .xls, .xlsx files only');
        }
    }

    function fitToColumn(arrayOfArray) {
        // get maximum character of each column
        const keys = Object.keys(arrayOfArray[0]);
        return keys.map(function (key) {
            const columnValues = arrayOfArray.map(function (obj) {
                return obj[key] ? obj[key].toString().length : 0;
            });

            return { wch: Math.max.apply(null, columnValues) };
        });
    }

    $scope.GenReport = function () {
        //$scope.GenReport() = function () {
        $scope.datavalue = [];
        angular.forEach($scope.gridOptions.rowData, function (value, key) {
            $scope.datavalue.push({ 'Asset ID': value.Asset_Code, 'Asset Name': value.Asset_Name, 'Location Name': value.Location_Name, 'Status': value.Status, 'Asset Type': value.TAG_NAME});
        });

        var ws = XLSX.utils.json_to_sheet($scope.datavalue);
        ws['!cols'] = fitToColumn($scope.datavalue);
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Asset_Recon");
        XLSX.writeFile(wb, "Asset_Recon.xlsx");
    }


    $timeout(function () {
        $scope.LoadData()
    }, 700);

    
})