﻿app.service("AssetBrandService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.SaveAssetBrand = function (obj) {
        debugger;
        deferred = $q.defer();
        $http.post('../../api/AssetBrand/SaveAssetBrand', obj)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error);
            });
        return deferred.promise;
    };


    this.ModifyAssetBrand = function (assetbrand) {
        deferred = $q.defer();
        $http.post(UtilityService.path + '/api/AssetBrand/ModifyAssetBrand', assetbrand)
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (error) {
                deferred.reject(error);
            });
        return deferred.promise;
    };


    this.GetBRNGrid = function () {
        debugger;
        deferred = $q.defer();
        return $http.get('../../api/AssetBrand/GetGridData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    };


    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }

    this.GetAssetMainCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/AssetBrand/AssetMainCategory'));
    };
    this.GetAssetSubCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/AssetBrand/AssetSubCategory'));
    };

}]);

app.controller('AssetBrandController', ['$scope', 'AssetBrandService', '$filter', '$timeout', function ($scope, AssetBrandService, $filter, $timeout) {

    $scope.assetBrand = [];
    $scope.brand = {};
    $scope.isEditMode = false;
    $scope.buttonText = "Add";
    $scope.AssetMainCategoryList = [];
    $scope.AssetSubCategoryList = [];
    /*$scope.brand.MANUFACTUER_STATUS = '1';*/
    $scope.EnableStatus = 1;

    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];

    $scope.PageLoad = function () {
        AssetBrandService.GetAssetMainCategory().then(function (Zdata) {
            $scope.AssetMainCategoryList = Zdata;
            //$scope.AssetMainCategoryList = $scope.AssetMainCategoryListS;

            AssetBrandService.GetAssetSubCategory().then(function (Vdata) {
                $scope.AssetSubCategoryListS = Vdata;
                //$scope.AssetSubCategoryList = $scope.AssetSubCategoryListS;

            }, function (error) {
                console.error('Error fetching AssetSubCategory:', error);
            });
        }, function (error) {
            console.error('Error fetching AssetMainCategory:', error);
        });
    };

    $scope.onBindSubcategory = function () {


        $scope.AssetSubCategoryList = $filter('filter')($scope.AssetSubCategoryListS, { VT_CODE: $scope.AssetMainCategoryList.VT_CODE[0].VT_CODE });

    }
    $scope.LoadData = function () {

        AssetBrandService.GetBRNGrid().then(function (data) {

            $scope.gridata = data;
            $scope.gridOptions.api.setRowData($scope.gridata);

        }).catch(error =>
            console.error('Error loading grid data:', error)
        );
    };



    columnDefs = [
        { headerName: "Brand Name", field: "manufacturer", width: 270, cellClass: 'grid-align' },
        { headerName: "Brand Code", field: "manufactuer_code", width: 270, cellClass: 'grid-align' },
        { headerName: "Asset Subcategory", field: "AST_SUBCAT_NAME", width: 270, cellClass: 'grid-align' },
        { headerName: "Asset Category", field: "VT_TYPE", width: 270, cellClass: 'grid-align' },
        { headerName: "Status", field: "MANUFACTURER_STATUS", template: "{{ShowStatus(data.MANUFACTURER_STATUS)}}", width: 270, cellClass: 'grid-align' },

        { headerName: "Edit", width: 270, template: '<a ng-click="EditData(data)"> <i class="Edit">Edit</i> </a>', cellClass: 'grid-align', suppressMenu: true }

    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableSorting: true,
        angularCompileRows: true,
        enableColResize: true,
        onGridReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };


    $scope.Save = function () {

        var astbrnd = {
            manufactuer_code: $scope.brand.manufactuer_code,
            manufacturer: $scope.brand.manufacturer,
            MANUFACTUER_TYPE_CODE: $scope.AssetMainCategoryList.VT_CODE[0].VT_CODE,
            manufacturer_type_subcode: $scope.AssetSubCategoryList.AST_SUBCAT_CODE[0].AST_SUBCAT_CODE,
            MANUFACTUER_STATUS: $scope.brand.MANUFACTUER_STATUS,
            MANU_REM: $scope.brand.MANU_REM
        };

        if ($scope.isEditMode) {
            var obj = {
                manufactuer_code: $scope.brand.manufactuer_code,
                manufacturer: $scope.brand.manufacturer,
                MANUFACTUER_TYPE_CODE: $scope.AssetMainCategoryList.VT_CODE[0].VT_CODE,
                manufacturer_type_subcode: $scope.AssetSubCategoryList.AST_SUBCAT_CODE[0].AST_SUBCAT_CODE,
                MANUFACTUER_STATUS: $scope.brand.MANUFACTUER_STATUS,
                MANU_REM: $scope.brand.MANU_REM
            };

            AssetBrandService.ModifyAssetBrand(obj).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Brand/Make Successfully Modified";
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.isEditMode = false;
                $scope.buttonText = "Add";
                $scope.LoadData();
                $scope.resetForm();
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data.Message || "An error occurred.";
                showNotification('error', 8, 'bottom-right', $scope.Success);
                console.log(error);
            });
        } else {

            AssetBrandService.SaveAssetBrand(astbrnd).then(function (response) {

                if (response.Message == "Asset Brand already exists.") {
                    $scope.ShowMessage = true;
                    $scope.error = "Brand/Make Code is in use; try another";
                    showNotification('error', 8, 'bottom-right', $scope.error);
                    $scope.assetbrand.$submitted = false;
                    $scope.LoadData();

                    //$scope.resetForm();
                } else {
                    $scope.Success = "New Asset Brand/Make Successfully Added";
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', $scope.Success);
                    $scope.isEditMode = false;
                    $scope.LoadData();
                    $scope.resetForm();
                }
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data.Message || "An error occurred.";
                showNotification('error', 8, 'bottom-right', $scope.Success);
                console.log(error);
            });
        }
    };


    $scope.EditData = function (data) {

        $scope.brand = {};
        $scope.EnableStatus = 0;
        $scope.AssetSubCategoryList = $scope.AssetSubCategoryListS;
        angular.forEach($scope.AssetMainCategoryList, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.AssetSubCategoryList, function (value, key) {
            value.ticked = false;
        });

        $scope.brand = angular.copy(data);

        var cat = _.find($scope.AssetMainCategoryList, { VT_CODE: data.MANUFACTURER_TYPE_CODE });
        if (cat != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cat.ticked = true;
                    //$scope.Branch.Country.push(cny);
                });
            }, 100)
        }

        var subcat = _.find($scope.AssetSubCategoryListS, { AST_SUBCAT_CODE: data.AST_SUBCAT_CODE });
        if (subcat != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    subcat.ticked = true;
                    //$scope.Branch.Region.push(RGN);
                });
            }, 100)
        }

        $scope.brand.MANUFACTUER_STATUS = data.MANUFACTURER_STATUS;

        $scope.isEditMode = true;
        $scope.buttonText = "Modify";
    }


    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.ShowStatus = function (value) {
        /*$('#MANUFACTUER_STATUS').selectpicker('refresh');*/
        return $scope.StaDet[value == 0 ? 1 : 0].Name;

    }
    $scope.resetForm = function () {
        $scope.assetbrand.$submitted = false;
        $scope.brand = {};
        //$scope.brand.manufactuer_code = {};
        //$scope.brand.manufacturer = {};
        $scope.EnableStatus = 1;
        $scope.isEditMode = false;
        $scope.buttonText = "Add";
        //$('#MANUFACTUER_STATUS').value = 1;
        angular.forEach($scope.AssetMainCategoryList, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.AssetSubCategoryList, function (value, key) {
            value.ticked = false;
        });
        //setTimeout(function () {
        //    $('#MANUFACTUER_STATUS').val(1).prop('selected', 'selected');
        //}, 100)

    };

    $scope.LoadData();
    $scope.PageLoad();

}]);

