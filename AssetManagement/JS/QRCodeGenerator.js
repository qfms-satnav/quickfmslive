﻿app.service("QRCodeGeneratorService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/QRCodeGenerator/GetGriddata', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }
    this.GetAssetName = function (data) {
        return handleAPIRequest($http.post(UtilityService.path + '/api/QRCodeGenerator/AssetName', data));
    };

});
app.controller('QRCodeGeneratorController', function ($scope, $q, $http, QRCodeGeneratorService, UtilityService, $timeout, $filter, $window, $document) {
    $scope.QRCodeGenerator = {};
    $scope.LoadInfo = [];
    $scope.AssetName = [];
    $scope.Location = [];
    $scope.PrinttypeVisible = 0;

    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            $scope.QRCodeGenerator.CNP_NAME = parseInt(CompanySessionId);
            angular.forEach($scope.Company, function (value, key) {
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySessionId) });
                a.ticked = true;
            });
            if (CompanySessionId == "1") { $scope.EnableStatus = 1; }
            else { $scope.EnableStatus = 0; }
        }

    });
    UtilityService.GetLocationsall(1).then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
            //if ($scope.Location.length > 0) {
            //    $scope.Location[0].ticked = true;
            //}
            //QRCodeGeneratorService.GetAssetName().then(function (obj) {
            //    $scope.AssetNameS = obj;

            //    // Get the ticked LCM codes
            //    var tickedLCMCodes = $scope.Location
            //        .filter(function (item) {
            //            return item.ticked === true;
            //        })
            //        .map(function (item) {
            //            return item.LCM_CODE;
            //        });

            //    // Filter asset names based on ticked locations
            //    $scope.AssetName = $filter('filter')($scope.AssetNameS, function (asset) {
            //        return tickedLCMCodes.includes(asset.AAT_LOC_ID);
            //    });
            //    $scope.AssetName.forEach(asset => {
            //        asset.ticked = true;
            //    });
            //}, function (error) {
            //    console.error('Error fetching AssetName:', error);
            //});
        }
    });
    var columnDefsAsset = [
        { headerName: "Sno", width: 30, cellClass: "grid-align", filter: 'set', template: '<input type="checkbox" value="Update" ng-model="data.ticked"  >', headerCellRenderer: headerCellRendererFunc },
        { headerName: 'Asset Name', field: 'AAT_NAME', width: 150 }
    ];

    $scope.gridOptionsAsset = {
        columnDefs: columnDefsAsset,
        onReady: function () {
            $scope.gridOptionsAsset.api.sizeColumnsToFit()
        },
        rowData: null,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        rowSelection: 'multiple'
    };
  
    function headerCellRendererFunc(params) {
        var cb = document.createElement('input');
        var br = document.createElement('br');
        cb.setAttribute('type', 'checkbox');
        var eHeader = document.createElement('label');
        var eTitle1 = document.createTextNode(params.colDef.headerName);
        eHeader.appendChild(cb);
        eHeader.appendChild(eTitle1);
        cb.addEventListener('change', function (e) {
            if ($(this)[0].checked) {
                $scope.$apply(function () {
                    var filter = $('#filtertxtAsset').value;
                    if (filter != '' && filter != undefined) {
                        angular.forEach($scope.AllGridData, function (value, key) {
                            value.ticked = true;
                        });
                    }
                    else {
                        angular.forEach($scope.gridOptionsAsset.rowData, function (value, key) {
                            value.ticked = true;
                        });
                    }
                });
            } else {
                $scope.$apply(function () {
                    angular.forEach($scope.gridOptionsAsset.rowData, function (value, key) {
                        value.ticked = false;
                    });
                });
            }
        });
        return eHeader;
    }
    var columnDefs = [
        { headerName: 'Country', field: 'CNY_NAME', width: 150, height: 150 },
        { headerName: 'City', field: 'CTY_NAME', width: 150, height: 150 },
        { headerName: 'Location', field: 'LCM_NAME', width: 150, height: 150 },
        { headerName: 'Asset Code', field: 'AAT_CODE', width: 150, height: 150 },
        { headerName: 'Asset Name', field: 'AST_SUBCAT_NAME', width: 150, height: 150 },
        { headerName: 'Asset Serial No', field: 'AAT_AST_SERIALNO', width: 150, height: 150 },
        { headerName: 'Model', field: 'AST_MD_NAME', width: 150, height: 150 },
        { headerName: 'QR Code', field: 'QrCode', width: 150, height: 150, cellRenderer: deltaIndicator },
        { headerName: 'Asset Created Date', field: 'AAT_UPT_DT', width: 150, height: 150, cellRenderer: function (params) { return $filter('date')(params.value, 'MM/dd/yyyy'); } }

    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
        rowHeight: 80
    };

    function deltaIndicator(params) {
        var element = document.createElement("span");
        var DivElement = document.createElement("div");
        $(DivElement).qrcode(
            {
                width: 70,
                height: 70,
                text: params.data.AAT_CODE
            });
        element.appendChild(DivElement);
        element.appendChild(document.createTextNode(params.value));
        return element;
    }
    $scope.BINDASSET = function () {
        var data = {
            LocationLstAsts: $scope.QRCodeGenerator.LCM_NAME
        }
        QRCodeGeneratorService.GetAssetName(data).then(function (obj) {
            $scope.AssetNameS = obj;
            setTimeout(function () {
                $scope.gridOptionsAsset.api.setRowData($scope.AssetNameS);
            }, 100)
            progress(0, 'Loading...', false);
        });
    }
    $scope.locselectall = function () {
        var data = {
            LocationLstAsts: $scope.Location
        }
        QRCodeGeneratorService.GetAssetName(data).then(function (obj) {
            $scope.AssetNameS = obj;
            setTimeout(function () {
                $scope.gridOptionsAsset.api.setRowData($scope.AssetNameS);
            }, 100)
            progress(0, 'Loading...', false);
        });
    };

    $scope.LoadData = function () {
        debugger;
        progress(0, 'Loading...', true);
        $scope.QRCodeGenerator.fromdate = moment().subtract(7, 'days').format('MM/DD/YYYY');
        $scope.QRCodeGenerator.todate = moment().format('MM/DD/YYYY'); 
        
        var params = {
            loclst: $scope.QRCodeGenerator.LCM_NAME,
            //LCM_CODE: _.find($scope.Location, { ticked: true }).LCM_CODE,
            CompanyId: _.find($scope.Company, { ticked: true }).CNP_ID,
            FromDate: $scope.QRCodeGenerator.fromdate,
            ToDate: $scope.QRCodeGenerator.todate,
            AAT_CODE: $scope.AssetName.AAT_NAME
        };
        /*   $scope.QRCodeGenerator.printType = 'A4_Print';*/
        QRCodeGeneratorService.GetGriddata(params).then(function (response) {

            if (response.griddata == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                $scope.LoadInfo = response.griddata;
                $scope.gridOptions.api.setRowData(response.griddata);

                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 600);

            }

        }, function (error) {
            console.log(error);
        });
    }

    $scope.Search = function () {
        progress(0, 'Loading...', true);
        var assetName = _.filter($scope.gridOptionsAsset.rowData, { ticked: true });
        if (assetName.length == 0) {
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', "Please select Asset");
            return;
        }
        var dataObj = {
            loclst: $scope.QRCodeGenerator.LCM_NAME,
            //LCM_CODE: _.find($scope.Location, { ticked: true }).LCM_CODE,
            CompanyId: _.find($scope.Company, { ticked: true }).CNP_ID,
            FromDate: $scope.QRCodeGenerator.fromdate,
            ToDate: $scope.QRCodeGenerator.todate,
            AssetData: assetName
        };
        /*   $scope.QRCodeGenerator.printType = 'A4_Print';*/
        QRCodeGeneratorService.GetGriddata(dataObj).then(function (response) {


            if (!response.griddata || response.griddata.length === 0) {
                $scope.gridOptions.api.setRowData([]);
                $scope.PrinttypeVisible = 0;
                progress(0, 'Loading...', false);
            }
            else {
                $scope.LoadInfo = response.griddata;
                $scope.gridOptions.api.setRowData(response.griddata);
                $scope.PrinttypeVisible = 1;
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 600);

            }

        }, function (error) {
            $scope.PrinttypeVisible = 0;
            console.log(error);
        });
    }
    //$scope.BINDASSET = function () {
    //    var tickedLCMCodes = $scope.Location
    //        .filter(function (item) {
    //            return item.ticked === true;
    //        })
    //        .map(function (item) {
    //            return item.LCM_CODE;
    //        });

    //    // Filter AssetNameS based on the ticked LCM_CODE values
    //    $scope.AssetName = $filter('filter')($scope.AssetNameS, function (asset) {
    //        return tickedLCMCodes.includes(asset.AAT_LOC_ID);
    //    });
    //    $scope.AssetName.forEach(function (asset) {
    //        asset.ticked = true;
    //    });
    //};

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
            sum = 0;
            res = 0;
        } else {
            $scope.DocTypeVisible = 0;
        }

    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    function onFilterChangedAsset(value) {
        $scope.AllGridData = $scope.AssetNameS;
        if (value) {
            $scope.AllGridData = $filter('filter')($scope.AllGridData, function (item) {
                const filterValues = value.split(',').map(v => v.trim());
                return item.AAT_NAME && filterValues.some(val => item.AAT_NAME.includes(val));
            });
            $scope.gridOptionsAsset.api.setRowData($scope.AllGridData);
        } else {
            $scope.AllGridData = $scope.AssetNameS;
            $scope.gridOptionsAsset.api.setRowData($scope.AllGridData);
        }
    }

    $("#filtertxtAsset").keydown(function () {
        onFilterChangedAsset($(this).val());
    }).keyup(function () {
        onFilterChangedAsset($(this).val());
    }).bind('paste', function () {
        onFilterChangedAsset($(this).val());
    })

    setTimeout(function () { $scope.LoadData(); }, 1000);
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);

    }
    $scope.GenReport = function (data) {
        debugger;
        if ($scope.QRCodeGenerator.printType !== 'Label_Print' && $scope.QRCodeGenerator.printType !== 'A4_Print' &&
            $scope.QRCodeGenerator.printType !== 'Single_Print') {
            alert('Please select a print type before exporting.');
            return; // Stop the function execution if printType is not one of the valid options
        }


        if (data == 'pdf') {
            var codes = {};
            const songs = {
                ASTCode: [],
                ASTData: []
            };
            angular.forEach($scope.LoadInfo, function (val) {
                songs.ASTCode.push(val.AAT_CODE);
                songs.ASTData.push(val.AssetData);
            });

            if ($scope.QRCodeGenerator.printType === 'Label_Print') {
                generatePDF(songs);

            } else if ($scope.QRCodeGenerator.printType === 'A4_Print') {
                qr_generate(songs);
            }
            else if ($scope.QRCodeGenerator.printType === 'Single_Print') {
                generateSinglePDF(songs);
            }

        }

    }


    function QRCodeCanvas(params) {
        var element = document.createElement("span");
        $(element).qrcode(
            {
                width: 100,
                height: 100,
                text: params
            });
        var data = element.childNodes[0].toDataURL('image/png');
        $(element).remove();
        return data;
    }

    function generatePDF(songs) {
        debugger;
        options = {
            render: "canvas",
            width: 100,
            height: 100,
            typeNumber: -1,
            correctLevel: QRErrorCorrectLevel.H,
            background: "#ffffff",
            foreground: "#000000"
        };

        var pdf = new jsPDF({
            orientation: 'l',
            unit: 'cm',
            format: [5.08, 10.16] // Width and Height in cm
        });
        for (var i = 0; i < songs.ASTCode.length; i = i + 1) {
            if (i != 0) {
                pdf.addPage();
            }
            var qr = new QRCode(options.typeNumber, options.correctLevel);
            qr.addData(songs.ASTCode[i]);
            qr.make();
            // Get QR code module count
            const moduleCount = qr.getModuleCount();
            const scale = 1; // Scale factor to reduce the size of the QR code, less than 1 to decrease
            const maxModuleSize = Math.min(5 / moduleCount, 2.5 / moduleCount);
            const size = maxModuleSize * scale; // Apply scaling factor to decrease the size

            // Center the QR code on the small page
            const xOffset = 0.8;
            const yOffset = 0.4;

            // Draw each module (pixel) of QR code
            for (let row = 0; row < moduleCount; row++) {
                for (let col = 0; col < moduleCount; col++) {
                    if (qr.isDark(row, col)) {
                        pdf.setFillColor(0); // Black color
                        pdf.rect(xOffset + col * size, yOffset + row * size, size, size, 'F');
                    }
                }
            }
            // Add text below the QR code
            pdf.setFontSize(10);
            let AssetCode = null;
            let AssetName = null;
            let AssetSerialNo = null;
            let Description = null;
            let BranchCode = null;
            let parts = songs.ASTData[i].split('-@@#@-');

            angular.forEach(parts, function (x) {
                if (x.startsWith('Name:')) {
                    AssetName = x.replace('Name:', '').trim();
                } if (x.startsWith('Code:')) {
                    AssetCode = x.replace('Code:', '').trim();
                }
                if (x.startsWith('SNO:')) {
                    AssetSerialNo = x.replace('SNO:', '').trim();
                } if (x.startsWith('DESC:')) {
                    Description = x.replace('DESC:', '').trim();
                }
                if (x.startsWith('Branch Code:')) {
                    BranchCode = x.replace('Branch Code:', '').trim();
                }
            })
            //let AssetCode = parts[0].length > 0 ? parts[0] : "N/A";
            //let AssetName = parts[1].length > 0 ? parts[1] : "N/A";
            //let AssetSerialNo = parts[2].length > 0 ? parts[2] : "N/A";
            //let Description = parts[3].length > 0 ? parts[3] : "N/A";

            // Define maximum width in points (or adjust based on your layout)
            const maxTextWidth = 3; // Adjust based on your page size and positioning
            const maxLength = 22;   // Character limit before truncating (adjust based on desired length)

            function truncateText(text, maxWidth, maxLength) {
                // Check the length in points
                const textWidth = pdf.getStringUnitWidth(text) * pdf.internal.getFontSize() / pdf.internal.scaleFactor;

                // Truncate based on width or character length
                if (textWidth >= maxWidth || text.length >= maxLength) {
                    return text.substring(0, maxLength) + '...'; // Truncate and add ellipsis
                }
                return text;
            }

            // Handle null or undefined values by setting them as an empty string
            let truncatedAssetCode = truncateText(AssetCode || '', maxTextWidth, maxLength);
            let truncatedAssetName = truncateText(AssetName || '', maxTextWidth, maxLength);
            let truncatedAssetSerialNo = truncateText(AssetSerialNo || '', maxTextWidth, maxLength);
            let truncatedDescription = truncateText(Description || '', maxTextWidth, maxLength);
            let truncatedBranchCode = truncateText(BranchCode || '', maxTextWidth, maxLength);

            let xAxis1 = 0.2;  // X-axis for labels
            let yAxis1 = 3.5;  // Starting Y-axis position
            let space1 = 0.5;  // Spacing between lines
            let valueXAxis = 1.5;  // X-axis for values



            if (AssetCode) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("Code :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedAssetCode, valueXAxis, yAxis1, { align: 'left' });
                yAxis1 += space1;  // Move to the next line
            }

            if (AssetName) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("Name :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedAssetName, valueXAxis, yAxis1, { align: 'left' });
                yAxis1 += space1;  // Move to the next line
            }

            if (AssetSerialNo) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("SNo  :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedAssetSerialNo, valueXAxis, yAxis1, { align: 'left' });
                yAxis1 += space1;  // Move to the next line
            }

            if (Description) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("Des  :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedDescription, valueXAxis, yAxis1, { align: 'left' });
                yAxis1 += space1;  // Move to the next line
            }
            if (BranchCode) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("Branch Code  :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedBranchCode, valueXAxis + 1.5, yAxis1, { align: 'left' });
                yAxis1 += space1;  // Move to the next line
            }


            songs.ASTCode[i] = songs.ASTCode[i++];
            if (songs.ASTCode[i] != undefined) {
                var qr1 = new QRCode(options.typeNumber, options.correctLevel);
                qr1.addData(songs.ASTCode[i]);
                qr1.make();
                // Get QR code module count
                const moduleCount1 = qr1.getModuleCount();
                const scale1 = 1; // Scale factor to reduce the size of the QR code, less than 1 to decrease
                const maxModuleSize1 = Math.min(5 / moduleCount1, 2.5 / moduleCount1);
                const size1 = maxModuleSize1 * scale1; // Apply scaling factor to decrease the size

                // Center the QR code on the small page
                const xOffset1 = 6.1;
                const yOffset1 = 0.4;

                // Draw each module (pixel) of QR code
                for (let row = 0; row < moduleCount1; row++) {
                    for (let col = 0; col < moduleCount1; col++) {
                        if (qr1.isDark(row, col)) {
                            pdf.setFillColor(0); // Black color
                            pdf.rect(xOffset1 + col * size1, yOffset1 + row * size1, size1, size1, 'F');
                        }
                    }
                }
                // Add text below the QR code
                pdf.setFontSize(10);
                pdf.setFontSize(10);
                let AssetCode = null;
                let AssetName = null;
                let AssetSerialNo = null;
                let Description = null;
                let BranchCode = null;
                let parts = songs.ASTData[i].split('-@@#@-');

                angular.forEach(parts, function (x) {
                    if (x.startsWith('Name:')) {
                        AssetName = x.replace('Name:', '').trim();
                    } if (x.startsWith('Code:')) {
                        AssetCode = x.replace('Code:', '').trim();
                    }
                    if (x.startsWith('SNO:')) {
                        AssetSerialNo = x.replace('SNO:', '').trim();
                    } if (x.startsWith('DESC:')) {
                        Description = x.replace('DESC:', '').trim();
                    }
                    if (x.startsWith('Branch Code:')) {
                        BranchCode = x.replace('Branch Code:', '').trim();
                    }

                })
                //let AssetCode = parts[0].length > 0 ? parts[0] : "N/A";
                //let AssetName = parts[1].length > 0 ? parts[1] : "N/A";
                //let AssetSerialNo = parts[2].length > 0 ? parts[2] : "N/A";
                //let Description = parts[3].length > 0 ? parts[3] : "N/A";

                // Define maximum width in points (or adjust based on your layout)
                const maxTextWidth = 3; // Adjust based on your page size and positioning
                const maxLength = 17;   // Character limit before truncating (adjust based on desired length)

                function truncateText(text, maxWidth, maxLength) {
                    // Check the length in points
                    const textWidth = pdf.getStringUnitWidth(text) * pdf.internal.getFontSize() / pdf.internal.scaleFactor;

                    // Truncate based on width or character length
                    if (textWidth >= maxWidth || text.length >= maxLength) {
                        return text.substring(0, maxLength) + '...'; // Truncate and add ellipsis
                    }
                    return text;
                }

                // Handle null or undefined values by setting them as an empty string
                let truncatedAssetCode = truncateText(AssetCode || '', maxTextWidth, maxLength);
                let truncatedAssetName = truncateText(AssetName || '', maxTextWidth, maxLength);
                let truncatedAssetSerialNo = truncateText(AssetSerialNo || '', maxTextWidth, maxLength);
                let truncatedDescription = truncateText(Description || '', maxTextWidth, maxLength);
                let truncatedBranchCode = truncateText(BranchCode || '', maxTextWidth, maxLength);

                let xAxis1 = 5.8;  // X-axis for labels
                let yAxis1 = 3.5;  // Starting Y-axis position
                let space1 = 0.5;  // Spacing between lines
                let valueXAxis = 7;  // X-axis for values



                if (AssetCode) {
                    pdf.setFont('helvetica', 'bold');
                    pdf.text("Code :", xAxis1, yAxis1, { align: 'right' });
                    pdf.setFont('helvetica', 'normal');
                    pdf.text(truncatedAssetCode, valueXAxis, yAxis1, { align: 'left' });
                    yAxis1 += space1;  // Move to the next line
                }

                if (AssetName) {
                    pdf.setFont('helvetica', 'bold');
                    pdf.text("Name :", xAxis1, yAxis1, { align: 'right' });
                    pdf.setFont('helvetica', 'normal');
                    pdf.text(truncatedAssetName, valueXAxis, yAxis1, { align: 'left' });
                    yAxis1 += space1;  // Move to the next line
                }

                if (AssetSerialNo) {
                    pdf.setFont('helvetica', 'bold');
                    pdf.text("SNo  :", xAxis1, yAxis1, { align: 'right' });
                    pdf.setFont('helvetica', 'normal');
                    pdf.text(truncatedAssetSerialNo, valueXAxis, yAxis1, { align: 'left' });
                    yAxis1 += space1;  // Move to the next line
                }

                if (Description) {
                    pdf.setFont('helvetica', 'bold');
                    pdf.text("Des  :", xAxis1, yAxis1, { align: 'right' });
                    pdf.setFont('helvetica', 'normal');
                    pdf.text(truncatedDescription, valueXAxis, yAxis1, { align: 'left' });
                    yAxis1 += space1;  // Move to the next line
                }
                if (BranchCode) {
                    pdf.setFont('helvetica', 'bold');
                    pdf.text("Branch Code  :", xAxis1, yAxis1, { align: 'right' });
                    pdf.setFont('helvetica', 'normal');
                    pdf.text(truncatedBranchCode, valueXAxis + 1.5, yAxis1, { align: 'left' });
                    yAxis1 += space1;  // Move to the next line
                }

            }
        }
        pdf.save("qr_codes.pdf");
    }
    function generateSinglePDF(songs) {
        options = {
            render: "canvas",
            width: 100,
            height: 100,
            typeNumber: -1,
            correctLevel: QRErrorCorrectLevel.H,
            background: "#ffffff",
            foreground: "#000000"
        };

        var pdf = new jsPDF({
            orientation: 'l',
            unit: 'cm',
            format: [5.08, 10.16] // Width and Height in cm
        });
        for (var i = 0; i < songs.ASTCode.length; i++) {
            if (i != 0) {
                pdf.addPage();
            }
            var qr = new QRCode(options.typeNumber, options.correctLevel);
            qr.addData(songs.ASTCode[i]);
            qr.make();
            // Get QR code module count
            const moduleCount = qr.getModuleCount();
            const scale = 1; // Scale factor to reduce the size of the QR code, less than 1 to decrease
            const maxModuleSize = Math.min(5 / moduleCount, 2.5 / moduleCount);
            const size = maxModuleSize * scale; // Apply scaling factor to decrease the size

            // Center the QR code on the small page
            const xOffset = 3.5;
            const yOffset = 0.2;

            // Draw each module (pixel) of QR code
            for (let row = 0; row < moduleCount; row++) {
                for (let col = 0; col < moduleCount; col++) {
                    if (qr.isDark(row, col)) {
                        pdf.setFillColor(0); // Black color
                        pdf.rect(xOffset + col * size, yOffset + row * size, size, size, 'F');
                    }
                }
            }
            // Add text below the QR code
            pdf.setFontSize(10);
            let AssetCode = null;
            let AssetName = null;
            let AssetSerialNo = null;
            let Description = null;
            let BranchCode = null;
            let parts = songs.ASTData[i].split('-@@#@-');
            angular.forEach(parts, function (x) {
                if (x.startsWith('Name:')) {
                    AssetName = x.replace('Name:', '').trim();
                } if (x.startsWith('Code:')) {
                    AssetCode = x.replace('Code:', '').trim();
                }
                if (x.startsWith('SNO:')) {
                    AssetSerialNo = x.replace('SNO:', '').trim();
                } if (x.startsWith('DESC:')) {
                    Description = x.replace('DESC:', '').trim();
                }
                if (x.startsWith('Branch Code:')) {
                    BranchCode = x.replace('Branch Code:', '').trim();
                }

            })
            const maxTextWidth = 3;
            const maxLength = 28;

            function truncateText(text, maxWidth, maxLength) {
                const textWidth = pdf.getStringUnitWidth(text) * pdf.internal.getFontSize() / pdf.internal.scaleFactor;
                if (text.length >= maxLength) {
                    return text.substring(0, maxLength) + '...';
                }
                else if (text.length <= maxLength) {
                    return text;
                }

            }

            let truncatedAssetCode = truncateText(AssetCode || '', maxTextWidth, maxLength);
            let truncatedAssetName = truncateText(AssetName || '', maxTextWidth, maxLength);
            let truncatedAssetSerialNo = truncateText(AssetSerialNo || '', maxTextWidth, maxLength);
            let truncatedDescription = truncateText(Description || '', maxTextWidth, maxLength);
            let truncatedBranchCode = truncateText(BranchCode || '', maxTextWidth, maxLength);

            let xAxis1 = 3.5;  // X-axis for labels
            let yAxis1 = 3.5;  // Starting Y-axis position
            let space1 = 0.5;  // Spacing between lines
            let valueXAxis = 4.8;  // X-axis for values


            if (AssetCode) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("Code :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedAssetCode, valueXAxis, yAxis1, { align: 'left' });
                yAxis1 += space1;
            }

            if (AssetName) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("Name :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedAssetName, valueXAxis, yAxis1, { align: 'left' });
                yAxis1 += space1;
            }

            if (AssetSerialNo) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("SNo  :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedAssetSerialNo, valueXAxis, yAxis1, { align: 'left' });
                yAxis1 += space1;
            }

            if (Description) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("Des  :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedDescription, valueXAxis, yAxis1, { align: 'left' });
                yAxis1 += space1;
            }
            if (BranchCode) {
                pdf.setFont('helvetica', 'bold');
                pdf.text("Branch Code  :", xAxis1, yAxis1, { align: 'right' });
                pdf.setFont('helvetica', 'normal');
                pdf.text(truncatedBranchCode, valueXAxis + 1.5, yAxis1, { align: 'left' });
                yAxis1 += space1;
            }


        }
        pdf.save("qr_codes.pdf");
    }


    function qr_generate(codes, columns, scale, paper_width, paper_height, margin_left, margin_top) {
        columns = columns || 4;
        scale = scale || 0.9;
        paper_width = paper_width || 210.0;
        paper_height = paper_height || 297.0;
        margin_left = margin_left || 10.0;
        margin_top = margin_top || 10.0;

        var doc = new jsPDF();

        //QR CODE GENERATOR - DO NOT CHANGE ANY PARAMETERS EXCEPT columns
        //--------------------------------------------------------------------------
        var ratio = 8.0 / parseFloat(columns);
        var x_0 = 3.125 * ratio, y_0 = 3.125 * ratio, dx = 6.25 * ratio, dy = 6.25 * ratio; qr_width = 20.0 * ratio;
        var code_margin_top = 9.15 * ratio, code_margin_between = 1.0 * ratio;
        var font_size_1 = 4.5 * ratio,
            font_size_2 = 4.0 * ratio;

        x_0 *= scale;
        y_0 *= scale;
        dx *= scale;
        dy *= scale;
        qr_width *= scale;
        code_margin_top *= scale;
        code_margin_between *= scale;
        font_size_1 *= scale;
        font_size_2 *= scale;

        x_0 += margin_left;
        y_0 += margin_top;

        var codes_per_page = columns * columns;
        var red_square_width_ratio = 0.13;
        var QRErrorCorrectLevel = {
            L: 1,
            M: 0,
            Q: 3,
            H: 2
        };
        options = {
            render: "canvas",
            width: qr_width,
            height: qr_width,
            typeNumber: -1,
            correctLevel: QRErrorCorrectLevel.H,
            background: "#ffffff",
            foreground: "#000000"
        };

        for (let k = 0; k < codes.ASTCode.length; k++) {
            if (k != 0 && k % codes_per_page == 0) {
                doc.addPage();
            }

            if (k % codes_per_page == 0) {
                for (let i = 0; i < columns + 1; i++) {
                    doc.setDrawColor(200, 200, 200);
                    doc.setLineWidth(0.2);
                    doc.line(margin_left + scale * paper_width / columns * i, margin_top, margin_left + scale * paper_width / columns * i, margin_top + scale * paper_height); // vertical line
                }
                for (let i = 0; i < columns + 1; i++) {
                    doc.setDrawColor(200, 200, 200);
                    doc.setLineWidth(0.2);
                    doc.line(margin_left, margin_top + scale * paper_height / columns * i, margin_left + scale * paper_width, margin_top + scale * paper_height / columns * i); // horizontal line
                }
            }

            let x = x_0 + (k % columns) * (options.width + dx);
            let y_1 = y_0 + ((k % codes_per_page - k % columns) / columns) * (options.height + code_margin_top + code_margin_between + (parseFloat(font_size_1) + parseFloat(font_size_2)) / 17.0 * 6.0 + dy);

            // Create QR code
            let qrcode = new QRCode(options.typeNumber, options.correctLevel);
            qrcode.addData(codes.ASTCode[k]);
            qrcode.make();

            let tileW = options.width / qrcode.getModuleCount();
            let tileH = options.height / qrcode.getModuleCount();

            doc.setDrawColor(0);
            doc.setFillColor(0, 0, 0);

            for (let row = 0; row < qrcode.getModuleCount(); row++) {
                let black_line = 0;
                for (let col = 0; col < qrcode.getModuleCount(); col++) {
                    if (qrcode.isDark(row, col)) {
                        black_line += 1;
                        if (col == qrcode.getModuleCount() - 1) {
                            doc.rect(x + tileW * (col - black_line + 1), y_1 + tileH * row, tileW * black_line, tileH, 'F');
                        }
                    } else {
                        if (black_line != 0) {
                            doc.rect(x + tileW * (col - black_line), y_1 + tileH * row, tileW * black_line, tileH, 'F');
                            black_line = 0;
                        }
                    }
                }
            }

            // Parse code details
            let AssetCode = null;
            let AssetName = null;
            let AssetSerialNo = null;
            let Description = null;
            let BranchCode = null;
            const maxLength = 20;

            let parts = codes.ASTData[k].split('-@@#@-');
            angular.forEach(parts, function (x) {
                if (x.startsWith('Name:')) {
                    AssetName = truncateLength(x.replace('Name:', '').trim(), maxLength);
                }
                if (x.startsWith('Code:')) {
                    AssetCode = truncateLength(x.replace('Code:', '').trim(), maxLength);
                }
                if (x.startsWith('SNO:')) {
                    AssetSerialNo = truncateLength(x.replace('SNO:', '').trim(), maxLength);
                }
                if (x.startsWith('DESC:')) {
                    Description = truncateLength(x.replace('DESC:', '').trim(), maxLength);
                }
                if (x.startsWith('Branch Code:')) {
                    BranchCode = truncateLength(x.replace('Branch Code:', '').trim(), maxLength);
                }
            });

            function truncateLength(text, maxLength) {
                return text.length > maxLength ? text.substring(0, maxLength) + '...' : text;
            }

            // Text formatting
            let code = AssetCode;
            let name = AssetName;
            let sno = AssetSerialNo;
            let Desc = Description;
            let BRCode = BranchCode
            let Xspace = 10;  // X-axis space between label and value

            // Initial Y-axis positions
            var y_text_line = y_1 + options.height + 5;  // Starting Y-axis position for first line
            var lineSpacing = parseFloat(font_size_2) / 17.0 * 4.0 + code_margin_between;  // Spacing between lines

            // Print 'Code' if available
            if (code) {
                doc.setFontSize(font_size_1);
                doc.setFont('helvetica', 'bold');
                doc.text(x, y_text_line, "Code:");
                doc.setFont('helvetica', 'normal');
                doc.text(x + Xspace, y_text_line, code);
                y_text_line += lineSpacing;  // Move down only if 'Code' exists
            }

            // Print 'Name' if available
            if (name) {
                doc.setFontSize(font_size_2);
                doc.setFont('helvetica', 'bold');
                doc.text(x, y_text_line, "Name:");
                doc.setFont('helvetica', 'normal');
                doc.text(x + Xspace, y_text_line, name);
                y_text_line += lineSpacing;  // Move down only if 'Name' exists
            }

            // Print 'SNo' if available
            if (sno) {
                doc.setFontSize(font_size_2);
                doc.setFont('helvetica', 'bold');
                doc.text(x, y_text_line, "SNo:");
                doc.setFont('helvetica', 'normal');
                doc.text(x + Xspace, y_text_line, sno);
                y_text_line += lineSpacing;  // Move down only if 'SNo' exists
            }

            // Print 'DESC' if available
            if (Desc) {
                doc.setFontSize(font_size_2);
                doc.setFont('helvetica', 'bold');
                doc.text(x, y_text_line, "DESC:");
                doc.setFont('helvetica', 'normal');
                doc.text(x + Xspace, y_text_line, Desc);
                y_text_line += lineSpacing;  // Move down only if 'DESC' exists
            }
            if (BRCode) {
                doc.setFontSize(font_size_2);
                doc.setFont('helvetica', 'bold');
                doc.text(x, y_text_line, "Branch Code:");
                doc.setFont('helvetica', 'normal');
                doc.text(x + Xspace + 8, y_text_line, BRCode);
                y_text_line += lineSpacing;  // Move down only if 'DESC' exists
            }

        }

        doc.output('save', 'qr_codes.pdf');
    }





});