﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-blue .ag-row-odd {
            background-color: #fff;
        }

        .cell-fail {
            text-align: left;
            font-weight: bold;
            background-color: red;
        }

        .cell-pass {
            text-align: left;
            font-weight: bold;
            background-color: #4caf50;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BranchIncentiveSubCategoryController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3>Branch Incentive Sub Category</h3>
        </div>
        <div class="card">
            <form role="form" id="form1" name="BranchIncentiveSubCategory" data-valid-submit="Save()" novalidate>

                <div class="row">

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': BranchIncentiveSubCategory.$submitted && BranchIncentiveSubCategory.BCL_TP_NAME.$invalid}">
                            <label class="control-label">Incentive Type Name</label>
                            <div isteven-multi-select data-input-model="MainType" data-output-model="BranchIncentiveSubCategory.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" selection-mode="single"
                                data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="GetMainCategory()">
                            </div>
                            <input type="text" data-ng-model="BranchIncentiveSubCategory.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="BranchIncentiveSubCategory.$submitted && BranchIncentiveSubCategory.MainType.$invalid" style="color: red">Please Select Main Type </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': BranchIncentiveSubCategory.$submitted && BranchIncentiveSubCategory.BCL_MC_NAME.$invalid}">
                            <label class="control-label">Incentive Category Name</label>
                            <div isteven-multi-select data-input-model="Category" data-output-model="BranchIncentiveSubCategory.Category" data-button-label="icon BCL_MC_NAME" data-item-label="icon BCL_MC_NAME" selection-mode="single"
                                data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="SearchA()">
                            </div>
                            <input type="text" data-ng-model="BranchIncentiveSubCategory.Category[0]" name="BCL_MC_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="BranchIncentiveSubCategory.$submitted && BranchIncentiveSubCategory.Category.$invalid" style="color: red">Please Select Main Category </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Incentive Sub Category Name<span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Incentive Sub Category Name')" onmouseout="UnTip()">
                                <input id="BCL_SUB_NAME" class="form-control" type="text" name="BCL_SUB_NAME" maxlength="150" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_NAME" />
                            </div>
                        </div>
                    </div>

                     <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Incentive Sub Category Weightage<span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Sub Category Weightage')" onmouseout="UnTip()">
                                <input id="BCL_SUB_WEIGHTAGE" class="form-control" type="text" name="BCL_SUB_WEIGHTAGE" maxlength="50" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_WEIGHTAGE" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label"> Remarks <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <textarea id="BCL_SUB_REM" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_REM" class="form-control" maxlength="500"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12" style="display: none">
                        {{BranchIncentiveSubCategory.BCL_MC_ID}}
                                 
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Check Sub Type <span style="color: red;">*</span></label>
                                <div>
                                    <select id="Select1" name="BCL_SUB_TYPE" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_TYPE" class="form-control" data-ng-change="fn_check()">
                                        <option value="">Select Sub Type</option>
                                        <option data-ng-repeat="sta in StaSubType" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12" id="disp" style="display: none;">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Add No Of Sub Type <span style="color: red;">*</span></label>
                                <div>
                                    <select id="Select5" name="BCL_SUB_TYPE_NO" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_TYPE_NO" class="form-control" data-ng-change="fn_Set()">
                                        <option value="">Add No Of Sub Type</option>
                                        <option data-ng-repeat="sta in StaflagSubType4" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row" style="display:none">
                 
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Add File<span style="color: red;">*</span></label>
                                <div>
                                    <input id="BCL_SUB_FLAG_TYPE1" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE1" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">                                  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Add Comment<span style="color: red;">*</span></label>
                                <div>
                                    <input id="BCL_SUB_FLAG_TYPE2" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE2" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-2 col-sm-6 col-xs-12">
                              <div class="form-group">
                            <div class="row">
                                <label class="control-label">Add Textbox<span style="color: red;">*</span></label>
                                <div>
                                    <input id="BCL_SUB_FLAG_TYPE3" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE3" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">                                  
                                </div>
                            </div>
                        </div>
                          </div>
                    <div class="col-md-2 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label">Pick Date<span style="color: red;">*</span></label>
                                <div>
                                    <input id="BCL_SUB_FLAG_PLANTYPE" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_FLAG_PLANTYPE" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Raise Request<span style="color: red;">*</span></label>
                            <div>
                                <input id="BCL_SUB_FLAG_RAISEREQUEST" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_FLAG_RAISEREQUEST" type="checkbox" checked data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Yes" data-off="No">
                            </div>
                        </div>
                    </div>
                   <%-- <div class="row">
                    </div>--%>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Rating<span style="color: red;">*</span></label>
                            <div>
                                <input id="BCL_SUB_FLAG_TYPE_RATING" class="form-control" name="BCL_SUB_FLAG_TYPE_RATING" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE_RATING" type="number" min="0" max="5" value="0.0" step="0.1">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Rating Value<span style="color: red;">*</span></label>
                            <div>
                                <input id="BCL_SUB_FLAG_RATING_VALUE" class="form-control" name="BCL_SUB_FLAG_RATING_VALUE" data-ng-model="BranchIncentiveSubCategory.BCL_SUB_FLAG_RATING_VALUE" type="number" min="0" max="5">
                            </div>

                        </div>
                    </div>

                    </div> 
                </div>
                <div class="row" id="dvScore" style="display: none;">
                </div>
                <div class="row" id="dvScoreEdit" style="display: none;">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div data-ag-grid="gridOptions2" class="ag-blue" style="height: 210px; width: auto"></div>
                    </div>


                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1" />
                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                        </div>
                    </div>
                </div>
      <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                <div class="row" style="padding-left: 30px;">
                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                    <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../Js/BranchIncentiveSubCategory.js"></script>
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
</body>
</html>
