﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }
    </style>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd/M/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="BranchScoreCardReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">

                <h3 class="panel-title">Report </h3>
            </div>
            <div class="card">
                <form id="BranchScoreCardReport" name="frmBranchScoreCardReport" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12" style="display:none">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.LCM_ZONE.$invalid}">
                                <label class="control-label">Region</label>
                                <div isteven-multi-select data-input-model="Region" data-output-model="BranchScoreCardReport.Region" data-button-label="icon LCM_ZONE" data-item-label="icon LCM_ZONE"
                                    data-on-item-click="GetCitiesByZone()" data-on-select-all="cnySelectAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="BranchScoreCardReport.Region" name="LCM_ZONE" style="display: none" required="" />
                                <span class="error" style="color: red;" data-ng-show="frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.LCM_ZONE.$invalid">Please select Region </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.CTY_NAME.$invalid}">
                                <label class="control-label">City</label>
                                <div isteven-multi-select data-input-model="City" data-output-model="BranchScoreCardReport.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                    data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="BranchScoreCardReport.City" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" style="color: red;" data-ng-show="frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.CTY_NAME.$invalid">Please select city </span>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.LCM_NAME.$invalid}">
                                <label class="control-label">Location</label>
                                <div isteven-multi-select data-input-model="Location" data-output-model="BranchScoreCardReport.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                    data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="BranchScoreCardReport.Location" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" style="color: red;" data-ng-show="frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.LCM_NAME.$invalid">Please select location </span>
                            </div>
                        </div>
                          <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.AUR_NAME.$invalid}">
                                <label class="control-label">SPOC</label>
                                <div isteven-multi-select data-input-model="Spocs" data-output-model="BranchScoreCardReport.Spocs" data-button-label="icon AUR_KNOWN_AS" data-item-label="icon AUR_KNOWN_AS"
                                    data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="BranchScoreCardReport.Spocs" name="AUR_KNOWN_AS" style="display: none" required="" />
                                <span class="error" style="color: red;" data-ng-show="frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.AUR_KNOWN_AS.$invalid">Please select Spoc </span>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.FromDate.$invalid}">
                                <label class="control-label">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" placeholder="dd/mm/yyyy" id="Text1" name="FromDate" ng-model="BranchScoreCardReport.FromDate" required />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                    </span>
                                </div>
                                <span class="error" style="color: red;" data-ng-show="frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.FromDate.$invalid" style="color: red"></span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.ToDate.$invalid}">
                                <label class="control-label">To Date</label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" id="Text2" class="form-control" required="" placeholder="dd/mm/yyyy" name="ToDate" ng-model="BranchScoreCardReport.ToDate" />
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                    </span>
                                </div>
                                <span class="error" style="color: red;" data-ng-show="frmBranchScoreCardReport.$submitted && frmBranchScoreCardReport.ToDate.$invalid" style="color: red"></span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 100px; padding-right: 30px; padding-top: 25px;">
                            <input type="submit" value="Search" data-ng-click="search()" class="btn btn-primary custom-button-color" />
                        </div>

                    </div>

                    <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 pull-right">
                            <a data-ng-click="GenReport(ScoreCard,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                        </div>
                    </div>
                </div>
                    <div class="row">
                       <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="input-group" style="width: 25%">
                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                            <div class="input-group-btn">
                                <button class="btn btn-primary custom-button-color" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div>
                    </div>

                </form>
            </div>

        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../Js/BranchScoreCardReport.js"></script>
   <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
    <script type="text/javascript" defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        function setDateVals() {

            $('#Text1').datepicker({
                format: 'dd/M/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#Text2').datepicker({
                format: 'dd/M/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#Text1').datepicker('setDate', new Date(moment().subtract(29, 'days').format('DD/MMM/YYYY')));
            $('#Text2').datepicker('setDate', new Date(moment().format('DD/MMM/YYYY')));


        }
    </script>

    <script type="text/javascript" defer>
        $(document).ready(function () {
            setDateVals();
        });
    </script>

</body>
</html>
