﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BranchIncentiveTypeMasterController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3>Incentive Amount Master</h3>
        </div>
        <div class="card">
            <form role="form" id="form1" name="BranchIncentiveTypeMaster" data-valid-submit="Save()" novalidate>
                <div class="row">
                   
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': BranchIncentiveTypeMaster.$submitted && BranchIncentiveTypeMaster.BICM_CODE.$invalid}">
                            <label class="control-label">Category Code</label>
                            <div isteven-multi-select data-input-model="MainType" data-output-model="BranchIncentiveTypeMaster.MainType" data-button-label="icon BICM_CODE" data-item-label="icon BICM_CODE" selection-mode="single"
                                data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="SearchA()">
                            </div>
                            <input type="text" data-ng-model="BranchIncentiveTypeMaster.MainType[0]" name="BICM_CODE" style="display: none" required="" />
                            <span class="error" data-ng-show="BranchIncentiveTypeMaster.$submitted && BranchIncentiveTypeMaster.MainType.$invalid" style="color: red">Please Select Main Type </span>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">From Score <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <input id="BIM_FROM_SCORE" class="form-control" type="text" name="BIM_FROM_SCORE" maxlength="50" data-ng-model="BranchIncentiveTypeMaster.BIM_FROM_SCORE" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Upto Score <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <input id="BIM_UPTO_SCORE" class="form-control" type="text" name="BIM_UPTO_SCORE" maxlength="50" data-ng-model="BranchIncentiveTypeMaster.BIM_UPTO_SCORE" />
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Invoice Amount <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <input id="BIM_INCENTIVE_AMOUNT" class="form-control" type="text" name="BIM_INCENTIVE_AMOUNT" maxlength="50" data-ng-model="BranchIncentiveTypeMaster.BIM_INCENTIVE_AMOUNT" />
                            </div>
                        </div>
                    </div>
                      </div>
                <div class="row">
                         <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="row" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': BranchIncentiveTypeMaster.$submitted && BranchIncentiveTypeMaster.BIM_MC_STA_ID.$invalid}" style="width: 230px">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <select id="ddlsta" name="Status" data-ng-model="BranchIncentiveTypeMaster.BIM_MC_STA_ID" class="form-control">
                                        <option value="">--Select--</option>
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                    <span class="error" data-ng-show="BranchIncentiveTypeMaster.$submitted && BranchIncentiveTypeMaster.BIM_MC_STA_ID.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please Select Status</span>
                                </div>
                            </div>
                        </div>

                </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1" />
                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                        </div>
                    </div>
                </div>
                   <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width:auto" />
                        <div data-ag-grid="gridOptions" style="height: 250px;" class="ag-blue"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../Js/BranchIncentiveTypeMaster.js"></script>
  
</body>
</html>
