﻿<%@ Page Language="C#" %>


<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/Scripts/angular-confirm.css" rel="stylesheet" />
    <style>
        .package-table tbody tr:nth-child(odd) {
            background: #e4f7f8;
        }

        .package-table {
            border: 1px solid #6f7ea6;
        }

            .package-table td {
                border-top: 0px !important;
                border-right: 1px solid #6f7ea6 !important;
            }

            /*.package-table tbody tr:nth-child(even) {
            background: #DFDFDF;
        }*/

            .package-table thead tr th {
                color: #4b66a9;
                background-color: #d9edf7;
                border-color: black;
                height: 40px;
                font-size: 1rem;
                border: 2px solid black;
                border-top: 2px solid black !important;
            }

            .package-table tdoby tr td {
                height: 40px;
            }

        .list-group-item {
            border: none;
        }

        .panel {
            box-shadow: 0 5px 5px 0 rgba(0,0,0,.25);
        }

        .panel-heading {
            border-bottom: 1px solid rgba(0,0,0,.12);
        }

        .table-fixed thead {
            width: 97%;
        }

        .table-fixed tbody {
            height: 400px;
            overflow-y: auto;
            width: 100%;
        }

        .table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
            display: block;
        }

            .table-fixed tbody td, .table-fixed thead > tr > th {
                float: left;
                border-bottom-width: 0;
            }

        .table th {
            background-color: dimgray;
            color: white;
        }

        .table > tbody > tr > td {
            padding: 0.75rem;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        input[type=radio] {
            margin: 0px 20px 0 !important;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }

        .list-special .list-group-item:first-child {
            border-top-right-radius: 0px !important;
            border-top-left-radius: 0px !important;
        }
    </style>
    <style>
        .hide-column {
            display: none;
        }
    </style>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true,
                endDate: 'today',
                startDate: 'today'
            });
        };

    </script>
</head>
<body data-ng-controller="BranchIncentiveApproveController" class="amantra" ng-init="loadModal()">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Scorecard Approval
                    <button type="button" class="btn btn-primary custom-button-color" data-toggle="modal" data-target="#myModal" data-ng-click="GetDrafts(3)">Validated Scorecard</button>
                    <button type="button" class="btn btn-primary custom-button-color" ng-click="Clear()">Clear All</button>
                </h3>
            </div>
            <div class="card">
                <form id="Form1" name="frmBranchIncentiveApprove" novalidate>
                    <%--data-valid-submit="Submit()"--%>
                    <div class="clearfix">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Location <span style="color: red;">*</span></label>
                                <br />
                                {{BranchIncentiveApprove.LCM_NAME}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Scorecard Created By <span style="color: red;">*</span></label>
                                <br />
                                {{BranchIncentiveApprove.BCL_INSPECTED_BY}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Scorecard Created Date <span style="color: red;">*</span></label>
                                <br />
                                {{BranchIncentiveApprove.BCL_SELECTED_DT}}
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Scorecard Validated By <span style="color: red;">*</span></label>
                                <br />
                                {{BranchIncentiveApprove.BCLD_REVIEWED_BY}}
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Scorecard Validated Date <span style="color: red;">*</span></label>
                                <br />
                                {{BranchIncentiveApprove.BCLD_REVIEWED_DT}}
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchIncentiveApprove.$submitted && frmBranchIncentiveApprove.INSPECTOR.$invalid}">
                                <label class="control-label">Scorecard Approved By <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Inspection" data-output-model="BranchIncentiveApprove.Inspection" data-button-label="icon INSPECTOR" data-item-label="icon INSPECTOR"
                                    data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="BranchIncentiveApprove.Inspection" name="INSPECTOR" style="display: none" required="" />
                                <span class="error" data-ng-show="frmBranchIncentiveApprove.$submitted && frmBranchIncentiveApprove.INSPECTOR.$invalid" style="color: red">Please select Approved By  </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchIncentiveApprove.$submitted && frmBranchIncentiveApprove.SVR_FROM_DATE.$invalid}">
                                <label class="control-label">Scorecard Approved Date <span style="color: red;">*</span></label>
                                <div class="input-group date" style="width: 150px" id='fromdate'>
                                    <span class="input-group-addon">
                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                    </span>
                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="SVR_FROM_DATE" name="SVR_FROM_DATE" readonly ng-model="BranchIncentiveApprove.SVR_FROM_DATE" required />
                                </div>
                                <span class="error" data-ng-show="frmBranchIncentiveApprove.$submitted && frmBranchIncentiveApprove.SVR_FROM_DATE.$invalid" style="color: red">Please select Approved date</span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div class="col-md-12">
                            <table class="table package-table">
                                <thead>
                                    <tr>
                                        <th class="col-md-2 col-sm-6 col-xs-12"><b>Main Category</b></th>
                                        <th class="col-md-2 col-sm-6 col-xs-12"><b>Sub Category</b></th>
                                        <th class="col-md-4 col-sm-6 col-xs-12"><b>Child Category</b></th>
                                        <th class="col-md-1 col-sm-6 col-xs-12"><b>Weightage</b></th>
                                        <th class="col-md-7 col-sm-6 col-xs-12"><b>Score</b></th>
                                    </tr>
                                </thead>
                                <tbody data-ng-repeat="value in TableData track by $index">
                                    <tr data-ng-repeat="(idx,column) in value.ChecklistScore1 track by $index">
                                        <td>
                                            <b>{{column.BCL_TP_NAME}}</b>
                                        </td>
                                        <td>
                                            <b>{{column.BCL_MC_NAME}}</b>
                                        </td>
                                        <td><b>{{column.BCL_SUB_NAME}}</b></td>
                                        <td><b>{{column.BCL_SUB_WEIGHTAGE}} %</b></td>
                                        <td>
                                            <div class="row" style="width: max-content; margin-right: -10px;">
                                                <div ng-if="column.BCL_SUB_TYPE=='Radio'" class="form-group col-auto">
                                                    <%--   <ul class="list-group">--%>
                                                    <%-- <div class="form-group" data-ng-class="{'has-error': frmCheckList.$submitted && frmCheckList.{{row.BCL_CH_CODE}}.$invalid}">--%>
                                                    <div class="row">
                                                        <div class="col-auto" data-ng-repeat="row in column.checklistDetails">
                                                            <label class="form-check-label" ng-if="column.RESPONSE==row.BCL_CH_NAME">
                                                                <input type="radio" class="form-check-input" data-ng-model="column.RESPONSE" ng-change="OverallScore()" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}">
                                                                {{row.BCL_CH_NAME}}
                                                            </label>
                                                            <label class="form-check-label" ng-if="column.RESPONSE!=row.BCL_CH_NAME">
                                                                <input type="radio" class="form-check-input" data-ng-model="column.RESPONSE" ng-change="OverallScore()" name="{{row.BCL_CH_NAME}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_NAME}}">
                                                                {{row.BCL_CH_NAME}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <%--  <span class="error" data-ng-show="frmCheckList.$submitted && frmCheckList.{{row.BCL_CH_CODE}}.$invalid" style="color: red">Please Check Score</span>
                                                    </div>--%>
                                                    <%-- </ul>--%>
                                                </div>
                                                <div ng-if="column.BCL_SUB_TYPE=='Multiple'" class="form-group col-auto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                            <label>
                                                                <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_CODE}}">
                                                                {{row.BCL_CH_NAME}}
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div ng-if="column.BCL_SUB_TYPE=='Toggle'" class="form-group col-auto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item" data-ng-repeat="row in column.checklistDetails track by $index">
                                                            <label>
                                                                <input type="checkbox" data-ng-model="row.RESPONSE" name="{{row.BCL_CH_CODE}}" id="{{row.BCL_CH_CODE}}" value="{{row.BCL_CH_CODE}}">
                                                                {{row.BCL_CH_NAME}}
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div ng-if="column.ISDATE=='Yes'" class="form-group col-auto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <div class="form-group">
                                                                <label>Date: </label>
                                                                <div class="input-group date" id="{{column.BCL_SUB_CODE}}_1">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar" ng-click="DateClick(column.BCL_SUB_CODE)"></i>
                                                                    </div>
                                                                    <%-- placeholder="dd-mm-yyyy"--%>
                                                                    <input class="form-control" data-ng-model="column.DATERESPONSE" data-ng-bind="column.DATERESPONSE| date:'MM/dd/yyyy'" name="{{column.BCL_SUB_CODE}}_1" type="text" placeholder="dd/mm/yyyy" ng-disabled="true" />
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div ng-if="column.ISFILE=='Yes'" class="form-group col-auto">
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <label>
                                                                File Upload :
                                                            </label>
                                                            <input type="file" data-ng-model="column.FILEPATH" ngif-multiple="false" id="{{column.BCL_SUB_CODE}}" name="{{column.BCL_SUB_CODE}}" onchange="angular.element(this).scope().SelectFile(event)" accept=".png,.jpg,.xlsx,.pdf,.docx">
                                                        </li>
                                                    </ul>
                                                </div>
                                                <%--<div class="form-group">
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <label style="width: 100%">
                                                                Validate Comments:
                                                                <textarea class="form-control" rows="1" data-ng-model="column.ValidateInchargeComment" name="{{column.BCL_SUB_CODE}}" disabled="disabled" ng-maxlength="20"></textarea>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="form-group">
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <label style="width: 100%">
                                                                Approval Comments:
                                                                <textarea class="form-control" rows="2" data-ng-model="column.ApproveInchargeComment" name="{{column.BCL_SUB_CODE}}" ng-maxlength="20"></textarea>
                                                            </label>
                                                        </li>
                                                    </ul>
                                                </div>--%>
                                            </div>
                                        </td>

                                    </tr>
                                    <tr style="height: 30px; background-color: #ffa500; padding-bottom: 5px;">
                                        <td></td>
                                        <td></td>
                                        <td><b>Total</b></td>
                                        <td><span style="color: black"><b>{{value.TotalWeightage}}</b></span></td>
                                        <td><span style="color: black"><b>{{value.TotalWeightagePercent}} %</b></span></td>
                                    </tr>

                                </tbody>
                                <tr style="height: 30px; background-color: #3cb371">
                                    <td></td>
                                    <td></td>
                                    <td><b>Final</b></td>
                                    <td><span style="color: black"><b>{{FinalTotalWeightage}}</b></span></td>
                                    <td><span style="color: black"><b>{{FinalTotalWeightagePercent}} %</b></span></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div style="margin-top: 1px;">
                                <label for="txtcode" class="custom-file"><strong>Scorecard Comments:</strong></label>
                                <textarea name="OVERALL_CMTS" id="OVERALL_CMTS" disabled="disabled" class="form-control" data-ng-model="BranchIncentiveApprove.OVERALL_CMTS" cols="20" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div style="margin-top: 1px;">
                                <label for="txtcode" class="custom-file"><strong>Validate Comments:</strong></label>
                                <textarea name="BCL_VALIDATE_OVERALL_CMTS" id="BCL_VALIDATE_OVERALL_CMTS" disabled="disabled" class="form-control" data-ng-model="BranchIncentiveApprove.BCL_VALIDATE_OVERALL_CMTS" cols="20" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <div style="margin-top: 1px;">
                                <label for="txtcode" class="custom-file"><strong>Approval Comments:</strong></label>
                                <textarea name="BCL_APPROVEL_OVERALL_CMTS" id="BCL_APPROVEL_OVERALL_CMTS" class="form-control" data-ng-model="BranchIncentiveApprove.BCL_APPROVEL_OVERALL_CMTS" cols="20" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />



                    <div class="clearfix">
                        <div id="dvbutton" class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-top: 26px">
                            <%--<input type="submit" value="Submit" class="btn btn-primary custom-button-color" />--%>
                            <input type="submit" value="Submit" name="Submit" ng-model='name' data-ng-click="Submit(4)" class="btn btn-primary custom-button-color" />
                            <input type="submit" value="Reject" name="Reject" data-ng-click="Submit(6)" class="btn btn-primary custom-button-color" />

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <div class="modal" id="myModal" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="margin-top: -100px; width: 5500px;">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title" id="hsavedtitle">Saved Items</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="col-md-12 table-responsive">
                        <table class="table GridStyle">
                            <thead>
                                <tr class="info">
                                    <th><b>Select All</b><input type="checkbox"  id="selectAll" ng-click="selectAllRecords()" /></th>
                                    <th><b>Request ID</b></th>
                                    <th><b>Location Code</b></th>
                                    <th><b>Location Name</b></th>
                                    <th><b>Submitted Date</b></th>
                                    <th><b>Submitted By</b></th>
                                    <th><b>Score</b></th>
                                    <th class="hide-column"><b>BIC Score</b></th>
                                    <th><b>Status</b></th>
                                </tr>
                            </thead>
                            <tr data-ng-repeat="items  in BranchIncentiveApprove.SaveList" ng-class="{'selected': item.isChecked}" >

                                <td>
                                    <input type="checkbox" class="row-checkbox" ng-model="items.isChecked" />
                                    {{item.BCL_ID}}
                                </td>
                                <td>
                                    <a href data-ng-bind="items.BCL_ID" class="control-label" data-ng-model="items.BCL_ID" ng-click="LoadData(items)" data-bs-dismiss="modal"></a>
                                </td>
                                <td>
                                    <label data-ng-bind="items.LCM_CODE" class="control-label" data-ng-model="items.LCM_CODE"></label>
                                </td>
                                <td>
                                    <label data-ng-bind="items.LCM_NAME" class="control-label" data-ng-model="items.LCM_NAME"></label>
                                </td>

                                <td>
                                    <label data-ng-bind="items.BCL_SELECTED_DT| date:'MM/dd/yyyy'" class="control-label" data-ng-model="items.BCL_SELECTED_DT">
                                    </label>
                                    <a ng-click="DeleteCheckList(items.BCL_ID,items.BCL_SUBMIT)" data-ng-if="items.BCL_SUBMIT==0"><i class="fa fa-trash"></i></a>
                                </td>
                                <td>
                                    <label data-ng-bind="items.BCL_INSPECTED_BY" class="control-label" data-ng-model="items.BCL_INSPECTED_BY"></label>
                                </td>
                                <td>
                                    <label data-ng-bind="items.SCORE" class="control-label" data-ng-model="items.SCORE"></label>
                                </td>
                                <td class="hide-column">
                                    <label data-ng-bind="items.bicscore" class="control-label" data-ng-model="items.bicscore"></label>
                                </td>
                                <td>
                                    <label data-ng-bind="Getstring(items.BCL_SUBMIT)" class="control-label" data-ng-model="items.BCL_SUBMIT"></label>
                                </td>

                            </tr>
                        </table>
                        <br />
                    </div>
                </div>

                <div class="modal-footer">

                    <input type="submit" value="Submit" name="Submit" ng-click="submitCheckedRecords()" class="btn btn-primary custom-button-color" />
                    <button type="button" class="btn btn-primary custom-button-color" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>

    <%=ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/date.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <script src="../../BootStrapCSS/Scripts/angular-confirm.js"></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select", "ui.date", "cp.ngConfirm"]);
        var companyid = '<%= Session["TENANT"] %>';
    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../Js/CreateScoreCard.js"></script>
    <script src="../Js/BranchIncentiveValidate.js"></script>
    <script src="../Js/BranchIncentiveApprove.js"></script>
    <%--<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>--%>
    <script>
        $(document).ready(function() {
            $('#selectAll').change(function() {
                var checked = $(this).is(':checked');
                $('.row-checkbox').prop('checked', checked);
                if (checked) {
                    $('tbody tr').addClass('selected');
                } else {
                    $('tbody tr').removeClass('selected');
                }
            });

            $('.row-checkbox').change(function() {
                if (!$(this).is(':checked')) {
                    $('#selectAll').prop('checked', false);
                }
                var row = $(this).closest('tr');
                row.toggleClass('selected', $(this).is(':checked'));
            });
        });
    </script>

</body>
</html>

