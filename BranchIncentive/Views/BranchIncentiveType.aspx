﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BranchIncentiveType.aspx.cs" Inherits="BranchIncentive_Views_BranchIncentiveType" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BranchIncentiveTypeController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3>Branch Incentive Type</h3>
        </div>
        <div class="card">
            <form role="form" id="form1" name="BranchIncentiveType" data-valid-submit="Save()" novalidate>
                <div class="row">
                   
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Incentive Type Name <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <input id="BCL_TP_NAME" class="form-control" type="text" name="BCL_TP_NAME" maxlength="50" data-ng-model="BranchIncentiveType.BCL_TP_NAME" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Incentive Type Remarks <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <textarea id="txtrem" data-ng-model="BranchIncentiveType.BCL_TP_REMARKS" class="form-control" maxlength="500"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                                <label class="control-label">Incentive  Approval Level <span style="color: red;">*</span></label>
                                <div>
                                    <select id="BCL_TP_LEVEL" name="BCL_TP_LEVEL" data-ng-model="BranchIncentiveType.BCL_TP_LEVEL" class="form-control selectpicker">
                                        <option value="">Select Incentive  Approval Level</option>
                                        <option data-ng-repeat="sta in StaTypeLevel" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12" style="display: none">
                        {{SubCatLocMap.BCL_TP_ID}}
                                 
                    </div>
                    
                </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1" />
                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                        </div>
                    </div>
                </div>
                 <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>

                 <div class="row">
                       <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="input-group" style="width: 25%">
                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                            <div class="input-group-btn">
                                <button class="btn btn-primary custom-button-color" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div>
                    </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../Js/BranchIncentiveType.js"></script>
</body>
</html>
