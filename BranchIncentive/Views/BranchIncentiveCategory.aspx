﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BranchIncentiveCategory.aspx.cs" Inherits="BranchIncentive_Views_BranchIncentiveCategory" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BranchIncentiveCategoryController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Incentive Category Master</h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="frmBranchincentive" data-valid-submit="Save()" novalidate>
                    <div class="row">
                         <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.BICM_CODE.$invalid}">
                                <label>Category Code</label>
                                    <input id="txtcode" name="BICM_CODE" type="text" data-ng-model="CategoryType.BICM_CODE" maxlength="15" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.BICM_CODE.$invalid" style="color: red">Please Enter Valid Code</span>
                                </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Category Name</label>
                                <div data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.BICM_NAME.$invalid}">
                                    <input id="txtname" type="text" name="BICM_NAME" maxlength="100" data-ng-model="CategoryType.BICM_NAME" autofocus required="" class="form-control" />
                                    <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.BICM_NAME.$invalid" style="color: red">Please Enter Valid Name</span>&nbsp; 
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Range</label>
                                <div data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.BICM_RANGE.$invalid}">
                                    <input id="txtrange" type="text" name="BICM_RANGE" maxlength="100" data-ng-model="CategoryType.BICM_RANGE" autofocus required="" class="form-control" />
                                    <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.BICM_RANGE.$invalid" style="color: red">Please Enter Valid Range</span>&nbsp; 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="row" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.BICM_STA_ID.$invalid}" style="width: 230px">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <select id="ddlsta" name="Status" data-ng-model="CategoryType.BICM_STA_ID" class="form-control">
                                        <option value="">--Select--</option>
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                    <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.BICM_STA_ID.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please Select Status</span>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
                                <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                            </div>
                        </div>
                    </div>
                 <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>

                       <div class="row">
                       <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="input-group" style="width: 25%">
                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                            <div class="input-group-btn">
                                <button class="btn btn-primary custom-button-color" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../Js/BranchIncentiveCategory.js"></script>
    <script src="../../SMViews/Utility.js"></script>
</body>
</html>
