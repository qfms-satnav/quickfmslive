﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="BranchIncentiveManCategoryController" class="amantra">
    <div class="al-content">
        <div class="widgets">
            <h3>Branch Incentive Main Category</h3>
        </div>
        <div class="card">
            <form role="form" id="form1" name="BranchIncentiveManCategory" data-valid-submit="Save()" novalidate>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': BranchIncentiveManCategory.$submitted && BranchIncentiveManCategory.BCL_TP_NAME.$invalid}">
                            <label class="control-label">Incentive Type Name</label>
                            <div isteven-multi-select data-input-model="MainType" data-output-model="BranchIncentiveManCategory.MainType" data-button-label="icon BCL_TP_NAME" data-item-label="icon BCL_TP_NAME" selection-mode="single"
                                data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-on-item-click="SearchA()">
                            </div>
                            <input type="text" data-ng-model="BranchIncentiveManCategory.MainType[0]" name="BCL_TP_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="BranchIncentiveManCategory.$submitted && BranchIncentiveManCategory.MainType.$invalid" style="color: red">Please Select Main Type </span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Incentive Category Name <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <input id="BCL_MC_NAME" class="form-control" type="text" name="BCL_MC_NAME" maxlength="50" data-ng-model="BranchIncentiveManCategory.BCL_MC_NAME" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Remarks <span style="color: red;"></span></label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <textarea id="BCL_MC_REM" data-ng-model="BranchIncentiveManCategory.BCL_MC_REM" class="form-control" maxlength="500"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12" style="display: none">
                        {{BranchIncentiveManCategory.BCL_MC_ID}}
                    </div>                  
                </div>

                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-click="SaveA()" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-click="Update()" data-ng-show="ActionStatus==1" />
                            <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                            <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                        </div>
                    </div>
                </div>
                <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>

                <div class="row">
                    <div class="col-md-12">
                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../Js/BranchIncentiveManCategory.js"></script>
</body>
</html>