﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-viewport-wrapper.ag-layout-normal {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="BranchIncentiveLocationMasterController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3> Incentive Location Master</h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="frmBranchincentive" data-valid-submit="SubmitData()" novalidate>
                    <div class="clearfix">
                     <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="ActionStatus==0">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.LCM_NAME.$invalid}">
                                <label class="control-label"> Select Branch Name </label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="Branch.LCM_CODE" data-button-label="icon LCM_NAME" selection-mode="single" 
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Branch.LCM_CODE" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.LCM_NAME.$invalid" style="color: red">Please select Branch Name </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12" data-ng-show="ActionStatus==1">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.BILCM_LOC_CODE.$invalid}">
                                <label>Branch Code</label>
                                    <input id="txtcode" name="BILCM_LOC_CODE" type="text" data-ng-model="Branch.BILCM_LOC_CODE" maxlength="15" class="form-control" data-ng-disabled="true" />
                                    <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.BILCM_LOC_CODE.$invalid" style="color: red">Please Enter Valid Code</span>
                                </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12" data-ng-show="ActionStatus==1">
                            <div class="form-group">
                                <label>Branch Name</label>
                                <div data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.BILCM_LOC_NAME.$invalid}">
                                    <input id="txtname" type="text" name="BILCM_LOC_NAME" maxlength="100" data-ng-model="Branch.BILCM_LOC_NAME"  class="form-control" data-ng-disabled="ActionStatus==0" />
                                    <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.BILCM_LOC_NAME.$invalid" style="color: red">Please Enter Valid Name</span>&nbsp; 
                                </div>
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.BICM_NAME.$invalid}">
                                <label class="control-label">Category code  </label>
                                <div isteven-multi-select data-input-model="Codes" data-output-model="Branch.BICM_CODE" data-button-label="icon BICM_NAME" selection-mode="single"
                                    data-item-label="icon BICM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Branch.BICM_CODE" name="BICM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.BICM_NAME.$invalid" style="color: red">Please select Category Code </span>
                            </div>
                        </div>
                         <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="row" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frmBranchincentive.$submitted && frmBranchincentive.BILCM_STA_ID.$invalid}" style="width: 230px">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <select id="ddlsta" name="Status" data-ng-model="Branch.BILCM_STA_ID" class="form-control">
                                        <option value="">--Select--</option>
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                    <span class="error" data-ng-show="frmBranchincentive.$submitted && frmBranchincentive.BILCM_STA_ID.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please Select Status</span>
                                </div>
                            </div>
                        </div>
                        </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
                                <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                            </div>
                        </div>
                    </div>
                    <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                      <div class="row">
                       <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="input-group" style="width: 25%">
                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                            <div class="input-group-btn">
                                <button class="btn btn-primary custom-button-color" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
     <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
<%--    <script src="../../Scripts</div>/jspdf.plugin.autotable.src.js"></script>--%>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../Js/BranchIncentiveLocationMaster.js"></script>
    <script src="../../SMViews/Utility.js"></script>
</body>
</html>
