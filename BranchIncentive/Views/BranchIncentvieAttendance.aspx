﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd/M/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>


</head>
<body data-ng-controller="BranchIncentvieAttendanceController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>BIC ATTENDANCE </h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="frmAttendance" data-valid-submit="SubmitData()" novalidate>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <input type="file" id="fileUpl" class="form-control" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <input type="button" value="Upload File" data-ng-click="UploadFile()" class="btn btn-primary custom-button-color" />
                            </div>
                           <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <a id="Attendance" href="BIC_ATTENDANCE.xlsx">Click here to view the template</a>
                                </div>
                            </div>--%>
                             <div class="col-md-3 col-sm-4 col-xs-6">
                                <input type="button" value="Download Excel" class='btn btn-primary custom-button-color' id="btnDownload" data-ng-click="ExcelDownload()" />
                               <%-- <a class='btn btn-primary custom-button-color' id="btnDownload" href="../../LocationExcelFiles/LocWiseUtility_Excel.xlsx">Download Excel</a>--%>
                            </div>
                        </div>
                </form>
              <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                <div class="row">
                    <div class="col-md-12">
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../Js/BranchIncentvieAttendance.js"></script>
   <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>

</body>
</html>