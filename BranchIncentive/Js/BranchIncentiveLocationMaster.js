﻿app.service("BranchIncentiveLocationMasterService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetCategory = function (data) {
        debugger;
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveLocationMaster/GetCategory', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveData = function (Ddata) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveLocationMaster/SaveData', Ddata)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.ModifyCategoryDetails = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/BranchIncentiveLocationMaster/ModifyCategoryDetails', response)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.BindGridData = function () {
        debugger;
        deferred = $q.defer();
        return $http.post('../../api/BranchIncentiveLocationMaster/BindGridData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('BranchIncentiveLocationMasterController', ['$scope', '$q', 'BranchIncentiveLocationMasterService', 'UtilityService', '$filter', function ($scope, $q, BranchIncentiveLocationMasterService, UtilityService, $filter) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.Branch = {};
    $scope.Codes = [];

    var columnDefs = [
        { headerName: "Branch Name", field: "BILCM_LOC_NAME", width: 150, cellClass: 'grid-align' },
        { headerName: "Branch Code", field: "BILCM_LOC_CODE", width: 150, cellClass: 'grid-align' },
        { headerName: "Category Code", field: "BILCM_CAT_CODE", width: 150, cellClass: 'grid-align' },
        { headerName: "Status", template: "{{ShowStatus(data.BILCM_STA_ID)}}", width: 170, cellClass: 'grid-align' },
        { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enablefilter: true,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    };


    $scope.Pageload = function () {

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                BranchIncentiveLocationMasterService.GetCategory().then(function (response) {
                    $scope.Codes = response;
                    BranchIncentiveLocationMasterService.BindGridData().then(function (data) {
                        $scope.gridata = data;
                        $scope.gridOptions.api.setRowData(data);
                        setTimeout(function () {
                            progress(0, 'Loading...', false);
                        }, 700);
                    }, function (error) {
                        console.log(error);
                    });

                    //angular.forEach($scope.Codes, function (value, key) {
                    //    value.ticked = true;
                    //})
                });
                //angular.forEach($scope.Locations, function (value, key) {
                //    value.ticked = true;
                //})
            }
        });
    }



    //$scope.SubmitData = function () {
    //    debugger;
    //    {
    //        $scope.Branch.LCM_CODE = $scope.Branch.LCM_CODE[0].LCM_CODE;
    //        $scope.Branch.BICM_CODE = $scope.Branch.BICM_CODE[0].BICM_CODE;

    //        BranchIncentiveLocationMasterService.SaveData($scope.Branch).then(function (response) {
    //            if (response.data != null) {
    //                debugger;
    //                $scope.ShowMessage = true;
    //                $scope.Success = "Data Successfully Inserted ";
    //            }
    //            else {
    //                showNotification('error', 8, 'bottom-right', response.Message);
    //            }

    //        }, function (error) {
    //            showNotification('error', 8, 'bottom-right', error);
    //        });

    //    }
    //}

    $scope.SubmitData = function () {
        debugger;
        $scope.Branch.LCM_CODE = $scope.Branch.LCM_CODE[0].LCM_CODE;
        $scope.Branch.BICM_CODE = $scope.Branch.BICM_CODE[0].BICM_CODE;
        /* BILCM_ID = $scope.Branch.BILCM_ID[0].BILCM_ID;*/
        if ($scope.IsInEdit) {
            BranchIncentiveLocationMasterService.ModifyCategoryDetails($scope.Branch).then(function (response) {
                debugger;
                var savedobj = {};
                angular.copy($scope.Branch, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data Modified successfully ";

                BranchIncentiveLocationMasterService.BindGridData().then(function (data) {
                    $scope.gridata = data;
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.IsInEdit = false;
                $scope.EraseData();
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            });
        }
        else {
            $scope.Branch.BILCM_STA_ID = "1";
            BranchIncentiveLocationMasterService.SaveData($scope.Branch).then(function (response) {
                debugger;
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully inserted";
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.Pageload();
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }

    $scope.EditData = function (data) {
        debugger;
        $scope.Branch = {};
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        angular.copy(data, $scope.Branch);
        $scope.Locations = [];
        $scope.Codes = [];
        UtilityService.getLocations(2).then(function (response) {
            $scope.Locations = response.data;
            angular.forEach($scope.Locations, function (value, key) {
                value.ticked = false;
                if (value.LCM_CODE.toString() == data.BILCM_LOC_CODE) {
                    value.ticked = true;
                }
            });
        });
        BranchIncentiveLocationMasterService.GetCategory().then(function (response) {
            $scope.Codes = response;
            angular.forEach($scope.Codes, function (value, key) {
                value.ticked = false;
                if (value.BICM_CODE.toString() == data.BILCM_CAT_CODE) {
                    value.ticked = true;
                }
            });
        })
        $scope.EditCategory = data;
        $scope.Branch.BILCM_LOC_CODE = value.BILCM_LOC_CODE;
        $scope.Branch.BILCM_LOC_NAME = value.BILCM_LOC_NAME;
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }
    $scope.EraseData = function () {
        $scope.Branch = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmBranchincentive.$submitted = false;

    }

    $scope.Pageload();

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "BranchIncentive_Location_Master.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();

    }

}]);