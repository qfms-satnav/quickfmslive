﻿
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
app.service("ValidateScoreCard", function ($http, $q, UtilityService) {

    this.GetGriddata = function (BCL_ID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/GetGriddata?BCL_ID=' + BCL_ID)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetScorePer = function () {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CreateScoreCard/GetScorePer')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.getSavedList = function (BCLStatus, Status) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/getSavedList?BCLStatus=' + BCLStatus + '&Status=' + Status + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getInspectorsZonalCentral = function (TPM_TP_ID, TPM_LCM_CODE, TPMD_APPR_LEVELS) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/getInspectorsZonalCentral?TPM_TP_ID=' + TPM_TP_ID + '&TPM_LCM_CODE=' + TPM_LCM_CODE + '&TPMD_APPR_LEVELS=' + TPMD_APPR_LEVELS + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveValidationData = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/SaveValidationData', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('BranchIncentiveValidateController', function ($scope, $q, $location, ValidateScoreCard, UtilityService, $filter, $http, $ngConfirm) {

    $scope.BranchIncentiveValidate = {};
    $scope.TableData = [];
    $scope.rowData = [];
    $scope.dvbutton = true;
    $scope.ChecklistID = '';
    $scope.Score_Per = [];

    $scope.OverallScore = function () {
        $scope.FinalTotalWeightagePercent = 0;
        _.forEach($scope.TableData, function (value) {
            var OverAllScore = 0;
            _.forEach(value.ChecklistScore1, function (value1) {

                var ScorePercenatge = _.find($scope.Score_Per, { BCL_CH_NAME: value1.RESPONSE })

                value1.BCL_WEIGHTAGE_PERCENTAGE = parseInt(value1.BCL_SUB_WEIGHTAGE, 10) * (parseInt(ScorePercenatge != undefined ? ScorePercenatge.BCL_CH_PERCENTAGE : 0, 10) / 100);
                OverAllScore = OverAllScore + value1.BCL_WEIGHTAGE_PERCENTAGE
            });
            value.TotalWeightagePercent = OverAllScore;
            $scope.FinalTotalWeightagePercent = $scope.FinalTotalWeightagePercent + OverAllScore
        });
    }

    //$scope.Getstring = function (data) {
    //    return (data =='2') ? 'Submitted' : 'Validated';
    //}
    $scope.Getstring = function (data) {
        if (data == '2') {
            return 'Submitted';
        } else if (data == '7') {
            return 'Partially Validated Health';
        } else if (data == '8') {
            return 'Partially Validated';
        }
    };


    $scope.GetDrafts = function (BCLStatus) {
        debugger;

        if (BCLStatus == 1) {
            $('#hsavedtitle').html("Saved Drafts");
        }
        else {
            $('#hsavedtitle').html("Saved Scorecard");

        }
        var Status = 1;
        ValidateScoreCard.getSavedList(BCLStatus, Status).then(function (response) {
            if (response.data != null) {
                $scope.BranchIncentiveValidate.SaveList = response.data;

            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    ValidateScoreCard.GetScorePer().then(function (response) {
        if (response.data != null) {
            $scope.Score_Per = response.data;
            //$scope.LoadData();
        };
    });
    $scope.BranchIncentiveValidate.SVR_FROM_DATE = moment().format('MM/DD/YYYY');
    $scope.DateClick = function (id) {
        $('#' + id + '_1').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        });
    }

    $scope.LoadIncharge = function (items) {
        ValidateScoreCard.getInspectorsZonalCentral(0, items.LCM_CODE, 0).then(function (response) {
            debugger;
            if (response.data != null) {
                $scope.Inspection = response.data;
                angular.forEach($scope.Inspection, function (item, index) {
                    if (index == 0) {
                        item.ticked = true
                    }
                });
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    var SCPenalty = '';
    $scope.LoadData = function (items) {
        debugger;
        $scope.LoadIncharge(items);
        $scope.BranchIncentiveValidate.LCM_NAME = items.LCM_NAME;
        $scope.BranchIncentiveValidate.BCL_INSPECTED_BY = items.BCL_INSPECTED_BY;
        $scope.BranchIncentiveValidate.BCL_SELECTED_DT = items.BCL_SELECTED_DT;
        $scope.BranchIncentiveValidate.BCL_ID = items.BCL_ID;
        $scope.GetLocationCode(items);
        /*SCPenalty = items.SCORE*/
        SCPenalty = items.BCL_PPM_ASSET_ID;
    }
    $scope.loadModal = function () {
        debugger;
        // Function to show the modal on page load
        $('#myModal').modal('show');
        $scope.GetDrafts(2);
    };
    $scope.GetLocationCode = function (data) {
        ValidateScoreCard.GetGriddata(data.BCL_ID).then(function (response) {
            debugger;
            if (response.data != null) {
                $scope.FinalTotalWeightage = 0;
                $scope.FinalTotalWeightagePercent = 0;
                if (response.data != null) {
                    $scope.TableData = response.data;
                    _.forEach($scope.TableData, function (value) {
                        var OverAllScore = 0;
                        var BCL_SUB_WEIGHTAGE = 0;
                        _.forEach(value.ChecklistScore1, function (value1) {
                            BCL_SUB_WEIGHTAGE = BCL_SUB_WEIGHTAGE + parseInt(value1.BCL_SUB_WEIGHTAGE, 10);
                            var ScorePercenatge = _.find($scope.Score_Per, { BCL_CH_NAME: value1.RESPONSE })
                            value1.BCL_WEIGHTAGE_PERCENTAGE = parseInt(value1.BCL_SUB_WEIGHTAGE, 10) * (parseInt(ScorePercenatge != undefined ? ScorePercenatge.BCL_CH_PERCENTAGE : 0, 10) / 100);
                            OverAllScore = OverAllScore + value1.BCL_WEIGHTAGE_PERCENTAGE
                        });
                        value.TotalWeightage = BCL_SUB_WEIGHTAGE;
                        value.TotalWeightagePercent = OverAllScore;
                        $scope.FinalTotalWeightage = $scope.FinalTotalWeightage + value.TotalWeightage;
                        $scope.FinalTotalWeightagePercent = $scope.FinalTotalWeightagePercent + value.TotalWeightagePercent;
                    });
                }
                angular.forEach($scope.Inspection, function (value, key) {
                    value.ticked = true;
                });
                angular.forEach($scope.Location, function (value, key) {
                    value.ticked = true;
                });
                angular.forEach($scope.Location, function (value, key) {
                    if (data.LCM_CODE == value.LCM_CODE) {
                        value.ticked = true;
                    }

                });
                angular.forEach($scope.Inspection, function (value, key) {
                    if (data.BCL_INSPECTED_BY == value.INSPECTOR) {
                        value.ticked = true;
                    }

                });
                $scope.BranchIncentiveValidate.SVR_FROM_DATE = data.BCL_SELECTED_DT;
                $scope.BranchIncentiveValidate.OVERALL_CMTS = data.BCL_OVERALL_CMTS;
                $scope.BranchIncentiveValidate.BCL_VALIDATE_OVERALL_CMTS = data.BCL_VALIDATE_OVERALL_CMTS;
                $scope.TableData = response.data;
                $('#myModal').modal('toggle');
            }
        });
    }
    $scope.Clear = function () {
        $scope.CreateScoreCard = {};
        $scope.TableData = [];
        $scope.rowData = [];
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        $scope.BranchIncentiveValidate.LCM_NAME = '';
        $scope.BranchIncentiveValidate.BCL_INSPECTED_BY = '';
        $scope.BranchIncentiveValidate.BCL_SELECTED_DT = '';
        $scope.BranchIncentiveValidate.OVERALL_CMTS = '';
        $scope.BranchIncentiveValidate.BCL_VALIDATE_OVERALL_CMTS = '';
        $scope.FinalTotalWeightage = '';
        $scope.FinalTotalWeightagePercent = '';
        $scope.dvbutton = true;
        $scope.ChecklistID = '';
        $scope.LoadData();
        $scope.loadModal();
    }

    // flag id 3 means Validate Submitted, 5 means Validate Rejected
    $scope.Submit = function (flagid) {
        debugger;
        $scope.RoleChecking = $scope.BranchIncentiveValidate.Inspection[0].Roleid.split(',');
        if ($scope.RoleChecking.includes('45'))
            flagid = 7;
        if ($scope.RoleChecking.includes('42'))
            flagid = 8;
        var sub = $scope.name;
        $scope.SelectedDatas = [];
        angular.forEach($scope.TableData, function (Value1, Key) {
            angular.forEach(Value1.ChecklistScore1, function (Value, Key) {
                var BCL_CH_CODE = _.find(Value.checklistDetails, { BCL_CH_NAME: Value.RESPONSE })
                $scope.SelectedData = {};
                $scope.SelectedData.TypeCode = Value1.BCL_TP_ID;
                $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
                $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
                $scope.SelectedData.ScoreCode = BCL_CH_CODE.BCL_CH_CODE;
                $scope.SelectedData.ScoreName = BCL_CH_CODE.BCL_CH_NAME;
                $scope.SelectedData.ScorePercent = Value.BCL_WEIGHTAGE_PERCENTAGE;
                $scope.SelectedDatas.push($scope.SelectedData);
            });
        });

        if ($scope.SelectedDatas.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Select the at least one Scorecard ');
            return;
        }
        if ($scope.BranchIncentiveValidate.Inspection[0].length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select the scorecard Validated By');
            return;
        }
        var TOTSCPenalty = SCPenalty - 15;
        if (TOTSCPenalty > $scope.FinalTotalWeightagePercent) {
            showNotification('error', 8, 'bottom-right', 'penalty percentage cannot be reduced more than 15% ');
            return;
        }
        if ($scope.FinalTotalWeightagePercent > SCPenalty) {
            showNotification('error', 8, 'bottom-right', 'Do not have the right to increase the score beyond the submitted score');
            return;
        }
        $scope.data = {
            InspectdBy: $scope.BranchIncentiveValidate.Inspection[0].INSPECTOR,
            date: $scope.BranchIncentiveValidate.SVR_FROM_DATE,
            Seldata: $scope.SelectedDatas,
            Flag: flagid,
            OVERALL_CMTS: $('#BCL_VALIDATE_OVERALL_CMTS').val(),
            BCL_ID: $scope.BranchIncentiveValidate.BCL_ID,
            F_Percentage: $scope.FinalTotalWeightagePercent
        };
        ValidateScoreCard.SaveValidationData($scope.data).then(function (response) {
            debugger;
            if (response.data != null) {
                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', response.data);
                }, 100);
                $scope.loadModal();
                $scope.Clear();
            } else {
                setTimeout(function () {
                    showNotification('error', 8, 'bottom-right', 'Something Went Wrong');
                }, 500);
            }
        });
    }
    $scope.Submitt = function (flagid) {
        debugger;
        var sub = $scope.name;
        $scope.SelectedDatas = [];
        angular.forEach($scope.TableData, function (Value1, Key) {
            angular.forEach(Value1.ChecklistScore1, function (Value, Key) {
                var BCL_CH_CODE = _.find(Value.checklistDetails, { BCL_CH_NAME: Value.RESPONSE })
                $scope.SelectedData = {};
                $scope.SelectedData.TypeCode = Value1.BCL_TP_ID;
                $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
                $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
                $scope.SelectedData.ScoreCode = BCL_CH_CODE.BCL_CH_CODE;
                $scope.SelectedData.ScoreName = BCL_CH_CODE.BCL_CH_NAME;
                $scope.SelectedData.ScorePercent = Value.BCL_WEIGHTAGE_PERCENTAGE;
                $scope.SelectedDatas.push($scope.SelectedData);
            });
        });

        if ($scope.SelectedDatas.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Select the at least one Scorecard ');
            return;
        }
        if ($scope.BranchIncentiveValidate.Inspection[0].length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select the scorecard Validated By');
            return;
        }
        
        $scope.data = {
            InspectdBy: $scope.BranchIncentiveValidate.Inspection[0].INSPECTOR,
            date: $scope.BranchIncentiveValidate.SVR_FROM_DATE,
            Seldata: $scope.SelectedDatas,
            Flag: flagid,
            OVERALL_CMTS: $('#BCL_VALIDATE_OVERALL_CMTS').val(),
            BCL_ID: $scope.BranchIncentiveValidate.BCL_ID,
            F_Percentage: $scope.FinalTotalWeightagePercent
        };
        ValidateScoreCard.SaveValidationData($scope.data).then(function (response) {
            debugger;
            if (response.data != null) {
                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', response.data);
                }, 100);
                $scope.loadModal();
                $scope.Clear();
            } else {
                setTimeout(function () {
                    showNotification('error', 8, 'bottom-right', 'Something Went Wrong');
                }, 500);
            }
        });
    }
})