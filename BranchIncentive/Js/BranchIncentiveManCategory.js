﻿app.service("BranchIncentiveManCategoryService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveManCategory/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGridData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveManCategory/GetGridData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.updateData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveManCategory/updateData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveManCategory/SaveData', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BranchIncentiveManCategoryController', ['$scope', '$q', 'UtilityService', 'BranchIncentiveManCategoryService', '$filter', '$timeout', "$rootScope",
    function ($scope, $q, UtilityService, BranchIncentiveManCategoryService, $filter, $timeout, $rootScope) {

        $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
        $scope.LoadInfo = [];
        $scope.BranchIncentiveManCategory = {};
        $scope.search = [];
        $scope.MainType = [];
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.Edit = false;
        function validatinFn() {
            if ($scope.BranchIncentiveManCategory.MainType[0] == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Main Type");
                return false;
            }
            if ($scope.BranchIncentiveManCategory.BCL_MC_NAME == undefined || $scope.BranchIncentiveManCategory.BCL_MC_NAME == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Main Category Name");
                return false;
            }
            if ($scope.BranchIncentiveManCategory.BCL_MC_REM == undefined || $scope.BranchIncentiveManCategory.BCL_MC_REM == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Main Category Remarks");
                return false;
            }
            return true;
        }
        $scope.SaveA = function () {
            debugger;
            if (validatinFn()) {
                var obj = {
                    BCL_MC_ID: 0,
                    BCL_MC_NAME: $scope.BranchIncentiveManCategory.BCL_MC_NAME,
                    BCL_TP_MC_ID: $scope.BranchIncentiveManCategory.MainType[0].BCL_TP_ID,
                    BCL_MC_REM: $scope.BranchIncentiveManCategory.BCL_MC_REM,
                }
                BranchIncentiveManCategoryService.SaveData(obj).then(function (response) {
                    if (response != null) {
                        if (response.data == 0) {
                            showNotification('success', 8, 'bottom-right', 'Data Saved Successfully');
                            setTimeout(function () { $scope.GetMainType(); }, 500);
                            $scope.ClearData();
                        }
                        else
                            showNotification('error', 8, 'bottom-right', "Main Category Already Exists");
                    }
                    else
                        showNotification('error', 8, 'bottom-right', "Some Error Occured");
                }, function (response) {
                    progress(0, '', false);

                });
            }
        }

        $scope.Update = function () {
            var obj = {
                BCL_MC_ID: $scope.BranchIncentiveManCategory.BCL_MC_ID,
                BCL_MC_NAME: $scope.BranchIncentiveManCategory.BCL_MC_NAME,
                BCL_TP_MC_ID: $scope.BranchIncentiveManCategory.MainType[0].BCL_TP_ID,
                BCL_MC_REM: $scope.BranchIncentiveManCategory.BCL_MC_REM,
            }

            BranchIncentiveManCategoryService.updateData(obj).then(function (response) {
                if (response != null) {
                    if (response == 0) {
                        showNotification('success', 8, 'bottom-right', 'Data update Successfully');
                        setTimeout(function () { $scope.GetMainType(); }, 500);
                    }
                    else
                        showNotification('error', 8, 'bottom-right', "Main Category Already Exists");
                }
                else
                    showNotification('error', 8, 'bottom-right', "Some Error Occured");
            }, function (response) {
                progress(0, '', false);
            });
        }

        $scope.ShowStatus = function (value) {
            return $scope.StaDet[value == 0 ? 1 : 0].Name;
        }

        $scope.locSelectAll = function () {
            $scope.BranchIncentiveManCategory.MainType = $scope.MainType;
            $scope.MainType();
        }

        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };

        setTimeout(function () { $scope.LoadData(); }, 1000);
        $scope.EditFunction = function (data) {
            $scope.BranchIncentiveManCategory = {};
            $scope.BranchIncentiveManCategory.BCL_MC_ID = data.BCL_MC_ID;
            $scope.MainType = [];
            BranchIncentiveManCategoryService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                angular.forEach($scope.MainType, function (value, key) {
                    value.ticked = false;
                    if (value.BCL_TP_ID.toString() == data.BCL_TP_ID) {
                        value.ticked = true;
                    }
                });
            })
            $scope.EditCategory = data;
            angular.copy(data, $scope.BranchIncentiveManCategory);
            $scope.ActionStatus = 1;
            $scope.IsInEdit = true;
            $scope.Edit = true;

        }
        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };
        var columnDefs = [
            { headerName: "Edit", width: 40, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Check List Category Code", field: "BCL_MC_CODE", cellClass: "grid-align", width: 150 },
            { headerName: "Check List Category Name", field: "BCL_MC_NAME", cellClass: "grid-align", width: 250 },
            { headerName: "Check List Type", field: "BCL_TP_NAME", cellClass: "grid-align", width: 200 },
            { headerName: "Remarks", field: "BCL_MC_REM", cellClass: "grid-align", width: 350 },
        ];
        $scope.gridOptions = {
            columnDefs: '',
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            },
        };
        $scope.SearchA = function () {
            var obj2 = {
                BCL_TP_MC_ID: _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(','),
            }
            BranchIncentiveManCategoryService.GetGridData(obj2).then(function (response) {
                if (response != null) {
                    $scope.gridata = response.data;
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                }
                progress(0, 'Loading...', false);
            })
        }
        $scope.GetMainType = function () {
            $scope.MainType = [];
            BranchIncentiveManCategoryService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                //angular.forEach($scope.MainType, function (value, key) {
                //    var a = _.find($scope.MainType);
                //    a.ticked = true;
                //});
                setTimeout(function () { $scope.SearchA(); }, 500);
            })
        }
        $scope.LoadData = function () {
            progress(0, 'Loading...', true);
            $scope.BranchIncentiveManCategory = {};
            $scope.GetMainType();
            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
            progress(0, '', false);
        }
        
        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);

            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "IncentiveSubCategoryMaster.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel();

        }
       
        $scope.ClearData = function () {
            $scope.BranchIncentiveManCategory = {};
            $scope.$broadcast('angucomplete-alt:clearInput');
        }
    }]);