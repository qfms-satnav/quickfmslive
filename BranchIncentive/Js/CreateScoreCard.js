﻿
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
app.service("CreateScoreCardService", function ($http, $q, UtilityService) {
    this.GetGriddata = function (BCL_ID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/GetGriddata?BCL_ID=' + BCL_ID)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetInspectorsIn = function (ID) {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CreateScoreCard/GetInspectorsIn?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.GetScorePer = function () {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CreateScoreCard/GetScorePer')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    this.getSavedList = function (BCLStatus) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/getSavedList?BCLStatus=' + BCLStatus + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.InsertCheckList = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/InsertCheckListV2', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.DeleteCheckList = function (BCLID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/DeleteCheckList?BCLID=' + BCLID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetLocationsScore = function (ID) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CreateScoreCard/GetLocationsScore?id=' + ID + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


});


app.controller('CreateScoreCardController', function ($scope, $q, $location, CreateScoreCardService, UtilityService, $filter, $http, $ngConfirm) {

    $scope.CreateScoreCard = {};
    $scope.TableData = [];
    $scope.rowData = [];
    $scope.dvbutton = true;
    $scope.ChecklistID = '';
    $scope.Score_Per = [];


    //$scope.columnDefs = [
    //    /*{ headerName: "Sub Category", field: "BCL_MC_NAME", cellClass: "grid-align", width: 250 },*/
    //    { headerName: "Selected File", field: "Name", cellClass: "grid-align", width: 250 },
    //    {
    //        headerName: "Download", cellClass: 'grid-align', width: 250, cellRenderer: function (params) {

    //            return "<a download='" + params.data.Name + "' href='../../UploadFiles/" + companyid + "/" + params.data.Name + "'><span class='glyphicon glyphicon-download'></span></a>";
    //        }
    //    },
    //    { headerName: "Remove", template: '<a ng-click="Remove(data)"> <span class="glyphicon glyphicon-remove-circle"></span></a>', cellClass: 'grid-align', width: 250 }
    //];

    //$scope.gridOptions = {
    //    columnDefs: $scope.columnDefs,
    //    rowData: $scope.rowData,
    //    angularCompileRows: true,
    //    onReady: function () {
    //        $scope.gridOptions.api.sizeColumnsToFit()
    //    },
    //};

    $scope.Getstring = function (data) {
        return (data == '2') ? 'Submitted' : 'Validated';
    }

    $scope.LoadData = function () {
        debugger;
        CreateScoreCardService.GetGriddata(0).then(function (response) {
            $scope.FinalTotalWeightage = 0;
            $scope.FinalTotalWeightagePercent = 0;
            if (response.data != null) {
                $scope.TableData = response.data;
                _.forEach($scope.TableData, function (value) {
                    var OverAllScore = 0;
                    var BCL_SUB_WEIGHTAGE = 0;
                    _.forEach(value.ChecklistScore1, function (value1) {
                        BCL_SUB_WEIGHTAGE = BCL_SUB_WEIGHTAGE + parseInt(value1.BCL_SUB_WEIGHTAGE, 10);
                        var ScorePercenatge = _.find($scope.Score_Per, { BCL_CH_NAME: value1.RESPONSE })
                        value1.BCL_WEIGHTAGE_PERCENTAGE = parseInt(value1.BCL_SUB_WEIGHTAGE, 10) * (parseInt(ScorePercenatge != undefined ? ScorePercenatge.BCL_CH_PERCENTAGE : 0, 10) / 100);
                        OverAllScore = OverAllScore + value1.BCL_WEIGHTAGE_PERCENTAGE
                    });
                    value.TotalWeightage = BCL_SUB_WEIGHTAGE;
                    value.TotalWeightagePercent = OverAllScore;
                    $scope.FinalTotalWeightage = $scope.FinalTotalWeightage + value.TotalWeightage;
                    $scope.FinalTotalWeightagePercent = $scope.FinalTotalWeightagePercent + value.TotalWeightagePercent;
                });
            }
        });
    }


    $scope.OverallScore = function () {
        $scope.FinalTotalWeightagePercent = 0;
        _.forEach($scope.TableData, function (value) {
            var OverAllScore = 0;
            _.forEach(value.ChecklistScore1, function (value1) {

                var ScorePercenatge = _.find($scope.Score_Per, { BCL_CH_NAME: value1.RESPONSE })

                value1.BCL_WEIGHTAGE_PERCENTAGE = parseInt(value1.BCL_SUB_WEIGHTAGE, 10) * (parseInt(ScorePercenatge != undefined ? ScorePercenatge.BCL_CH_PERCENTAGE : 0, 10) / 100);
                OverAllScore = OverAllScore + value1.BCL_WEIGHTAGE_PERCENTAGE
            });
            value.TotalWeightagePercent = OverAllScore;
            $scope.FinalTotalWeightagePercent = $scope.FinalTotalWeightagePercent + OverAllScore
        });
    }


    $scope.GetDrafts = function (BCLStatus) {

        if (BCLStatus == 1) {
            $('#hsavedtitle').html("Saved Drafts");
        }
        else {
            $('#hsavedtitle').html("Saved Scorecard");

        }
        //if ($scope.CreateScoreCard.MainType.length == 0) {
        //    showNotification('error', 8, 'bottom-right', "Please Select Main Type");
        //    return;
        //}
        //var BCL_TP_ID = _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(',');
        CreateScoreCardService.getSavedList(BCLStatus).then(function (response) {
            if (response.data != null) {
                $scope.CreateScoreCard.SaveList = response.data;

            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }



    //UtilityService.getCities(2).then(function (response) {
    //    if (response.data != null) {
    //        $scope.City = response.data;
    //        $scope.City[0].ticked = true;
    //angular.forEach($scope.City, function (value, key) {
    //    value.ticked = true;
    //});
    CreateScoreCardService.GetLocationsScore(2).then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
            CreateScoreCardService.GetInspectorsIn(2).then(function (response) {
                if (response.data != null) {
                    $scope.Inspection = response.data;
                    $scope.Inspection[0].ticked = true;
                    CreateScoreCardService.GetScorePer().then(function (response) {
                        if (response.data != null) {
                            $scope.Score_Per = response.data;
                            $scope.LoadData();
                        };
                    });
                }
            });
        }

    });



    $scope.CreateScoreCard.SVR_FROM_DATE = moment().format('MM/DD/YYYY');
    //$scope.DateClick = function (id) {
    //    $('#' + id+'_1').datepicker({
    //        format: 'mm/dd/yyyy',
    //        autoclose: true,
    //        todayHighlight: true,
    //       // endDate: 'today',
    //        //startDate: 'today'
    //    });
    //}

    $scope.DateClick = function (id) {
        $('#' + id + '_1').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
            // endDate: 'today',
            //startDate: 'today'
        });
    }


    $scope.SelectFile = function (e) {
        var UplFile = $('#' + e.target.id)[0];
        var currentDateTime = moment().format('YYYYMMDDHHmmss_');
        var newFileName = currentDateTime + UplFile.files[0].name

        if ($scope.gridOptions.rowData.length > 0)
            _.remove($scope.rowData, { SubCatCode: e.target.name });
        $scope.rowData.push({ SubCatCode: e.target.name, Name: newFileName, file: e.target.files[0] });
        $scope.gridOptions.api.setRowData($scope.rowData);

        var formData = new FormData();

        formData.append("UplFile", UplFile.files[0], newFileName);
        $http.post(UtilityService.path + '/api/CreateScoreCard/UploadFiles', formData, {
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined }
        }).then(function (data, status) {
            showNotification('success', 8, 'bottom-right', data.data);
        }), function (error, data, status) {

        };
    };

    //$scope.Remove = function (data) {
    //    if ($scope.gridOptions.rowData.length > 0)
    //        _.remove($scope.rowData, { SubCatCode: data.SubCatCode });
    //    $scope.gridOptions.api.setRowData($scope.rowData);
    //}

    //$scope.fileNameChanged = function (fu, typ, BCL_SUB_NAME) {
    //    if (rowData.length === 0) {
    //        $scope.rowData = [];
    //    }
    //    data = new FormData($('form')[0]);
    //    for (i = 0; i < fu.files.length; i++) {
    //        var gridLength = $scope.gridOptions.rowData.length;
    //        if (gridLength > 0) {
    //            for (j = 1; j <= gridLength; j++) {
    //                rowData.splice(0, 1);
    //                $scope.gridOptions.api.setRowData(rowData);
    //            }
    //        }
    //        rowData.push({ SubCatName: BCL_SUB_NAME, Name: fu.files[i].name, file: fu.files[i] });

    //        $scope.gridOptions.api.setRowData($scope.rowData);
    //    }

    //};

    //$scope.Save = function () {
    //    console.log($scope.TableData);
    //}

    $scope.FindFileName = function (data, param) {
        var name = '';
        angular.forEach(data, function (Value, Key) {
            if (param == Value.SubCatCode) {
                name = Value.Name;
            }
        })
        return name;

    }

    $scope.CommaSperateData = function (typ, data, listdata) {
        if (typ == 'Multiple' || typ == 'Toggle') {
            var newarray = [];
            angular.forEach(listdata, function (Value, Key) {
                if (Value.RESPONSE) {
                    newarray.push(Value.BCL_CH_NAME);
                }
            })
            return newarray.join(",");
        } else {
            return data;
        }
    }

    $scope.Submit = function () {
        debugger;
        $scope.SelectedDatas = [];
        angular.forEach($scope.TableData, function (Value1, Key) {
            angular.forEach(Value1.ChecklistScore1, function (Value, Key) {
                // var BCL_CH_CODE = _.find(Value.checklistDetails, { BCL_CH_NAME: Value.RESPONSE })
                var BCL_CH_CODE = _.find(Value.checklistDetails, { BCL_CH_NAME: Value.RESPONSE }) || {}
                $scope.SelectedData = {};
                $scope.SelectedData.TypeCode = Value1.BCL_TP_ID;
                $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
                $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
                $scope.SelectedData.ScoreCode = BCL_CH_CODE.BCL_CH_CODE || '';
                $scope.SelectedData.ScoreName = BCL_CH_CODE.BCL_CH_NAME || '';
                $scope.SelectedData.txtdata = Value.TEXT;
                $scope.SelectedData.Date = Value.DATERESPONSE;
                $scope.SelectedData.SubScore = '';
                $scope.SelectedData.ScorePercent = Value.BCL_WEIGHTAGE_PERCENTAGE;
                //$scope.SelectedData.FilePath = $scope.FindFileName($scope.gridOptions.rowData, Value.BCL_SUB_CODE);
                $scope.SelectedDatas.push($scope.SelectedData);
            });
        });

        var data = true;
        data = _.every($scope.SelectedDatas, function (o) { return o.ScoreCode.length > 0; });
        if (!data) {
            showNotification('error', 8, 'bottom-right', 'Please fill all the fields to submit the Checklist');
            return;
        }

        $scope.data = {
            LCMLST: $scope.CreateScoreCard.Location,
            LcmList: $scope.CreateScoreCard.Location[0].LCM_CODE,
            InspectdBy: $scope.CreateScoreCard.Inspection[0].INSPECTOR,
            date: $scope.CreateScoreCard.SVR_FROM_DATE,
            SelCompany: '1',
            Seldata: $scope.SelectedDatas,
            Flag: 2,
            OVERALL_CMTS: $('#OVERALL_CMTS').val(),
            //imagesList: $scope.gridOptions.rowData,
            //BCL_TYPE_ID: $scope.CreateScoreCard.MainType[0].BCL_TP_ID,
            BCL_ID: $scope.ChecklistID,
            F_Percentage: $scope.FinalTotalWeightagePercent
        };

        CreateScoreCardService.InsertCheckList($scope.data).then(function (response) {
            if (response != null) {
                $scope.Clear();
                setTimeout(function () {
                    if (response.Message == "5") {
                        $ngConfirm({
                            icon: 'fa fa-warning',
                            title: 'You have already completed your inspection for the day',
                            content: '',
                            closeIcon: true,
                            closeIconClass: 'fa fa-close',
                            buttons: {
                                close: function ($scope, button) {
                                }
                            }
                        });
                    }
                    else {
                        showNotification('success', 8, 'bottom-right', response.data);
                    }
                }, 500);

            }
        })
    }

    $scope.Save = function () {
        if ($scope.CreateScoreCard.Location.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select Location');
            return;
        }
        if ($scope.CreateScoreCard.City.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select City');
            return;
        }
        if ($scope.CreateScoreCard.Inspection.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select Inspection By');
            return;
        }
        if ($scope.CreateScoreCard.SVR_FROM_DATE == undefined || $scope.ScoreCard.SVR_FROM_DATE == '') {
            showNotification('error', 8, 'bottom-right', 'Please select Date');
            return;
        }
        $scope.SelectedDatas = [];
        angular.forEach($scope.TableData, function (Value, Key) {
            $scope.SelectedData = {};
            $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
            $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
            $scope.SelectedData.ScoreCode = $scope.CommaSperateData(Value.BCL_SUB_TYPE, Value.RESPONSE, Value.checklistDetails);
            $scope.SelectedData.ScoreName = '';
            $scope.SelectedData.txtdata = Value.TEXT;
            $scope.SelectedData.Date = Value.DATERESPONSE;
            $scope.SelectedData.SubScore = '';
            //$scope.SelectedData.FilePath = $scope.FindFileName($scope.gridOptions.rowData, Value.BCL_SUB_CODE);
            $scope.SelectedDatas.push($scope.SelectedData);

        });
        var data = [];
        data = _.filter($scope.SelectedDatas, function (o) { return o.ScoreCode.length > 0; });
        if (data.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Select at least one value');
            return;
        }
        $scope.data = {
            LCMLST: $scope.CreateScoreCard.Location,
            LcmList: $scope.CreateScoreCard.Location[0].LCM_CODE,
            InspectdBy: $scope.CreateScoreCard.Inspection[0].INSPECTOR,
            date: $scope.CreateScoreCard.SVR_FROM_DATE,
            SelCompany: '1',
            Seldata: $scope.SelectedDatas,
            Flag: 1,
            OVERALL_CMTS: $('#OVERALL_CMTS').val(),
            //imagesList: $scope.gridOptions.rowData,
            BCL_TYPE_ID: $scope.CreateScoreCard.MainType[0].BCL_TP_ID,
            BCL_ID: $scope.ChecklistID,
        };
        CreateScoreCardService.InsertCheckList($scope.data).then(function (response) {
            if (response != null) {
                $scope.Clear();
                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', response.data);
                }, 500);

            }
        })


    }

    $scope.DeleteCheckList = function (BCLID, BCLStatus) {
        CreateScoreCardService.DeleteCheckList(BCLID).then(function (response) {
            if (response != undefined) {
                $scope.GetDrafts(BCLStatus);
                showNotification('success', 8, 'bottom-right', "Deleted Successfully");
            } else {
                console.log('Submit Error');
            }
        });

    }

    $scope.Clear = function () {
        $scope.CreateScoreCard = {};
        $scope.TableData = [];
        $scope.rowData = [];
        //$scope.gridOptions.api.setRowData($scope.rowData);
        angular.forEach($scope.Inspection, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        //angular.forEach($scope.MainType, function (value, key) {
        //    value.ticked = false;
        //});
        $scope.dvbutton = true;
        $scope.ChecklistID = '';
        $scope.LoadData();
    }


    $scope.GetLocationCode = function (data) {
        CreateScoreCardService.GetGriddata(data.BCL_ID).then(function (response) {
            if (response.data != null) {
                $scope.FinalTotalWeightage = 0;
                $scope.FinalTotalWeightagePercent = 0;
                if (response.data != null) {
                    $scope.TableData = response.data;
                    _.forEach($scope.TableData, function (value) {
                        var OverAllScore = 0;
                        var BCL_SUB_WEIGHTAGE = 0;
                        _.forEach(value.ChecklistScore1, function (value1) {
                            BCL_SUB_WEIGHTAGE = BCL_SUB_WEIGHTAGE + parseInt(value1.BCL_SUB_WEIGHTAGE, 10);
                            var ScorePercenatge = _.find($scope.Score_Per, { BCL_CH_NAME: value1.RESPONSE })
                            value1.BCL_WEIGHTAGE_PERCENTAGE = parseInt(value1.BCL_SUB_WEIGHTAGE, 10) * (parseInt(ScorePercenatge != undefined ? ScorePercenatge.BCL_CH_PERCENTAGE : 0, 10) / 100);
                            OverAllScore = OverAllScore + value1.BCL_WEIGHTAGE_PERCENTAGE
                        });
                        value.TotalWeightage = BCL_SUB_WEIGHTAGE;
                        value.TotalWeightagePercent = OverAllScore;
                        $scope.FinalTotalWeightage = $scope.FinalTotalWeightage + value.TotalWeightage;
                        $scope.FinalTotalWeightagePercent = $scope.FinalTotalWeightagePercent + value.TotalWeightagePercent;
                    });
                }
                if (data.BCL_SUBMIT == 2) {
                    $scope.dvbutton = false;
                }
                else {
                    $scope.dvbutton = true;
                    $scope.ChecklistID = data.BCL_ID;
                }
                angular.forEach($scope.Inspection, function (value, key) {
                    value.ticked = false;
                });
                angular.forEach($scope.Location, function (value, key) {
                    value.ticked = false;
                });
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = false;
                });
                angular.forEach($scope.Location, function (value, key) {
                    if (data.LCM_CODE == value.LCM_CODE) {
                        value.ticked = true;
                    }

                });
                angular.forEach($scope.City, function (value, key) {
                    if (data.CTY_CODE == value.CTY_CODE) {
                        value.ticked = true;
                    }

                });
                angular.forEach($scope.Inspection, function (value, key) {
                    if (data.BCL_INSPECTED_BY == value.INSPECTOR) {
                        value.ticked = true;
                    }

                });
                $scope.CreateScoreCard.SVR_FROM_DATE = data.BCL_SELECTED_DT;

                $scope.CreateScoreCard.OVERALL_CMTS = data.BCL_OVERALL_CMTS;
                $scope.TableData = response.data;
                //var selectfiles = [];
                //$scope.rowData = [];
                //selectfiles = _.filter(response.data, function (o) { return o.FILEPATH.length > 0; });
                //angular.forEach(selectfiles, function (value, key) {
                //    $scope.rowData.push({ Name: value.FILEPATH, SubCatCode: value.BCL_SUB_CODE });
                //})
                //$scope.gridOptions.api.setRowData($scope.rowData);
                $('#myModal').modal('toggle');
            }
        });
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.ScoreCard.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

    }
})
