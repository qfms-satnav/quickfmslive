﻿app.service("BranchIncentiveSubCategoryService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveSubCategory/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetMainCategory = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveSubCategory/GetMainCategory', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGridData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveSubCategory/GetGridData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetScoreData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveSubCategory/GetScoreData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.updateData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveSubCategory/updateData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveSubCategory/SaveData', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.DeleteScore = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveSubCategory/DeleteScore', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BranchIncentiveSubCategoryController', ['$scope', '$q', 'UtilityService', 'BranchIncentiveSubCategoryService', '$filter', '$timeout', "$rootScope",
    function ($scope, $q, UtilityService, BranchIncentiveSubCategoryService, $filter, $timeout, $rootScope) {
        $scope.StaSubType = [{ Id: 'Radio', Name: 'Radio' }, { Id: 'Multiple', Name: 'Multiple' }, { Id: 'Toggle', Name: 'Toggle' }];
        $scope.StaflagSubType4 = [{ Id: '1', Name: '1' }, { Id: '2', Name: '2' }, { Id: '3', Name: '3' }, { Id: '4', Name: '4' }, { Id: '5', Name: '5' }];
        $scope.LoadInfo = [];
        $scope.BranchIncentiveSubCategory = {};
        $scope.search = [];
        $scope.MainType = [];
        $scope.scoreArr = [];
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.Edit = false;

        $scope.fn_check = function () {
            if ($scope.BranchIncentiveSubCategory.BCL_SUB_TYPE != undefined) {
                $('#disp').show();

                if ($scope.BranchIncentiveSubCategory.BCL_SUB_TYPE == 'Toggle') {
                    $scope.BranchIncentiveSubCategory.BCL_SUB_TYPE_NO = "2";
                    $('#Select5').attr('disabled', true);
                }
                else {
                    $scope.BranchIncentiveSubCategory.BCL_SUB_TYPE_NO = "1";
                    $('#Select5').attr('disabled', false);
                }
                $scope.fn_Set();
            }
        }
        $scope.fn_Set = function () {
            if ($scope.BranchIncentiveSubCategory.BCL_SUB_TYPE_NO != undefined) {
                $("#dvScore").html('');
                var hTable = "<table id='tblScore' class='table table-condensed table - bordered table - hover table - striped'>";
                hTable += "<tr><th>SR. NO.</th><th style='text-align:center'>SCORE NAME</th><th style='text-align:center'>ADD</th></tr>";
                hTable += "<tbody>";
                var srno = 0;
                for (var i = 0; i < $scope.BranchIncentiveSubCategory.BCL_SUB_TYPE_NO; i++) {
                    hTable += "<tr id='tdrow" + i + "'>";
                    hTable += "<td>" + (srno + 1) + "</td>";
                    hTable += "<td align='center'><input type='hidden' id ='hdid_" + i.toString() + "' name='hdid_" + i.toString() + "' value='0' />";
                    hTable += "<div onmouseover='Tip(\"Enter Remarks upto 500 characters\")' onmouseout='UnTip()'>";
                    hTable += "<input id ='BCL_CH_NAME_" + i.toString() + "' class='form-control' type='text' name='BCL_CH_NAME_" + i.toString() + "' maxlength='50' />";
                    hTable += "</div></td>";
                    hTable += "<td align='center'>";
                    hTable += "<a title='Add' id='Add" + i + "' name='Add" + i + "' onclick='angular.element(this).scope().fn_change(\"" + i + "\")' ><i class='fa fa-plus' aria-hidden='true' style='margin-left: 15px;height: 22px;cursor:pointer;'></i></a>";
                    hTable += "</td></tr>";
                    srno++;
                }
                hTable += "</tbody></table>";
                $("#dvScore").html(hTable);
                $("#dvScore").show();
            }
        }
        const cellClassRules = {
            "cell-pass": params => params.value == 'ACTIVE',
            "cell-fail": params => params.value == 'INACTIVE'
        };
        var columnDefs2 = [
            { headerName: "SCORE CODE", field: "BCL_CH_CODE", width: 250, cellClass: 'grid-align' },
            { headerName: "SCORE NAME", field: "BCL_CH_NAME", width: 380, cellClass: 'grid-align', template: "<input type='text' ng-model='data.BCL_CH_NAME' />" },
            { headerName: "SCORE STATUS", field: "ScoreStatus", width: 250, cellClass: 'grid-align', cellClassRules: cellClassRules },
            { headerName: "Change Status", width: 100, template: '<a ng-click = "fn_Delete(data.BCL_CH_ID,data.BCL_SUB_ID, data.BCL_MC_CODE,data.BCL_CH_STA_ID)"> <i class="fa fa-toggle-on" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },


        ];

        $scope.gridOptions2 = {
            columnDefs: columnDefs2,
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            onReady: function () {
                $scope.gridOptions2.api.sizeColumnsToFit();
            }
        };
        $scope.fn_SetEdit = function (BCL_SUB_ID, BCL_MC_CODE) {
            progress(0, 'Loading...', true);
            var obj2 = {
                BCL_SUB_ID: BCL_SUB_ID,
                BCL_MC_CODE: BCL_MC_CODE
            }
            BranchIncentiveSubCategoryService.GetScoreData(obj2).then(function (response) {
                if (response != null) {
                    if (response == undefined) {
                        progress(0, 'Loading...', false);
                        $scope.gridOptions2.api.setRowData([]);
                    }
                    else {
                        var dtaa = angular.copy(response.data);
                        $scope.gridOptions2.api.setRowData(dtaa);
                        progress(0, 'Loading...', false);
                    }
                    $("#dvScore").hide();
                    $("#dvScoreEdit").show();
                }
                progress(0, 'Loading...', false);
            });

            //}
        }

        $scope.fn_Delete = function (BCL_CH_ID, BCL_SUB_ID, BCL_MC_CODE, BCL_CH_STA_ID) {
            var vals = {
                BCL_CH_ID: BCL_CH_ID,
                BCL_CH_STA_ID: BCL_CH_STA_ID == 0 ? 1 : 0
            }
            var msg = BCL_CH_STA_ID == 0 ? "Activated" : "Deactivated";
            var params = JSON.stringify(vals);
            BranchIncentiveSubCategoryService.DeleteScore(params).then(function (response) {
                if (response != undefined) {
                    //error   
                    showNotification('success', 8, 'bottom-right', msg + " Successfully");
                    setTimeout(function () { $scope.fn_SetEdit(BCL_SUB_ID, BCL_MC_CODE); }, 500);
                } else {
                    console.log('Submit Error');
                }
            });
        }
        $scope.fn_change = function (ind) {
            if ($("#BCL_CH_NAME_" + ind).val().length > 0) {
                var c = 0;
                angular.forEach($scope.gridOptions2.rowData, function (value, key) {
                    if (value.BCL_CH_NAME == $("#BCL_CH_NAME_" + ind).val()) {
                        showNotification('error', 8, 'bottom-right', "Score Already Exists,Please Enter Another");
                        $("#BCL_CH_NAME_" + ind).val('');
                        c = 1;
                        return false;
                    }
                });
                if (c == 0) {
                    $("#Add" + ind).css("display", "none");
                    $("#tdrow" + ind).css("background-color", "#dbdeed");
                    //$("#tdrow" + ind).attr("disabled", "disabled");
                    if (contains($scope.scoreArr, $("#BCL_CH_NAME_" + ind).val()) == false) {
                        //if ($scope.scoreArr.indexOf($("#BCL_CH_NAME_" + ind).val()) == -1) {
                        $scope.scoreArr.push({ 'BCL_CH_ID': $("#hdid_" + ind).val(), 'BCL_CH_CODE': $("#BCL_CH_NAME_" + ind).val() });
                    }
                }
            }
            else {
                showNotification('error', 8, 'bottom-right', "Please Enter Score Name");
            }
        }
        function contains(arr, BCL_CH_CODE) {
            return $filter('filter')(arr, { 'BCL_CH_CODE': BCL_CH_CODE }, true).length != 0;
        }
        function validatinFn() {
            if ($scope.BranchIncentiveSubCategory.MainType[0] == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Type");
                return false;
            }
            if ($scope.BranchIncentiveSubCategory.Category == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Category");
                return false;
            }
            if ($scope.BranchIncentiveSubCategory.Category[0] == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Category");
                return false;
            }
            if ($scope.BranchIncentiveSubCategory.BCL_SUB_NAME == undefined || $scope.BranchIncentiveSubCategory.BCL_SUB_NAME == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Sub Category Name");
                return false;
            }
            if ($scope.BranchIncentiveSubCategory.BCL_SUB_REM == undefined || $scope.BranchIncentiveSubCategory.BCL_SUB_REM == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Sub Category Remarks");
                return false;
            }
            if ($scope.BranchIncentiveSubCategory.BCL_SUB_TYPE == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Sub Type");
                return false;
            }
            if ($scope.BranchIncentiveSubCategory.BCL_SUB_TYPE_NO != undefined) {
                if ($scope.scoreArr.length == 0) {
                    showNotification('error', 8, 'bottom-right', "Please Press Add Button");
                    return false;
                }
            }
            return true;
        }
        $scope.SaveA = function () {
            debugger;
            if ($('#BCL_SUB_FLAG_TYPE1').prop('checked') == true) {
                $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE1 = "Yes";
            }
            else { $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE1 = "No"; };
            if ($('#BCL_SUB_FLAG_TYPE2').prop('checked') == true) {
                $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE2 = "Yes";
            }
            else { $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE2 = "No"; };
            if ($('#BCL_SUB_FLAG_PLANTYPE').prop('checked') == true) {
                $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_PLANTYPE = "Yes";
            }
            else { $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_PLANTYPE = "No"; };

            if (validatinFn()) {
                var obj = {
                    BCL_SUB_ID: 0,
                    BCL_SUB_NAME: $scope.BranchIncentiveSubCategory.BCL_SUB_NAME,
                    BCL_MC_CODE: $scope.BranchIncentiveSubCategory.Category[0].BCL_MC_CODE,
                    BCL_SUB_REM: $scope.BranchIncentiveSubCategory.BCL_SUB_REM,
                    BCL_SUB_TYPE: $scope.BranchIncentiveSubCategory.BCL_SUB_TYPE,
                    BCL_SUB_FLAG_TYPE1: $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE1,
                    BCL_SUB_FLAG_TYPE2: $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE2,
                    BCL_SUB_FLAG_PLANTYPE: $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_PLANTYPE,
                    BCL_SUB_WEIGHTAGE: $scope.BranchIncentiveSubCategory.BCL_SUB_WEIGHTAGE,
                    BCL_SUB_TYPE_NO: 0,//$scope.BranchIncentiveSubCategory.BCL_SUB_TYPE_NO,
                    scoreA: $scope.scoreArr,
                }
                BranchIncentiveSubCategoryService.SaveData(obj).then(function (response) {
                    if (response != null) {
                        if (response.data == 0) {
                            showNotification('success', 8, 'bottom-right', 'Data Saved Successfully');
                            setTimeout(function () { $scope.GetMainType(); }, 500);
                            $scope.ClearData();
                            $('#disp').hide();
                            $("#dvScore").hide();
                        }
                        else
                            showNotification('error', 8, 'bottom-right', "Sub Category Already Exists");
                    }
                    else
                        showNotification('error', 8, 'bottom-right', response.Message);
                }, function (response) {
                    progress(0, '', false);

                });
            }

        }

        $scope.Update = function () {
            
            angular.forEach($scope.gridOptions2.rowData, function (value, key) {
                $scope.scoreArr.push({ 'BCL_CH_ID': value.BCL_CH_ID, 'BCL_CH_CODE': value.BCL_CH_NAME });                
            });
            if ($('#BCL_SUB_FLAG_TYPE1').prop('checked') == true) {
                $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE1 = "Yes";
            }
            else { $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE1 = "No"; };
            if ($('#BCL_SUB_FLAG_TYPE2').prop('checked') == true) {
                $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE2 = "Yes";
            }
            else { $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE2 = "No"; };
            if ($('#BCL_SUB_FLAG_PLANTYPE').prop('checked') == true) {
                $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_PLANTYPE = "Yes";
            }
            else { $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_PLANTYPE = "No"; };
            if (validatinFn()) {
                var obj = {
                    BCL_SUB_ID: $scope.BranchIncentiveSubCategory.BCL_SUB_ID,
                    BCL_SUB_NAME: $scope.BranchIncentiveSubCategory.BCL_SUB_NAME,
                    BCL_MC_CODE: $scope.BranchIncentiveSubCategory.Category[0].BCL_MC_CODE,
                    BCL_SUB_REM: $scope.BranchIncentiveSubCategory.BCL_SUB_REM,
                    BCL_SUB_TYPE: $scope.BranchIncentiveSubCategory.BCL_SUB_TYPE,
                    BCL_SUB_FLAG_TYPE1: $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE1,
                    BCL_SUB_FLAG_TYPE2: $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_TYPE2,
                    BCL_SUB_FLAG_PLANTYPE: $scope.BranchIncentiveSubCategory.BCL_SUB_FLAG_PLANTYPE,
                    BCL_SUB_WEIGHTAGE: $scope.BranchIncentiveSubCategory.BCL_SUB_WEIGHTAGE,
                    BCL_SUB_TYPE_NO: 0,//$scope.BranchIncentiveSubCategory.BCL_SUB_TYPE_NO,
                    scoreA: $scope.scoreArr,
                }
                BranchIncentiveSubCategoryService.updateData(obj).then(function (response) {
                    if (response != null) {
                        if (response == 0) {
                            showNotification('success', 8, 'bottom-right', 'Data update Successfully');
                            setTimeout(function () { $scope.GetMainType(); }, 500);
                            $scope.scoreArr = [];
                            //$scope.ClearData();
                        }
                        else
                            showNotification('error', 8, 'bottom-right', "Sub Category Already Exists");
                    }
                    else
                        showNotification('error', 8, 'bottom-right', response.Message);
                }, function (response) {
                    progress(0, '', false);
                });
            }
        }

        $scope.ShowStatus = function (value) {
            return $scope.StaDet[value == 0 ? 1 : 0].Name;
        }

        $scope.locSelectAll = function () {
            $scope.BranchIncentiveSubCategory.MainType = $scope.MainType;
            $scope.MainType();
        }

        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };

        setTimeout(function () { $scope.LoadData(); }, 1000);
        $scope.EditFunction = function (data) {
            $scope.BranchIncentiveSubCategory = {};
            $scope.BranchIncentiveSubCategory.BCL_SUB_ID = data.BCL_SUB_ID;
            $scope.BranchIncentiveSubCategory.BCL_SUB_WEIGHTAGE = data.BCL_SUB_WEIGHTAGE;
            BranchIncentiveSubCategoryService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                angular.forEach($scope.MainType, function (value, key) {
                    value.ticked = false;
                    if (value.BCL_TP_ID.toString() == data.BCL_TP_ID) {
                        value.ticked = true;
                    }
                });
            })
            setTimeout(function () {
                                angular.forEach($scope.Category, function (value, key) {
                    value.ticked = false;
                    if (value.BCL_MC_CODE.toString() == data.BCL_MC_CODE) {
                        value.ticked = true;
                    }
                });
            }, 1000);
            if (data.BCL_SUB_FLAG_TYPE1 == "Yes") {
                $('#BCL_SUB_FLAG_TYPE1').bootstrapToggle('on')
            }
            else {
                $('#BCL_SUB_FLAG_TYPE1').bootstrapToggle('off')
            }
            if (data.BCL_SUB_FLAG_TYPE2 == "Yes") {
                $('#BCL_SUB_FLAG_TYPE2').bootstrapToggle('on')
            }
            else {
                $('#BCL_SUB_FLAG_TYPE2').bootstrapToggle('off')
            }
            if (data.BCL_SUB_FLAG_PLANTYPE == "Yes") {
                $('#BCL_SUB_FLAG_PLANTYPE').bootstrapToggle('on')
            }
            else {
                $('#BCL_SUB_FLAG_PLANTYPE').bootstrapToggle('off')
            }
            $scope.EditCategory = data;
            angular.copy(data, $scope.BranchIncentiveSubCategory);
            $('#disp').show();
            setTimeout(function () { $scope.fn_SetEdit(data.BCL_SUB_ID, data.BCL_MC_CODE); }, 500);
            $scope.ActionStatus = 1;
            $scope.IsInEdit = true;
            $scope.Edit = true;
        }
        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };
        var columnDefs = [
            { headerName: "Edit", width: 40, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Type", field: "BCL_TP_NAME", cellClass: "grid-align", width: 120 },
            { headerName: "Category", field: "BCL_MC_NAME", cellClass: "grid-align", width: 120 },
            { headerName: "Sub Cat. Code", field: "BCL_SUB_CODE", cellClass: "grid-align", width: 120 },
            { headerName: "Sub Cat. Name", field: "BCL_SUB_NAME", cellClass: "grid-align", width: 130 },
            { headerName: "Weightage", field: "BCL_SUB_WEIGHTAGE", cellClass: "grid-align", width: 120 },
            { headerName: "Sub Type", field: "BCL_SUB_TYPE", cellClass: "grid-align", width: 90 },
            { headerName: "File", field: "BCL_SUB_FLAG_TYPE1", cellClass: "grid-align", width: 100 },
            { headerName: "Text", field: "BCL_SUB_FLAG_TYPE2", cellClass: "grid-align", width: 120 },
            { headerName: "Date", field: "BCL_SUB_FLAG_PLANTYPE", cellClass: "grid-align", width: 120 },
            { headerName: "Remarks", field: "BCL_SUB_REM", cellClass: "grid-align", width: 145 },
        ];
        $scope.gridOptions = {
            columnDefs: '',
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            },
        };
        $scope.SearchA = function () {
            var obj2 = {
                BCL_MC_CODE: _.filter($scope.Category, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_MC_CODE; }).join(','),
            }
            BranchIncentiveSubCategoryService.GetGridData(obj2).then(function (response) {
                if (response != null) {
                    $scope.gridata = response.data;
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                }
                progress(0, 'Loading...', false);
            })
        }

        $scope.GetMainType = function () {
            $scope.MainType = [];
            BranchIncentiveSubCategoryService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                //angular.forEach($scope.MainType, function (value, key) {
                //    var a = _.find($scope.MainType);
                //    a.ticked = true;
                //});
                setTimeout(function () { $scope.GetMainCategory(); }, 500);
            })
        }
        $scope.GetMainCategory = function () {
            $scope.Category = [];
            var obj2 = {
                BCL_TP_MC_ID: _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(','),
            }
            BranchIncentiveSubCategoryService.GetMainCategory(obj2).then(function (response) {
                $scope.Category = response.data;
                angular.forEach($scope.Category, function (value, key) {
                    var a = _.find($scope.Category);
                    a.ticked = true;
                });
                setTimeout(function () { $scope.SearchA(); }, 500);
            })
        }
        $scope.LoadData = function () {
            progress(0, 'Loading...', true);
            $scope.BranchIncentiveSubCategory = {};
            $scope.GetMainType();
            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
            progress(0, '', false);
        }
     
        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);

            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "IncentiveChildMaster.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel();

        }
        $scope.ClearData = function () {
            $scope.BranchIncentiveSubCategory = {};
            $scope.$broadcast('angucomplete-alt:clearInput');
        }
    }]);