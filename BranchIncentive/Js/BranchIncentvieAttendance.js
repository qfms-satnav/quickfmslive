﻿app.service("BranchIncentvieAttendanceService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGridData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentvieAttendance/GetGridData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.LocationExcelDownload = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentvieAttendance/LocationExcelDownload')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    };

}]);
app.controller('BranchIncentvieAttendanceController', function ($scope, UtilityService, $timeout, BranchIncentvieAttendanceService, $http) {

    $scope.UploadFile = function () {
        progress(0, 'Loading...', true);
        var ext = $('#fileUpl').val().split('.').pop().toLowerCase();
        if (ext == "xlsx" || ext == "xls") {
            var formData = new FormData();
            var UplFile = $('#fileUpl')[0];
            formData.append("UplFile", UplFile.files[0]);
            $http.post(UtilityService.path + '/api/BranchIncentvieAttendance/UploadFile', formData,
                {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function (data, status) {
                    $('#fileUpl').val('');
                    progress(0, 'Loading...', false);
                    setTimeout(function () {
                        showNotification(data.data.Message == 'Uploaded Successfully' ? 'success' : 'error', 25000, 'bottom-right', data.data.Message);
                    }, 1000);
                   /* showNotification(data.data.Message == 'Uploaded Successfully' ? 'success' : 'error', 25000, 'bottom-right', data.data.Message);*/
                }), function (error, data, status) {
                    $('#fileUpl').val('');
                    /* showNotification('error', 1000, 'bottom-right', 'Something Went Wrong,Please try again');*/
                    setTimeout(function () {
                        showNotification('error', 1000, 'bottom-right', 'Something Went Wrong,Please try again');
                    }, 1000);
                };
        } else {
            $('#fileUpl').val('');
            showNotification('error', 1000, 'bottom-right', 'Upload .xls, .xlsx files only');
        }
        $timeout(function () {
            $scope.LoadData()
        }, 300);
    }

    var columnDefs = [
        { headerName: "Branch Code", field: "Branch_Code", cellClass: "grid-align", width: 150 },
        { headerName: "Branch Name", field: "BIM_BIC_LOC_NAME", cellClass: "grid-align", width: 150 },
        { headerName: "BIC User ID", field: "BIM_BIC_USR_ID", cellClass: "grid-align", width: 150 },
        { headerName: "BIC Name", field: "BIM_BIC_USR_NAME", cellClass: "grid-align", width: 150 },
        { headerName: "Email", field: "BIM_BIC_EMAIL", cellClass: "grid-align", width: 150 },
        { headerName: "Quarter Name", field: "BIM_BIC_QUATER_NAME", cellClass: "grid-align", width: 150 },
        { headerName: "Quater StartDate", field: "BIM_BIC_QTR_START_DATE", cellClass: "grid-align", width: 150 },
        { headerName: "Quater EndDate", field: "BIM_BIC_QTR_END_DATE", cellClass: "grid-align", width: 150 },
        { headerName: "Tenure", field: "BIM_BIC_TENURE", cellClass: "grid-align", width: 150 },
        { headerName: "BIC Start Date", field: "BIM_BIC_START_DATE", cellClass: "grid-align", width: 150 },
        { headerName: "App Start Date", field: "BIM_BIC_APP_START_DATE", cellClass: "grid-align", width: 150 },
        { headerName: "BIC End date", field: "BIM_BIC_END_DATE", cellClass: "grid-align", width: 150 },
        { headerName: "Applicable Days", field: "BIM_BIC_APPLICABLE_DAYS", cellClass: "grid-align", width: 150 }

    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableScrollbars: false,
        angularCompileRows: true,
        groupHideGroupColumns: true,
        suppressHorizontalScroll: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        },
    };

    $scope.LoadData = function () {
        BranchIncentvieAttendanceService.GetGridData().then(function (response) {
            if (response != null) {
                $scope.gridata = response.data;
                $scope.gridOptions.api.setColumnDefs(columnDefs);
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            else {
                $scope.gridOptions.api.setColumnDefs(columnDefs);
            }
            progress(0, 'Loading...', false);
        })
    }

    $timeout(function () {
        $scope.LoadData()
    }, 700);

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var ws = XLSX.utils.json_to_sheet($scope.gridOptions.rowData);
        var columnHeaders = Object.keys($scope.gridOptions.rowData[0]);
        /* Iterate over the column headers */
        columnHeaders.forEach(function (header, columnIndex) {
            var Length = [];
            var headerLength = header.length + 2; // Start with the length of the header
            /* Iterate over the rows to find the maximum length of cell content */
            $scope.gridOptions.rowData.forEach(function (row) {
                var cellValue = row[header] ? row[header].toString() : '';
                var maxLength = Math.max(headerLength, cellValue.length + 2); // Add 2 for padding
                Length.push(maxLength);
            });
            let maximumValue = Math.max.apply(null, Length);
            //let maximumValue = Math.max(...Length);
            var columnPixels = maximumValue;
            /* Set the column width in the worksheet */
            ws['!cols'] = ws['!cols'] || [];
            ws['!cols'][columnIndex] = { width: columnPixels };

        });
        /* add to workbook */
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "BIC_ATTENDANCE.xlsx");
        /* write workbook and force a download */
        XLSX.writeFile(wb, "BIC_ATTENDANCE.xlsx");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }

    $scope.ExcelDownload = function () {
        debugger;
        BranchIncentvieAttendanceService.LocationExcelDownload().then(function (response) {
            var a = document.createElement('a');
            a.href = "../../UploadFiles/BIC_ATTENDANCE.xlsx";
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        });

    }
    function save(blob, fileName) {
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, fileName);
            progress(0, '', false);
        } else {
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
            window.URL.revokeObjectURL(link.href);
            $scope.ToggleDiv = !$scope.ToggleDiv;
            progress(0, '', false);
        }
    }

})