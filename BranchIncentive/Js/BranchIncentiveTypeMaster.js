﻿app.service("BranchIncentiveTypeMasterService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetMainType = function () {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveTypeMaster/GetMainType')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGridData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveTypeMaster/GetGridData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.updateData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveTypeMaster/updateData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveTypeMaster/SaveData', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BranchIncentiveTypeMasterController', ['$scope', '$q', 'UtilityService', 'BranchIncentiveTypeMasterService', '$filter', '$timeout', "$rootScope",
    function ($scope, $q, UtilityService, BranchIncentiveTypeMasterService, $filter, $timeout, $rootScope) {

       $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
        $scope.LoadInfo = [];
        $scope.BranchIncentiveTypeMaster = {};
        $scope.search = [];
        $scope.MainType = [];
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.Edit = false;
        function validatinFn() {
            if ($scope.BranchIncentiveTypeMaster.MainType[0] == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Category Code");
                return false;
            }
            return true;
        }
        $scope.SaveA = function () {
            if (validatinFn()) {
                var obj = {
                    BIM_CODE: $scope.BranchIncentiveTypeMaster.MainType[0].BICM_CODE,
                    BIM_NAME: $scope.BranchIncentiveTypeMaster.MainType[0].BICM_NAME,
                    BCL_TP_MC_ID:"1" ,//$scope.BranchIncentiveTypeMaster.MainType[0].BCL_TP_ID,
                    BIM_FROM_SCORE: $scope.BranchIncentiveTypeMaster.BIM_FROM_SCORE,
                    BIM_UPTO_SCORE: $scope.BranchIncentiveTypeMaster.BIM_UPTO_SCORE,
                    BIM_INCENTIVE_AMOUNT: $scope.BranchIncentiveTypeMaster.BIM_INCENTIVE_AMOUNT
                }
                BranchIncentiveTypeMasterService.SaveData(obj).then(function (response) {
                    if (response != null) {
                        if (response.data == 0) {
                            showNotification('success', 8, 'bottom-right', 'Data Saved Successfully');
                            setTimeout(function () { $scope.GetMainType(); }, 500);
                            $scope.ClearData();
                        }
                        else
                            showNotification('error', 8, 'bottom-right', "Category Code Already Exists");
                    }
                    else
                        showNotification('error', 8, 'bottom-right', "Some Error Occured");
                }, function (response) {
                    progress(0, '', false);

                });
            }
        }

        $scope.Update = function () {
            var obj = {
                BIM_CODE: $scope.BranchIncentiveTypeMaster.BIM_ID,
                BIM_NAME: $scope.BranchIncentiveTypeMaster.BIM_NAME,
                BCL_TP_MC_ID: "1",// $scope.BranchIncentiveTypeMaster.MainType[0].BCL_TP_ID,
                    BIM_FROM_SCORE: $scope.BranchIncentiveTypeMaster.BIM_FROM_SCORE,
                    BIM_UPTO_SCORE: $scope.BranchIncentiveTypeMaster.BIM_UPTO_SCORE,
                BIM_INCENTIVE_AMOUNT: $scope.BranchIncentiveTypeMaster.BIM_INCENTIVE_AMOUNT,
                       BIM_MC_STA_ID: $scope.BranchIncentiveTypeMaster.BIM_MC_STA_ID
            }
            BranchIncentiveTypeMasterService.updateData(obj).then(function (response) {
                debugger;
                if (response != null) {
                    if (response == 0) {
                        showNotification('success', 8, 'bottom-right', 'Data update Successfully');
                        setTimeout(function () { $scope.GetMainType(); }, 500);
                        $scope.ClearData();
                    }
                    else
                        setTimeout(function () { showNotification('success', 8, 'bottom-right', "Data update Successfully"); }, 1000);
                     /*   showNotification('success', 8, 'bottom-right', "Data update Successfully");*/
                    $scope.LoadData();
                }
                else
                    showNotification('error', 8, 'bottom-right', "Some Error Occured");
            }, function (response) {
                progress(0, '', false);
            });
        }
        $scope.locSelectAll = function () {
            $scope.BranchIncentiveTypeMaster.MainType = $scope.MainType;
            $scope.MainType();
        }
        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };
        setTimeout(function () { $scope.LoadData(); }, 1000);      
        $scope.EditFunction = function (data) {
            $scope.BranchIncentiveTypeMaster = {};
            $scope.BranchIncentiveTypeMaster.BIM_CODE = data.BIM_CODE;
            $scope.MainType = [];
            BranchIncentiveTypeMasterService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                angular.forEach($scope.MainType, function (value, key) {
                    value.ticked = false;
                    if (value.BICM_CODE.toString() == data.BIM_CODE) {
                        value.ticked = true;
                    }
                });
            })
            $scope.EditCategory = data;
            angular.copy(data, $scope.BranchIncentiveTypeMaster);
            $scope.ActionStatus = 1;
            $scope.IsInEdit = true;
            $scope.Edit = true;

        }
        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };
        var columnDefs = [
            { headerName: "Category Code", field: "BIM_CODE", cellClass: "grid-align", width: 150 },
            { headerName: "From Score", field: "BIM_FROM_SCORE", cellClass: "grid-align", width: 250 },
            { headerName: "Upto Score", field: "BIM_UPTO_SCORE", cellClass: "grid-align", width: 250 },
            { headerName: "Incentive Amount", field: "BIM_INCENTIVE_AMOUNT", cellClass: "grid-align", width: 350 },
            { headerName: "Status", template: "{{ShowStatus(data.BIM_MC_STA_ID)}}", width: 170, cellClass: 'grid-align' },
            { headerName: "Edit", width: 150, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true }

        ];
        $scope.gridOptions = {
            columnDefs: '',
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: false,
            enableSorting: true,
            enableScrollbars: false,
            angularCompileRows: true,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            },
        };
        $scope.ShowStatus = function (value) {
            return $scope.StaDet[value == 0 ? 1 : 0].Name;
        }
        $scope.SearchA = function () {
            var obj2 = {
                BCL_TP_MC_ID: _.filter($scope.MainType, function (o) { return o.ticked == true; }).map(function (x) { return x.BCL_TP_ID; }).join(','),
            }
            BranchIncentiveTypeMasterService.GetGridData(obj2).then(function (response) {
                if (response != null) {
                    $scope.gridata = response.data;
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                }
                progress(0, 'Loading...', false);
            })
        }
        $scope.GetMainType = function () {
            $scope.MainType = [];
            BranchIncentiveTypeMasterService.GetMainType().then(function (response) {
                $scope.MainType = response.data;
                //angular.forEach($scope.MainType, function (value, key) {
                //    var a = _.find($scope.MainType);
                //    a.ticked = true;
                //});
                setTimeout(function () { $scope.SearchA(); }, 500);
            })
        }
        $scope.LoadData = function () {
            progress(0, 'Loading...', true);
            $scope.BranchIncentiveTypeMaster = {};
            $scope.GetMainType();
            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
            progress(0, '', false);
        }
        $scope.GenReport = function (data) {
            progress(0, 'Loading...', true);
            setTimeout(function () {
                progress(0, 'Loading...', false);
                if (data == 'pdf') {
                    const songs = [];
                    angular.forEach($scope.LoadInfo, function (val) {
                        songs.push({ "AAT_CODE": val.AAS_SPAREPART_NAME + ',' + val.AAS_SPAREPART_ID, "AAT_NAME": val.AAS_SPAREPART_DES });
                    });
                    qr_generate(songs);
                    progress(0, 'Loading...', false);
                }
            }, 3000);
        }
        $scope.ClearData = function () {
            $scope.BranchIncentiveTypeMaster = {};
            $scope.$broadcast('angucomplete-alt:clearInput');
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.Edit = false;
        }

        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);

            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "Incentive_Amount_Master.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
                $scope.GenerateFilterExcel();
           
        }
    }]);