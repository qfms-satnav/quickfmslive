﻿app.service("BranchIncentiveTypeService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetGridData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveType/GetGridData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.updateData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveType/updateData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchIncentiveType/SaveData', data)
            .then(function (response) {
                deferred.resolve(response);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('BranchIncentiveTypeController', ['$scope', '$q', 'UtilityService', 'BranchIncentiveTypeService', '$filter', '$timeout', "$rootScope",
    function ($scope, $q, UtilityService, BranchIncentiveTypeService, $filter, $timeout, $rootScope) {
        $scope.StaTypeLevel = [{ Id: '0', Name: 'Inspection Level' }, { Id: '1', Name: 'Validation Level' }, { Id: '2', Name: 'Approval Level' }];
        $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
        $scope.LoadInfo = [];
        $scope.BranchIncentiveType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.Edit = false;
        function validatinFn() {           
            if ($scope.BranchIncentiveType.BCL_TP_NAME == undefined || $scope.BranchIncentiveType.BCL_TP_NAME == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Check Type Name");
                return false;
            }            
            if ($scope.BranchIncentiveType.BCL_TP_REMARKS == undefined || $scope.BranchIncentiveType.BCL_TP_REMARKS == '') {
                showNotification('error', 8, 'bottom-right', "Please Enter Check Type Remarks");
                return false;
            }
            if ($scope.BranchIncentiveType.BCL_TP_LEVEL == undefined) {
                showNotification('error', 8, 'bottom-right', "Please Select Check List Approval Level");
                return false;
            }
            return true;
        }        
        $scope.SaveA = function () {            
            if (validatinFn()) {                
                var obj = {
                    BCL_TP_ID: 0,
                    BCL_TP_NAME: $scope.BranchIncentiveType.BCL_TP_NAME,
                    BCL_TP_REMARKS: $scope.BranchIncentiveType.BCL_TP_REMARKS,
                    BCL_TP_LEVEL: $scope.BranchIncentiveType.BCL_TP_LEVEL
                    //SUBCAT_STS_ID: 1,
                }
                BranchIncentiveTypeService.SaveData(obj).then(function (response) {
                    if (response != null) {                       
                        if (response.data == 0) {
                            showNotification('success', 8, 'bottom-right', 'Data Saved Successfully');
                            setTimeout(function () { $scope.LoadData(); }, 500);
                            $scope.ClearData();
                        }
                        else
                            showNotification('error', 8, 'bottom-right', "Check Type Name Already Exists");
                    }
                    else
                        showNotification('error', 8, 'bottom-right', "Some Error Occured");
                }, function (response) {
                    progress(0, '', false);

                });
            }

        }

        $scope.Update = function () {
            var obj = {
                BCL_TP_ID: $scope.BranchIncentiveType.BCL_TP_ID,
                BCL_TP_NAME: $scope.BranchIncentiveType.BCL_TP_NAME,
                BCL_TP_REMARKS: $scope.BranchIncentiveType.BCL_TP_REMARKS,
                BCL_TP_LEVEL: $scope.BranchIncentiveType.BCL_TP_LEVEL
            }
            BranchIncentiveTypeService.updateData(obj).then(function (response) {
                if (response != null) {
                    if (response == 0) {
                        showNotification('success', 8, 'bottom-right', 'Data update Successfully');
                        setTimeout(function () { $scope.SearchA(); }, 500);
                    }
                    else
                        showNotification('error', 8, 'bottom-right', "Check Type Name Already Exists");
                }
                else
                    showNotification('error', 8, 'bottom-right', "Some Error Occured");
            }, function (response) {
                progress(0, '', false);
            });
        }

        $scope.ShowStatus = function (value) {
            return $scope.StaDet[value == 0 ? 1 : 0].Name;
        }

     

        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };

        setTimeout(function () { $scope.LoadData(); }, 1000);
        //$timeout($scope.LoadData, 1000);

        $scope.EditFunction = function (data) {
            $scope.BranchIncentiveType = {};
            $scope.BranchIncentiveType.BCL_TP_ID = data.BCL_TP_ID;
            $scope.BranchIncentiveType.BCL_TP_LEVEL = data.BCL_TP_LEVEL;
            setTimeout(function () { $('#BCL_TP_LEVEL').val(data.BCL_TP_LEVEL); }, 500);            
            $scope.EditCategory = data;
            angular.copy(data, $scope.BranchIncentiveType);
            $scope.ActionStatus = 1;
            $scope.IsInEdit = true;
            $scope.Edit = true;

        }
        $scope.remoteUrlRequestFn = function (str) {
            return { q: str };
        };
        var columnDefs = [
            { headerName: "Edit", width: 40, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true },
            { headerName: "Check Type Name", field: "BCL_TP_NAME", cellClass: "grid-align", width: 250 },
            { headerName: "Check List Approval Level", field: "LEVELTYPE", cellClass: "grid-align", width: 250 },
            { headerName: "Check Type Remarks", field: "BCL_TP_REMARKS", cellClass: "grid-align", width: 450 },
           // { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.SUBC_STA_ID)}}", width: 120, cellClass: 'grid-align' },
        ];
        $scope.gridOptions = {
            columnDefs: '',
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,          
            enableScrollbars: false,
            angularCompileRows: true,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,            
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            },
        };
        $scope.SearchA = function () {           
           BranchIncentiveTypeService.GetGridData().then(function (response) {
                if (response != null) {
                    $scope.gridata = response.data;
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(columnDefs);
                }
                progress(0, 'Loading...', false);
            })
        }        

        $scope.LoadData = function () {           
            progress(0, 'Loading...', true);
            $scope.BranchIncentiveType = {};
            $scope.SearchA();
            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })
            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
            progress(0, '', false);
        }
      
        $scope.GenerateFilterExcel = function () {
            progress(0, 'Loading...', true);

            var Filterparams = {

                columnGroups: true,
                allColumns: true,
                onlySelected: false,
                columnSeparator: ',',
                fileName: "IncentiveMainCategory.csv"
            };
            $scope.gridOptions.api.exportDataAsCsv(Filterparams);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 1000);
        }

        $scope.GenReport = function (Type) {
            progress(0, 'Loading...', true);
            $scope.GenerateFilterExcel();

        }
           
        $scope.ClearData = function () {
            $scope.BranchIncentiveType = {};
            $scope.$broadcast('angucomplete-alt:clearInput');
        }
    }]);