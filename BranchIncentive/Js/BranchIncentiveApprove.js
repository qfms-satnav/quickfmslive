﻿
app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
app.service("ApproveScoreCard", function ($http, $q, UtilityService) {

    this.GetGriddata = function (BCL_ID) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/GetGriddata?BCL_ID=' + BCL_ID)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetScorePer = function () {
        var deferred = $q.defer();
        $http.get(UtilityService.path + '/api/CreateScoreCard/GetScorePer')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };
    this.getSavedList = function (BCLStatus, Status) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/getSavedList?BCLStatus=' + BCLStatus + '&Status=' + Status + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getInspectorsZonalCentral = function (TPM_TP_ID, TPM_LCM_CODE, TPMD_APPR_LEVELS) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/getInspectorsZonalCentral?TPM_TP_ID=' + TPM_TP_ID + '&TPM_LCM_CODE=' + TPM_LCM_CODE + '&TPMD_APPR_LEVELS=' + TPMD_APPR_LEVELS + ' ')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveApprovalData = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/SaveApprovalData', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.BulkApprovalData = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CreateScoreCard/BulkApprovalData', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

});

app.controller('BranchIncentiveApproveController', function ($scope, $q, $location, ApproveScoreCard, UtilityService, $filter, $http, $ngConfirm) {

    $scope.BranchIncentiveApprove = {};
    $scope.TableData = [];
    $scope.rowData = [];
    $scope.dvbutton = true;
    $scope.ChecklistID = '';
    $scope.Score_Per = [];

    $scope.OverallScore = function () {
        $scope.FinalTotalWeightagePercent = 0;
        _.forEach($scope.TableData, function (value) {
            var OverAllScore = 0;
            _.forEach(value.ChecklistScore1, function (value1) {

                var ScorePercenatge = _.find($scope.Score_Per, { BCL_CH_NAME: value1.RESPONSE })

                value1.BCL_WEIGHTAGE_PERCENTAGE = parseInt(value1.BCL_SUB_WEIGHTAGE, 10) * (parseInt(ScorePercenatge != undefined ? ScorePercenatge.BCL_CH_PERCENTAGE : 0, 10) / 100);
                OverAllScore = OverAllScore + value1.BCL_WEIGHTAGE_PERCENTAGE
            });
            value.TotalWeightagePercent = OverAllScore;
            $scope.FinalTotalWeightagePercent = $scope.FinalTotalWeightagePercent + OverAllScore
        });
    }

    $scope.Getstring = function (data) {
        return (data == '2') ? 'Submitted' : 'Validated';
    }

    $scope.GetDrafts = function (BCLStatus) {
        if (BCLStatus == 1) {
            $('#hsavedtitle').html("Saved Drafts");
        }
        else {
            $('#hsavedtitle').html("Validated Scorecard");

        }

        var Status = 2;
        ApproveScoreCard.getSavedList(BCLStatus, Status).then(function (response) {
            if (response.data != null) {
                $scope.BranchIncentiveApprove.SaveList = response.data;
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }

    ApproveScoreCard.GetScorePer().then(function (response) {
        if (response.data != null) {
            $scope.Score_Per = response.data;
            //$scope.LoadData();
        };
    });
    $scope.BranchIncentiveApprove.SVR_FROM_DATE = moment().format('MM/DD/YYYY');
    $scope.DateClick = function (id) {
        $('#' + id + '_1').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true,
            todayHighlight: true,
        });
    }

    $scope.LoadIncharge = function (items) {
        ApproveScoreCard.getInspectorsZonalCentral(0, items.LCM_CODE, 0).then(function (response) {
            if (response.data != null) {
                $scope.Inspection = response.data;
                angular.forEach($scope.Inspection, function (item, index) {
                    if (index == 0) {
                        item.ticked = true
                    }
                });
            }
            else
                showNotification('error', 8, 'bottom-right', response.Message);
        }, function (response) {
            progress(0, '', false);
        });
    }


    var SCPenalty = '';
    $scope.LoadData = function (items) {
        $scope.LoadIncharge(items);
        $scope.BranchIncentiveApprove.LCM_NAME = items.LCM_NAME;
        $scope.BranchIncentiveApprove.BCL_INSPECTED_BY = items.BCL_INSPECTED_BY;
        $scope.BranchIncentiveApprove.BCL_SELECTED_DT = items.BCL_SELECTED_DT;
        $scope.BranchIncentiveApprove.BCLD_REVIEWED_BY = items.BCLD_REVIEWED_BY;
        $scope.BranchIncentiveApprove.BCLD_REVIEWED_DT = items.BCLD_REVIEWED_DT
        $scope.BranchIncentiveApprove.BCL_ID = items.BCL_ID;
        $scope.GetLocationCode(items);
        /*SCPenalty = items.bicscore;*/
        SCPenalty = items.BCL_PPM_ASSET_ID;

    }

    $scope.loadModal = function () {
        // Function to show the modal on page load
        $('#myModal').modal('show');
        $scope.GetDrafts(3);
    };
    $scope.GetLocationCode = function (data) {
        ApproveScoreCard.GetGriddata(data.BCL_ID).then(function (response) {
            if (response.data != null) {
                $scope.FinalTotalWeightage = 0;
                $scope.FinalTotalWeightagePercent = 0;
                if (response.data != null) {
                    $scope.TableData = response.data;
                    _.forEach($scope.TableData, function (value) {
                        var OverAllScore = 0;
                        var BCL_SUB_WEIGHTAGE = 0;
                        _.forEach(value.ChecklistScore1, function (value1) {
                            BCL_SUB_WEIGHTAGE = BCL_SUB_WEIGHTAGE + parseInt(value1.BCL_SUB_WEIGHTAGE, 10);
                            var ScorePercenatge = _.find($scope.Score_Per, { BCL_CH_NAME: value1.RESPONSE })
                            value1.BCL_WEIGHTAGE_PERCENTAGE = parseInt(value1.BCL_SUB_WEIGHTAGE, 10) * (parseInt(ScorePercenatge != undefined ? ScorePercenatge.BCL_CH_PERCENTAGE : 0, 10) / 100);
                            OverAllScore = OverAllScore + value1.BCL_WEIGHTAGE_PERCENTAGE
                        });
                        value.TotalWeightage = BCL_SUB_WEIGHTAGE;
                        value.TotalWeightagePercent = OverAllScore;
                        $scope.FinalTotalWeightage = $scope.FinalTotalWeightage + value.TotalWeightage;
                        $scope.FinalTotalWeightagePercent = $scope.FinalTotalWeightagePercent + value.TotalWeightagePercent;
                    });
                }
                angular.forEach($scope.Inspection, function (value, key) {
                    value.ticked = true;
                });
                angular.forEach($scope.Location, function (value, key) {
                    value.ticked = true;
                });
                angular.forEach($scope.Location, function (value, key) {
                    if (data.LCM_CODE == value.LCM_CODE) {
                        value.ticked = true;
                    }

                });
                angular.forEach($scope.Inspection, function (value, key) {
                    if (data.BCL_INSPECTED_BY == value.INSPECTOR) {
                        value.ticked = true;
                    }

                });
                $scope.BranchIncentiveApprove.SVR_FROM_DATE = data.BCL_SELECTED_DT;
                $scope.BranchIncentiveApprove.OVERALL_CMTS = data.BCL_OVERALL_CMTS;
                $scope.BranchIncentiveApprove.BCL_VALIDATE_OVERALL_CMTS = data.BCL_VALIDATE_OVERALL_CMTS;
                $scope.BranchIncentiveApprove.BCL_APPROVEL_OVERALL_CMTS = data.BCL_APPROVEL_OVERALL_CMTS;

                $scope.TableData = response.data;
                $('#myModal').modal('toggle');
            }
        });
    }
    $scope.Clear = function () {
        $scope.CreateScoreCard = {};
        $scope.TableData = [];
        $scope.rowData = [];
        //angular.forEach($scope.Inspection, function (value, key) {
        //    value.ticked = false;
        //});
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        $scope.BranchIncentiveApprove.LCM_NAME = '';
        $scope.BranchIncentiveApprove.BCL_INSPECTED_BY = '';
        $scope.BranchIncentiveApprove.BCL_SELECTED_DT = '';
        $scope.BranchIncentiveApprove.BCLD_REVIEWED_BY = '';
        $scope.BranchIncentiveApprove.BCLD_REVIEWED_DT = '';
        $scope.BranchIncentiveApprove.OVERALL_CMTS = '';
        $scope.BranchIncentiveApprove.BCL_VALIDATE_OVERALL_CMTS = '';
        $scope.BranchIncentiveApprove.BCL_APPROVEL_OVERALL_CMTS = '';
        $scope.FinalTotalWeightage = '';
        $scope.FinalTotalWeightagePercent = '';

        $scope.dvbutton = true;
        $scope.ChecklistID = '';
        $scope.LoadData();
        $scope.loadModal();
    }

    // flag id 4 means Approval Submitted, 6 means Approval Rejected
    $scope.Submit = function (flagid) {

        $scope.SelectedDatas = [];
        angular.forEach($scope.TableData, function (Value1, Key) {
            angular.forEach(Value1.ChecklistScore1, function (Value, Key) {
                var BCL_CH_CODE = _.find(Value.checklistDetails, { BCL_CH_NAME: Value.RESPONSE })
                $scope.SelectedData = {};
                $scope.SelectedData.TypeCode = Value1.BCL_TP_ID;
                $scope.SelectedData.CatCode = Value.BCL_MC_CODE;
                $scope.SelectedData.SubcatCode = Value.BCL_SUB_CODE;
                //$scope.SelectedData.BCLD_CENTRAL_ACTIONS = Value.ApprovalAction;
                //$scope.SelectedData.BCLD_CENTRAL_TEAM_CMTS = Value.ApproveInchargeComment;
                $scope.SelectedData.ScoreCode = BCL_CH_CODE.BCL_CH_CODE;
                $scope.SelectedData.ScoreName = BCL_CH_CODE.BCL_CH_NAME;
                $scope.SelectedData.ScorePercent = Value.BCL_WEIGHTAGE_PERCENTAGE;
                $scope.SelectedDatas.push($scope.SelectedData);
            });
        });
        if ($scope.SelectedDatas.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please Select the at least one Scorecard ');
            return;
        }
        if ($scope.BranchIncentiveApprove.Inspection[0].length == 0) {
            showNotification('error', 8, 'bottom-right', 'Please select the scorecard Validated By');
            return;
        }
        var TOTSCPenalty = SCPenalty - 15;
        if (TOTSCPenalty > $scope.FinalTotalWeightagePercent) {
            showNotification('error', 8, 'bottom-right', 'penalty percentage cannot be reduced  more than 15% ');
            return;
        }
        if ($scope.FinalTotalWeightagePercent > SCPenalty) {
            showNotification('error', 8, 'bottom-right', 'Do not have the right to increase the score beyond the submitted score');
            return;
        }

        $scope.data = {
            InspectdBy: $scope.BranchIncentiveApprove.Inspection[0].INSPECTOR,
            date: $scope.BranchIncentiveApprove.SVR_FROM_DATE,
            Seldata: $scope.SelectedDatas,
            Flag: flagid,
            OVERALL_CMTS: $('#BCL_APPROVEL_OVERALL_CMTS').val(),
            BCL_ID: $scope.BranchIncentiveApprove.BCL_ID,
            F_Percentage: $scope.FinalTotalWeightagePercent
        };
        ApproveScoreCard.SaveApprovalData($scope.data).then(function (response) {
            debugger;
            if (response.data != null) {

                setTimeout(function () {
                    showNotification('success', 8, 'bottom-right', response.data);
                }, 500);
                $scope.Clear();
            } else {
                setTimeout(function () {
                    showNotification('error', 8, 'bottom-right', 'Something Went Wrong');
                }, 500);

            }
        });
    }

    //$scope.submitCheckedRecords = function () {
    //    debugger;
    //    var checkedRecords = $scope.BranchIncentiveApprove.SaveList
    //        .filter(function (item) {
    //            return item.isChecked;
    //        })
    //        .map(function (item) {
    //            return item.BCL_ID;
    //        });

    //    console.log(checkedRecords);

    //    if (checkedRecords.length === 0) {
    //        setTimeout(function () {
    //            showNotification('error', 8, 'bottom-right', 'Please Select the at least one Scorecard for Submitt');
    //        }, 500);
    //    } else {
    //        ApproveScoreCard.BulkApprovalData(checkedRecords).then(function (response) {
    //            debugger;
    //            if (response != null) {
    //                setTimeout(function () {
    //                    showNotification('success', 8, 'bottom-right', 'Approved Successfully');
    //                }, 500);
    //                $scope.loadModal();
    //            }
    //        });
    //    }
    //};

    $scope.selectAllRecords = function () {
        var selectAllCheckbox = $('#selectAll').is(':checked');
        angular.forEach($scope.BranchIncentiveApprove.SaveList, function (item) {
            item.isChecked = selectAllCheckbox;
        });
    };

    $scope.submitCheckedRecords = function () {
        var checkedRecords = $scope.BranchIncentiveApprove.SaveList
            .filter(function (item) {
                return item.isChecked;
            })
            .map(function (item) {
                return item.BCL_ID;
            });

        console.log(checkedRecords);

        if (checkedRecords.length === 0) {
            setTimeout(function () {
                showNotification('error', 8, 'bottom-right', 'Please Select at least one Scorecard for Submit');
            }, 500);
        } else {
            ApproveScoreCard.BulkApprovalData(checkedRecords).then(function (response) {
                if (response != null) {
                    setTimeout(function () {
                        showNotification('success', 8, 'bottom-right', 'Approved Successfully');
                    }, 500);
                    $scope.loadModal();
                }
            });
        }
    };


})