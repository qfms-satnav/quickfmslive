﻿app.service("BranchScoreCardReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetRegions = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchScoreCardReport/GetRegions', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetSpocs = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchScoreCardReport/GetSpocs', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetScoreCardtDetails = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchScoreCardReport/GetScoreCardtDetails', data)

            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetCitiesByZone = function (ID,ZNlsts ) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchScoreCardReport/GetCitiesByZone?id=' + ID + ' ', ZNlsts)

            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetLocationsScoreReport = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BranchScoreCardReport/GetLocationsScoreReport', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


}]);
app.controller('BranchScoreCardReportController', ['$scope', '$q', '$http', 'BranchScoreCardReportService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, BranchScoreCardReportService, UtilityService, $timeout, $filter) {
    $scope.BranchScoreCardReport = {};
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.ZNlsts = [];
    $scope.Region = [];
    $scope.Spocs = [];

    $scope.gridOptions = {
        columnDefs: '',
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableScrollbars: false,
        groupHideGroupColumns: true,
        suppressHorizontalScroll: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };

    $scope.Pageload = function () {

        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        BranchScoreCardReportService.GetLocationsScoreReport().then(function (response) {
            if (response!= null) {
                $scope.Location = response
                BranchScoreCardReportService.GetSpocs().then(function (response) {
                    debugger;
                    if (response != null) {
                        $scope.Spocs = response;
                        angular.forEach($scope.Spocs, function (value, key) {
                            value.ticked = true;
                        })
                    }
                });
                angular.forEach($scope.Location, function (value, key) {
                    value.ticked = true;
                })
            }
        });
       
    }

    $scope.GetCitiesByZone = function () {
        debugger;
        BranchScoreCardReportService.GetCitiesByZone(2,$scope.BranchScoreCardReport.Region).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getLocationsByCity = function () {

        UtilityService.getLocationsByCity($scope.BranchScoreCardReport.City, 2).then(function (response) {
            $scope.Location = response.data;
        }, function (error) {
            console.log(error);
        });
    }


    $scope.LocationChange = function () {
        debugger;
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.BranchScoreCardReport.City = cty;
            }
        });

        angular.forEach($scope.Location, function (value, key) {
            var zn = _.find($scope.Region, { LCM_ZONE: value.LCM_ZONE });
            if (zn != undefined && value.ticked == true) {
                zn.ticked = true;
                $scope.BranchScoreCardReport.Region = zn;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.BranchScoreCardReport.Location = $scope.Location;
        $scope.Location();
    }


    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        var dataObj = {
            SearchValue: searchval,
            LCM_ZONE: _.filter($scope.BranchScoreCardReport.Region, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_ZONE; }).join(','),
            CTY_CODE: _.filter($scope.BranchScoreCardReport.City, function (o) { return o.ticked == true; }).map(function (x) { return x.CTY_CODE; }).join(','),
            LCM_CODE: _.filter($scope.BranchScoreCardReport.Location, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
            AUR_KNOWN_AS: _.filter($scope.BranchScoreCardReport.Spocs, function (o) { return o.ticked == true; }).map(function (x) { return x.AUR_KNOWN_AS; }).join(','),
            FromDate: moment($scope.BranchScoreCardReport.FromDate, "DD/MMM/YYYY").format("MM-DD-YYYY"),
            ToDate: moment($scope.BranchScoreCardReport.ToDate, "DD/MMM/YYYY").format("MM-DD-YYYY")
        };

        BranchScoreCardReportService.GetScoreCardtDetails(dataObj).then(function (response) {
            debugger;
            if (response.Message == "OK") {

                $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData(response.griddata);
            }
            else {
                $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData([]);
            }
            progress(0, 'Loading...', false);
        });

    };
    
    $scope.Pageload();
    setTimeout(function () { $scope.LoadData(); }, 3000);

    $scope.search = function () {
        $scope.LoadData();
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var ws = XLSX.utils.json_to_sheet($scope.gridOptions.rowData);
        var columnHeaders = Object.keys($scope.gridOptions.rowData[0]);
        columnHeaders.forEach(function (header, columnIndex) {
            var Length = [];
            var headerLength = header.length + 2; 
            $scope.gridOptions.rowData.forEach(function (row) {
                var cellValue = row[header] ? row[header].toString() : '';
                var maxLength = Math.max(headerLength, cellValue.length + 2); 
                Length.push(maxLength);
            });
            let maximumValue = Math.max.apply(null, Length);
            var columnPixels = maximumValue;
            ws['!cols'] = ws['!cols'] || [];
            ws['!cols'][columnIndex] = { width: columnPixels };

        });
     
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "SCORE_CARD_REPORT");
        XLSX.writeFile(wb, "SCORE_CARD_REPORT.xlsx");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }


    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
}]);
