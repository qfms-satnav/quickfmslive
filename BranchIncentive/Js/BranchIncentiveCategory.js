﻿app.service("BranchIncentiveCategoryService", ['$http', '$q', function ($http, $q) {
    this.SaveCategoryDetails = function (response) {
        debugger;
        deferred = $q.defer();
        return $http.post('../../api/BranchIncentiveCategory/SaveCategoryDetails', response)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.ModifyCategoryDetails = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/BranchIncentiveCategory/ModifyCategoryDetails', response)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetConfGrid = function () {
        debugger;
        deferred = $q.defer();
        return $http.post('../../api/BranchIncentiveCategory/GetGridData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    
}]);
app.controller('BranchIncentiveCategoryController', ['$scope', '$q', 'BranchIncentiveCategoryService', 'UtilityService', '$filter', function ($scope, $q,BranchIncentiveCategoryService, UtilityService, $filter) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.CategoryType = {};
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

        var columnDefs = [
            { headerName: "Category Code", field: "BICM_CODE", width: 150, cellClass: 'grid-align' },
            { headerName: "Category Name", field: "BICM_NAME", width: 150, cellClass: 'grid-align' },
            { headerName: "Range", field: "BICM_RANGE", width: 280, cellClass: 'grid-align' },
            { headerName: "Status", template: "{{ShowStatus(data.BICM_STA_ID)}}", width: 170, cellClass: 'grid-align' },
            { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];

        $scope.gridOptions = {
            columnDefs: columnDefs,
            enablefilter: true,
            enableCellSelection: false,
            rowData: null,
            enableSorting: true,
            angularCompileRows: true,
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit()
            }

    };

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        BranchIncentiveCategoryService.GetConfGrid().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData(data);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 700);
        }, function (error) {
            console.log(error);
        });
    }


    $scope.Save = function () {
        if ($scope.IsInEdit) {
            BranchIncentiveCategoryService.ModifyCategoryDetails($scope.CategoryType).then(function (response) {
                var savedobj = {};
                angular.copy($scope.CategoryType, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data Modified successfully ";

                BranchIncentiveCategoryService.GetConfGrid().then(function (data) {
                    $scope.gridata = data;
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.IsInEdit = false;
                $scope.EraseData();

                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            });
        }
        else {
            $scope.CategoryType.BICM_STA_ID = "1";
            BranchIncentiveCategoryService.SaveCategoryDetails($scope.CategoryType).then(function (response) {
                debugger;
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully inserted";
                var savedobj = {};
                angular.copy($scope.CategoryType, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.CategoryType = {};
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }

    $scope.EditData = function (data) {
        $scope.CategoryType = {};
        angular.copy(data, $scope.CategoryType);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
    }
    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }
    $scope.EraseData = function () {
        $scope.CategoryType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmBranchincentive.$submitted = false;

    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1000);

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "BranchIncentive_Category_Master.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();

    }
}]);
