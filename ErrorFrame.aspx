﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ErrorFrame.aspx.vb" Inherits="ErrorFrame" %>

<html>
<head>
    <title>Error Page</title>
    <%--<script type="text/javascript">
        function DIVVisible(Parm) {
            if (Parm == 'VISIBLE') {
                document.getElementById('<%= ErrorDiv.ClientID %>').style.display = 'block';
                document.getElementById('<%= tdShow.ClientID %>').style.display = 'none'
                document.getElementById('<%= tdHide.ClientID %>').style.visibility = 'visible'
            }
            else {
                document.getElementById('<%= ErrorDiv.ClientID %>').style.display = 'none';
                document.getElementById('<%= tdShow.ClientID %>').style.display = 'block'
                document.getElementById('<%= tdHide.ClientID %>').style.visibility = 'hidden'
            }
        }
    </script>--%>
</head>
<body>
    <form runat="server" id="form1">

        <%-- For Production Version --%>

       <%-- <div align="center" style="padding-top: 10%;">
            <div>
                <img src='<%= Page.ResolveUrl("~/BootStrapCSS/images/quickfms_p_l_logo1.png")%>' />
            </div>
            <div style="padding-top: 40px; margin-left: 350px;">
                "Something went wrong", Sorry for the inconvenience.   Please let us know about it by writing to support@quickfms.com
            </div>

            <hr>
        </div>--%>
         <div style="padding-top: 55px; text-align:center">
         <img src='<%= Page.ResolveUrl("~/BootStrapCSS/images/QuickFMSLogo.jpg")%>' />
    </div>
          <br />
    <br />
    <div style="padding-top: 40px; text-align:center">
        <strong>"Something went wrong", Sorry for the inconvenience. </strong><br/>  <br/>  Please let us know about it by writing to <a href="mailto:support@quickfms.com" target="_top">support@quickfms.com</a> 
    </div>

        <%-- For Development Version --%>
        <%--<div runat="server">
            #Error Message:
            <div id="tdShow" runat="Server">
                <asp:HyperLink ID="hlShow" Text="Show" runat="server" onclick="javascript:DIVVisible('VISIBLE');"></asp:HyperLink>
            </div>

            <div id="tdHide" runat="Server" style="visibility: hidden;">
                <asp:HyperLink ID="hlHide" Text="Hide" runat="server" onclick="javascript:DIVVisible('INVISIBLE');"></asp:HyperLink>
            </div>

            <div id="ErrorDiv" style="display: none;" runat="server">
                <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
            </div>
        </div>--%>
    </form>
</body>
</html>
