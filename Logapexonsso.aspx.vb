﻿
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.Owin.Security
Imports System.Configuration.ConfigurationManager
Imports System.Security.Claims

Partial Class Logapexonsso
    Inherits System.Web.UI.Page
    Public Function logerrors(ByVal [error] As String)
        If (Session("TENANT") = "[Tavant].dbo") Then
            Dim filename As String = "Log_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt"

            Dim filepath As String = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") & filename)

            If File.Exists(filepath) Then
                Using stwriter As New StreamWriter(filepath, True)
                    stwriter.WriteLine("-------------------START-------------" + DateTime.Now)
                    stwriter.WriteLine([error])
                    stwriter.WriteLine("-------------------END-------------" + DateTime.Now)
                End Using
            Else
                Dim stwriter As StreamWriter = File.CreateText(filepath)
                stwriter.WriteLine("-------------------START-------------" + DateTime.Now)
                stwriter.WriteLine([error])
                stwriter.WriteLine("-------------------END-------------" + DateTime.Now)
                stwriter.Close()
            End If

        End If
        Return [error]
    End Function


    Protected Sub btnSignIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSignIn.Click
        Dim UserStatus As String
        Session("UID") = ""
        Session("uname") = ""
        Session("COMPANYID") = ""
        Session("TENANT") = tenantID.Text
        Session("DepMethod") = ""
        Dim staid As String = ""
        Dim Tenant_Id As String = ""
        Dim COMPANYID As Integer
        Dim Tenant_Active As Integer
        Session("UID") = txtUsrId.Text
        Tenant_Id = ValidateTenant(tenantID.Text)

        Session("TENANT") = Tenant_Id
        If Tenant_Id <> "0" Then
            Tenant_Active = ValidateIsTenantActive(tenantID.Text)
            If Tenant_Active = "0" Then
                lbl1.Text = "Account has been Inactive!"
                Exit Sub
            End If
            staid = ValidateUser(txtUsrId.Text, txtUsrPwd.Text)
            If staid <> "0" And staid <> "1" Then
                FormsAuthentication.Initialize()
                Dim authTicket As New FormsAuthenticationTicket(txtUsrId.Text, True, 60)
                Dim encryptedticket As String = FormsAuthentication.Encrypt(authTicket)
                Dim authcookie As New HttpCookie(FormsAuthentication.FormsCookieName, encryptedticket)

                COMPANYID = ValidateCompayID(txtUsrId.Text)
                If COMPANYID <> "0" Then
                    Session("COMPANYID") = COMPANYID

                    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CHECK_CATEGORY_DEPRECIATION")
                    Dim ds As New DataSet
                    ds = sp.GetDataSet()
                    Session("DepMethod") = ds.Tables(0).Rows(0).Item("returnstatus")

                    Response.Cookies.Add(authcookie)
                    Session("LoginTime") = getoffsetdatetime(DateTime.Now)
                    Session("uname") = staid
                    displaybsmdata()
                    GET_ASSET_MODULE_CHECK_FOR_USER()
                    GetuserRoleMappingEdit()

                    UserStatus = GetUserStatus(Session("UID"))
                    If UserStatus <> "" Then
                        Panel1.Visible = True
                        mp1.Show()
                    Else
                        Dim lastlogin As DateTime = savelastlogin(Session("UID"))
                        Session("Lastlogintime") = lastlogin.ToString("dd MMM yyyy HH:mm:ss")
                        Dim stl As String = Validatelicence(txtUsrId.Text, txtUsrPwd.Text)
                        If stl = "0" Then
                            Dim param As SqlParameter() = New SqlParameter(0) {}
                            param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar)
                            param(0).Value = txtUsrId.Text
                            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "PASSWORD_CHECK", param)

                            If ds.Tables(0).Rows(0).Item("RESULT") = "1" Then
                                Response.Redirect("~/Changepassword.aspx")
                            Else
                                Response.Redirect("~/frmAMTDefault.aspx")
                            End If
                            Response.Redirect("~/frmLicense.aspx")
                        End If
                    End If

                    'End checking the user licence 
                Else

                    lbl1.Text = "User is In-active"
                End If
            ElseIf staid = "1" Then
                lbl1.Text = "User already logged in."
            Else
                lbl1.Text = "Invalid Login!"
            End If
        Else
            lbl1.Text = "Invalid Company ID!"
        End If

    End Sub

    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Response.Redirect("~/login.aspx")
    End Sub
    Protected Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click
        Dim Status As String = ""
        Dim Mode As String = "2"
        Dim ds As New DataSet
        Dim param As SqlParameter() = New SqlParameter(1) {}
        param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
        param(0).Value = txtUsrId.Text
        param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
        param(1).Value = Mode
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_USER_STATUS", param)
        If ds.Tables.Count > 0 Then
            If ds.Tables(0).Rows.Count > 0 Then
                Status = ds.Tables(0).Rows(0).Item("SNO")
            Else
                Status = ""
            End If
        End If
        Response.Redirect("~/login.aspx")
    End Sub


    Public Function ValidateCompayID(ByVal User_ID As String) As String
        Dim Company_Id As String = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "ADM_CHECK_USER_COMPANY_ID")
        sp.Command.AddParameter("@AUR_ID", User_ID, DbType.String)
        Company_Id = sp.ExecuteScalar
        Return Company_Id
    End Function

    'Get the Parent, Child for business specific master
    Public Sub getuseroffsetandculture()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USER_OFFSET_CULTURE_BYID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("useroffset") = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            Session("userculture") = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Session("usercountry") = Convert.ToString(ds.Tables(0).Rows(0).Item("AUR_COUNTRY"))
            Session("location") = Convert.ToString(ds.Tables(0).Rows(0).Item("AUR_LOCATION"))
            'Dim useroffsetcookie As HttpCookie = New HttpCookie("useroffset")
            'useroffsetcookie.Value = ds.Tables(0).Rows(0).Item("AUR_TIME_ZONE")
            'Response.Cookies.Add(useroffsetcookie)
            Dim userculturecookie As HttpCookie = New HttpCookie("userculture")
            userculturecookie.Value = ds.Tables(0).Rows(0).Item("AUR_CULTURE")
            Response.Cookies.Add(userculturecookie)

        End If
    End Sub

    Public Sub displaybsmdata()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AMT_BSM_GETALL")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("Parent") = ds.Tables(0).Rows(0).Item("AMT_BSM_PARENT")
            Session("Child") = ds.Tables(0).Rows(0).Item("AMT_BSM_CHILD")
            'Session("GHT") = ds.Tables(1).Rows(0).Item("ASM_PARENT")
            If ds.Tables(1).Rows.Count > 0 Then
                Session("GHT") = ds.Tables(1).Rows(0).Item("ASM_PARENT")
            End If
        End If
    End Sub
    Public Sub GET_ASSET_MODULE_CHECK_FOR_USER()
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "GET_ASSET_MODULE_CHECK_FOR_USER")
        sp.Command.AddParameter("@TENANT_ID", Session("TENANT"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Dim asset = ds.Tables(0).Rows(0).Item("T_STA_ID")
            If asset = 1 Then
                Get_asset_sysp_value()
            End If
        End If
    End Sub


    Public Sub GetuserRoleMappingEdit()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_USERROLEMAPPINGEDIT")

        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("USER_EDIT") = ds.Tables(0).Rows(0).Item("SYSP_VAL1")
        End If
    End Sub
    Public Sub Get_asset_sysp_value()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_AST_SYSP_VALUE")
        Dim ds As New DataSet
        ds = sp.GetDataSet
        If ds.Tables(0).Rows.Count > 0 Then
            Session("Procurement") = ds.Tables(0).Rows(0).Item("AST_SYSP_VAL1")
        End If
    End Sub
    Public Function ValidateTenant(ByVal Tenant_Name As String) As String
        Dim Valid_Tenant_Id As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "CHECK_USER_TENANT_ISVALD")
        sp.Command.AddParameter("@TENANT_NAME", Tenant_Name, DbType.String)
        Session("useroffset") = "+05:30"
        Valid_Tenant_Id = sp.ExecuteScalar
        Return Valid_Tenant_Id
    End Function
    Public Function ValidateUser(ByVal UserName As String, ByVal Password As String) As String
        getuseroffsetandculture()
        Dim ValidateStatus As String = ""
        'ValidateStatus = SPs.ValidateUser(UserName, Password).ExecuteScalar
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "VALIDATE_USER")
        sp.Command.AddParameter("@USR_ID", UserName, DbType.String)
        sp.Command.AddParameter("@USR_LOGIN_PASSWORD", Password, DbType.String)
        ValidateStatus = sp.ExecuteScalar
        Return ValidateStatus
    End Function

    'Public Function savelastlogin(ByVal UserName As String) As DateTime
    '    Dim ValidateStatus As DateTime = getoffsetdatetime(DateTime.Now)
    '    Dim Mode As String = "1"
    '    'ValidateStatus = SPs.ValidateUser(UserName, Password).ExecuteScalar
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "SAVE_USER_LASTLOGIN")
    '    sp.Command.AddParameter("@USR_ID", UserName, DbType.String)
    '    sp.Command.AddParameter("@Mode", Mode, DbType.String)
    '    ValidateStatus = sp.ExecuteScalar()
    '    Return ValidateStatus
    'End Function
    Public Function savelastlogin(ByVal UserName As String) As DateTime
        Dim ValidateStatus As DateTime = getoffsetdatetime(DateTime.Now)
        Dim Mode As String = "3"
        Dim ds As New DataSet

        Try

            Dim param As SqlParameter() = New SqlParameter(2) {}
            param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
            param(0).Value = UserName
            param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
            param(1).Value = Mode
            param(2) = New SqlParameter("@TIMEOUT", SqlDbType.NVarChar)
            param(2).Value = AppSettings("TIMEOUT").ToString()
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SAVE_USER_LASTLOGIN", param)
            Session("LoginUniqueID") = ds.Tables(0).Rows(0).Item("SNO")
            ValidateStatus = ds.Tables(0).Rows(0).Item("LASTLOGINTIME")
        Catch ex As Exception
        Finally

        End Try
        Return ValidateStatus
    End Function

    Public Function savelastloginmob(ByVal UserName As String) As DateTime
        Dim ValidateStatus As DateTime = getoffsetdatetime(DateTime.Now)
        Dim Mode As String = "4"
        Dim ds As New DataSet

        Try

            Dim param As SqlParameter() = New SqlParameter(2) {}
            param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
            param(0).Value = UserName
            param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
            param(1).Value = Mode
            param(2) = New SqlParameter("@TIMEOUT", SqlDbType.NVarChar)
            param(2).Value = AppSettings("TIMEOUT").ToString()
            ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "SAVE_USER_LASTLOGIN", param)
            Session("LoginUniqueID") = ds.Tables(0).Rows(0).Item("SNO")
            ValidateStatus = ds.Tables(0).Rows(0).Item("LASTLOGINTIME")
        Catch ex As Exception
        Finally

        End Try
        Return ValidateStatus
    End Function


    Public Function Validatelicence(ByVal UserName As String, ByVal Password As String) As String
        Dim ValidateStatus As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "VALIDATE_USER_EXPIRY")
        sp.Command.AddParameter("@TID", Session("Tenant"), DbType.String)
        ValidateStatus = sp.ExecuteScalar
        Return ValidateStatus
    End Function

    Private tm As New SoftwareLocker.TrialMaker("Amantra", "Amantra", "1234")
    Public ClaimsList As List(Of Claim)


    Private ReadOnly Property AuthenticationManager As IAuthenticationManager
        Get
            Return Request.GetOwinContext().Authentication
        End Get
    End Property

    Public Shared Function CreateLogFile(ByVal message As String) As Boolean
        Try
            'string location = @"C://IRPC//myfile1.txt";
            Dim filename As String = "NewLog_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt"
            Dim location As String = HttpContext.Current.Server.MapPath(Convert.ToString("~/ErrorLogFiles/") & filename) '''(System.Environment.CurrentDirectory + ("\log.txt"))
            If Not File.Exists(location) Then
                Dim fs As FileStream
                fs = New FileStream(location, FileMode.Append, FileAccess.Write, FileShare.ReadWrite)
                fs.Close()
            End If

            Console.WriteLine(message)
            'Release the File that is created
            Dim sw As StreamWriter = New StreamWriter(location, True)
            sw.Write((message + Environment.NewLine))
            sw.Close()
            sw = Nothing
            Return True
        Catch ex As Exception
            EventLog.WriteEntry("MIDocShare", ("Error in CreateLogFile" + ex.Message.ToString), EventLogEntryType.Error, 6000)
            Return False
        End Try

    End Function
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            If (Not IsPostBack) Then

                CreateLogFile("SSo Start.!")
                Session.Clear()
                CreateLogFile("SSO 0")

                'If tm.CheckRegister() = False Then
                '    pnlLic.Visible = True
                '    lblKey.Text = tm.BaseString
                '    pnlMain.Visible = False
                '    Exit Sub
                'Else
                '    pnlLic.Visible = False
                '    pnlMain.Visible = True

                'End If

                'Dim SessionUserid As String
                Dim Sessioncompany As String
                'SessionUserid = Page.Request.QueryString("sessiontoken")
                Sessioncompany = Page.Request.QueryString("company")
                CreateLogFile("SSO Step 1:: CompanyID:-" + Sessioncompany)

                If (Sessioncompany = "Apexon") Then
                    Dim empid As String = Page.Request.QueryString("empid")
                    Dim empmail As String = Page.Request.QueryString("email")
                    Dim Tenant_Id1 As String = ValidateTenant(Sessioncompany)
                    CreateLogFile("SSO Step 2:: empid- " + empid + " empmail:-" + empmail + " Tenant_Id1:-" + Tenant_Id1)
                    Session("TENANT") = Tenant_Id1
                    Session("UID") = empid
                    'txtUsrId.Text = Session("UID")
                    CreateLogFile("351  TENANT- " + Session("TENANT").ToString() + " UID :-" + Session("UID").ToString())

                    Session("useroffset") = "+05:30"
                    Dim uname = Session("UID")
                    Session("uname") = Session("UID")
                    Dim staid As String = ""

                    Dim COMPANYID As String
                    Dim Tenant_Active As Integer

                    ''Tenant_Active = ValidateIsTenantActive(Request.QueryString("tenant"))
                    Tenant_Active = ValidateIsTenantActive(Session("TENANT").ToString())
                    CreateLogFile("360 Tenant_Active : " + Tenant_Active.ToString())
                    If Tenant_Active = "0" Then
                        lbl1.Text = "Account has been Inactive!"
                        pnlLic.Visible = False
                        pnlMain.Visible = True
                        Exit Sub
                    End If

                    Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "Get_UserID_By_Email")
                    ''sp1.Command.AddParameter("@Mail", empmail, DbType.String)
                    sp1.Command.AddParameter("@Mail", empid, DbType.String)
                    Dim ds1 As New DataSet
                    ds1 = sp1.GetDataSet()
                    If (ds1.Tables(0).Rows.Count = "0") Then
                        'Server.Transfer("~/login.aspx")
                        lbl1.Text = "User Not exists in tool"
                        CreateLogFile("User Not exists in tool")
                        Exit Sub
                    Else
                        CreateLogFile("User exists")
                        Session("UID") = ds1.Tables(0).Rows(0).Item("AUR_ID")
                        uname = Session("UID")
                        Session("uname") = Session("UID")
                        FormsAuthentication.Initialize()
                        Dim lastlogin As DateTime = savelastlogin(Session("UID"))
                        Dim authTicket As New FormsAuthenticationTicket(txtUsrId.Text, True, 60)
                        Dim encryptedticket As String = FormsAuthentication.Encrypt(authTicket)
                        Dim authcookie As New HttpCookie(FormsAuthentication.FormsCookieName, encryptedticket)
                        authcookie.Path = ""
                        COMPANYID = ValidateCompayID(uname)
                        CreateLogFile("CompanyID :: - " + COMPANYID.ToString())
                        If (COMPANYID <> "NoUser") Then

                            CreateLogFile("CompanyID :: - " + COMPANYID.ToString() + " UID:-" + Session("UID").ToString())

                            getuseroffsetandculture()
                            Session("COMPANYID") = COMPANYID

                            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "AM_CHECK_CATEGORY_DEPRECIATION")
                            Dim ds As New DataSet
                            ds = sp.GetDataSet()
                            Session("DepMethod") = ds.Tables(0).Rows(0).Item("returnstatus")

                            CreateLogFile("DepMethod :: - " + Session("DepMethod").ToString())

                            Response.Cookies.Add(authcookie)
                            Session("LoginTime") = getoffsetdatetime(DateTime.Now)

                            CreateLogFile("LoginTime :: - " + Session("LoginTime").ToString())

                            displaybsmdata()
                            GET_ASSET_MODULE_CHECK_FOR_USER()
                            GetuserRoleMappingEdit()

                            Dim stl As String = Validatelicence()

                            If stl = "0" Then
                                CreateLogFile("stl :: - " + stl.ToString())
                                Response.Redirect("~/frmAMTDefault.aspx", False)
                            Else
                                Server.Transfer("~/frmLicense.aspx")
                            End If
                        Else
                            Server.Transfer("~/UserNotExists.aspx")
                        End If
                    End If
                End If


            End If

        Catch ex As Exception
            logerrors("Exception :" + ex.ToString())
            CreateLogFile("Error " + ex.Message)
            'Dim errh As New ErrorHandler()
            'errh._WriteErrorLog(ex.ToString())
        End Try
    End Sub
    Public Function Validatelicence() As String
        Dim ValidateStatus As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "VALIDATE_USER_EXPIRY")
        sp.Command.AddParameter("@TID", Session("Tenant"), DbType.String)
        ValidateStatus = sp.ExecuteScalar
        Return ValidateStatus
    End Function
    Protected Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        If tm.CheckSerial(txtSerialKey.Text) = True Then
            tm.RegisterPassword(txtSerialKey.Text)
            pnlLic.Visible = False
            pnlMain.Visible = True
            lbl1.Visible = False
        Else
            lbl1.Visible = True
            lbl1.Text = "Please enter valid Activation Code"
            pnlLic.Visible = True
            pnlMain.Visible = False

        End If
    End Sub

    Public Function GetUserStatus(ByVal UserName As String) As String
        Dim Status As String = ""
        Dim Mode As String = "1"

        Dim ds As New DataSet

        Dim param As SqlParameter() = New SqlParameter(1) {}
        param(0) = New SqlParameter("@USR_ID", SqlDbType.NVarChar)
        param(0).Value = UserName
        param(1) = New SqlParameter("@Mode", SqlDbType.NVarChar)
        param(1).Value = Mode
        ds = SqlHelper.ExecuteDataset(CommandType.StoredProcedure, "GET_USER_STATUS", param)

        If ds.Tables(0).Rows.Count > 0 Then
            Status = ds.Tables(0).Rows(0).Item("SNO")
        Else
            Status = ""
        End If
        Return Status
    End Function

    Public Function ValidateIsTenantActive(ByVal Tenant_Name As String) As String
        Dim ValidateTenantStatus As String = ""
        Dim sp As New SubSonic.StoredProcedure(AppSettings("FRMDB") & "." & "VALIDATE_TENANT_ACTIVE")
        sp.Command.AddParameter("@TID", Session("Tenant"), DbType.String)
        ValidateTenantStatus = sp.ExecuteScalar
        Return ValidateTenantStatus
    End Function

End Class
