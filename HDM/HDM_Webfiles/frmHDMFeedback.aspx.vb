Imports System.Data
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Data.SqlClient
Imports System.Configuration
Partial Class HDM_HDM_Webfiles_frmHDMFeedback
    Inherits System.Web.UI.Page

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        If Page.IsValid = True Then
            AddFeedback()
            lblMsg.Text = "Feedback Added Successfully"
            BindGrid()
            Cleardata()
        End If
    End Sub

    Private Sub Cleardata()
        txtreq.Text = ""
        txtReqId.Text = ""
        txtreqdesc.Text = ""
        txtcomments.Text = ""
        ddlfeedback.SelectedIndex = 0
    End Sub

    Private Sub AddFeedback()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_FEEDBACK_ADD")
        sp.Command.AddParameter("@REQUEST_ID", txtReqId.Text, DbType.String)
        sp.Command.AddParameter("@feedback", txtcomments.Text, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@TEST", ddlfeedback.SelectedItem.Value, DbType.String)
        sp.ExecuteScalar()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack() Then
            BindRequstDetails()
            BindGrid()
        End If
        lblMsg.Text = ""
    End Sub

    Private Sub BindRequstDetails()
        Dim ReqId As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_FEEDBACK_BIND_REQUEST_DETAILS")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String) 'Request.QueryString("id")
        Dim ds As DataSet = sp.GetDataSet()
        txtreq.Text = ds.Tables(0).Rows(0).Item("AUR_KNOWN_AS")
        txtReqId.Text = ReqId
        txtreqdesc.Text = ds.Tables(0).Rows(0).Item("SER_dESCRIPTION")
    End Sub

    Private Sub BindGrid()
        Dim ReqId As String = Request("RID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_FEEDBACK_BIND_GRID_DETAILS")
        sp.Command.AddParameter("@REQ_ID", ReqId, DbType.String)
        gvitems.DataSource = sp.GetDataSet()
        gvitems.DataBind()
        If gvitems.Rows.Count > 0 Then
            txtcomments.Enabled = False
            ddlfeedback.Enabled = False
            btnsubmit.Enabled = False
        End If
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        BindGrid()
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
        Response.Redirect("frmHDMViewRequisitions.aspx")

    End Sub

End Class
