﻿#Region "NameSpaces"

Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

#End Region

Partial Class HDM_HDM_Webfiles_Reports_frmHDMReportByUser
    Inherits System.Web.UI.Page

#Region "Methods"

    Private Sub GetRequestCount()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_REQUESTS_COUNT")
        Dim dummy As String = sp.Command.CommandSql
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        hyOpen.Text = " Pending Requests   " & ds.Tables(0).Rows(0).Item("PENDING")
        hyProgress.Text = " In-Progress Requests   " & ds.Tables(1).Rows(0).Item("INPROGRESS")
        hyClosed.Text = " Closed Requests   " & ds.Tables(2).Rows(0).Item("CLOSED")
    End Sub

#End Region

#Region "Export"

    Public Shared Sub Export(ByVal fileName As String, ByVal gv As GridView)
        HttpContext.Current.Response.Clear()
        HttpContext.Current.Response.AddHeader("content-disposition", String.Format("attachment; filename={0}", fileName))
        HttpContext.Current.Response.ContentType = "application/ms-excel"
        Dim sw As StringWriter = New StringWriter
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)
        '  Create a form to contain the grid
        Dim table As Table = New Table
        table.GridLines = gv.GridLines
        '  add the header row to the table
        If (Not (gv.HeaderRow) Is Nothing) Then
            PrepareControlForExport(gv.HeaderRow)
            table.Rows.Add(gv.HeaderRow)
        End If
        '  add each of the data rows to the table
        For Each row As GridViewRow In gv.Rows
            PrepareControlForExport(row)
            table.Rows.Add(row)
        Next
        '  add the footer row to the table
        If (Not (gv.FooterRow) Is Nothing) Then
            PrepareControlForExport(gv.FooterRow)
            table.Rows.Add(gv.FooterRow)
        End If
        '  render the table into the htmlwriter
        table.RenderControl(htw)
        '  render the htmlwriter into the response
        HttpContext.Current.Response.Write(sw.ToString)
        HttpContext.Current.Response.End()

    End Sub

    ' Replace any of the contained controls with literals
    Private Shared Sub PrepareControlForExport(ByVal control As Control)
        Dim i As Integer = 0
        Do While (i < control.Controls.Count)
            Dim current As Control = control.Controls(i)
            If (TypeOf current Is LinkButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, LinkButton).Text))
            ElseIf (TypeOf current Is ImageButton) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, ImageButton).AlternateText))
            ElseIf (TypeOf current Is HyperLink) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, HyperLink).Text))
            ElseIf (TypeOf current Is DropDownList) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, DropDownList).SelectedItem.Text))
            ElseIf (TypeOf current Is TextBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, TextBox).Text))
            ElseIf (TypeOf current Is CheckBox) Then
                control.Controls.Remove(current)
                control.Controls.AddAt(i, New LiteralControl(CType(current, CheckBox).Checked))
                'TODO: Warning!!!, inline IF is not supported ?
            End If
            If current.HasControls Then
                PrepareControlForExport(current)
            End If
            i = (i + 1)
        Loop
    End Sub

#End Region

#Region "PageEventHandlers"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        txtfromdate.Attributes.Add("readonly", "readonly")
        txttodate.Attributes.Add("readonly", "readonly")
        If Not IsPostBack() Then
            'temp.Visible = False
            'BindDepartment()
            GetRequestCount()
            Dim Status As String = Request("RID")
            If Status = 0 Then
                BindGrid()
                'Else
                '    BindGrid(Status)
            End If
            'txtfromdate.Attributes.Add("onClick", "displayDatePicker('" + txtfromdate.ClientID + "')")
            'txtfromdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
            'txttodate.Attributes.Add("onClick", "displayDatePicker('" + txttodate.ClientID + "')")
            'txttodate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
        End If
    End Sub

    'Protected Sub ddlassigned_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlassigned.SelectedIndexChanged
    '    If ddlassigned.SelectedValue = "Employee" Then
    '        ' temp.Visible = True
    '    Else
    '        'temp.Visible = False
    '    End If
    '    txtempid.Text = ""
    'End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        'Response.Redirect("~/HDM/HDM_Webfiles/Reports/frmHDMStatusDetailsReport.aspx?" + "RID=0" + "&FROMDATE=" + txtfromdate.Text + "&TODATE=" + txttodate.Text + "&EMPID=" + txtempid.Text)
        Dim Status As String = Request("RID")
        If Status = 0 Then
            BindGrid()
            'Else
            '    BindGrid(Status)
        End If

    End Sub
    Private Sub BindGrid()
        Dim Stat As String = Request("RID")
        Dim FROMDATE As String = Request("FROMDATE")
        Dim TODATE As String = Request("TODATE")
        Dim EMPID As String = Request("EMPID")
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "HDM_GET_REPORT_BY_STATUS_ALL")
        'Dim dummy As String = sp.Command.CommandSql
        sp.Command.AddParameter("@STATUS", 0, DbType.String)
        sp.Command.AddParameter("@FROMDATE", txtfromdate.Text, DbType.String)
        sp.Command.AddParameter("@TODATE", txttodate.Text, DbType.String)
        sp.Command.AddParameter("@EMPID", txtempid.Text, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If Request("RID") = 0 Then
            gvViewRequisitions.Columns(7).Visible = True
        End If
        gvViewRequisitions.DataSource = ds
        gvViewRequisitions.DataBind()
        For i As Integer = 0 To gvViewRequisitions.Rows.Count - 1
            Dim lblstatusId As Label = CType(gvViewRequisitions.Rows(i).FindControl("lblStatusId"), Label)
            Dim lblStatus As Label = CType(gvViewRequisitions.Rows(i).FindControl("lblStatus"), Label)
            If lblstatusId.Text = "1" Then
                lblStatus.Text = "Request Submitted"
            ElseIf lblstatusId.Text = "2" Then
                lblStatus.Text = "Request Modified"
            ElseIf lblstatusId.Text = "3" Then
                lblStatus.Text = "Request Cancelled"
            ElseIf lblstatusId.Text = "4" Then
                lblStatus.Text = "RM Approved"
            ElseIf lblstatusId.Text = "5" Then
                lblStatus.Text = "RM Rejected"
            ElseIf lblstatusId.Text = "6" Then
                lblStatus.Text = "On Hold"
            ElseIf lblstatusId.Text = "7" Then
                lblStatus.Text = "In Progress"
            ElseIf lblstatusId.Text = "8" Then
                lblStatus.Text = "Closed"
            ElseIf lblstatusId.Text = "9" Then
                lblStatus.Text = "Assigned"
            End If
        Next
    End Sub
    Protected Sub gvViewRequisitions_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvViewRequisitions.PageIndexChanging
        gvViewRequisitions.PageIndex = e.NewPageIndex
        Dim Status As String = Request("RID")
        If Status = 0 Then
            BindGrid()
            'Else
            '    BindGrid(Status)
        End If
        'BindGrid(Status)
    End Sub

#End Region

End Class
