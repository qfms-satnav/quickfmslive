﻿#Region "NameSpaces"

Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Configuration
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Commerce.Common
Imports clsSubSonicCommonFunctions

#End Region

Partial Class HDM_HDM_Webfiles_Reports_frmHDMViewCategoryDetailReport
    Inherits System.Web.UI.Page

#Region "Variable Declarations"

    Dim ObjSubsonic As New clsSubSonicCommonFunctions

#End Region

#Region "User Defined Methods"

    Private Function GetCurrentUser() As Integer
        If String.IsNullOrEmpty(Session("UID")) Then
            Return 0
        Else
            Return CInt(Session("UID"))
        End If
    End Function

    Private Sub BindRequisition()

        Dim ReqId As String = Request("id")
        Dim RaisedBy As Integer = 0
        If String.IsNullOrEmpty(ReqId) Then
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_REQUISITION_GetByReqId_ReportByUSer")
            sp.Command.AddParameter("@ReqId", ReqId, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            lblReqId.Text = ReqId
            BindLocations()
            ddlLocation.ClearSelection()
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_BDG_ID")).Selected = True
            BindCategories(ddlLocation.SelectedItem.Value)
            ddlServiceCategory.ClearSelection()
            ddlServiceCategory.Items.FindByValue(ds.Tables(0).Rows(0).Item("REQ_TYPE")).Selected = True
            BindServiceType(ddlLocation.SelectedItem.Value, ddlServiceCategory.SelectedItem.Value)
            ddlServiceType.ClearSelection()
            ddlServiceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_REQ_FOR")).Selected = True
            txtRemarks.Text = ds.Tables(0).Rows(0).Item("SER_DESCRIPTION")
            txtconvdate.Text = ds.Tables(0).Rows(0).Item("CONVINIENT_DATE")
            txtEmployee.Text = ds.Tables(0).Rows(0).Item("SER_AUR_ID")
            txtServReqAt.Text = ds.Tables(0).Rows(0).Item("SER_REQUIRED_AT")
            cboHr.ClearSelection()
            cboHr.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONV_HOURS")).Selected = True
            ddlMins.ClearSelection()
            ddlMins.Items.FindByValue(ds.Tables(0).Rows(0).Item("CONV_MINS")).Selected = True
            txtRMRemarks.Text = ds.Tables(0).Rows(0).Item("SER_RM_REMARKS")
            txtAdminRemarks.Text = ds.Tables(0).Rows(0).Item("SER_ADM_REMARKS")
        End If
    End Sub

    Private Sub BindUsers(ByVal AUR_ID As String)
        Dim param(0) As SqlParameter
        param(0) = New SqlParameter("@AUR_ID", SqlDbType.NVarChar, 200)
        param(0).Value = AUR_ID
        ObjSubsonic.Binddropdown(ddlEmp, "HDM_BINDUSERS_NAME", "NAME", "AUR_ID", param)
        Dim li As ListItem = ddlEmp.Items.FindByValue(AUR_ID)
        If Not li Is Nothing Then
            li.Selected = True
        End If
    End Sub

    Private Sub BindCategories(lcm_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GETACTIVESERREQUESTTYPE")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        ddlServiceCategory.DataSource = sp.GetDataSet()
        ddlServiceCategory.DataTextField = "SER_NAME"
        ddlServiceCategory.DataValueField = "SER_CODE"
        ddlServiceCategory.DataBind()
        ddlServiceCategory.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindServiceType(lcm_code As String, ser_cat As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_SERTYPEBYCATLOCATION")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        sp.Command.AddParameter("@SER_CODE", ser_cat, DbType.String)
        ddlServiceType.DataSource = sp.GetDataSet()
        ddlServiceType.DataTextField = "SER_TYPE_NAME"
        ddlServiceType.DataValueField = "SER_TYPE_CODE"
        ddlServiceType.DataBind()
        ddlServiceType.Items.Insert(0, "--Select--")
    End Sub

    Private Sub BindLocations()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
    End Sub

    Private Function GetRequestId() As String
        Dim ReqId As String = Request("RID")
        Return ReqId
    End Function

#End Region

#Region "Page Event Handlers"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("uid") = "" Then
            Response.Redirect(Application("FMGLogout"))
        Else
            If Not IsPostBack Then
                BindLocations()
                Dim UID As String = Session("uid")
                BindUsers(UID)
                BindRequisition()
            End If
        End If
        txtconvdate.Attributes.Add("onClick", "displayDatePicker('" + txtconvdate.ClientID + "')")
        txtconvdate.Attributes.Add("onKeyPress", "javascript:return Date_KeyPress();")
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("../../../EFM/EFM_WEBfiles/MaintenanceReport2.aspx")
    End Sub

    Public Function GetServiceStart() As DataSet
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_SERSTARTTIME")
        sp.Command.AddParameter("@SER_TYPE_LCMID", ddlLocation.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_TYPE_CODE", ddlServiceType.SelectedItem.Value, DbType.String)
        sp.Command.AddParameter("@SER_TYPE_CATID", ddlServiceCategory.SelectedItem.Value, DbType.String)
        Dim dsSTime As New DataSet()
        dsSTime = sp.GetDataSet()
        Return dsSTime
    End Function

#End Region

End Class
