﻿app.service("HDMTATReportService", ['$http', '$q', 'UtilityService',function ($http, $q, UtilityService) {
    this.LoadGrid = function (HDMTAT) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMTATReport/GetGrid', HDMTAT)
            .then(function (response) {
                deferred.resolve(response.data);
                response.data.forEach(function (d) {
                    d.STA_TITLE = `<span style="color:${d.STA_COLOR}">${d.STA_TITLE}</span>`;
                });
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetDetailsOnSelection = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMTATReport/GetDetailsOnSelection/', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.getRequestTypes = function () {
        deferred = $q.defer();
        $http.get(UtilityService.path + '/api/HDMTATReport/GetRequestTypes')
            .then(function (response) {
                deferred.resolve(response.data);
            }, function (response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

}]);

app.controller('HDMTATReportController', ['$scope','$q', '$http', 'HDMTATReportService', 'UtilityService', '$timeout', function ($scope, $q, $http, HDMTATReportService, UtilityService, $timeout) {
    $scope.HDMTAT = {};
    $scope.GridVisiblity = true;
    $scope.Spcdata = {};
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.REQUEST_TYPE_For_PopUP = "";
    $scope.RequestTypes = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    var OVERALL_COUNT = 0;

    $scope.columnDefs = [
        {
            headerName: "Request ID", field: "REQ_ID", width: 200, cellClass: 'grid-align',
            template: '<a ng-click="ShowPopup(data)">{{data.REQ_ID}}</a>', filter: 'text', pinned: 'left', suppressMenu: true
        },
        { headerName: "Description", field: "DESCRIPTION", cellClass: 'grid-align', width: 500 },
        { headerName: "Status", field: "STA_TITLE", cellClass: 'grid-align', width: 300 },
    ];

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.HDMTAT.CNP_NAME.push(a);
                });

                if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
            }

        });
    }, 500);
    var searchval = $("#filtertxt").val();
    
    $scope.LoadData = function () {
        if (moment($scope.HDMTAT.FromDate) > moment($scope.HDMTAT.ToDate)) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        $("#btLast").hide();
        $("#btFirst").hide();
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    FromDate: $scope.HDMTAT.FromDate,
                    ToDate: $scope.HDMTAT.ToDate,
                    Request_Type: $scope.HDMTAT.Request_Type,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CompanyId: $scope.HDMTAT.CNP_NAME[0].CNP_ID
                };
                var fromdate = moment($scope.HDMTAT.FromDate);
                var todate = moment($scope.HDMTAT.ToDate);
                if (fromdate > todate) {
                    $scope.GridVisiblity = false;
                    showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                    progress(0, 'Loading...', false);
                }
                else {
                    progress(0, 'Loading...', true);
                    HDMTATReportService.LoadGrid(params).then(function (data) {
                        $scope.gridata = data;
                        if ($scope.gridata == null) {
                            //  params.failCallback();
                            $("#btNext").attr("disabled", true);
                            $scope.gridOptions.api.setRowData([]);
                            $scope.GridVisiblity = false;
                        }
                        else {
                            $scope.GridVisiblity = true;
                            //$scope.gridOptions.api.setRowData([]);
                            //params.successCallback(data, lastRow);
                            $scope.gridOptions.api.setRowData($scope.gridata);
                            $scope.gridOptions.api.sizeColumnsToFit();
                            OVERALL_COUNT = $scope.gridata[0].OVERALL_COUNT;
                        }
                        progress(0, 'Loading...', false);
                    });
                }
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    //$scope.LoadData = function () {
    //    var params = {
    //        FromDate: $scope.HDMTAT.FromDate,
    //        ToDate: $scope.HDMTAT.ToDate,
    //        Request_Type: $scope.HDMTAT.Request_Type,
    //        CompanyId: $scope.HDMTAT.CNP_NAME[0].CNP_ID
    //    };

    //    var fromdate = moment($scope.HDMTAT.FromDate);
    //    var todate = moment($scope.HDMTAT.ToDate);
    //    if (fromdate > todate) {
    //        $scope.GridVisiblity = false;
    //        showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
    //        progress(0, 'Loading...', false);
    //    }
    //    else {
    //        progress(0, 'Loading...', true);
    //        $scope.GridVisiblity = true;
    //        HDMTATReportService.LoadGrid(params).then(function (data) {
    //            $scope.gridata = data;
    //            if ($scope.gridata == null) {
    //                progress(0, 'Loading...', false);
    //                $scope.gridOptions.api.setRowData([]);
    //            }
    //            else {
    //                $scope.gridOptions.api.setRowData($scope.gridata);
    //            }
    //            progress(0, 'Loading...', false);
    //        }, function (error) {
    //            console.log(error);
    //        });
    //    }
    //}

    HDMTATReportService.getRequestTypes().then(function (response) {
        if (response.data != null) {
            $scope.RequestTypes = response.data;
        }
    });

    $scope.GetTAT = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        progress(0, 'Loading...', true);
        //$scope.rptDateRanges();
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                $scope.Pageload = {
                    FromDate: $scope.HDMTAT.FromDate,
                    ToDate: $scope.HDMTAT.ToDate,
                    Request_Type: $scope.HDMTAT.Request_Type,
                    CompanyId: $scope.HDMTAT.CNP_NAME[0].CNP_ID,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.HDMTAT.CNP_NAME[0].CNP_ID,
                    SearchValue: searchval
                };
                HDMTATReportService.LoadGrid($scope.Pageload).then(function (data) {
                    $scope.gridata = data;
                    if ($scope.gridata == null || $scope.gridata == "") {
                        $("#btNext").attr("disabled", true);
                        $scope.gridOptions.api.setRowData([]);
                        progress(0, '', false);
                    }
                    else {
                        progress(0, '', false);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        OVERALL_COUNT = $scope.gridata[0].OVERALL_COUNT;
                    }
                }, function (error) {
                    console.log(error);
                    progress(0, '', false);
                });
            }
        }
        
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    var PopDefs = [
        { headerName: "Request ID", field: "REQ_ID", width: 190, cellClass: 'grid-align', suppressMenu: false, },
        { headerName: "Requested By", field: "REQUESTED_BY", width: 190, cellClass: 'grid-align' },
        { headerName: "Requested Date", field: "REQUESTED_DATE", template: '<span>{{data.REQUESTED_DATE | date:"dd-MM-yyyy - HH:mm:ss"}}</span>' ,width: 190, cellClass: 'grid-align', suppressMenu: false, },
        { headerName: "Assigend To", field: "ASSIGNED_TO", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: false, },
        { headerName: "Comments", field: "DESCRIPTION", width: 190, cellClass: 'grid-align', hide: false, suppressMenu: false, },
        { headerName: "Status", field: "REQ_STATUS", cellClass: 'grid-align', width: 200 },
        { headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align', hide: false, suppressMenu: false, },
        { headerName: "Total Time", field: "TOTAL_TIME", width: 100, cellClass: 'grid-align', hide: false, suppressMenu: false, },
        { headerName: "Delayed Time", field: "DELAYED_TIME", width: 110, cellClass: 'grid-align', hide: false, suppressMenu: false, },
        { headerName: "Closed Time", field: "CLOSED_TIME", template: '<span>{{data.CLOSED_TIME | date:"dd-MM-yyyy - HH:mm:ss"}}</span>' , width: 150, cellClass: 'grid-align', suppressMenu: false, },
    ];

    $scope.PopOptions = {
        columnDefs: PopDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        rowData: null,
        onReady: function () {
            $scope.PopOptions.api.sizeColumnsToFit()
        }
    }

    $scope.ShowPopup = function (data) {
        $scope.SelValue = data;
        $scope.REQUEST_TYPE_For_PopUP = data.REQUEST_TYPE;
        $scope.CurrentProfile = [];
        $("#historymodal").modal('show');
    }

    $('#historymodal').on('shown.bs.modal', function () {
        HDMTATReportService.GetDetailsOnSelection($scope.SelValue).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.popdata = response.data;
            $scope.PopOptions.api.setRowData($scope.popdata);
            progress(0, '', false);
        });
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Request ID", key: "REQ_ID" }, { title: "Description", key: "DESCRIPTION" }, { title: "Status", key: "STA_TITLE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("HDMTATReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "HDMTATReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (spcdata, Type) {
        if (moment($scope.HDMTAT.FromDate) > moment($scope.HDMTAT.ToDate)) {
            $scope.GridVisiblity = false;
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        //spcdata.Type = Type;
        hdobj = {};
        hdobj.CompanyId = spcdata.CNP_NAME[0].CNP_ID;
        hdobj.FromDate = spcdata.FromDate;
        hdobj.ToDate = spcdata.ToDate;
        hdobj.Type = Type;
        hdobj.Request_Type = spcdata.Request_Type;
        hdobj.SearchValue = searchval;
        hdobj.PageNumber = 1;
        hdobj.PageSize =OVERALL_COUNT;

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (spcdata.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/HDMTATReport/GetSpaceRequisitionReportdata',
                method: 'POST',
                data: hdobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    window.navigator.msSaveOrOpenBlob(file, 'HDMTATReport.' + Type);
                }
                else if ((navigator.userAgent.indexOf("Firefox") != -1)) {
                    window.navigator.msSaveOrOpenBlob(file, 'HDMTATReport.' + Type);
                }
                else {
                    var fileURL = URL.createObjectURL(file);
                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'HDMTATReport.' + Type;
                    document.body.appendChild(a);
                    a.click();
                    progress(0, 'Loading...', false);
                }
            }), function (error,data, status, headers, config) { 

            };
        };
    }


    $scope.selVal = "THISMONTH";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'SELECT':
                $scope.HDMTAT.FromDate = "";
                $scope.HDMTAT.ToDate = "";
                break;
            case 'TODAY':
                $scope.HDMTAT.FromDate = moment().format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.HDMTAT.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.HDMTAT.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.HDMTAT.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.HDMTAT.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.HDMTAT.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.HDMTAT.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }
    $scope.HDMTAT.Request_Type = "ALL";

    setTimeout(function () {
        $scope.GetTAT();
    }, 800);
    setTimeout(function () {
    $scope.rptDateRanges();
    }, 500);
}]);