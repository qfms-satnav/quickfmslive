﻿app.service("HDMSubRequestReportService", function ($http, $q, UtilityService) {
    this.GetReportGrid = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/HDMSubRequestReport/GetReportGrid')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetGriddataBySearch = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMSubRequestReport/GetGriddataBySearch', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});


app.controller('HDMSubRequestReportController', function ($scope, $q, $http, HDMSubRequestReportService, HDMViewSubRequisitionsService, UtilityService, $timeout, $filter, $window) {
    $scope.SubRequestReport = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = true;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Cols = [];
    $scope.CompanyVisible = 0;
    $scope.columnDefs = [];
    var RequestId;
    // fill company ddls



    // }

    var columnDefs = [
        { headerName: "Requisition Id", field: "REQUEST_ID", width: 130, cellClass: 'grid-align', },
        { headerName: "Requested Date", field: "REQUESTED_DATE", width: 140, cellClass: 'grid-align', },
        { headerName: "Requested By", field: "REQUESTED_BY", width: 150, cellClass: 'grid-align', },
        { headerName: "Location", field: "LOCATION", width: 120, cellClass: 'grid-align', },
        { headerName: "Main Category", field: "MAIN_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Sub Category", field: "SUB_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Child Category", field: "CHILD_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Updated By", field: "UPDATED_BY", width: 150, cellClass: 'grid-align', },
        { headerName: "Updated Date", field: "UPDATED_DT", width: 130, cellClass: 'grid-align', },
        { headerName: "Status", field: "STA_TITLE", width: 100, cellClass: 'grid-align', },
        { headerName: "Billed By", field: "BILLED_BY", width: 100, cellClass: 'grid-align', },
        { headerName: "Customer Id", field: "CUST_ID", width: 130, cellClass: 'grid-align', },
        { headerName: "Customer Name", field: "CUST_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Invoice No", field: "INV_NO", width: 130, cellClass: 'grid-align', },
        { headerName: "Descripition", field: "SER_PROB_DESC_SUB1", width: 130, cellClass: 'grid-align', },
        { headerName: "Comments", field: "UPDATED_COM", width: 130, cellClass: 'grid-align', },
    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Main Category", field: "Main_Category",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.SubRequestReport.selVal = "30";
    
    $scope.SubRequestReport.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
    $scope.SubRequestReport.ToDate = moment().format('DD-MMM-YYYY');
    $scope.SubRequestReport.selstatus = "1";
    $scope.STATUSCHANGE = function () {
        switch ($scope.SubRequestReport.selstatus) {
            case '1':
                $scope.SubRequestReport.selstatus = "1";
                break;
            case '2':
                $scope.SubRequestReport.selstatus = "2";
                break;
            case '3':
                $scope.SubRequestReport.selstatus = "3";
                break
        }
    }
    $scope.selVal = "30";
    $scope.rptDateRanges = function () {
        switch ($scope.SubRequestReport.selVal) {
            case 'SELECT':
                $scope.SubRequestReport.FromDate = "";
                $scope.SubRequestReport.ToDate = "";
                break;
            case 'TODAY':
                $scope.SubRequestReport.FromDate = moment().format('DD-MMM-YYYY');
                $scope.SubRequestReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.SubRequestReport.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.SubRequestReport.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break
            case '7':
                $scope.SubRequestReport.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.SubRequestReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.SubRequestReport.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.SubRequestReport.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.SubRequestReport.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.SubRequestReport.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.SubRequestReport.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.SubRequestReport.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;
        }
    }

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    var clk = true;
    var ExportColumns;
    $scope.LoadData = function () {
        var unticked = _.filter($scope.Cols, function (item) {
            return item.ticked == false;
        });
        var ticked = _.filter($scope.Cols, function (item) {
            return item.ticked == true;
        });

        if ($scope.SubRequestReport.FromDate == undefined && $scope.SubRequestReport.ToDate == undefined) {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1)) + '/' + firstDay.getDate() + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1)) + '/' + lastDay.getDate() + '/' + lastDay.getFullYear();

            $scope.SubRequestReport.FromDate = firstDayWithSlashes;
            $scope.SubRequestReport.ToDate = lastDayWithSlashes;
        }
        var params = {
            RequestID: $scope.SubRequestReport.RequestID,
            LCMlst: $scope.SubRequestReport.Locations,
            CHILDlst: $scope.SubRequestReport.child_Category,
            STATUSlst: $scope.SubRequestReport.status,
            HSTSTATUS: $scope.SubRequestReport.selstatus,
            FromDate: $scope.SubRequestReport.FromDate,
            ToDate: $scope.SubRequestReport.ToDate,
        };

        //HDMcustomizedReportService.GetGriddata().then(function (response) {
        //    console.log(response);
        //    if (clk) {
        //        $scope.Cols = response.data.coldata;
        //    }
        //    clk = false;
        //    ExportColumns = response.data.lst.exportCols;
        //    $scope.gridOptions.api.setColumnDefs(response.data.lst.Coldef);
        //    $scope.gridata = response.data.lst.griddata;
        //    console.log($scope.gridata);
        //    if ($scope.gridata == null) {
        //        $scope.GridVisiblity = false;
        //        $scope.gridOptions.api.setRowData([]);
        //    }
        //    else {
        //        // progress(0, 'Loading...', true);
        //        $scope.GridVisiblity = true;
        //        $scope.gridOptions.api.setRowData($scope.gridata);
        //        var cols = [];

        //        for (i = 0; i < unticked.length; i++) {
        //            cols[i] = unticked[i].value;
        //        }
        //        $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
        //        cols = [];
        //        for (i = 0; i < ticked.length; i++) {
        //            cols[i] = ticked[i].value;
        //        }
        //        //$scope.gridOptions.columnApi.setColumnsVisible(cols, true);
        //    }
        //    // progress(0, '', false);
        //})

        HDMSubRequestReportService.GetGriddataBySearch(params).then(function (response) {
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData(response.data);
                progress(0, 'Loading...', false);
            }
        });

    }, function (error) {
        console.log(error);
    }

    $scope.CustmPageLoad = function () {
    }, function (error) {
        console.log(error);
    }

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "REQUEST ID", key: "REQUEST_ID" }, { title: "LOCATION", key: "LOCATION" }, { title: "MAIN CATEGORY", key: "MAIN_CATEGORY" }, { title: "SUB CATEGORY", key: "SUB_CATEGORY" }, { title: "CHILD CATEGORY", key: "CHILD_CATEGORY" }, { title: "REQUESTED BY", key: "REQUESTED_BY" }, { title: "REQUESTED DATE", key: "REQUESTED_DATE" }, { title: "UPDATED BY", key: "UPDATED_BY" }, { title: "UPDATED DATE", key: "UPDATED_DT" }, { title: "STATUS", key: "STA_TITLE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("SubRequestReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SubRequestReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        progress(0, 'Loading...', true);
        Customized.Type = Type;
        var params = {
            RequestID: $scope.SubRequestReport.RequestID,
            LCMlst: $scope.SubRequestReport.Locations,
            CHILDlst: $scope.SubRequestReport.child_Category,
            STATUSlst: $scope.SubRequestReport.status,
            HSTSTATUS: $scope.SubRequestReport.selstatus,
            FromDate: $scope.SubRequestReport.FromDate,
            ToDate: $scope.SubRequestReport.ToDate,
            Type: Type
        };
        //Customized.loclst = $scope.LeaseRep.Locations;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Customized.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/HDMSubRequestReport/GetGrid',
                method: 'POST',
                data: params,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SubRequestReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }), function (error,data, status, headers, config) {

            };
        };
    }
    $scope.locSelectAll = function () {
        $scope.SubRequestReport.Locations = $scope.Locations;

    }
    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.SubRequestReport.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getchildbysub = function () {
        UtilityService.getchildbysub($scope.SubRequestReport.Sub_Category).then(function (response) {
            $scope.child_Category = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.SubRequestReport.Main_Category[0] = main;
            }
        });
    }
    $scope.childchange = function () {

        angular.forEach($scope.Main_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub_Category, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.child_Category, function (value, key) {
            var main = _.find($scope.Main_Category, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (main != undefined && value.ticked == true) {
                main.ticked = true;
                $scope.SubRequestReport.Main_Category[0] = main;
            }
        });

        angular.forEach($scope.child_Category, function (value, key) {
            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sub != undefined && value.ticked == true) {
                sub.ticked = true;
                $scope.SubRequestReport.Sub_Category[0] = sub;
            }
        });

    }
    $scope.mainSelectAll = function () {
        $scope.SubRequestReport.Main_Category = $scope.Main_Category;
        $scope.getsubbymain();
    }
    $scope.subSelectAll = function () {
        $scope.SubRequestReport.Sub_Category = $scope.Sub_Category;
        $scope.getchildbysub();
    }
    $scope.subSelectAll = function () {
        $scope.SubRequestReport.child_Category = $scope.child_Category;
        $scope.childchange();
    }
    $scope.PageLoad = function () {

        UtilityService.GetLocationsall(1).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;
                //angular.forEach($scope.Locations, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });

        HDMViewSubRequisitionsService.GetMainCat().then(function (response) {
            if (response.data != null) {
                $scope.Main_Category = response.data;
                //angular.forEach($scope.Main_Category, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });
        HDMViewSubRequisitionsService.Getsubcat().then(function (response) {
            if (response.data != null) {
                $scope.Sub_Category = response.data;
                //angular.forEach($scope.Sub_Category, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });
        HDMViewSubRequisitionsService.Getchildcat().then(function (response) {
            if (response.data != null) {
                $scope.child_Category = response.data;
                //angular.forEach($scope.child_Category, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });
        HDMViewSubRequisitionsService.GetStatusList().then(function (response) {
            if (response.data != null) {
                $scope.status = response.data;
                //angular.forEach($scope.status, function (value, key) {
                //    value.ticked = true;
                //});
            }
        });

        HDMSubRequestReportService.GetReportGrid().then(function (response) {
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData(response.data);
                progress(0, 'Loading...', false);
            }
        });
        //setTimeout(function () {
        //    UtilityService.GetCompanies().then(function (response) {
        //        if (response.data != null) {
        //            $scope.Company = response.data;

        //            var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
        //            if (a)
        //                a.ticked = true;

        //            $scope.LoadData();
        //            if (CompanySession == "1") { $scope.CompanyVisible = 0; }
        //            else { $scope.CompanyVisible = 1; }
        //        }
        //    });
        //}, 2000);
    }

    $scope.PageLoad();


});
