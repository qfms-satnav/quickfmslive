﻿app.service("HDMcustomizedReportMSService", function ($http, $q, UtilityService) {
    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMcustomizedReportMS/GetCustomizedDetailsMS', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetExportdataMS = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMcustomizedReportMS/GetCustomizedDetailsExportMS', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('HDMcustomizedReportMSController', function ($scope, $q, $http, HDMcustomizedReportMSService, UtilityService, $timeout, $filter) {
    $scope.HDMcustomizedMS = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Cols = [];
    $scope.CompanyVisible = 0;
    $scope.columnDefs = [];
    // fill company ddls

    var columnDefs = [
        { headerName: "Ticket ID", field: "RequisitionId", width: 130, cellClass: 'grid-align' },
        { headerName: "Store Code", field: "StoreCode", width: 120, cellClass: 'grid-align' },
        { headerName: "Main Category", field: "MainCategory", width: 130, cellClass: 'grid-align' },
        { headerName: "Sub Category", field: "SubCategory", width: 140, cellClass: 'grid-align' },
        { headerName: "Child Category", field: "ChildCategory", width: 150, cellClass: 'grid-align' },
        { headerName: "Requested Date", field: "CallLogDate", template: '<span>{{data.CallLogDate | date:"dd-mm-yyyy"}}</span>', width: 130, cellClass: 'grid-align' },
        { headerName: "Problem Description", field: "Problem_Description", width: 150, cellClass: 'grid-align' },
        { headerName: "Latest Remarks", field: "LatestRemarks", width: 100, cellClass: 'grid-align' },
        {
            headerName: "Status", field: "STA_TITLE", width: 100, cellClass: 'grid-align', cellStyle: function (params) {

                const backgroundColor = params.data.STA_COLOR ? params.data.STA_COLOR : 'gray';
                return { color: '#000000', 'color': backgroundColor };

            }
        },
        { headerName: "Closed Time", field: "ClosedTime", template: '<span>{{data.ClosedTime | date:"dd-MM-yyyy"}}</span>', width: 130, cellClass: 'grid-align' },
        { headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align' },
        { headerName: "Assigned TO", field: "HELPDESK", width: 130, cellClass: 'grid-align' },
        { headerName: "TAT Status", field: "TAT_STATUS", width: 100, cellClass: 'grid-align' },
        { headerName: "Month", field: "Month", width: 150, cellClass: 'grid-align' },
        { headerName: "Final Cost", field: "Cost", width: 150, cellClass: 'grid-align' }

    ];



    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Main Category", field: "MainCategory",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };
    $scope.GenerateFilteredExcel = function () {
        debugger;
        progress(0, 'Loading...', true);

        var currentDate = new Date();
        var formattedDate = currentDate.toLocaleDateString('en-GB');

        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "HDMCustomizableReport_" + formattedDate + ".csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.HDMcustomizedMS.selVal = "THISMONTH";
    $scope.HDMcustomizedMS.selstatus = "1";
    $scope.STATUSCHANGE = function () {
        switch ($scope.HDMcustomizedMS.selstatus) {
            case '1':
                $scope.HDMcustomizedMS.selstatus = "1";
                break;
            case '2':
                $scope.HDMcustomizedMS.selstatus = "2";
                break;
            case '3':
                $scope.HDMcustomizedMS.selstatus = "3";
                break
        }
    }
    $scope.rptDateRanges = function () {
        switch ($scope.HDMcustomizedMS.selVal) {
            case 'TODAY':
                $scope.HDMcustomizedMS.FromDate = moment().format('MM/DD/YYYY');
                $scope.HDMcustomizedMS.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.HDMcustomizedMS.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.HDMcustomizedMS.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break
            case '7':
                $scope.HDMcustomizedMS.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.HDMcustomizedMS.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.HDMcustomizedMS.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.HDMcustomizedMS.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.HDMcustomizedMS.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.HDMcustomizedMS.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.HDMcustomizedMS.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.HDMcustomizedMS.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.LoadData = function () {

        if (moment($scope.HDMcustomizedMS.FromDate) > moment($scope.HDMcustomizedMS.ToDate)) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        progress(0, 'Loading...', true);
        if ($scope.HDMcustomizedMS.FromDate == undefined && $scope.HDMcustomizedMS.ToDate == undefined) {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1)) + '/' + firstDay.getDate() + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1)) + '/' + lastDay.getDate() + '/' + lastDay.getFullYear();

            $scope.HDMcustomizedMS.FromDate = firstDayWithSlashes;
            $scope.HDMcustomizedMS.ToDate = lastDayWithSlashes;
        }

        var cp = _.find($scope.Company, { ticked: true });
        var params = {
            SearchValue: "",
            PageNumber: 1,
            PageSize: 10,
            ZNlst: $scope.HDMcustomizedMS.Zones,
            LCMlst: $scope.HDMcustomizedMS.Locations,
            CHILDlst: $scope.HDMcustomizedMS.child_Category,
            STATUSlst: $scope.HDMcustomizedMS.status,
            HSTSTATUS: $scope.HDMcustomizedMS.selstatus,
            Request_Type: $scope.HDMcustomizedReportMS.Request_Type,
            FromDate: $scope.HDMcustomizedMS.FromDate,
            ToDate: $scope.HDMcustomizedMS.ToDate,
            CompanyId: cp.CNP_ID
        };

        HDMcustomizedReportMSService.GetGriddata(params).then(function (response) {
            console.log(params);
            console.log(response);
            $scope.gridata = response.data != undefined ? response.data.lst.griddata : null;
            console.log($scope.gridata);
            if ($scope.gridata.length == 0) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                // progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
            }

        });
        //progress(0, '', false);
    }, function (error) {
        console.log(error);
    };

    var clk = true;
    var ExportColumns;

    $scope.ExportData = function () {
        console.log($scope.Company);

    };


    $scope.CustmPageLoad = function () {
    }, function (error) {
        console.log(error);
    }

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns; //[{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" }, { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" }, { title: "Status", key: "STATUS" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("HDMcustomizationReportMS.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        debugger;
        var cp = _.find($scope.Company, { ticked: true });
        var params = {
            ZNlst: $scope.HDMcustomizedMS.Zones,
            LCMlst: $scope.HDMcustomizedMS.Locations,
            CHILDlst: $scope.HDMcustomizedMS.child_Category,
            STATUSlst: $scope.HDMcustomizedMS.status,
            HSTSTATUS: $scope.HDMcustomizedMS.selstatus,
            Request_Type: $scope.HDMcustomizedReportMS.Request_Type,
            FromDate: $scope.HDMcustomizedMS.FromDate,
            ToDate: $scope.HDMcustomizedMS.ToDate,
            CompanyId: cp.CNP_ID
        };

        HDMcustomizedReportMSService.GetExportdataMS(params).then(function (response1) {

            $scope.Exportdata = response1.griddata;
            progress(0, 'Loading...', true);
            var data = [];
            var dataSet = [];
            var date = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/g;
            var datewithtime = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?/g;
            $scope.Exportdata.forEach((val, index) => {
                if ($scope.Exportdata[index] !== null) {
                    for (var i in $scope.Exportdata[index]) {
                        if (index == 0) {

                            data.push(i);

                        }
                        else {
                            if ((date.test($scope.Exportdata[index][i]))) {
                                var array = $scope.Exportdata[index][i].split('-');
                                var day = parseInt(array[0]);
                                var month = parseInt(array[1]);
                                var year = parseInt(array[2]);

                                data.push(new Date(year, month - 1, day + 1));
                            }
                            else if ((datewithtime.test($scope.Exportdata[index][i]))) {

                                data.push(new Date($scope.Exportdata[index][i]));
                            }
                            else {
                                data.push($scope.Exportdata[index][i]);
                            }
                        }
                    }

                    dataSet.push(data);
                    data = [];
                }
            });


            (function () {
                var urlJSZip = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js";
                var urlXLSX = "https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.9.2/xlsx.core.min.js";
                var urlFileSaver = "https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.js";

                $.when($.getScript(urlJSZip), $.getScript(urlXLSX), $.getScript(urlFileSaver)).then(function () {
                    /* original data */
                    var ws_name = "SheetJS";
                    var wb = new Workbook(), ws = sheet_from_array_of_arrays(dataSet);

                    /* add worksheet to workbook */
                    wb.SheetNames.push(ws_name);
                    wb.Sheets[ws_name] = ws;
                    var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

                    saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), "Customized Report.xlsx");
                });

                function datenum(v, date1904) {
                    if (date1904) v += 1462;
                    var epoch = Date.parse(v);
                    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                }

                function sheet_from_array_of_arrays(data, opts) {
                    var ws = {};
                    var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
                    for (var R = 0; R != data.length; ++R) {
                        for (var C = 0; C != data[R].length; ++C) {
                            if (range.s.r > R) range.s.r = R;
                            if (range.s.c > C) range.s.c = C;
                            if (range.e.r < R) range.e.r = R;
                            if (range.e.c < C) range.e.c = C;
                            var cell = { v: data[R][C] };
                            if (cell.v == null) continue;
                            var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                            if (typeof cell.v === 'number') cell.t = 'n';
                            else if (typeof cell.v === 'boolean') cell.t = 'b';
                            else if (cell.v instanceof Date) {
                                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                cell.v = datenum(cell.v);
                            }
                            else cell.t = 's';

                            ws[cell_ref] = cell;
                        }
                    }
                    if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                    return ws;
                }


                function Workbook() {
                    if (!(this instanceof Workbook)) return new Workbook();
                    this.SheetNames = [];
                    this.Sheets = {};
                }

                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }
            })();


            progress(0, 'Loading...', false);
        });
    }

    setTimeout(function () {
        $scope.GenReport = function (Type) {
            if (moment($scope.HDMcustomizedMS.FromDate) > moment($scope.HDMcustomizedMS.ToDate)) {
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                return;
            }
            progress(0, 'Loading...', true);
            woobj = {};
            woobj.Type = Type;
            //if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            //    if (ConsReport.Type == "pdf") {
            //        $scope.GenerateFilterPdf();
            //    }
            //    else {
            //        $scope.GenerateFilterExcel();
            //    }
            //} else {
            $http({
                url: UtilityService.path + '/api/HDMcustomizedReportMS/GetCustomizedDetailsExportMS',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                //if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                //    window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
                //}
                //else if ((navigator.userAgent.indexOf("Firefox") != -1)) {
                //    window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
                //}
                //   else {
                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Customizable Report.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, 'Loading...', false);
                // }
            }), function (error, data, status, headers, config) {

            };
            //  };
        };
    }, 2500);

    $scope.zoneSelectAll = function () {
        debugger;
        $scope.HDMcustomizedMS.Zones = $scope.Zones;
        $scope.getlocbyzone();
    }
    $scope.getlocbyzone = function () {
        UtilityService.getlocbyzone($scope.HDMcustomizedMS.Zones).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.locSelectAll = function () {
        $scope.HDMcustomizedMS.Locations = $scope.Locations;

    }
    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.HDMcustomizedMS.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getchildbysub = function () {
        UtilityService.getchildbysub($scope.HDMcustomizedMS.Sub_Category).then(function (response) {
            $scope.child_Category = response.data;
        }, function (error) {
            console.log(error);
        });

    }
    $scope.childchange = function () {

        angular.forEach($scope.child_Category, function (value, key) {
            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sub != undefined && value.ticked == true) {
                sub.ticked = true;
                $scope.HDMcustomizedMS.Sub_Category[0] = sub;
            }
        });

    }
    $scope.mainSelectAll = function () {
        $scope.HDMcustomizedMS.Main_Category = $scope.Main_Category;
        $scope.getsubbymain();
    }
    $scope.subSelectAll = function () {
        $scope.HDMcustomizedMS.Sub_Category = $scope.Sub_Category;
        $scope.getchildbysub();
    }
    $scope.childSelectAll = function () {
        $scope.HDMcustomizedMS.child_Category = $scope.child_Category;
        $scope.Getchildcat();
    }


    $scope.PageLoad = function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                if (a)
                    a.ticked = true;
                if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
                UtilityService.Getallzones().then(function (response) {
                    if (response.data != null) {
                        $scope.Zones = response.data;
                        angular.forEach($scope.Zones, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.GetLocationsall(1).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.Getmaincat().then(function (response) {
                                    if (response.data != null) {
                                        $scope.Main_Category = response.data;
                                        angular.forEach($scope.Main_Category, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.Getsubcat().then(function (response) {
                                            if (response.data != null) {
                                                $scope.Sub_Category = response.data;
                                                angular.forEach($scope.Sub_Category, function (value, key) {
                                                    value.ticked = true;
                                                });
                                                UtilityService.Getchildcat().then(function (response) {
                                                    if (response.data != null) {
                                                        $scope.child_Category = response.data;
                                                        angular.forEach($scope.child_Category, function (value, key) {
                                                            value.ticked = true;
                                                        });
                                                        UtilityService.Getstatus().then(function (response) {
                                                            if (response.data != null) {
                                                                $scope.status = response.data;
                                                                angular.forEach($scope.status, function (value, key) {
                                                                    value.ticked = true;
                                                                });
                                                                setTimeout(function () {
                                                                    $scope.LoadData();
                                                                }, 3000);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }


    $scope.PageLoad();

    //setTimeout(function () {
    //    $scope.LoadData();
    //}, 3000);


});
