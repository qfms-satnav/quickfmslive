﻿app.service("HDMReqStatusconsolidatedReportService", function ($http, $q, UtilityService) {
    this.BindGrid = function (params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMReqStatusconsolidatedReport/BindGrid', params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.HDMconsolidatedChart = function (params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMReqStatusconsolidatedReport/HDMconsolidatedChart', params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('HDMReqStatusconsolidatedReportController', function ($scope, $q, HDMReqStatusconsolidatedReportService, UtilityService, $timeout, $http) {
    $scope.ConsRpt = [];
    $scope.CompanyVisible = 0;
    $scope.rptArea = {};
    $scope.ex = {};
    //$scope.GridVisiblity = true;
    $scope.Type = [];
    $scope.DocTypeVisible = 0;
    $scope.GridVisiblity = false;
    $scope.GridVisiblityZone = false;
    $scope.HDMReqconsolidated = {};
    $scope.Chartdata = [];
    $scope.Pageload = function () {
        setTimeout(function () {
            UtilityService.GetCompanies().then(function (response) {
                if (response.data != null) {
                    $scope.Company = response.data;
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    /* $scope.BindGrid();*/

                    UtilityService.getAllEmployees(1).then(function (response1) {
                        if (response1.data != null) {
                            $scope.Employees = response1.data

                            UtilityService.getCities(1).then(function (response2) {
                                if (response2.data != null) {
                                    $scope.Citys = response2.data
                                    angular.forEach($scope.Citys, function (value, key) {
                                        value.ticked = true;
                                    });
                                    UtilityService.GetLocationsall(1).then(function (response3) {
                                        if (response3.data != null) {
                                            $scope.Locations = response3.data
                                            angular.forEach($scope.Locations, function (value, key) {
                                                value.ticked = true;
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                    if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                    else { $scope.CompanyVisible = 1; }
                }

            });
            $scope.HDMReqconsolidated.FromDate = moment().subtract(29, 'days').format('DD/MMM/YYYY');
            $scope.HDMReqconsolidated.ToDate = moment().format('DD/MMM/YYYY');
        }, 1000)
    }
    $scope.ctySelectAll = function () {
        UtilityService.GetLocationsall(1).then(function (response3) {
            if (response3.data != null) {
                $scope.Locations = response3.data
                angular.forEach($scope.Locations, function (value, key) {
                    value.ticked = true;
                });
            }
        });

    }
    $scope.ctySelectNone = function () {
        $scope.Locations = [];
    }
    $scope.GetLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.ConsRpt.CTY_NAME, 1).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.Search = function () {

        if (tabSummary.className == 'tab-pane fade in active') {
            if ($scope.ConsRpt.selstatus == "1") {
                $scope.BindGrid();
            }
            else if ($scope.ConsRpt.selstatus == "2") {
                $scope.BindGrid();
            }
            if ($scope.ConsRpt.selstatus == "3") {
                $scope.BindGrid();
            }
        }
        else if (tabZone.className == 'tab-pane fade active in') {
            if ($scope.ConsRpt.selstatus == "1") {
                $scope.BindGrid_Zone();
            }
            else if ($scope.ConsRpt.selstatus == "2") {
                $scope.BindGrid_Zone();
            }
            else ($scope.ConsRpt.selstatus == "3")
            {
                $scope.BindGrid_Zone();
            }
        }



    }

    //var columnDefs = [
    //    { headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 0, suppressMenu: true },
    //    { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 1, hide: true, suppressMenu: true },
    //    { headerName: "Location", field: "LCM_NAME", width: 200, cellClass: 'grid-align', rowGroupIndex: 2, hide: true },
    //    { headerName: "Total Requests", field: "Total_Requests", width: 180, cellClass: 'grid-align', suppressMenu: true },
    //    { headerName: "Pending Requests", field: "Pending_Requests", width: 130, cellClass: 'grid-align', suppressMenu: true },
    //    { headerName: "Requests In-Progress", field: "In_Progress_Requests", width: 250, cellClass: 'grid-align', suppressMenu: true },
    //    { headerName: "Closed Requests", field: "Closed_Requests", width: 130, cellClass: 'grid-align', suppressMenu: true },
    //    { headerName: "Canceled Requests", field: "Canceled_Requests", width: 130, cellClass: 'grid-align', suppressMenu: true }
    //];
    var columnDefs = [
        { headerName: "Zone", field: "ZONE", width: 100, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "City", field: "CTY_NAME", width: 100, cellClass: 'grid-align', hide: true, suppressMenu: true },
        { headerName: "Location", field: "LOCATION", width: 200, cellClass: 'grid-align' },
        { headerName: "SubCategory", field: "SUB CATEGORY", width: 180, cellClass: 'grid-align' },
        { headerName: "Closed", field: "CLOSED", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "OnHold", field: "ON HOLD", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "In Progress", field: "IN PROGRESS", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Assigned", field: "Assigned", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Request Submitted", field: "REQUEST SUBMITTED", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Request Cancelled", field: "REQUEST CANCELLED", width: 130, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Waiting For Clarification", field: "WAITING FOR CLARIFICATION", width: 200, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Total Requests", field: "Total Tickets", width: 100, cellClass: 'grid-align', suppressMenu: true },
    ];

    $scope.pageSize = '10';

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableCellSelection: false,
        //groupAggFunction: groupAggFunction,
        //groupHideGroupColumns: true,
        //groupColumnDef: {
        //    headerName: "Country", field: "CNY_NAME",
        //    cellRenderer: {
        //        renderer: "group"
        //    }
        //},
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            Total_Requests: 0,
            Pending_Requests: 0,
            In_Progress_Requests: 0,
            Closed_Requests: 0,
            Canceled_Requests: 0
        };

        rows.forEach(function (row) {
            var data = row.data;
            sums.Total_Requests += parseInt(data.Total_Requests);
            sums.Pending_Requests += parseInt(data.Pending_Requests);
            sums.In_Progress_Requests += parseInt(data.In_Progress_Requests);
            sums.Closed_Requests += parseInt(data.Closed_Requests);
            sums.Canceled_Requests += parseInt(data.Canceled_Requests);
        });
        return sums;
    }

    // $scope.BindGrid();

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })


    $scope.BindGrid = function () {
        progress(0, 'Loading...', true);
        var cp = _.find($scope.Company, { ticked: true })
        var params = {
            CNP_NAME: cp.CNP_ID,
            FLAG: 1,
            HSTSTATUS: $scope.ConsRpt.selstatus,
            AUR_ID: $scope.ConsRpt.AUR_ID[0].AUR_ID,
            CTY_CODE: $scope.ConsRpt.CTY_NAME[0].CTY_CODE,
            LCM_CODE: $scope.ConsRpt.LCM_NAME,
            FromDate: moment($scope.HDMReqconsolidated.FromDate, 'DD/MMM/YYYY').format('MM/DD/YYYY'),
            ToDate: moment($scope.HDMReqconsolidated.ToDate, 'DD/MMM/YYYY').format('MM/DD/YYYY')
        };


        HDMReqStatusconsolidatedReportService.BindGrid(params).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.RptByUsrGrid = true;
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.GridVisiblityZone = false;
                $scope.gridOptions.api.setRowData($scope.gridata.Details);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }
            $scope.HDMconsolidatedChart();
        }, function (error) {
            console.log(error);
        });
    }
    var columnDefszone = [
        { headerName: "Zone", field: "LCM_ZONE", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Pending Requests", field: "Pending_Requests", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "In Progress Requests", field: "In_Progress_Requests", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Closed Requests", field: "Closed_Requests", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Canceled Requests", field: "Canceled_Requests", width: 170, cellClass: 'grid-align', suppressMenu: true },
        { headerName: "Total Requests", field: "Total_Requests", width: 170, cellClass: 'grid-align', suppressMenu: true },

    ];
    $scope.pageSize = '10';

    $scope.gridzone = {
        columnDefs: columnDefszone,
        rowData: null,
        //enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        //groupAggFunction: groupaggfunctionProject,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Zone", field: "LCM_ZONE",
            cellRenderer: {
                renderer: "group"
            }
        },
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridzone.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    }

    function onFilterChangedzone(value) {
        $scope.gridzone.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    $("#filteredzone").change(function () {
        onFilterChangedzone($(this).val());
    }).keydown(function () {
        onFilterChangedzone($(this).val());
    }).keyup(function () {
        onFilterChangedzone($(this).val());
    }).bind('paste', function () {
        onFilterChangedzone($(this).val());
    });

    $scope.CityChanged = function () {
        $scope.locationddl = [];
        UtilityService.getLocationsByCity($scope.ConsRpt.CTY_NAME, 1).then(function (data) {
            if (data.data == null) { $scope.Locationlst = [] } else {
                $scope.Locations = data.data;
            }
        }, function (error) {
            console.log(error);
        });
    };

    $scope.BindGrid_Zone = function () {
        progress(0, 'Loading...', true);
        var cp = _.find($scope.Company, { ticked: true })
        var params = {
            CNP_NAME: cp.CNP_ID,
            FLAG: 2,
            HSTSTATUS: $scope.ConsRpt.selstatus
        };
        HDMReqStatusconsolidatedReportService.BindGrid(params).then(function (response) {
            progress(0, 'Loading...', true);
            $scope.RptByUsrGrid = true;
            $scope.gridata = response.data;
            if (response.data == null) {
                $scope.GridVisiblityZone = false;
                $scope.gridzone.api.setRowData([]);
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', true);
                $scope.GridVisiblity = false;
                $scope.GridVisiblityZone = true;
                $scope.gridzone.api.setRowData($scope.gridata);
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }
            $scope.HDMconsolidatedChart();
        }, function (error) {
            console.log(error);
        });
    }




    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    $("#table2").fadeIn();

    var chart;
    //chart = c3.generate({
    //    data: {
    //        columns: [],
    //        cache: false,
    //        type: 'pie',
    //        empty: { label: { text: "Sorry, No Data Found" } }
    //    },
    //    tooltip: {
    //        contents: tooltip_contents
    //    },
    //    pie: {
    //        label: {
    //            format: function (value, ratio, id) {
    //                return (value);
    //            }
    //        }
    //    }
    //});
    var chart = c3.generate({
        data: {
            columns: [],
            type: 'bar',
            onclick: function (e) {
                $("#RequestTypeDetails").modal("show");
                getRequestDetails(e.id);
            }
        },
        //donut: {
        //    expand: false,
        //    label: {
        //        format: function (value, ratio, id) {
        //            return d3.format('')(value);
        //        }
        //    }

        //},
        //axis: {
        //    x: {
        //        position: 'outer-center',
        //    },
        //},
        bar: {
            width: {
                ratio: 1 // this makes bar width 50% of length between ticks
            }
        }
    });

    function tooltip_contents(d, defaultTitleFormat, defaultValueFormat, color) {
        var $$ = this, config = $$.config, CLASS = $$.CLASS,
            titleFormat = config.tooltip_format_title || defaultTitleFormat,
            nameFormat = config.tooltip_format_name || function (name) { return name; },
            valueFormat = config.tooltip_format_value || defaultValueFormat,
            text, i, title, value, name, bgcolor;

        for (i = 0; i < d.length; i++) {
            if (!(d[i] && (d[i].value || d[i].value === 0))) { continue; }

            if (!text) {
                text = "<table class='" + CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
            }

            name = nameFormat(d[i].name);
            value = d[i].value;
            bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

            text += "<tr class='" + CLASS.tooltipName + "-" + d[i].id + "'>";
            text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
            text += "<td class='value'>" + value + "</td>";
            text += "</tr>";
        }
        return text + "</table>";
    }

    //GetCostChartData
    $scope.HDMconsolidatedChart = function () {
        //var cp = _.find($scope.Company, { ticked: true })
        //HDMReqStatusconsolidatedReportService.HDMconsolidatedChart(cp.CNP_ID).then(function (response) {
        //    chart.unload();
        //    chart.unload({
        //        ids: ['HDMconsolidatedGraph']
        //    });
        var cp = _.find($scope.Company, { ticked: true })
        var params = {
            CNP_NAME: cp.CNP_ID,
            AUR_ID: $scope.ConsRpt.AUR_ID[0].AUR_ID,
            CTY_CODE: $scope.ConsRpt.CTY_NAME[0].CTY_CODE,
            LCM_CODE: $scope.ConsRpt.LCM_NAME,
            FromDate: moment($scope.HDMReqconsolidated.FromDate, 'DD/MMM/YYYY').format('MM/DD/YYYY'),
            ToDate: moment($scope.HDMReqconsolidated.ToDate, 'DD/MMM/YYYY').format('MM/DD/YYYY')
        };
        HDMReqStatusconsolidatedReportService.HDMconsolidatedChart(params).then(function (response) {
            chart.unload();
            chart.unload({
                ids: ['HDMconsolidatedGraph']
            });




            setTimeout(function () {
                chart.load({ columns: response.data });
                //$("#HDMconsolidatedGraph").append(chart.element);
                $("#HDMconsolidatedGraph").empty();
                $("#HDMconsolidatedGraph").append(chart.element);
                $("#HDMconsolidatedGraph").load();
            }, 200);

        }, function (error) {
            console.log(error);
        });
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
                $("#table2").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
                $("#table2").fadeOut();
                $scope.HDMconsolidatedChart();
            });
        }
    });

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [
            { title: "Country", key: "CNY_NAME" }, { title: "Total Requests", key: "ALLOCATED_SEATS" }, { title: "Pending Requests", key: "OCCUPIED_SEATS" },
            { title: "Requests In-Progress", key: "ALLOCATED_VACANT" }, { title: "Closed Requests", key: "VACANT_SEATS" },
            { title: "Canceled Requests", key: "TOTAL_SEATS" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        console.log(jsondata);
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("HDMReqStatusconsolidatedReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "HDMReqStatusconsolidatedReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        $scope.rptArea.DocType = Type;
        var cp = _.find($scope.Company, { ticked: true })
        $scope.rptArea.CNP_NAME = cp.CNP_ID;
        $scope.rptArea.AUR_ID = $scope.ConsRpt.AUR_ID[0].AUR_ID;
        $scope.rptArea.CTY_CODE = $scope.ConsRpt.CTY_NAME[0].CTY_CODE;
        $scope.rptArea.LCM_CODE = $scope.ConsRpt.LCM_NAME;
        $scope.rptArea.FromDate = moment($scope.HDMReqconsolidated.FromDate, 'DD/MMM/YYYY').format('MM/DD/YYYY');
        $scope.rptArea.ToDate = moment($scope.HDMReqconsolidated.ToDate, 'DD/MMM/YYYY').format('MM/DD/YYYY');
        $scope.rptArea.HSTSTATUS = $scope.ConsRpt.selstatus;
        $scope
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            progress(0, 'Loading...', true);
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/HDMReqStatusconsolidatedReport/Export_HDMconsolidatedRpt',
                method: 'POST',
                data: $scope.rptArea,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
                }
                else if ((navigator.userAgent.indexOf("Firefox") != -1)) {
                    window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
                }
                else {
                    var fileURL = URL.createObjectURL(file);

                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'HDMReqStatusconsolidatedReport.' + Type;
                    document.body.appendChild(a);
                    a.click();
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000)
                }
            }), function (error, data, status, headers, config) {

            };
        }
    };

    $scope.GenerateZoneFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [
            { title: "Country", key: "LCM_ZONE" }, { title: "Total Requests", key: "ALLOCATED_SEATS" }, { title: "Pending Requests", key: "OCCUPIED_SEATS" },
            { title: "Requests In-Progress", key: "ALLOCATED_VACANT" }, { title: "Closed Requests", key: "VACANT_SEATS" },
            { title: "Canceled Requests", key: "TOTAL_SEATS" }
        ];
        var model = $scope.gridzone.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        console.log(jsondata);
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("ZoneconsolidatedReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.GenerateZoneFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "ZoneconsolidatedReport.csv"
        };
        $scope.gridzone.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }


    $scope.GenReportZone = function (Type) {
        $scope.rptArea.DocType = Type;
        var cp = _.find($scope.Company, { ticked: true })
        $scope.rptArea.CNP_NAME = cp.CNP_ID;
        $scope.rptArea.HSTSTATUS = $scope.ConsRpt.selstatus;
        $scope
        if ($scope.gridzone.api.isAnyFilterPresent($scope.columnDefs)) {
            progress(0, 'Loading...', true);
            if ($scope.rptArea.DocType == "pdf") {
                $scope.GenerateZoneFilterPdf();
            }
            else {
                $scope.GenerateZoneFilterExcel();
            }
        }
        else {
            progress(0, 'Loading...', true);
            $http({
                url: UtilityService.path + '/api/HDMReqStatusconsolidatedReport/ExportZoneconsolidatedRpt',
                method: 'POST',
                data: $scope.rptArea,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    window.navigator.msSaveOrOpenBlob(file, 'Zone Consolidated Report.' + Type);
                }
                else if ((navigator.userAgent.indexOf("Firefox") != -1)) {
                    window.navigator.msSaveOrOpenBlob(file, 'Zone Consolidated Report.' + Type);
                }
                else {
                    var fileURL = URL.createObjectURL(file);

                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'ZoneconsolidatedReport.' + Type;
                    document.body.appendChild(a);
                    a.click();
                    setTimeout(function () {
                        progress(0, 'Loading...', false);
                    }, 1000)
                }
            }).error(function (data, status, headers, config) {

            });
        }
    };
    function RequestDetails(CompanyId) {
        var param = { companyid: parseInt(CompanyId) };
        var chart = c3.generate({
            data: {
                columns: [],
                type: 'donut',
                onclick: function (e) {
                    $("#RequestTypeDetails").modal("show");
                    getRequestDetails(CompanyId, e.id);
                }
            },
            donut: {
                expand: false,
                label: {
                    format: function (value, ratio, id) {
                        return d3.format('')(value);
                    }
                }

            },
            axis: {
                x: {
                    position: 'outer-center',
                },
            },

        });
    }

    $scope.GenerateFilterExcel1 = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "HDMReqStatusconsolidatedReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }
    $scope.GenChartReport = function (Type) {
        $scope.ex.DocType = Type;
        var cp = _.find($scope.Company, { ticked: true })
        $scope.ex.CNP_NAME = cp.CNP_ID;
        $scope.ex.AUR_ID = $scope.ConsRpt.AUR_ID[0].AUR_ID;
        $scope.ex.CTY_CODE = $scope.ConsRpt.CTY_NAME[0].CTY_CODE;
        $scope.ex.LCM_CODE = $scope.ConsRpt.LCM_NAME;
        $scope.ex.FromDate = moment($scope.HDMReqconsolidated.FromDate, 'DD/MMM/YYYY').format('MM/DD/YYYY');
        $scope.ex.ToDate = moment($scope.HDMReqconsolidated.ToDate, 'DD/MMM/YYYY').format('MM/DD/YYYY');
        $scope.ex.Status = $scope.ConsRpt.Status;
        $scope.ex.HSTSTATUS = $scope.ConsRpt.selstatus;
        //if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
        //    progress(0, 'Loading...', true);
        //    if ($scope.ex.DocType == "pdf") {
        //        $scope.GenerateFilterPdf();
        //    }
        //    else {
        //        $scope.GenerateFilterExcel1();
        //    }
        //}
        //else {
        //    progress(0, 'Loading...', true);
        $http({
            url: UtilityService.path + '/api/HDMReqStatusconsolidatedReport/Export_HDMconsolidatedRpt1',
            method: 'POST',
            data: $scope.ex,
            responseType: 'arraybuffer'

        }).then(function (data, status, headers, config) {
            var file = new Blob([data.data], {
                type: 'application/' + Type
            });
            if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
            }
            else if ((navigator.userAgent.indexOf("Firefox") != -1)) {
                window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
            }
            else {
                var fileURL = URL.createObjectURL(file);

                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'HDMReqStatusconsolidatedReport.' + Type;
                document.body.appendChild(a);
                a.click();
                setTimeout(function () {
                    progress(0, 'Loading...', false);
                }, 1000)
            }
        }), function (error, data, status, headers, config) {

            //    };
        }
    };


    function getRequestDetails(type) {
        var cp = _.find($scope.Company, { ticked: true })
        $scope.ConsRpt.Status = type;
        var params = {
            CNP_NAME: cp.CNP_ID,
            FLAG: 1,
            HSTSTATUS: $scope.ConsRpt.selstatus,
            AUR_ID: $scope.ConsRpt.AUR_ID[0].AUR_ID,
            CTY_CODE: $scope.ConsRpt.CTY_NAME[0].CTY_CODE,
            LCM_CODE: $scope.ConsRpt.LCM_NAME,
            FromDate: moment($scope.HDMReqconsolidated.FromDate, 'DD/MMM/YYYY').format('MM/DD/YYYY'),
            ToDate: moment($scope.HDMReqconsolidated.ToDate, 'DD/MMM/YYYY').format('MM/DD/YYYY'),
            Status: type
        };
        $.ajax({
            //url: "../api/AssetDBAPI/GetMap_umMapAssets_DB",  
            url: UtilityService.path + '/api/HDMReqStatusconsolidatedReport/HDMReq_Details',
            //data: { "category": type },
            contentType: "application/json; charset=utf-8",
            method: "POST",
            //dataType: 'json',
            data: JSON.stringify(params),
            success: function (data) {
                //console.log(data);
                $scope.Chartdata = data
                var table = $('#tblassetTypes');
                $('#tblassetTypes td').remove();
                var dt;
                dt = data.Details;
                for (var i = 0; i < dt.length; i++) {
                    table.append("<tr>" +
                        "<td>" + (i + 1) + "</td>" +
                        "<td>" + dt[i].Request_ID + "</td>" +
                        "<td>" + dt[i].LOCATION + "</td>" +
                        "<td>" + dt[i].CHILD_CATEGORY + "</td>" +
                        "<td>" + dt[i].Requested_Date + "</td>" +
                        "<td>" + dt[i].status + "</td>" +
                        "<td>" + dt[i].Raised_By + "</td>" +
                        "<td>" + dt[i].Assigned_To + "</td>" +
                        "</tr>");
                }

                if (dt.length == 0) {
                    table.append("<tr>" + "<td colspan='3' align='center'> No Requests found</td>" + "</tr>");
                }
            },
            error: function (result) {
            }
        });
    }
    $scope.Pageload();


});

