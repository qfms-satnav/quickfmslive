﻿app.service("HDMReportByCategoryService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetReportByCategory = function (searchObj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMReportByCategory/GetHDMReportByCategoryGrid', searchObj)
            .then(function (response) {
                deferred.resolve(response.data);
                response.data.data.forEach(function (d) {
                    d.REQ_STATUS = `<span style="color:${d.STA_COLOR}">${d.REQ_STATUS}</span>`;
                });
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetHistory = function (data) {
        deferred = $q.defer();
        return $http.post('../../../../api/HDMReportByUser/GetHistory/', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

}]);

app.controller('HDMReportByCategoryController', ['$scope', '$q', 'UtilityService', 'HDMReportByCategoryService', '$timeout', '$http', function ($scope, $q, UtilityService, HDMReportByCategoryService, $timeout, $http) {
    $scope.RptByCategory = {};
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.RptByCategoryGrid = true;
    $scope.ActionStatus = 0;
    $scope.DocTypeVisible = 0;

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                console.log(CompanySession);
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.RptByCategory.CNP_NAME.push(a);
                });

                if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
            }

        });
    }, 500);

    var OVERALL_COUNT = 0;
    $scope.GetReportByCategory = function () {
        if (moment($scope.RptByCategory.FromDate) > moment($scope.RptByCategory.ToDate)) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();
        progress(0, 'Loading...', true);
        //$scope.rptDateRanges();
        var dataSource = {
            rowCount: null,
            getRows: function (params) {
                $scope.HDMRptByCategory = {
                    FromDate: $scope.RptByCategory.FromDate,
                    ToDate: $scope.RptByCategory.ToDate,
                    CompanyId: $scope.RptByCategory.CNP_NAME[0].CNP_ID,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    SEARCHVAL: searchval
                };
                var fromdate = moment($scope.RptByCategory.FromDate);
                var todate = moment($scope.RptByCategory.ToDate);
                if (fromdate > todate) {
                    $scope.GridVisiblity = false;
                    showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                }
                else {
                    progress(0, 'Loading...', true);
                    HDMReportByCategoryService.GetReportByCategory($scope.HDMRptByCategory).then(function (response) {

                        $scope.RptByCategoryGrid = true;
                        $scope.gridata = response;
                        console.log(response);
                        if (response.data == null) {
                            $("#btNext").attr("disabled", true);
                            $scope.gridOptions.api.setRowData([]);
                            progress(0, 'Loading...', false);

                        }
                        else {

                            progress(0, 'Loading...', true);
                            $scope.gridOptions.api.setRowData(response.data);
                            OVERALL_COUNT = $scope.gridata.data[0].OVERALL_COUNT;
                            progress(0, 'Loading...', false);

                        }

                        function bindLocChart() {
                            $scope.CatWiseBarChart($scope.HDMRptByCategory);
                        }
                        function bindSubCatGraph() {
                            $scope.subCatGraph($scope.HDMRptByCategory);
                        }
                        $timeout(bindLocChart, 1000);
                        $timeout(bindSubCatGraph, 1000);

                    }, function (error) {
                        console.log(error);
                    });
                };
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    var columnDefs = [
        { headerName: "Requisition Id", field: "SER_REQ_ID", width: 120, cellClass: 'grid-align', },
        { headerName: "Main Category ", field: "MAIN_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Sub Category", field: "SUB_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Child Category", field: "CHILD_CATEGORY", width: 150, cellClass: 'grid-align', },
        { headerName: "Location", field: "LOCATION", width: 150, cellClass: 'grid-align', },
        { headerName: "Requested By ID", field: "REQUESTED_BY_ID", width: 150, cellClass: 'grid-align', },
        { headerName: "Requested By Name", field: "REQUESTED_BY_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Requested Date", field: "REQUESTED_DATE", width: 150, cellClass: 'grid-align', },
        { headerName: "Problem Description", field: "SER_PROB_DESC", width: 150, cellClass: 'grid-align', },
        { headerName: "Assigned To ID", field: "ASSIGNED_TO", width: 150, cellClass: 'grid-align', },
        { headerName: "Assigned To Name", field: "ASSIGNED_TO_NAME", width: 150, cellClass: 'grid-align', },
        { headerName: "Contact No", field: "CONTACT_NO", width: 150, cellClass: 'grid-align', },
        { headerName: "Service Status", field: "REQ_STATUS", width: 150, cellClass: 'grid-align', },
        { headerName: "Total Time", field: "TOTAL_TIME", width: 180, cellClass: 'grid-align', },
        { headerName: "Closed Time", field: "CLOSED_TIME", width: 150, cellClass: 'grid-align', },
        { headerName: "Defined TAT", field: "DEFINED_TAT", width: 120, cellClass: 'grid-align', },
        { headerName: "TAT", field: "TAT", width: 80, cellClass: 'grid-align', hide: true },
        { headerName: "Delayed Time", field: "DELAYED_TIME", width: 180, cellClass: 'grid-align', },
    ];

    $scope.pageSize = '10';

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        rowData: null,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };


    function onUpdateFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1;
        } else {
            $scope.DocTypeVisible = 0;
        }
    }
    $("#UpdteFilter").change(function () {
        onUpdateFilterChanged($(this).val());
    }).keydown(function () {
        onUpdateFilterChanged($(this).val());
    }).keyup(function () {
        onUpdateFilterChanged($(this).val());
    }).bind('paste', function () {
        onUpdateFilterChanged($(this).val());
    })

    //$scope.DATE_RANGE = [
    //  { range: "Today", RANGE_VAL: "TODAY", ticked: false },
    //  { range: "Yesterday", RANGE_VAL: "YESTERDAY", ticked: false },
    //  { range: "Last 7 Days", RANGE_VAL: "7", ticked: false },
    //  { range: "Last 30 Days", RANGE_VAL: "30", ticked: false },
    //  { range: "This Month", RANGE_VAL: "THISMONTH", ticked: false },
    //  { range: "Last Month", RANGE_VAL: "LASTMONTH", ticked: false }

    //];

    //$scope.selctedRange = [];

    $scope.RptByCategory.selVal = "THISMONTH";

    $scope.rptDateRanges = function () {
        switch ($scope.RptByCategory.selVal) {
            case 'SELECT':
                $scope.RptByCategory.FromDate = "";
                $scope.RptByCategory.ToDate = "";
                break;
            case 'TODAY':
                $scope.RptByCategory.FromDate = moment().format('DD-MMM-YYYY');
                $scope.RptByCategory.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'YESTERDAY':
                $scope.RptByCategory.FromDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                $scope.RptByCategory.ToDate = moment().subtract(1, 'days').format('DD-MMM-YYYY');
                break;
            case '7':
                $scope.RptByCategory.FromDate = moment().subtract(6, 'days').format('DD-MMM-YYYY');
                $scope.RptByCategory.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case '30':
                $scope.RptByCategory.FromDate = moment().subtract(29, 'days').format('DD-MMM-YYYY');
                $scope.RptByCategory.ToDate = moment().format('DD-MMM-YYYY');
                break;
            case 'THISMONTH':
                $scope.RptByCategory.FromDate = moment().startOf('month').format('DD-MMM-YYYY');
                $scope.RptByCategory.ToDate = moment().endOf('month').format('DD-MMM-YYYY');
                break;
            case 'LASTMONTH':
                $scope.RptByCategory.FromDate = moment().subtract(1, 'month').startOf('month').format('DD-MMM-YYYY');
                $scope.RptByCategory.ToDate = moment().subtract(1, 'month').endOf('month').format('DD-MMM-YYYY');
                break;

        }
    }


    function getLastMonthData() {
        $scope.GetReportByCategory();
        //$scope.CatWiseBarChart($scope.HDMRptByCategory);
        //$scope.subCatGraph($scope.HDMRptByCategory);
    }

    function getLastMonthChartData() {
        $scope.CatWiseBarChart($scope.HDMRptByCategory);
    }

    $timeout(getLastMonthData, 200);
    //$timeout(getLastMonthChartData, 2000);

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Main Category", key: "MAIN_CATEGORY" }, { title: "Sub Category", key: "SUB_CATEGORY" }, { title: "Child Category", key: "CHILD_CATEGORY" }, { title: "Location", key: "LOCATION" },
        { title: "Requested By ID", key: "REQUESTED_BY_ID" }, { title: "Requested By Name", key: "REQUESTED_BY_NAME" }, { title: "Requested Date", key: "REQUESTED_DATE" }, { title: "Prbolem Description", key: "SER_PROB_DESC" },
        { title: "Assigned To ID", key: "ASSIGNED_TO" }, { title: "Assigned To Name", key: "ASSIGNED_TO_NAME" },
        { title: "Contact No", key: "CONTACT_NO" }, { title: "Service Status", key: "REQ_STATUS" }, { title: "Total Time ", key: "TOTAL_TIME" }, { title: "Closed Time", key: "CLOSED_TIME" },
        { title: "Defined TAT", key: "DEFINED_TAT" }, { title: "Delayed Time(in Minutes)", key: "DELAYED_TIME" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF('p', 'pt', 'A4');
        doc.autoTable(columns, jsondata);
        doc.save("ReportByCategory.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ",",
            fileName: "ReportByCategory.csv"
        }; $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (RptByCategory, Type) {
        if (moment($scope.RptByCategory.FromDate) > moment($scope.RptByCategory.ToDate)) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        progress(0, 'Loading...', true);
       
        var searchval = $("#filtertxt").val();
        woobj = {};
        woobj.CompanyId = RptByCategory.CNP_NAME[0].CNP_ID;
        woobj.FromDate = RptByCategory.FromDate;
        woobj.ToDate = RptByCategory.ToDate;
        woobj.DocType = Type;
        woobj.SEARCHVAL = searchval;
        woobj.PageNumber =1;
        woobj.PageSize =OVERALL_COUNT;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (RptByCategory.DocType == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        } else {

            $http({
                url: UtilityService.path + '/api/HDMReportByCategory/GetReportByCategory',
                method: 'POST',
                data: woobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                //var popupwin = window.open(fileURL, "SpaceRequisitionReport", "toolbar=no,width=500,height=500");
                //setTimeout(function () {
                //    popupwin.focus();
                //}, 500);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Report By Category.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, 'Loading...', false);
            }), function (error,data, status, headers, config) {

            };
        };
    }

    $("#Tabular").fadeIn();
    $("#Graphicaldiv").fadeOut();
    var chart;
    var chartSubCat;
    chart = c3.generate({
        data: {
            //x: 'x',
            columns: [],
            cache: false,
            type: 'bar',
            empty: { label: { text: "Sorry, No Data Found" } },
        },
        legend: {
            position: 'top'
        },
        axis: {
            x: {
                type: 'category',
                categories: ['Total', 'Open', 'Closed'],
                height: 130,
                show: true,
                //label: {
                //    text: 'Locations',
                //    position: 'outer-center'
                //}
            },
            y: {
                show: true,
                label: {
                    text: 'Requisition Count',
                    position: 'outer-middle'
                }
            }
        },

        width:
        {
            ratio: 0.5
        }
    });

    chartSubCat = c3.generate({
        data: {
            columns: [],
            cache: false,
            type: 'pie',
            empty: { label: { text: "Sorry, No Data Found" } }
        },
        tooltip: {
            contents: tooltip_contents
        },
        pie: {
            label: {
                format: function (value, ratio, id) {
                    return (value);
                }
            }
        }
    });

    function tooltip_contents(d, defaultTitleFormat, defaultValueFormat, color) {
        var $$ = this, config = $$.config, CLASS = $$.CLASS,
            titleFormat = config.tooltip_format_title || defaultTitleFormat,
            nameFormat = config.tooltip_format_name || function (name) { return name; },
            valueFormat = config.tooltip_format_value || defaultValueFormat,
            text, i, title, value, name, bgcolor;

        for (i = 0; i < d.length; i++) {
            if (!(d[i] && (d[i].value || d[i].value === 0))) { continue; }

            if (!text) {
                text = "<table class='" + CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
            }

            name = nameFormat(d[i].name);
            value = d[i].value;
            bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);

            text += "<tr class='" + CLASS.tooltipName + "-" + d[i].id + "'>";
            text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + name + "</td>";
            text += "<td class='value'>" + value + "</td>";
            text += "</tr>";
        }
        return text + "</table>";
    }

    $scope.CatWiseBarChart = function (data) {
        $http({
            url: UtilityService.path + '/api/HDMReportByCategory/GetCategoryWiseChart',
            method: 'POST',
            data: data
        }).then(function (result) {
            chart.unload();

            chart.load({ columns: result });
        });
        setTimeout(function () {
            $("#LocationGraph").append(chart.element);
        }, 700);
    }

    $scope.subCatGraph = function (spcData) {
        $http({
            url: UtilityService.path + '/api/HDMReportByCategory/GetCategoryWisePieChartData',
            method: 'POST',
            data: spcData
        }).then(function (result) {

            chartSubCat.unload();
            chartSubCat.load({ columns: result });
        });
        setTimeout(function () {
            $("#SubCatGraph").empty();
            $("#SubCatGraph").append(chartSubCat.element);
            $("#SubCatGraph").load();
            //$("#SubCatGraph").append(chartSubCat.element);
        }, 1500);
    }

    $('#viewswitch').on('switchChange.bootstrapSwitch', function (event, state) {
        if (state) {
            $("#Graphicaldiv").fadeOut(function () {
                $("#Tabular").fadeIn();
            });
        }
        else {
            $("#Tabular").fadeOut(function () {
                $("#Graphicaldiv").fadeIn();
            });
        }
    });
    $timeout($scope.GetReportByCategory, 800);
    setTimeout(function () {
    $scope.rptDateRanges();
    }, 500);
}]);

