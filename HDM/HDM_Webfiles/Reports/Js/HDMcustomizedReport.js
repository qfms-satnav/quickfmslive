﻿app.service("HDMcustomizedReportService", function ($http, $q, UtilityService) {
    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMcustomizedReport/GetCustomizedDetails', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetExportdata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/HDMcustomizedReport/GetCustomizedDetailsExport', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});

app.controller('HDMcustomizedReportController', function ($scope, $q, $http, HDMcustomizedReportService, UtilityService, $timeout, $filter) {
    $scope.HDMcustomized = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Cols = [];
    $scope.CompanyVisible = 0;
    $scope.columnDefs = [];
    // fill company ddls

    var columnDefs = [
        { headerName: "Requisition Id", field: "RequisitionId", width: 130, cellClass: 'grid-align' },
        { headerName: "Main Category", field: "MainCategory", width: 130, cellClass: 'grid-align' },
        { headerName: "Sub Category", field: "SubCategory", width: 140, cellClass: 'grid-align' },
        { headerName: "Child Category", field: "ChildCategory", width: 150, cellClass: 'grid-align' },
        { headerName: "Requested Date", field: "CallLogDate", template: '<span>{{data.CallLogDate | date:"MM-dd-yy"}}</span>', width: 130, cellClass: 'grid-align' },
        { headerName: "Requested By ID", field: "REQUESTEDBY_ID", width: 150, cellClass: 'grid-align' },
        { headerName: "Requested By Name", field: "REQUESTEDBY_NAME", width: 150, cellClass: 'grid-align' },
        { headerName: "Problem Description", field: "Problem_Description", width: 150, cellClass: 'grid-align' },
        { headerName: "Location Code", field: "LCM_CODE", width: 120, cellClass: 'grid-align' },
        { headerName: "Location Name", field: "Location", width: 120, cellClass: 'grid-align' },
        { headerName: "Company", field: "CNP_NAME", width: 120, cellClass: 'grid-align' },
        { headerName: "Latest Remarks", field: "LatestRemarks", width: 100, cellClass: 'grid-align' },
        {
            headerName: "Status", field: "STA_TITLE", width: 100, cellClass: 'grid-align', cellStyle: function (params) {

                const backgroundColor = params.data.STA_COLOR ? params.data.STA_COLOR : 'gray';
                return { color: '#000000', 'color': backgroundColor };

            }
        },
        { headerName: "Closed Time", field: "ClosedTime", template: '<span>{{data.ClosedTime | date:"MM-dd-yy"}}</span>', width: 130, cellClass: 'grid-align' },
        { headerName: "Defined TAT", field: "DEFINED_TAT", width: 100, cellClass: 'grid-align' },
        //{ headerName: "TAT", field: "TAT", width: 100, cellClass: 'grid-align' },
        { headerName: "Response TAT", field: "RESPONSE_TAT", width: 140, cellClass: 'grid-align' },
        { headerName: "Delayed TAT", field: "DELAYED_TAT", width: 140, cellClass: 'grid-align' },
        { headerName: "TAT", field: "TOTAL_TIME", width: 100, cellClass: 'grid-align' },
        { headerName: "Final Amount", field: "ser_claim_amt", width: 100, cellClass: 'grid-align' },
        { headerName: "Assigned TO", field: "HELPDESK", width: 130, cellClass: 'grid-align' },
        { headerName: "L1Manager", field: "L1Manager", width: 100, cellClass: 'grid-align' },
        { headerName: "L2Manager", field: "L2Manager", width: 140, cellClass: 'grid-align' },
        { headerName: "L3Manager", field: "L3Manager", width: 140, cellClass: 'grid-align' },
        { headerName: "L4Manager", field: "L4Manager", width: 100, cellClass: 'grid-align' },
        { headerName: "L5Manager", field: "L5Manager", width: 140, cellClass: 'grid-align' },
        { headerName: "L6Manager", field: "L6Manager", width: 140, cellClass: 'grid-align' },
        { headerName: "Helpdesk Incharge ESC DT", field: "Helpdesk_Escalation_Date", width: 130, cellClass: 'grid-align' },
        { headerName: "L1 Manager ESC DT", field: "L1Manager_Escalation_Date", width: 100, cellClass: 'grid-align' },
        { headerName: "L2 Manager ESC DT", field: "L2Manager_Escalation_Date", width: 140, cellClass: 'grid-align' },
        { headerName: "L3 Manager ESC DT", field: "L3Manager_Escalation_Date", width: 140, cellClass: 'grid-align' },
        { headerName: "L4 Manager ESC DT", field: "L4Manager_Escalation_Date", width: 140, cellClass: 'grid-align' },
        { headerName: "ZONE", field: "ZONE", width: 100, cellClass: 'grid-align', },
        { headerName: "Region", field: "rgn_name", width: 100, cellClass: 'grid-align', },
        { headerName: "Urgency", field: "UGC_NAME", width: 100, cellClass: 'grid-align', },
        { headerName: "Impact", field: "IMP_NAME", width: 100, cellClass: 'grid-align' },
        { headerName: "Raised From", field: "SER_REQUSET_FROM", width: 100, cellClass: 'grid-align' },
        { headerName: "Approved By", field: "Approved_BY", width: 100, cellClass: 'grid-align', },
        { headerName: "Approved Date", field: "Approved_Date", template: '<span>{{data.ClosedTime | date:"dd-MM-yyyy - HH:mm:ss"}}</span>', width: 130, cellClass: 'grid-align' },
        { headerName: "Last Updated By", field: "SER_UPDATED_BY", width: 100, cellClass: 'grid-align' },
        { headerName: "TAT Status", field: "TAT_STATUS", width: 100, cellClass: 'grid-align' },
        { headerName: "Reopen", field: "Reopen_Tickets", width: 100, cellClass: 'grid-align' },




    ];


    $scope.Cols = [

        { COL: "Requisition Id", value: "RequisitionId", ticked: true },
        { COL: "Main Category", value: "MainCategory", ticked: true },
        { COL: "Sub Category", value: "SubCategory", ticked: true },
        { COL: "Child Category", value: "ChildCategory", ticked: true },
        { COL: "Requested Date (dd/mm/yyyy , hh:mm:ss)", value: "CallLogDate", ticked: true },
        { COL: "Requested By ID", value: "REQUESTEDBY_ID", ticked: true },
        { COL: "Requested By Name", value: "REQUESTEDBY_NAME", ticked: true },
        { COL: "Problem Description", value: "Problem_Description", ticked: true },
        { COL: "Location Code", value: "LCM_CODE", ticked: true },
        { COL: "Location Name", value: "Location", ticked: true },
        { COL: "Company", value: "CNP_NAME", ticked: true },
        { COL: "Latest Remarks", value: "LatestRemarks", ticked: true },
        { COL: "Status", value: "STA_TITLE", ticked: true },
        { COL: "Closed Time (dd/mm/yyyy , hh:mm:ss)", value: "ClosedTime", ticked: true },
        { COL: "Defined TAT", value: "DEFINED_TAT", ticked: true },
        { COL: "Response TAT", value: "RESPONSE_TAT", ticked: true },
        { COL: "Delayed TAT", value: "DELAYED_TAT", ticked: true },
        { COL: "TAT", value: "TOTAL_TIME", ticked: true },
        { COL: "Final Amount", value: "ser_claim_amt", ticked: true },
        { COL: "Assigned TO", value: "HELPDESK", ticked: true },
        { COL: "L1Manager", value: "L1Manager", ticked: true },
        { COL: "L2Manager", value: "L2Manager", ticked: true },
        { COL: "L3Manager", value: "L3Manager", ticked: true },
        { COL: "L4Manager", value: "L4Manager", ticked: true },
        { COL: "L5Manager", value: "L5Manager", ticked: true },
        { COL: "L6Manager", value: "L6Manager", ticked: true },
        { COL: "Helpdesk Incharge ESC DT", value: "Helpdesk_Escalation_Date", ticked: true },
        { COL: "L1 Manager ESC DT (dd/mm/yyyy , hh:mm:ss)", value: "L1Manager_Escalation_Date", ticked: true },
        { COL: "L2 Manager ESC DT(dd / mm / yyyy, hh: mm: ss)", value: "L2Manager_Escalation_Date", ticked: true },
        { COL: "L3 Manager ESC DT(dd / mm / yyyy, hh: mm: ss)", value: "L3Manager_Escalation_Date", ticked: true },
        { COL: "L4 Manager ESC DT(dd / mm / yyyy, hh: mm: ss)", value: "L4Manager_Escalation_Date", ticked: true },
        { COL: "ZONE", value: "ZONE", ticked: true },
        { COL: "Region", value: "rgn_name", ticked: true },
        { COL: "Urgency", value: "UGC_NAME", ticked: true },
        { COL: "Impact", value: "IMP_NAME", ticked: true },
        { COL: "Raised From", value: "SER_REQUSET_FROM", ticked: true },
        { COL: "Approved By", value: "Approved_BY", ticked: true },
        { COL: "Approved Date (dd/mm/yyyy , hh:mm:ss)", value: "Approved_Date", ticked: true },
        { COL: "Last Updated By", value: "SER_UPDATED_BY", ticked: true },
        { COL: "TAT Status", value: "TAT_STATUS", ticked: true },
        { COL: "Reopen", value: "Reopen_Tickets", ticked: true },
        


    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        //groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Main Category", field: "MainCategory",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.HDMcustomized.selVal = "THISMONTH";
    $scope.HDMcustomized.selstatus = "1";
    $scope.STATUSCHANGE = function () {
        switch ($scope.HDMcustomized.selstatus) {
            case '1':
                $scope.HDMcustomized.selstatus = "1";
                break;
            case '2':
                $scope.HDMcustomized.selstatus = "2";
                break;
            case '3':
                $scope.HDMcustomized.selstatus = "3";
                break
        }
    }
    $scope.rptDateRanges = function () {
        switch ($scope.HDMcustomized.selVal) {
            case 'SELECT':
                $scope.HDMcustomized.FromDate = "";
                $scope.HDMcustomized.ToDate = "";
                break;
            case 'TODAY':
                $scope.HDMcustomized.FromDate = moment().format('MM/DD/YYYY');
                $scope.HDMcustomized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.HDMcustomized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.HDMcustomized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break
            case '7':
                $scope.HDMcustomized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.HDMcustomized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.HDMcustomized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.HDMcustomized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.HDMcustomized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.HDMcustomized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.HDMcustomized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.HDMcustomized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
        }
    }
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.LoadData = function () {
        if (moment($scope.HDMcustomized.FromDate) > moment($scope.HDMcustomized.ToDate)) {
            showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
            return;
        }
        progress(0, 'Loading...', true);
        if ($scope.HDMcustomized.FromDate == undefined && $scope.HDMcustomized.ToDate == undefined) {
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            var firstDayWithSlashes = ((firstDay.getMonth() + 1)) + '/' + firstDay.getDate() + '/' + firstDay.getFullYear();
            var lastDayWithSlashes = ((lastDay.getMonth() + 1)) + '/' + lastDay.getDate() + '/' + lastDay.getFullYear();

            $scope.HDMcustomized.FromDate = firstDayWithSlashes;
            $scope.HDMcustomized.ToDate = lastDayWithSlashes;
        }

        var cp = _.find($scope.Company, { ticked: true });
        var params = {
            SearchValue: "",
            PageNumber: 1,
            PageSize: 10,
            ZNlst: $scope.HDMcustomized.Zones,
            LCMlst: $scope.HDMcustomized.Locations,
            CHILDlst: $scope.HDMcustomized.child_Category,
            STATUSlst: $scope.HDMcustomized.status,
            HSTSTATUS: $scope.HDMcustomized.selstatus,
            Request_Type: $scope.HDMcustomizedReport.Request_Type,
            FromDate: $scope.HDMcustomized.FromDate,
            ToDate: $scope.HDMcustomized.ToDate,
            CompanyId: cp.CNP_ID
        };

        HDMcustomizedReportService.GetGriddata(params).then(function (response) {

            console.log(params);
            console.log(response);
            $scope.gridata = response.data != undefined ? response.data.lst.griddata : null;
            console.log($scope.gridata);
            if ($scope.gridata.length == 0) {
                $scope.gridOptions.api.setRowData([]);
                progress(0, '', false);
            }
            else {
                // progress(0, 'Loading...', true);
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, '', false);
            }
            var cols = [];
            var unticked = _.filter($scope.Cols, function (item) {
                return item.ticked == false;
            });

            var ticked = _.filter($scope.Cols, function (item) {
                return item.ticked == true;
            });
            //console.log(ticked);
            for (i = 0; i < unticked.length; i++) {
                cols[i] = unticked[i].value;
            }
            $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
            cols = [];
            for (i = 0; i < ticked.length; i++) {
                // console.log(ticked[i]);
                cols[i] = ticked[i].value;
            }
            $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
        });
        //progress(0, '', false);
    }, function (error) {
        console.log(error);
    };

    var clk = true;
    var ExportColumns;

    $scope.ExportData = function () {
        console.log($scope.Company);

    };


    $scope.CustmPageLoad = function () {
    }, function (error) {
        console.log(error);
    }

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns; //[{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" }, { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" }, { title: "Status", key: "STATUS" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("HDMcustomizationReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {

        var cp = _.find($scope.Company, { ticked: true });
        var params = {
            ZNlst: $scope.HDMcustomized.Zones,
            LCMlst: $scope.HDMcustomized.Locations,
            CHILDlst: $scope.HDMcustomized.child_Category,
            STATUSlst: $scope.HDMcustomized.status,
            HSTSTATUS: $scope.HDMcustomized.selstatus,
            Request_Type: $scope.HDMcustomizedReport.Request_Type,
            FromDate: $scope.HDMcustomized.FromDate,
            ToDate: $scope.HDMcustomized.ToDate,
            CompanyId: cp.CNP_ID
        };

        HDMcustomizedReportService.GetExportdata(params).then(function (response1) {

            $scope.Exportdata = response1.griddata;
            progress(0, 'Loading...', true);
            var data = [];
            var dataSet = [];
            var date = /(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/g;
            var datewithtime = /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?/g;
            $scope.Exportdata.forEach((val, index) => {
                if ($scope.Exportdata[index] !== null) {
                    for (var i in $scope.Exportdata[index]) {
                        if (index == 0) {

                            data.push(i);

                        }
                        else {
                            if ((date.test($scope.Exportdata[index][i]))) {
                                var array = $scope.Exportdata[index][i].split('-');
                                var day = parseInt(array[0]);
                                var month = parseInt(array[1]);
                                var year = parseInt(array[2]);

                                data.push(new Date(year, month - 1, day + 1));
                            }
                            else if ((datewithtime.test($scope.Exportdata[index][i]))) {

                                data.push(new Date($scope.Exportdata[index][i]));
                            }
                            else {
                                data.push($scope.Exportdata[index][i]);
                            }
                        }
                    }

                    dataSet.push(data);
                    data = [];
                }
            });


            (function () {
                var urlJSZip = "https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js";
                var urlXLSX = "https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.9.2/xlsx.core.min.js";
                var urlFileSaver = "https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/1.3.3/FileSaver.js";

                $.when($.getScript(urlJSZip), $.getScript(urlXLSX), $.getScript(urlFileSaver)).then(function () {
                    /* original data */
                    var ws_name = "SheetJS";
                    var wb = new Workbook(), ws = sheet_from_array_of_arrays(dataSet);

                    /* add worksheet to workbook */
                    wb.SheetNames.push(ws_name);
                    wb.Sheets[ws_name] = ws;
                    var wbout = XLSX.write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

                    saveAs(new Blob([s2ab(wbout)], { type: "application/octet-stream" }), "Customized Report.xlsx");
                });

                function datenum(v, date1904) {
                    if (date1904) v += 1462;
                    var epoch = Date.parse(v);
                    return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                }

                function sheet_from_array_of_arrays(data, opts) {
                    var ws = {};
                    var range = { s: { c: 10000000, r: 10000000 }, e: { c: 0, r: 0 } };
                    for (var R = 0; R != data.length; ++R) {
                        for (var C = 0; C != data[R].length; ++C) {
                            if (range.s.r > R) range.s.r = R;
                            if (range.s.c > C) range.s.c = C;
                            if (range.e.r < R) range.e.r = R;
                            if (range.e.c < C) range.e.c = C;
                            var cell = { v: data[R][C] };
                            if (cell.v == null) continue;
                            var cell_ref = XLSX.utils.encode_cell({ c: C, r: R });

                            if (typeof cell.v === 'number') cell.t = 'n';
                            else if (typeof cell.v === 'boolean') cell.t = 'b';
                            else if (cell.v instanceof Date) {
                                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                cell.v = datenum(cell.v);
                            }
                            else cell.t = 's';

                            ws[cell_ref] = cell;
                        }
                    }
                    if (range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                    return ws;
                }


                function Workbook() {
                    if (!(this instanceof Workbook)) return new Workbook();
                    this.SheetNames = [];
                    this.Sheets = {};
                }

                function s2ab(s) {
                    var buf = new ArrayBuffer(s.length);
                    var view = new Uint8Array(buf);
                    for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                    return buf;
                }
            })();


            progress(0, 'Loading...', false);
        });
    }

    setTimeout(function () {
        $scope.GenReport = function (Type) {
            if (moment($scope.HDMcustomized.FromDate) > moment($scope.HDMcustomized.ToDate)) {
                showNotification('error', 8, 'bottom-right', UtilityService.DateValidationOnSubmit);
                return;
            }
            progress(0, 'Loading...', true);
            //COL-selected columns      Cols-Total columns           
            if ($scope.COL.length < $scope.Cols.length) {
                var tickedColumns = _.filter($scope.Cols, function (item) {
                    return item.ticked === true;
                });

                // Map backend column names to Grid column names
                var columnHeaders = tickedColumns.map(col => col.COL); // Grid names
                var backendColumns = tickedColumns.map(col => col.value); // Backend field names

                var gridData = [];
                $scope.gridOptions.api.forEachNode(function (rowNode) {
                    var rowData = {};
                    backendColumns.forEach(function (col, index) {
                        rowData[columnHeaders[index]] = rowNode.data[col]; // Use Grid names
                    });
                    gridData.push(rowData);
                });

                var workbook = new ExcelJS.Workbook();
                var worksheet = workbook.addWorksheet("Report");

                worksheet.addRow(columnHeaders);

                const headerRow = worksheet.getRow(1);
                headerRow.eachCell((cell) => {
                    cell.fill = {
                        type: 'pattern',
                        pattern: 'solid',
                        fgColor: { argb: 'FF387cb4' }
                    };
                    cell.font = { color: { argb: 'FFFFFFFF' } };
                });
                gridData.forEach(data => {
                    worksheet.addRow(Object.values(data));
                });

                workbook.xlsx.writeBuffer().then(function (buffer) {
                    var file = new Blob([buffer], {
                        type: 'application/' + Type
                    });

                    var fileURL = URL.createObjectURL(file);
                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'Customizable Report.xlsx'
                    document.body.appendChild(a);
                    a.click();
                    progress(0, 'Loading...', false);
                });
            }
            else {
                woobj = {};
                woobj.Type = Type;
                //if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
                //    if (ConsReport.Type == "pdf") {
                //        $scope.GenerateFilterPdf();
                //    }
                //    else {
                //        $scope.GenerateFilterExcel();
                //    }
                //} else {
                $http({
                    url: UtilityService.path + '/api/HDMcustomizedReport/GetCustomizedDetailsExport',
                    method: 'POST',
                    data: woobj,
                    responseType: 'arraybuffer'

                }).then(function (data, status, headers, config) {
                    var file = new Blob([data.data], {
                        type: 'application/' + Type
                    });
                    //if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    //    window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
                    //}
                    //else if ((navigator.userAgent.indexOf("Firefox") != -1)) {
                    //    window.navigator.msSaveOrOpenBlob(file, 'Consolidated Report.' + Type);
                    //}
                    //   else {
                    //trick to download store a file having its URL
                    var fileURL = URL.createObjectURL(file);
                    $("#reportcontainer").attr("src", fileURL);
                    var a = document.createElement('a');
                    a.href = fileURL;
                    a.target = '_blank';
                    a.download = 'Customizable Report.' + Type;
                    document.body.appendChild(a);
                    a.click();
                    progress(0, 'Loading...', false);
                    // }
                }), function (error, data, status, headers, config) {

                };
            }
            //  };
        };
    }, 2500);

    $scope.zoneSelectAll = function () {
        $scope.HDMcustomized.Zones = $scope.Zones;
        $scope.getlocbyzone();
    }
    $scope.getlocbyzone = function () {
        UtilityService.getlocbyzone($scope.HDMcustomized.Zones).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.locSelectAll = function () {
        $scope.HDMcustomized.Locations = $scope.Locations;

    }
    $scope.getsubbymain = function () {
        UtilityService.getsubbymain($scope.HDMcustomized.Main_Category).then(function (response) {
            $scope.Sub_Category = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.getchildbysub = function () {
        UtilityService.getchildbysub($scope.HDMcustomized.Sub_Category).then(function (response) {
            $scope.child_Category = response.data;
        }, function (error) {
            console.log(error);
        });

    }
    $scope.childchange = function () {

        angular.forEach($scope.child_Category, function (value, key) {
            var sub = _.find($scope.Sub_Category, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sub != undefined && value.ticked == true) {
                sub.ticked = true;
                $scope.HDMcustomized.Sub_Category[0] = sub;
            }
        });

    }
    $scope.mainSelectAll = function () {
        $scope.HDMcustomized.Main_Category = $scope.Main_Category;
        $scope.getsubbymain();
    }
    $scope.subSelectAll = function () {
        $scope.HDMcustomized.Sub_Category = $scope.Sub_Category;
        $scope.getchildbysub();
    }
    $scope.childSelectAll = function () {
        $scope.HDMcustomized.child_Category = $scope.child_Category;
        $scope.Getchildcat();
    }


    $scope.PageLoad = function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                if (a)
                    a.ticked = true;
                if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
                UtilityService.Getallzones().then(function (response) {
                    if (response.data != null) {
                        $scope.Zones = response.data;
                        angular.forEach($scope.Zones, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.GetLocationsall(1).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                });
                                UtilityService.Getmaincat().then(function (response) {
                                    if (response.data != null) {
                                        $scope.Main_Category = response.data;
                                        angular.forEach($scope.Main_Category, function (value, key) {
                                            value.ticked = true;
                                        });
                                        UtilityService.Getsubcat().then(function (response) {
                                            if (response.data != null) {
                                                $scope.Sub_Category = response.data;
                                                angular.forEach($scope.Sub_Category, function (value, key) {
                                                    value.ticked = true;
                                                });
                                                UtilityService.Getchildcat().then(function (response) {
                                                    if (response.data != null) {
                                                        $scope.child_Category = response.data;
                                                        angular.forEach($scope.child_Category, function (value, key) {
                                                            value.ticked = true;
                                                        });
                                                        UtilityService.Getstatus().then(function (response) {
                                                            if (response.data != null) {
                                                                $scope.status = response.data;
                                                                angular.forEach($scope.status, function (value, key) {
                                                                    value.ticked = true;
                                                                });
                                                                setTimeout(function () {
                                                                    $scope.LoadData();
                                                                }, 3000);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }


    $scope.PageLoad();

    //setTimeout(function () {
    //    $scope.LoadData();
    //}, 3000);


});
