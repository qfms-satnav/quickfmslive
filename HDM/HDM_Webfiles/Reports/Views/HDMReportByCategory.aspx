﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.css" rel="stylesheet" />
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/highlight.css" rel="stylesheet" />
    <script type="text/javascript" defer>        
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        hr {
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="HDMReportByCategoryController" class="amantra">
    <div class="animsition" ng-cloak>
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Category Report</h3>
            </div>
            <div class="card">
                <form role="form" id="HDMRptByCategory" name="frmHDMRptByCategory" data-valid-submit="GetReportByCategory()">
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Select Range<span style="color: red;">*</span></label>
                                <br />
                                <select id="ddlRange" class="selectpicker" ng-model="RptByCategory.selVal" data-ng-change="rptDateRanges()">
                                    <option value="SELECT">Select Range</option>
                                    <option value="TODAY">Today</option>
                                    <option value="YESTERDAY">Yesterday</option>
                                    <option value="7">Last 7 Days</option>
                                    <option value="30">Last 30 Days</option>
                                    <option value="THISMONTH">This Month</option>
                                    <option value="LASTMONTH">Last Month</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">From Date <span style="color: red;">*</span></label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="RptByCategory.FromDate" id="FROM_DATE" name="FROM_DATE" required="" placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">To Date <span style="color: red;">*</span></label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="RptByCategory.ToDate" id="TO_DATE" name="TO_DATE" required="" placeholder="mm/dd/yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                            <div class="form-group">
                                <label class="control-label">Company</label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="RptByCategory.CNP_NAME" button-label="icon CNP_NAME"
                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="RptByCategory.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmHDMRptByCategory.$submitted && frmHDMRptByCategory.Company.$invalid" style="color: red">Please Select Company </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" style="padding-left: 18px">
                        <div class="col-md-6">
                            <label>View In: </label>
                            <input id="viewswitch" type="checkbox" checked data-size="small"
                                data-on-text="<span class='fa fa-table'></span>"
                                data-off-text="<span class='fa fa-bar-chart'></span>" />
                        </div>

                        <div class="col-md-6" id="table2">
                            <br />
                            <a data-ng-click="GenReport(RptByCategory,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport(RptByCategory,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport(RptByCategory,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>
                    </div>
                    <%-- </form>

                            <form id="form2">--%>

                    <div id="Tabular" data-ng-show="RptByCategoryGrid">
                        <div class="row" style="padding-left: 17px;">
                            <div class="col-md-12" style="height: 350px">
                                <div class="input-group" style="width: 20%">
                                    <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary custom-button-color" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div id="Graphicaldiv">
                            <%--<div id="LocationGraph" >&nbsp</div>--%>
                            <div id="SubCatGraph">&nbsp</div>
                        </div>
                    </div>
                </form>


                <div class="modal fade bs-example-modal-lg col-md-12 " id="historymodal">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="panel-group box box-primary" id="Div2">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <div class="panel-heading ">
                                        <h4 class="panel-title modal-header-primary" style="padding-left: 17px" data-target="#collapseTwo">History Details</h4>
                                        <br />
                                        <form role="form" name="form2" id="form3">
                                            <div class="clearfix">
                                                <div class="col-md-12">
                                                    <div class="box">
                                                        <div class="box-danger table-responsive">
                                                            <div data-ag-grid="PopOptions" class="ag-blue" style="height: 305px; width: auto"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>


    <script src="../../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js" defer></script>

    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../Js/HDMReportByCategory.js" defer></script>
    <script src="../../../../SMViews/Utility.min.js" defer></script>
    <script src="../../../../Scripts/moment.min.js" defer></script>

    <script type="text/javascript" defer>
        var CompanySession = '<%= Session["COMPANYID"]%>';
        $(document).ready(function () {
            setDateVals();
        });

        function setDateVals() {
            $('#FROM_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#TO_DATE').datepicker({
                format: 'dd-M-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FROM_DATE').datepicker('setDate', new Date(moment().subtract(1, 'days').format('DD-MMM-YYYY')));
            $('#TO_DATE').datepicker('setDate', new Date(moment().format('DD-MMM-YYYY')));
        }
    </script>
</body>
</html>
