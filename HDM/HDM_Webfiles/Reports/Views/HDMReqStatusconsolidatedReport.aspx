﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../../BootStrapCSS/Bootstrapswitch/css/bootstrap-switch.min.css" rel="stylesheet" />
    <style>
        /*.grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }*/

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        #excel1 {
            color: #2AE214;
        }

        /* .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }*/
        .ag-blue .ag-cell:first-of-type {
            justify-content: left !important;
        }

        .modal.fade .modal-dialog {
            transform: translate(0px, -150px);
        }
    </style>
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd/M/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="HDMReqStatusconsolidatedReportController" class="amantra">
    <div class="animsition" ng-cloak>
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Age of Open Calls Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Summary Report</h3>
            </div>
            <div class="card">
                <%-- <div class="panel-body" style="padding-right: 10px;">--%>
                <form id="form1" name="frmCons" data-valid-submit="BindGrid()">
                    <div class="clearfix">
                        <div class="row">
                            <%-- <div class="col-md-2 col-sm-6 col-xs-12">
                            <label>View In: </label>
                            <input id="viewswitch" type="checkbox" checked data-size="small"
                                data-on-text="<span class='fa fa-table'></span>"
                                data-off-text="<span class='fa fa-bar-chart'></span>" />
                        </div>--%>
                            <%--  <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                        <div class="form-group">
                                            <label class="control-label">Company</label>
                                            <div isteven-multi-select data-input-model="Company" data-output-model="ConsRpt.CNP_NAME" button-label="icon CNP_NAME"
                                                item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="ConsRpt.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                            <span class="error" data-ng-show="frmCons.$submitted && frmCons.Company.$invalid" style="color: red">Please Select Company </span>
                                        </div>
                                    </div>--%>
                            <%--     <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">History Status<span style="color: red;"></span></label>
                                            <br />
                                            <select id="ddlstatus" class="selectpicker" ng-model="ConsRpt.selstatus" data-ng-change="STATUSCHANGE()">
                                                <option value="1" >Active</option>
                                                <option value="2">In-Active</option>
                                                <option value="3">All</option>
                                            </select>
                                        </div>
                                    </div>--%>

                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <div isteven-multi-select data-input-model="Citys" data-output-model="ConsRpt.CTY_NAME" button-label="icon CTY_NAME"
                                        item-label="icon CTY_NAME" tick-property="ticked" data-on-item-click="GetLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="ConsRpt.CTY_NAME" name="CTY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmCons.$submitted && frmCons.CTY_NAME.$invalid" style="color: red">Please Select City </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <div isteven-multi-select data-input-model="Locations" data-output-model="ConsRpt.LCM_NAME" button-label="icon LCM_NAME"
                                        item-label="icon LCM_NAME" tick-property="ticked" data-on-select-all="locSelectAll()" data-on-select-none="" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="ConsRpt.LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmCons.$submitted && frmCons.Locations.$invalid" style="color: red">Please Select Location </span>
                                </div>
                            </div>
                            <%--                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                            <div class="form-group">
                                <label class="control-label">Employee</label>
                                <div isteven-multi-select data-input-model="Employees" data-output-model="ConsRpt.AUR_ID" button-label="icon AUR_KNOWN_AS"
                                    item-label="icon AUR_KNOWN_AS" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="ConsRpt.AUR_ID" name="AUR_ID" style="display: none" required="" />
                                <span class="error" data-ng-show="frmCons.$submitted && frmCons.Employees.$invalid" style="color: red">Please Select Employee </span>
                            </div>
                        </div>--%>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">From Date</label>
                                    <div class="input-group date" id='fromdate'>
                                        <input type="text" class="form-control" data-ng-model="HDMReqconsolidated.FromDate" id="Text1" name="FromDate" required="" placeholder="dd/mm/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="form1.$submitted && form1.FromDate.$invalid" style="color: red;">Please Select From Date</span>
                                </div>
                            </div>
                            <%-- </div>
                        <div class="clearfix">--%>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">To Date</label>
                                    <div class="input-group date" id='todate'>
                                        <input type="text" class="form-control" data-ng-model="HDMReqconsolidated.ToDate" id="Text2" name="ToDate" required="" placeholder="dd/mm/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="form1.$submitted && form1.ToDate.$invalid" style="color: red;">Please Select To Date</span>
                                </div>
                            </div>

                            <div class="clearfix">

                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <label class="control-label">Employee</label>
                                        <div isteven-multi-select data-input-model="Employees" data-output-model="ConsRpt.AUR_ID" button-label="icon AUR_KNOWN_AS"
                                            item-label="icon AUR_KNOWN_AS" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                        </div>
                                        <input type="text" data-ng-model="ConsRpt.AUR_ID" name="AUR_ID" style="display: none" required="" />
                                        <span class="error" data-ng-show="frmCons.$submitted && frmCons.Employees.$invalid" style="color: red">Please Select Employee </span>
                                    </div>
                                </div>
                                <div class="col-md-9 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="box-footer text-right" style="padding-top: 20px">
                                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="Search()" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2 col-sm-6 col-xs-12">
                                <label>View In : </label>
                                <input id="viewswitch" type="checkbox" checked data-size="small"
                                    data-on-text="<span class='fa fa-table'></span>"
                                    data-off-text="<span class='fa fa-bar-chart'></span>" />


                                <div class="col-md-1 col-sm-6 col-xs-6 text-right" id="table2" data-ng-show="GridVisiblity">
                                    <br />
                                    <%-- <a data-ng-click="GenReport('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>--%>
                                    <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <%--  <a data-ng-click="GenReport('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>
                                </div>
                            </div>
                            <%--   <div class="col-md-4 col-sm-6 col-xs-12 text-right" id="table3" data-ng-show="GridVisiblityZone">
                                        <br />
                                        <a data-ng-click="GenReportZone('doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReportZone('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReportZone('pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                    </div>--%>
                        </div>
                </form>
                <br />
                <form id="form2">

                    <div id="Tabular">
                        <div class="col-md-12">
                            <div class="panel-heading ">
                                <ul class="nav nav-tabs">
                                    <li><a href="#tabSummary" class="active" data-ng-click="BindGrid()" data-toggle="tab">Summary</a></li>
                                    <%-- <li><a href="#tabZone" data-ng-click="BindGrid_Zone()" data-toggle="tab">Zone Wise</a></li>--%>
                                </ul>
                            </div>


                            <div class="panel-body">
                                <div class="tab-content">
                                    <div class="tab-pane fade in active show" id="tabSummary">
                                        <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                    </div>
                                    <div class="tab-pane fade" id="tabZone">
                                        <input type="text" class="form-control" id="filteredzone" placeholder="Filter by any..." style="width: 25%" />
                                        <div data-ag-grid="gridzone" class="ag-blue" style="height: 310px; width: auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div id="Graphicaldiv">
                        <div id="HDMconsolidatedGraph">&nbsp</div>
                    </div>
                    <div class="modal fade" id="RequestTypeDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="H1">
                                        <label for="lblASTmsg" class="control-label"></label>
                                    </h4>
                                </div>
                                <div class="modal-body">
                                    <a data-ng-click="GenChartReport('xls')"><i id="excel1" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                    <table id="tblassetTypes" class="table table-condensed table-bordered table-hover table-striped">
                                        <tr>
                                            <th>SNO</th>
                                            <th>Request ID</th>
                                            <th>Location</th>
                                            <th>Child Category</th>
                                            <th>Requisition Date</th>
                                            <th>Status</th>
                                            <th>Raised By</th>
                                            <th>Assigned To</th>
                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <%-- </div>
            </div>--%>
    </div>
    <%-- </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/bootstrap-switch.min.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js" defer></script>
    <script src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js" defer></script>


    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>

    <script defer>

        //agGrid.initialiseAgGridWithAngular1(angular);
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../../Scripts/moment.min.js" defer></script>
    <script src="../../../../SMViews/Utility.js" defer></script>
    <script src="../Js/HDMReqStatusconsolidatedReport.js" defer></script>
</body>

<script type="text/javascript" defer>
    var CompanySession = '<%= Session["COMPANYID"]%>';
</script>




</html>
