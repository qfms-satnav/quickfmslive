﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmHDMAdminRequisitionDetails.aspx.vb"
    Inherits="HDM_HDM_Webfiles_frmHDMAdminRequisitionDetails" Title="View/Modify Requisition Details" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>View & Assign Details
                        </legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Requisition Id </label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="lblReqId" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Raise by</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtEmployee" runat="server" ReadOnly="true" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Assigned Employee </label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlEmp" runat="server" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Location </label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Service Category </label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlServiceCategory" runat="server" AutoPostBack="True" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Service Type</label>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlServiceType" runat="server" AutoPostBack="True" Enabled="false" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Service Required At </label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" ReadOnly="true" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">
                                            Status</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtStatus" runat="server" CssClass="form-control" ReadOnly="true" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Problem Description </label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="trassignedto" runat="server" class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Assigned To <span style="color: red;">*</span></label>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAssign"
                                            Display="None" ErrorMessage="Please Enter Assigned To" InitialValue="--Select--" ValidationGroup="Val1"></asp:RequiredFieldValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlAssign" runat="server" AutoPostBack="True" Enabled="true" CssClass="selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tradminremarks" runat="server" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Admin Remarks <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtAdminRemarks"
                                            Display="None" ErrorMessage="Please Enter Admin Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtAdminRemarks" runat="server" TextMode="MultiLine" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="trSIRemarks" runat="server" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Service In Charge Remarks </label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtSIRemarks" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tradmin2" runat="server" class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Admin Remarks </label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtadmin2" runat="server" TextMode="MultiLine" CssClass="form-control" Enabled="false">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnAssign" Text="Assign" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" CausesValidation="true" Visible="true" />
                                    <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="btn btn-primary custom-button-color" OnClick="btnBack_Click" />
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

