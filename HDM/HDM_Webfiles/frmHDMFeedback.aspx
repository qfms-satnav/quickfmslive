<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmHDMFeedback.aspx.vb" Inherits="HDM_HDM_Webfiles_frmHDMFeedback" Title="Feedback" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>

    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Feedback</legend>
                    </fieldset>
                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger"
                            ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Requested By</label>

                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtreq" runat="server" CssClass="form-control" ReadOnly="TRUE"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Requisition ID</label>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control" ReadOnly="TRUE"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Request Description</label>

                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtreqdesc" runat="server" CssClass="form-control" ReadOnly="TRUE"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Give Feedback by entering Comments<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="none"
                                            ErrorMessage="Please Enter Comments " ControlToValidate="txtcomments" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:TextBox ID="txtcomments" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" MaxLength="1000"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Feedback<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="none"
                                            ErrorMessage="Please Select Feedback " ControlToValidate="ddlfeedback" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlfeedback" runat="server" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                <asp:ListItem Value="Excellent">Excellent</asp:ListItem>
                                                <asp:ListItem Value="VeryGood">Very Good</asp:ListItem>
                                                <asp:ListItem Value="Good">Good</asp:ListItem>
                                                <asp:ListItem Value="Satisfactory">Satisfactory</asp:ListItem>
                                                <asp:ListItem Value="Poor">Poor</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" CausesValidation="true"
                                        ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary custom-button-color" Text="Back" CausesValidation="true" />
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvitems" runat="server" AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false" EmptyDataText="No Records Found"
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsno" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Requested By">
                                            <ItemTemplate>
                                                <asp:Label ID="lblreqstner" runat="server" Text='<%#Eval("REQUISITIONER") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Id">
                                            <ItemTemplate>
                                                <asp:Label ID="lblid" runat="server" Text='<%#Eval("REQUEST_ID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Request Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lblreqdesc" runat="server" Text='<%#Eval("SER_DESCRIPTION") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Feedback">
                                            <ItemTemplate>
                                                <asp:Label ID="lblfeedback" runat="server" Text='<%#Eval("COMMENTS")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comments">
                                            <ItemTemplate>
                                                <asp:Label ID="lblComments" runat="server" Text='<%#Eval("FEEDBACK") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lbldate" runat="server" Text='<%#Eval("FEEDBACK_DATE") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

