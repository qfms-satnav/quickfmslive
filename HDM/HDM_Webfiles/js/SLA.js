﻿app.service("SLAService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    var deferred = $q.defer();
    //to save
    this.SaveSLADetails = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SLA/SaveDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //update
    this.UpdateSLADetails = function (dataobject) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SLA/UpdateDetails', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //edit
    this.EditSLADetails = function (dt) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SLA/EditSLA/', dt)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMainCategory = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SLA/GetMaincategory')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetCountries = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SLA/GetCountries')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetCityByCountry = function (SLA_CNY_CODE) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SLA/GetCityByCountry/' + SLA_CNY_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetLocationByCity = function (SLA_CTY_CODE) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SLA/GetLocationByCity/' + SLA_CTY_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    ///Amantra_Multi_UI/api/SLA/GetSubCategoryByMain/4444
    this.GetSubCategoryByMain = function (SLA_MNC_CODE) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SLA/GetSubCategoryByMain/' + SLA_MNC_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //to get child by sub category
    this.GetChildCategoryBySub = function (SLA_SUBC_CODE) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SLA/GetChildCategoryBySubCat/' + SLA_SUBC_CODE)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //For Timer View
    this.GetSLATime = function (id) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SLA/GetSLATime/' + id)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //For Grid
    this.GetSLAList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/SLA/GetSLAGrid')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.AddSla = function (dataobject) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SLA/AddSla', dataobject)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('SLAcontroller', ['$scope', '$q', 'SLAService', '$timeout', 'ChildCatApprovalLocMatrixService', 'MainCategoryService', 'HDMUtilityService', 'UtilityService', 'SubCategoryService', 'ChildCategoryService' ,function ($scope, $q, SLAService, $timeout, ChildCatApprovalLocMatrixService, MainCategoryService, HDMUtilityService, UtilityService, SubCategoryService, ChildCategoryService) {
    $scope.SLAMaster = {};
    $scope.ActionStatus = 0;
    $scope.countrylist = [];
    $scope.cny = [];
    $scope.Citylist = [];
    $scope.locationlist = [];
    $scope.maincategorylist = [];
    $scope.SubCategorylist = [];

    $scope.ChildCategorylist = [];
    $scope.populationlist = [];
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
    $scope.EnableStatus = 0;
    //$scope.timetype = [{ value: "1", Name: "Minute(s)" }, { value: "60", Name: "Hour(s)" }, { value: "1440", Name: "Day(s)" }];
    $scope.EditMode = false;

    setTimeout(function () {
        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;

                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Location = response.data;
                            }

                            UtilityService.getSysPreferences().then(function (response) {
                                if (response.data != null) {
                                    $scope.SysPref = response.data;
                                    var prefval = _.find($scope.SysPref, { SYSP_CODE: "BUSS_HRS_MINS" })
                                    if ((prefval == undefined) || prefval.SYSP_VAL2 == undefined) {

                                        $scope.timetype = [{ value: "1", Name: "Minute(s)" }, { value: "60", Name: "Hour(s)" }, { value: "1440", Name: "Day(s)" }];
                                        $scope.AddSlahide = true;
                                    }
                                    else if (prefval.SYSP_VAL2 == 540) {
                                        $scope.timetype = [{ value: "1", Name: "Minute(s)" }, { value: "60", Name: "Hour(s)" }, { value: "540", Name: "Day(s)" }];
                                        console.log($scope.timetype)
                                        $scope.AddSlahide = false;
                                    }
                                }
                            });
                        });
                    }
                });

            }
        });
    }, 1000)

    $scope.CnyChangeAll = function () {
        $scope.SLAMaster.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.SLAMaster.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.SLAMaster.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.SLAMaster.Country, 2).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }

    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.SLAMaster.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.SLAMaster.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        $scope.SLAMaster.Country = [];

        UtilityService.getLocationsByCity($scope.SLAMaster.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SLAMaster.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.SLAMaster.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.SLAMaster.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {

        $scope.SLAMaster.Country = [];
        $scope.SLAMaster.City = [];
        UtilityService.getTowerByLocation($scope.SLAMaster.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true && cny.ticked == false) {
                cny.ticked = true;
                $scope.SLAMaster.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true && cty.ticked == false) {
                cty.ticked = true;
                $scope.SLAMaster.City.push(cty);
            }
        });


    }

    HDMUtilityService.getMainCategories().then(function (response) {
        if (response.data != null) {
            $scope.Main = response.data;

            HDMUtilityService.getAllSubCategories().then(function (response) {
                if (response.data != null) {
                    $scope.Sub = response.data;

                    HDMUtilityService.getAllChildCategories().then(function (response) {
                        if (response.data != null) {
                            $scope.Child = response.data;
                        }
                    });
                }
            });

        }
    });
    //// Main Category Events
    $scope.MainChangeAll = function () {
        $scope.SLAMaster.Main = $scope.Main;
        $scope.MainChanged();
    }

    $scope.MainSelectNone = function () {
        $scope.SLAMaster.Main = [];
        $scope.MainChanged();
    }

    $scope.MainChanged = function () {
        if ($scope.SLAMaster.Main.length != 0) {
            HDMUtilityService.getSubCatbyMainCategories($scope.SLAMaster.Main, 2).then(function (response) {
                if (response.data != null)
                    $scope.Sub = response.data;
                else
                    $scope.Sub = [];
            });
        }
        else
            $scope.Sub = [];
    }

    //// Sub Category Events
    $scope.SubChangeAll = function () {
        $scope.SLAMaster.Sub = $scope.Sub;
        $scope.SubChanged();
    }
    $scope.SubSelectNone = function () {
        $scope.SLAMaster.Sub = [];
        $scope.SubChanged();
    }

    $scope.SubChanged = function () {
        $scope.SLAMaster.Main = [];

        HDMUtilityService.getChildCatBySubCategories($scope.SLAMaster.Sub, 2).then(function (response) {
            if (response.data != null)
                $scope.Child = response.data;
            else
                $scope.Child = [];
        });

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Sub, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.SUBC_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.SLAMaster.Main.push(mn);
            }
        });
    }

    ///// Child Category Events
    $scope.ChildChangeAll = function () {
        $scope.SLAMaster.Child = $scope.Child;
        $scope.ChildChanged();
    }

    $scope.ChildSelectNone = function () {
        $scope.SLAMaster.Child = [];
        $scope.ChildChanged();
    }

    $scope.ChildChanged = function () {

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Child, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.SLAMaster.Main.push(mn);
            }
        });

        angular.forEach($scope.Child, function (value, key) {
            var sb = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sb != undefined && value.ticked == true && sb.ticked == false) {
                sb.ticked = true;
                $scope.SLAMaster.Sub.push(sb);
            }
        });
        //For Timer view
        if ($scope.SLAMaster.Module[0].HDM_MAIN_MOD_ID != undefined) {
            SLAService.GetSLATime($scope.SLAMaster.Module[0].HDM_MAIN_MOD_ID).then(function (response) {
                $scope.Roles = response.ROLELST;
                $scope.SLADetails = response.SLADET;
            }, function (error) {
            });
        }
        else {
            SLAService.GetSLATime($scope.SLAMaster.Module).then(function (response) {
                $scope.Roles = response.ROLELST;
                $scope.SLADetails = response.SLADET;
            }, function (error) {
            });
        }

    }
    //main category drop down bind

    //to bind city on country change
    $scope.CountryChanged = function () {
        $scope.cny.push($scope.SLAMaster.SLA_CNY_CODE);
        SLAService.GetCityByCountry($scope.SLAMaster.SLA_CNY_CODE).then(function (data) {
            $scope.SLAMaster.SLA_CTY_CODE = "";
            $scope.Citylist = data;
        }, function (error) {
            console.log(error);
        });
    }

    //location by city
    $scope.CityChanged = function () {
        SLAService.GetLocationByCity($scope.SLAMaster.SLA_CTY_CODE).then(function (data) {
            $scope.SLAMaster.SLA_LOC_CODE = "";
            $scope.locationlist = data;
        }, function (error) {
            console.log(error);
        });
    }

    //to bind subcategory on main category change
    $scope.MainCategoryChanged = function () {
        SLAService.GetSubCategoryByMain($scope.SLAMaster.SLA_MNC_CODE).then(function (data) {
            $scope.SLAMaster.SLA_SUBC_CODE = "";
            $scope.Roles = [];
            $scope.SLADetails = [];
            $scope.SubCategorylist = data;
        }, function (error) {
            console.log(error);
        });
    }

    //to bind childcategory on sub category change
    $scope.SubCategoryChanged = function () {
        SLAService.GetChildCategoryBySub($scope.SLAMaster.SLA_SUBC_CODE).then(function (data) {
            $scope.SLAMaster.SLA_CHC_CODE = "";
            $scope.Roles = [];
            $scope.SLADetails = [];
            $scope.ChildCategorylist = data;
        }, function (error) {
            console.log(error);
        });
    }

    //For Grid column Headers
    var columnDefs = [
        { headerName: "Country", field: "CNY_NAME", cellClass: "grid-align", filter: 'set' },
        { headerName: "City", field: "CTY_NAME", cellClass: "grid-align" },
        { headerName: "Location", field: "LCM_NAME", cellClass: "grid-align" },
        { headerName: "Main Category", field: "MNC_NAME", cellClass: "grid-align" },
        { headerName: "Sub Category", field: "SUBC_NAME", cellClass: "grid-align" },
        { headerName: "Child Category", field: "CHC_TYPE_NAME", cellClass: "grid-align" },
        { headerName: "Edit", suppressMenu: true, template: '<a data-ng-click ="EditSLA(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)" }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        SLAService.GetSLAList().then(function (data) {
            if (data != null) {
                $scope.gridata = data;
                $scope.gridOptions.api.setRowData(data);
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Data not found');
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableScrollbars: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    };
    //$scope.pageSize = '10';

    $scope.createNewDatasource = function () {
        var dataSource = {
            pageSize: parseInt($scope.pageSize),
            getRows: function (params) {
                setTimeout(function () {
                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($scope.gridata.length <= params.endRow) {
                        lastRow = $scope.gridata.length;
                    }
                    params.successCallback(rowsThisPage, lastRow);
                }, 500);
            }
        };
        $scope.gridOptions.api.setDatasource(dataSource);
    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    setTimeout(function () {
        $scope.LoadData();
    }, 1500);

    $scope.AddSla = function () {
        $scope.dataobject = { Lcmlst: $scope.SLAMaster.Location };
        if ($scope.SLAMaster.Location.length == 0) {
            showNotification('error', 8, 'bottom-right', 'Select a Location');
            return;
        }
        //if (Lcmlst.length)
        SLAService.AddSla($scope.dataobject).then(function (dataobj) {

            progress(0, '', false);
            $scope.clear();

            //$scope.gridOptions.api.refreshView()
            showNotification('success', 8, 'bottom-right', 'Data successfully uploaded');
            SLAService.GetSLAList().then(function (data) {
                if (data != null) {
                    $scope.gridata = data;
                    $scope.gridOptions.api.setRowData(data);
                }
            });


        });



    }


    //To save a record
    $scope.SaveDetails = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            //$scope.dataobject = { SLA: $scope.SLAMaster, SLADET: $scope.SLADetails, UPLSTA: $scope.RetStatus };
            $scope.dataobject = { SLA_ID: $scope.SLAMaster.SLA_ID, Lcmlst: $scope.SLAMaster.Location, SLA: $scope.SLAMaster.Child, SLADET: $scope.SLADetails, selected: $scope.SLA };
            SLAService.UpdateSLADetails($scope.dataobject).then(function (dataobj) {
                progress(0, '', false);
                $scope.clear();
                SLAService.GetSLAList().then(function (data) {
                    if (data != null) {
                        $scope.gridata = data;
                        $scope.gridOptions.api.setRowData(data);
                    }
                });
                showNotification('success', 8, 'bottom-right', 'Data successfully uploaded');

            }, function (response) {
                progress(0, '', false);
            });
        }
        else {
            var count = 0;
            var keepGoing = true;
            angular.forEach($scope.SLADetails, function (value) {
                count = 0;
                if (keepGoing) {
                    angular.forEach(value.Value, function (innerval) {
                        if (innerval.Value.SLAD_ESC_TIME != 0) {
                            count = count + 1;
                        }
                    });
                    if (count == 0) {
                        keepGoing = false;
                        progress(0, '', false);
                        showNotification('error', 8, 'bottom-right', 'Enter at least one value for the each Status');
                    }
                }
            });

            if (keepGoing) {
                $scope.dataobject = { Lcmlst: $scope.SLAMaster.Location, SLA: $scope.SLAMaster.Child, SLADET: $scope.SLADetails, selected: $scope.SLA };
                SLAService.SaveSLADetails($scope.dataobject).then(function (dataobj) {
                    if (dataobj.data.SLA_ID == 0) {
                        $scope.clear();
                        SLAService.GetSLAList().then(function (data) {
                            if (data != null) {
                                $scope.gridata = data;
                                $scope.gridOptions.api.setRowData(data);
                            }
                        });
                        progress(0, '', false);
                        showNotification('success', 8, 'bottom-right', dataobj.Message);
                    }
                    else {
                        $scope.gridata.unshift(dataobj.data);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.clear();
                        SLAService.GetSLAList().then(function (data) {
                            if (data != null) {
                                $scope.gridata = data;
                                $scope.gridOptions.api.setRowData(data);
                            }
                        });
                        progress(0, '', false);
                        showNotification('success', 8, 'bottom-right', dataobj.Message);
                    }
                }, function (response) {
                    progress(0, '', false);
                });
            }
        }
    }



    //clear 
    $scope.clear = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Child, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Module, function (value, key) {
            value.ticked = false;
        });
        $scope.SLAMaster = {};
        $scope.SLADetails = false;
        $scope.ActionStatus = 0;
        $scope.EnableStatus = 0;
        $scope.IsInEdit = false;
        $scope.EditMode = false;
        $scope.frmSLA.$submitted = false;
    }

    //Edit
    $scope.EditSLA = function (data) {
        console.log(data);
        console.log($scope.SLAMaster);
        progress(0, 'Loading...', true);
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Child, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Module, function (value, key) {
            value.ticked = false;
        });

        var Mod = _.find($scope.Module, { HDM_MAIN_MOD_ID: data.HDM_MAIN_MOD_ID });
        if (Mod != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    Mod.ticked = true;
                    $scope.SLAMaster.Module.push(Mod);
                });
            }, 550)
        }

        var cny = _.find($scope.Country, { CNY_CODE: data.SLA_CNY_CODE });
        console.log(cny);
        if (cny != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cny.ticked = true;
                    $scope.SLAMaster.Country.push(cny);
                });
            }, 550)
        }



        var cty = _.find($scope.City, { CTY_CODE: data.SLA_CTY_CODE });
        if (cty != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    cty.ticked = true;
                    $scope.SLAMaster.City.push(cty);
                });
            }, 550)
        }
        var lcm = _.find($scope.Location, { LCM_CODE: data.SLA_LOC_CODE });
        if (lcm != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    lcm.ticked = true;
                    $scope.SLAMaster.Location.push(lcm);
                });
            }, 550)
        }
        var main = _.find($scope.Main, { MNC_CODE: data.SLA_MNC_CODE });
        if (main != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    main.ticked = true;
                    $scope.SLAMaster.Main.push(main);
                });
            }, 550)
        }
        var sub = _.find($scope.Sub, { SUBC_CODE: data.SLA_SUBC_CODE });
        if (sub != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    sub.ticked = true;
                    $scope.SLAMaster.Sub.push(sub);
                });
            }, 550)
        }
        var child = _.find($scope.Child, { CHC_TYPE_CODE: data.SLA_CHC_CODE });
        if (child != undefined) {
            setTimeout(function () {
                $scope.$apply(function () {
                    child.ticked = true;
                    $scope.SLAMaster.Child.push(child);
                });
            }, 550)
        }

        $scope.ActionStatus = 1;
        $scope.EnableStatus = 1;
        $scope.IsInEdit = true;

        var dt = {
            SLA_ID: data.SLA_ID,
            MOD_ID: data.HDM_MAIN_MOD_ID
        }
        console.log(dt);
        SLAService.EditSLADetails(dt).then(function (response) {
            SLAService.GetSubCategoryByMain(data.SLA_MNC_CODE).then(function (subcatData) {     //sub cat ddl
                $scope.SubCategorylist = subcatData;
                SLAService.GetChildCategoryBySub(data.SLA_SUBC_CODE).then(function (childcatData) {     //child cat ddl
                    $scope.ChildCategorylist = childcatData;
                    SLAService.GetCityByCountry(data.SLA_CNY_CODE).then(function (cityData) {        //city ddl
                        $scope.Citylist = cityData;
                        SLAService.GetLocationByCity(data.SLA_CTY_CODE).then(function (locData) {  //location ddl   
                            $scope.locationlist = locData;
                            $scope.SLAMaster = data;
                            $scope.Roles = response.ROLELST;
                            $scope.SLADetails = response.SLADET;
                        }, function (error) {
                            console.log(error);
                        });
                    }, function (error) {
                        console.log(error);
                    });
                }, function (error) {
                    console.log(error);
                });
            }, function (error) {
                console.log(error);
            });
            progress(0, '', false);
        }, function (response) {
            progress(0, '', false);
        });
    }

    UtilityService.getCountires(1).then(function (response) {
        $scope.countrylist = response.data;
    }, function (error) {
        console.log(error);
    });

    function getmaincat() {
        SLAService.GetMainCategory().then(function (data) {
            $scope.maincategorylist = data;
        }, function (error) {
            console.log(error);
        });
    }

    $timeout($scope.getcountires, 200)
    $timeout(getmaincat(), 500)

    $scope.PageLoad = function () {
        MainCategoryService.HelpDeskModuleHide().then(function (response) {
            var result = response.data;
            if (result == "1") {
                MainCategoryService.GetCompanyModules().then(function (response) {
                    $scope.Module = response.data.data;
                    $scope.SLAMaster.Module = $scope.Module;
                });
            } else {
                $scope.HelpDeskModule = true;
                $scope.SLAMaster.Module = "1";
            }

        });
    }

    $scope.PageLoad();
    $scope.getMainCategories = function () {
        var params = {
            CompanyId: $scope.SLAMaster.Module[0].HDM_MAIN_MOD_ID
        }
        SubCategoryService.GetMainCategoryByModule(params).then(function (response) {
            $scope.Main = response.data;
            $scope.SLAMaster.Main = $scope.Main;
            angular.forEach($scope.Sub, function (value, key) {
                value.ticked = false;
            });
            angular.forEach($scope.Child, function (value, key) {
                value.ticked = false;
            });
        }, function (error) {
            console.log(error);
        });

        ChildCatApprovalLocMatrixService.GetChildCategoryByModule(params).then(function (response) {
            $scope.Child = response.data;
            //$scope.ChildCatLocApp.Child = $scope.Child;
        });

        ChildCategoryService.GetSubCategoryByModule(params).then(function (response) {
            $scope.Sub = response;
        });

    }

}]);
