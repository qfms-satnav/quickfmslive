﻿app.service("ChildCatApprovalMatService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetHeldeskList = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ChildCatApprovalMat/GetHMDList')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.SaveData = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalMat/InsertDetails', Dataobj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    this.UpdateData = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalMat/UpdateDetails', Dataobj)
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };

    //For Grid
    this.GetGridData = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ChildCatApprovalMat/GetGridDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.CheckMappedDet = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalMat/CheckMappingDetails', Dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetEmpOnRole = function (Dataobj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalMat/GetRoleBasedEmployees', Dataobj)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    //For Edit
    this.editData = function (obj) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ChildCatApprovalMat/editData/', obj)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };


}]);

app.controller('ChildCatApprovalMatController', ['$scope', '$q', 'ChildCatApprovalMatService', 'HDMUtilityService', 'UtilityService',
    '$filter',
    function ($scope, $q, ChildCatApprovalMatService, HDMUtilityService, UtilityService, $filter) {
    $scope.ChildCatApp = {};
    $scope.ActionStatus = 0;
    $scope.ShowMessage = false;

    $scope.ClearData = function () {
        $scope.ChildCatApp = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmMapping.$submitted = false;

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Child, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Designation, function (value, key) {
            value.ticked = false;
        });

        $scope.Apprlst = [];
        $scope.Numbers.Nums = '';
    }


    HDMUtilityService.getMainCategories().then(function (response) {
        if (response.data != null) {
            $scope.Main = response.data;

            HDMUtilityService.getAllSubCategories().then(function (response) {
                if (response.data != null) {
                    $scope.Sub = response.data;

                    HDMUtilityService.getAllChildCategories().then(function (response) {
                        if (response.data != null) {
                            $scope.Child = response.data;
                        }
                    });
                }
            });
        }
    });

    ChildCatApprovalMatService.GetHeldeskList().then(function (response) {
        if (response != null) {
            $scope.HDMList = response;
        }
    });


    $scope.GetEmpOnRole = function (data) {
        console.log(data);
        var SelectedRole = { Role: data.ROL_ID };
        ChildCatApprovalMatService.GetEmpOnRole(SelectedRole).then(function (response) {
            if (response != null) {
                data.Emplst = response.data;
            }
        });
    }


    var columnDefs = [
    { headerName: "Child Category", field: "CHC_TYPE_NAME", width: 290, cellClass: 'grid-align' },
    { headerName: "Location", field: "LCM_NAME", width: 290, cellClass: 'grid-align' },
    { headerName: "Designation", field: "DSN_AMT_TITLE", width: 290, cellClass: 'grid-align' },
    { headerName: "No. of Approval Levels", field: "COUNT", width: 380, cellClass: 'grid-align' },
     { headerName: "Edit", width: 114, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, suppressSorting: true, onmouseover: "cursor: hand (a pointing hand)" }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        ChildCatApprovalMatService.GetGridData().then(function (response) {
            if (response.data != null) {
                $scope.gridata = response.data;
                $scope.gridOptions.api.setRowData(response.data);
                progress(0, '', false);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'No Data Found');
            }
        }, function (response) {
            progress(0, '', false);
        });
    }

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    $scope.ChildCatAppMat = function () {
        if ($scope.IsInEdit) {
            $scope.Dataobj = { ChildCatlst: $scope.ChildCatApp.Child, Apprlst: $scope.Apprlst, HDM_NUM: $scope.Numbers.Nums, lcmlst: $scope.ChildCatApp.Locations, dsglst: $scope.ChildCatApp.Designation };
            ChildCatApprovalMatService.UpdateData($scope.Dataobj).then(function (response) {
                var updatedobj = {};
                angular.copy($scope.ChildCatApp, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.Success = "Data Updated Successfully";
                $scope.LoadData();
                $scope.IsInEdit = false;
                $scope.ClearData();
                setTimeout(function () {
                    $scope.$apply(function () {
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            })
        }
        else {
            $scope.Dataobj = { ChildCatlst: $scope.ChildCatApp.Child, Apprlst: $scope.Apprlst, HDM_NUM: $scope.Numbers.Nums, lcmlst: $scope.ChildCatApp.Locations, dsglst: $scope.ChildCatApp.Designation };
            ChildCatApprovalMatService.SaveData($scope.Dataobj).then(function (response) {
                $scope.ShowMessage = true;
                ChildCatApprovalMatService.GetGridData().then(function (response) {
                    if (response.data != null) {
                        $scope.gridata = response.data;
                        $scope.gridOptions.api.setRowData(response.data);
                        progress(0, '', false);
                    }
                });
                //$scope.LoadData();
                showNotification('success', 8, 'bottom-right', "Data Inserted Successfully");
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                $scope.ClearData();
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }

    }

    setTimeout(function () {
        $scope.LoadData();
    }, 700);


    //// Main Category Events
    $scope.MainChangeAll = function () {
        $scope.ChildCatApp.Main = $scope.Main;
        $scope.MainChanged();
    }

    $scope.MainSelectNone = function () {
        $scope.ChildCatApp.Main = [];
        $scope.MainChanged();
    }

    $scope.MainChanged = function () {
        if ($scope.ChildCatApp.Main.length != 0) {
            HDMUtilityService.getSubCatbyMainCategories($scope.ChildCatApp.Main, 2).then(function (response) {
                if (response.data != null) {
                    $scope.Sub = response.data;
                }
                else
                    $scope.Sub = [];
            });
        }
        else
            $scope.Sub = [];
    }

    //// Sub Category Events
    $scope.SubChangeAll = function () {
        $scope.ChildCatApp.Sub = $scope.Sub;
        $scope.SubChanged();
    }
    $scope.SubSelectNone = function () {
        $scope.ChildCatApp.Sub = [];
        $scope.SubChanged();
    }

    $scope.SubChanged = function () {
        $scope.ChildCatApp.Main = [];

        HDMUtilityService.getChildCatBySubCategories($scope.ChildCatApp.Sub, 2).then(function (response) {
            if (response.data != null)
                $scope.Child = response.data;
            else
                $scope.Child = [];
        });

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Sub, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.SUBC_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.ChildCatApp.Main.push(mn);
            }
        });
    }

    ///// Child Category Events
    $scope.ChildChangeAll = function () {
        $scope.ChildCatApp.Child = $scope.Child;
        $scope.ChildChanged();
    }

    $scope.ChildSelectNone = function () {
        $scope.ChildCatApp.Child = [];
        $scope.ChildChanged();
    }

    $scope.ChildChanged = function () {
        $scope.ChildCatApp.Main = [];
        $scope.ChildCatApp.Sub = [];


        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Child, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.ChildCatApp.Main.push(mn);
            }
        });

        angular.forEach($scope.Child, function (value, key) {
            var sb = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sb != undefined && value.ticked == true && sb.ticked == false) {
                sb.ticked = true;
                $scope.ChildCatApp.Sub.push(sb);
            }
        });

        //$scope.Dataobj = { ChildCatlst: $scope.ChildCatApp.Child };
        //ChildCatApprovalMatService.CheckMappedDet($scope.Dataobj).then(function (response) {
        //    console.log(response);
        //    if (response != "Ok")
        //        showNotification('success', 8, 'bottom-right', response);
        //});

    }

    $scope.Numbers = [{ Value: '1', Key: 1 }, { Value: '2', Key: 2 }, { Value: '3', Key: 3 }, { Value: '4', Key: 4 }, { Value: '5', Key: 5 },
        { Value: '6', Key: 6 }, { Value: '7', Key: 7 }, { Value: '8', Key: 8 }, { Value: '9', Key: 9 }, { Value: '10', Key: 10 }];


    $scope.NumberChanged = function () {
        var num = $scope.Numbers.Nums;
        loadtable(num);
    }

    function loadtable(val) {
        $scope.Apprlst = [];
        var obj = {};
        for (var i = 1; i <= val; i++) {
            obj = {};
            obj.ROL_ID = "";
            obj.ROL_LEVEL = i;
            $scope.Apprlst.push(obj);
        }
    }


    $scope.Numbers.Nums = '2';
    loadtable($scope.Numbers.Nums);

    $scope.EditFunction = function (data) {
        console.log(data);
        var obj = { CHILD_CODE: data.CAM_CHC_CODE, LCM_CODE: data.LCM_CODE, DSG_CODE: data.DSG_CODE };
        console.log(obj);
        ChildCatApprovalMatService.editData(obj).then(function (response) {
            console.log(response);
            $scope.Numbers.Nums = response.data.HDM_NUM;
            $scope.Apprlst = response.data.Apprlst;
            angular.forEach($scope.Child, function (value, key) {
                value.ticked = false;
            });

            angular.forEach($scope.Locations, function (value, key) {
                value.ticked = false;
            });

            angular.forEach(response.data.lcmlst, function (value, key) {
                var sb = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });

                if (sb != undefined && sb.ticked == false) {
                    sb.ticked = true;

                }
            });

            angular.forEach($scope.Designation, function (value, key) {
                value.ticked = false;
            });

            angular.forEach(response.data.dsglst, function (value, key) {
                var sb = _.find($scope.Designation, { DSG_CODE: value.DSG_CODE });

                if (sb != undefined && sb.ticked == false) {
                    sb.ticked = true;

                }
            });

            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var sb = _.find($scope.Child, { CHC_TYPE_CODE: value.CHC_TYPE_CODE });
                if (sb != undefined && sb.ticked == false) {
                    sb.ticked = true;
                    $scope.ChildCatApp.Child.push(sb);
                }
            });
            angular.forEach($scope.Sub, function (value, key) {
                value.ticked = false;
            });
            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var cc = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
                if (cc != undefined && cc.ticked == false) {
                    cc.ticked = true;
                    $scope.ChildCatApp.Sub.push(cc);
                }
            });
            angular.forEach($scope.Main, function (value, key) {
                value.ticked = false;
            });

            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var mm = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
                if (mm != undefined && mm.ticked == false) {
                    mm.ticked = true;
                    $scope.ChildCatApp.Main.push(mm);
                }
            });


        });

        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;

    }

    $scope.column = {};




    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;

        }
    });

    UtilityService.getDesignation().then(function (response) {
        if (response.data != null) {
            $scope.Designation = response.data;
        }
    });

    $scope.locSelectAll = function () {

    }

    $scope.LocationChange = function () {


    }

    $scope.LocationSelectNone = function () {
        $scope.Locationlst = [];

    }

    $scope.DesignationSelectAll = function () {

    }

    $scope.DesignationChange = function () {


    }

    $scope.DesignationSelectNone = function () {
        $scope.Designation = [];

    }
}]);