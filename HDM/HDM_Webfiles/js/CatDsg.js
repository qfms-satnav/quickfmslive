﻿app.service("CatDsgService", function ($http, $q, UtilityService) {

    this.getDesignations = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CatDsg/getDesignations')
         .then(function (response) {
             deferred.resolve(response.data);
             return deferred.promise;
         }, function (response) {
             deferred.reject(response);
             return deferred.promise;
         });
    };
    //SAVE
    this.saveData = function (saveCat) {
        deferred = $q.defer();
        return $http.post('../../../api/CatDsg/Create', saveCat)
          .then(function (response) {
              deferred.resolve(response);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    //UPDATE 
    this.updateData = function (updateCat) {
        deferred = $q.defer();
        return $http.post('../../../api/CatDsg/Update/', updateCat)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    //For Grid
    this.GetMappingDetails = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/CatDsg/GetMappingDetails')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //For Edit
    this.editData = function (Id) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/CatDsg/editData/' + Id)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
});

app.controller('CatDsgController', function ($scope, $q, CatDsgService, HDMUtilityService, UtilityService, $filter) {
    $scope.CatDsgMaster = {};
    $scope.Main = [];
    $scope.Sub = [];
    $scope.Child = [];
    $scope.Designation = [];
    $scope.CatDsgMaster.Main = [];
    $scope.CatDsgMaster.Sub = [];
    $scope.CatDsgMaster.Child = [];
    $scope.CatDsgMaster.Designation = [];
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
  

    //to Save the data
    $scope.CatDsgMaster1 = function () {

        if ($scope.IsInEdit) {           
            var ReqObj = { ChildCatlst: $scope.CatDsgMaster.Child, Dsglst: $scope.CatDsgMaster.Designation };
            CatDsgService.updateData(ReqObj).then(function (repeat) {
                var updatedobj = {};
                angular.copy($scope.CatDsgMaster, updatedobj)              
                $scope.ShowMessage = true;
                $scope.Success = "Data Updated Successfully";
                $scope.LoadData();
                $scope.IsInEdit = false;    
                $scope.ClearData();
                //$scope.frmMapping.$setPristine();
                setTimeout(function () {
                    $scope.$apply(function () {
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        //$scope.ShowMessage = false;
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            })
        }
        else {
            var ReqObj = {ChildCatlst: $scope.CatDsgMaster.Child, Dsglst:  $scope.CatDsgMaster.Designation };         
            CatDsgService.saveData(ReqObj).then(function (response) {
                $scope.ShowMessage = true;
                $scope.LoadData();              
                setTimeout(function () {
                    $scope.$apply(function () {
                        showNotification('success', 8, 'bottom-right', "Data Inserted Successfully");
                    });
                }, 700);
                $scope.ClearData();
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    } 
    setTimeout(function () {
        $scope.LoadData();
    }, 700);

    $scope.EditFunction = function (data) {       
        CatDsgService.editData(data.HDM_DSN_CODE).then(function (response) {            
            angular.forEach($scope.Child, function (value, key) {
                value.ticked = false;
            });
            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var sb = _.find($scope.Child, { CHC_TYPE_CODE: value.CHC_TYPE_CODE });
                if (sb != undefined  && sb.ticked == false) {
                    sb.ticked = true;
                    $scope.CatDsgMaster.Child.push(sb);
                }
            });
            angular.forEach($scope.Sub, function (value, key) {
                value.ticked = false;
            });
            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var cc = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
                if (cc != undefined && cc.ticked == false) {
                    cc.ticked = true;
                    $scope.CatDsgMaster.Sub.push(cc);
                }
            });
            angular.forEach($scope.Main, function (value, key) {
                value.ticked = false;
            });
            angular.forEach(response.data.ChildCatlst, function (value, key) {
                var mm = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
                if (mm != undefined && mm.ticked == false) {
                    mm.ticked = true;
                    $scope.CatDsgMaster.Main.push(mm);
                }
            });
            angular.forEach($scope.Designation, function (value, key) {
                value.ticked = false;
            });
            angular.forEach(response.data.Dsglst, function (value, key) {
                var ds = _.find($scope.Designation, { DSN_CODE: value.DSN_CODE });
                if (ds != undefined && ds.ticked == false) {
                    ds.ticked = true;
                    $scope.CatDsgMaster.Designation.push(ds);
                }
            });

            $scope.ActionStatus = 1;
            $scope.IsInEdit = true;
            
        });

    }
    //for GridView
    var columnDefs = [
       { headerName: "Designation", field: "DSN_AMT_TITLE", width: 290, cellClass: 'grid-align' },
       { headerName: "Child Category", field: "CHC_TYPE_NAME", width: 380, cellClass: 'grid-align' },      
       { headerName: "Edit", width: 114, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, suppressSorting: true, onmouseover: "cursor: hand (a pointing hand)" }
    ];

    // To display grid row data
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        CatDsgService.GetMappingDetails().then(function (data) {                  
            $scope.gridOptions.api.setRowData(data.data);
            progress(0, '', false);
        }, function (error) {
            console.log(error);
        });
    }
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }
    };
    $scope.ClearData = function () {
        //$scope.Main = {};
        //$scope.Sub = {};
        //$scope.Child = {};
        //$scope.Designation = {};
        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Child, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Designation, function (value, key) {
            value.ticked = false;
        });
        $scope.CatDsgMaster = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frmMapping.$submitted = false;
    }
   
    HDMUtilityService.getMainCategories().then(function (response) {
        if (response.data != null) {
            $scope.Main = response.data;

            HDMUtilityService.getAllSubCategories().then(function (response) {
                if (response.data != null) {
                    $scope.Sub = response.data;

                    HDMUtilityService.getAllChildCategories().then(function (response) {
                        if (response.data != null) {
                            $scope.Child = response.data;

                            CatDsgService.getDesignations().then(function (response) {
                                if (response.data != null) {
                                    $scope.Designation = response.data;
                                }
                            });
                        }
                    });
                }
            });

        }
    });


    //// Main Category Events
    $scope.MainChangeAll = function () {
        $scope.CatDsgMaster.Main = $scope.Main;
        $scope.MainChanged();
    }

    $scope.MainSelectNone = function () {
        $scope.CatDsgMaster.Main = [];
        $scope.MainChanged();
    }

    $scope.MainChanged = function () {
        if ($scope.CatDsgMaster.Main.length != 0) {
            HDMUtilityService.getSubCatbyMainCategories($scope.CatDsgMaster.Main, 2).then(function (response) {
                if (response.data != null)
                    $scope.Sub = response.data;
                else
                    $scope.Sub = [];
            });
        }
        else
            $scope.Sub = [];
    }

    //// Sub Category Events
    $scope.SubChangeAll = function () {
        $scope.CatDsgMaster.Sub = $scope.Sub;
        $scope.SubChanged();
    }
    $scope.SubSelectNone = function () {
        $scope.CatDsgMaster.Sub = [];
        $scope.SubChanged();
    }

    $scope.SubChanged = function () {
        $scope.CatDsgMaster.Main = [];

        HDMUtilityService.getChildCatBySubCategories($scope.CatDsgMaster.Sub, 2).then(function (response) {
            if (response.data != null)
                $scope.Child = response.data;
            else
                $scope.Child = [];
        });

        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Sub, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.SUBC_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.CatDsgMaster.Main.push(mn);
            }
        });
    }

    ///// Child Category Events
    $scope.ChildChangeAll = function () {
        $scope.CatDsgMaster.Child = $scope.Child;
        $scope.ChildChanged();
    }

    $scope.ChildSelectNone = function () {
        $scope.CatDsgMaster.Child = [];
        $scope.ChildChanged();
    }

    $scope.ChildChanged = function () {
        $scope.CatDsgMaster.Main = [];
        $scope.CatDsgMaster.Sub = [];


        angular.forEach($scope.Main, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Sub, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Child, function (value, key) {
            var mn = _.find($scope.Main, { MNC_CODE: value.CHC_TYPE_MNC_CODE });
            if (mn != undefined && value.ticked == true && mn.ticked == false) {
                mn.ticked = true;
                $scope.CatDsgMaster.Main.push(mn);
            }
        });

        angular.forEach($scope.Child, function (value, key) {
            var sb = _.find($scope.Sub, { SUBC_CODE: value.CHC_TYPE_SUBC_CODE });
            if (sb != undefined && value.ticked == true && sb.ticked == false) {
                sb.ticked = true;
                $scope.CatDsgMaster.Sub.push(sb);
            }
        });

    }

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }









});