﻿<%@ Page Language="C#" AutoEventWireup="true"  %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
      <%--<link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->

   <%-- <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />--%>
   <%-- <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />--%>
   <%-- <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>--%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="AssetLocationController" class="amantra">

    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Asset Location" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                            <h3 class="panel-title">Asset Location </h3>
                        </div>
                        <div class="card">
                      <%--  <div class="card-body" style="padding-right: 10px;">--%>
                            <form role="form" id="form1" name="frmAssetLocation" data-valid-submit="Save()" novalidate>
                                <div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Location Code<span style="color: red;">*</span></label>
                                                <div data-ng-class="{'has-error': frmAssetLocation.$submitted && frmAssetLocation.ASSETLOC_Code.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                    <input id="ASSETLOC_Code" type="text" name="ASSETLOC_Code" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="RegExpCode.REGEXP" data-ng-model="AssetLocation.ASSETLOC_Code" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmAssetLocation.$submitted && frmAssetLocation.ASSETLOC_Code.$invalid" style="color: red"  data-ng-bind="RegExpCode.REGMSG"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Asset Location Name<span style="color: red;">*</span></label>
                                                <div data-ng-class="{'has-error': frmAssetLocation.$submitted && frmAssetLocation.ASSETLOC_Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                                    <input id="ASSETLOC_Name" type="text" name="ASSETLOC_Name" maxlength="50" data-ng-model="AssetLocation.ASSETLOC_Name" data-ng-pattern="RegExpName.REGEXP" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmAssetLocation.$submitted && frmAssetLocation.ASSETLOC_Name.$invalid" style="color: red"  data-ng-bind="RegExpName.REGMSG"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <div data-ng-class="{'has-error': frmAssetLocation.$submitted && frmAssetLocation.ASSETLOC_REM.$invalid}" onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                    <textarea id="ASSETLOC_REM" name="ASSETLOC_REM" runat="server" data-ng-model="AssetLocation.ASSETLOC_REM" class="form-control" maxlength="500" data-ng-pattern="RegExpRemarks.REGEXP"></textarea>
                                                    <span class="error" data-ng-show="frmAssetLocation.$submitted && frmAssetLocation.ASSETLOC_REM.$invalid" style="color: red"  data-ng-bind="RegExpRemarks.REGMSG"></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div data-ng-show="ActionStatus==1">
                                                    <label>Status<span style="color: red;">*</span></label>
                                                    <select id="ASSETLOC_Status_Id" name="ASSETLOC_Status_Id" data-ng-model="AssetLocation.ASSETLOC_Status_Id" class="form-control">
                                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                                <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                                <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                        <div data-ag-grid="gridOptions" style="height: 329px; width: 100%;" class="ag-blue"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            <%--</div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script defer>

        var app = angular.module('QuickFMS', ["agGrid"]);
        app.service("AssetLocationervice", function ($http, $q) {
            var deferred = $q.defer();
            this.getAssetLocation = function () {
                deferred = $q.defer();
                return $http.get('../../../api/AssetLocation')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            //SAVE
            this.saveAssetLocation = function (assetLoc) {
                deferred = $q.defer();
                return $http.post('../../../api/AssetLocation/Create', assetLoc)
                  .then(function (response) {
                      deferred.resolve(response);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //UPDATE BY ID
            this.updateAssetLocation = function (asset) {
                deferred = $q.defer();
                return $http.post('../../../api/AssetLocation/UpdateAssetLocationData', asset)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //Bind Grid
            this.GetAssetLocationBindGrid = function () {
                deferred = $q.defer();
                return $http.get('../../../api/AssetLocation/GetAssetLocationBindGrid')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });

        app.controller('AssetLocationController', function ($scope, $q, AssetLocationervice, $timeout, $http, UtilityService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.AssetLocation = {};
            $scope.repeatCategorylist = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;
            $scope.RegExpName = UtilityService.RegExpName;
            $scope.RegExpRemarks = UtilityService.RegExpRemarks;
            $scope.RegExpCode = UtilityService.RegExpCode;

            //to Save the data
            $scope.Save = function () {
                if ($scope.IsInEdit) {

                    AssetLocationervice.updateAssetLocation($scope.AssetLocation).then(function (repeat) {
                        var updatedobj = {};
                        angular.copy($scope.AssetLocation, updatedobj)
                        $scope.gridata.unshift(updatedobj);
                        $scope.ShowMessage = true;
                        $scope.Success = "Data successfully uploaded";
                        $scope.LoadData();
                        $scope.IsInEdit = false;
                        $scope.ClearData();

                        setTimeout(function () {
                            $scope.$apply(function () {
                                showNotification('success', 8, 'bottom-right', $scope.Success);
                            });
                        }, 700);
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.AssetLocation.ASSETLOC_Status_Id = "1";
                    AssetLocationervice.saveAssetLocation($scope.AssetLocation).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data successfully inserted";
                        var savedobj = {};
                        angular.copy($scope.AssetLocation, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.AssetLocation = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            //for GridView
            var columnDefs = [
               { headerName: "Asset Location Code", field: "ASSETLOC_Code", width: 120, cellClass: 'grid-align' },
               { headerName: "Asset Location Name", field: "ASSETLOC_Name", width: 160, cellClass: 'grid-align' },
               { headerName: "Status",suppressMenu: true, template: "{{ShowStatus(data.ASSETLOC_Status_Id)}}", width: 100, cellClass: 'grid-align' },
               { headerName: "Edit", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, onmouseover: "cursor: hand (a pointing hand)" }
            ];

            // To display grid row data
            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                AssetLocationervice.GetAssetLocationBindGrid().then(function (data) {
                    $scope.gridata = data;
                    //$scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                    progress(0, '', false);
                }, function (error) {
                    console.log(error);
                });
            }

            $scope.onPageSizeChanged = function () {
                $scope.createNewDatasource();
            };


            //$scope.pageSize = '10';

            $scope.createNewDatasource = function () {
                var dataSource = {
                    pageSize: parseInt($scope.pageSize),
                    getRows: function (params) {
                        setTimeout(function () {
                            var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
                            var lastRow = -1;
                            if ($scope.gridata.length <= params.endRow) {
                                lastRow = $scope.gridata.length;
                            }
                            params.successCallback(rowsThisPage, lastRow);
                        }, 500);
                    }
                };
                $scope.gridOptions.api.setDatasource(dataSource);
            }

            $scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableFilter: true,
                enableCellSelection: false,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };
            $timeout($scope.LoadData, 1000);

            $scope.EditFunction = function (data) {
                $scope.AssetLocation = {};
                angular.copy(data, $scope.AssetLocation);
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;

            }
            $scope.ClearData = function () {
                $scope.AssetLocation = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.frmAssetLocation.$submitted = false;
            }
            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
        });

    </script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
        <script src="../../../SMViews/Utility.min.js" defer></script>

</body>
</html>
