﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
    </style>--%>
</head>

<body data-ng-controller="SLAcontroller" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="SLA Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">SLA Time Definition</h3>
            </div>
            <div class="card">
                <%-- <div class="card-body" style="padding-right: 10px;">--%>
                <form role="form" id="form2" name="frmSLA" novalidate>
                    <div class="row">
                        <div class="col-md-12 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <div class="row">
                                    <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                            <div class="form-group">
                                <%--data-ng-class="{'has-error': frmService.$submitted && frmService.HDM_MAIN_MOD_NAME.$invalid}"--%>
                                <label class="control-label">Module <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Module" data-output-model="SLAMaster.Module" data-button-label="icon HDM_MAIN_MOD_NAME"
                                    data-on-item-click="getMainCategories()" data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="SLAMaster.Module[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                                <%--<span class="error" data-ng-show="frmService.$submitted && frmService.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select Module</span>--%>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Country" data-output-model="SLAMaster.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                    data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==1">
                                </div>
                                <input type="text" data-ng-model="SLAMaster.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.CNY_NAME.$invalid" style="color: red">Please Select Country </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.CTY_NAME.$invalid}">
                                <label class="control-label">City <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="SLAMaster.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==1">
                                </div>
                                <input type="text" data-ng-model="SLAMaster.City[0]" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.CTY_NAME.$invalid" style="color: red">Please Select City  </span>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Location" data-output-model="SLAMaster.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==1">
                                </div>
                                <input type="text" data-ng-model="SLAMaster.Location[0]" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.LCM_NAME.$invalid" style="color: red">Please Select Location</span>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.MNC_NAME.$invalid}">
                                <label class="control-label">Main Category <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Main" data-output-model="SLAMaster.Main" data-button-label="icon MNC_NAME" data-item-label="icon MNC_NAME"
                                    data-on-item-click="MainChanged()" data-on-select-all="MainChangeAll()" data-on-select-none="MainSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==1">
                                </div>
                                <input type="text" data-ng-model="SLAMaster.Main[0]" name="MNC_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.MNC_NAME.$invalid" style="color: red">Please Select Main Category </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SUBC_NAME.$invalid}">
                                <label class="control-label">Sub Category <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Sub" data-output-model="SLAMaster.Sub" data-button-label="icon SUBC_NAME" data-item-label="icon SUBC_NAME"
                                    data-on-item-click="SubChanged()" data-on-select-all="SubChangeAll()" data-on-select-none="SubSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==1">
                                </div>
                                <input type="text" data-ng-model="SLAMaster.Sub[0]" name="SUBC_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SUBC_NAME.$invalid" style="color: red">Please Select Subcategory </span>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.CHC_TYPE_NAME.$invalid}">
                                <label class="control-label">Child Category <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Child" data-output-model="SLAMaster.Child" data-button-label="icon CHC_TYPE_NAME" data-item-label="icon CHC_TYPE_NAME"
                                    data-on-item-click="ChildChanged()" data-on-select-all="ChildChangeAll()" data-on-select-none="ChildSelectNone()" data-tick-property="ticked" data-max-labels="1" data-is-disabled="EnableStatus==1">
                                </div>
                                <input type="text" data-ng-model="SLAMaster.Child[0]" name="CHC_TYPE_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.CHC_TYPE_NAME.$invalid" style="color: red">Please Select Child Category </span>
                            </div>
                        </div>
                    </div>
                    <%-- <div class="row">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Country<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_CNY_CODE.$invalid}">
                                                <select id="ddlCountry" name="SLA_CNY_CODE" class="form-control" data-ng-model="SLAMaster.SLA_CNY_CODE" required data-live-search="true" data-ng-change="CountryChanged()" data-ng-disabled="IsInEdit">
                                                    <option value="">--Select-- </option>
                                                    <option data-ng-repeat="countrycategory in countrylist" value="{{countrycategory.CNY_CODE}}">{{countrycategory.SLA_CNY_CODE}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_CNY_CODE.$invalid" style="color: red">Please Select Country </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>City<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_CTY_CODE.$invalid}">
                                                <select id="ddlCity" name="SLA_CTY_CODE" class="form-control" data-ng-model="SLAMaster.SLA_CTY_CODE" required data-live-search="true" data-ng-change="CityChanged()" data-ng-disabled="IsInEdit">
                                                    <option value="">--Select-- </option>
                                                    <option data-ng-repeat="citycategory in Citylist" value="{{citycategory.CTY_CODE}}">{{citycategory.SLA_CTY_CODE}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_CTY_CODE.$invalid" style="color: red">Please Select City</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Location<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_LOC_CODE.$invalid}">
                                                <select id="ddlLocation" name="SLA_LOC_CODE" class="form-control" data-ng-model="SLAMaster.SLA_LOC_CODE" required data-live-search="true" data-ng-disabled="IsInEdit">
                                                    <option value="">--Select-- </option>
                                                    <option data-ng-repeat="locationcategory in locationlist" value="{{locationcategory.LCM_CODE}}">{{locationcategory.SLA_LOC_CODE}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_LOC_CODE.$invalid" style="color: red">Please Select Location</span>
                                            </div>
                                        </div>
                                    </div>



                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Main Category<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_MNC_CODE.$invalid}">
                                                <select id="ddlMain" name="SLA_MNC_CODE" class="form-control" data-ng-model="SLAMaster.SLA_MNC_CODE" required data-live-search="true" data-ng-change="MainCategoryChanged()" data-ng-disabled="IsInEdit">
                                                    <option value="">--Select-- </option>
                                                    <option data-ng-repeat="maincategory in maincategorylist" value="{{maincategory.MNC_CODE}}">{{maincategory.SLA_MNC_CODE}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_MNC_CODE.$invalid" style="color: red">Please Select Main Category </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Sub Category<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_SUBC_CODE.$invalid}">
                                                <select id="ddlSub" name="SLA_SUBC_CODE" class="form-control" data-ng-model="SLAMaster.SLA_SUBC_CODE" required data-live-search="true" data-ng-change="SubCategoryChanged()" data-ng-disabled="IsInEdit">
                                                    <option value="">--Select-- </option>
                                                    <option data-ng-repeat="subcategory in SubCategorylist" value="{{subcategory.SUBC_CODE}}">{{subcategory.SLA_SUBC_CODE}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_SUBC_CODE.$invalid" style="color: red">Please Select Sub Category </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Child Category<span style="color: red;">*</span></label>
                                            <div data-ng-class="{'has-error': frmSLA.$submitted && frmSLA.SLA_CHC_CODE.$invalid}">
                                                <select id="ddlChild" name="SLA_CHC_CODE" class="form-control" data-ng-model="SLAMaster.SLA_CHC_CODE" required data-live-search="true" data-ng-change="GetSLA()" data-ng-disabled="IsInEdit">
                                                    <option value="">--Select-- </option>
                                                    <option data-ng-repeat="childcategory in ChildCategorylist" value="{{childcategory.CHC_TYPE_CODE}}">{{childcategory.SLA_CHC_CODE}}</option>
                                                </select>
                                                <span class="error" data-ng-show="frmSLA.$submitted && frmSLA.SLA_CHC_CODE.$invalid" style="color: red">Please Select Child Category </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>--%>
                    <br />
                    <br />
                    <br />

                    <div class="col-md-12 col-sm-12 col-xs-12" data-ng-show="SLADetails.length>0">
                        <div class="box">
                            <div class="box-body">
                                <table data-ng-table="tableParams" class="table GridStyle ng-table-responsive">
                                    <tr>
                                        <th>Status </th>
                                        <th>Email Escalation</th>
                                        <th data-ng-repeat="rol in Roles">{{rol.ROL_DESCRIPTION}}</th>
                                    </tr>
                                    <tr data-ng-repeat="SLA in SLADetails">
                                        <td style="vertical-align: central !important;">
                                            <label>{{SLA.Key.SLAD_DESC}}</label>
                                        </td>
                                        <td>
                                            <input type="checkbox" id="chk" data-ng-model="SLA.Key.selected" value="{{SLA.Key.selected}}" data-ng-init="SLA.Key.selected = true" />
                                        </td>
                                        <td data-ng-repeat="SLARoles in SLA.Value" data-ng-form="innerForm">
                                            <div class="input-group margin">
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-time"></span>
                                                </span>
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && innerForm.SLAD_ESC_TIME.$invalid}">
                                                    <input class="form-control input-sm" placeholder="" data-ng-model="SLARoles.Value.SLAD_ESC_TIME" data-ng-pattern="/^[0-9]{1,5}$/" name="SLAD_ESC_TIME" required="" type="text">
                                                </div>
                                                <div data-ng-class="{'has-error': frmSLA.$submitted && innerForm.SLAD_ESC_TIME_TYPE.$invalid}">
                                                    <select class="form-control input-sm" data-ng-model="SLARoles.Value.SLAD_ESC_TIME_TYPE" name="SLAD_ESC_TIME_TYPE" required="">
                                                        <option data-ng-repeat="type in timetype" data-ng-selected="type.value==SLARoles.Value.SLAD_ESC_TIME_TYPE" value="{{type.value}}">{{type.Name}}</option>
                                                    </select>
                                                </div>
                                                <span class="error" data-ng-show="innerForm.SLAD_ESC_TIME.$error.pattern">Enter Valid Number</span>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-right" style="padding-top: 5px">
                            <div class="form-group">
                                <input type="submit" value="Submit" data-ng-click="SaveDetails()" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" data-ng-click="SaveDetails()" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                <input id="btnClear" type="button" class="btn btn-primary custom-button-color" value="Clear" data-ng-click="clear()" />
                                <%--<input id="btnBack" type="button" class='btn btn-primary' value="Back"  data-ng-click="goBack()" >--%>
                                <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                            </div>
                        </div>
                        <div class="col-md-12 text-right" style="padding-top: 5px" ng-hide="AddSlahide">
                            <input type="button" value="ADD SLA" data-ng-click="AddSla()" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                        </div>
                    </div>

                    <div style="height: 320px">
                        <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" style="height: 80%;" class="ag-blue"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%--</div>
        </div>
    </div>--%>
</body>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
<script src="../../../Scripts/aggrid/ag-grid.min.js" defer></script>
<script src="../js/HDMUtility.js" defer></script>
<script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>

<script defer>
    var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    var CompanySession = '<%= Session("COMPANYID")%>';
</script>

<%--<script src="../js/SLA.js"></script>--%>
<%--<script src="../../../SMViews/Utility.js"></script>--%>
<%--<script src="../js/HDMUtility.js"></script>--%>
<%--	<script src="../../../HelpdeskManagement/JS/MainCategoryMaster.js"></script>		
<script src="../../../HelpdeskManagement/JS/SubCategoryMaster.js"></script>		
<script src="../../../HelpdeskManagement/JS/ChildCategoryMaster.js"></script>		
<script src="../js/ChildCatApprovalLocMatrix.js"></script>		
<script src="../js/ChildCatApprovalMat.js"></script>--%>

<script src="../js/SLA.min.js" defer></script>
<script src="../../../SMViews/Utility.min.js" defer></script>
<script src="../js/HDMUtility.min.js" defer></script>
<script src="../../../HelpdeskManagement/JS/MainCategoryMaster.min.js" defer></script>
<script src="../../../HelpdeskManagement/JS/SubCategoryMaster.min.js" defer></script>
<script src="../../../HelpdeskManagement/JS/ChildCategoryMaster.min.js" defer></script>
<script src="../js/ChildcatApprovalLocMatrix.min.js" defer></script>
<script src="../js/ChildCatApprovalMat.min.js" defer></script>

</html>
