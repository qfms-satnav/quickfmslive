﻿Imports System.Data
Imports System.Data.SqlClient
Imports SubSonic

Partial Class HDM_HDM_Webfiles_Masters_statusMaster
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindDropDownList()
        End If


    End Sub

    Private Sub BindDropDownList()
        Dim spSts As New SubSonic.StoredProcedure(Convert.ToString(Session("TENANT")) & "." & "[GET_HD_STATUS]")
        spSts.Command.AddParameter("@AUR_ID", Session("UID"), System.Data.DbType.[String])
        Dim ds1 As DataSet = spSts.GetDataSet()

        rptStatus.DataSource = ds1.Tables(0)
        rptStatus.DataBind()

        ddlModifyStatus.Items.Clear()

        For Each row As DataRow In ds1.Tables(0).Rows
            Dim statusID As Integer = Convert.ToInt32(row("STA_ID"))
            Dim statusDesc As String = row("STA_DESC").ToString()

            ddlModifyStatus.Items.Add(New ListItem(statusDesc, statusID.ToString()))
        Next
    End Sub

    Protected Sub SubmitButton_Click(ByVal sender As Object, ByVal e As EventArgs)

        Dim selectedStatusID As Integer = Integer.Parse(ddlModifyStatus.SelectedValue)
        'Dim staColor As String = Request.Form("STA_COLOR")
        Dim staColor As String = If(String.IsNullOrEmpty(Request.Form("STA_COLOR")), "#000000", Request.Form("STA_COLOR"))

        Try

            Dim tenant As String = Convert.ToString(Session("TENANT"))
            Dim storedProcedureName As String = tenant & ".UpdateSTAColor"

            Dim sp As New SubSonic.StoredProcedure(storedProcedureName)
            sp.Command.AddParameter("@STA_COLOR", staColor, DbType.String)
            sp.Command.AddParameter("@STA_ID", selectedStatusID, DbType.Int32)

            sp.Execute()

            ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT> alert('Data saved successfully');</SCRIPT>", False)
            BindDropDownList()

        Catch ex As Exception
            ' Handle exceptions
        End Try
    End Sub

    'Dim connectionString As String = "Server=20.219.50.13,1433;initial catalog=QuickFMS_UAT_MASTER_DB;max pool size=200;user id=srv.sql; Password=Qazxcds@1111#@#;"
    'Dim connectionString As String = ConfigurationManager.ConnectionStrings("CSAmantraFAM").ConnectionString
    '
    'Using connection As New SqlConnection(connectionString)
    '    connection.Open()
    '
    '    Dim commandText As String = "Product_UAT.dbo.UpdateSTAColor"
    '    Using command As New SqlCommand(commandText, connection)
    '        command.CommandType = CommandType.StoredProcedure
    '
    '        command.Parameters.AddWithValue("@STA_COLOR", staColor)
    '        command.Parameters.AddWithValue("@STA_ID", selectedStatusID)
    '
    '        command.ExecuteNonQuery()
    '        ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Data saved successfully');</SCRIPT>", False)
    '    End Using
    'End Using
    'BindDropDownList()

    'Catch ex As Exception
    '        ' Handle exceptions
    '    End Try
    'End Sub




End Class