﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSubCategory.aspx.vb" Inherits="HDM_HDM_Webfiles_Masters_frmSubCategory" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="SubCategoryController" class="amantra">

    <div class="al-content">
        <div class="widgets">
            <%-- <div ba-panel ba-panel-title="HD Sub Category" ba-panel-class="with-scroll horizontal-tabs tabs-panel medium-panel">
                <div class="panel">
                    <div class="panel-heading" style="height: 41px;">--%>
            <h3 class="panel-title">HD Sub Category </h3>
        </div>
        <div class="card">
            <%--<div class="card-body" style="padding-right: 50px;">--%>

            <form role="form" id="form1" name="frm" data-valid-submit="Save()" novalidate>

                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12" ng-hide="HelpDeskModule">
                        <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.HDM_MAIN_MOD_NAME.$invalid}">
                            <label class="control-label">Module <span style="color: red;">*</span></label>
                            <div isteven-multi-select data-input-model="Company" data-output-model="SubCategory.Company" data-button-label="icon HDM_MAIN_MOD_NAME" data-on-item-click="getMainCategories()"
                                data-item-label="icon HDM_MAIN_MOD_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                            </div>
                            <input type="text" data-ng-model="SubCategory.Company[0]" name="HDM_MAIN_MOD_NAME" style="display: none" required="" />
                            <span class="error" data-ng-show="frm.$submitted && frm.HDM_MAIN_MOD_NAME.$invalid" style="color: red;">Please Select Module</span>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.SUBC_CODE.$invalid}">
                            <label>Subcategory Code<span style="color: red;">*</span></label>
                            <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                <input id="txtcode" name="SUBC_CODE" type="text" data-ng-model="SubCategory.SUBC_CODE" data-ng-pattern="codepattern" maxlength="15" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="frm.$submitted && frm.SUBC_CODE.$invalid" style="color: red">Please Enter Valid Code </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.SUBC_NAME.$invalid}">
                            <label>Subcategory Name<span style="color: red;">*</span></label>
                            <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                onmouseout="UnTip()">
                                <input id="txtCName" name="SUBC_NAME" type="text" data-ng-model="SubCategory.SUBC_NAME" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                <span class="error" data-ng-show="frm.$submitted && frm.SUBC_NAME.$invalid" style="color: red">Please Enter Valid Name</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.MNC_NAME.$invalid}">
                            <label>Main Category<span style="color: red;">*</span></label>
                            <div data-ng-class="{'has-error': frm.$submitted && frm.MNC_NAME.$invalid}">
                                <select id="ddlMain" name="MNC_NAME" class="form-control" data-ng-model="SubCategory.MNC_CODE" required>
                                    <option id="ddlsub" value=''>--Select--</option>
                                    <option data-ng-repeat="maincategory in maincategorylist" value="{{maincategory.MNC_CODE}}">{{maincategory.MNC_NAME}}</option>
                                </select>
                                <span class="error" data-ng-show="frm.$submitted && frm.MNC_NAME.$invalid" style="color: red">Please Select Main Category </span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label>Remarks</label>
                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                <textarea id="txtremarks" data-ng-model="SubCategory.SUBC_REM" class="form-control" maxlength="500"></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">

                        <div class="form-group" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.SUBC_STA_ID.$invalid}">
                            <label>Status<span style="color: red;">*</span></label>
                            <select id="ddlsta" name="SUBC_STA_ID" data-ng-model="SubCategory.SUBC_STA_ID" class="form-control">
                                <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                            </select>
                            <span class="error" data-ng-show="frm.$submitted && frm.SUBC_STA_ID.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please Select Status </span>
                        </div>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <div class="form-group">
                            <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                            <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                            <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
                            <%--<input type="button" value="Back" class='btn btn-primary' onclick="window.location = 'frmMASMasters.aspx'" />--%>
                            <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                        </div>
                    </div>
                </div>
                <div class="row" style="padding-left: 30px;">
                    <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                    <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                </div>
            </form>
        </div>
    </div>
    <%-- </div>
        </div>
    </div>--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../../SMViews/Utility.js" defer></script>
    <script>

        //app.service("SubCategoryService", function ($http, $q) {
        //    var deferred = $q.defer();
        //    this.getSubCategory = function () {
        //        return $http.get('../../../api/SubCategory')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };

        //    this.CreateSubCategory = function (category) {
        //        deferred = $q.defer();
        //        return $http.post('../../../api/SubCategory/Create', category)
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };

        //    this.UpdateSubCategory = function (category) {
        //        deferred = $q.defer();
        //        return $http.post('../../../api/SubCategory/UpdateSubcatData', category)
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };

        //    this.GetMainCategory = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/SubCategory/GetMaincategory')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    }

        //    this.GetGriddata = function () {
        //        deferred = $q.defer();
        //        return $http.get('../../../api/SubCategory/GetGridData')
        //            .then(function (response) {
        //                deferred.resolve(response.data);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };

        //    this.GetMainCategoryByModule = function (dataObject) {
        //        deferred = $q.defer();
        //        return $http.post('../../../api/SubCategory/GetMainCategoryByModule', dataObject)
        //            .then(function (response) {
        //                deferred.resolve(response);
        //                return deferred.promise;
        //            }, function (response) {
        //                deferred.reject(response);
        //                return deferred.promise;
        //            });
        //    };

        //    //this.HelpDeskModuleHide = function () {
        //    //    deferred = $q.defer();
        //    //    return $http.get('../../../api/SubCategory/HelpDeskModuleHide')
        //    //        .then(function (response) {
        //    //            deferred.resolve(response);
        //    //            return deferred.promise;
        //    //        }, function (response) {
        //    //            deferred.reject(response);
        //    //            return deferred.promise;
        //    //        });
        //    //};

        //});



        //app.controller('SubCategoryController', function ($scope, $q, SubCategoryService, MainCategoryService) {
        //    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
        //    $scope.SubCategory = {};
        //    $scope.categorydata = [];
        //    $scope.maincategorylist = [];
        //    $scope.ActionStatus = 0;
        //    $scope.IsInEdit = false;
        //    $scope.ShowMessage = false;

        //    SubCategoryService.GetMainCategory().then(function (data) {
        //        $scope.maincategorylist = data;
        //        $scope.LoadData();
        //    }, function (error) {
        //        console.log(error);
        //    });

        //    $scope.Save = function () {
        //        if ($scope.IsInEdit) {
        //            $scope.SubCategory.Company = $scope.SubCategory.Company[0].HDM_MAIN_MOD_ID;
        //            SubCategoryService.UpdateSubCategory($scope.SubCategory).then(function (category) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = "Data Updated Successfully";
        //                var savedobj = {};
        //                $scope.gridOptions.api.setRowData($scope.gridata);
        //                $scope.EraseData();
        //                $scope.IsInEdit = false;
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                        showNotification('success', 8, 'bottom-right', $scope.Success);
        //                    });
        //                }, 700);
        //                $scope.LoadData();
        //                $scope.SubCategory = {};
        //                $scope.ActionStatus = 0;
        //            }, function (error) {
        //                console.log(error);
        //            })
        //        }
        //        else {
        //            $scope.SubCategory.SUBC_STA_ID = "1";
        //            $scope.SubCategory.Company = $scope.SubCategory.Company[0].HDM_MAIN_MOD_ID;
        //            SubCategoryService.CreateSubCategory($scope.SubCategory).then(function (response) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = "Data Inserted Successfully";
        //                var savedobj = {};
        //                $scope.SubCategory.MNC_NAME = $scope.GetMainName($scope.SubCategory.MNC_CODE);
        //                angular.copy($scope.SubCategory, savedobj)
        //                $scope.gridata.unshift(savedobj);
        //                $scope.gridOptions.api.setRowData($scope.gridata);
        //                $scope.EraseData();
        //                showNotification('success', 8, 'bottom-right', $scope.Success);
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                    });
        //                }, 700);
        //                $scope.SubCategory = {};
        //            }, function (error) {
        //                $scope.ShowMessage = true;
        //                $scope.Success = error.data;
        //                showNotification('error', 8, 'bottom-right', $scope.Success);
        //                setTimeout(function () {
        //                    $scope.$apply(function () {
        //                        $scope.ShowMessage = false;
        //                    });
        //                }, 1000);
        //                console.log(error);
        //            });
        //        }
        //    }
        //    var columnDefs = [
        //        { headerName: "Sub Category Code", field: "SUBC_CODE", width: 190, cellClass: 'grid-align' },
        //        { headerName: "Sub Category Name", field: "SUBC_NAME", width: 180, cellClass: 'grid-align' },
        //        { headerName: "Main Category", field: "MNC_NAME", width: 180, cellClass: 'grid-align' },
        //        { headerName: "Module", field: "Company", width: 180, cellClass: 'grid-align' },
        //        { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.SUBC_STA_ID)}}", width: 270, cellClass: 'grid-align' },
        //        { headerName: "Edit", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110, suppressMenu: true }];

        //    $scope.PageLoad = function () {
        //        MainCategoryService.HelpDeskModuleHide().then(function (response) {
        //            var result = response.data;
        //            if (result == "1") {
        //                MainCategoryService.GetCompanyModules().then(function (response) {

        //                    $scope.Company = response.data.data;
        //                    $scope.SubCategory.Company = $scope.Company;
        //                });
        //            } else {
        //                $scope.HelpDeskModule = true;
        //                $scope.SubCategory.Company = "1";
        //            }

        //        });
        //    }
        //    $scope.PageLoad();

        //    $scope.getMainCategories = function () {
        //        var params = {
        //            Company: $scope.SubCategory.Company[0].HDM_MAIN_MOD_ID
        //        }
        //        console.log(params);
        //        SubCategoryService.GetMainCategoryByModule(params).then(function (response) {
        //            $scope.maincategorylist = response.data;
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    }

        //    $scope.LoadData = function () {
        //        SubCategoryService.GetGriddata().then(function (data) {
        //            $scope.gridata = data;
        //            //$scope.createNewDatasource();
        //            $scope.gridOptions.api.setRowData(data);
        //        }, function (error) {
        //            console.log(error);
        //        });
        //    }

        //    //$scope.pageSize = '10';

        //    $scope.createNewDatasource = function () {
        //        var dataSource = {
        //            pageSize: parseInt($scope.pageSize),
        //            getRows: function (params) {
        //                setTimeout(function () {
        //                    var rowsThisPage = $scope.gridata.slice(params.startRow, params.endRow);
        //                    var lastRow = -1;
        //                    if ($scope.gridata.length <= params.endRow) {
        //                        lastRow = $scope.gridata.length;
        //                    }
        //                    params.successCallback(rowsThisPage, lastRow);
        //                }, 500);
        //            }
        //        };
        //        $scope.gridOptions.api.setDatasource(dataSource);
        //    }

        //    $scope.gridOptions = {
        //        columnDefs: columnDefs,
        //        enableCellSelection: false,
        //        rowData: null,
        //        enableSorting: true,
        //        enableFilter: true,
        //        angularCompileRows: true,
        //        onReady: function () {
        //            $scope.gridOptions.api.sizeColumnsToFit();
        //        }
        //    };
        //    //$scope.LoadData();

        //    $scope.EditData = function (data) {
        //        $scope.ActionStatus = 1;
        //        $scope.IsInEdit = true;
        //        angular.forEach($scope.Company, function (val) {
        //            if (val.HDM_MAIN_MOD_NAME == data.Company) {
        //                val.ticked = true;
        //            }
        //            else { val.ticked = false; }

        //        });
        //        SubCategoryService.GetMainCategory().then(function (data) {
        //            $scope.maincategorylist = data;
        //        }, function (error) {
        //            console.log(error);
        //        });
        //        angular.copy(data, $scope.SubCategory);
        //    }

        //    $scope.EraseData = function () {
        //        $scope.SubCategory = {};
        //        $scope.ActionStatus = 0;
        //        $scope.IsInEdit = false;
        //        $scope.frm.$submitted = false;
        //        angular.forEach($scope.Company, function (Company) {
        //            Company.ticked = false;
        //        });
        //    }

        //    $scope.ShowStatus = function (value) {
        //        return $scope.StaDet[value == 0 ? 1 : 0].Name;
        //    }

        //    //get main cat name to display in grid after submit
        //    $scope.GetMainName = function (mncCode) {
        //        //var fruitId = $scope.maincategorylist;
        //        for (var i = 0; $scope.maincategorylist != null && i < $scope.maincategorylist.length; i += 1) {
        //            var result = $scope.maincategorylist[i];
        //            if (result.MNC_CODE === mncCode) {
        //                return result.MNC_NAME;
        //            }
        //        }
        //    }

        //    $("#filtertxt").change(function () {
        //        onFilterChanged($(this).val());
        //    }).keydown(function () {
        //        onFilterChanged($(this).val());
        //    }).keyup(function () {
        //        onFilterChanged($(this).val());
        //    }).bind('paste', function () {
        //        onFilterChanged($(this).val());
        //    })

        //    function onFilterChanged(value) {
        //        $scope.gridOptions.api.setQuickFilter(value);
        //    }
        //});
    </script>
    <%--<script src="../../../HelpdeskManagement/JS/MainCategoryMaster.js"></script>
    <script src="../../../HelpdeskManagement/JS/SubCategoryMaster.js"></script>--%>

    <script src="../../../HelpdeskManagement/JS/MainCategoryMaster.min.js" defer></script>
    <script src="../../../HelpdeskManagement/JS/SubCategoryMaster.min.js" defer></script>
</body>
</html>
