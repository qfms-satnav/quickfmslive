﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmImpact.aspx.vb" Inherits="Masters_Mas_Webfiles_frmImpact" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="ImpactController" class="amantra">

    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Impact Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Impact Master </h3>
            </div>
            <div class="card">
                <%--<div class="card-body" style="padding-right: 10px;">--%>
                <form role="form" id="form1" name="frm" data-valid-submit="Save()" novalidate>
                    <div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Impact Code<span style="color: red;">*</span></label>
                                    <div data-ng-class="{'has-error': frm.$submitted && frm.Code.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">

                                        <input id="txtcode" type="text" name="Code" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="RegExpCode.REGEXP" data-ng-model="Impact.Code" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frm.$submitted && frm.Code.$invalid" style="color: red" data-ng-bind="RegExpCode.REGMSG"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Impact Name<span style="color: red;">*</span></label>
                                    <div data-ng-class="{'has-error': frm.$submitted && frm.Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">

                                        <input id="txtCName" type="text" name="Name" maxlength="50" data-ng-model="Impact.Name" data-ng-pattern="RegExpName.REGEXP" class="form-control" required="required" />&nbsp;
                                                    
                                                        <span class="error" data-ng-show="frm.$submitted && frm.Name.$invalid" style="color: red" data-ng-bind="RegExpName.REGMSG"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Remarks</label>
                                    <div data-ng-class="{'has-error': frm.$submitted && frm.Remarks.$invalid}" onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                        <input id="Remarks" name="Remarks" data-ng-model="Impact.Remarks" class="form-control" maxlength="500" data-ng-pattern="RegExpRemarks.REGEXP" />
                                        <span class="error" data-ng-show="frm.$submitted && frm.Remarks.$invalid" style="color: red" data-ng-bind="RegExpRemarks.REGMSG"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-show="ActionStatus==1">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <select id="ddlsta" name="Status" data-ng-model="Impact.Status" class="form-control ">
                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="submit" value="Submit" class="btn btn-default btn-primary" data-ng-show="ActionStatus==0" />
                                    <input type="submit" value="Modify" class="btn btn-default btn-primary" data-ng-show="ActionStatus==1" />
                                    <input type="reset" value="Clear" class="btn btn-default btn-primary" data-ng-click="EraseData()" />
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div style="height: 320px">
                        <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <%-- </div>--%>
    <%--</div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);

        app.directive('validSubmit', ['$parse', function ($parse) {
            return {
                require: 'form',
                link: function (scope, element, iAttrs, form) {
                    form.$submitted = false;
                    var fn = $parse(iAttrs.validSubmit);
                    element.on('submit', function (event) {
                        scope.$apply(function () {
                            form.$submitted = true;
                            if (form.$valid) {
                                fn(scope, { $event: event });
                                form.$submitted = false;
                            }
                        });
                    });
                }
            };
        }
        ])
        app.service("ImpactService", function ($http, $q) {
            var deferred = $q.defer();
            this.getImpact = function () {
                return $http.get('../../../api/Impact')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.CreateImpact = function (impact) {
                deferred = $q.defer();
                return $http.post('../../../api/Impact/Create', impact)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.UpdateImpact = function (impact) {
                deferred = $q.defer();
                return $http.post('../../../api/Impact/UpdateImpactData', impact)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.GetGriddata = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Impact/GetGridData')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

        });

        app.controller('ImpactController', function ($scope, $q, ImpactService, UtilityService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.Impact = {};
            $scope.impactdata = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;
            $scope.RegExpName = UtilityService.RegExpName;
            $scope.RegExpRemarks = UtilityService.RegExpRemarks;
            $scope.RegExpCode = UtilityService.RegExpCode;
            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    ImpactService.UpdateImpact($scope.Impact).then(function (impact) {
                        $scope.ActionStatus = 0;
                        $scope.ShowMessage = true;
                        $scope.Success = "Data successfully uploaded";
                        var savedobj = {};
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.IsInEdit = false;
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                                showNotification('success', 8, 'bottom-right', $scope.Success);
                            });
                        }, 700);
                        $scope.LoadData();
                        $scope.Impact = {};
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.Impact.Status = "1";
                    ImpactService.CreateImpact($scope.Impact).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data successfully inserted";
                        //var savedobj = {};
                        //angular.copy($scope.Impact, savedobj)
                        //$scope.gridata.unshift(savedobj);
                        //$scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.LoadData();
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.Impact = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            var columnDefs = [
                { headerName: "Impact Code", field: "Code", width: 190, cellClass: 'grid-align' },
                { headerName: "Impact Name", field: "Name", width: 280, cellClass: 'grid-align' },
                { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.Status)}}", width: 170, cellClass: 'grid-align' },
                { headerName: "Edit", width: 70, template: '<a ng-click = "EditData(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, onmouseover: "cursor: hand (a pointing hand)" }];
            //{ headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];

            $scope.LoadData = function () {
                ImpactService.GetGriddata().then(function (data) {
                    $scope.gridata = data;
                    // $scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });
            }
            $scope.gridOptions = {
                columnDefs: columnDefs,
                enableCellSelection: false,
                rowData: null,
                enableFilter: true,
                enableSorting: true,
                angularCompileRows: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }

            };
            $scope.LoadData();

            $scope.EditData = function (data) {
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
                angular.copy(data, $scope.Impact);
            }

            $scope.EraseData = function () {
                $scope.Impact = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.frm.$submitted = false;
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }
        });
    </script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <script src="../../../SMViews/Utility.min.js" defer></script>
</body>
</html>
