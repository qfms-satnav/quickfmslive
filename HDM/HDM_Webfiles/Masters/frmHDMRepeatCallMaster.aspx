﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="~/HDM/HDM_Webfiles/Masters/frmHDMRepeatCallMaster.vb" inherits="HDM_HDM_Webfiles_Masters_frmHDMRepeatCallMaster"%>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
   <%-- <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />--%>
   <%-- <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />--%>

    <%--<style>
        .grid-align
        {
            text-align: center;
        }

        a:hover
        {
            cursor: pointer;
        }

        .custom-button-color
        {
            width: 52px;
        }

        #Textarea1
        {
            width: 129px;
            height: 35px;
        }

        #RPT_Name
        {
            width: 106px;
        }

        #RPT_Code
        {
            width: 105px;
        }
    </style>--%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="RepeatController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Repeat Call Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                            <h3 class="panel-title">Repeat Call Master</h3>
                        </div>
                        <div class="card">
                       <%-- <div class="card-body" style="padding-right: 10px;">--%>
                            <form role="form" id="form1" name="frmRepeatCall" data-valid-submit="Save()" novalidate>
                                <div>

                                    <div class="row">
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Repeat Call Code<span style="color: red;">*</span></label>
                                                <div data-ng-class="{'has-error': frmRepeatCall.$submitted && frmRepeatCall.RPT_Code.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')" onmouseout="UnTip()">
                                                    <input id="RPT_Code" type="text" name="RPT_Code" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="RegExpCode.REGEXP" data-ng-model="RepeatCalls.RPT_Code" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmRepeatCall.$submitted && frmRepeatCall.RPT_Code.$invalid" style="color: red" data-ng-bind="RegExpCode.REGMSG"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Repeat Call Name<span style="color: red;">*</span></label>
                                                <div data-ng-class="{'has-error': frmRepeatCall.$submitted && frmRepeatCall.RPT_Name.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')" onmouseout="UnTip()">
                                                    <input id="RPT_Name" type="text" name="RPT_Name" maxlength="50" data-ng-model="RepeatCalls.RPT_Name" data-ng-pattern="RegExpName.REGEXP" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmRepeatCall.$submitted && frmRepeatCall.RPT_Name.$invalid" style="color: red" data-ng-bind="RegExpName.REGMSG"> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Remarks</label>
                                                <div data-ng-class="{'has-error': frmRepeatCall.$submitted && frmRepeatCall.RPT_REM.$invalid}"  onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                    <textarea id="RPT_REM" name="RPT_REM" runat="server" data-ng-model="RepeatCalls.RPT_REM" class="form-control" maxlength="500" data-ng-pattern="RegExpRemarks.REGEXP"></textarea>
                                                    <span class="error" data-ng-show="frmRepeatCall.$submitted && frmRepeatCall.RPT_REM.$invalid" style="color: red" data-ng-bind="RegExpRemarks.REGMSG"> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group" data-ng-show="ActionStatus==1">
                                                <label>Status<span style="color: red;">*</span></label>
                                                <select id="RPT_Status_Id" name="RPT_Status_Id" data-ng-model="RepeatCalls.RPT_Status_Id" class="form-control">
                                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' style="width: 60px" data-ng-show="ActionStatus==0" />
                                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                                <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                                <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <div style="height: 320px">
                            <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
       <%-- </div>
    </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
        app.service("RepeatCallService", function ($http, $q) {
            var deferred = $q.defer();
            this.getRepeatCalls = function () {
                deferred = $q.defer();
                return $http.get('../../../api/RepeatCalls')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            //SAVE
            this.saveRepeatCall = function (repeat) {
                deferred = $q.defer();
                return $http.post('../../../api/RepeatCalls/Create', repeat)
                  .then(function (response) {
                      deferred.resolve(response);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };

            //UPDATE BY ID
            this.updateRepeatCall = function (repeat) {
                deferred = $q.defer();
                return $http.post('../../../api/RepeatCalls/UpdateRepeatCallsData', repeat)
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
            //Bind Grid
            this.GetRepeatCallBindGrid = function () {
                deferred = $q.defer();
                return $http.get('../../../api/RepeatCalls/GetRepeatBindGrid')
                  .then(function (response) {
                      deferred.resolve(response.data);
                      return deferred.promise;
                  }, function (response) {
                      deferred.reject(response);
                      return deferred.promise;
                  });
            };
        });

        app.controller('RepeatController', function ($scope, $q, RepeatCallService, $http, $timeout, UtilityService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.RepeatCalls = {};
            $scope.repeatCategorylist = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;
            $scope.RegExpName = UtilityService.RegExpName;
            $scope.RegExpRemarks = UtilityService.RegExpRemarks;
            $scope.RegExpCode = UtilityService.RegExpCode;

            //to Save the data
            $scope.Save = function () {
                if ($scope.IsInEdit) {

                    RepeatCallService.updateRepeatCall($scope.RepeatCalls).then(function (repeat) {
                        var updatedobj = {};
                        angular.copy($scope.RepeatCalls, updatedobj)
                        $scope.gridata.unshift(updatedobj);
                        $scope.ShowMessage = true;
                        $scope.Success = "Data successfully uploaded";
                        $scope.LoadData();
                        $scope.IsInEdit = false;
                        $scope.ClearData();
                        setTimeout(function () {
                            $scope.$apply(function () {
                                showNotification('success', 8, 'bottom-right', $scope.Success);
                            });
                        }, 700);
                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.RepeatCalls.RPT_Status_Id = "1";
                    RepeatCallService.saveRepeatCall($scope.RepeatCalls).then(function (response) {
                        $scope.ShowMessage = true;
                        $scope.Success = "Data successfully inserted";
                        var savedobj = {};
                        angular.copy($scope.RepeatCalls, savedobj)
                        $scope.gridata.unshift(savedobj);
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 700);
                        $scope.RepeatCalls = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;

                        showNotification('error', 8, 'bottom-right', $scope.Success);
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                            });
                        }, 1000);
                        console.log(error);
                    });
                }
            }
            //for GridView
            var columnDefs = [
               { headerName: "Repeat Call Code", field: "RPT_Code", width: 120, cellClass: 'grid-align' },
               { headerName: "Repeat Call Name", field: "RPT_Name", width: 160, cellClass: 'grid-align' },
               { headerName: "Status",suppressMenu: true, template: "{{ShowStatus(data.RPT_Status_Id)}}", width: 100, cellClass: 'grid-align' },
               { headerName: "Edit", width: 70, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, onmouseover: "cursor: hand (a pointing hand)" }
            ];

            // To display grid row data
            $scope.LoadData = function () {
                progress(0, 'Loading...', true);
                RepeatCallService.GetRepeatCallBindGrid().then(function (data) {
                    $scope.gridata = data;
                    // $scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                    progress(0, '', false);
                }, function (error) {
                    console.log(error);
                });
            }

            $scope.gridOptions = {
                columnDefs: columnDefs,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableFilter: true,
                enableCellSelection: false,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }


            };
            $timeout($scope.LoadData, 1000);

            $scope.EditFunction = function (data) {

                $scope.RepeatCalls = {};
                angular.copy(data, $scope.RepeatCalls);
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
            }
            $scope.ClearData = function () {
                $scope.RepeatCalls = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.frmRepeatCall.$submitted = false;
            }
            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

        });

    </script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <script src="../../../SMViews/Utility.min.js" defer></script>
</body>
</html>
