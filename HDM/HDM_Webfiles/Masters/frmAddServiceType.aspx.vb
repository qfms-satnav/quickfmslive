﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Web.UI.WebControls
Imports System
Imports System.Collections
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI.HtmlControls
Imports SubSonic
Partial Class HDM_HDM_Webfiles_Masters_frmAddServiceType
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If Not IsPostBack Then
            getlocation()
            ddlSerCat.Items.Insert(0, "--Select--")
            btnSubmit.Text = "Add"
            fillgrid()
            trServType.Visible = False
        End If
    End Sub

    Protected Sub rbActions_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbActions.CheckedChanged, rbActionsModify.CheckedChanged
         If rbActions.Checked = True Then
                    trServType.Visible = False
                    cleardata()
                    btnSubmit.Text = "Add"
                    lblMsg.Visible = False
                    ddlLocation.Enabled = True
                    ddlSerCat.Enabled = True
                    txtSerCatCode.Enabled = True
                    fillgrid()
        Else
            getlocation()
            'getservicecategory()
            GetAllServiceTypes()
            trServType.Visible = True
            cleardata()
            btnSubmit.Text = "Modify"
            lblMsg.Visible = False
            ddlLocation.Enabled = False
            ddlSerCat.Enabled = False
            txtSerCatCode.Enabled = False
            fillgrid()
                End If
            
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        If Not IsValid Then
            Exit Sub
        End If

        If btnSubmit.Text = "Add" Then
            insertnewrecord()
        Else
            modifydata()
        End If
    End Sub

    Public Sub cleardata()
        ddlLocation.SelectedIndex = 0
        ddlSerCat.SelectedIndex = 0
        txtSerCatCode.Text = ""
        txtSerCatName.Text = ""
        ddlStatus.SelectedIndex = 0
        txtRemarks.Text = ""
        starttimehr.SelectedIndex = 0
        starttimemin.SelectedIndex = 0
        endtimehr.SelectedIndex = 0
        endtimemin.SelectedIndex = 0
    End Sub

    Public Sub getlocation()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"USP_Location_GetAll")
        'sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlLocation.DataSource = sp.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, "--Select--")
    End Sub

    Public Sub insertnewrecord()
        Try
            Dim StartHr As String = "00"
            Dim EndHr As String = "00"
            Dim StartMM As String = "00"
            Dim EndMM As String = "00"
            Dim ftime As String
            Dim ttime As String

            If starttimehr.SelectedItem.Value <> "HH" Then
                StartHr = starttimehr.SelectedItem.Text
            End If
            If starttimemin.SelectedItem.Value <> "MM" Then
                StartMM = starttimemin.SelectedItem.Text
            End If
            If endtimehr.SelectedItem.Value <> "HH" Then
                EndHr = endtimehr.SelectedItem.Text
            End If
            If endtimemin.SelectedItem.Value <> "MM" Then
                EndMM = endtimemin.SelectedItem.Text
            End If

            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
            If ftime >= ttime Then
                lblMsg.Visible = True
                lblMsg.Text = "To Time Should Be Greater Than From Time..."
                Exit Sub
            End If
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_INSERT_SERVICE_TYPE")
            sp1.Command.AddParameter("@SER_LCM_ID", ddlLocation.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_CATEGORY", ddlSerCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_TYPE_CODE", txtSerCatCode.Text, DbType.String)
            sp1.Command.AddParameter("@SER_TYPE_NAME", txtSerCatName.Text, DbType.String)
            sp1.Command.AddParameter("@SER_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@SER_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@SER_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@FROM_TIME", ftime, DbType.DateTime)
            sp1.Command.AddParameter("@TO_TIME", ttime, DbType.DateTime)
            'sp1.ExecuteScalar()
            Dim i As Integer = sp1.ExecuteScalar()
            If i = 1 Then
                fillgrid()
                lblMsg.Visible = True
                lblMsg.Text = "New Service Type Added Successfully..."
                cleardata()
            ElseIf i = 0 Then
                lblMsg.Visible = True
                lblMsg.Text = "Service Type Already Exists..."
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Public Sub modifydata()
        Try
            Dim StartHr As String = "00"
            Dim EndHr As String = "00"
            Dim StartMM As String = "00"
            Dim EndMM As String = "00"
            Dim ftime As String
            Dim ttime As String

            If starttimehr.SelectedItem.Value <> "HH" Then
                StartHr = starttimehr.SelectedItem.Text
            End If
            If starttimemin.SelectedItem.Value <> "MM" Then
                StartMM = starttimemin.SelectedItem.Text
            End If
            If endtimehr.SelectedItem.Value <> "HH" Then
                EndHr = endtimehr.SelectedItem.Text
            End If
            If endtimemin.SelectedItem.Value <> "MM" Then
                EndMM = endtimemin.SelectedItem.Text
            End If

            ftime = StartHr + ":" + StartMM
            ttime = EndHr + ":" + EndMM
            If ftime >= ttime Then
                lblMsg.Visible = True
                lblMsg.Text = "To Time Should Be Greater Than From Time..."
                Exit Sub
            End If
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_UPDATE_SERVICE_TYPE")
            sp1.Command.AddParameter("@SER_TYPE_ID", ddlServTypeName.SelectedItem.Value, DbType.Int64)
            'sp1.Command.AddParameter("@SER_TYPE_CODE", ddlServTypeName.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@SER_LCM_ID", ddlLocation.SelectedItem.Value, DbType.String)
            'sp1.Command.AddParameter("@SER_CATEGORY", ddlSerCat.SelectedItem.Value, DbType.String)
            sp1.Command.AddParameter("@SER_TYPE_NAME", txtSerCatName.Text, DbType.String)
            sp1.Command.AddParameter("@SER_STA_ID", ddlStatus.SelectedItem.Value, DbType.Int32)
            sp1.Command.AddParameter("@SER_UPT_BY", Session("Uid"), DbType.String)
            sp1.Command.AddParameter("@SER_REM", txtRemarks.Text, DbType.String)
            sp1.Command.AddParameter("@FROM_TIME", ftime, DbType.DateTime)
            sp1.Command.AddParameter("@TO_TIME", ttime, DbType.DateTime)
            sp1.ExecuteScalar()
            fillgrid()
            lblMsg.Visible = True
            lblMsg.Text = "Service Type Modified Successfully..."
            cleardata()
            ddlServTypeName.SelectedIndex = 0
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GET_ALLSERVICES_GRID")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvLocation.DataSource = ds
        gvLocation.DataBind()
        For i As Integer = 0 To gvLocation.Rows.Count - 1
            Dim lblstatus As Label = CType(gvLocation.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "1" Then
                lblstatus.Text = "Active"
            Else
                lblstatus.Text = "Inactive"
            End If
        Next
    End Sub

    Public Sub GetAllServiceTypes()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_GET_ALL_SERVICE_TYPES")
        sp.Command.AddParameter("@dummy", 1, DbType.Int32)
        ddlServTypeName.DataSource = sp.GetDataSet()
        ddlServTypeName.DataTextField = "SER_TYPE_NAME"
        ddlServTypeName.DataValueField = "SER_TYPE_ID"
        ddlServTypeName.DataBind()
        ddlServTypeName.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        lblMsg.Text = ""
        ddlSerCat.Items.Clear()
        getcategory(ddlLocation.SelectedItem.Value)
    End Sub

    Private Sub getcategory(lcm_code As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_GETACTIVESERREQUESTTYPE")
        sp.Command.AddParameter("@LCM_CODE", lcm_code, DbType.String)
        ddlSerCat.DataSource = sp.GetDataSet()
        ddlSerCat.DataTextField = "SER_NAME"
        ddlSerCat.DataValueField = "SER_CODE"
        ddlSerCat.DataBind()
        ddlSerCat.Items.Insert(0, "--Select--")
    End Sub

    Protected Sub ddlServTypeName_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlServTypeName.SelectedIndexChanged
        Try
            getlocation()
            If ddlServTypeName.SelectedIndex <> 0 Then
                lblMsg.Visible = False
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." &"HDM_USP_GET_SERVICETYPE_DATA_FORMODIFY")
                sp.Command.AddParameter("@SER_TYPE_ID", ddlServTypeName.SelectedItem.Value, DbType.Int64)
                Dim ds As New DataSet
                ds = sp.GetDataSet
                If ds.Tables(0).Rows.Count > 0 Then '
                    ddlLocation.ClearSelection()
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("LCM_CODE")).Selected = True
                    getcategory(ddlLocation.SelectedItem.Value)
                    ddlSerCat.ClearSelection()
                    ddlSerCat.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_CODE")).Selected = True
                    txtSerCatCode.Text = ds.Tables(0).Rows(0).Item("SER_TYPE_CODE")
                    txtSerCatName.Text = ds.Tables(0).Rows(0).Item("SER_TYPE_NAME")
                    txtRemarks.Text = ds.Tables(0).Rows(0).Item("SER_TYPE_REM")
                    ddlStatus.ClearSelection()
                    ddlStatus.Items.FindByValue(ds.Tables(0).Rows(0).Item("SER_TYPE_STAID")).Selected = True
                    starttimehr.ClearSelection()
                    starttimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_HOURS")).Selected = True
                    starttimemin.ClearSelection()
                    starttimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("FROM_MINS")).Selected = True
                    endtimehr.ClearSelection()
                    endtimehr.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_HOURS")).Selected = True
                    endtimemin.ClearSelection()
                    endtimemin.Items.FindByValue(ds.Tables(0).Rows(0).Item("TO_MINS")).Selected = True
                End If
            Else
                cleardata()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvLocation_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvLocation.PageIndexChanging
        gvLocation.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub



End Class
