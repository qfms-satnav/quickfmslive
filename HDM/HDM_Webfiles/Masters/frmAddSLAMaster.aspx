﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmAddSLAMaster.aspx.vb" Inherits="HDM_HDM_Webfiles_Masters_frmAddSLAMaster" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>SLA Master</legend>
                    </fieldset>

                    <form id="form1" class="form-horizontal well" runat="server">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                        </asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="Label1" runat="server" class="col-md-5 control-label">Select Location<span style="color: red;">*</span></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" Display="None" ControlToValidate="ddlLocation"
                                            ErrorMessage="Please Select Location" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>

                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Location"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="lblAssetBrand" class="col-md-5 control-label" runat="server">Select Service Category <span style="color: red;">*</span></asp:Label>
                                        <asp:Label ID="lblTemp" class="col-md-5 control-label" runat="server" Visible="false"></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlServiceCat"
                                            ErrorMessage="Please Select Service Category" ValueToCompare="--Select--" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="rfcservcat" runat="server" ValidationGroup="Val1" ControlToValidate="ddlServiceCat" Display="none"
                                            ErrorMessage="Please Select Service Category" InitialValue=""></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlServiceCat" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Service Category"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <asp:Label ID="Label2" class="col-md-5 control-label" runat="server">Select Service Type <span style="color: red;">*</span></asp:Label>
                                        <asp:Label ID="Label3" class="col-md-5 control-label" runat="server" Visible="false"></asp:Label>
                                        <asp:CompareValidator ID="CompareValidator3" runat="server" Display="None" ControlToValidate="ddlSerType"
                                            ErrorMessage="Please Select Service Type" ValueToCompare="" Operator="NotEqual" ValidationGroup="Val1"></asp:CompareValidator>
                                        <asp:RequiredFieldValidator ID="rfvServType" runat="server" ValidationGroup="Val1" ControlToValidate="ddlSerType" Display="none"
                                            ErrorMessage="Please Select Service Type" InitialValue=""></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlSerType" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Service Type"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">L1 (Mins.) <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvl1" runat="server" ValidationGroup="Val1" ControlToValidate="txtL1" Display="none" InitialValue="" ErrorMessage="Please Enter L1"></asp:RequiredFieldValidator>

                                        <asp:RegularExpressionValidator ID="revl1" runat="server" ValidationGroup="Val1" ControlToValidate="txtL1" Display="none" ErrorMessage="Please Enter L1 in Numerics" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>


                                        <div class="col-md-7">

                                            <asp:TextBox ID="txtL1" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">L2 (Mins.)<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ValidationGroup="Val1" ControlToValidate="txtL2" Display="none" ErrorMessage="Please Enter L2"></asp:RequiredFieldValidator>

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="Val1" ControlToValidate="txtL2" Display="none" ErrorMessage="Please Enter L2 in Numerics" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>


                                        <div class="col-md-7">

                                            <asp:TextBox ID="txtL2" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">L3 (Mins.)<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Val1" ControlToValidate="txtL3" Display="none" ErrorMessage="Please Enter L3"></asp:RequiredFieldValidator>

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationGroup="Val1" ControlToValidate="txtL3" Display="none" ErrorMessage="Please Enter L3 in Numerics" ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>


                                        <div class="col-md-7">


                                            <asp:TextBox ID="txtL3" runat="server" MaxLength="5" CssClass="form-control"></asp:TextBox>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Select Status<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                            Display="none" ErrorMessage="Please Select Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="selectpicker" data-live-search="true"
                                                ToolTip="Select Status">
                                                <asp:ListItem>--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">Active</asp:ListItem>
                                                <asp:ListItem Value="0">Inactive</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <label class="col-md-5 control-label">Remarks<span style="color: red;">*</span></label>
                                        <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                            ControlToValidate="txtRemarks" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters  " ValidationGroup="Val1"></asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Please Enter Remarks  " ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-7">
                                            <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine"
                                                    MaxLength="500" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Add" ValidationGroup="Val1" />
                                    <asp:Button ID="btnBack" CssClass="btn btn-primary custom-button-color" runat="server" Text="Back" PostBackUrl="~/Workspace/SMS_WEBfiles/helpmasters.aspx" CausesValidation="False" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:GridView ID="gvSer" runat="server" AllowPaging="True" AllowSorting="False"
                                    RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left"
                                    PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Service Level Found." CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>

                                                <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("SER_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLcmName" runat="server" CssClass="lblLcmName" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Service Category Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCatCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("SER_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Service Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSerType" runat="server" CssClass="lblStatus" Text='<%#Bind("SER_TYPE_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="L1 (Mins.)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblL1" runat="server" CssClass="lblStatus" Text='<%#Bind("L1_HR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="L2 (Mins.)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblL2" runat="server" CssClass="lblStatus" Text='<%#Bind("L2_HR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="L3 (Mins.)">
                                            <ItemTemplate>
                                                <asp:Label ID="lblL3" runat="server" CssClass="lblStatus" Text='<%#Bind("L3_HR")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status">
                                            <ItemTemplate>
                                                <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#Bind("STA_ID")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:ButtonField Text="EDIT" CommandName="EDIT" />
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

