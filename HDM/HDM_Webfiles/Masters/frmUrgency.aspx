﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmUrgency.aspx.vb" Inherits="Masters_Mas_Webfiles_frmUrgency" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>






    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%-- <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />--%>
    <%--   <link href="../../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
    <%-- <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>--%>
</head>
<body data-ng-controller="UrgencyController" class="amantra">

    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="HD Main Category" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Urgency Master </h3>
            </div>
            <div class="card">
                <%--<div class="card-body" style="padding-right: 10px;">--%>
                <form role="form" id="form1" name="frm" data-valid-submit="Save()" novalidate>
                    <div class="row">
                        <%-- <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <label id="lblSuccess" for="lblMsg" data-ng-show="ShowMessage" class="col-md-12 control-label" style="color: red">{{Success}}</label>
                                            </div>
                                        </div>
                                    </div>--%>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Code.$invalid}">
                                <label>Urgency Code<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter code in alphabets and numbers, upto 15 characters allowed')"
                                    onmouseout="UnTip()">
                                    <input type="text" id="txtcode" name="Code" data-ng-pattern="RegExpCode.REGEXP" data-ng-model="Urgency.Code" maxlength="15" class="form-control" required="required" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.Code.$invalid" style="color: red" data-ng-bind="RegExpCode.REGMSG"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Name.$invalid}">
                                <label>Urgency Name<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                    onmouseout="UnTip()">
                                    <input id="txtCName" type="text" name="Name" data-ng-model="Urgency.Name" maxlength="50" class="form-control" required="required" data-ng-pattern="RegExpName.REGEXP" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.Name.$invalid" style="color: red" data-ng-bind="RegExpName.REGMSG"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.Remarks.$invalid}" onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                    <textarea id="txtremarks" name="Remarks" data-ng-model="Urgency.Remarks" class="form-control" maxlength="500" data-ng-pattern="RegExpRemarks.REGEXP"></textarea>
                                    <span class="error" data-ng-show="frm.$submitted && frm.Remarks.$invalid" style="color: red" data-ng-bind="RegExpRemarks.REGMSG"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.Status.$invalid}">
                                <label>Status<span style="color: red;">*</span></label>
                                <select id="ddlsta" name="Status" data-ng-model="Urgency.Status" class="form-control">
                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                </select>
                                <span class="error" data-ng-show="frm.$submitted && frm.Status.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please select status </span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()">
                                <%--<input type="button" value="Back" class='btn btn-primary' onclick="window.location = 'frmMASMasters.aspx'" />--%>
                                <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                            </div>
                        </div>
                    </div>
                    <div style="height: 320px">
                        <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                        <div data-ag-grid="gridOptions" style="height: 90%;" class="ag-blue"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%-- </div>
        </div>
    </div>--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../SMViews/Utility.js" defer></script>

    <%--  <script src="../../../Scripts/aggrid/ag-grid.min.js"></script>--%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
        app.directive('validSubmit', ['$parse', function ($parse) {
            return {
                require: 'form',
                link: function (scope, element, iAttrs, form) {
                    form.$submitted = false;
                    var fn = $parse(iAttrs.validSubmit);
                    element.on('submit', function (event) {
                        scope.$apply(function () {
                            form.$submitted = true;
                            if (form.$valid) {
                                fn(scope, { $event: event });
                                form.$submitted = false;
                            }
                        });
                    });
                }
            };
        }
        ])
        app.service("UrgencyService", function ($http, $q) {
            var deferred = $q.defer();
            this.getUrgency = function () {
                return $http.get('../../../api/Urgency')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.CreateUrgency = function (urgency) {
                deferred = $q.defer();
                return $http.post('../../../api/Urgency/Create', urgency)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.UpdateUrgency = function (urgency) {
                deferred = $q.defer();
                return $http.post('../../../api/Urgency/UpdateUrgencyMaster', urgency)
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };

            this.GetGriddata = function () {
                deferred = $q.defer();
                return $http.get('../../../api/Urgency/GetGridData')
                    .then(function (response) {
                        deferred.resolve(response.data);
                        return deferred.promise;
                    }, function (response) {
                        deferred.reject(response);
                        return deferred.promise;
                    });
            };
        });

        app.controller('UrgencyController', function ($scope, $q, UrgencyService, UtilityService) {
            $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
            $scope.Urgency = {};
            $scope.urgencydata = [];
            $scope.ActionStatus = 0;
            $scope.IsInEdit = false;
            $scope.ShowMessage = false;
            $scope.RegExpName = UtilityService.RegExpName;
            $scope.RegExpRemarks = UtilityService.RegExpRemarks;
            $scope.RegExpCode = UtilityService.RegExpCode;

            $scope.LoadData = function () {
                UrgencyService.GetGriddata().then(function (data) {
                    $scope.gridata = data;
                    // $scope.createNewDatasource();
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });
            }
            $scope.Save = function () {
                if ($scope.IsInEdit) {
                    UrgencyService.UpdateUrgency($scope.Urgency).then(function (urgency) {
                        showNotification('success', 8, 'bottom-right', "Data successfully uploaded");
                        $scope.LoadData();
                        $scope.ShowMessage = true;
                        $scope.EraseData();
                        angular.copy($scope.Urgency, savedobj)
                        $scope.IsInEdit = false;
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                                showNotification('success', 8, 'bottom-right', "");
                            });
                        }, 700);

                    }, function (error) {
                        console.log(error);
                    })
                }
                else {
                    $scope.Urgency.Status = "1";
                    UrgencyService.CreateUrgency($scope.Urgency).then(function (response) {
                        showNotification('success', 8, 'bottom-right', "Data successfully inserted ");
                        $scope.ShowMessage = true;
                        $scope.EraseData();
                        //angular.copy($scope.Urgency, savedobj)
                        //$scope.gridata.unshift(savedobj);
                        //$scope.gridOptions.api.setRowData($scope.gridata);
                        $scope.LoadData();
                        $scope.Urgency = {};
                    }, function (error) {
                        $scope.ShowMessage = true;
                        $scope.Success = error.data;
                        setTimeout(function () {
                            $scope.$apply(function () {
                                $scope.ShowMessage = false;
                                showNotification('error', 8, 'bottom-right', error.data);
                            });
                        }, 100);
                    });
                }
            }

            var columnDefs = [
                { headerName: "Urgency Code", field: "Code", width: 290, cellClass: 'grid-align' },
                { headerName: "Urgency Name", field: "Name", width: 380, cellClass: 'grid-align' },
                { headerName: "Status", suppressMenu: true, template: "{{ShowStatus(data.Status)}}", width: 270, cellClass: 'grid-align' },
                { headerName: "Edit", width: 70, template: '<a ng-click = "EditData(data)"> <i class="fa fa-pencil class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, onmouseover: "cursor: hand (a pointing hand)" }];


            $scope.gridOptions = {
                columnDefs: columnDefs,
                enableCellSelection: false,
                rowData: null,
                enableSorting: true,
                cellClass: 'grid-align',
                angularCompileRows: true,
                enableFilter: true,
                onReady: function () {
                    $scope.gridOptions.api.sizeColumnsToFit()
                }
            };

            $scope.LoadData();

            $scope.EditData = function (data) {
                $scope.ActionStatus = 1;
                $scope.IsInEdit = true;
                angular.copy(data, $scope.Urgency);
            }

            $scope.EraseData = function () {
                $scope.Urgency = {};
                $scope.ActionStatus = 0;
                $scope.IsInEdit = false;
                $scope.frm.$submitted = false;
            }

            $scope.ShowStatus = function (value) {
                return $scope.StaDet[value == 0 ? 1 : 0].Name;
            }

            $("#filtertxt").change(function () {
                onFilterChanged($(this).val());
            }).keydown(function () {
                onFilterChanged($(this).val());
            }).keyup(function () {
                onFilterChanged($(this).val());
            }).bind('paste', function () {
                onFilterChanged($(this).val());
            })

            function onFilterChanged(value) {
                $scope.gridOptions.api.setQuickFilter(value);
            }

        });

    </script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <script src="../../../SMViews/Utility.min.js" defer></script>
</body>
</html>

