﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="frmAddLocationMaster.aspx.vb" Inherits="HDM_HDM_Webfiles_Masters_frmAddLocationMaster"%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <script src="../../Scripts/wz_tooltip.js" type="text/javascript" language="javascript" defer></script>

        <div>
            <table id="table1" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                <tr>
                    <td align="center" width="100%">
                        <asp:Label ID="lblHead" runat="server" CssClass="clsHead" Width="95%" Font-Underline="False"
                            ForeColor="Black">Service Location Master
             <hr align="center" width="60%" /></asp:Label></td>
                </tr>
            </table>
            <asp:Panel ID="PNLCONTAINER" runat="server" Width="95%" Height="100%">
                <table id="table4" cellspacing="0" cellpadding="0" width="95%" align="center" border="0">
                    <tr>
                        <td align="left" width="100%" colspan="3">
                            <asp:Label ID="LBLNOTE" runat="server" CssClass="note" ToolTip="Please provide information for (*) mandatory fields. ">(*) Mandatory Fields. </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img alt="" height="27" src="../../Images/table_left_top_corner.gif" width="9" /></td>
                        <td width="100%" class="tableHEADER" align="left">
                            <strong>&nbsp;Service Location Master</strong>
                        </td>
                        <td>
                            <img alt="" height="27" src="../../Images/table_right_top_corner.gif" width="16" /></td>
                    </tr>
                    <tr>
                        <td background="../../Images/table_left_mid_bg.gif">&nbsp;</td>
                        <td align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="clsMessage"
                                ForeColor="" ValidationGroup="Val1" />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label><br />
                            <table id="table2" cellspacing="1" cellpadding="1" width="100%" border="1">
                                <tr>
                                    <td align="center" width="50%" colspan="2">
                                        <asp:RadioButtonList ID="rbActions" runat="server" Width="176px" Height="13px" AutoPostBack="True"
                                            Font-Bold="True" RepeatDirection="Horizontal" OnSelectedIndexChanged="rbActions_SelectedIndexChanged">
                                            <asp:ListItem Value="Add" Selected="true">Add</asp:ListItem>
                                            <asp:ListItem Value="Modify">Modify</asp:ListItem>
                                        </asp:RadioButtonList></td>
                                </tr>
                                <tr  id="trLName" runat="server">
                                    <td width="50%">
                                        <p>
                                            <asp:Label ID="lblLocationSelect" runat="server" Cssclass="bodytext" Visible="False">Select Location</asp:Label>
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" Display="None" ControlToValidate="ddlLocation"
                                                ErrorMessage="Please Select the Location" ValueToCompare="--Select--" Operator="NotEqual"></asp:CompareValidator>
                                        </p>
                                    </td>
                                    <td width="50%">
                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="clsComboBox" Width="100%"
                                            AutoPostBack="True" Visible="False">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left" style="height: 16px; width: 50%;">Location Code <font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtLcmCode"
                                            Display="none" ErrorMessage="Please Enter Location  Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                      <%--  <asp:RegularExpressionValidator ID="revPropertyType" runat="server" ControlToValidate="txtLcmCode"
                                            ErrorMessage="Please enter Location Code" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                                            ValidationGroup="Val1">
                                        </asp:RegularExpressionValidator>--%>
                                    </td>
                                    <td align="left" style="height: 16px; width: 50%;">
                                        <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtLcmCode" runat="server" CssClass="clsTextField" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="height: 16px; width: 50%;">Location Name <font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLcmName"
                                            Display="none" ErrorMessage="Please Enter Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                   <%--     <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtLcmName"
                                            ErrorMessage="Please enter Location Name" Display="None" ValidationExpression="^[0-9a-zA-Z ]+"
                                            ValidationGroup="Val1">
                                        </asp:RegularExpressionValidator>--%>
                                    </td>
                                    <td align="left" style="height: 16px; width: 50%;">
                                        <div onmouseover="Tip('Enter code in alphabets,numbers')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtLcmName" runat="server" CssClass="clsTextField" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align="left" style="height: 16px; width: 50%;">Select Status<font class="clsNote">*</font>
                                        <asp:RequiredFieldValidator ID="rfvstatus" runat="server" ControlToValidate="ddlStatus"
                                            Display="none" ErrorMessage="Please Status" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left" style="height: 16px; width: 50%;">
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="clsComboBox" Width="99%">
                                            <asp:ListItem>--Select--</asp:ListItem>
                                            <asp:ListItem Value="1">Active</asp:ListItem>
                                            <asp:ListItem Value="0">Inactive</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left"  style="width: 50%; height: 16px">
                                        Remarks<font class="clsNote">*</font>
                                        <asp:CustomValidator ID="cvRemarks" runat="server" ClientValidationFunction="maxLength"
                                            ControlToValidate="txtRemarks" Display="None" ErrorMessage="Remarks Shoud be less than 500 characters  " ValidationGroup="Val1"></asp:CustomValidator>
                                        <asp:RequiredFieldValidator ID="rfvrem" runat="server" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Please Enter Remarks  " ValidationGroup="Val1"></asp:RequiredFieldValidator></td>
                                    <td align="left" style="width: 43%; height: 16px">
                                        <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="clsTextField" TextMode="MultiLine"
                                                Width="97%" MaxLength="500" Height="35px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center" style="height: 39px">
                                        <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back" PostBackUrl="~/Workspace/SMS_WEBfiles/helpmasters.aspx" CausesValidation="False" />
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="btnSubmit" CssClass="button" runat="server" Text="Add" ValidationGroup="Val1" />

                                        <%--   <asp:Button ID="btnCancel" CssClass="button" runat="server" Text="Back" />--%>&nbsp;
                                    </td>
                                </tr>
                            </table>

                            <table id="Table5" width="100%" runat="Server" cellpadding="0" cellspacing="0" border="1">
                                <tr>
                                    <td align="center" style="height: 20px">
                                        <asp:GridView ID="gvLocation" runat="server" AllowPaging="True" AllowSorting="False"
                                            RowStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" Width="100%"
                                            PageSize="10" AutoGenerateColumns="false" EmptyDataText="No Records Found">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField Visible="false">
                                                    <ItemTemplate>
                                                            
                                                        <asp:Label ID="lblID" runat="server" CssClass="lblAST ID" Text='<%#Eval("LCM_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" runat="server" CssClass="lblASTCode" Text='<%#Eval("LCM_CODE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" CssClass="lblStatus" Text='<%#Bind("LCM_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblstatus" runat="server" CssClass="lblStatus" Text='<%#BIND("LCM_STA_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td background="../../Images/table_right_mid_bg.gif" style="width: 10px; height: 100%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 10px; height: 17px;">
                            <img alt="" height="17" src="../../Images/table_left_bot_corner.gif" width="9" /></td>
                        <td style="height: 17px" background="../../Images/table_bot_mid_bg.gif">
                            <img alt="" height="17" src="../../Images/table_bot_mid_bg.gif" width="25" /></td>
                        <td style="height: 17px">
                            <img alt="" height="17" src="../../Images/table_right_bot_corner.gif" width="16" /></td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </ContentTemplate>
   
</asp:UpdatePanel>

</asp:Content>

