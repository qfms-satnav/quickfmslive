﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="statusMaster.aspx.vb" Inherits="HDM_HDM_Webfiles_Masters_statusMaster" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Status Master</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <style>
        .tablecss {
            width: 100%;
            max-width: 100%;
            border: 1px solid #2556ad;
            border-radius: 5px;
        }

        thead tr {
            height: 48px !important;
            line-height: 48px !important;
            border-radius: 5px;
            background: #ebf0fb !important;
            color: #0f3a9f !important;
            font-size: 12px;
            padding: 0 !important;
            font-weight: bold;
            text-align: center;
        }

            thead tr th, tbody tr td {
                text-align: center;
                border-right: 1px solid #2556ad;
            }

        body tr {
            height: 39px !important;
            line-height: 39px !important;
            border-radius: 5px;
            /*background: white !important;*/
            font-size: 12px;
            text-align: center;
            border-bottom: 1px solid #2556ad;
        }

        .colorBox {
            height: 20px;
            width: 20px;
            display: inline-block;
            margin-top: 10px;
        }
    </style>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Status Master</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server" data-valid-submit="Save()">
                    <div>

                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-show="ActionStatus==1">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <asp:DropDownList ID="ddlModifyStatus" AutoPostBack="true" CssClass="selectpicker" runat="server">
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Color Code<span style="color: red;">*</span></label>
                                    <div data-ng-class="{'has-error': frmFeedBack.$submitted && frmFeedBack.STA_COLOR.$invalid}" onmouseover="Tip('Enter / Select color code or colr name')" onmouseout="UnTip()">
                                        <input id="STA_COLOR" type="text" name="STA_COLOR" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="RegExpCode.REGEXP" class="form-control color-picker" required="required" />&nbsp;
                                        <span class="error" data-ng-show="frmFeedBack.$submitted && frmFeedBack.STA_COLOR.$invalid" style="color: red" data-ng-bind="RegExpCode.REGMSG"></span>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="form-group">
                                    <input type="button" id="SubmitButton" value="Submit" class="btn btn-primary custom-button-color" runat="server" onserverclick="SubmitButton_Click" />
                                    <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" onclick="clearForm()" />
                                    <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <%--data table--%>
                    <div class="row">
                        <div class="col-md-12">
                            <%--<input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>
                            <%--<div data-ag-grid="gridOptions" style="height: 329px; width: 100%;" class="ag-blue"></div>--%>

                            <table class="tablecss">
                                <thead>
                                    <tr>
                                        <%--<th>Status ID</th>--%>
                                        <th>Status</th>
                                        <th>Color</th>
                                        <%--<th>Edit</th>--%>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptStatus" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <%--<td><%# Eval("STA_ID") %></td>--%>
                                                <td><%# Eval("STA_DESC") %></td>
                                                 <td>
                                                    <%--<%# Eval("STA_COLOR") %>--%>
                                                    <span style="background-color :<%# Eval("STA_COLOR") %>" class="colorBox"></span>
                                                </td>
                                                <%--   <td>
                                                    <a href="#" onclick="editRow('<%# Eval("STA_ID") %>', '<%# Eval("STA_COLOR") %>')">
                                                        <i class="fa fa-pencil"></i> Edit
                                                    </a>
                                                </td>--%>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/spectrum/1.8.0/spectrum.min.css">

    <script>
        // Initialize color picker
        $('.color-picker').spectrum({
            preferredFormat: "hex",
            showInput: true,
            showPalette: true,
            palette: [["red", "green", "blue"], ["#000", "#fff"]]
        });

        function clearForm() {
            $('#<%= ddlModifyStatus.ClientID %>').val('0');

            $('.color-picker').spectrum('set', '#000000');
        }

        function editRow(statusId, statusColor) {
            $('.color-picker').spectrum('set', statusColor);

            $('html, body').animate({
                scrollTop: $('#form1').offset().top
            }, 500);
        }
    </script>

</body>
</html>
