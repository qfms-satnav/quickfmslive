<%@ Page Language="VB" AutoEventWireup="false"
    CodeFile="frmChangePassword.aspx.vb" Inherits="frmChangePassword" Title="Change Password" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Change Password" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Change Password</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">User Id</label>
                                            <asp:TextBox ID="txtUsrId" runat="server" ReadOnly="True" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Employee Name</label>
                                            <asp:TextBox ID="txtEmpName" runat="server" ReadOnly="True" CssClass="form-control">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Email Id</label>
                                            <asp:RegularExpressionValidator ID="revemail" runat="server" ControlToValidate="txtmailid"
                                                Display="None" ErrorMessage="Enter Valid Email Id" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                                            </asp:RegularExpressionValidator>
                                            <asp:TextBox ID="txtmailid" runat="server" ReadOnly="True" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Enter Old Password<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvOldPwd" runat="server" ControlToValidate="txtOldPwd"
                                                Display="None" ErrorMessage="Please Enter Old Password" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtOldPwd" runat="server" CssClass="form-control" MaxLength="20"
                                                TextMode="Password">
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Enter New Password<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvNewPwd" runat="server" ControlToValidate="txtpwd"
                                                Display="None" ErrorMessage="Please Enter New Password" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtpwd" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label">Confirm New Password<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvCpwd" runat="server" ControlToValidate="txtcpwd"
                                                Display="None" ErrorMessage="Please Enter Confirm New Password" SetFocusOnError="True">
                                            </asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvPwd" runat="server" ControlToCompare="txtpwd" ControlToValidate="txtcpwd"
                                                Display="None" ErrorMessage="Passwords do not match" SetFocusOnError="True">
                                            </asp:CompareValidator>
                                            <asp:TextBox ID="txtcpwd" runat="server" CssClass="form-control" MaxLength="20" TextMode="Password">                                      
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnSubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Submit" OnClick="btnSubmit_Click" />
                                            <asp:Label ID="lblPwd" runat="server" CssClass="clsnote" Visible="False"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">
                                                <h5>
                                                    <label class="col-md-12 control-label" style="text-align: center;">
                                                        <span style="color: red;">Note:</span> Password Should Contain Minimum of 8 characters, 2 Numerics, 1 Special Character
                                                    </label>
                                                </h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
