﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Emailtemplate2.aspx.vb" Inherits="Emailtemplate2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px;">

            <table style="border: 1px solid #466788; border-collapse: collapse; width: 730px;">
                <tr>
                    <td>
                        <img src="http://devv4.a-mantra.com/BootStrapCSS/images/header.JPG" width="850" />
                           <br /><br />
                        <span style="padding: 8px; margin: 8px; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Dear <b>Jaydeep K Gharat ,  </b>
                            <br /><br />
                        </span>
                        <span style="padding: 8px; margin: 8px; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">
                            <b>Today's total conference room booking details:</b></span>
                        <br /><br />
                        <table style="width: 100%; flex-align: center; padding: 8px; margin: 8px; border-collapse: collapse; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">
                            <tr>
                                <th style="border: 1px solid black; background-color: #466788; color: white; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">City</th>
                                <th style="border: 1px solid black; background-color: #466788; color: white; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Location</th>
                                <th style="border: 1px solid black; background-color: #466788; color: white; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Tower</th>
                                <th style="border: 1px solid black; background-color: #466788; color: white; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Floor</th>
                                <th style="border: 1px solid black; background-color: #466788; color: white; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Conference Room</th>
                                <th style="border: 1px solid black; background-color: #466788; color: white; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Conference Slot</th>
                                <th style="border: 1px solid black; background-color: #466788; color: white; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Booked Hours</th>
                            </tr>
                            <tr>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Hyderabad</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Hyderabad - Madhapur</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">No Tower</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">First Floor</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Presentation Room</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">1:00PM :  4:00PM</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">3</td>
                            </tr>
                            <tr>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Hyderabad</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Hyderabad - Hitech City</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Satnav Towers</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Ground Floor</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">Video Conference Room1</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">10:00PM : 11:00PM</td>
                                <td style="border: 1px solid black; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">1</td>
                            </tr>
                        </table>
                        <br />
                        <br /><br />   <br /><br />
                        <span style="padding: 8px; margin: 8px; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">
                            <b>Regards,</b><br />
                        </span>
                        <span style="padding: 8px; margin: 8px; font-family: 'Open Sans',sans-serif; position: relative; margin: 0; font-size: 12px; mso-line-height-rule: exactly; line-height: 25px">
                            <b>a-mantra Team</b></span>
                           
                        <img src="http://devv4.a-mantra.com/BootStrapCSS/images/footer.JPG" style="display: block;" width="850" />

                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
