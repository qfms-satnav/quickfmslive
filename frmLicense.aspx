﻿<%@ Page Title="" Language="VB" AutoEventWireup="false" CodeFile="frmLicense.aspx.vb" Inherits="frmLicence" %>


<!DOCTYPE html>
<html lang="en">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="~/BootStrapCSS/images/favicon.ico" />
    <title>a-mantra :: Facilities Management Solutions</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="BootStrapCSS/menu/css/metismenu.min.css" rel="stylesheet" />
    <link href="BootStrapCSS/menu/css/menu.css" rel="stylesheet" />

    

    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style type="text/css">
        .control-label {
            color: #00CC66;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation">
            <div class="navbar-header">
                <a class="brand" href='#'>&nbsp;<img src='<%=Page.ResolveUrl("~/BootStrapCSS/images/QuickFMSLogo.jpg")%>' class="ie8-pngcss" alt="" style="margin-top: 2px; height: 49px; width: 245px; margin-left: 610px;" /></a></div>
        </nav>

        <div style="clear: both; min-height: 425px; width: 100%; position: inherit;">
            <div id="wrapper">
                <div id="page-wrapper" class="row">
                    <div class="row form-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>
                                <asp:Label ID="lbl1" runat="server" Text="  Your Subscription has expired, Please contact  support@quickfms.com" style="text-align:center" CssClass="col-md-12 control-label" ForeColor="Green"></asp:Label>
                                    </h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <asp:Literal ID="litMenu" runat="server"></asp:Literal>
        </div>
        <div class="footer">
            <div class="row">
                 <div class="pull-left" style="padding-left: 45px">
                Other Links : 
                <a href="http://www.quickfms.com/" target="_blank">www.quickfms.com</a>
            </div>
                <%--<div class="col-md-3 pull-right">
                    &copy; 2015 QuickFMS
                </div>--%>
                <div class="pull-right">
                <%Response.Write("&copy;" & Year(Now))%>&nbsp;<%Response.Write("QuickFMS v3.2")%>
            </div>
            </div>
        </div>

    </form>
</body>
</html>


