﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="DailyUsageController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Daily Utility Usage </h3>
            </div>
            <div class="card">
                <div class="clearfix">
                    <div class="box-footer text-right">
                        <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                    </div>
                </div>
                <br />
                <form id="form1" name="frmDailyUsage" data-valid-submit="DataForGrid()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDailyUsage.$submitted && frmDailyUsage.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Country" data-selection-mode="single" data-output-model="DailyUse.Country" data-button-label="icon CNY_NAME"
                                    data-item-label="icon CNY_NAME maker" data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="DailyUse.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDailyUsage.$submitted && frmDailyUsage.CNY_NAME.$invalid" style="color: red">Please select Country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDailyUsage.$submitted && frmDailyUsage.CTY_NAME.$invalid}">
                                <label class="control-label">City<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-selection-mode="single" data-input-model="City" data-output-model="DailyUse.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="DailyUse.City[0]" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDailyUsage.$submitted && frmDailyUsage.CTY_NAME.$invalid" style="color: red">Please select City </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDailyUsage.$submitted && frmDailyUsage.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-selection-mode="single" data-input-model="Locations" data-output-model="DailyUse.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="DailyUse.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDailyUsage.$submitted && frmDailyUsage.LCM_NAME.$invalid" style="color: red">Please select Location </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmDailyUsage.$submitted && frmDailyUsage.TWR_NAME.$invalid}">
                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-selection-mode="single" data-input-model="Tower" data-output-model="DailyUse.Tower" data-button-label="icon TWR_NAME"
                                    data-item-label="icon TWR_NAME maker" data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="DailyUse.Tower[0]" name="TWR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDailyUsage.$submitted && frmDailyUsage.TWR_NAME.$invalid" style="color: red">Please select Tower </span>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <%--<label for="txtcode">Date</label>--%>
                            <label class="control-label">Date <span style="color: red;">*</span></label>
                            <div class="input-group date" id='FromDate'>
                                <input type="text" class="form-control" data-ng-model="DailyUse.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                </span>
                            </div>
                            <span class="error" data-ng-show="frmDailyUsage.$submitted && frmDailyUsage.FromDate.$invalid" style="color: red;">Please select Date</span>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="EnableStatus==1">
                            <div class="form-group" data-ng-class="{'has-error': frmDailyUsage.$submitted && frmDailyUsage.CNP_NAME.$invalid}">
                                <label class="control-label">Company</label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="DailyUse.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="frmDailyUsage.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmDailyUsage.$submitted && frmDailyUsage.Company.$invalid" style="color: red">Please select Company </span>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <br />
                            <div class="form-group">
                                <input type="submit" value="Search" id="btnSearch" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                    </div>
                    <br />
                </form>
                <form id="form2" name="frmDailyUsageSer" data-valid-submit="SaveData()" novalidate>
                    <div style="height: 300px" data-ng-show="ActionStatus==1">
                        <div data-ag-grid="DailyUsageGrid" style="height: 100%;" class="ag-blue"></div>
                    </div>
                    <br />
                    <div class="row" data-ng-show="ActionStatus==1">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" id="btnSubmit" class="btn btn-primary custom-button-color" />
                                <%--<input type="submit" value="Modify" id="btnmodify" class='btn btn-primary custom-button-color'  />--%>
                                <input id="btnClear" type="button" class='btn btn-primary custom-button-color' value="Clear" data-ng-click="Clear()" />
                                <%--<a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>--%>
                            </div>
                        </div>
                    </div>

                </form>
                <div class="row">
                    <div class="col-md-12">
                        <div data-ag-grid="semdetailsOptions" style="height: 250px;" class="ag-blue"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js" defer></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script type="text/javascript" defer>
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#FromDate').datepicker('setDate', new moment().format('MM/DD/YYYY'));
            //$('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        }
    </script>

    <script type="text/javascript" defer>
        $(document).ready(function () {
            setDateVals();
        });

    </script>

    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/DailyUsage.js" defer></script>
</body>
</html>
