﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        /* .panel-title {
            font-size: 16px;
        }
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
*/
        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        /* .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }*/
    </style>
</head>
<body data-ng-controller="CustomizedReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%-- <div ba-panel ba-panel-title="Utility Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Utility Report</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                </div>
                            </div>
                            <br />--%>
                <form id="form1" name="CustomizedReport" data-valid-submit="LoadData()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                    data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                    data-tick-property="ticked" data-max-labels="2">
                                </div>
                                <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid" style="color: red">Please select Country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid}">
                                <label class="control-label">City<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid" style="color: red">Please select City </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="LocationSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid" style="color: red">Please select Location </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid}">
                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Towers" data-output-model="Customized.Towers" data-button-label="icon TWR_NAME "
                                    data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid" style="color: red">Please select Tower </span>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Quick Select</label>
                                <br />
                                <select id="ddlRange" class="selectpicker" ng-model="Customized.QuickSelectedParam" data-ng-change="rptDateRanges()">
                                    <%--   <option value="">Select Range</option>--%>
                                    <option value="Daily">Daily</option>
                                    <%-- <option value="YESTERDAY">Yesterday</option>--%>
                                    <option value="Weekly">Weekly</option>
                                    <%-- <option value="30">Last 30 Days</option>--%>
                                    <%--<option value="THISMONTH">Monthly</option>--%>
                                    <option value="Monthly">Monthly</option>
                                    <option value="Quarterly">Quarterly</option>
                                    <%-- <option value="Half Yearly">Half Yearly</option>--%>
                                    <option value="Yearly">Yearly</option>
                                </select>


                            </div>

                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="EnableDateTextboxes">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please select From Date</span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="EnableDateTextboxes">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='todate'>
                                    <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please select To Date</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" id="divyear" data-ng-show="EnableYearStatus">

                            <div class="form-group">
                                <label for="txtcode">Select Year</label>
                                <select id="year" class="selectpicker" data-ng-model="Customized.DateParam">
                                    <option value="2024">2024</option>
                                     <option value="2023">2023</option>
                                     <option value="2022">2022</option>

                                </select>
                            </div>

                            <%--</div> --%>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="EnableStatus==1">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CNP_NAME.$invalid}">
                                <label class="control-label">Company</label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="Customized.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="CustomizedReport.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Company.$invalid" style="color: red">Please select Company </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <br />
                            <div class=" box-footer pull right">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>
                    </div>
                </form>
                <form id="form2">
                    <div data-ng-show="GridVisiblity">
                        <div>
                            <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport(Customized,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                            <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="text" class="selectpicker form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
        <%--</div>
            </div>
        </div>--%>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>


    <script src="../../Scripts/jspdf.min.js" defer></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/DailyUsageReport.js" defer></script>

    <script type="text/javascript" defer>
        function setDateVals() {
            $('#Text1').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#Text2').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#Text1').datepicker('setDate', new Date(moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY')));
            $('#Text2').datepicker('setDate', new Date(moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY')));
        }
    </script>

    <script type="text/javascript" defer>
        $(document).ready(function () {
            setDateVals();
        });
    </script>
    <script type="text/javascript" defer>
        //inline javascript
        $(function () {
            //$('#Yearmonth').yearpicker();
            //$('#month').monthpicker();
            $('#year').yearpicker();
            $('#halfyear').halfyearpicker();
            $('#quarteryear').quarteryearpicker();
        });

        0

        //picker.js  
        $.fn.extend(
            {
                yearpicker: function () {
                    var select = $(this);

                    var year = new Date().getFullYear();
                    for (var i = -10; i < 1; i++) {
                        var option = $('<option/>');
                        var year_to_add = year + i;

                        option.val(year_to_add).text(year_to_add);

                        if (year == year_to_add) {
                            option
                                .css('font-weight', 'bold')
                                .attr('selected', 'selected');
                        }

                        select.append(option);
                    }
                },
                halfyearpicker: function () {
                    var select = $(this);

                    var date = new Date();
                    var year = date.getFullYear();
                    var half = Math.floor(date.getMonth() / 6);

                    for (var i = -10; i < 11; i++) {
                        var year_to_add = year + i;

                        for (var j = 0; j < 2; j++) {
                            var option = $('<option/>');
                            var half_text = j == 0 ? 'Jan-Jun' : 'Jul-Dec';
                            var value = year_to_add + '-' + (j + 1);
                            var text = year_to_add + ' ' + half_text;

                            option.val(value).text(text);

                            if (year_to_add == year && half == j) {
                                option
                                    .css('font-weight', 'bold')
                                    .attr('selected', 'selected');
                            }

                            select.append(option);
                        }
                    }
                },
                quarteryearpicker: function () {
                    var select = $(this);

                    var date = new Date();
                    var year = date.getFullYear();
                    var quarter = Math.floor(date.getMonth() / 3);

                    for (var i = -10; i < 11; i++) {
                        var year_to_add = year + i;

                        for (var j = 0; j < 4; j++) {
                            var option = $('<option/>');
                            var quarter_text = get_quarter_text(j);

                            var value = year_to_add + '-' + (j + 1);
                            var text = year_to_add + ' ' + quarter_text;

                            option.val(value).text(text);

                            if (year_to_add == year && quarter == j) {
                                option
                                    .css('font-weight', 'bold')
                                    .attr('selected', 'selected');
                            }

                            select.append(option);
                        }
                    }

                    function get_quarter_text(num) {
                        switch (num) {
                            case 0:
                                return 'Jan-Mar';
                            case 1:
                                return 'Apr-Jun';
                            case 2:
                                return 'Jul-Sep';
                            case 3:
                                return 'Oct-Dec';
                        }
                    }

                }, monthpicker: function () {
                    var select = $(this);
                    var d = new Date();
                    var monthArray = new Array();
                    monthArray[0] = "January";
                    monthArray[1] = "February";
                    monthArray[2] = "March";
                    monthArray[3] = "April";
                    monthArray[4] = "May";
                    monthArray[5] = "June";
                    monthArray[6] = "July";
                    monthArray[7] = "August";
                    monthArray[8] = "September";
                    monthArray[9] = "October";
                    monthArray[10] = "November";
                    monthArray[11] = "December";
                    for (m = 0; m <= 11; m++) {
                        var optn = document.createElement("OPTION");
                        optn.text = monthArray[m];
                        // server side month start from one
                        optn.value = (m + 1);

                        // if june selected
                        if (m == 5) {
                            optn.selected = true;
                        }

                        document.getElementById('month').options.add(optn);
                    }
                }
            });



    </script>
</body>
</html>
