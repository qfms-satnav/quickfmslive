﻿app.service("DailyUsageService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {



    this.GetGriddata = function (Ddata) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/DailyUsage/GetGriddata', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.SaveData = function (Ddata) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/DailyUsage/SaveData', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

}]);
app.controller('DailyUsageController', ['$scope', '$q', '$http', 'DailyUsageService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, DailyUsageService, UtilityService, $timeout, $filter) {
    $scope.DailyUse = {};
    $scope.DailyUse.CNP_NAME = [];

    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Tower = [];

    $scope.DailyUse.Country = [];
    $scope.DailyUse.City = [];
    $scope.DailyUse.Locations = [];
    $scope.DailyUse.Tower = [];
    $scope.ActionStatus = 0;

    $scope.Pageload = function () {

        UtilityService.getCountires(1).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;

            }
        });

        UtilityService.getCities(1).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;

            }
        });

        UtilityService.getLocations(1).then(function (response) {
            if (response.data != null) {
                $scope.Locations = response.data;

            }
        });
        UtilityService.getTowers(1).then(function (response) {
            if (response.data != null) {
                $scope.Tower = response.data;

            }
        });

        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                //$scope.Customized.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.DailyUse.CNP_NAME.push(a);
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });


    }
    $scope.DataForGrid = function () {
        var Ddata = {
            twrlst: $scope.DailyUse.Tower,
            date: $scope.DailyUse.FromDate
        };
        DailyUsageService.GetGriddata(Ddata).then(function (response) {
            console.log(response);
            if (response.length != 0) {
                $scope.ActionStatus = 1;
                $scope.gridata = response;
                $scope.DailyUsageGrid.api.setRowData([]);
                $scope.DailyUsageGrid.api.setRowData($scope.gridata);
            }
            else {
                $scope.ActionStatus = 0;
                $scope.gridata = response;
                $scope.DailyUsageGrid.api.setRowData([]);
                $scope.DailyUsageGrid.api.setRowData($scope.gridata);
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', 'Budget not allocated for selected Tower');
            }

        });
    }
    $scope.DailyUsagecolumnDefs = [
            { headerName: "Country ", field: "EM_UM_CNY_ID", cellClass: "grid-align", },
            { headerName: "City ", field: "CTY_NAME", cellClass: "grid-align", },
            { headerName: "Location ", field: "LCM_NAME", cellClass: "grid-align", },
            { headerName: "Tower ", field: "TWR_NAME", cellClass: "grid-align", },
            { headerName: "Utility Type ", field: "EM_DUD_TYPE", cellClass: "grid-align", },
            { headerName: " Meter Number", cellClass: "grid-align", filter: 'set', template: "<input type='textbox' class='form-control'  ng-model='data.EM_DUD_SERNO'  style='text-align: center' >", suppressMenu: true, },
        { headerName: " Quantity/Unit", cellClass: "grid-align", filter: 'set', template: "<input type='textbox' class='form-control'  ng-model='data.EM_DUD_QUNTY' style='text-align: center' >", suppressMenu: true, },
            { headerName: "Units", field: "EM_UM_MEASUR", cellClass: "grid-align", },


    ];
    $scope.DailyUsageGrid = {
        columnDefs: $scope.DailyUsagecolumnDefs,
        rowData: null,
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        enableSorting: true,


        onReady: function () {
            $scope.DailyUsageGrid.api.sizeColumnsToFit()
        },

    };




    //// Country Events
    $scope.CnyChangeAll = function () {
        $scope.DailyUse.Country = $scope.Country;
        $scope.CnyChanged();
    }

    $scope.CnySelectNone = function () {
        $scope.DailyUse.Country = [];
        $scope.CnyChanged();
    }

    $scope.CnyChanged = function () {
        if ($scope.DailyUse.Country.length != 0) {
            UtilityService.getCitiesbyCny($scope.DailyUse.Country, 1).then(function (response) {
                if (response.data != null)
                    $scope.City = response.data;
                else
                    $scope.City = [];
            });
        }
        else
            $scope.City = [];
    }



    //// City Events
    $scope.CtyChangeAll = function () {
        $scope.DailyUse.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.DailyUse.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.DailyUse.City, 1).then(function (response) {
            if (response.data != null)
                $scope.Locations = response.data;
            else
                $scope.Locations = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DailyUse.Country.push(cny);
            }
        });
    }

    ///// Location Events
    $scope.LcmChangeAll = function () {
        $scope.DailyUse.Locations = $scope.Locations;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.DailyUse.Locations = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.DailyUse.Locations, 1).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Locations, function (value, key) {
            console.log(value);
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DailyUse.Country.push(cny);
            }
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DailyUse.City.push(cty);
            }
        });


    }



    //// Tower Events
    $scope.TwrChangeAll = function () {
        $scope.DailyUse.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.DailyUse.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.DailyUse.Tower, 1).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.DailyUse.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.DailyUse.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.DailyUse.Locations.push(lcm);
            }
        });

        //$scope.DataForGrid();
        $scope.ActionStatus = 0;
    }

    $scope.Pageload();

    $scope.Clear = function () {

        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });
        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Locations, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Tower, function (tower) {
            tower.ticked = false;
        });

        $scope.DailyUsageGrid.api.setRowData([]);
        $scope.ActionStatus = 0;
        $scope.DailyUse.FromDate = new moment().format('MM/DD/YYYY');
    }

    $scope.SaveData = function () {
        progress(0, 'Loading', true);
        var Ddata = {
            DailyUsgeDetList: $scope.gridata,
            twrlst: $scope.DailyUse.Tower,
            date: $scope.DailyUse.FromDate,
            companyid: $scope.DailyUse.CNP_NAME[0].CNP_ID
        };
        console.log(Ddata);
        DailyUsageService.SaveData(Ddata).then(function (response) {
            progress(0, 'Loading...', true);
            if (response.data != null) {
                progress(0, '', false);
                $scope.Clear();
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        });


    }


}]);