﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    

    <!--[if lt IE 9]>
        <script src="../../../BootStrapCSS/Scripts/html5shiv.js"></script>
        <script src="../../../BootStrapCSS/Scripts/respond.min.js"></script>
    <![endif]-->
    <link href="../../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../../Scripts/aggrid/css/theme-blue.min.css" rel="stylesheet" />
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="UnitMappingController" class="amantra">
    <div id="page-wrapper" class="row">
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Unit Mapping Master</legend>
                    </fieldset>
                    <form role="form" id="form1" name="frmUnitMapping" class="form-horizontal well" data-valid-submit="Save()" novalidate>
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Country<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmUnitMapping.$submitted && frmUnitMapping.EM_UM_CNY_ID.$invalid}">
                                                    <select id="EM_UM_CNY_ID" name="EM_UM_CNY_ID" data-ng-model="EntityAdmin.EM_UM_CNY_ID" class="form-control" data-live-search="true" required>
                                                        <option value="">--Select--</option>
                                                        <option data-ng-repeat="Enty in ParentEntityList" value="{{Enty.PRNT_CODE}}">{{Enty.PRNT_NAME}}</option>
                                                    </select>
                                                    <span class="error" data-ng-show="frmUnitMapping.$submitted && frmUnitMapping.EM_UM_CNY_ID.$invalid" style="color: red">Please Select Country </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Electric<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmUnitMapping.$submitted && frmUnitMapping.EM_UM_ELE.$invalid}" onmouseover="Tip('Enter code in alphabets and numbers)" onmouseout="UnTip()">
                                                    <input id="EM_UM_ELE" type="text" name="EM_UM_ELE" data-ng-readonly="ActionStatus==1" maxlength="15" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="EntityAdmin.EM_UM_ELE" autofocus class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmUnitMapping.$submitted && frmUnitMapping.EM_UM_ELE.$invalid" style="color: red">Please enter valid Electic Unit </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Water<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmUnitMapping.$submitted && frmUnitMapping.EM_UM_WTR.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers)" onmouseout="UnTip()">
                                                    <input id="EM_UM_WTR" type="text" name="EM_UM_WTR" maxlength="50" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="EntityAdmin.EM_UM_WTR" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmUnitMapping.$submitted && frmUnitMapping.EM_UM_WTR.$invalid" style="color: red">Please enter valid water units </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Natural Gas<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmUnitMapping.$submitted && frmUnitMapping.EM_UM_NG.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers)" onmouseout="UnTip()">
                                                    <input id="EM_UM_NG" type="text" name="EM_UM_NG" maxlength="50" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="EntityAdmin.EM_UM_NG" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmUnitMapping.$submitted && frmUnitMapping.EM_UM_NG.$invalid" style="color: red">Please enter valid natural gas </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Fuel<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmUnitMapping.$submitted && frmUnitMapping.EM_UM_FUL.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers)" onmouseout="UnTip()">
                                                    <input id="EM_UM_FUL" type="text" name="EM_UM_FUL" maxlength="50" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="EntityAdmin.EM_UM_FUL" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmUnitMapping.$submitted && frmUnitMapping.EM_UM_FUL.$invalid" style="color: red">Please enter valid fuel unit </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Solar<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div data-ng-class="{'has-error': frmUnitMapping.$submitted && frmUnitMapping.EM_UM_SLR.$invalid}" onmouseover="Tip('Enter Name in alphabets,numbers)" onmouseout="UnTip()">
                                                    <input id="EM_UM_SLR" type="text" name="EM_UM_SLR" maxlength="50" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="EntityAdmin.EM_UM_SLR" class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmUnitMapping.$submitted && frmUnitMapping.EM_UM_SLR.$invalid" style="color: red">Please enter valid solar unit </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-4 control-label">Remarks</label>
                                            <div class="col-md-8">
                                                <div onmouseover="Tip('Enter Remarks upto 500 characters')" onmouseout="UnTip()">
                                                    <textarea id="Textarea1" runat="server" data-ng-model="EntityAdmin.EM_UM_REM" class="form-control" maxlength="500"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="row" data-ng-show="ActionStatus==1">
                                            <label class="col-md-4 control-label">Status<span style="color: red;">*</span></label>
                                            <div class="col-md-8">
                                                <div>
                                                    <select id="EM_UM_STA_ID" name="EM_UM_STA_ID" data-ng-model="EntityAdmin.EM_UM_STA_ID" class="form-control">
                                                        <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                        <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                        <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                        <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="padding-left: 30px;">
                            <input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                            <div data-ag-grid="gridOptions" style="height: 250px; width: 100%;" class="ag-blue"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid"]);
    </script>
    <script src="../../../Utility.js" defer></script>
</body>
</html>



