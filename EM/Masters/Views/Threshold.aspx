﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <link href="../../../BootStrapCSS/maploader.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.draw.css" rel="stylesheet" />
    <link href="../../../BootStrapCSS/leaflet/leaflet.label.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="ThresholdController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Energy Budget Master</h3>
            </div>
            <div class="card">
                <%--<div class="panel-body" style="padding-right: 10px;">--%>
                <div class="clearfix">
                    <div class="box-footer text-right">
                        <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                    </div>
                </div>
                <br />
                <form id="form1" name="frmThreshold" data-valid-submit="SaveData()" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmThreshold.$submitted && frmThreshold.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select selection-mode="single" data-input-model="Country" data-output-model="Threshold.Country" data-button-label="icon CNY_NAME"
                                    data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Threshold.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmThreshold.$submitted && frmThreshold.CNY_NAME.$invalid" style="color: red">Please select Country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmThreshold.$submitted && frmThreshold.CTY_NAME.$invalid}">
                                <label class="control-label">City <span style="color: red;">**</span></label>
                                <div isteven-multi-select selection-mode="single" data-input-model="City" data-output-model="Threshold.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Threshold.City[0]" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmThreshold.$submitted && frmThreshold.CTY_NAME.$invalid" style="color: red">Please select City </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmThreshold.$submitted && frmThreshold.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select selection-mode="single" data-input-model="Locations" data-output-model="Threshold.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Threshold.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmThreshold.$submitted && frmThreshold.LCM_NAME.$invalid" style="color: red">Please select Location </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmThreshold.$submitted && frmThreshold.TWR_NAME.$invalid}">
                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                <div isteven-multi-select selection-mode="single" data-input-model="Towers" data-output-model="Threshold.Towers" data-button-label="icon TWR_NAME"
                                    data-item-label="icon TWR_NAME maker" data-on-item-click="TwrChanged()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Threshold.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmThreshold.$submitted && frmThreshold.TWR_NAME.$invalid" style="color: red">Please select Tower </span>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmThreshold.$submitted && frmThreshold.Name.$invalid}" data-ng-show="ActionStatus==1">
                                <label class="control-label">Status <span style="color: red;">*</span></label>
                                <div isteven-multi-select selection-mode="single" data-input-model="StaDet" data-output-model="Threshold.StaDet" data-button-label="icon Name"
                                    data-item-label="icon Name maker" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Threshold.StaDet[0]" name="Name" style="display: none" />
                                <span class="error" data-ng-show="frmThreshold.$submitted && frmThreshold.Name.$invalid" style="color: red">Please select Status </span>
                            </div>
                        </div>
                    </div>
                    <div style="height: 300px; width: 100%" data-ng-show="GridVisible==1">
                        <div data-ag-grid="ThreshDeclareGrid" style="height: 100%;" class="ag-blue"></div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class='btn btn-primary custom-button-color' data-ng-show="ActionStatus==1" />
                                <input type="button" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearData()" />
                                <a class='btn btn-primary custom-button-color' href="javascript:history.back()">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <%--<script src="../../../SMViews/Utility.js"></script>--%>
    <%--<script src="../JS/Threshold.js"></script>--%>
    <script src="../../../SMViews/Utility.min.js" defer></script>
    <script src="../JS/Threshold.js" defer></script>
    <%--<script src="../../JS/DailyUsage.js"></script>--%>
    <script src="../../JS/DailyUsage.min.js" defer></script>
</body>
</html>
