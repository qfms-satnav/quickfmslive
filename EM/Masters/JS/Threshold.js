﻿app.service('ThresholdService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    var deferred = $q.defer();

    this.GetGriddata = function (Ddata) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Threshold/GetGriddata', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.SaveData = function (Ddata) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/Threshold/SaveData', Ddata)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

}]);

app.controller('ThresholdController', ['$scope', '$q', 'ThresholdService', 'UtilityService', 'DailyUsageService', '$timeout', function ($scope, $q, ThresholdService, UtilityService, DailyUsageService, $timeout) {

    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.Threshold = {};
    $scope.ParentEntityList = [];
    $scope.ActionStatus = 0;
    $scope.GridVisible = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    //$scope.SelectedData = [];

    UtilityService.getCountires(1).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;
        }
    });

    UtilityService.getCities(1).then(function (response) {
        if (response.data != null) {
            $scope.City = response.data;
        }
    });

    UtilityService.getLocations(1).then(function (response) {
        if (response.data != null) {
            $scope.Locations = response.data;
        }
    });

    UtilityService.getTowers(1).then(function (response) {
        if (response.data != null) {
            $scope.Towers = response.data;
        }
    });

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Threshold.Country, 1).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Threshold.City, 1).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Threshold.Country[0] = cny;
            }
        });
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.Threshold.Locations, 1).then(function (response) {
            $scope.Towers = response.data;
        }, function (error) {
            console.log(error);
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Threshold.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Threshold.City[0] = cty;
            }
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Threshold.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.ctySelectAll = function () {
        $scope.Threshold.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.Threshold.Towers, 1).then(function (response) {
            //if (response.data != null)
            //    $scope.Floors = response.data;
            //else
            //    $scope.Floors = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Locations, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Towers, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Threshold.Country[0] = cny;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Threshold.City[0] = cty;
            }
        });

        angular.forEach($scope.Towers, function (value, key) {
            var lcm = _.find($scope.Locations, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.Threshold.Locations[0] = lcm;
            }
        });
        $scope.DataForGrid();
    }

    //// Tower Events
    $scope.twrSelectAll = function () {
        $scope.DailyUse.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.DailyUse.Tower = [];
        $scope.TwrChanged();
    }

    $scope.DataForGrid = function () {
        var Ddata = {
            cnylst: $scope.Threshold.Country, twrlst: $scope.Threshold.Towers
        };
        ThresholdService.GetGriddata(Ddata).then(function (data) {
            $scope.GridVisible = 1;
            $scope.gridata = data;
            $scope.ThreshDeclareGrid.api.setRowData([]);
            $scope.ThreshDeclareGrid.api.setRowData($scope.gridata);

        });
    }

    $scope.ThreshDeclarecolumnDefs = [
          { headerName: "Country ", field: "EM_UM_CNY_ID", cellClass: "grid-align",  },
           { headerName: "City ", field: "CTY_NAME", cellClass: "grid-align", },
            { headerName: "Location ", field: "LCM_NAME", cellClass: "grid-align", },
             { headerName: "Tower ", field: "TWR_NAME", cellClass: "grid-align", },
          { headerName: "Utility Type ", field: "EM_THRD_TYPE", cellClass: "grid-align", },
          { headerName: "Quantity/Unit", cellClass: "grid-align", filter: 'set', template: "<input type='textbox' ng-model='data.EM_THRD_QUNTY' class='form-control' style='text-align: center' >", suppressMenu: true, },
          { headerName: "Units", field: "EM_UM_MEASUR", cellClass: "grid-align", },
    ];

    $scope.ThreshDeclareGrid = {
        columnDefs: $scope.ThreshDeclarecolumnDefs,
        rowData: null,
        angularCompileRows: true,
        enableScrollbars: false,
        enableFilter: true,
        enableSorting: true,
        onReady: function () {
            $scope.ThreshDeclareGrid.api.sizeColumnsToFit()
        },
    };
    
    //$scope.EditFunction = function (data) {
    //    $scope.Threshold = {};
    //    angular.copy(data, $scope.Threshold);
    //    $scope.ActionStatus = 1;
    //    $scope.IsInEdit = true;
    //}

    $scope.ClearData = function () {
        $scope.ActionStatus = 0;
        $scope.GridVisible = 0;
        $scope.IsInEdit = false;
        $scope.Threshold = {};
        $scope.gridata = [];
        angular.forEach($scope.Country, function (country) {
            country.ticked = false;
        });
        angular.forEach($scope.City, function (city) {
            city.ticked = false;
        });
        angular.forEach($scope.Locations, function (location) {
            location.ticked = false;
        });
        angular.forEach($scope.Towers, function (tower) {
            tower.ticked = false;
        });
        $scope.frmThreshold.$submitted = false;
    }

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }

    $scope.SaveData = function () {
        var Ddata = {
            ThresholdDetList: $scope.gridata, twrlst: $scope.Threshold.Towers
        };
        console.log(Ddata);
        ThresholdService.SaveData(Ddata).then(function (response) {
            if (response.data != null) {
                progress(0, '', false);
                $scope.ClearData();
                //$scope.back();
                showNotification('success', 8, 'bottom-right', response.Message);
            }
            else {
                progress(0, '', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        });
    }
    
}]);