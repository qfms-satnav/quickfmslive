﻿app.service("ConferenceTypeMasterService", ['$http', '$q', function ($http, $q) {
    this.SaveConfDetails = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceTypeMaster/SaveConferenceDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.ModifyConference = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceTypeMaster/ModifyConferenceDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.GetConfGrid = function () {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceTypeMaster/GetGridData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('ConferenceTypeMasterController', ['$scope', '$q', 'ConferenceTypeMasterService', 'UtilityService', '$filter', function ($scope, $q, ConferenceTypeMasterService, UtilityService, $filter) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.ConferenceType = {};
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.ShowMessage = false;

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        ConferenceTypeMasterService.GetConfGrid().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData(data);
            setTimeout(function () {
                progress(0, 'Loading...', false);
            }, 700);
        }, function (error) {
            console.log(error);
        });
    }

    $scope.Save = function () {
        if ($scope.IsInEdit) {
            ConferenceTypeMasterService.ModifyConference($scope.ConferenceType).then(function (response) {
                var savedobj = {};
                angular.copy($scope.ConferenceType, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully uploaded";

                ConferenceTypeMasterService.GetConfGrid().then(function (data) {
                    $scope.gridata = data;
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.IsInEdit = false;
                $scope.EraseData();

                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            });
        }
        else {
            $scope.ConferenceType.CONF_STA_ID = "1";
            ConferenceTypeMasterService.SaveConfDetails($scope.ConferenceType).then(function (response) {
                $scope.ShowMessage = true;
                $scope.Success = "Data successfully inserted";
                var savedobj = {};
                angular.copy($scope.ConferenceType, savedobj)
                $scope.gridata.unshift(savedobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 700);
                $scope.ConferenceType = {};
            }, function (error) {
                $scope.ShowMessage = true;
                $scope.Success = error.data;
                showNotification('error', 8, 'bottom-right', $scope.Success);
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 1000);
                console.log(error);
            });
        }
    }
    var columnDefs = [
               { headerName: "Reservation Code", field: "CONF_CODE", width: 190, cellClass: 'grid-align' },
               { headerName: "Reservation Name", field: "CONF_NAME", width: 280, cellClass: 'grid-align' },
               { headerName: "Status", template: "{{ShowStatus(data.CONF_STA_ID)}}", width: 170, cellClass: 'grid-align' },
               { headerName: "Action", template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $scope.gridOptions = {
        columnDefs: columnDefs,
        enablefilter: true,
        enableCellSelection: false,
        rowData: null,
        enableSorting: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    };
    setTimeout(function () {
        $scope.LoadData();
    }, 1000);

    $scope.EditData = function (data) {
        $scope.ConferenceType = {};
        angular.copy(data, $scope.ConferenceType);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
    }

    $scope.EraseData = function () {
        $scope.ConferenceType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frm.$submitted = false;

    }

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }
}]);