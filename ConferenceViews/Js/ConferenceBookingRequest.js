﻿app.service("ConferenceBookingRequestService", function ($http, $q, UtilityService) {

});
app.controller("ConferenceBookingRequestController", function ($scope, $q, $location, ConferenceBookingRequestService, UtilityService, $filter) {
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor = [];
    $scope.Capacity = [];
    $scope.BookingRequest = {};

    UtilityService.getCities(2).then(function (response) {
        if (response.data != null) {
            $scope.City = response.data;
        }
    });

    UtilityService.getLocations(2).then(function (response) {
        if (response.data != null) {
            $scope.Location = response.data;
        }
    });

    UtilityService.getTowers(2).then(function (response) {
        if (response.data != null) {
            $scope.Tower = response.data;
        }
    });

    UtilityService.getFloors(2).then(function (response) {
        if (response.data != null) {
            $scope.Floor = response.data;
        }
    });

    $scope.CtyChangeAll = function () {
        $scope.BookingRequest.City = $scope.City;
        $scope.CtyChanged();
    }

    $scope.CtySelectNone = function () {
        $scope.BookingRequest.City = [];
        $scope.CtyChanged();
    }

    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.BookingRequest.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.BookingRequest.Country.push(cny);
            }
        });
    }

    $scope.LcmChangeAll = function () {
        $scope.BookingRequest.Location = $scope.Location;
        $scope.LcmChanged();
    }

    $scope.LcmSelectNone = function () {
        $scope.BookingRequest.Location = [];
        $scope.LcmChanged();
    }

    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.BookingRequest.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });


        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.BookingRequest.Country.push(cny);
            }
        });
        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.BookingRequest.City.push(cty);
            }
        });
    }

    $scope.TwrChangeAll = function () {
        $scope.BookingRequest.Tower = $scope.Tower;
        $scope.TwrChanged();
    }

    $scope.TwrSelectNone = function () {
        $scope.BookingRequest.Tower = [];
        $scope.TwrChanged();
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.BookingRequest.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.BookingRequest.Country.push(cny);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.BookingRequest.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.BookingRequest.Location[0].push(lcm);
            }
        });



    }

    $scope.FlrChangeAll = function () {
        $scope.BookingRequest.Floor = $scope.Floor;
        $scope.FlrChanged();
    }

    $scope.FlrSelectNone = function () {
        $scope.BookingRequest.Floor = [];
        $scope.FlrChanged();
    }

    $scope.FlrChanged = function () {

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.BookingRequest.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.BookingRequest.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.BookingRequest.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.BookingRequest.Tower.push(twr);
            }
        });

    }
});