﻿app.service("ConferenceRoomBookingService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {
    this.GetConferenceRooms = function (conferenceBookingSearch) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomBooking/GetConferenceRooms', conferenceBookingSearch)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetConferenceRoomBookedTimings = function (conferenceTimingFilter) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomBooking/GetConferenceRoomBookedTimings', conferenceTimingFilter)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveBookingRequest = function (bookingRequestModel) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomBooking/SaveBookingRequest', bookingRequestModel)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.DeleteBookingRequest = function (bookingCancelModel) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomBooking/DeleteBookingRequest', bookingCancelModel)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetInternalAttendess = function (searchText) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ConferenceRoomBooking/GetInternalAttendess?searchText=' + searchText)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetAttendessByConferenceId = function (conferenceReqId) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ConferenceRoomBooking/GetAttendessByConferenceId?conferenceReqId=' + conferenceReqId)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.ModifyBookingRequest = function (bookingRequestModel) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomBooking/ModifyBookingRequest', bookingRequestModel)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetConferenceBehalfUsers = function (searchText) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ConferenceRoomBooking/GetConferenceBehalfUsers?searchText=' + searchText)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.IsEmployee = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ConferenceRoomBooking/IsEmployee')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.CheckSlotAvalability = function (conferenceSlotVM) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomBooking/CheckSlotAvalability', conferenceSlotVM)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.IsSlotBooked = function (conferenceSlotVM) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ConferenceRoomBooking/IsSlotBooked', conferenceSlotVM)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetCreatedUserInfo = function (conferenceReqId) {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/ConferenceRoomBooking/GetCreatedUserInfo?conferenceReqId=' + conferenceReqId)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);

app.controller('ConferenceRoomBookingController', ['$scope', '$q', 'ConferenceRoomBookingService', 'UtilityService', '$filter', function ($scope, $q, ConferenceRoomBookingService, UtilityService, $filter) {
    $scope.TimeList = Array.from({ length: 23 + 1 }, (_, index) => index);
    $scope.MinutesList = Array.from({ length: 60 / 5 + 1 }, (_, index) => index * 5);
    $scope.ConferenceRoomBooking = {};
    $scope.City = [];
    $scope.Location = [];
    $scope.Tower = [];
    $scope.Floor;
    $scope.isButtonDisabled = true;
    $scope.ConferenceRoomBooking.City;
    $scope.ConferenceRoomBooking.Location;
    $scope.ConferenceRoomBooking.Tower;
    $scope.ConferenceRoomBooking.Floor;
    $scope.ConferenceRooms = [];
    $scope.ConferenceTimings = [];
    $scope.SelectedConferenceRoom = {};
    $scope.Slots = [];
    $scope.Hours = ["00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "00"];
    $scope.SlotMinutes = ["00", "30", "00"];
    let startType = "AM";
    let endType = "AM";
    $scope.SelectedConferenceInDropDown = "";
    $scope.SelectedSlot = {};
    $scope.ConferenceRoomBooking.FromDate = moment().format('MM/DD/yyyy');
    $scope.BookingBtnEnabled = true;
    $scope.BookingOptions = ['Self', 'On behalf of'];
    $scope.BookingType = $scope.BookingOptions[0];
    $scope.ShowBehalfField = false;
    $scope.BookingSlotType = ['One Time', 'Recurring']
    $scope.SelectedBookingSlotType = $scope.BookingSlotType[0];
    $scope.EnableEndField = false;
    $scope.IsEmployee = false;
    $scope.ShowSubmitButtons = true;

    //$scope.init = function () {
    //    for (let i = 0; i < ($scope.Hours.length - 1); i++) {
    //        for (let j = 0; j < ($scope.SlotMinutes.length - 1); j++) {
    //            let slot = {
    //                startHour: $scope.Hours[i],
    //                startMinutes: $scope.SlotMinutes[j],
    //                startType: startType,
    //                endHour: $scope.Hours[i],
    //                endMinutes: $scope.SlotMinutes[j + 1],
    //                endType: endType
    //            };
    //            if (j == ($scope.SlotMinutes.length - 2)) {
    //                slot.endHour = $scope.Hours[i + 1];
    //                if (slot.endHour == "12") {
    //                    slot.endType = "PM";
    //                    endType = "PM";
    //                    startType = "PM";
    //                }
    //            }
    //            $scope.Slots.push(slot);
    //        }
    //    }
    //    console.log($scope.Slots);
    //}
    //$scope.init();

    UtilityService.getCities(2).then(function (response) {
        if (response.data != null) {
            $scope.City = response.data;
            UtilityService.getLocations(2).then(function (response) {
                if (response.data != null) {
                    $scope.Location = response.data;
                    UtilityService.getTowers(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Tower = response.data;
                            UtilityService.getFloors(2).then(function (response) {
                                if (response.data != null) {
                                    $scope.Floor = response.data;
                                }
                            }).then($scope.SingleItem());
                        }
                    });
                }
            });
        }
        ConferenceRoomBookingService.IsEmployee().then(function (response) {
            if (response != null) {
                $scope.IsEmployee = response;
            }
        });
        $scope.GetConferenceRooms();
    });

    $scope.SingleItem = function () {
        function setTickedTrueIfNotSet(array) {
            if (array.length === 1 && !array[0].ticked) {
                array[0].ticked = true;
            }
        }

        setTickedTrueIfNotSet($scope.City);
        setTickedTrueIfNotSet($scope.Location);
        setTickedTrueIfNotSet($scope.Tower);
    };



    $scope.CtyChanged = function () {
        UtilityService.getLocationsByCity($scope.ConferenceRoomBooking.City, 2).then(function (response) {
            if (response.data != null)
                $scope.Location = response.data;
            else
                $scope.Location = [];
        });

    }

    $scope.LcmChanged = function () {
        UtilityService.getTowerByLocation($scope.ConferenceRoomBooking.Location, 2).then(function (response) {
            if (response.data != null)
                $scope.Tower = response.data;
            else
                $scope.Tower = [];
        });

        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceRoomBooking.City.push(cty);
            }
        });
    }

    $scope.TwrChanged = function () {
        UtilityService.getFloorByTower($scope.ConferenceRoomBooking.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        });

        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceRoomBooking.City.push(cty);
            }
        });
        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ConferenceRoomBooking.Location[0].push(lcm);
            }
        });
    }

    $scope.FlrChanged = function () {

        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ConferenceRoomBooking.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ConferenceRoomBooking.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.ConferenceRoomBooking.Tower.push(twr);
            }
        });
    }
    $scope.FlrSelectNone = function () {
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
    }
    var Cost = "";
    $scope.GetConferenceRooms = function () {
        progress(0, 'Loading...', true);
        var conferenceRoomSearch = {
            CityCode: $scope.ConferenceRoomBooking.City != null && $scope.ConferenceRoomBooking.City.length > 0 ? $scope.ConferenceRoomBooking.City[0].CTY_CODE : null,
            LocationCode: $scope.ConferenceRoomBooking.Location != null && $scope.ConferenceRoomBooking.Location.length > 0 ? $scope.ConferenceRoomBooking.Location[0].LCM_CODE : null,
            TowerCode: $scope.ConferenceRoomBooking.Tower != null && $scope.ConferenceRoomBooking.Tower.length > 0 ? $scope.ConferenceRoomBooking.Tower[0].TWR_CODE : null,
            FloorCode: $scope.ConferenceRoomBooking.Floor != null && $scope.ConferenceRoomBooking.Floor.length > 0 ? $scope.ConferenceRoomBooking.Floor[0].FLR_CODE : null,
            Capacity: 0,
            FromDate: $scope.ConferenceRoomBooking.FromDate
        };
        ConferenceRoomBookingService.GetConferenceRooms(conferenceRoomSearch).then(function (response) {
            if (response != null) {
                $scope.SelectedConferenceRoom = {};
                $scope.SelectedSlot = {};
                $scope.ConferenceRooms = response;
                $scope.SelectedFromDate = $scope.ConferenceRoomBooking.FromDate;
                progress(0, 'Loading...', false);
            }
            else {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (error) {
            progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', error);
        });
    }

    $scope.openPopup = function (room, selectedSlot, isPopupOpened = false) {

       // if ($scope.IsEmployee) {
            var selectedDate = new Date($scope.SelectedFromDate);
            if (selectedSlot.EndHour == "00" && selectedSlot.EndMinutes == "00") {
                selectedDate.setDate(selectedDate.getDate() + 1);
            }
            selectedDate.setHours(selectedSlot.EndHour);
            selectedDate.setMinutes(selectedSlot.EndMinutes);

            var currentDate = new Date();
            if (selectedDate <= currentDate) {
                if (selectedSlot.IsBooked) {
                    showNotification('error', 8, 'bottom-right', 'Cancellation Time Completed');
                }
                else {
                    showNotification('error', 8, 'bottom-right', 'Booking Time Elapsed');
                }
                return;
            }
      //  }
        $scope.SelectedConferenceRoom = room;
        Cost = room.CONFERENCE_COST;
        $scope.BookingType = $scope.BookingOptions[0];
        $scope.ShowBehalfField = false;
        $scope.SelectedBookingSlotType = $scope.BookingSlotType[0];
        $scope.EnableEndField = false;
        $scope.SelectedConferenceRoom.InternalAttendees = "";
        $scope.SelectedConferenceRoom.ExternalAttendees = "";
        $scope.SelectedConferenceRoom.Description = "";
        $scope.SelectedConferenceRoom.BehalfUser = "";
        $scope.searchBehalfText = "";
        $scope.BookingBtnEnabled = true;
        $scope.ShowSubmitButtons = true;

        if (selectedSlot.IsBooked) {
            progress(0, 'Loading...', true);
            ConferenceRoomBookingService.GetAttendessByConferenceId(selectedSlot.BookingId).then(function (response) {
                if (response != undefined && response.length > 0) {
                    $scope.SelectedConferenceRoom.InternalAttendees = response[0].SSAD_INT_ATTND;
                    $scope.SelectedConferenceRoom.ExternalAttendees = response[0].SSAD_EXT_ATTND;
                    $scope.SelectedConferenceRoom.Description = response[0].SSA_DESC;
                    $scope.SelectedConferenceRoom.BehalfUser = response[0].SSA_EMP_MAP;
                    $scope.searchBehalfText = response[0].AUR_KNOWN_AS;
                    $scope.BookingType = (response[0].SSA_EMP_MAP == response[0].SSA_CREATED_BY) ? $scope.BookingOptions[0] : $scope.BookingOptions[1];
                    $scope.ShowBehalfField = ($scope.BookingType == 'On behalf of');
                    $scope.SelectedBookingSlotType = (response[0].DATEDIFFERENCE > 0) ? $scope.BookingSlotType[1] : $scope.BookingSlotType[0];
                    $scope.SelectedConferenceRoom.SelectedStartDate = new Date(response[0].SSA_FROM_DATE);
                    $scope.SelectedConferenceRoom.SelectedStartTime = moment().set({ 'hour': response[0].STARTHOUR, 'minute': response[0].STARTMINUTES, 'second': 0, 'millisecond': 0 }).toDate();
                    $scope.SelectedConferenceRoom.SelectedEndDate = new Date(response[0].SSA_TO_DATE);
                    $scope.SelectedConferenceRoom.SelectedEndTime = moment().set({ 'hour': response[0].ENDHOUR, 'minute': response[0].ENDMINUTES, 'second': 0, 'millisecond': 0 }).toDate();
                    if ($scope.SelectedConferenceRoom.BehalfUser == $('#lblUserId').text() || response[0].SSA_CREATED_BY == $('#lblUserId').text() || !$scope.IsEmployee) {
                        $scope.BookingBtnEnabled = false;
                    }
                    else {
                        $scope.ShowSubmitButtons = false;
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', 'Selected Slot is already booked by other user');
                        return;
                    }
                }
                progress(0, 'Loading...', false);
            });
        }
        else {
            $scope.SelectedConferenceRoom.SelectedStartDate = new Date($scope.SelectedFromDate);
            $scope.SelectedConferenceRoom.SelectedStartTime = moment().set({ 'hour': selectedSlot.StartHour, 'minute': selectedSlot.StartMinutes, 'second': 0, 'millisecond': 0 }).toDate();
            $scope.SelectedConferenceRoom.SelectedEndDate = new Date($scope.SelectedFromDate);
            $scope.SelectedConferenceRoom.SelectedEndTime = moment().set({ 'hour': selectedSlot.EndHour, 'minute': selectedSlot.EndMinutes, 'second': 0, 'millisecond': 0 }).toDate();
            var start = moment($scope.SelectedConferenceRoom.SelectedStartTime);
            var end = moment($scope.SelectedConferenceRoom.SelectedEndTime);
            var diffMinutes = end.diff(start, 'minutes');
            $scope.diffMinutes = diffMinutes;
            $scope.SelectedConferenceRoom.Cost = ($scope.diffMinutes / 60) * Cost;
        }
        //if ($scope.SelectedSlot != undefined && $scope.SelectedSlot.StartType != undefined) {
        //    $scope.SelectedSlot.StartHour = selectedSlot.StartHour;
        //    $scope.SelectedSlot.StartMinutes = selectedSlot.StartMinutes;
        //    $scope.SelectedSlot.EndHour = ($scope.SelectedSlot.EndHour < selectedSlot.EndHour) ? selectedSlot.EndHour : $scope.SelectedSlot.EndHour;
        //    $scope.SelectedSlot.EndMinutes = ($scope.SelectedSlot.EndHour <= selectedSlot.EndHour && $scope.SelectedSlot.EndMinutes > selectedSlot.EndMinutes) ? selectedSlot.EndMinutes : $scope.SelectedSlot.EndMinutes;
        //}
        //else {
        $scope.SelectedSlot = selectedSlot;
        $scope.SelectedConferenceRoom.BookingId = selectedSlot.BookingId;
        //}
        //$scope.SelectedConferenceRoom.SelectedStartDate = new Date($scope.SelectedFromDate);
        //$scope.SelectedConferenceRoom.SelectedStartTime = moment().set({ 'hour': $scope.SelectedSlot.StartHour, 'minute': $scope.SelectedSlot.StartMinutes, 'second': 0, 'millisecond': 0 }).toDate();
        //$scope.SelectedConferenceRoom.SelectedEndDate = new Date($scope.SelectedFromDate);
        //$scope.SelectedConferenceRoom.SelectedEndTime = moment().set({ 'hour': $scope.SelectedSlot.EndHour, 'minute': $scope.SelectedSlot.EndMinutes, 'second': 0, 'millisecond': 0 }).toDate();
        if (!isPopupOpened) {
            $("#myScrollableModal").modal();
            $("#myScrollableModal").show();
        }
    }

    $scope.getClass = function (selectedTimings) {
        if (selectedTimings?.StartHour == undefined) {
            return;
        }
        if (selectedTimings.IsBooked) {
            return "half_hour_child notAvailable";
        }
        if (new Date($scope.SelectedFromDate) > new Date()) {
            if (selectedTimings.EndHour == new Date($scope.SelectedConferenceRoom.SelectedEndTime).getHours()
                && selectedTimings.EndMinutes == new Date($scope.SelectedConferenceRoom.SelectedEndTime).getMinutes()) {
                return "half_hour_child selectedTime";
            }
            return "half_hour_child available";
        }
        //var startTime = moment().set({ 'hour': selectedTimings.StartHour, 'minute': selectedTimings.StartMinutes, 'second': 0, 'millisecond': 0 }).toDate();
        //var endTime = moment().set({ 'hour': selectedTimings.EndHour, 'minute': selectedTimings.EndMinutes, 'second': 0, 'millisecond': 0 }).toDate();


        var selectedDate = new Date($scope.SelectedFromDate);
        if (selectedTimings.EndHour == "00" && selectedTimings.EndMinutes == "00") {
            selectedDate.setDate(selectedDate.getDate() + 1);
        }
        selectedDate.setHours(selectedTimings.EndHour);
        selectedDate.setMinutes(selectedTimings.EndMinutes);

        var currentDate = new Date();
        if (selectedDate <= currentDate) {
            selectedTimings.BookingStatus = "Out of time";
            return "half_hour_child";
        }
        else {
            var startTime = moment().set({ 'hour': selectedTimings.StartHour, 'minute': selectedTimings.StartMinutes, 'second': 0, 'millisecond': 0 }).toDate();
            var endTime = moment().set({ 'hour': selectedTimings.EndHour, 'minute': selectedTimings.EndMinutes, 'second': 0, 'millisecond': 0 }).toDate();
            if (startTime >= new Date($scope.SelectedConferenceRoom.SelectedStartTime) && endTime <= new Date($scope.SelectedConferenceRoom.SelectedEndTime) && endTime.getHours() != "0") {
                return "half_hour_child selectedTime";
            }
            if (selectedDate.getHours() == new Date($scope.SelectedConferenceRoom.SelectedEndTime).getHours()
                && selectedDate.getMinutes() == new Date($scope.SelectedConferenceRoom.SelectedEndTime).getMinutes()) {
                return "half_hour_child selectedTime";
            }
            return "half_hour_child available";
        }


        //if (selectedTimings.StartHour < new Date().getHours() || (selectedTimings.StartHour == new Date().getHours() && selectedTimings.StartMinutes <= new Date().getMinutes())) {
        //    selectedTimings.BookingStatus = "Out of time";
        //    return "half_hour_child";
        //}
        //var startTime = moment().set({ 'hour': selectedTimings.StartHour, 'minute': selectedTimings.StartMinutes, 'second': 0, 'millisecond': 0 }).toDate();
        //var endTime = moment().set({ 'hour': selectedTimings.EndHour, 'minute': selectedTimings.EndMinutes, 'second': 0, 'millisecond': 0 }).toDate();
        //if (startTime >= new Date($scope.SelectedConferenceRoom.SelectedStartTime) && endTime <= new Date($scope.SelectedConferenceRoom.SelectedEndTime) && endTime.getHours() != "0") {
        //    return "half_hour_child selectedTime";
        //}
        //return ($scope.SelectedSlot.StartHour === selectedTimings.StartHour && $scope.SelectedSlot.StartMinutes === selectedTimings.StartMinutes) ? 'half_hour_child' : (selectedTimings.IsBooked ? 'half_hour_child notAvailable' : 'half_hour_child available');
    };

    $scope.ConferenceTimeChange = function (changedfield) {

        if (changedfield === 'startDate') {
            let ConferenceIds = [];
            ConferenceIds.push({ ConferenceId: $scope.SelectedConferenceRoom.CONFERENCE_CODE });

            var timingFilter = {
                ConferenceDate: moment($scope.SelectedConferenceRoom.SelectedStartDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                ConferenceIdsType: ConferenceIds
            };
            ConferenceRoomBookingService.GetConferenceRoomBookedTimings(timingFilter).then(function (timingsResponse) {
                progress(0, 'Loading...', true);
                if (timingsResponse != null) {
                    var objIndex = $scope.ConferenceRooms.findIndex((obj => obj.CONFERENCE_CODE == $scope.SelectedConferenceRoom.CONFERENCE_CODE));
                    if (objIndex > -1) {
                        $scope.ConferenceRooms[objIndex].BookedTimings = timingsResponse;
                    }
                    $scope.BookingBtnEnabled = true;
                }
                progress(0, 'Loading...', false);
            });
        }
        progress(0, 'Loading...', false);
    }

    $scope.ClosePopup = function () {
        $scope.SelectedConferenceRoom = {};
        $scope.SelectedSlot = {};
        $("#myScrollableModal").modal();
        $("#myScrollableModal").hide();
    }

    $scope.selectAnotherConferenceRoom = function () {
        // Implement your condition for setting selected
        $scope.SelectedConferenceRoom = {};
        $scope.SelectedSlot = {};
        $scope.SelectedConferenceRoom = $scope.ConferenceRooms.filter(x => x.CONFERENCE_CODE == $scope.SelectedConferenceInDropDown)[0];
        $scope.SelectedConferenceRoom.SelectedStartDate = new Date();
        $scope.BookingBtnEnabled = true;
        $scope.SelectedConferenceRoom.BookingId = "";
        $scope.SelectedConferenceRoom.InternalAttendees = "";
        $scope.ShowSubmitButtons = true;
        var startDate = new Date();
        var starHour = startDate.getHours();
        var startMinutes = startDate.getMinutes();
        var endHour = 0;
        var endMinutes = 0;
        if (startMinutes <= 30) {
            startMinutes = 30;
            endHour = starHour + 1;
            endMinutes = 0;
        }
        else if (startMinutes >= 30) {
            startMinutes = 00;
            starHour = starHour + 1;
            endHour = starHour;
            endMinutes = 30;
        }
        var selectedBookSlot = $scope.SelectedConferenceRoom.BookedTimings.filter(x => x.StartHour == starHour && x.StartMinutes == startMinutes && x.EndHour == endHour && x.EndMinutes == endMinutes);
        if (selectedBookSlot != undefined && selectedBookSlot.length > 0) {
            if (selectedBookSlot[0].IsBooked) {
                if (selectedBookSlot[0].CreatedBy == $('#lblUserId').text()) {
                    if (selectedSlot.StartHour < new Date().getHours() || (selectedSlot.StartHour == new Date().getHours() && selectedSlot.StartMinutes <= new Date().getMinutes())) {
                        $scope.ShowSubmitButtons = false;
                        showNotification('error', 8, 'bottom-right', 'Cancellation Time Completed');
                        return;
                    }
                    progress(0, 'Loading...', true);
                    ConferenceRoomBookingService.GetAttendessByConferenceId(selectedBookSlot[0].BookingId).then(function (response) {
                        if (response != undefined && response.length > 0) {
                            $scope.SelectedConferenceRoom.InternalAttendees = response[0].SSAD_INT_ATTND;
                            $scope.SelectedConferenceRoom.ExternalAttendees = response[0].SSAD_EXT_ATTND;
                            $scope.SelectedConferenceRoom.Description = response[0].SSA_DESC;
                            $scope.SelectedConferenceRoom.BehalfUser = response[0].SSA_EMP_MAP;
                            $scope.searchBehalfText = response[0].AUR_KNOWN_AS;
                            $scope.BookingType = (response[0].SSA_EMP_MAP == response[0].SSA_CREATED_BY) ? $scope.BookingOptions[0] : $scope.BookingOptions[1];
                            $scope.ShowBehalfField = ($scope.BookingType == 'On behalf of');
                            $scope.SelectedBookingSlotType = (response[0].DATEDIFFERENCE > 0) ? $scope.BookingSlotType[1] : $scope.BookingSlotType[0];
                            $scope.SelectedConferenceRoom.SelectedStartDate = new Date(response[0].SSA_FROM_DATE);
                            $scope.SelectedConferenceRoom.SelectedStartTime = moment().set({ 'hour': response[0].STARTHOUR, 'minute': response[0].STARTMINUTES, 'second': 0, 'millisecond': 0 }).toDate();
                            $scope.SelectedConferenceRoom.SelectedEndDate = new Date(response[0].SSA_TO_DATE);
                            $scope.SelectedConferenceRoom.SelectedEndTime = moment().set({ 'hour': response[0].ENDHOUR, 'minute': response[0].ENDMINUTES, 'second': 0, 'millisecond': 0 }).toDate();
                            if ($scope.SelectedConferenceRoom.BehalfUser == $('#lblUserId').text() || response[0].SSA_CREATED_BY == $('#lblUserId').text()) {
                                $scope.BookingBtnEnabled = false;
                            }
                            else {
                                $scope.ShowSubmitButtons = false;
                                progress(0, 'Loading...', false);
                                showNotification('error', 8, 'bottom-right', 'Selected Slot is already booked by other user');
                                return;
                            }
                        }
                        progress(0, 'Loading...', false);
                    });
                    $scope.BookingBtnEnabled = false;
                    $scope.SelectedConferenceRoom.BookingId = selectedBookSlot[0].BookingId;
                }
                else {
                    showNotification('error', 8, 'bottom-right', 'Selected Slot is already booked');
                    return;
                }
            }
            else {
                $scope.SelectedConferenceRoom.SelectedStartTime = moment().set({ 'hour': starHour, 'minute': startMinutes, 'second': 0, 'millisecond': 0 }).toDate();
                $scope.SelectedConferenceRoom.SelectedEndDate = new Date();
                $scope.SelectedConferenceRoom.SelectedEndTime = moment().set({ 'hour': endHour, 'minute': endMinutes, 'second': 0, 'millisecond': 0 }).toDate();
            }
        }
        else {
            $scope.SelectedConferenceRoom.SelectedStartTime = moment().set({ 'hour': starHour, 'minute': startMinutes, 'second': 0, 'millisecond': 0 }).toDate();
            $scope.SelectedConferenceRoom.SelectedEndDate = new Date();
            $scope.SelectedConferenceRoom.SelectedEndTime = moment().set({ 'hour': endHour, 'minute': endMinutes, 'second': 0, 'millisecond': 0 }).toDate();
        }
        $scope.BookingType = $scope.BookingOptions[0];
        $scope.ShowBehalfField = false;
        $scope.SelectedBookingSlotType = $scope.BookingSlotType[0];
        $scope.EnableEndField = false;

    };

    $scope.isConferenceSelected = function (conferenceCode) {
        //$scope.SelectedConferenceInDropDown = conferenceCode;
        return $scope.SelectedConferenceRoom.CONFERENCE_CODE === conferenceCode;
    }

    $scope.SaveBooking = function () {
        progress(0, 'Loading...', true);
        var conferenceSlotVM = {
            StartHour: new Date($scope.SelectedConferenceRoom.SelectedStartTime).getHours(),
            StartMinutes: new Date($scope.SelectedConferenceRoom.SelectedStartTime).getMinutes(),
            EndHour: new Date($scope.SelectedConferenceRoom.SelectedEndTime).getHours(),
            EndMinutes: new Date($scope.SelectedConferenceRoom.SelectedEndTime).getMinutes(),
            ConferenceId: $scope.SelectedConferenceRoom.CONFERENCE_CODE,
            FromDate: moment($scope.SelectedConferenceRoom.SelectedStartDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
            ToDate: moment($scope.SelectedConferenceRoom.SelectedEndDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ')
        };
        ConferenceRoomBookingService.IsSlotBooked(conferenceSlotVM).then(function (response) {
            if (response != undefined && response === false) {
                var bookingRequest = {
                    LocationCode: $scope.SelectedConferenceRoom.LCM_CODE,
                    TowerCode: $scope.SelectedConferenceRoom.TWR_CODE,
                    FloorCode: $scope.SelectedConferenceRoom.FLR_CODE,
                    SpaceType: $scope.SelectedConferenceRoom.CONF_NAME,
                    ConferenceCode: $scope.SelectedConferenceRoom.CONFERENCE_CODE,
                    FromDate: moment($scope.SelectedConferenceRoom.SelectedStartDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    ToDate: moment($scope.SelectedConferenceRoom.SelectedEndDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    FromTime: moment($scope.SelectedConferenceRoom.SelectedStartTime).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    EndTime: moment($scope.SelectedConferenceRoom.SelectedEndTime).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    Description: $scope.SelectedConferenceRoom.Description,
                    InternalAttendes: $scope.SelectedConferenceRoom.InternalAttendees,
                    ExternalAttendes: $scope.SelectedConferenceRoom.ExternalAttendees,
                    StatusId: 2,
                    BehalfUser: $scope.SelectedConferenceRoom.BehalfUser,
                    Cost: $scope.SelectedConferenceRoom.Cost
                };
                ConferenceRoomBookingService.SaveBookingRequest(bookingRequest).then(function (response) {
                    if (response.Status != undefined) {
                        if (response.Status == 0) {
                            progress(0, 'Loading...', false);
                            showNotification('success', 8, 'bottom-right', response.Message);
                            return;
                        }
                    }
                    if (response != undefined && response.length > 0) {
                        $scope.BookingBtnEnabled = false;
                        $scope.SelectedConferenceRoom.Description = "";
                        $scope.SelectedConferenceRoom.InternalAttendees = "";
                        $scope.SelectedConferenceRoom.ExternalAttendees = "";
                        var objIndex = $scope.ConferenceRooms.findIndex((obj => obj.CONFERENCE_CODE == bookingRequest.ConferenceCode));
                        if (objIndex > -1) {
                            $scope.ConferenceRooms[objIndex].BookedTimings = response;
                        }
                        progress(0, 'Loading...', false);
                        $scope.ClosePopup();
                        showNotification('success', 8, 'bottom-right', 'Booking Confirmed');
                    }
                    else {
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', 'Something went wrong');
                    }
                });
            }
            else {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Oops already slot booked');
            }
        });
    }

    $scope.DeleteBookingRequest = function () {
        var bookingCancelModel = {
            BookingId: $scope.SelectedConferenceRoom.BookingId,
            BookingDate: moment($scope.SelectedConferenceRoom.SelectedStartDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
            ConferenceId: $scope.SelectedConferenceRoom.CONFERENCE_CODE
        };
        ConferenceRoomBookingService.DeleteBookingRequest(bookingCancelModel).then(function (response) {
            progress(0, 'Loading...', true);
            if (response != undefined && response.length > 0) {
                $scope.BookingBtnEnabled = true;
                $scope.SelectedConferenceRoom.Description = "";
                $scope.SelectedConferenceRoom.InternalAttendees = "";
                $scope.SelectedConferenceRoom.ExternalAttendees = "";
                var objIndex = $scope.ConferenceRooms.findIndex((obj => obj.CONFERENCE_CODE == bookingCancelModel.ConferenceId));
                if (objIndex > -1) {
                    $scope.ConferenceRooms[objIndex].BookedTimings = response;
                }
                progress(0, 'Loading...', false);
                $scope.ClosePopup();
                showNotification('success', 8, 'bottom-right', 'Booking Cancelled');
            }
            else {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Something went wrong');
            }
        });
    }

    $scope.ModifyBookingRequest = function () {
        progress(0, 'Loading...', true);
        var conferenceSlotVM = {
            StartHour: new Date($scope.SelectedConferenceRoom.SelectedStartTime).getHours(),
            StartMinutes: new Date($scope.SelectedConferenceRoom.SelectedStartTime).getMinutes(),
            EndHour: new Date($scope.SelectedConferenceRoom.SelectedEndTime).getHours(),
            EndMinutes: new Date($scope.SelectedConferenceRoom.SelectedEndTime).getMinutes(),
            ConferenceId: $scope.SelectedConferenceRoom.CONFERENCE_CODE,
            FromDate: moment($scope.SelectedConferenceRoom.SelectedStartDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
            ToDate: moment($scope.SelectedConferenceRoom.SelectedEndDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
            RequestId: $scope.SelectedConferenceRoom.BookingId
        };
        ConferenceRoomBookingService.IsSlotBooked(conferenceSlotVM).then(function (response) {
            if (response != undefined && response === false) {
                var bookingRequest = {
                    RequestId: $scope.SelectedConferenceRoom.BookingId,
                    FromDate: moment($scope.SelectedConferenceRoom.SelectedStartDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    ToDate: moment($scope.SelectedConferenceRoom.SelectedEndDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    FromTime: moment($scope.SelectedConferenceRoom.SelectedStartTime).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    EndTime: moment($scope.SelectedConferenceRoom.SelectedEndTime).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    Description: $scope.SelectedConferenceRoom.Description,
                    InternalAttendes: $scope.SelectedConferenceRoom.InternalAttendees,
                    ExternalAttendes: $scope.SelectedConferenceRoom.ExternalAttendees,
                    Description: $scope.SelectedConferenceRoom.Description,
                    BehalfUser: $scope.SelectedConferenceRoom.BehalfUser,
                    SelectedDate: moment($scope.ConferenceRoomBooking.FromDate).format('YYYY-MM-DDTHH:mm:ss.SSSZ'),
                    ConferenceCode: $scope.SelectedConferenceRoom.CONFERENCE_CODE,
                };
                ConferenceRoomBookingService.ModifyBookingRequest(bookingRequest).then(function (response) {
                    if (response.Status != undefined) {
                        if (response.Status == 0) {
                            progress(0, 'Loading...', false);
                            showNotification('success', 8, 'bottom-right', response.Message);
                            return;
                        }
                    }
                    if (response != undefined && response.length > 0) {
                        $scope.BookingBtnEnabled = false;
                        $scope.SelectedConferenceRoom.Description = "";
                        $scope.SelectedConferenceRoom.InternalAttendees = "";
                        $scope.SelectedConferenceRoom.ExternalAttendees = "";
                        var objIndex = $scope.ConferenceRooms.findIndex((obj => obj.CONFERENCE_CODE == bookingRequest.ConferenceCode));
                        if (objIndex > -1) {
                            $scope.ConferenceRooms[objIndex].BookedTimings = response;
                        }
                        progress(0, 'Loading...', false);
                        $scope.ClosePopup();
                        showNotification('success', 8, 'bottom-right', 'Booking Request Modified Successfully');
                    }
                    else {
                        progress(0, 'Loading...', false);
                        showNotification('error', 8, 'bottom-right', 'Something went wrong');
                    }
                });
            }
            else {
                progress(0, 'Loading...', false);
                showNotification('error', 8, 'bottom-right', 'Oops already slot booked');
            }
        });
    }
    $scope.updateCost = function () {
        if ($scope.SelectedConferenceRoom.SelectedEndTime && $scope.SelectedConferenceRoom.SelectedStartTime) {
            var startTime = moment($scope.SelectedConferenceRoom.SelectedStartTime, 'HH:mm');
            var endTime = moment($scope.SelectedConferenceRoom.SelectedEndTime, 'HH:mm');
            var duration = moment.duration(endTime.diff(startTime));
            var durationInMinutes = duration.asMinutes();
            var costPerMinute = Cost / 60
            $scope.SelectedConferenceRoom.Cost = durationInMinutes * costPerMinute;
        }
    };
    $scope.submitOnValidation = function (methodName) {
        progress(0, 'Loading...', true);
        if ($scope.SelectedBookingSlotType === $scope.BookingSlotType[0]) {
            $scope.SelectedConferenceRoom.SelectedEndDate = $scope.SelectedConferenceRoom.SelectedStartDate;
        }
        var startDate = new Date($scope.SelectedConferenceRoom.SelectedStartDate);
        startDate.setHours(new Date($scope.SelectedConferenceRoom.SelectedStartTime).getHours());
        startDate.setMinutes(new Date($scope.SelectedConferenceRoom.SelectedStartTime).getMinutes());
        var endDate = new Date($scope.SelectedConferenceRoom.SelectedEndDate);
        endDate.setHours(new Date($scope.SelectedConferenceRoom.SelectedEndTime).getHours());
        endDate.setMinutes(new Date($scope.SelectedConferenceRoom.SelectedEndTime).getMinutes());

        if (startDate > endDate) {
            progress(0, 'Loading...', false);
            //if (changedfield === 'startDate') {
            $scope.SelectedConferenceRoom.SelectedEndDate = $scope.SelectedConferenceRoom.SelectedStartDate;
            //  return;
            //}
            showNotification('error', 4, 'bottom-right', 'End date cannot be greater than start date');
            return;
        }

        var differenceHours = Math.abs(startDate - endDate) / 3600000;
        $scope.updateCost()
        if (startDate < new Date()) {
            progress(0, 'Loading...', false);
            showNotification('error', 4, 'bottom-right', 'Booking Time Elapsed');
            return;
        }
        if ($scope.IsEmployee) {
            //Recurring Validations
            if ($scope.SelectedBookingSlotType !== $scope.BookingSlotType[0]) {
                if ((differenceHours / 24) > 4) {
                    progress(0, 'Loading...', false);
                    showNotification('error', 4, 'bottom-right', 'You can book slot only for 4 days');
                    return;
                }
                var recurringEndDate = new Date($scope.SelectedConferenceRoom.SelectedStartDate);
                recurringEndDate.setHours(new Date($scope.SelectedConferenceRoom.SelectedEndTime).getHours());
                recurringEndDate.setMinutes(new Date($scope.SelectedConferenceRoom.SelectedEndTime).getMinutes());
                differenceHours = Math.abs(startDate - recurringEndDate) / 3600000;
            }

            if (differenceHours > 4) {
                progress(0, 'Loading...', false);
                showNotification('error', 4, 'bottom-right', 'You cannot select slot more than 4 hours');
                return;
            }
        }

        if (differenceHours < 0.25) {
            progress(0, 'Loading...', false);
            showNotification('error', 4, 'bottom-right', 'You cannot book slot less than 30 minutes');
            return;
        }

        switch (methodName) {
            case "Save":
                $scope.SaveBooking();
                break;
            case "Modify":
                $scope.ModifyBookingRequest();
                break;
            case "Delete":
                if ($scope.IsEmployee) {
                    progress(0, 'Loading...', true);
                    ConferenceRoomBookingService.GetCreatedUserInfo($scope.SelectedConferenceRoom.BookingId).then(function (response) {
                        if (response != undefined && response.length > 0) {
                            if (response[0].SSA_EMP_MAP == $('#lblUserId').text() || response[0].SSA_CREATED_BY == $('#lblUserId').text()) {
                                progress(0, 'Loading...', false);
                                $scope.DeleteBookingRequest();
                            }
                            else {
                                progress(0, 'Loading...', false);
                                showNotification('error', 8, 'bottom-right', 'You dont have access to cancel the booking');
                                return;
                            }
                        }
                        else {
                            progress(0, 'Loading...', false);
                            showNotification('error', 8, 'bottom-right', 'Something went wrong');
                            return;
                        }
                    });
                }
                else {
                    $scope.DeleteBookingRequest();
                }
                break;
            case "startDate":
            case "startTime":
            case "endDate":
            case "endTime":
                $scope.ConferenceTimeChange(methodName);
                break;
        }
    }

    $scope.search = function () {
        if ($scope.searchText?.length > 1) {
            $scope.showDropdown = true;
            ConferenceRoomBookingService.GetInternalAttendess($scope.searchText).then(function (response) {
                progress(0, 'Loading...', true);
                if (response != undefined && response.length > 0) {
                    $scope.filteredSuggestions = response;
                    progress(0, 'Loading...', false);
                }
                else {
                    $scope.showDropdown = false;
                    $scope.filteredSuggestions = [];
                    progress(0, 'Loading...', false);
                }
            });
        } else {
            $scope.showDropdown = false;
            $scope.filteredSuggestions = [];
        }
    };

    $scope.selectItem = function (item) {
        $scope.showDropdown = false;
        $scope.filteredSuggestions = [];
        $scope.searchText = "";
        $scope.SelectedConferenceRoom.InternalAttendees = $scope.SelectedConferenceRoom.InternalAttendees + " " + item.AUR_EMAIL + ";";
    };

    $scope.onBookingTypeChange = function (option) {
        if (!$scope.IsEmployee) {
            $scope.BookingType = option;
            $scope.ShowBehalfField = ($scope.BookingType !== $scope.BookingOptions[0]);
            if ($scope.ShowBehalfField) {
                $scope.SelectedConferenceRoom.BehalfUser = "";
            }
        }
        else {
            $scope.BookingType = $scope.BookingOptions[0];
        }
    }

    $scope.searchBehalfUsers = function () {
        if ($scope.searchBehalfText?.length > 1) {
            $scope.showBehalfUsersDropdown = true;
            ConferenceRoomBookingService.GetConferenceBehalfUsers($scope.searchBehalfText).then(function (response) {
                progress(0, 'Loading...', true);
                if (response != undefined && response.length > 0) {
                    $scope.BehalfUsers = response;
                    progress(0, 'Loading...', false);
                }
                else {
                    $scope.showBehalfUsersDropdown = false;
                    $scope.BehalfUsers = [];
                    progress(0, 'Loading...', false);
                }
            });
        } else {
            $scope.showBehalfUsersDropdown = false;
            $scope.BehalfUsers = [];
        }
    };

    $scope.selectBehalfUser = function (item) {
        $scope.showBehalfUsersDropdown = false;
        $scope.BehalfUsers = [];
        $scope.searchBehalfText = item.AUR_KNOWN_AS;
        $scope.SelectedConferenceRoom.BehalfUser = item.AUR_ID;
    };

    $scope.onBookingSlotTypeChange = function (option) {
        $scope.EnableEndField = false;
        $scope.SelectedBookingSlotType = option;
        if ($scope.SelectedBookingSlotType === 'Recurring') {
            $scope.EnableEndField = true;
        }
        else {
            $scope.SelectedConferenceRoom.SelectedEndDate = new Date($scope.SelectedConferenceRoom.SelectedStartDate);
        }
    }

}]);

