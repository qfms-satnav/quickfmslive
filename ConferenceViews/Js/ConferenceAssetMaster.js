﻿app.service("ConferenceAssetMasterService", ['$http', '$q', function ($http, $q) {
    this.SaveAstData = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceAssetMaster/SaveAssetDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.ModifyAstData = function (response) {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceAssetMaster/ModifyAssetDetails', response)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
    this.GetAstData = function () {
        deferred = $q.defer();
        return $http.post('../../api/ConferenceAssetMaster/GetGridData')
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('ConferenceAssetMasterController', ['$scope', '$q', 'ConferenceAssetMasterService', 'UtilityService', '$filter', function ($scope, $q, ConferenceAssetMasterService, UtilityService, $filter) {
    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.AssetType = {};
    $scope.ActionStatus = 0;
    $scope.IsInEdit = false;
    $scope.Save = function () {
        progress(0, 'Loading...', true);
        if ($scope.IsInEdit) {
            ConferenceAssetMasterService.ModifyAstData($scope.AssetType).then(function (response) {
                var updatedobj = {};
                angular.copy($scope.AssetType, updatedobj)
                $scope.gridata.unshift(updatedobj);
                $scope.ShowMessage = true;
                $scope.Success = "Data Updated Successfully";

                ConferenceAssetMasterService.GetAstData().then(function (data) {
                    $scope.gridata = data;
                    $scope.gridOptions.api.setRowData(data);
                }, function (error) {
                    console.log(error);
                });

                $scope.IsInEdit = false;
                progress(0, 'Loading...', false);
                showNotification('success', 8, 'bottom-right', $scope.Success);
                $scope.EraseData();
                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        showNotification('success', 8, 'bottom-right', $scope.Success);
                    });
                }, 700);
            }, function (error) {
                console.log(error);
            })

        }
        else {
            $scope.AssetType.AST_STA_ID = "1";
            ConferenceAssetMasterService.SaveAstData($scope.AssetType).then(function (response) {
                console.log(response);
                if (response.data == null) {
                    progress(0, 'Loading...', false);
                    showNotification('error', 8, 'bottom-right', response.Message);
                }
                else {
                    progress(0, 'Loading...', false);
                    showNotification('success', 8, 'bottom-right', response.Message);
                    var savedobj = {};
                    angular.copy($scope.AssetType, savedobj)
                    $scope.gridata.unshift(savedobj);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    $scope.EraseData();
                }
            }, function (error) {
                console.log(error);
            });
        }
    }
    var columnDefs = [
               { headerName: "Asset Code", field: "AST_CODE", width: 190, cellClass: 'grid-align' },
               { headerName: "Asset Name", field: "AST_NAME", width: 280, cellClass: 'grid-align' },
               { headerName: "Status", template: "{{ShowStatus(data.AST_STA_ID)}}", width: 170, cellClass: 'grid-align' },
               { headerName: "Action", suppressMenu: true, template: '<a data-ng-click="EditData(data)"><i class="fa fa-pencil fa-fw"></i></a>', cellClass: 'grid-align', width: 110 }];


    $scope.LoadData = function () {
        ConferenceAssetMasterService.GetAstData().then(function (data) {
            $scope.gridata = data;
            $scope.gridOptions.api.setRowData(data);
        }, function (error) {
            console.log(error);
        });
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $scope.gridOptions = {
        columnDefs: columnDefs,
        enableCellSelection: false,
        rowData: null,
        enableFilter: true,
        enableSorting: true,
        angularCompileRows: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        }

    };
    $scope.LoadData();

    $scope.EditData = function (data) {
        console.log(data);
        $scope.ActionStatus = 1;
        $scope.IsInEdit = true;
        angular.copy(data, $scope.AssetType);
    }

    $scope.EraseData = function () {
        $scope.AssetType = {};
        $scope.ActionStatus = 0;
        $scope.IsInEdit = false;
        $scope.frm.$submitted = false;
    }

    $scope.ShowStatus = function (value) {
        return $scope.StaDet[value == 0 ? 1 : 0].Name;
    }
}]);