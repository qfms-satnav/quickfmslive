﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>

    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <style>
        .card-meeting {
            border: 1px solid;
            padding: 10px 5px;
            border-radius: 3px;
            background-color: white;
            margin: 10px -15px 0px -15px;
        }

        .timecard {
            padding: 0px 12px 10px 10px;
            border-radius: 5px;
            height: 92vh;
        }

        .flex_box {
            display: flex;
            margin-top: 10px;
        }

        .half_hour_child {
            background-color: #ccc;
            height: 40px;
            width: 13%;
            margin-right: 2px;
        }

        .heilightBorder {
            border: 2px solid green;
            width: 38px;
            height: 52px;
            position: absolute;
            bottom: -6px;
            left: 80px;
            border-radius: 4px;
        }

        .notAvailable {
            background-color: orange;
        }

        .available {
            background-color: green;
        }

        .selectedTime {
            background-color: blue;
        }

        .tooltip {
            width: 17%;
        }

        .modal-dialog {
            max-width: 99% !important;
        }

        .modal-content, .modal-dialog {
            margin: 0px !important;
        }

        .fieldBlock {
            border: 1px solid #ccc;
            padding: 8px;
            background: #fff;
            border-radius: 4px;
        }

        .pdLr-5 {
            padding: 0px 5px !important;
        }

        .btnDiv {
            display: flex;
            justify-content: center;
            margin-top: 10px
        }

        .btnClose {
            border-radius: 5px;
            width: 80px;
            background: #fff;
            margin-left: 15px;
            margin-top: 0px !important;
        }

            .btnClose:hover {
                background: #1e78ab;
                color: #fff;
            }

        .colorLegends {
            display: flex;
            background: #fff;
            padding: 4px;
            border: 1px solid #ccc;
            margin: 0px 5px;
        }

        .colorToolTip {
            width: 30px;
            height: 20px;
            margin-right: 10px;
        }

        .marT10 {
            margin-top: 10px;
        }
    </style>
</head>
<body data-ng-controller="ConferenceRoomBookingController" class="amantra">
    <label id="lblUserId" style="display: none"></label>
    <div class="modal" id="myScrollableModal" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" style="width: 100%; padding-top: 0px !important">
            <div class="modal-content">

                <!-- Modal Header -->

                <!-- Modal body -->
                <div class="modal-body overflow-auto" style="overflow-y: scroll; max-height: 95vh;">
                    <div class="col-md-3" style="padding-left: 0px">
                        <select ng-model="SelectedConferenceInDropDown" class="custom-select custom-select-lg mb-3" ng-change="selectAnotherConferenceRoom()">
                            <option data-ng-repeat="room in ConferenceRooms" ng-selected="isConferenceSelected(room.CONFERENCE_CODE)" ng-value="room.CONFERENCE_CODE">{{room.CONFERENCE_CODE}} - {{room.CONFERENCE_NAME}} ({{room.CONFERENCE_CAPACITY}} Seats)</option>
                        </select>
                        <br>
                        <div data-ng-repeat="slot in SelectedConferenceRoom.BookedTimings" ng-if="$index % 7 == 0" class="row" style="margin-bottom: 10px; margin-left: 0px">
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index].EndType}} {{SelectedConferenceRoom.BookedTimings[$index].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 1], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 1]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 1].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 1].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 1].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 1].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 1].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 1].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 1].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 2], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 2]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 2].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 2].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 2].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 2].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 2].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 2].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 2].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 3], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 3]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 3].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 3].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 3].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 3].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 3].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 3].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 3].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 4], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 4]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 4].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 4].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 4].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 4].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 4].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 4].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 4].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 5], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 5]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 5].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 5].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 5].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 5].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 5].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 5].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 5].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 6], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 6]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 6].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 6].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 6].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 6].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 6].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 6].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 6].BookingStatus}}"></div>
                            <%--<div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 7], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 7]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 7].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 7].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 7].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 7].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 7].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 7].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 7].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 8], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 8]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 8].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 8].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 8].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 8].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 8].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 8].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 8].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 9], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 9]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 9].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 9].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 9].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 9].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 9].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 9].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 9].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 10], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 10]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 10].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 10].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 10].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 10].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 10].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 10].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 10].BookingStatus}}"></div>
                            <div class="col-xs-4" data-ng-click="openPopup(SelectedConferenceRoom, SelectedConferenceRoom.BookedTimings[$index + 11], true)" ng-class="getClass({{SelectedConferenceRoom.BookedTimings[$index + 11]}})" data-toggle="tooltip" title="{{SelectedConferenceRoom.BookedTimings[$index + 11].StartHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 11].StartMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 11].StartType}} - {{SelectedConferenceRoom.BookedTimings[$index + 11].EndHour}}:{{SelectedConferenceRoom.BookedTimings[$index + 11].EndMinutes}} {{SelectedConferenceRoom.BookedTimings[$index + 11].EndType}} {{SelectedConferenceRoom.BookedTimings[$index + 11].BookingStatus}}"></div>--%>
                        </div>
                        <hr>
                    </div>
                    <div class="col-md-9" style="padding: 0px;">
                        <div class="timecard">
                            <div class="row" ng-show="!IsEmployee">
                                <div class="col-md-6 pdLr-5">
                                    <div class="fieldBlock" style="height: 100%; line-height: 2.5">
                                        <label ng-repeat="option in BookingOptions track by $index" style="padding-top: 10px">
                                            <input type="radio" name="occurrences" ng-value="option" ng-model="BookingType" ng-click="onBookingTypeChange(option)" />
                                            {{ option }}
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6 pdLr-5" ng-show="ShowBehalfField">
                                    <div class="fieldBlock">
                                        <label for="start" style="margin-top: 0px">Select Employee:</label>
                                        <input type="text" ng-model="searchBehalfText" ng-keyup="searchBehalfUsers()" placeholder="Type to search by employee">
                                        <div class="autocomplete-dropdown" ng-show="showBehalfUsersDropdown">
                                            <div class="autocomplete-item" ng-repeat="item in BehalfUsers" ng-click="selectBehalfUser(item)">
                                                {{ item.AUR_KNOWN_AS }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row marT10">
                                <div class="col-md-6 pdLr-5">
                                    <div class="fieldBlock">
                                        <label for="start" style="margin-top: 0px">Start:</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" name="startdate" ng-model="SelectedConferenceRoom.SelectedStartDate" ng-change="submitOnValidation('startDate')" ng-enabled="!EnableEndField">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="time" class="form-control" name="starttime" ng-model="SelectedConferenceRoom.SelectedStartTime" ng-change="submitOnValidation('startTime')">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 pdLr-5">
                                    <div class="fieldBlock">
                                        <label for="start" style="margin-top: 0px">End:</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" name="enddate" ng-model="SelectedConferenceRoom.SelectedEndDate" ng-change="submitOnValidation('endDate')" ng-enabled="!EnableEndField">
                                            </div>
                                            <div class="col-md-6">
                                                <input type="time" class="form-control" name="endtime" ng-model="SelectedConferenceRoom.SelectedEndTime" ng-change="submitOnValidation('endTime')">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row marT10">
                                <div class="col-md-4 pdLr-5">
                                    <div class="fieldBlock">
                                        <label for="start" style="margin-top: 0px">Select Internal Attendees:</label>
                                        <input type="text" ng-model="searchText" ng-keyup="search()" placeholder="Type to search">
                                        <div class="autocomplete-dropdown" ng-show="showDropdown">
                                            <div class="autocomplete-item" ng-repeat="item in filteredSuggestions" ng-click="selectItem(item)">
                                                {{ item.AUR_EMAIL }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 pdLr-5">
                                    <div class="fieldBlock" style="padding-bottom: 1px;">
                                        <label for="start" style="margin-top: 0px">Internal Attendees:</label>
                                        <textarea style="width: 100%" ng-model="SelectedConferenceRoom.InternalAttendees"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row marT10">
                                <div class="col-md-6 pdLr-5">
                                    <div class="fieldBlock">
                                        <label for="start" style="margin-top: 0px">External Attendees:</label>
                                        <textarea style="width: 100%" ng-model="SelectedConferenceRoom.ExternalAttendees"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6 pdLr-5">
                                    <div class="fieldBlock">
                                        <label for="start" style="margin-top: 0px">Description:</label>
                                        <textarea style="width: 100%" ng-model="SelectedConferenceRoom.Description"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row marT10">
                                <div class="col-md-6 pdLr-5">
                                    <div class="fieldBlock">
                                        <label ng-repeat="option in BookingSlotType track by $index">
                                            <input type="radio" name="bookingslots" ng-value="option" ng-model="SelectedBookingSlotType" ng-click="onBookingSlotTypeChange(option)" />
                                            {{ option }}
                                        </label>
                                    </div>
                                </div>
                            
                            <div class="col-md-6 pdLr-5">
                                    <div class="fieldBlock">
                                        <label for="start" style="margin-top: 0px">Cost:</label>
                                      <%--  <textarea style="width: 100%" ng-model="SelectedConferenceRoom.Cost"></textarea>--%>
                                         <input id="Cost" name="Cost" type="number" min="0" data-ng-model="SelectedConferenceRoom.Cost" class="form-control" autofocus />
                                    </div>
                                </div>
                                </div>
                            <div class="btnDiv">
                                <button class="btn btn-lg btn-block btn-secondary" style="border-radius: 3px; background-color: green; width: 200px" ng-if="ShowSubmitButtons && BookingBtnEnabled" ng-click="submitOnValidation('Save')">Confirm this booking</button>
                                <button class="btn btn-primary custom-button-color" style="border-radius: 3px; background-color: blue; width: 100px" ng-if="ShowSubmitButtons && !BookingBtnEnabled" ng-click="submitOnValidation('Modify')">Modify</button>
                                <button class="btn btn-lg btn-block btn-secondary" style="border-radius: 3px; background-color: red; width: 200px" ng-if="ShowSubmitButtons && !BookingBtnEnabled" ng-click="submitOnValidation('Delete')">Cancel this booking</button>
                                <button class="btn btn-lg btn-block btnClose" ng-click="ClosePopup()">Close</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="animsition">
        <div class="al-content">
            <div class="row" style="padding-bottom: 5px;">
                <div class="col-md-6" style="">
                    <h3 class="panel-title" style="margin-top: 5px;">Conference Room Booking</h3>
                </div>
                <div class="col-md-6">
                    <div style="display: flex; justify-content: end;">
                        <div class="colorLegends">
                            <div class="colorToolTip" style="background: orange;">
                            </div>
                            Booked
                        </div>
                        <div class="colorLegends">
                            <div class="colorToolTip" style="background: green;">
                            </div>
                            Available
                        </div>
                        <div class="colorLegends">
                            <div class="colorToolTip" style="background-color: #ccc;">
                            </div>
                            Lapsed
                        </div>
                        <div class="colorLegends">
                            <div class="colorToolTip" style="background: blue;">
                            </div>
                            Selected
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <form role="form" id="form1" name="frmConferenceRoomBooking">
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">City </label>
                                <div isteven-multi-select selection-mode="single" data-input-model="City" data-output-model="ConferenceRoomBooking.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                    data-on-item-click="CtyChanged()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1" ng-disabled="isButtonDisabled">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Location </label>
                                <div isteven-multi-select selection-mode="single" data-input-model="Location" data-output-model="ConferenceRoomBooking.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                     data-on-item-click="LcmChanged()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1" ng-disabled="isButtonDisabled">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Tower </label>
                                <div isteven-multi-select selection-mode="single" data-input-model="Tower" data-output-model="ConferenceRoomBooking.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                     data-on-item-click="TwrChanged()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1" ng-disabled="isButtonDisabled">
                                </div>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Floor </label>
                                <div isteven-multi-select selection-mode="single" data-input-model="Floor" data-output-model="ConferenceRoomBooking.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                    data-on-item-click="FlrChanged()" data-on-select-none="FlrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="margin-right: 0px;">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='fromdate'>
                                    <input type="text" class="form-control" id="FromDate" name="FromDate" placeholder="mm/dd/yyyy" data-ng-readonly="true" ng-model="ConferenceRoomBooking.FromDate" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('fromdate')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11 col-sm-6 col-xs-12">
                            <div class="box-footer text-right">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="GetConferenceRooms()" />
                            </div>
                        </div>
                    </div>
             
            <div style="margin: 0px 15px;">
                <div class="row">
                    <div class="col-md-6" data-ng-repeat="room in ConferenceRooms" style="padding-right: 20px;">
                        <div class="card-meeting">
                            <div class="row">
                                <div class="col-md-3">
                                    <img ng-if="room.CONF_NAME === 'Discussion Room'" src="../../images/ConferenceImages/ConferenceRoom.jpg" width="100%" style="height:100px !important" alt="">
                                    <img ng-if="room.CONF_NAME === 'Meeting Room'" src="../../images/ConferenceImages/MeetingRoom.jpg" width="100%" style="height:100px !important" alt="">
                                    <img ng-if="room.CONF_NAME === 'Conference Room'" src="../../images/ConferenceImages/DiscussionRoom.jpg" width="100%" style="height:100px !important" alt="">
                                    <img ng-if="room.CONF_NAME !== 'Discussion Room' && room.CONF_NAME !== 'Conference Room' && room.CONF_NAME !== 'Meeting Room'" src="../../images/ConferenceImages/DefaultRoom.jpg" width="100%" style="height:100px !important" alt="">
                                </div>
                                <div class="col-md-9">
                                    <%--<p><span><b>Room Id: </b>{{room.CONFERENCE_CODE}}</span> <span style="float: right;"><b>Location: </b>{{room.LCM_NAME}}</span></p>--%>
                                    <p><span style="font-size:1.2rem"><b>{{room.CONFERENCE_NAME}}</b></span> </p>
                                    <p><span><b>Capacity: </b>{{room.CONFERENCE_CAPACITY}} people</span></p>
                                    <p><span><b>Type: </b>{{room.CONF_NAME}}</span> 
                                     <p><span><b>Floor: </b>{{room.FLR_NAME}}</span></p>   
                                        <%--<span style="float: right;"><b>Floor: </b>{{room.FLR_NAME}}</span></p> --%>
                                   
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                     <div class="flex_box">
                                        <div data-ng-repeat="slot in room.BookedTimings" data-ng-click="openPopup(room, slot)" ng-class="getClass({{slot}})" data-toggle="tooltip" title="{{slot.StartHour}}:{{slot.StartMinutes}} {{slot.StartType}} - {{slot.EndHour}}:{{slot.EndMinutes}} {{slot.EndType}} {{slot.BookingStatus}}"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                       </form>
            </div>
        </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script type="text/javascript" defer>
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true
            });
        };
    </script>
    <script>
        var app = angular.module('QuickFMS', ["isteven-multi-select"]);
        //function openPopup() {
        //    $("#myScrollableModal").modal();
        //    $("#myScrollableModal").show();
        //}
        $(document).ready(function () {
            $('#lblUserId').text('<%= Session["UID"].ToString() %>');
            $('[data-toggle="tooltip"]').tooltip();
        });
        function closePopup() {
            document.getElementById('popup').style.display = 'none';
        }
    </script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../Js/ConferenceRoomBooking.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
</body>
</html>

