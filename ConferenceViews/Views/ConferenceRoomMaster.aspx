﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>





    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%--<style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }
        .ag-blue .ag-row {
            line-height: 15px !important;
        }



    </style>--%>
    <%--<link href="../../Scripts/aggrid/css/ag-grid.min.css" rel="stylesheet" />
    <link href="../../Scripts/aggrid/css/theme-blue.css" rel="stylesheet" />--%>
    <link href="../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
    </script>
</head>
<body data-ng-controller="ConferenceRoomMasterController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Reservation Type Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                <h3 class="panel-title">Reservation Room Master</h3>
            </div>
            <div class="card">
                <%--  <div class="card-body" style="padding-right: 10px;">--%>
                <form role="form" id="form1" name="frm" data-valid-submit="SaveData()" novalidate>
                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                        </div>
                    </div>
                    <br />
                    <br />
                    <div class="clearfix row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_TYPE.$invalid}">
                                <label>Reservation Type<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="ConfType" data-output-model="ConferenceRoom.CONF_NAME" data-button-label="icon CONF_NAME"
                                    data-item-label="icon CONF_NAME maker" data-tick-property="ticked" data-max-labels="1" selection-mode="single">
                                </div>
                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_TYPE.$invalid" style="color: red">Please select reservation type</span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_ROOM_CODE.$invalid}">
                                <label>Reservation Code<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                    onmouseout="UnTip()">
                                    <input id="txtCName" name="CONFERENCE_ROOM_CODE" type="text" data-ng-model="ConferenceRoom.CONFERENCE_ROOM_CODE" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_ROOM_CODE.$invalid" style="color: red">Please enter valid reservation code</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_ROOM_NAME.$invalid}">
                                <label>Reservation Name<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter Name in alphabets,numbers and  (space,-,_ ,(,),\,/ allowed) and upto 50 characters allowed')"
                                    onmouseout="UnTip()">
                                    <input id="Text1" name="CONFERENCE_ROOM_NAME" type="text" data-ng-model="ConferenceRoom.CONFERENCE_ROOM_NAME" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_ROOM_NAME.$invalid" style="color: red">Please enter valid reservation name</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_CAPACITY.$invalid}">
                                <label>Reservation Capacity<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter numbers')"
                                    onmouseout="UnTip()">
                                    <input id="Text2" name="CONFERENCE_CAPACITY" type="number" min="0" data-ng-model="ConferenceRoom.CONFERENCE_CAPACITY" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_CAPACITY.$invalid" style="color: red">Please enter valid reservation capacity</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_CITY.$invalid}">
                                <label>City<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="ConferenceRoom.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="ConferenceRoom.City" name="CONFERENCE_CITY" style="display: none" required="" />
                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_CITY.$invalid" style="color: red">Please select city</span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_LOCATION.$invalid}">
                                <label>Location<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Location" data-output-model="ConferenceRoom.Location" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="ConferenceRoom.Location" name="CONFERENCE_LOCATION" style="display: none" required="" />
                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_LOCATION.$invalid" style="color: red">Please select location </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_TOWER.$invalid}">
                                <label>Tower<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Tower" data-output-model="ConferenceRoom.Tower" data-button-label="icon TWR_NAME "
                                    data-item-label="icon TWR_NAME maker" data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="ConferenceRoom.Tower" name="CONFERENCE_TOWER" style="display: none" required="" />
                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_TOWER.$invalid" style="color: red">Please select tower </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_FLOOR.$invalid}">
                                <label>Floor<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Floor" data-output-model="ConferenceRoom.Floor" data-button-label="icon FLR_NAME"
                                    data-item-label="icon FLR_NAME maker" data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()"
                                    data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="ConferenceRoom.Floor" name="CONFERENCE_FLOOR" style="display: none" required="" />
                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_FLOOR.$invalid" style="color: red">Please select floor </span>
                            </div>
                        </div>
                    </div>


                    <div class="clearfix row">
                          <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_COST.$invalid}">
                                <label>Per Hour Cost<span style="color: red;">*</span></label>
                                <div onmouseover="Tip('Enter numbers')"
                                    onmouseout="UnTip()">
                                    <input id="CONFERENCE_COST" name="CONFERENCE_COST" type="number" min="0" data-ng-model="ConferenceRoom.CONFERENCE_COST" data-ng-pattern="namepattern" maxlength="50" class="form-control" autofocus required="" />
                                    <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_COST.$invalid" style="color: red">Please Enter Valid Reservation cost</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-show="ActionStatus==1" data-ng-class="{'has-error': frm.$submitted && frm.CONFERENCE_STATUS.$invalid}">
                                <label>Status<span style="color: red;">*</span></label>
                                <select id="ddlsta" name="Status" data-ng-model="ConferenceRoom.CONFERENCE_STATUS" class="form-control">
                                    <option value="">--Select--</option>
                                    <option data-ng-repeat="sta in StaDet" value="{{sta.Id}}">{{sta.Name}}</option>
                                </select>
                                <span class="error" data-ng-show="frm.$submitted && frm.CONFERENCE_STATUS.$invalid" data-ng-disabled="ActionStatus==0" style="color: red">Please select status</span>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />
                                <input type="submit" value="Modify" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==1" />
                                <input type="button" class="btn btn-primary custom-button-color" data-ng-click="Clear()" value="Clear" />
                                <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12" id="table2">
                            <br />
                            <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <input id="filtertxt" placeholder="Filter..." type="text" class="form-control" style="width: 25%;" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 300px; width: 100%"></div>
                            </div>
                        </div>
                    <%--</div>--%>
                </form>
            </div>
        </div>
    </div>
    <%-- </div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../Scripts/Lodash/lodash.min.js" defer></script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <%-- <script src="../../SMViews/Utility.min.js" defer></script>--%>
    <script src="../Js/ConferenceRoomMaster.js"></script>
    <%--<script src="../Js/ConferenceRommMaster.min.js" defer></script>--%>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js"></script>
</body>
</html>
