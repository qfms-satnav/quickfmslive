﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
      /*  .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }*/

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        /*.ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }*/
    </style>

    <script type="text/javascript" defer>
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body data-ng-controller="ConferenceUserUtilizationReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <%--<div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">--%>
                            <h3 class="panel-title">User Reservation Utilization Report</h3>
                        </div>
                         <div class="card"> 
                       <%-- <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <span style="color: red;">*</span>  Mandatory field &nbsp; &nbsp;   <span style="color: red;">**</span>  Select to auto fill the data
                                </div>
                            </div>--%>
                            <br />
                            <form id="ConferenceUtilizationReport" name="frmConferenceUserUtilizationReport"  novalidate>
                                <div class="row">
                                    

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.CNY_NAME.$invalid}">
                                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                                <div isteven-multi-select data-input-model="Country" data-output-model="ConferenceUtilizationReport.Country" data-button-label="icon CNY_NAME" data-item-label="icon CNY_NAME"
                                                    data-on-item-click="CnyChanged()" data-on-select-all="CnyChangeAll()" data-on-select-none="CnySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ConferenceUtilizationReport.Country" name="CNY_NAME" style="display: none" required="" />
                                                <span class="error" style="color: red;" style="color: red;" data-ng-show="frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.CNY_NAME.$invalid">Please select country </span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.CTY_NAME.$invalid}">
                                                <label class="control-label">City <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="City" data-output-model="ConferenceUtilizationReport.City" data-button-label="icon CTY_NAME" data-item-label="icon CTY_NAME"
                                                    data-on-item-click="CtyChanged()" data-on-select-all="CtyChangeAll()" data-on-select-none="CtySelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ConferenceUtilizationReport.City" name="CTY_NAME" style="display: none" required="" />
                                                <span class="error" style="color: red;" data-ng-show="frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.CTY_NAME.$invalid">Please select city </span>

                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.LCM_NAME.$invalid}">
                                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Location" data-output-model="ConferenceUtilizationReport.Location" data-button-label="icon LCM_NAME" data-item-label="icon LCM_NAME"
                                                    data-on-item-click="LcmChanged()" data-on-select-all="LcmChangeAll()" data-on-select-none="LcmSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ConferenceUtilizationReport.Location" name="LCM_NAME" style="display: none" required="" />
                                                <span class="error" style="color: red;" data-ng-show="frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.LCM_NAME.$invalid">Please select location </span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.TWR_NAME.$invalid}">
                                                <label class="control-label">Tower <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Tower" data-output-model="ConferenceUtilizationReport.Tower" data-button-label="icon TWR_NAME" data-item-label="icon TWR_NAME"
                                                    data-on-item-click="TwrChanged()" data-on-select-all="TwrChangeAll()" data-on-select-none="TwrSelectNone()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ConferenceUtilizationReport.Tower" name="TWR_NAME" style="display: none" required="" />
                                                <span class="error" style="color: red;" data-ng-show="frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.TWR_NAME.$invalid">Please select tower </span>
                                            </div>
                                        </div>
                                    
                              

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.FLR_NAME.$invalid}">
                                                <label class="control-label">Floor <span style="color: red;">**</span></label>
                                                <div isteven-multi-select data-input-model="Floor" data-output-model="ConferenceUtilizationReport.Floor" data-button-label="icon FLR_NAME" data-item-label="icon FLR_NAME"
                                                    data-on-select-all="FlrChangeAll()" data-on-select-none="FlrSelectNone()" data-on-item-click="FlrChanged()" data-tick-property="ticked" data-max-labels="1">
                                                </div>
                                                <input type="text" data-ng-model="ConferenceUtilizationReport.Floor" name="FLR_NAME" style="display: none" required="" />
                                                <span class="error" style="color: red;" data-ng-show="frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.FLR_NAME.$invalid">Please select floor </span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.CNF_FROM_DATE.$invalid}">
                                                <label class="control-label">From Date <span style="color: red;">*</span></label>
                                                <div class="input-group date" id='fromdate'>
                                                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="Text1" name="CNF_FROM_DATE" ng-model="ConferenceUtilizationReport.CNF_FROM_DATE" required />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                    </span>
                                                </div>
                                                <span class="error" style="color: red;" data-ng-show="frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.CNF_FROM_DATE.$invalid" style="color: red"></span>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group" data-ng-class="{'has-error': frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.CNF_TO_DATE.$invalid}">
                                                <label class="control-label">To Date <span style="color: red;">*</span></label>
                                                <div class="input-group date" id='todate'>
                                                    <input type="text" id="Text2" class="form-control" required="" placeholder="mm/dd/yyyy" name="CNF_TO_DATE" ng-model="ConferenceUtilizationReport.CNF_TO_DATE" />
                                                    <span class="input-group-addon">
                                                        <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                    </span>
                                                </div>
                                                <span class="error" style="color: red;" data-ng-show="frmConferenceUserUtilizationReport.$submitted && frmConferenceUserUtilizationReport.CNF_TO_DATE.$invalid" style="color: red"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                            <div class="form-group">
                                                <label class="control-label">Company</label>
                                                <div isteven-multi-select data-input-model="Company" data-output-model="ConferenceUtilizationReport.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                                </div>
                                                <input type="text" data-ng-model="ConferenceUtilizationReport.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                                <span class="error" style="color: red;" data-ng-show="ConferenceUtilizationReport.$submitted && ConferenceUtilizationReport.Company.$invalid" style="color: red">Please select company </span>
                                            </div>

                                        </div>
                                        <br />
                                        <br />
                                        <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                            <input type="submit" value="Search" data-ng-click="LoadData(0)" class="btn btn-primary custom-button-color" />
                                        </div>
                                   
                                </div>

                                <div class="clearfix" data-ng-show="CompanyVisible==0">
                                </div>
                                <div class="row" style="padding-left: 18px">
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <%--   <label>View In : </label>
                                        <input id="viewswitch" type="checkbox" checked data-size="small" data-on-text="<span class='fa fa-table'></span>"
                                            data-off-text="<span class='fa fa-bar-chart'></span>" />--%>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" id="table2">
                                        <a data-ng-click="GenReport(AreaReport,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(AreaReport,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(AreaReport,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                    </div>
                                </div>
                                <br />
                                <div id="Tabular">
                                    <div class="row">
                                        <%--<input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />--%>
                                        <div class="input-group" style="width: 20%">
                                             <div class="col-md-12 col-sm-6">
                                            <div class="input-group-btn">
                                                <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                                <button class="btn btn-primary custom-button-color" data-ng-click="LoadData(1)" type="submit">
                                                    <i class="glyphicon glyphicon-search"></i>
                                                </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-6 col-xs-12">
                                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px;"></div>
                                    </div>
                                        </div>
                                </div>
                                <div id="Graphicaldiv">
                                    <div id="SpcGraph">&nbsp</div>
                                    <div id="TotalGraph">&nbsp</div>
                                </div>
                            </form>
                        </div>
                    </div>
               <%-- </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../../Dashboard/C3/d3.v3.min.js" defer></script>
    <script src="../../../Dashboard/C3/c3.min.js" defer></script>
    <link href="../../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../../Scripts/jspdf.min.js" defer></script>
    <script src="../../../Scripts/jspdf.plugin.autotable.src.js" defer></script>
    <script src="../../../Scripts/Lodash/lodash.min.js" defer></script>
    <script src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js" defer></script>
    <script src="../../../Scripts/moment.min.js" defer></script>
    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>

    <script src="../JS/ConferenceUserUtilizationReport.js" defer></script>
    <%--<script src="../JS/ConferenceUserUtilizationReport.js"></script>--%>
 
    <script src="../../../SMViews/Utility.min.js" defer></script>

    <script type="text/javascript" defer>

        function setDateVals() {

            $('#Text1').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#Text2').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

            $('#Text1').datepicker('setDate', new Date(moment().subtract(29, 'days').format('MM/DD/YYYY')));
            $('#Text2').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

            //$('#Text1').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));
            //$('#Text2').datepicker('setDate', new Date(moment().format('MM/DD/YYYY')));

            //Timepicker
            //$(".timepicker").timepicker({
            //    showInputs: false,
            //    showMeridian: false
            //});
            //$scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
            //$scope.Customized.ToDate = moment().format('MM/DD/YYYY');
        }
    </script>

    <script type="text/javascript" defer>
        $(document).ready(function () {
            setDateVals();
        });
    </script>

</body>
</html>
