﻿app.service("ReservationRoomBookingsService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (ReservationRoomBookings) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/ReservationRoomBookings/GetReservationRoomBookings', ReservationRoomBookings)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('ReservationRoomBookings', ['$scope', '$q', '$http', 'ReservationRoomBookingsService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, ReservationRoomBookingsService, UtilityService, $timeout, $filter) {
    $scope.ReservationRoomBookings = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.countrylist = [];
    $scope.Citylst = [];
    $scope.Locationlst = [];
    $scope.Towerlist = [];
    $scope.Floorlist = [];
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.BsmDet = { Parent: "", Child: "" };
    $scope.Company = [];
    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.ReservationRoomBookings.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });
    }, 100);

    $scope.Pageload = function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                $scope.ReservationRoomBookings.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; }
                else { $scope.EnableStatus = 0; }
            }

        });

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getLocations(2).then(function (response) {
            if (response.data != null) {
                $scope.Location = response.data;
                angular.forEach($scope.Location, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getTowers(2).then(function (response) {
            if (response.data != null) {
                $scope.Tower = response.data;
                angular.forEach($scope.Tower, function (value, key) {
                    value.ticked = true;
                })
            }
        });

        UtilityService.getFloors(2).then(function (response) {
            if (response.data != null) {
                $scope.Floor = response.data;
                angular.forEach($scope.Floor, function (value, key) {
                    value.ticked = true;
                })
            }
        });
    }

    $scope.cnySelectAll = function () {
        $scope.ReservationRoomBookings.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.CnySelectNone = function () {
        $scope.ReservationRoomBookings.Country = [];
        $scope.getCitiesbyCny();
    }


    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.ReservationRoomBookings.Country, 1).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.ctySelectAll = function () {
        $scope.ReservationRoomBookings.City = $scope.City;
        $scope.getLocationsByCity();
    }
    $scope.CtySelectNone = function () {
        $scope.ReservationRoomBookings.City = [];
        $scope.getLocationsByCity();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.ReservationRoomBookings.City, 1).then(function (response) {
            $scope.Location = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ReservationRoomBookings.Country[0] = cny;
            }
        });
    }

    $scope.locSelectAll = function () {
        $scope.ReservationRoomBookings.Location = $scope.Location;
        $scope.getTowerByLocation();
    }
    $scope.LcmSelectNone = function () {
        $scope.ReservationRoomBookings.Location = [];
        $scope.getTowerByLocation();
    }

    $scope.getTowerByLocation = function () {
        UtilityService.getTowerByLocation($scope.ReservationRoomBookings.Location, 2).then(function (response) {
            $scope.Tower = response.data;
        }, function (error) {
            console.log(error);
        });


        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ReservationRoomBookings.Country[0] = cny;
            }
        });

        angular.forEach($scope.Location, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ReservationRoomBookings.City[0] = cty;
            }
        });
    }

    $scope.twrSelectAll = function () {
        $scope.ReservationRoomBookings.Tower = $scope.Tower;
        $scope.getFloorByTower();
    }
    $scope.TwrSelectNone = function () {
        $scope.ReservationRoomBookings.Tower = [];
        $scope.getFloorByTower();
    }

    $scope.getFloorByTower = function () {
        UtilityService.getFloorByTower($scope.ReservationRoomBookings.Tower, 2).then(function (response) {
            if (response.data != null)
                $scope.Floor = response.data;
            else
                $scope.Floor = [];
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ReservationRoomBookings.Country[0] = cny;
            }
        });

        angular.forEach($scope.Tower, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ReservationRoomBookings.City[0] = cty;
            }
        });

        angular.forEach($scope.Tower, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ReservationRoomBookings.Location[0] = lcm;
            }
        });
    }

    $scope.floorChangeAll = function () {
        $scope.ReservationRoomBookings.Floor = $scope.Floor;
        $scope.FloorChange();
    }
    $scope.FloorSelectNone = function () {
        $scope.Floorlist = [];
        $scope.FloorChange();
    }

    $scope.FloorChange = function () {

        $scope.ReservationRoomBookings.Country = [];
        $scope.ReservationRoomBookings.City = [];
        $scope.ReservationRoomBookings.Location = [];
        $scope.ReservationRoomBookings.Tower = [];

        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Tower, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.Location, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.ReservationRoomBookings.Country.push(cny);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.ReservationRoomBookings.City.push(cty);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var lcm = _.find($scope.Location, { LCM_CODE: value.LCM_CODE });
            if (lcm != undefined && value.ticked == true) {
                lcm.ticked = true;
                $scope.ReservationRoomBookings.Location.push(lcm);
            }
        });

        angular.forEach($scope.Floor, function (value, key) {
            var twr = _.find($scope.Tower, { TWR_CODE: value.TWR_CODE });
            if (twr != undefined && value.ticked == true) {
                twr.ticked = true;
                $scope.ReservationRoomBookings.Tower.push(twr);
            }
        });

    }

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        console.log($scope.ReservationRoomBookings.Floor);
                var params = {
                   
                    flrlst: $scope.ReservationRoomBookings.Floor,                    
                    FromDate: $scope.ReservationRoomBookings.FromDate
                   
                };


        ReservationRoomBookingsService.GetGriddata(params).then(function (response) {
            console.log(response);
            ExportColumns = response.exportCols;
            $scope.gridOptions.api.setColumnDefs(response.data.Coldef);
            $scope.gridata = response.data.griddata;

            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
                progress(0, 'Loading...', false);
            }

        })

    }, function (error) {
        console.log(error);
        progress(0, '', false);
    }
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            TotalHours: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.TotalHours += parseFloat((data.TotalHours).toFixed(2));
        });
        return sums;
    }
    var OVERALL_COUNT;
    
    $scope.CustmPageLoad = function () {


    }, function (error) {
        console.log(error);
    }
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns; //[{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" }, { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" }, { title: "Status", key: "STATUS" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("Reservation Romm Bookings Report.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Reservation Romm Bookings Report.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.ReservationRoomBookings.Request_Type = "All";
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.selVal = "30";
    //$scope.ReservationRoomBookings.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
    //$scope.ReservationRoomBookings.ToDate = moment().format('MM/DD/YYYY');
    $scope.rptDateRanges = function () {
        console.log($scope.selVal);
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.ReservationRoomBookings.FromDate = moment().format('MM/DD/YYYY');
                $scope.ReservationRoomBookings.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.ReservationRoomBookings.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.ReservationRoomBookings.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.ReservationRoomBookings.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.ReservationRoomBookings.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.ReservationRoomBookings.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.ReservationRoomBookings.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.ReservationRoomBookings.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.ReservationRoomBookings.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.ReservationRoomBookings.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.ReservationRoomBookings.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;

        }
    }


    $scope.GenerateFilterExcel1 = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "MultipleSeating.csv"
        };
        $scope.Options.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = ExportColumns; //[{ title: "Requisition Id", key: "REQ_ID" }, { title: "Requisition Date", key: "REQ_DATE" }, { title: "From Date", key: "FROM_DATE" }, { title: "To Date", key: "TO_DATE" }, { title: "Country", key: "CNY_NAME" }, { title: "City", key: "CTY_NAME" }, { title: "Location", key: "LCM_NAME" }, { title: "Tower", key: "TWR_NAME" }, { title: "Floor", key: "FLR_NAME" }, { title: "Vertical", key: "VER_NAME" }, { title: "Status", key: "STATUS" }, { title: "Request Type", key: "REQUEST_TYPE" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("ReservationRoomBookingsReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

 
  



    $scope.GenReport = function (ReservationRoomBookings, Type) {
        progress(0, 'Loading...', true);
        $scope.ReservationRoomBookings.Type = Type;
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterExcel();
                }
            }
        }
        else {
            if (Type == 'xls') {
                //if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                //    JSONToCSVConvertor($scope.gridata, "Customized Report", true, "CustomizedReport");
                //}
                //else {
                //    JSONToCSVConvertor($scope.gridata, "Customized Report", true, "CustomizedReport");
                //}
                $scope.GenerateFilterExcel();
            }
            else if (Type == 'pdf') {
                if ((navigator.userAgent.indexOf("MSIE") != -1) || (!!document.documentMode == true)) {
                    $scope.GenerateFilterPdf();
                }
                else {
                    $scope.GenerateFilterPdf();
                }
            }
        };
        progress(0, '', false);
    }
   
    $scope.Pageload();
    setTimeout(function () {
        $scope.LoadData(1, 'All');
    }, 2000);
    //setTimeout(function () {
    //    $scope.Pageload();
    //}, 500);

}]);
