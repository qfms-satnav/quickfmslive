﻿app.service('DispatchService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }
    this.GetParcelReqid = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/EditParcel/GetParcelReqid'));
    };
    this.BindCourierCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierCategory'));
    };
    //this.GetGriddata = function () {
    //    return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/GetDispatcherGridData'));

    //};
    this.GetGriddata = function (recipientType) {
        // Pass recipientType to the API endpoint
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/GetDispatcherGridData', {
            params: { recipientType: recipientType }  // Adding recipientType as a query parameter
        }));
    };

    this.GetParcelDetails = function (ReqId) {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/GetParcelDetails?ReqId=' + ReqId));
    };
    this.BindCourierMode = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierMode'));
    };
    this.BindCourierDetails = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierDetails'));
    };
    this.BindCourierAgency = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierAgency'));
    };
 
    this.UpdateData = function (data) {
        return handleAPIRequest($http.post(UtilityService.path + '/api/BookParcel/UpdateDispatchData', data));
    }
 
    
  

}]);
app.controller('DispatchController', ['$scope', 'DispatchService', '$http', 'UtilityService', '$filter', function ($scope, DispatchService, $http, UtilityService, $filter) {
    $scope.BOOKPARCEL = {};
    $scope.ReqDetails = false;
    $scope.CourierAgencyList = [];
    $scope.CourierModeList = [];
    $scope.CourierDetailsList = [];
    $scope.CourieCategoryList = [];

    var columnDefs = [
        { headerName: "Booking ID",field: "DFDC_ACT_NAME",width: 200,cellRenderer: function (params) {return `<a ng-click="onBookingIdClick('${params.value}')" style="cursor: pointer;">${params.value}</a>`;},cellClass: 'grid-align'},
        { headerName: "Sender Name", width: 150,field: "SENDER_DEPT",cellClass: 'grid-align'},
        { headerName: "Sender Location",field: "SEND_LOC", width: 150,cellClass: 'grid-align'},
        { headerName: "Recipient Name", field: "DES_DEP",width: 150,cellClass: 'grid-align'},
        { headerName: "Recipient Location", field: "DES_LOC", width: 150,cellClass: 'grid-align' },
        { headerName: "Courier Status",field: "STA_TITLE", width: 150,cellClass: 'grid-align'},

    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        angularCompileRows: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };
    //DispatchService.GetGriddata().then(function (gddata) {
    //    $scope.gridata = gddata;
    //    $scope.gridOptions.api.setRowData(gddata);
    //}, function (error) {
    //    console.log(error);
    //});
    //$scope.BindGrid_Project = function () {
    //    debugger;
    //    DispatchService.GetGriddata().then(function (gddata) {
    //        $scope.gridata = gddata;  
    //        if ($scope.gridOptions && $scope.gridOptions.api) {
    //            $scope.gridOptions.api.setRowData(gddata);
    //        }
    //    }, function (error) {
    //        console.error('Error fetching grid data:', error);
    //    });
    //};
    $scope.BindGrid_Project = function () {
        debugger;
        var recipientType = $scope.recipientType;

        DispatchService.GetGriddata(recipientType).then(function (gddata) {
            $scope.gridata = gddata;

            if ($scope.gridOptions && $scope.gridOptions.api) {
                $scope.gridOptions.api.setRowData(gddata);
            }
        }, function (error) {
            console.error('Error fetching grid data:', error);
        });
    };


    DispatchService.BindCourierMode().then(function (cdata) {
        $scope.CourierModeList = cdata;
    }, function (error) {
    });
    DispatchService.BindCourierCategory().then(function (xdata) {
        $scope.CourieCategoryList = xdata;
    }, function (error) {
    });
    DispatchService.BindCourierDetails().then(function (cdata) {
        $scope.CourierDetailsList = cdata;
    }, function (error) {
    });
    DispatchService.BindCourierAgency().then(function (bdata) {
        $scope.CourierAgencyList = bdata;
    }, function (error) {
    });
   

    $scope.onBookingIdClick = function (bookingId) {
        /*           alert("You clicked Booking ID: " + bookingId);*/
        $scope.ReqDetails = true;
        $scope.showBookingDate = true;
        DispatchService.GetParcelDetails(bookingId).then(function (data) {
            if (data[0]) {
                const parcel = data[0];
                $scope.BOOKPARCEL.todate = new Date(data[0].todate).toLocaleDateString('en-GB');


                angular.forEach($scope.LocationList, function (location) {
                    if (location.LCM_CODE === data[0].senderLocation) {
                        location.ticked = true;
                        angular.forEach($scope.UserList, function (Sender) {
                            if (Sender.AUR_ID === data[0].SenderId) {
                                Sender.ticked = true;
                            }
                        });
                    }
                });
                $scope.BOOKPARCEL.SENDERDEPT = data[0].SenderDepartment;
                $scope.BOOKPARCEL.SENDERCONTACT = data[0].SenderContactno;
                $scope.BOOKPARCEL.SENDEREMAIL = data[0].SENDEREMAIL;
                $scope.BOOKPARCEL.SENDERCITY = data[0].SENDERCITY;
                $scope.BOOKPARCEL.SENDERPINCODE = data[0].SENDERPINCODE;
                $scope.BOOKPARCEL.SENDERCOUNTRY = data[0].SENDERCOUNTRY;
                $scope.BOOKPARCEL.AUR_KNOWN_AS = data[0].SenderName;
                $scope.BOOKPARCEL.LCM_NAME = data[0].LCM_NAME;


                angular.forEach($scope.DestinationLocationList, function (location) {
                    if (location.LCM_CODE === data[0].RecipientLocation) {
                        location.ticked = true;
                        angular.forEach($scope.DestinationUserList, function (Recipient) {
                            if (Recipient.AUR_ID === data[0].RecipientId) {
                                Recipient.ticked = true;
                            }
                        });
                    }
                });
                $scope.BOOKPARCEL.DestinationDEPT = data[0].RecipientDepartment;
                $scope.BOOKPARCEL.DestinationCONTACT = data[0].RecipientContactno;
                $scope.BOOKPARCEL.DestinationEMAIL = data[0].RecipientEmail;
                $scope.BOOKPARCEL.DestinationCITY = data[0].RecipientCity;
                $scope.BOOKPARCEL.DestinationPINCODE = data[0].RecipientPincode;
                $scope.BOOKPARCEL.DestinationCOUNTRY = data[0].RecipientCountry;
                $scope.BOOKPARCEL.DESTINATION_LCM_NAME = data[0].DESTINATION_LCM_NAME;
                $scope.BOOKPARCEL.AUR_KNOWN_AS_DEST = data[0].RecipientName;
                
            
            
                $scope.BOOKPARCEL.BRNCODE = bookingId;


                angular.forEach($scope.CourierAgencyList, function (Agency) {
                    if (Agency.COURIER_CODE === data[0].CourierAgency) {
                        Agency.ticked = true;
                    }
                });
                $scope.BOOKPARCEL.AWBPODNO = data[0].AWBPODNO;


                angular.forEach($scope.CourierModeList, function (Mode) {
                    if (Mode.COURIERMODE_CODE === data[0].Mode) {
                        Mode.ticked = true;
                    }
                });


                angular.forEach($scope.CourieCategoryList, function (Category) {
                    if (Category.PARCEL_CODE === data[0].ParcelCategory) {
                        Category.ticked = true;
                    }
                });


                angular.forEach($scope.CourierDetailsList, function (ParcelDet) {
                    if (ParcelDet.COURIERDETAILS_CODE === data[0].ParcelDetails) {
                        ParcelDet.ticked = true;
                    }
                });
                $scope.BOOKPARCEL.INVOICENO = data[0].INVOICENO;
                $scope.BOOKPARCEL.InvoiceAMOUNT = data[0].InvoiceAMOUNT;
                $scope.BOOKPARCEL.WEIGHT = data[0].Weight;
                $scope.BOOKPARCEL.NOOFPIECES = data[0].NOOFPIECES;
                $scope.BOOKPARCEL.DNREMARKS = data[0].DeliveryRemarks;
            }
        }, function (error) {
            console.error('Error fetching parcel details:', error);
        });

        $scope.ReqDetails = true;
    };

    $scope.Save = function () {
        var obj =
        {
            BRNCODE: $scope.BOOKPARCEL.BRNCODE,
            AWBPODNO: $scope.BOOKPARCEL.AWBPODNO,
            Mode: $scope.CourierModeList.COURIERMODE_NAME[0].COURIERMODE_CODE,
            ParcelCategory: $scope.CourieCategoryList.PARCEL_NAME[0].PARCEL_CODE,
            ParcelDetails: $scope.CourierDetailsList.COURIERDETAILS_NAME[0].COURIERDETAILS_CODE,
            Weight: $scope.BOOKPARCEL.WEIGHT,
            NOOFPIECES: $scope.BOOKPARCEL.NOOFPIECES,
            DeliveryRemarks: $scope.BOOKPARCEL.DNREMARKS,
            CourierAgency: $scope.CourierAgencyList.COURIER_NAME[0].COURIER_CODE,
        };
        DispatchService.UpdateData(obj).then(function (response) {


            if (response.Message != null) {
                $scope.ShowMessage = true;
                $scope.Success = "Dispatched Sucessfully";
                var savedobj = {};
                angular.copy(obj, savedobj)
                showNotification('success', 8, 'bottom-right', response.Message);
                //$scope.ClearData();
                $scope.EraseData();

                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        //window.location.reload();
                    });
                }, 500);

            }
            else {
                showNotification('error', 8, 'bottom-right', 'Something went wrong. Please try again later.');
            }

        }, function (error) {
            showNotification('error', 8, 'bottom-right', error);


        });
    }
    $scope.EraseData = function () {
        debugger;
        $scope.CourierAgencyList = [];
        $scope.BOOKPARCEL = {};
        $scope.CourierModeList = [];
        $scope.CourierDetailsList = [];

      
    };


}]);