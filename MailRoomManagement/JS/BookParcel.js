﻿app.service('BookParcelService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }
    this.GetRefNum = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/Getrefnum'));
    };
   
    this.BindLocation = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindLocation'));
    };
    this.BindUser = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindUser'));
    };
    this.BindCourierAgency = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierAgency'));
    };
    this.BindCourierMode = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierMode'));
    };
    this.BindCourierCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierCategory'));
    };
    this.BindCourierDetails = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierDetails'));
    };
    this.SaveData = function (data) {
        return handleAPIRequest($http.post(UtilityService.path + '/api/BookParcel/SaveData', data));
    };
}]);

app.controller('BookParcelController', ['$scope', 'BookParcelService', '$http', 'UtilityService', '$filter', function ($scope, BookParcelService, $http, UtilityService, $filter) {

    $scope.BOOKPARCEL = {};
    $scope.LocationList = [];
    $scope.UserList = [];
    $scope.DestinationLocationList = [];
    $scope.DestinationUserList = [];
    $scope.CourierAgencyList = [];
    $scope.CourierModeList = [];
    $scope.CourierDetailsList = [];
    $scope.CourieCategoryList = [];

    $scope.LoadData = function () {

        BookParcelService.GetRefNum().then(function (data8) {
            $scope.BOOKPARCEL.BRNCODE = data8;
        }, function (error) {
        });
        BookParcelService.BindLocation().then(function (Udata) {
            $scope.LocationList = Udata;
        }, function (error) {
        });
        BookParcelService.BindUser().then(function (Vdata) {
            $scope.UserListS = Vdata;
        }, function (error) {
        });
        BookParcelService.BindLocation().then(function (Udata) {
            $scope.DestinationLocationList = Udata;
        }, function (error) {
        });
        BookParcelService.BindUser().then(function (Vdata) {
            $scope.DestinationUserListS = Vdata;
        }, function (error) {
        });
        BookParcelService.BindCourierAgency().then(function (bdata) {
            $scope.CourierAgencyList = bdata;
        }, function (error) {
        });
        BookParcelService.BindCourierMode().then(function (cdata) {
            $scope.CourierModeList = cdata;
        }, function (error) {
        });
        BookParcelService.BindCourierCategory().then(function (xdata) {
            $scope.CourieCategoryList = xdata;
        }, function (error) {
        });
        BookParcelService.BindCourierDetails().then(function (cdata) {
            $scope.CourierDetailsList = cdata;
        }, function (error) {
        });

    };

    $scope.GetParties = function () {
        $scope.BOOKPARCEL.SENDERDEPT = $scope.UserList.find(item => item.ticked)?.AUR_DEP_ID || null;
        $scope.BOOKPARCEL.SENDERCONTACT = $scope.UserList.find(item => item.ticked)?.AUR_RES_NUMBER || null;
        $scope.BOOKPARCEL.SENDEREMAIL = $scope.UserList.find(item => item.ticked)?.AUR_EMAIL || null;
        $scope.BOOKPARCEL.SENDERCITY = $scope.UserList.find(item => item.ticked)?.CTY_NAME || null;
        $scope.BOOKPARCEL.SENDERPINCODE = $scope.UserList.find(item => item.ticked)?.LCM_PINCODE || null;
        $scope.BOOKPARCEL.SENDERCOUNTRY = $scope.UserList.find(item => item.ticked)?.CNY_NAME || null;
        $scope.BOOKPARCEL.DestinationDEPT = $scope.DestinationUserList.find(item => item.ticked)?.AUR_DEP_ID || null;
        $scope.BOOKPARCEL.DestinationCONTACT = $scope.DestinationUserList.find(item => item.ticked)?.AUR_RES_NUMBER || null;
        $scope.BOOKPARCEL.DestinationEMAIL = $scope.DestinationUserList.find(item => item.ticked)?.AUR_EMAIL || null;
        $scope.BOOKPARCEL.DestinationCITY = $scope.DestinationUserList.find(item => item.ticked)?.CTY_NAME || null;
        $scope.BOOKPARCEL.DestinationPINCODE = $scope.DestinationUserList.find(item => item.ticked)?.LCM_PINCODE || null;
        $scope.BOOKPARCEL.DestinationCOUNTRY = $scope.DestinationUserList.find(item => item.ticked)?.CNY_NAME || null;


    }
    //var columnDefs = [
    //    { headerName: "Document Name", field: "DFDC_ACT_NAME", width: 130, cellClass: 'grid-align' },
    //    { headerName: "Delete", width: 70, template: '<a ng-click = "DeleteDoc(data)"> <i class="fa fa-times class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', onmouseover: "cursor: hand (a pointing hand)", suppressMenu: true }
    //];

    //$scope.gridOptions = {
    //    columnDefs: columnDefs,
    //    rowData: null,
    //    enableSorting: true,
    //    cellClass: 'grid-align',
    //    angularCompileRows: true,
    //    enableFilter: true,
    //    //enableColResize: true,
    //    suppressHorizontalScroll: true,
    //    enableCellSelection: false,
    //    onReady: function () {
    //        $scope.gridOptions.api.sizeColumnsToFit()
    //    }
    //};
    $scope.onBindUsers = function () {
        debugger;

        $scope.UserList = $filter('filter')($scope.UserListS, { LCM_CODE: $scope.LocationList.LCM_NAME[0].LCM_CODE });

    }
    $scope.onBindRecipientUsers = function () {
        debugger;

        $scope.DestinationUserList = $filter('filter')($scope.DestinationUserListS, { LCM_CODE: $scope.DestinationLocationList.Destination_LCM_NAME[0].Destination_LCM_CODE });

    }
    $scope.Save = function () {
        debugger;
        var obj =
        {
            BRNCODE: $scope.BOOKPARCEL.BRNCODE,
            todate: $scope.BOOKPARCEL.todate,
            senderLocation: $scope.LocationList.LCM_NAME[0].LCM_CODE,
            SenderName: $scope.UserList.AUR_KNOWN_AS[0].AUR_ID,
            SenderDepartment: $scope.BOOKPARCEL.SENDERDEPT,
            SenderContactno: $scope.BOOKPARCEL.SENDERCONTACT,
            SENDEREMAIL: $scope.BOOKPARCEL.SENDEREMAIL,
            SENDERCITY: $scope.BOOKPARCEL.SENDERCITY,
            SENDERPINCODE: $scope.BOOKPARCEL.SENDERPINCODE,
            SENDERCOUNTRY: $scope.BOOKPARCEL.SENDERCOUNTRY,
            //RecipientLocation: $scope.DestinationLocationList.Destination_LCM_NAME[0].Destination_LCM_CODE,
            //RecipientName: $scope.DestinationUserList.Destination_AUR_KNOWN_AS[0].Destination_AUR_ID,
            RecipientLocation: $scope.DestinationLocationList.Destination_LCM_NAME && $scope.DestinationLocationList.Destination_LCM_NAME.length > 0
                ? $scope.DestinationLocationList.Destination_LCM_NAME[0].Destination_LCM_CODE
                : null,
            RecipientName: $scope.DestinationUserList.Destination_AUR_KNOWN_AS && $scope.DestinationUserList.Destination_AUR_KNOWN_AS.length > 0
                ? $scope.DestinationUserList.Destination_AUR_KNOWN_AS[0].Destination_AUR_ID
                : null,
            RecipientDepartment: $scope.BOOKPARCEL.DestinationDEPT || null,
            RecipientContactno: $scope.BOOKPARCEL.DestinationCONTACT || null,
            RecipientEmail: $scope.BOOKPARCEL.DestinationEMAIL || null,
            RecipientCity: $scope.BOOKPARCEL.DestinationCITY || null,
            RecipientPincode: $scope.BOOKPARCEL.DestinationPINCODE || null,
            RecipientCountry: $scope.BOOKPARCEL.DestinationCOUNTRY || null,
            BORecipientLocation: $scope.BOOKPARCEL.RCL || null,
            BORecipientName: $scope.BOOKPARCEL.RecipientName || null,
            BORecipientContactno: $scope.BOOKPARCEL.RecipientContactno || null,
            BORecipientEmail: $scope.BOOKPARCEL.RecipientEmail || null,
            BORecipientCity: $scope.BOOKPARCEL.RecipientCity || null,
            BORecipientPincode: $scope.BOOKPARCEL.RecipientPincode || null,
            BORecipientCountry: $scope.BOOKPARCEL.RecipientCountry || null,
            recipientType: $scope.recipientType || null,

            //CourierAgency: $scope.CourierAgencyList.COURIER_NAME[0].COURIER_CODE,
            //AWBPODNO: $scope.BOOKPARCEL.AWBPODNO,
            //Mode: $scope.CourierModeList.COURIERMODE_NAME[0].COURIERMODE_CODE,
            //ParcelCategory: $scope.CourieCategoryList.PARCEL_NAME[0].PARCEL_CODE,
            //ParcelDetails: $scope.CourierDetailsList.COURIERDETAILS_NAME[0].COURIERDETAILS_CODE,
            //INVOICENO: $scope.BOOKPARCEL.INVOICENO,
            //InvoiceAMOUNT: $scope.BOOKPARCEL.InvoiceAMOUNT,
            //Weight: $scope.BOOKPARCEL.WEIGHT,
            //NOOFPIECES: $scope.BOOKPARCEL.NOOFPIECES,
            //DeliveryRemarks: $scope.BOOKPARCEL.DNREMARKS,
        };

        BookParcelService.SaveData(obj).then(function (response) {


            if (response.Message != null) {
                $scope.ShowMessage = true;
                $scope.Success = "Courier Successfully Registered in Outbound List";
                var savedobj = {};
                angular.copy(obj, savedobj)
                showNotification('success', 8, 'bottom-right', response.Message);
                $scope.ClearData();
               
               

                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        //window.location.reload();
                    });
                }, 500);

            }
            else {
                showNotification('error', 8, 'bottom-right','Something went wrong. Please try again later.');
            }

        }, function (error) {
            showNotification('error', 8, 'bottom-right', error);
           

        });
    }

    $scope.ClearData = function () {

       
        $scope.SaveExpUtility = {};
        $scope.BOOKPARCEL = {};
        $scope.LocationList = [];
        $scope.UserList = [];
        $scope.DestinationLocationList = [];
        $scope.DestinationUserList = [];
        $scope.CourierAgencyList = [];
        $scope.CourierModeList = [];
        $scope.CourierDetailsList = [];
        $scope.CourieCategoryList = [];
       
    }
    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
    $scope.UploadFile = function () {

        if ($scope.document.DFDC_DDT_CODE != undefined && $scope.document.DFDC_DDT_CODE != "") {
            uploadFileWithCode('DFDC_DDT_CODE', 'FileUpl');
        }
    }
    function uploadFileWithCode(DocType, UplFileID) {
        debugger;
        if (DocType != undefined && DocType != "") {
            var Ext = $('#' + UplFileID).val().split('.').pop().toLowerCase();
            var fname = $('#' + UplFileID).val().split('\\');
            var str = fname[fname.length - 1];
            var fileNameLength = str.length;
            var ExtCount = str.split('.');
            if (ExtCount.length <= 2) {
                if ($('#' + UplFileID).val()) {
                    if (/^[a-zA-Z0-9-_. ]*$/.test(str)) {
                        if (Ext == "pdf") {
                            if (fileNameLength <= 50) {
                                if (!window.FormData) {
                                    redirect(); // if IE8   
                                } else {
                                    var formData = new FormData();
                                    var uplFile = $('#' + UplFileID)[0];
                                    formData.append("UplFile", uplFile.files[0]);
                                    formData.append("CurrObj", $scope.document[DocType]);
                                    $.ajax({
                                        url: UtilityService.path + '/api/FactSheet/UploadTemplate',
                                        type: "POST",
                                        data: formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success: function (data) {
                                            var respdata = JSON.parse(data);
                                            if (respdata.data != null) {
                                                angular.forEach(respdata.data, function (data) {
                                                    $scope.DocumentDetails.push(data);
                                                });
                                                console.log($scope.DocumentDetails);
                                                $scope.gridOptions.api.setRowData($scope.DocumentDetails);
                                                showNotification('success', 8, 'bottom-right', "File Uploaded Successfully");
                                                $('#' + UplFileID).val('');
                                                $scope.document[DocType] = '';
                                            } else {
                                                showNotification('error', 8, 'bottom-right', respdata.Message);
                                            }
                                        }
                                    });
                                }
                            } else {
                                showNotification('error', 8, 'bottom-right', 'Filename exceeds the maximum allowed length (50 characters).');
                            }
                        } else {
                            showNotification('error', 8, 'bottom-right', 'Please upload only PDF files');
                        }
                    } else {
                        showNotification('error', 8, 'bottom-right', 'Your file name contains special characters');
                    }
                } else {
                    showNotification('error', 8, 'bottom-right', 'Please select a file to upload');
                }
            } else {
                showNotification('error', 8, 'bottom-right', 'Please upload a valid file.');
            }
        } else {
            showNotification('error', 8, 'bottom-right', 'Please select a document type');
        }
    }
    $scope.LoadData();
}]);
