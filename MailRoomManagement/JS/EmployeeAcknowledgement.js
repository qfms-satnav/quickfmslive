﻿// Service to handle API requests
app.service('GetEmployeAcknowledgeService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }


    this.GetEMPLOYEEACKGRID = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/GetEMPLOYEEACKGRID' ));
    };

    this.GetParcelDetails = function (ReqId) {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/GetParcelDetails?ReqId=' + ReqId));
    };
    this.EmployeeAcknowledge = function (data) {
        return handleAPIRequest($http.post(UtilityService.path + '/api/BookParcel/EmployeeAcknowledge', data));
    };
    this.EmployeeAcknowledgeReject = function (data) {
        return handleAPIRequest($http.post(UtilityService.path + '/api/BookParcel/EmployeeAcknowledgeReject', data));
    };
}]);

app.controller('GetEmployeAcknowledgeController', ['$scope', '$http', 'UtilityService', 'GetEmployeAcknowledgeService', '$compile', '$timeout', function ($scope, $http, UtilityService, GetEmployeAcknowledgeService, $compile, $timeout) {

    $scope.BOOKPARCEL = {};
    $scope.LocationList = [];
    $scope.UserList = [];
    $scope.DestinationLocationList = [];
    $scope.DestinationUserList = [];
    $scope.CourierAgencyList = [];
    $scope.CourierModeList = [];
    $scope.CourierDetailsList = [];
    $scope.CourieCategoryList = [];
    $scope.BookingList = [];
    $scope.ReqDetails = false;
    // Column definitions for the grid
    
    var columnDefs = [
        {  headerName: "Select All",field: "ticked",width: 90,cellRenderer: function (params) { return `<input type='checkbox' ng-model='data.ticked' ng-change='chkChanged(data)' />`; }, cellClass: 'grid-align', suppressMenu: true},
        { headerName: "Booking ID", field: "DFDC_ACT_NAME", width: 200, cellClass: 'grid-align'},
        { headerName: "Sender Name",width: 150,field: "SENDER_DEPT",cellClass: 'grid-align' },
        { headerName: "Sender Location",field: "SEND_LOC",width: 150,cellClass: 'grid-align'},
        { headerName: "Recipient Name", field: "DES_DEP", width: 150,cellClass: 'grid-align' },
        { headerName: "Recipient Location",field: "DES_LOC", width: 150,cellClass: 'grid-align' },
        { headerName: "Awb No",field: "PARCEL_AWB_NO",width: 150,cellClass: 'grid-align' },
        { headerName: "Courier Agency",field: "COURIER_NAME",width: 150, cellClass: 'grid-align'},
        { headerName: "Courier Status",field: "STA_TITLE", width: 150,cellClass: 'grid-align'},

    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableCellSelection: false,
        suppressHorizontalScroll: true,
        enableFilter: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };


  

    $scope.LoadData = function () {
        GetEmployeAcknowledgeService.GetEMPLOYEEACKGRID().then(function (gddata) {
            $scope.gridData = gddata;  // Corrected variable name
            $scope.gridOptions.api.setRowData(gddata);
        }, function (error) {
            console.log(error);
        });

    };

    $timeout(function () {
        $scope.LoadData();
    }, 500);

    $scope.getSelectedRows = function () {
        var selectedRows = [];
        var rowData = $scope.gridOptions.api.getRenderedNodes();
        rowData.forEach(function (row) {
            if (row.data.ticked) {
                selectedRows.push(row.data); 
            }
        });
        return selectedRows;
    };

    $scope.UpdateStatus = function () {
        debugger;
        var selectedRows = $scope.getSelectedRows();

        if (selectedRows.length === 0) {
            showNotification('error', 8, 'bottom-right', 'Please select at least one checkbox to proceed.');
            return; 
        }

        var selectedBookingIds = selectedRows.map(function (row) {
            return row.DFDC_ACT_NAME; 
        });

        selecteddata = [];
        angular.forEach(selectedRows, function(item){
            if (item.ticked = true) {

                selecteddata.push({ DFDC_ACT_NAME :item.DFDC_ACT_NAME });
            }
        })


        var obj = {
            UpdateStatusAcknowledgeList: selecteddata
        };

        GetEmployeAcknowledgeService.EmployeeAcknowledge(obj).then(function (response) {
            if (response.Message != null) {
                $scope.ShowMessage = true;
                $scope.Success = "Employee acknowledgment was successful";
                showNotification('success', 8, 'bottom-right', response.Message);
                setTimeout(function () {
                    location.reload();
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                      
                    });
                }, 500);
            } else {
                showNotification('error', 8, 'bottom-right', 'Something went wrong. Please try again later.');
            }

        }, function (error) {
            showNotification('error', 8, 'bottom-right', error);
        });
    };
    $scope.UpdateStatusReject = function () {
        debugger;
        var selectedRows = $scope.getSelectedRows();

        if (selectedRows.length === 0) {
            showNotification('error', 8, 'bottom-right', 'Please select at least one checkbox to proceed.');
            return;
        }

        var selectedBookingIds = selectedRows.map(function (row) {
            return row.DFDC_ACT_NAME;
        });

        selecteddata = [];
        angular.forEach(selectedRows, function (item) {
            if (item.ticked = true) {

                selecteddata.push({ DFDC_ACT_NAME: item.DFDC_ACT_NAME });
            }
        })


        var obj = {
            UpdateStatusAcknowledgeList: selecteddata
        };

        GetEmployeAcknowledgeService.EmployeeAcknowledgeReject(obj).then(function (response) {
            if (response.Message != null) {
                $scope.ShowMessage = true;
                $scope.Success = "Employee acknowledgment was Rejectedt";
                showNotification('success', 8, 'bottom-right', response.Message);
                setTimeout(function () {
                    location.reload();
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                    });
                }, 500);
            } else {
                showNotification('error', 8, 'bottom-right', 'Something went wrong. Please try again later.');
            }

        }, function (error) {
            showNotification('error', 8, 'bottom-right', error);
        });
    };


     


 
    

}]);
