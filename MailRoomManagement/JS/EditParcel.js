﻿app.service('EditParcelService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    function handleAPIRequest(promise) {
        var deferred = $q.defer();
        promise.then(function (response) {
            deferred.resolve(response.data);
        }, function (response) {
            deferred.reject(response);
        });
        return deferred.promise;
    }
    this.GetParcelReqid = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/EditParcel/GetParcelReqid'));
    };
    this.GetParcelDetails = function (ReqId) {
        return handleAPIRequest($http.get(UtilityService.path + '/api/EditParcel/GetParcelDetails?ReqId='+ReqId));
    };
    this.GetGriddata = function () {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/BookParcel/GetGridData')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.BindLocation = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindLocation'));
    };
    this.BindUser = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindUser'));
    };
    this.BindCourierAgency = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierAgency'));
    };
    this.BindCourierMode = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierMode'));
    };
    this.BindCourierCategory = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierCategory'));
    };
    this.BindCourierDetails = function () {
        return handleAPIRequest($http.get(UtilityService.path + '/api/BookParcel/BindCourierDetails'));
    };
    this.SaveData = function (data) {
        return handleAPIRequest($http.post(UtilityService.path + '/api/BookParcel/SaveData', data));
    };
    this.UpdateData = function (data) {
        return handleAPIRequest($http.post(UtilityService.path + '/api/BookParcel/UpdateData', data));
    }
}]);

app.controller('EditParcelController', ['$scope', 'EditParcelService', '$http', 'UtilityService', '$filter', function ($scope, EditParcelService, $http, UtilityService, $filter) {

    $scope.BOOKPARCEL = {};
    $scope.LocationList = [];
    $scope.UserList = [];
    $scope.DestinationLocationList = [];
    $scope.DestinationUserList = [];
    $scope.CourierAgencyList = [];
    $scope.CourierModeList = [];
    $scope.CourierDetailsList = [];
    $scope.CourieCategoryList = [];
    $scope.BookingList = [];
    $scope.ReqDetails = false;
    

    $scope.LoadData = function () {
        EditParcelService.GetParcelReqid().then(function (data8) {
            $scope.BookingList = data8;
        }, function (error) {
        });       
        EditParcelService.BindLocation().then(function (Udata) {
            $scope.LocationList = Udata;
        }, function (error) {
        });
        EditParcelService.BindLocation().then(function (Udata) {
            $scope.DestinationLocationList = Udata;
        }, function (error) {
        });
        EditParcelService.BindUser().then(function (Vdata) {
            $scope.UserList = Vdata;
        }, function (error) {
        });
        EditParcelService.BindUser().then(function (Vdata) {
            $scope.DestinationUserList = Vdata;
        }, function (error) {
        });
        EditParcelService.BindCourierAgency().then(function (bdata) {
            $scope.CourierAgencyList = bdata;
        }, function (error) {
        });
        EditParcelService.BindCourierMode().then(function (cdata) {
            $scope.CourierModeList = cdata;
        }, function (error) {
        });
        EditParcelService.BindCourierCategory().then(function (xdata) {
            $scope.CourieCategoryList = xdata;
        }, function (error) {
        });
        EditParcelService.BindCourierDetails().then(function (cdata) {
            $scope.CourierDetailsList = cdata;
        }, function (error) {
        });        
    };

    $scope.GetParties = function () {
        $scope.BOOKPARCEL.SENDERDEPT = $scope.UserList.find(item => item.ticked)?.AUR_DEP_ID || null;
        $scope.BOOKPARCEL.SENDERCONTACT = $scope.UserList.find(item => item.ticked)?.AUR_RES_NUMBER || null;
        $scope.BOOKPARCEL.SENDEREMAIL = $scope.UserList.find(item => item.ticked)?.AUR_EMAIL || null;
        $scope.BOOKPARCEL.SENDERCITY = $scope.UserList.find(item => item.ticked)?.CTY_NAME || null;
        $scope.BOOKPARCEL.SENDERPINCODE = $scope.UserList.find(item => item.ticked)?.LCM_PINCODE || null;
        $scope.BOOKPARCEL.SENDERCOUNTRY = $scope.UserList.find(item => item.ticked)?.CNY_NAME || null;
        $scope.BOOKPARCEL.DestinationDEPT = $scope.DestinationUserList.find(item => item.ticked)?.AUR_DEP_ID || null;
        $scope.BOOKPARCEL.DestinationCONTACT = $scope.DestinationUserList.find(item => item.ticked)?.AUR_RES_NUMBER || null;
        $scope.BOOKPARCEL.DestinationEMAIL = $scope.DestinationUserList.find(item => item.ticked)?.AUR_EMAIL || null;
        $scope.BOOKPARCEL.DestinationCITY = $scope.DestinationUserList.find(item => item.ticked)?.CTY_NAME || null;
        $scope.BOOKPARCEL.DestinationPINCODE = $scope.DestinationUserList.find(item => item.ticked)?.LCM_PINCODE || null;
        $scope.BOOKPARCEL.DestinationCOUNTRY = $scope.DestinationUserList.find(item => item.ticked)?.CNY_NAME || null;


    }
    var columnDefs = [
        {
            headerName: "Booking ID",
            field: "DFDC_ACT_NAME",
            width: 200,
            cellRenderer: function (params) {
                return `<a ng-click="onBookingIdClick('${params.value}')" style="cursor: pointer;">${params.value}</a>`;

            },
            cellClass: 'grid-align'
        },
        {
            headerName: "Sender Name",
            width: 150,
            field: "SENDER_DEPT",
            cellClass: 'grid-align'
        },
        {
            headerName: "Sender Location",
            field: "SEND_LOC",
            width: 150,
            cellClass: 'grid-align'
        },
        {
            headerName: "Recipient Name",
            field: "DES_DEP",
            width: 150,
            cellClass: 'grid-align'
        },
        {
            headerName: "Recipient Location",
            field: "DES_LOC",
            width: 150,
            cellClass: 'grid-align'

        },
         {
            headerName: "Awb No",
             field: "PARCEL_AWB_NO",
            width: 150,
            cellClass: 'grid-align'

        },
        {
            headerName: "Courier Agency",
            field: "COURIER_NAME",
            width: 150,
            cellClass: 'grid-align'

        },
        {
            headerName: "Courier Status",
            field: "STA_TITLE",
            width: 150,
            cellClass: 'grid-align'

        },

    ];

    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        enableFilter: true,
        angularCompileRows: true,
        suppressHorizontalScroll: true,
        enableCellSelection: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };
 

    EditParcelService.GetGriddata().then(function (gddata) {
        $scope.gridata = gddata;
        $scope.gridOptions.api.setRowData(gddata);
    }, function (error) {
        console.log(error);
    });
    
   
  
        $scope.onBookingIdClick = function (bookingId) {
 /*           alert("You clicked Booking ID: " + bookingId);*/
            $scope.ClearAll();
            $scope.ReqDetails = true;
            $scope.showBookingDate = true;
            EditParcelService.GetParcelDetails(bookingId).then(function (data) {
                if (data[0]) {
                    const parcel = data[0];
                    $scope.BOOKPARCEL.todate = new Date(data[0].todate).toLocaleDateString('en-GB');

            
                    angular.forEach($scope.LocationList, function (location) {
                        if (location.LCM_CODE === data[0].senderLocation) {
                            location.ticked = true;
                            angular.forEach($scope.UserList, function (Sender) {
                                if (Sender.AUR_ID === data[0].SenderId) {
                                    Sender.ticked = true;
                                }
                            });
                        }
                    });
                    $scope.BOOKPARCEL.SENDERDEPT = data[0].SenderDepartment;
                    $scope.BOOKPARCEL.SENDERCONTACT = data[0].SenderContactno;
                    $scope.BOOKPARCEL.SENDEREMAIL = data[0].SENDEREMAIL;
                    $scope.BOOKPARCEL.SENDERCITY = data[0].SENDERCITY;
                    $scope.BOOKPARCEL.SENDERPINCODE = data[0].SENDERPINCODE;
                    $scope.BOOKPARCEL.SENDERCOUNTRY = data[0].SENDERCOUNTRY;

               
                    angular.forEach($scope.DestinationLocationList, function (location) {
                        if (location.LCM_CODE === data[0].RecipientLocation) {
                            location.ticked = true;
                            angular.forEach($scope.DestinationUserList, function (Recipient) {
                                if (Recipient.AUR_ID === data[0].RecipientId) {
                                    Recipient.ticked = true;
                                }
                            });
                        }
                    });
                    $scope.BOOKPARCEL.DestinationDEPT = data[0].RecipientDepartment;
                    $scope.BOOKPARCEL.DestinationCONTACT = data[0].RecipientContactno;
                    $scope.BOOKPARCEL.DestinationEMAIL = data[0].RecipientEmail;
                    $scope.BOOKPARCEL.DestinationCITY = data[0].RecipientCity;
                    $scope.BOOKPARCEL.DestinationPINCODE = data[0].RecipientPincode;
                    $scope.BOOKPARCEL.DestinationCOUNTRY = data[0].RecipientCountry;
                    $scope.BOOKPARCEL.BRNCODE = bookingId;

         
                    angular.forEach($scope.CourierAgencyList, function (Agency) {
                        if (Agency.COURIER_CODE === data[0].CourierAgency) {
                            Agency.ticked = true;
                        }
                    });
                    $scope.BOOKPARCEL.AWBPODNO = data[0].AWBPODNO;

              
                    angular.forEach($scope.CourierModeList, function (Mode) {
                        if (Mode.COURIERMODE_CODE === data[0].Mode) {
                            Mode.ticked = true;
                        }
                    });

              
                    angular.forEach($scope.CourieCategoryList, function (Category) {
                        if (Category.PARCEL_CODE === data[0].ParcelCategory) {
                            Category.ticked = true;
                        }
                    });

               
                    angular.forEach($scope.CourierDetailsList, function (ParcelDet) {
                        if (ParcelDet.COURIERDETAILS_CODE === data[0].ParcelDetails) {
                            ParcelDet.ticked = true;
                        }
                    });
                    $scope.BOOKPARCEL.INVOICENO = data[0].INVOICENO;
                    $scope.BOOKPARCEL.InvoiceAMOUNT = data[0].InvoiceAMOUNT;
                    $scope.BOOKPARCEL.WEIGHT = data[0].Weight;
                    $scope.BOOKPARCEL.NOOFPIECES = data[0].NOOFPIECES;
                    $scope.BOOKPARCEL.DNREMARKS = data[0].DeliveryRemarks;
                }
            }, function (error) {
                console.error('Error fetching parcel details:', error);
            });

            $scope.ReqDetails = true;
        };

    $scope.onBackClick = function () {
        $scope.ClearAll();
        location.reload();
        bookingId = "";
        $scope.ReqDetails = false;
    };


    $scope.onBindUsers = function () {
        debugger;

        $scope.UserList = $filter('filter')($scope.UserListS, { LCM_CODE: $scope.LocationList.LCM_NAME[0].LCM_CODE });

    }
    //$scope.onBindBooking = function (bookingId) {
    //    debugger;
    //    var ReqId = bookingId;
    //    $scope.ClearAll();
    //    EditParcelService.GetParcelDetails(ReqId).then(function (data) {

    //        $scope.BOOKPARCEL.todate = new Date(data[0].todate).toLocaleDateString('en-GB');
    //        if (data[0] && data[0].senderLocation) {
    //            angular.forEach($scope.LocationList, function (location) {
    //                if (location.LCM_CODE === data[0].senderLocation) { 
    //                    location.ticked = true;
    //                    angular.forEach($scope.UserList, function (Sender) {
    //                        if (Sender.AUR_ID === data[0].SenderId) {
    //                            Sender.ticked = true;
    //                        }
    //                    });
    //                }
    //            });                
    //            $scope.BOOKPARCEL.SENDERDEPT = data[0].SenderDepartment;
    //            $scope.BOOKPARCEL.SENDERCONTACT = data[0].SenderContactno;
    //            $scope.BOOKPARCEL.SENDEREMAIL = data[0].SENDEREMAIL;
    //            $scope.BOOKPARCEL.SENDERCITY = data[0].SENDERCITY;
    //            $scope.BOOKPARCEL.SENDERPINCODE = data[0].SENDERPINCODE;
    //            $scope.BOOKPARCEL.SENDERCOUNTRY = data[0].SENDERCOUNTRY;
    //        }
    //        if (data[0] && data[0].senderLocation) {
    //            angular.forEach($scope.DestinationLocationList, function (location) {
    //                if (location.LCM_CODE === data[0].RecipientLocation) {
    //                    location.ticked = true;                        
    //                    angular.forEach($scope.DestinationUserList, function (Recipient) {
    //                        if (Recipient.AUR_ID === data[0].RecipientId) {
    //                            Recipient.ticked = true;
    //                        }
    //                    });
    //                }
    //            });
    //            $scope.BOOKPARCEL.DestinationDEPT = data[0].RecipientDepartment;
    //            $scope.BOOKPARCEL.DestinationCONTACT = data[0].RecipientContactno;
    //            $scope.BOOKPARCEL.DestinationEMAIL = data[0].RecipientEmail;
    //            $scope.BOOKPARCEL.DestinationCITY = data[0].RecipientCity;
    //            $scope.BOOKPARCEL.DestinationPINCODE = data[0].RecipientPincode;
    //            $scope.BOOKPARCEL.DestinationCOUNTRY = data[0].RecipientCountry;
    //        }
    //        angular.forEach($scope.CourierAgencyList, function (Agency) {
    //            if (Agency.COURIER_CODE === data[0].CourierAgency) {
    //                Agency.ticked = true;
    //            }
    //        });
    //        $scope.BOOKPARCEL.AWBPODNO = data[0].AWBPODNO;
    //        angular.forEach($scope.CourierModeList, function (Mode) {
    //            if (Mode.COURIERMODE_CODE === data[0].Mode) {
    //                Mode.ticked = true;
    //            }
    //        });
    //        angular.forEach($scope.CourieCategoryList, function (Category) {
    //            if (Category.PARCEL_CODE === data[0].ParcelCategory) {
    //                Category.ticked = true;
    //            }
    //        });
    //        angular.forEach($scope.CourierDetailsList, function (ParcelDet) {
    //            if (ParcelDet.COURIERDETAILS_CODE === data[0].ParcelDetails) {
    //                ParcelDet.ticked = true;
    //            }
    //        });
    //        $scope.BOOKPARCEL.INVOICENO = data[0].INVOICENO;
    //        $scope.BOOKPARCEL.InvoiceAMOUNT = data[0].InvoiceAMOUNT;
    //        $scope.BOOKPARCEL.WEIGHT = data[0].Weight;
    //        $scope.BOOKPARCEL.NOOFPIECES = data[0].NOOFPIECES;
    //        $scope.BOOKPARCEL.DNREMARKS = data[0].DeliveryRemarks;
    //    }, function (error) {
    //    });

    //    $scope.ReqDetails = true;
    //    ReqId = "";
    //}

    //$scope.onBindRecipientUsers = function () {
    //    debugger;

    //    $scope.DestinationUserList = $filter('filter')($scope.DestinationUserListS, { LCM_CODE: $scope.DestinationLocationList.LCM_NAME[0].LCM_CODE });

    //}
  

    $scope.Update = function () {
        var obj =
        {
        BRNCODE: $scope.BOOKPARCEL.BRNCODE,
        todate: $scope.BOOKPARCEL.todate,
        senderLocation: $scope.LocationList.LCM_NAME[0].LCM_CODE,
        SenderName: $scope.UserList.AUR_KNOWN_AS[0].AUR_ID,
        SenderDepartment: $scope.BOOKPARCEL.SENDERDEPT,
        SenderContactno: $scope.BOOKPARCEL.SENDERCONTACT,
        SENDEREMAIL: $scope.BOOKPARCEL.SENDEREMAIL,
        SENDERCITY: $scope.BOOKPARCEL.SENDERCITY,
        SENDERPINCODE: $scope.BOOKPARCEL.SENDERPINCODE,
        SENDERCOUNTRY: $scope.BOOKPARCEL.SENDERCOUNTRY,
        RecipientLocation: $scope.DestinationLocationList.LCM_NAME[0].LCM_CODE,
        RecipientName: $scope.DestinationUserList.AUR_KNOWN_AS[0].AUR_ID,
        RecipientDepartment: $scope.BOOKPARCEL.DestinationDEPT,
        RecipientContactno: $scope.BOOKPARCEL.DestinationCONTACT,
        RecipientEmail: $scope.BOOKPARCEL.DestinationEMAIL,
        RecipientCity: $scope.BOOKPARCEL.DestinationCITY,
        RecipientPincode: $scope.BOOKPARCEL.DestinationPINCODE,
        RecipientCountry: $scope.BOOKPARCEL.DestinationCOUNTRY,
        CourierAgency: $scope.CourierAgencyList.COURIER_NAME[0].COURIER_CODE,
        AWBPODNO: $scope.BOOKPARCEL.AWBPODNO,
        Mode: $scope.CourierModeList.COURIERMODE_NAME[0].COURIERMODE_CODE,
        ParcelCategory: $scope.CourieCategoryList.PARCEL_NAME[0].PARCEL_CODE,
        ParcelDetails: $scope.CourierDetailsList.COURIERDETAILS_NAME[0].COURIERDETAILS_CODE,
        INVOICENO: $scope.BOOKPARCEL.INVOICENO,
        InvoiceAMOUNT: $scope.BOOKPARCEL.InvoiceAMOUNT,
        Weight: $scope.BOOKPARCEL.WEIGHT,
        NOOFPIECES: $scope.BOOKPARCEL.NOOFPIECES,
        DeliveryRemarks: $scope.BOOKPARCEL.DNREMARKS,
    };
    EditParcelService.UpdateData(obj).then(function (response) {


        if (response.Message != null) {
            $scope.ShowMessage = true;
            $scope.Success = "Courier Successfully Updated in Outbound List";
            var savedobj = {};
            angular.copy(obj, savedobj)
            showNotification('success', 8, 'bottom-right', response.Message);
            $scope.ClearData();

            setTimeout(function () {
                $scope.$apply(function () {
                    $scope.ShowMessage = false;
                    //window.location.reload();
                });
            }, 500);

        }
        else {
            showNotification('error', 8, 'bottom-right', 'Something went wrong. Please try again later.');
        }

    }, function (error) {
        showNotification('error', 8, 'bottom-right', error);


    });
    }

    $scope.ClearData = function () {

       
        $scope.SaveExpUtility = {};
        $scope.BOOKPARCEL = {};
        $scope.LocationList = [];
        $scope.UserList = [];
        $scope.DestinationLocationList = [];
        $scope.DestinationUserList = [];
        $scope.CourierAgencyList = [];
        $scope.CourierModeList = [];
        $scope.CourierDetailsList = [];
        $scope.CourieCategoryList = [];
       
    }
    $scope.ClearAll = function () {

        function resetTickedProperty(list) {
            angular.forEach(list, function (item) {
                item.ticked = false;
            });
        }
        resetTickedProperty($scope.LocationList);
        resetTickedProperty($scope.UserList);
        resetTickedProperty($scope.DestinationLocationList);
        resetTickedProperty($scope.DestinationUserList);
        resetTickedProperty($scope.CourierAgencyList);
        resetTickedProperty($scope.CourierModeList);
        resetTickedProperty($scope.CourierDetailsList);
        resetTickedProperty($scope.CourieCategoryList);
        $scope.SaveExpUtility = {};
        $scope.BOOKPARCEL = {};
    };
    setTimeout(function () {
        //$scope.LoadData();
    }, 1000);
    $scope.UploadFile = function () {

        if ($scope.document.DFDC_DDT_CODE != undefined && $scope.document.DFDC_DDT_CODE != "") {
            uploadFileWithCode('DFDC_DDT_CODE', 'FileUpl');
        }
    }
    function uploadFileWithCode(DocType, UplFileID) {
        debugger;
        if (DocType != undefined && DocType != "") {
            var Ext = $('#' + UplFileID).val().split('.').pop().toLowerCase();
            var fname = $('#' + UplFileID).val().split('\\');
            var str = fname[fname.length - 1];
            var fileNameLength = str.length;
            var ExtCount = str.split('.');
            if (ExtCount.length <= 2) {
                if ($('#' + UplFileID).val()) {
                    if (/^[a-zA-Z0-9-_. ]*$/.test(str)) {
                        if (Ext == "pdf") {
                            if (fileNameLength <= 50) {
                                if (!window.FormData) {
                                    redirect(); // if IE8   
                                } else {
                                    var formData = new FormData();
                                    var uplFile = $('#' + UplFileID)[0];
                                    formData.append("UplFile", uplFile.files[0]);
                                    formData.append("CurrObj", $scope.document[DocType]);
                                    $.ajax({
                                        url: UtilityService.path + '/api/FactSheet/UploadTemplate',
                                        type: "POST",
                                        data: formData,
                                        cache: false,
                                        contentType: false,
                                        processData: false,
                                        success: function (data) {
                                            var respdata = JSON.parse(data);
                                            if (respdata.data != null) {
                                                angular.forEach(respdata.data, function (data) {
                                                    $scope.DocumentDetails.push(data);
                                                });
                                                console.log($scope.DocumentDetails);
                                                $scope.gridOptions.api.setRowData($scope.DocumentDetails);
                                                showNotification('success', 8, 'bottom-right', "File Uploaded Successfully");
                                                $('#' + UplFileID).val('');
                                                $scope.document[DocType] = '';
                                            } else {
                                                showNotification('error', 8, 'bottom-right', respdata.Message);
                                            }
                                        }
                                    });
                                }
                            } else {
                                showNotification('error', 8, 'bottom-right', 'Filename exceeds the maximum allowed length (50 characters).');
                            }
                        } else {
                            showNotification('error', 8, 'bottom-right', 'Please upload only PDF files');
                        }
                    } else {
                        showNotification('error', 8, 'bottom-right', 'Your file name contains special characters');
                    }
                } else {
                    showNotification('error', 8, 'bottom-right', 'Please select a file to upload');
                }
            } else {
                showNotification('error', 8, 'bottom-right', 'Please upload a valid file.');
            }
        } else {
            showNotification('error', 8, 'bottom-right', 'Please select a document type');
        }
    }
    $scope.LoadData();
}]);
