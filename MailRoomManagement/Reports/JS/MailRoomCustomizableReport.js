﻿app.service("MailRoomtService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.BindGridCustomizableReport = function () {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BookParcel/BindGridCustomizableReport')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };



    this.BindGrid_Search = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/BookParcel/BindGrid_Search', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


}]);



app.controller('MailRoomtController', ['$scope', '$q', 'MailRoomtService', '$filter', 'UtilityService', '$timeout', '$http',
    function ($scope, $q, MailRoomtService, $filter, UtilityService, $timeout, $http) {

        $scope.gridOptions = {
            columnDefs: '',
            enableColResize: true,
            enableCellSelection: false,
            enableFilter: true,
            enableSorting: true,
            enableScrollbars: false,
            groupHideGroupColumns: true,
            suppressHorizontalScroll: false,
            onReady: function () {
                $scope.gridOptions.api.sizeColumnsToFit();
            }
        };


      


        $scope.LoadData = function () {
            debugger;

            MailRoomtService.BindGridCustomizableReport().then(function (response) {
                if (response != null) {

                    $scope.gridOptions.api.setColumnDefs(response.ColumnDefinitions);
                    $scope.gridOptions.api.setRowData(response.GridData);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(response.ColumnDefinitions);
                    $scope.gridOptions.api.setRowData([]);
                }
                progress(0, 'Loading...', false);
            });
           
        };



        function fitToColumn(arrayOfArray) {
            // get maximum character of each column
            const keys = Object.keys(arrayOfArray[0]);
            return keys.map(function (key) {
                const columnValues = arrayOfArray.map(function (obj) {
                    return obj[key] ? obj[key].toString().length : 0;
                });

                return { wch: Math.max.apply(null, columnValues) };
            });
        }

        //$scope.GenReport = function () {

        //    var ws = XLSX.utils.json_to_sheet($scope.gridOptions.rowData);
        //    //ws['!cols'] = fitToColumn($scope.gridOptions.rowData);
        //    var wb = XLSX.utils.book_new();
        //    XLSX.utils.book_append_sheet(wb, ws, "Branch Details");
        //    XLSX.writeFile(wb, "Branch Occupancy Report.xlsx");
        //}
        $scope.GenReport = function () {
            var ws = XLSX.utils.json_to_sheet($scope.gridOptions.rowData);

            var maxLengths = [];
            $scope.gridOptions.rowData.forEach(function (row) {
                Object.keys(row).forEach(function (key, index) {
                    /* var length = row[key].toString().length;*/
                    var value = row[key] != null ? row[key].toString() : '';
                    var length = value.length;
                    maxLengths[index] = maxLengths[index] || 0;
                    if (length > maxLengths[index]) {
                        maxLengths[index] = length;
                    }
                });
            });

            ws['!cols'] = maxLengths.map(function (length) {
                return { width: Math.min(110, Math.max(25, length * 1.5)) }; // Adjust these values as needed
            });

            var wb = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(wb, ws, "Expense Details");
            XLSX.writeFile(wb, "Expense Report.xlsx");
        }



        $scope.BindGrid_Project = function () {
            debugger;
            var params = {
                loclst: $scope.Consolidated.LCM_NAME,


            };


            MailRoomtService.BindGrid_Search(params).then(function (response) {
                debugger;
                if (response != null) {

                    $scope.gridOptions.api.setColumnDefs(response.ColumnDefinitions);
                    $scope.gridOptions.api.setRowData(response.GridData);
                }
                else {
                    $scope.gridOptions.api.setColumnDefs(response.ColumnDefinitions);
                    $scope.gridOptions.api.setRowData([]);
                }
                progress(0, 'Loading...', false);

            }, function (error) {
                console.log(error);
            });
        };









        function onFilterChanged(value) {
            $scope.gridOptions.api.setQuickFilter(value);
            if (value) {
                $scope.DocTypeVisible = 1
            }
            else { $scope.DocTypeVisible = 0 }
        }

        $("#filtertxt").change(function () {
            onFilterChanged($(this).val());
        }).keydown(function () {
            onFilterChanged($(this).val());
        }).keyup(function () {
            onFilterChanged($(this).val());
        }).bind('paste', function () {
            onFilterChanged($(this).val());
        });

        $scope.LoadData();

    }]);

