﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<script runat="server">

</script>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
   
    <link href="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
<%--<style>
  .input-container {
    display: flex;  /* Use flexbox to align items horizontally */
    align-items: center; /* Vertically center the items */
  }

  .small-input {
    width: 600px;               /* Set the width of the input */
    padding: 5px;               /* Padding for input */
    box-sizing: border-box;     /* Include padding in the width */
  }

  .custom-button-color {
    margin-left: 10px;          /* Add space between the input and button */
  }
</style>--%>
    <style>
  .input-container {
    display: flex;               /* Use Flexbox to align items horizontally */
    align-items: center;         /* Vertically center the items */
    justify-content: flex-start; /* Align items to the left */
    gap: 10px;                   /* Adds space between the items */
  }

  .small-input {
    width: 250px;                /* Adjust the width of the input field */
    padding: 5px;                /* Padding for the input field */
    box-sizing: border-box;      /* Include padding in the width */
  }

  .custom-button-color {
    padding: 6px 15px;           /* Adjust padding for the button */
  }

  #excel {
    cursor: pointer;            /* Makes the Excel icon clickable */
  }
</style>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #428bca;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .word {
            color: #4813CA;
        }

        .pdf {
            color: #FF0023;
        }

        .excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }


        .with-nav-tabs.panel-primary .nav-tabs > .open > a,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
            color: #fff;
            background-color: #3071a9;
            border-color: transparent;
        }

        .with-nav-tabs.panel-primary .nav-tabs > li.active > a,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
        .with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
            color: #428bca;
            background-color: #fff;
            border-color: #428bca;
            border-bottom-color: transparent;
        }
    </style>
</head>
<body data-ng-controller="MailRoomtController" class="amantra">
    <div class="animsition" >
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Expense Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Customizable Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <div class="clearfix row">
                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
<%--                                    <div class="form-group">
                                        <label class="control-label">Search By</label>
                                        <input type="text" class="form-control" data-ng-model="Consolidated.LCM_NAME"
                                            name="LCM_NAME" placeholder="Search By Awb No/Booking Id" required />

                                   
                                    </div>--%>

                                </div>

<%--                                <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                    <div class="form-group">
                                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 18px; margin-left: 1%; width: 256px;">
                                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="BindGrid_Project()" />
                                            <a data-ng-click="GenReport()"><i id="excel" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>

                                        </div>
                                    </div>
                                </div>--%>
                            </div>
                            <br />
                            <br />
                            <div class="row" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                <div class="input-container">

                                    <input type="text" class="form-control small-input" data-ng-model="Consolidated.LCM_NAME"
                                        name="LCM_NAME" placeholder="Search By Awb No/Booking Id" required />
                                    <input type="submit" value="Search" class="btn btn-primary custom-button-color" data-ng-click="BindGrid_Project()" />
                                    <a data-ng-click="GenReport()"><i id="excel" data-bs-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x"></i></a>

                                </div>


                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script defer src="../../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js"></script>
    <script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
     <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    
    <script defer>

        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);

    </script>
    <script src="../../../SMViews/Utility.js"></script>
    <script src="../JS/MailRoomCustomizableReport.js"></script>
</body>
</html>