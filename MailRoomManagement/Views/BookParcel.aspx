﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };



    </script>
    <style>
        .black-line {
            border: 1px solid black;
        }
    </style>

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-viewport-wrapper.ag-layout-normal {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="BookParcelController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Book a Parcel</h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="BookParcel" data-valid-submit="Save()" novalidate>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Booking Reference No.<span style="color: red;">*</span></label>
                                <input id="txtcode" name="BRNCODE" type="text" data-ng-model="BOOKPARCEL.BRNCODE"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.BRNCODE.$invalid" style="color: red">Please Enter Booking Reference No</span>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Booking Date <span style="color: red;">*</span></label>
                                <div class="input-group date" id="todate">
                                    <input type="text" class="form-control" data-ng-model="BOOKPARCEL.todate" id="todate" name="todate" placeholder="MM/DD/YYYY" data-ng-readonly="true" required />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('todate')"></i>
                                    </span>
                                </div>
                                <span class="error" data-ng-show="BookParcel.todate.$invalid && BookParcel.$submitted" style="color: red">Please select Booking Date</span>
                            </div>
                        </div>

                    </div>

                    <hr class="black-line" />
                    <h3><strong>Sender Details :</strong></h3>

                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Sender Location<span style="color: red;">*</span></label>
                                <div>
                                    <div isteven-multi-select data-input-model="LocationList" data-output-model="LocationList.LCM_NAME" button-label="icon LCM_NAME" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon LCM_NAME" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" ng-model="LocationList.LCM_NAME"  data-on-item-click="onBindUsers()">
                                    </div>
                                    <input type="text" data-ng-model="LocationList.LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" ng-show="BookParcel.$submitted && BookParcel.LCM_NAME.$invalid" style="color: red">Please Select Sender Location </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Sender Name<span style="color: red;">*</span></label>
                                <div>
                                    <div isteven-multi-select data-input-model="UserList" data-output-model="UserList.AUR_KNOWN_AS" button-label="icon AUR_KNOWN_AS" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon AUR_KNOWN_AS" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" data-on-item-click="GetParties()" ng-model="UserList.AUR_KNOWN_AS">
                                    </div>
                                    <input type="text" data-ng-model="UserList.AUR_KNOWN_AS" name="AUR_KNOWN_AS" style="display: none" required="" />
                                    <span class="error" ng-show="BookParcel.$submitted && BookParcel.AUR_KNOWN_AS.$invalid" style="color: red">Please Select Sender Name</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Sender Department <span style="color: red;">*</span></label>
                                <input id="SD" name="SENDERDEPT" type="text" data-ng-model="BOOKPARCEL.SENDERDEPT"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.SENDERDEPT.$invalid" style="color: red">Sender Department Cannot be Empty</span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Sender Contactno <span style="color: red;">*</span></label>
                                <input id="SCN" name="SENDERCONTACT" type="text" data-ng-model="BOOKPARCEL.SENDERCONTACT"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.SENDERCONTACT.$invalid" style="color: red">Sender ContactNo Cannot be Empty</span>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Sender Email <span style="color: red;">*</span></label>
                                <input id="SE" name="SENDEREMAIL" type="text" data-ng-model="BOOKPARCEL.SENDEREMAIL"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.SENDEREMAIL.$invalid" style="color: red">Sender Email Cannot be Empty</span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Sender City <span style="color: red;">*</span></label>
                                <input id="SC" name="SENDERCITY" type="text" data-ng-model="BOOKPARCEL.SENDERCITY"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.SENDERCITY.$invalid" style="color: red">Sender City Cannot be Empty</span>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Sender Pincode <span style="color: red;">*</span></label>
                                <input id="SP" name="SENDERPINCODE" type="text" data-ng-model="BOOKPARCEL.SENDERPINCODE"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.SENDERPINCODE.$invalid" style="color: red">Sender Pincode Cannot be Empty</span>

                            </div>
                        </div>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Sender Country <span style="color: red;">*</span></label>
                                <input id="SCY" name="SENDERCOUNTRY" type="text" data-ng-model="BOOKPARCEL.SENDERCOUNTRY"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.SENDERCOUNTRY.$invalid" style="color: red">Sender Country Cannot be Empty</span>

                            </div>
                        </div>
                    </div>
                    <hr class="black-line" />
                   <%-- <div class="row">
                        <h3><strong>Recipient Details :</strong>
                            <input type="radio" id="Within Organisation" name="recipient" value="Within Organisation">
                            <label for="Within Organisation">Within Organisation</label>
                            <input type="radio" id="Beyond Organisation" name="recipient" value="Beyond Organisation">
                            <label for="Beyond Organisation">Beyond Organisation</label>
                        </h3>
                        </div>
                     <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Recipient Location<span style="color: red;">*</span></label>
                                <div>
                                    <div isteven-multi-select data-input-model="DestinationLocationList" data-output-model="DestinationLocationList.Destination_LCM_NAME" button-label="icon Destination_LCM_NAME" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon Destination_LCM_NAME" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" ng-model="DestinationLocationList.Destination_LCM_NAME" data-on-item-click="onBindRecipientUsers()">
                                    </div>
                                    <input type="text" data-ng-model="DestinationLocationList.Destination_LCM_NAME" name="Destination_LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.Destination_LCM_NAME.$invalid" style="color: red">Please Select Recipient Location</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Recipient Name<span style="color: red;">*</span></label>
                                <div>
                                    <div isteven-multi-select data-input-model="DestinationUserList" data-output-model="DestinationUserList.Destination_AUR_KNOWN_AS" button-label="icon Destination_AUR_KNOWN_AS" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon Destination_AUR_KNOWN_AS" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" data-on-item-click="GetParties()" ng-model="DestinationUserList.Destination_AUR_KNOWN_AS">
                                    </div>
                                    <input type="text" data-ng-model="DestinationUserList.Destination_AUR_KNOWN_AS" name="Destination_AUR_KNOWN_AS" style="display: none" required="" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.Destination_AUR_KNOWN_AS.$invalid" style="color: red">Please Select Recipient Name</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Recipient Department <span style="color: red;">*</span></label>
                                <input id="DD" name="DestinationDEPT" type="text" data-ng-model="BOOKPARCEL.DestinationDEPT"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationDEPT.$invalid" style="color: red">Recipient Department Cannot be Empty</span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Recipient Contactno <span style="color: red;">*</span></label>
                                <input id="DC" name="DestinationCONTACT" type="text" data-ng-model="BOOKPARCEL.DestinationCONTACT"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCONTACT.$invalid" style="color: red">Recipient Contactno Cannot be Empty</span>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Recipient Email <span style="color: red;">*</span></label>
                                <input id="DE" name="DestinationEMAIL" type="text" data-ng-model="BOOKPARCEL.DestinationEMAIL"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationEMAIL.$invalid" style="color: red">Recipient Email Cannot be Empty </span>

                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Recipient City <span style="color: red;">*</span></label>
                                <input id="DECC" name="DestinationCITY" type="text" data-ng-model="BOOKPARCEL.DestinationCITY"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCITY.$invalid" style="color: red">Recipient City Cannot be Empty</span>

                            </div>
                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Recipient Pincode <span style="color: red;">*</span></label>
                                <input id="DP" name="DestinationPINCODE" type="text" data-ng-model="BOOKPARCEL.DestinationPINCODE"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationPINCODE.$invalid" style="color: red">Recipient Pincode Cannot be Empty</span>

                            </div>
                        </div>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" >
                                <label>Recipient Country <span style="color: red;">*</span></label>
                                <input id="DCTY" name="DestinationCOUNTRY" type="text" data-ng-model="BOOKPARCEL.DestinationCOUNTRY"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCOUNTRY.$invalid" style="color: red">Recipient Country Cannot be Empty</span>
                                 
                            </div>
                        </div>
                        </div>
                    </div>--%>
                    <div class="row">
    <h3><strong>Recipient Details :</strong>
        <input type="radio" id="Within Organisation" name="recipient" value="Within Organisation" ng-model="recipientType">
        <label for="Within Organisation">Within Organisation</label>
        <input type="radio" id="Beyond Organisation" name="recipient" value="Beyond Organisation" ng-model="recipientType">
        <label for="Beyond Organisation">Beyond Organisation</label>
    </h3>
</div>

<div class="row" ng-show="recipientType === 'Within Organisation'">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Recipient Location<span style="color: red;">*</span></label>
            <div>
                <div isteven-multi-select data-input-model="DestinationLocationList" data-output-model="DestinationLocationList.Destination_LCM_NAME" button-label="icon Destination_LCM_NAME" data-is-disabled="EnableStatus==0"
                    data-item-label="icon Destination_LCM_NAME" data-tick-property="ticked"
                    data-max-labels="1" data-selection-mode="single" ng-model="DestinationLocationList.Destination_LCM_NAME" data-on-item-click="onBindRecipientUsers()">
                </div>
                <input type="text" data-ng-model="DestinationLocationList.Destination_LCM_NAME" name="Destination_LCM_NAME" style="display: none" ng-required="recipientType === 'Within Organisation'" />
                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.Destination_LCM_NAME.$invalid  && recipientType === 'Within Organisation'" style="color: red">Please Select Recipient Location</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Recipient Name<span style="color: red;">*</span></label>
            <div>
                <div isteven-multi-select data-input-model="DestinationUserList" data-output-model="DestinationUserList.Destination_AUR_KNOWN_AS" button-label="icon Destination_AUR_KNOWN_AS" data-is-disabled="EnableStatus==0"
                    data-item-label="icon Destination_AUR_KNOWN_AS" data-tick-property="ticked"
                    data-max-labels="1" data-selection-mode="single" data-on-item-click="GetParties()" ng-model="DestinationUserList.Destination_AUR_KNOWN_AS">
                </div>
                <input type="text" data-ng-model="DestinationUserList.Destination_AUR_KNOWN_AS" name="Destination_AUR_KNOWN_AS" style="display: none" ng-required="recipientType === 'Within Organisation'"  />
                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.Destination_AUR_KNOWN_AS.$invalid  && recipientType === 'Within Organisation'"  style="color: red">Please Select Recipient Name</span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Recipient Department <span style="color: red;">*</span></label>
            <input id="DD" name="DestinationDEPT" type="text" data-ng-model="BOOKPARCEL.DestinationDEPT"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Within Organisation'"  />
            <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationDEPT.$invalid  && recipientType === 'Within Organisation'" style="color: red">Recipient Department Cannot be Empty</span>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Recipient Contactno <span style="color: red;">*</span></label>
            <input id="DC" name="DestinationCONTACT" type="text" data-ng-model="BOOKPARCEL.DestinationCONTACT"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Within Organisation'"  />
            <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCONTACT.$invalid  && recipientType === 'Within Organisation'" style="color: red">Recipient Contactno Cannot be Empty</span>
        </div>
    </div>
</div>

<div class="row" ng-show="recipientType === 'Within Organisation'">
    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Recipient Email <span style="color: red;">*</span></label>
            <input id="DE" name="DestinationEMAIL" type="text" data-ng-model="BOOKPARCEL.DestinationEMAIL"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Within Organisation'"  />
            <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationEMAIL.$invalid  && recipientType === 'Within Organisation'" style="color: red">Recipient Email Cannot be Empty</span>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Recipient City <span style="color: red;">*</span></label>
            <input id="DECC" name="DestinationCITY" type="text" data-ng-model="BOOKPARCEL.DestinationCITY"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Within Organisation'"  />
            <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCITY.$invalid  && recipientType === 'Within Organisation'" style="color: red">Recipient City Cannot be Empty</span>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Recipient Pincode <span style="color: red;">*</span></label>
            <input id="DP" name="DestinationPINCODE" type="text" data-ng-model="BOOKPARCEL.DestinationPINCODE"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Within Organisation'"  />
            <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationPINCODE.$invalid  && recipientType === 'Within Organisation'" style="color: red">Recipient Pincode Cannot be Empty</span>
        </div>
    </div>

    <div class="col-md-3 col-sm-12 col-xs-12">
        <div class="form-group">
            <label>Recipient Country <span style="color: red;">*</span></label>
            <input id="DCTY" name="DestinationCOUNTRY" type="text" data-ng-model="BOOKPARCEL.DestinationCOUNTRY"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Within Organisation'"  />
            <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCOUNTRY.$invalid  && recipientType === 'Within Organisation'" style="color: red">Recipient Country Cannot be Empty</span>
        </div>

    </div>
                    </div>
                    <div class="row" ng-show="recipientType === 'Beyond Organisation'">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.RCL.$invalid}">
                                <label>Recipient Location<span style="color: red;">*</span></label>
                                <input id="RCL" name="RCL" type="text" data-ng-model="BOOKPARCEL.RCL" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1"  ng-required="recipientType === 'Beyond Organisation'" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.RCL.$invalid && recipientType === 'Beyond Organisation'" style="color: red">Please Enter Recipient Location</span>

                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.RecipientName.$invalid}">
                                <label>Recipient Name<span style="color: red;">*</span></label>
                                <input id="RecipientName" name="RecipientName" type="text" data-ng-model="BOOKPARCEL.RecipientName" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Beyond Organisation'" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.RecipientName.$invalid && recipientType === 'Beyond Organisation'" style="color: red">Please Enter Recipient Name</span>

                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.RecipientContactno.$invalid}">
                                <label>Recipient Contactno<span style="color: red;">*</span></label>
                                <input id="RecipientContactno" name="RecipientContactno" type="text" data-ng-model="BOOKPARCEL.RecipientContactno" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Beyond Organisation'" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.RecipientContactno.$invalid && recipientType === 'Beyond Organisation'" style="color: red">Please Enter Recipient Contactno</span>

                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.RecipientEmail.$invalid}">
                                <label>Recipient Email <span style="color: red;">*</span></label>
                                <input id="RecipientEmail" name="RecipientEmail" type="text" data-ng-model="BOOKPARCEL.RecipientEmail" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Beyond Organisation'" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.RecipientEmail.$invalid && recipientType === 'Beyond Organisation'" style="color: red">Please Enter Recipient Email</span>

                            </div>

                        </div>
                    </div>
                    <div class="row" ng-show="recipientType === 'Beyond Organisation'">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.RecipientCity.$invalid}">
                                <label>Recipient City<span style="color: red;">*</span></label>
                                <input id="RecipientCity" name="RecipientCity" type="text" data-ng-model="BOOKPARCEL.RecipientCity" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Beyond Organisation'" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.RecipientCity.$invalid && recipientType === 'Beyond Organisation'" style="color: red">Please Enter Recipient City</span>

                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.AWBPODNO.$invalid}">
                                <label>Recipient Pincode<span style="color: red;">*</span></label>
                                <input id="RecipientPincode" name="RecipientPincode" type="text" data-ng-model="BOOKPARCEL.RecipientPincode" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" ng-required="recipientType === 'Beyond Organisation'" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.RecipientPincode.$invalid && recipientType === 'Beyond Organisation'" style="color: red">Please Enter Recipient Pincode</span>

                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.RecipientCountry.$invalid}">
                                <label>Recipient Country<span style="color: red;">*</span></label>
                                <input id="RecipientCountry" name="RecipientCountry" type="text" data-ng-model="BOOKPARCEL.RecipientCountry" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1"  ng-required="recipientType === 'Beyond Organisation'"/>
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.RecipientCountry.$invalid && recipientType === 'Beyond Organisation'" style="color: red">Recipient Country</span>

                            </div>

                        </div>
                    </div>




                    <hr class="black-line" />
                   <%-- <div class="row">
                         <h3><strong>Parcel Information :</strong></h3>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Courier Agency<span style="color: red;">*</span></label>
                                <div>
                                    <div isteven-multi-select data-input-model="CourierAgencyList" data-output-model="CourierAgencyList.COURIER_NAME" button-label="icon COURIER_NAME" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon COURIER_NAME" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" ng-model="CourierAgencyList.COURIER_NAME">
                                    </div>
                                    <input type="text" data-ng-model="CourierAgencyList.COURIER_NAME" name="COURIER_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.COURIER_NAME.$invalid" style="color: red">Please Select Courier Agency</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.AWBPODNO.$invalid}">
                                <label>AWB/POD No<span style="color: red;">*</span></label>
                                <input id="DCC" name="AWBPODNO" type="text" data-ng-model="BOOKPARCEL.AWBPODNO"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.AWBPODNO.$invalid" style="color: red">Please Enter AWB/POD No</span>

                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Mode<span style="color: red;">*</span></label>
                                <div>
                                    <div isteven-multi-select data-input-model="CourierModeList" data-output-model="CourierModeList.COURIERMODE_NAME" button-label="icon COURIERMODE_NAME" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon COURIERMODE_NAME" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" ng-model="CourierModeList.COURIERMODE_NAME">
                                    </div>
                                    <input type="text" data-ng-model="CourierModeList.COURIERMODE_NAME" name="COURIERMODE_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.COURIERMODE_NAME.$invalid" style="color: red">Please Select Mode</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Parcel Category<span style="color: red;">*</span></label>
                                <div>
                                    <div isteven-multi-select data-input-model="CourieCategoryList" data-output-model="CourieCategoryList.PARCEL_NAME" button-label="icon PARCEL_NAME" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon PARCEL_NAME" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" ng-model="CourieCategoryList.PARCEL_NAME">
                                    </div>
                                    <input type="text" data-ng-model="CourieCategoryList.PARCEL_NAME" name="PARCEL_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.PARCEL_NAME.$invalid" style="color: red">Please Select Parcel Category</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Parcel Details<span style="color: red;">*</span></label>
                                <div>
                                    <div isteven-multi-select data-input-model="CourierDetailsList" data-output-model="CourierDetailsList.COURIERDETAILS_NAME" button-label="icon COURIERDETAILS_NAME" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon COURIERDETAILS_NAME" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single" ng-model="CourierDetailsList.COURIERDETAILS_NAME">
                                    </div>
                                    <input type="text" data-ng-model="CourierDetailsList.COURIERDETAILS_NAME" name="COURIERDETAILS_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.COURIERDETAILS_NAME.$invalid" style="color: red">Please Select ParcelDetails</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.INVOICENO.$invalid}">
                                <label>Invoice No<span style="color: red;">*</span></label>
                                <input id="INN" name="INVOICENO" type="text" data-ng-model="BOOKPARCEL.INVOICENO"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.INVOICENO.$invalid" style="color: red">Please Enter Invoice No</span>

                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.InvoiceAMOUNT.$invalid}">
                                <label>Invoice Amount<span style="color: red;">*</span></label>
                                <input id="INA" name="InvoiceAMOUNT" type="text" data-ng-model="BOOKPARCEL.InvoiceAMOUNT"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.InvoiceAMOUNT.$invalid" style="color: red">Please Enter Invoice Amount</span>

                            </div>

                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.WEIGHT.$invalid}">
                                <label>Weight<span style="color: red;">*</span></label>
                                <input id="WEI" name="WEIGHT" type="text" data-ng-model="BOOKPARCEL.WEIGHT"  class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.WEIGHT.$invalid" style="color: red">Please Enter Parcel Weight</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.NOOFPIECES.$invalid}">
                                <label>No.of Pieces<span style="color: red;">*</span></label>
                                <input id="NOOFP" name="NOOFPIECES" type="text" data-ng-model="BOOKPARCEL.NOOFPIECES"   required="" data-ng-readonly="ActionStatus==1" />
                                <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.NOOFPIECES.$invalid" style="color: red">Please Enter No.of Pieces</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.DNREMARKS.$invalid}">
                                <label>Delivery Note Remarks</label>
                                <input id="DCY" name="DNREMARKS" type="text" data-ng-model="BOOKPARCEL.DNREMARKS"  class="form-control"  data-ng-readonly="ActionStatus==1" />
                            </div>
                        </div>

                    </div>--%>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" />
                                <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
<%--                                <a class="btn btn-primary custom-button-color" href="javascript:history.back()">Back</a>--%>
                            </div>
                        </div>
                    </div>
                     <hr class="black-line" />
                    <div class="row">
                        <div class="clearfix">
                            <div class="col-md-12">
                                <div class="box">
                                    <div data-ag-grid="gridOptions" style="height: 250px; width: 50%;" class="ag-blue"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        </div>
        <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
        <script src="../../Scripts/Lodash/lodash.min.js"></script>
        <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
        <script src="../../Scripts/moment.min.js"></script>
        <script>
            var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
            var CompanySession = '<%= Session["COMPANYID"]%>';
        </script>
        <script src="../../SMViews/Utility.js"></script>
        <script src="../JS/BookParcel.js"></script>
</body>
</html>
