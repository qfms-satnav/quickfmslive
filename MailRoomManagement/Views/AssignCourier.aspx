﻿<%@ Page Language="C#" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        .black-line {
            border: 1px solid black;
        }
    </style>

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-viewport-wrapper.ag-layout-normal {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="EditParcelController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Assign Courier</h3>
            </div>


            
                                <div class="card">
                              <div class="row">
                        <div class="clearfix">
                            <div class="col-md-12">
                                <div class="box">
                                    <div 
                                        data-ag-grid="gridOptions" 
                                        style="height: 400px; width: auto;" 
                                        class="ag-blue" 
                                        ng-show="!ReqDetails">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>

                         <div class="card" ng-hide="!isCardVisible">
                              <form role="form" id="form1" name="BookParcel" data-valid-submit="Assign()" novalidate>
                             <div class="row">                          
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Employee ID / Name<span style="color: red;">*</span></label>
                                    <div>
                                        <div><isteven-multi-select data-input-model="DestinationUserList" data-output-model="DestinationUserList.AUR_KNOWN_AS" button-label="icon AUR_KNOWN_AS" data-is-disabled="EnableStatus==0"
                                            data-item-label="icon AUR_KNOWN_AS" data-tick-property="ticked"
                                            data-max-labels="1" data-selection-mode="single" data-on-item-click="GetParties()" ng-model="UserList.AUR_KNOWN_AS"></isteven-multi-select>
                                        </div>
                                        <input type="text" data-ng-model="DestinationUserList.AUR_KNOWN_AS" name="AUR_KNOWN_AS" style="display: none" required="" />
                                        <span class="error" data-ng-show="BOOKPARCEL.$submitted && BOOKPARCEL.AUR_KNOWN_AS.$invalid" style="color: red">Please Select Recipient Name</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.SENDERDEPT.$invalid}">
                                    <label>Email</label>
                                    <input id="SD" name="DestinationEMAIL" type="text" data-ng-model="BOOKPARCEL.DestinationEMAIL" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationEMAIL.$invalid" style="color: red">Email Cannot be Empty</span>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.SENDERCONTACT.$invalid}">
                                    <label>Department</label>
                                    <input id="SCN" name="DestinationDEPT" type="text" data-ng-model="BOOKPARCEL.DestinationDEPT" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationDEPT.$invalid" style="color: red">Department Cannot be Empty</span>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.SENDEREMAIL.$invalid}">
                                    <label>Contact No</label>
                                    <input id="SE" name="DestinationCONTACT" type="text" data-ng-model="BOOKPARCEL.DestinationCONTACT" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCONTACT.$invalid" style="color: red">Sender Contact No Cannot be Empty</span>

                                </div>
                            </div>
<%--                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Status<span style="color: red;">*</span></label>
                                    <div>
                                        <div><isteven-multi-select data-input-model="Status" data-output-model="selectedStatusE" button-label="STA_CODE" data-is-disabled="EnableStatus==0"
                                            data-item-label="STA_CODE" data-tick-property="ticked"  data-ng-change="onStatusChange()"
                                            data-max-labels="1" data-selection-mode="single"  ng-model="STA_CODE" ></isteven-multi-select>
                                        </div>
                                        <input type="text" data-ng-model="LocationList.LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BOOKPARCEL.$submitted && BOOKPARCEL.LCM_NAME.$invalid" style="color: red">Please Select Status</span>
                                    </div>
                                </div>
                            </div>  --%>
                            <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">
                                <input type="submit" value="Submit" class="btn btn-primary custom-button-color" ng-click="Assign()"/>
                            </div>
                        </div>
                    </div>
                        </div>
                                  </div>
</form>
</div>
 
                    <div id="ReqDetailsBlock" data-ng-model="ReqDetailsBlock" data-ng-show="ReqDetails">
                        <hr class="black-line" />
                        <h3><strong>Sender Details :</strong></h3>
                       
                        <hr class="black-line" />
                        <div class="row">
                            <h3><strong>Recipient Details :</strong></h3>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Recipient Location<span style="color: red;">*</span></label>
                                    <div>
                                        <div><isteven-multi-select data-input-model="DestinationLocationList" data-output-model="DestinationLocationList.LCM_NAME" button-label="icon LCM_NAME" data-is-disabled="EnableStatus==0"
                                            data-item-label="icon LCM_NAME" data-tick-property="ticked"
                                            data-max-labels="1" data-selection-mode="single" ng-model="DestinationLocationList.LCM_NAME" data-on-item-click="onBindRecipientUsers()"></isteven-multi-select>
                                        </div>
                                        <input type="text" data-ng-model="DestinationLocationList.LCM_NAME" name="LCM_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BOOKPARCEL.$submitted && BOOKPARCEL.LCM_NAME.$invalid" style="color: red">Please Select Recipient Location</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Recipient Name<span style="color: red;">*</span></label>
                                    <div>
                                        <div><isteven-multi-select data-input-model="DestinationUserList" data-output-model="DestinationUserList.AUR_KNOWN_AS" button-label="icon AUR_KNOWN_AS" data-is-disabled="EnableStatus==0"
                                            data-item-label="icon AUR_KNOWN_AS" data-tick-property="ticked"
                                            data-max-labels="1" data-selection-mode="single" data-on-item-click="GetParties()" ng-model="DestinationUserList.AUR_KNOWN_AS"></isteven-multi-select>
                                        </div>
                                        <input type="text" data-ng-model="DestinationUserList.AUR_KNOWN_AS" name="AUR_KNOWN_AS" style="display: none" required="" />
                                        <span class="error" data-ng-show="BOOKPARCEL.$submitted && BOOKPARCEL.AUR_KNOWN_AS.$invalid" style="color: red">Please Select Recipient Name</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.DestinationDEPT.$invalid}">
                                    <label>Recipient Department</label>
                                    <input id="DD" name="DestinationDEPT" type="text" data-ng-model="BOOKPARCEL.DestinationDEPT" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationDEPT.$invalid" style="color: red">Recipient Department Cannot be Empty</span>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.DestinationCONTACT.$invalid}">
                                    <label>Recipient Contactno.</label>
                                    <input id="DC" name="DestinationCONTACT" type="text" data-ng-model="BOOKPARCEL.DestinationCONTACT" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCONTACT.$invalid" style="color: red">Recipient Contactno Cannot be Empty</span>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.DestinationEMAIL.$invalid}">
                                    <label>Recipient Email</label>
                                    <input id="DE" name="DestinationEMAIL" type="text" data-ng-model="BOOKPARCEL.DestinationEMAIL" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationEMAIL.$invalid" style="color: red">Recipient Email Cannot be Empty </span>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.DestinationCITY.$invalid}">
                                    <label>Recipient City</label>
                                    <input id="DECC" name="DestinationCITY" type="text" data-ng-model="BOOKPARCEL.DestinationCITY" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCITY.$invalid" style="color: red">Recipient City Cannot be Empty</span>

                                </div>
                            </div>

                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.DestinationPINCODE.$invalid}">
                                    <label>Recipient Pincode</label>
                                    <input id="DP" name="DestinationPINCODE" type="text" data-ng-model="BOOKPARCEL.DestinationPINCODE" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationPINCODE.$invalid" style="color: red">Recipient Pincode Cannot be Empty</span>

                                </div>
                            </div>


                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.DestinationCOUNTRY.$invalid}">
                                    <label>Recipient Country</label>
                                    <input id="DCTY" name="DestinationCOUNTRY" type="text" data-ng-model="BOOKPARCEL.DestinationCOUNTRY" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.DestinationCOUNTRY.$invalid" style="color: red">Recipient Country Cannot be Empty</span>

                                </div>
                            </div>
                        </div>
                        <hr class="black-line" />
                        <div class="row">
                            <h3><strong>Parcel Information :</strong></h3>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Courier Agency<span style="color: red;">*</span></label>
                                    <div>
                                        <div><isteven-multi-select data-input-model="CourierAgencyList" data-output-model="CourierAgencyList.COURIER_NAME" button-label="icon COURIER_NAME" data-is-disabled="EnableStatus==0"
                                            data-item-label="icon COURIER_NAME" data-tick-property="ticked"
                                            data-max-labels="1" data-selection-mode="single" ng-model="CourierAgencyList.COURIER_NAME"></isteven-multi-select>
                                        </div>
                                        <input type="text" data-ng-model="CourierAgencyList.COURIER_NAME" name="COURIER_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BOOKPARCEL.$submitted && BOOKPARCEL.COURIER_NAME.$invalid" style="color: red">Please Select Courier Agency</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.AWBPODNO.$invalid}">
                                    <label>AWB/POD No</label>
                                    <input id="DCC" name="AWBPODNO" type="text" data-ng-model="BOOKPARCEL.AWBPODNO" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.AWBPODNO.$invalid" style="color: red">Please Enter AWB/POD No</span>

                                </div>

                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Mode<span style="color: red;">*</span></label>
                                    <div>
                                        <div><isteven-multi-select data-input-model="CourierModeList" data-output-model="CourierModeList.COURIERMODE_NAME" button-label="icon COURIERMODE_NAME" data-is-disabled="EnableStatus==0"
                                            data-item-label="icon COURIERMODE_NAME" data-tick-property="ticked"
                                            data-max-labels="1" data-selection-mode="single" ng-model="CourierModeList.COURIERMODE_NAME"></isteven-multi-select>
                                        </div>
                                        <input type="text" data-ng-model="CourierModeList.COURIERMODE_NAME" name="COURIERMODE_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BOOKPARCEL.$submitted && BOOKPARCEL.COURIERMODE_NAME.$invalid" style="color: red">Please Select Mode</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Parcel Category<span style="color: red;">*</span></label>
                                    <div>
                                        <div><isteven-multi-select data-input-model="CourieCategoryList" data-output-model="CourieCategoryList.PARCEL_NAME" button-label="icon PARCEL_NAME" data-is-disabled="EnableStatus==0"
                                            data-item-label="icon PARCEL_NAME" data-tick-property="ticked"
                                            data-max-labels="1" data-selection-mode="single" ng-model="CourieCategoryList.PARCEL_NAME"></isteven-multi-select>
                                        </div>
                                        <input type="text" data-ng-model="CourieCategoryList.PARCEL_NAME" name="PARCEL_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BOOKPARCEL.$submitted && BOOKPARCEL.PARCEL_NAME.$invalid" style="color: red">Please Select Parcel Category</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Parcel Details<span style="color: red;">*</span></label>
                                    <div>
                                        <div><isteven-multi-select data-input-model="CourierDetailsList" data-output-model="CourierDetailsList.COURIERDETAILS_NAME" button-label="icon COURIERDETAILS_NAME" data-is-disabled="EnableStatus==0"
                                            data-item-label="icon COURIERDETAILS_NAME" data-tick-property="ticked"
                                            data-max-labels="1" data-selection-mode="single" ng-model="CourierDetailsList.COURIERDETAILS_NAME"></isteven-multi-select>
                                        </div>
                                        <input type="text" data-ng-model="CourierDetailsList.COURIERDETAILS_NAME" name="COURIERDETAILS_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="BOOKPARCEL.$submitted && BOOKPARCEL.COURIERDETAILS_NAME.$invalid" style="color: red">Please Select ParcelDetails</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.INVOICENO.$invalid}">
                                    <label>Invoice No</label>
                                    <input id="INN" name="INVOICENO" type="text" data-ng-model="BOOKPARCEL.INVOICENO" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.INVOICENO.$invalid" style="color: red">Please Enter Invoice No</span>

                                </div>

                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.InvoiceAMOUNT.$invalid}">
                                    <label>Invoice Amount</label>
                                    <input id="INA" name="InvoiceAMOUNT" type="text" data-ng-model="BOOKPARCEL.InvoiceAMOUNT" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.InvoiceAMOUNT.$invalid" style="color: red">Please Enter Invoice Amount</span>

                                </div>

                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.WEIGHT.$invalid}">
                                    <label>Weight</label>
                                    <input id="WEI" name="WEIGHT" type="text" data-ng-model="BOOKPARCEL.WEIGHT" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.WEIGHT.$invalid" style="color: red">Please Enter Parcel Weight</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.NOOFPIECES.$invalid}">
                                    <label>No.of Pieces</label>
                                    <input id="NOOFP" name="NOOFPIECES" type="text" data-ng-model="BOOKPARCEL.NOOFPIECES" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                    <span class="error" data-ng-show="BookParcel.$submitted && BookParcel.NOOFPIECES.$invalid" style="color: red">Please Enter No.of Pieces</span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': BookParcel.$submitted && BookParcel.DNREMARKS.$invalid}">
                                    <label>Delivery Note Remarks</label>
                                    <input id="DCY" name="DNREMARKS" type="text" data-ng-model="BOOKPARCEL.DNREMARKS" class="form-control" autofocus required="" data-ng-readonly="ActionStatus==1" />
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': frmPRMUpl.$submitted && frmPRMUpl.UPLFILE2.$invalid}">
                                    <label>Attachment(if any)</label>
                                    <input type="file" name="UPLFILE2" id="FileUpl2" accept=".pdf" />
                                    <input type="hidden" value="{{Login}}" name="Login" />
                                    <input type="hidden" value="{{document.DFDC_DDT_CODE}}" name="DocType" />
                                </div>
                            </div>

                            <div class="col-md-2 col-sm-12 col-xs-12">
                                <input type="button" id="btnUpload" data-ng-click="UploadFile()" class="btn btn-primary custom-button-color" value="Upload" />
               </div>                                   
             <div class="form-group">
                 <label>Status<span style="color: red;">*</span></label>
                 <div>
                <isteven-multi-select  
                    data-input-model="Status" 
                    data-output-model="selectedStatusE" 
                    button-label="STA_CODE" 
                    data-is-disabled="EnableStatus==0" 
                    data-item-label="STA_CODE" 
                    data-tick-property="ticked" 
                    data-max-labels="1" 
                    data-selection-mode="single">
                </isteven-multi-select>                
                  <span class="error" style="color: red" 
                             data-ng-show="BOOKPARCEL.$submitted && !BOOKPARCEL.Status">
                         Please select a status.
                     </span>
                 </div>
             </div>                  
                             <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                    <input type="submit" value="Update" class="btn btn-primary custom-button-color" data-ng-click="Update()" />
                                    <input type="reset" value="Clear" class="btn btn-primary custom-button-color" data-ng-click="EraseData()" />
                                    <a class="btn btn-primary custom-button-color" href="#" ng-click="onBackClick()">Back</a>
                                </div>
                            </div>
                        </div>
                        <hr class="black-line" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/AssignCourier.js"></script>
</body>
</html>
