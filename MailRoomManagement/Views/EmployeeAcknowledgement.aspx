﻿<%@ Page Language="C#" %>

<!DOCTYPE html>
<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Facility Management Services::a-mantra </title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.css" rel="stylesheet" />
</head>
<body data-ng-controller="GetEmployeAcknowledgeController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel-title="Approved List" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Employee Acknowledgment</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form role="form" id="Form" name="APRLIST" novalidate>
                                <div class="col-md-12">
                                    <div class="box-footer text-right">
                                    </div>
                                    <div data-ag-grid="gridOptions" class="ag-blue" style="height: 300px; width: auto"></div>
                                </div>

                                <div class="box-footer text-right">
                                    <div class="form-group">
                                        <button type="submit" id="btnsubmit" class="btn btn-primary custom-button-color"  data-ng-click="UpdateStatus()">Acknowledge</button>
                                        <span style="margin-right: 10px;"></span>
                                        <%--<button type="reset" id="btnsendback" ng-hide="Approval" class="btn btn-danger custom-button-color" data-ng-click="UpdateStatusReject()">Reject</button>
                                        <span style="margin-right: 10px;"></span>--%>
                                    </div>
                                </div>
                                
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <%=ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script defer src="../../../../BootStrapCSS/Bootstrapswitch/js/highlight.js"></script>
    <script defer src="../../../../BootStrapCSS/Bootstrapswitch/js/main.js"></script>
    <script defer src="../../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script defer src="../../../../SMViews/Utility.js"></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js"></script>
    <script defer src="../../../../Scripts/Lodash/lodash.min.js"></script>
    <script defer src="../../../../BlurScripts/BlurJs/moment.js"></script>
    <script src="../JS/EmployeeAcknowledgement.js"></script>
</body>
</html>
