﻿app.service("WorkRequestService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetGridTotalRequest = function (Property) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/WorkRequest/GetWR', Property)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetPropertyDetails = function (Property) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/WorkRequest/GetPropertyDetails', Property)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetCompleteWR = function (Property) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/WorkRequest/GetCompleteWR', Property)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);
app.controller('WorkRequestController', ['$scope', '$q', '$http', 'WorkRequestService', 'UtilityService','$timeout', function ($scope, $q, $http, WorkRequestService, UtilityService, $timeout) {
    $scope.WorkReq = {};
    $scope.WorkRequest = {}
    $scope.GridVisible = true
    $scope.PropertyDetailsVisible = false;
    $scope.DocTypeVisible = 0;
    $scope.Type = [];
    $scope.ReqCount = {};
    $scope.CompanyVisible = 0;
    $scope.EnableStatus = 0;
    $scope.Company = [];
    $scope.columnDefs = [
          { headerName: "Property Name", field: "PM_NAME", width: 165, cellClass: 'grid-align', template: '<a ng-click="ShowReqDetails(data)">{{data.PM_NAME}}</a>', filter: 'text', pinned: 'left', suppressMenu: true },
          { headerName: "Property Code", field: "PM_CODE", cellClass: 'grid-align', width: 200 },
          { headerName: "Total Requests", field: "TOTAL_WR", width: 100, cellClass: 'grid-align', width: 190, }];


    $scope.columnDefsProperty = [
          { headerName: "Request Id", field: "PM_WR_REQ_ID", width: 165, cellClass: 'grid-align', template: '<a ng-click="ShowPopup(data)">{{data.PM_WR_REQ_ID}}</a>', filter: 'text', pinned: 'left', suppressMenu: true },
          { headerName: "Raised By", field: "PM_WR_CREATED_BY", cellClass: 'grid-align', width: 200 },
          { headerName: "Status", field: "STA_TITLE", width: 100, cellClass: 'grid-align', width: 190, },
          { headerName: "Requested Date", field: "PM_WR_CREATED_DT", template: '<span>{{data.PM_WR_CREATED_DT | date:"dd MMM, yyyy"}}</span>', width: 100, cellClass: 'grid-align', width: 190, },
          { headerName: "Last Updated Date", field: "PM_UWR_CREATED_DT", template: '<span>{{data.PM_UWR_CREATED_DT | date:"dd MMM, yyyy"}}</span>', width: 100, cellClass: 'grid-align', width: 190 }];

    $scope.WorkReqDefs = [
          { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 200 },
          { headerName: "Location", field: "LCM_NAME", width: 100, cellClass: 'grid-align', width: 190, },
          { headerName: "Property", field: "PM_PPT_NAME", width: 100, cellClass: 'grid-align', width: 190, },
          { headerName: "Property Type", field: "PN_PROPERTYTYPE", width: 100, cellClass: 'grid-align', width: 190, },
          { headerName: "Vendor", field: "AVR_NAME", width: 100, cellClass: 'grid-align', width: 190, },
          { headerName: "Vendor Address", field: "PM_WR_VEN_ADDRESS", width: 100, cellClass: 'grid-align', width: 190, },
          { headerName: "Vendor Phone", field: "PM_WR_VEN_PH_NO", width: 100, cellClass: 'grid-align', width: 190, }];

    setTimeout(function () {
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                $scope.Company = response.data;
                //$scope.WorkReq.CNP_NAME = parseInt(CompanySession);
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.WorkRequest.CNP_NAME.push(a);
                });
                if (CompanySession == "1") { $scope.EnableStatus = 1; } 
                else { $scope.EnableStatus = 0; }
                $scope.LoadData();
            }

        });
    }, 500);

    $scope.LoadData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,

            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
            CNP_NAME: $scope.WorkRequest.CNP_NAME[0].CNP_ID
        };
        progress(0, 'Loading...', true);
        $scope.GridVisiblity = true;
        WorkRequestService.GetGridTotalRequest(params).then(function (data) {
            $scope.gridata = data;
            console.log(data);
            if ($scope.gridata.data == null) {
                $("#btNext").attr("disabled", true);
                $scope.gridOptions.api.setRowData([]);
                $scope.PropertyDetailsVisible = false;
                
            }
            else {
                $scope.gridOptions.api.setRowData([]);
                $scope.gridOptions.api.setRowData($scope.gridata.data);
            }
            progress(0, 'Loading...', false);
        });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }

    function onFilterChanged(value) {
        $scope.gridOptionsProperty.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $("#filtertxt1").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.gridOptionsProperty = {
        columnDefs: $scope.columnDefsProperty,
        enableFilter: true,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptionsProperty.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    $scope.PopOptions = {
        columnDefs: $scope.WorkReqDefs,
        enableFilter: true,
        angularCompileRows: true,
        enableCellSelection: false,
        enableColResize: true,
        rowData: null,
        onReady: function () {
            $scope.PopOptions.api.sizeColumnsToFit()
        }
    }
    $scope.ShowReqDetails = function (data) {
        $scope.PropertyDetailsVisible = true;
        var PropObj = { Property: data.PM_CODE };
        WorkRequestService.GetPropertyDetails(PropObj).then(function (data) {
            $scope.Propgridata = data;
            if ($scope.Propgridata == null) {
                $scope.gridOptionsProperty.api.setRowData([]);
            }
            else {
                $scope.gridOptionsProperty.api.setRowData($scope.Propgridata);
            }
            progress(0, 'Loading...', false);
        });
    }

    $scope.ShowPopup = function (data) {
        var obj = { RequestId: data.PM_WR_REQ_ID }
        $scope.ReqCount = obj;
        $("#historymodal").modal('show');

        $('#historymodal').on('shown.bs.modal', function () {
            WorkRequestService.GetCompleteWR(obj).then(function (data) {
                progress(0, 'Loading...', true);
                $scope.popdata = data;
                $scope.PopOptions.api.setRowData($scope.popdata);
                progress(0, 'Loading...', false);
            });
        });
    }

    $scope.GenReport = function (WorkReq, Type) {
        
        console.log($scope.ReqCount);
        var Req = $scope.ReqCount.RequestId;
        console.log(Req);
        var TypeObject = {
            Type: Type,
            RequestId: Req
        };
        progress(0, 'Loading...', true);
        if ($scope.PopOptions.api.isAnyFilterPresent($scope.WorkReqDefs)) {
            if (WorkReq.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/WorkRequest/GetAllProperties',
                method: 'POST',
                data: TypeObject,
                responseType: 'arraybuffer'

            }).success(function (data, status, headers, config) {
                var file = new Blob([data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'WorkRequest.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }).error(function (data, status, headers, config) {

            });
        };
    }
    //setTimeout(function () {
    //    $scope.LoadData();
    //}, 1000);
}]);