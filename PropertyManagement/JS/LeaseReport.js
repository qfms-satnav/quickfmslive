﻿app.service("LeaseReportService", ['$http', '$q','UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseReport/GetGriddata', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };

    this.GetGrid = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseReport/GetGrid', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;

          });
    };

    this.GetDetailsOnSelection = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseReport/GetDetailsOnSelection/', data)
          .then(function (response) {
              deferred.resolve(response.data);
              return deferred.promise;
          }, function (response) {
              deferred.reject(response);
              return deferred.promise;
          });
    };
}]);

app.controller('LeaseReportController', ['$scope', '$q', '$http', 'LeaseReportService', 'UtilityService','$timeout', function ($scope, $q, $http, LeaseReportService, UtilityService, $timeout) {
  
    $scope.GridVisiblity = false;
  
    $scope.Type = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.DocTypeVisible = 0;
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];

    UtilityService.getCountires(2).then(function (response) {
        if (response.data != null) {
            $scope.Country = response.data;
            angular.forEach($scope.Country, function (value, key) {
                value.ticked = true;
            });
            UtilityService.getCities(2).then(function (response) {
                if (response.data != null) {
                    $scope.City = response.data;
                    angular.forEach($scope.City, function (value, key) {
                        value.ticked = true;
                    });
                    UtilityService.getLocations(2).then(function (response) {
                        if (response.data != null) {
                            $scope.Locations = response.data;
                            angular.forEach($scope.Locations, function (value, key) {
                                value.ticked = true;
                            });
                        }
                    });
                }
            });

        }
    });
    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.LeaseRep.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.LeaseRep.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.LeaseRep.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.LeaseRep.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.LeaseRep.City = $scope.City;
        $scope.getLocationsByCity();
    }


    $scope.locSelectAll = function () {
        $scope.LeaseRep.Locations = $scope.Locations;
        $scope.LocationChange();
    }

    $scope.LocationChange = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.LeaseRep.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.LeaseRep.City[0] = cty;
            }
        });
    }



  

    //$scope.columnDefs = [
    //    { headerName: "Lease-Property Type", field: "PROP_TYPE", width: 200, cellClass: 'grid-align' },
    //    { headerName: "Property Name", field: "PRP_NAME", width: 100, cellClass: 'grid-align', width: 200, },
    //    { headerName: "Entity", field: "CHE_NAME", width: 100, cellClass: 'grid-align', width: 200, },
    //    { headerName: "Lease Start Date", field: "LEASE_SDATE", template: '<span>{{data.LEASE_SDATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 100, },
    //    { headerName: "Lease End Date", field: "LEASE_EDATE", template: '<span>{{data.LEASE_EDATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 100 },
    //    { headerName: "City", field: "STATE", cellClass: 'grid-align', width: 100 },
    //    { headerName: "Location Code", field: "branch_code", cellClass: 'grid-align', width: 120 },
    //    { headerName: "Basic Rent", field: "MON_RENT", cellClass: 'grid-align', width: 100, suppressMenu: true, },
    //    { headerName: "Maintainance", field: "MAINT_CHRG", cellClass: 'grid-align', width: 100 },
    //    { headerName: "Amount", field: "TOTAL_RENT", cellClass: 'grid-align', width: 100 },
    //    { headerName: "Landlord No.", field: "LL_NUM", cellClass: 'grid-align', width: 120 },
    //    { headerName: "Security Deposit", field: "SEC_DEPOSIT", cellClass: 'grid-align', width: 100 },
    //    { headerName: "Broker Name", field: "BRO_NAME", cellClass: 'grid-align', width: 150 },
    //    //{ headerName: "Broker Fee", field: "BRO_FEE", cellClass: 'grid-align', width: 100, suppressMenu: true, },
    //    { headerName: "Landlord Name", field: "LAND_NAME", cellClass: 'grid-align', width: 150 },
    //    { headerName: "Landlord Address", field: "LAND_ADDR", cellClass: 'grid-align', width: 200, suppressMenu: true, },
    //    { headerName: "Landlord Number", field: "landlord_phone", cellClass: 'grid-align', width: 100, suppressMenu: true, },
    //    { headerName: "Landlord Gst No.", field: "LAND_GST", cellClass: 'grid-align', width: 100 },
    //    { headerName: "Landlord Rent", field: "LAND_RENT", cellClass: 'grid-align', width: 100 },
    //    { headerName: "Landlord Security Deposit", field: "LAND_SEC_DEP", cellClass: 'grid-align', width: 120 },
    //    { headerName: "Lease Payment Terms", field: "lease_payment_terms", cellClass: 'grid-align', width: 120 },

    //];

    $scope.columnDefs = [
        { headerName: "Lease-Property Type", field: "PROP_TYPE", width: 200, cellClass: 'grid-align' },
        { headerName: "Branch Code", field: "Property_Code", width: 100, cellClass: 'grid-align', width: 200, },
        { headerName: "Property Name", field: "PRP_NAME", width: 100, cellClass: 'grid-align', width: 200, },
        { headerName: "Property Address", field: "PPT_ADDRESS", width: 100, cellClass: 'grid-align', width: 200, },
        { headerName: "Carpet Area", field: "CRAPET_AREA", width: 100, cellClass: 'grid-align', width: 200, },
        { headerName: "BuiltUp Area", field: "BUILT_AREA", width: 100, cellClass: 'grid-align', width: 200, },
        { headerName: "Entity", field: "CHE_NAME", width: 100, cellClass: 'grid-align', width: 200, },
        { headerName: "Lease Start Date", field: "LEASE_SDATE", template: '<span>{{data.LEASE_SDATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 100, },
        { headerName: "Lease End Date", field: "LEASE_EDATE", template: '<span>{{data.LEASE_EDATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 100 },
        { headerName: "State", field: "STATE_NAME", cellClass: 'grid-align', width: 100 },
        { headerName: "Zone", field: "ZONE_NAME", cellClass: 'grid-align', width: 100 },
        { headerName: "City", field: "STATE", cellClass: 'grid-align', width: 100 },
        { headerName: "Location Code", field: "branch_code", cellClass: 'grid-align', width: 120 },
        { headerName: "Location Name", field: "LOC_NAME", cellClass: 'grid-align', width: 120 },
        { headerName: "Floor Name", field: "FLOOR_NAME", cellClass: 'grid-align', width: 120 },
        { headerName: "Basic Rent", field: "MON_RENT", cellClass: 'grid-align', width: 100, suppressMenu: true, },
        { headerName: "Maintainance", field: "MAINT_CHRG", cellClass: 'grid-align', width: 100 },
        { headerName: "Amount", field: "TOTAL_RENT", cellClass: 'grid-align', width: 100 },
        { headerName: "Landlord No.", field: "LL_NUM", cellClass: 'grid-align', width: 120 },
        { headerName: "Security Deposited in Months", field: "SEC_DEPOSITMONTH", cellClass: 'grid-align', width: 100 },
        { headerName: "LockinPeriod(In Months)", field: "LOCK_IN", cellClass: 'grid-align', width: 100 },
        { headerName: "NoticePeriod(In Months)", field: "NOTICE_PERIOD", cellClass: 'grid-align', width: 100 },
        { headerName: "Rent Free Period", field: "RENTFREE_PERIOD", cellClass: 'grid-align', width: 100 },
        { headerName: "Security Deposit", field: "SEC_DEPOSIT", cellClass: 'grid-align', width: 100 },
        { headerName: "Broker Name", field: "BRO_NAME", cellClass: 'grid-align', width: 150 },
        //{ headerName: "Broker Fee", field: "BRO_FEE", cellClass: 'grid-align', width: 100, suppressMenu: true, },
        { headerName: "No of Landlords in Premise", field: "PM_LES_NO_OF_LL", cellClass: 'grid-align', width: 150 },
        { headerName: "Landlord(Single/Multiple)", field: "LANDLORD_NO", cellClass: 'grid-align', width: 150 },
        { headerName: "Landlord Name", field: "LAND_NAME", cellClass: 'grid-align', width: 150 },
        { headerName: "Landlord Address", field: "LAND_ADDR", cellClass: 'grid-align', width: 200, suppressMenu: true, },
        { headerName: "Landlord State", field: "LL_STATE", cellClass: 'grid-align', width: 200, suppressMenu: true, },
        { headerName: "Landlord City", field: "LL_CITY", cellClass: 'grid-align', width: 200, suppressMenu: true, },
        { headerName: "Landlord Pincode", field: "LL_PIN", cellClass: 'grid-align', width: 200, suppressMenu: true, },
        { headerName: "Landlord Mobile Number", field: "LANDLORD_NUMBER", cellClass: 'grid-align', width: 100, suppressMenu: true, },
        { headerName: "Landlord Email ID", field: "LANDLORD_EMAIL", cellClass: 'grid-align', width: 100, suppressMenu: true, },
        { headerName: "Landlord PANNO", field: "LANDLORD_PAN", cellClass: 'grid-align', width: 100 },
        { headerName: "Landlord Gst", field: "LAND_GST", cellClass: 'grid-align', width: 100 },
        { headerName: "Landlord Gst No", field: "LANDLORD_GST_NO", cellClass: 'grid-align', width: 100 },
        { headerName: "Landlord Rent", field: "LAND_RENT", cellClass: 'grid-align', width: 100 },
        { headerName: "Landlord Security Deposit", field: "LAND_SEC_DEP", cellClass: 'grid-align', width: 120 },
        { headerName: "Landlord Maintenance", field: "MAINT_CHARG", cellClass: 'grid-align', width: 120 },
        { headerName: "Landlord Aminities", field: "AMNI_CHARG", cellClass: 'grid-align', width: 120 },
        { headerName: "Landlord Bank Name", field: "LANDLORD_BANK", cellClass: 'grid-align', width: 120 },
        { headerName: "Landlord Account Number", field: "LANDLORD_ACCOUNT", cellClass: 'grid-align', width: 120 },
        { headerName: "Landlord IFSC Code", field: "LANDLORD_IFSC", cellClass: 'grid-align', width: 120 },
        { headerName: "Lease Payment Terms", field: "lease_payment_terms", cellClass: 'grid-align', width: 120 },
        { headerName: "Vendor Code", field: "VENDER_CODE", cellClass: 'grid-align', width: 120 },
        { headerName: "Rental Payment(Advance/Succeeding)", field: "PM_LL_ADDRESS2", cellClass: 'grid-align', width: 120 },
        { headerName: "Escalation(Yes/No)", field: "ESC_1", cellClass: 'grid-align', width: 120 },
        { headerName: "Lease Remarks", field: "STATUS1", cellClass: 'grid-align', width: 120 },
        { headerName: "Branch Status", field: "Branch_Status", cellClass: 'grid-align', width: 120 }

    ];
            
    setTimeout(function () {
        progress(0, 'Loading...', true);
        UtilityService.GetCompanies().then(function (response) {
            if (response.data != null) {
                console.log(CompanySession);
                $scope.Company = response.data;
                angular.forEach($scope.Company, function (value, key) {
                    var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                    a.ticked = true;
                    $scope.LeaseRep.CNP_NAME.push(a);
                });
                if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                else { $scope.CompanyVisible = 1; }
                $scope.LoadData();
            }
            progress(0, 'Loading...', false);
        });

    }, 500);
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,

            getRows: function (params) {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.LeaseRep.CNP_NAME[0].CNP_ID,
                    FromDate: $scope.LeaseRep.FromDate,
                    ToDate: $scope.LeaseRep.ToDate,
                    loclst: $scope.LeaseRep.Locations

                };

                LeaseReportService.GetGriddata(params).then(function (data) {
                    $scope.gridata = data.data;
                    if ($scope.gridata == null) {
                        $("#btNext").attr("disabled", true);
                        $scope.GridVisiblity = false;
                        $scope.gridOptions.api.setRowData([]);
                    }
                    else {
                        $scope.GridVisiblity = true;
                        $scope.gridOptions.api.setRowData($scope.gridata);
                    }
                    progress(0, 'Loading...', false);
                }, function (error) {
                    console.log(error);
                });
            }
        }
        $scope.gridOptions.api.setDatasource(dataSource);
    };


    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})

    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableFilter: false,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //},
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };  
 

    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Lease-Property Type", key: "PROP_TYPE" }, { title: "Property Name", key: "PRP_NAME" }, { title: "Lease Start Date", key: "LEASE_SDATE" }, { title: "Lease End Date", key: "LEASE_EDATE" }, { title: "Monthly Rent", key: "MONTHLY_RENT" }, { title: "Security Deposit", key: "SEC_DEPOSIT" }, { title: "Broker Name", key: "BRO_NAME" }, { title: "Broker Fee", key: "BRO_FEE" }, { title: "Landlord Name", key: "LAND_NAME" }, { title: "Landlord Address", key: "LAND_ADDR" }, { title: "Landlord Rent", key: "LAND_RENT" }, { title: "Landlord Security Deposit", key: "LAND_SEC_DEP" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("LeaseReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "LeaseReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (LeaseRep, Type) {
        console.log($scope.gridata);
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);
        var params = {
            SearchValue: searchval,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: $scope.gridata[0].OVERALL_COUNT,
            CNP_NAME: $scope.LeaseRep.CNP_NAME[0].CNP_ID,
            FromDate: $scope.LeaseRep.FromDate,
            ToDate: $scope.LeaseRep.ToDate,
            loclst: $scope.LeaseRep.Locations,
            Type: Type

        };
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (LeaseRep.Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/LeaseReport/GetGrid',
                method: 'POST',
                data: params,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });

                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
              
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'LeaseReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }), function (error,data, status, headers, config) {

            };
        };
    }
    setTimeout(function () {
        $scope.LoadData()
    }, 800);

}]);