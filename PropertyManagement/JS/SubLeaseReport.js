﻿app.service("SubLeaseReportService", function ($http, $q, UtilityService) {

    this.GetGriddata = function (SubLease) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/SubLeaseReport/GetGriddata', SubLease)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});
app.controller('SubLeaseReportController', function ($scope, $q, $http, SubLeaseReportService, UtilityService, $timeout, $filter) {


    $scope.SubLease = {};
    $scope.DocTypeVisible = 0;

    UtilityService.GetCompanies().then(function (response) {
        if (response.data != null) {
            $scope.Company = response.data;
            angular.forEach($scope.Company, function (value, key) {
                value.ticked = true;
            });
        }

    });

    $scope.Pageload = function () {


        UtilityService.getCities(2).then(function (response) {
            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getLocations(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Locations = response.data;
                        angular.forEach($scope.Locations, function (value, key) {
                            value.ticked = true;
                        })
                        setTimeout(function () {
                            $scope.LoadData();
                        }, 500);
                    }
                });
            }
        });

    }

    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var params = {
            cpylst: $scope.SubLease.Company,
            loclst: $scope.SubLease.Locations

        };
        console.log(params);
        SubLeaseReportService.GetGriddata(params).then(function (data) {
            $scope.gridata = data.data;
            console.log(data.data);
            if ($scope.gridata == null) {
                $scope.GridVisiblity = false;
                $scope.gridOptions.api.setRowData([]);
            }
            else {
                $scope.GridVisiblity = true;
                $scope.gridOptions.api.setRowData($scope.gridata);
            }
            progress(0, 'Loading...', false);
        }, function (error) {
            console.log(error);
        });

    }


    $scope.ctySelectAll = function () {
        $scope.SubLease.City = $scope.City;
        $scope.getLocationsByCity();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.SubLease.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.locSelectAll = function () {
        $scope.SubLease.Locations = $scope.Locations;

    }

    $scope.cpySelectAll = function () {
        $scope.SubLease.Company = $scope.Company;

    }

    $scope.columnDefs = [
        { headerName: "Main Entity", field: "MAIN_ENTITY", width: 100, cellClass: 'grid-align', width: 80 },
        { headerName: "Sub Entity", field: "CNP_NAME", width: 100, cellClass: 'grid-align', width: 80 },
       
        { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 100 },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 150 },
        { headerName: "Property Name", field: "PM_PPT_NAME", cellClass: 'grid-align', width: 100 },
        { headerName: "Lease ID", field: "PM_SLA_PM_LR_REQ_ID", cellClass: 'grid-align', width: 100, suppressMenu: true, },
        { headerName: "SubLease ID", field: "PM_SLA_SUB_LES_REQ_ID", cellClass: 'grid-align', width: 200, suppressMenu: true, },
        { headerName: "Agreement Type", field: "PM_LES_AGR_TYPE", cellClass: 'grid-align', width: 200, suppressMenu: true, },
        { headerName: "SubLease Start Date", field: "PM_SLA_AGREE_ST_DT", cellClass: 'grid-align', width: 150, suppressMenu: true, },
        { headerName: "SubLease End Date", field: "PM_SLA_AGREE_END_DT", cellClass: 'grid-align', width: 150, suppressMenu: true, },
        { headerName: "Total Area", field: "PM_AR_CARPET_AREA", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum' },
        { headerName: "SubLease Area", field: "PM_SLA_AREA_OR_SEATING", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum' },
        { headerName: "SubLease Cost", field: "PM_SLA_COST", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum' },
        { headerName: "Maintenance Cost", field: "PM_SLA_MAINT_CHARGES", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum' },
        { headerName: "Total Cost", field: "Total", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum' },
        { headerName: "Pay Term", field: "PM_SLA_PAY_TERMS", cellClass: 'grid-align', width: 60, suppressMenu: true, aggFunc: 'sum' },
        { headerName: "Status", field: "STA_TITLE", cellClass: 'grid-align', width: 200 },
        { headerName: "Remarks", field: "PM_SLA_APPR_REMARKS", cellClass: 'grid-align', width: 200 },


    ];



    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })



    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: false,
        groupHideGroupColumns: true,

    };


    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Company", key: "CNP_NAME" }, { title: "City", key: "CTY_NAME" },
        { title: "Location", key: "LCM_NAME" }, { title: "Property Name", key: "PM_PPT_NAME" }, { title: "Lease ID", key: "PM_SLA_PM_LR_REQ_ID" }, { title: "SubLease ID", key: "PM_SLA_SUB_LES_REQ_ID" },
        { title: "SubLease Start Date", key: "PM_SLA_AGREE_ST_DT" }, { title: "SubLease Start Date", key: "PM_SLA_AGREE_END_DT" }, { title: "Total Area", key: "PM_AR_CARPET_AREA" }, { title: "SubLease Area", key: "PM_SLA_AREA_OR_SEATING" },
        { title: "SubLease Cost", key: "PM_SLA_COST" }, { title: "Maintenance Cost", key: "PM_SLA_MAINT_CHARGES" }, { title: "Total Cost", key: "Total" }];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("SubLeaseReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "SubLeaseReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (SubLease, Type) {

        progress(0, 'Loading...', true);
        eaobj = {};
        eaobj.cpylst = SubLease.Company;
        eaobj.Type = Type;
        eaobj.loclst = SubLease.Locations;

        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({

                url: UtilityService.path + '/api/SubLeaseReport/GetSubLeaseData',
                method: 'POST',
                data: eaobj,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {
                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'SubLeaseReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            }),function(error,data, status, headers, config) {

            };
        };
    }

    $scope.Pageload();
    //setTimeout(function () {
    //    $scope.LoadData();
    //}, 100);

});
