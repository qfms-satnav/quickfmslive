﻿app.service("LeaseRentalandEscaltionService", function ($http, $q, UtilityService) {
    this.GetPoprtyData = function (data) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LeaseRentalandEscaltion/GetPoprtyData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getProjects = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LeaseRentalandEscaltion/getProjects')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetGriddata = function (param) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseRentalandEscaltion/GetGriddata', param)
            .then(function (response) {
                // console.log(response);
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    }
    this.GetReqType = function (data) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LeaseRentalandEscaltion/GetReqType', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});
app.controller('LeaseRentalandEscaltionController', function ($scope, $q, $http, LeaseRentalandEscaltionService, UtilityService,
    $timeout, $filter) {

    var LREC = this;
    $scope.downloadExcel = false;

    $scope.ProReqType = [];
    $scope.Property = [];
    $scope.DocTypeVisible = 0;
    $scope.display = 1;
    $scope.GridOptionsArr = [];
    $scope.RentalGridOptionArr = [];

    // $("#form_date").data("DateTimePicker").date(null);


    LeaseRentalandEscaltionService.GetPoprtyData().then(function (response) {
        if (response.data != null) {
            $scope.Property = response.data;
            angular.forEach($scope.Property, function (value, key) {
                //value.ticked = true;
            });
        };

        LeaseRentalandEscaltionService.GetReqType().then(function (response1) {
            if (response1.data != null) {
                $scope.ProReqType = response1.data;
                console.log($scope.ProReqType);
                angular.forEach($scope.ProReqType, function (value, key) {
                    //value.ticked = true;
                });
            }


        });


    });



    $scope.columnDefs = [
        { headerName: "Lease name", field: "LEASE_NAME", cellClass: 'grid-align', width: 130, },
        { headerName: "Entity", field: "SUB_GROUP", cellClass: 'grid-align', width: 100, },
        { headerName: "Country", field: "CNY_NAME", width: 100, cellClass: 'grid-align', },
        //{ headerName: "State", field: "STATE", cellClass: 'grid-align', suppressMenu: true, width: 100 },
        { headerName: "City", field: "CTY_NAME", cellClass: 'grid-align', width: 100, suppressMenu: false },
        { headerName: "Location", field: "LCM_NAME", cellClass: 'grid-align', width: 130, suppressMenu: false },
        { headerName: "Branch Code", field: "branch_code", width: 100 },
        { headerName: "Property name", field: "PM_PPT_NAME", cellClass: 'grid-align', width: 130, },
        { headerName: "Property Address", field: "PM_PPT_ADDRESS", cellClass: 'grid-align', width: 130, },
        { headerName: "Property Type", field: "PN_PROPERTYTYPE", cellClass: 'grid-align', width: 100 },
        { headerName: "Request/Type of Agreement", field: "pm_ppt_req_type", cellClass: 'grid-align', width: 100 },
        { headerName: "Agreement Type", field: "AGREEMENT_TYPE", cellClass: 'grid-align', width: 90, },
        { headerName: "Pooja/Inauguration Date", field: "PPT_ESTD", template: '<span>{{data.PPT_ESTD | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },

        //{ headerName: "Agreement Type", field: "agreement_type", cellClass: 'grid-align', width: 100 },
        { headerName: "Agreement Start Date", field: "AGREE_START_DATE", template: '<span>{{data.AGREE_START_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Lease Start Date", field: "LEASE_START_DATE", template: '<span>{{data.LEASE_START_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Lease End Date", field: "LEASE_END_DATE", template: '<span>{{data.LEASE_END_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Lease Extended Date", field: "EXTESNION_TODATE", template: '<span>{{data.EXTESNION_TODATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },

        { headerName: "Landlord name", field: "LANDLORD_NAME", cellClass: 'grid-align', width: 170, },
        { headerName: "GST Applicable", field: "GST", cellClass: 'grid-align', width: 100, },
        { headerName: "Landlord GST No.", field: "GSTNO", cellClass: 'grid-align', width: 150, suppressMenu: false, },
        //{ headerName: "Landlord Address", field: "LANDLORD_ADDRESS", cellClass: 'grid-align', width: 200, suppressMenu: true, hide: true },
        { headerName: "Lease Monthly Rent", field: "MONTHLY_RENT", cellClass: 'grid-align', width: 90, },
        { headerName: "Security Deposit", field: "SECURITY_DEPOSIT", cellClass: 'grid-align', width: 130, suppressMenu: false, },
        //{ headerName: "Lesse Name", field: "lease_payment_terms", cellClass: 'grid-align', width: 130 },
        //{ headerName: "Amount Paid", field: "PAID_AMOUNT", cellClass: 'grid-align', width: 130 },


        { headerName: "Carpet Area[Sqft]", field: "PM_AR_CARPET_AREA", cellClass: 'grid-align', width: 130 },
        { headerName: "Rentable Area[Sqft]", field: "PM_AR_RENT_AREA", cellClass: 'grid-align', width: 130 },
        { headerName: "BuiltUp Area[Sqft]", field: "PM_AR_BUA_AREA", cellClass: 'grid-align', width: 130 },


        //{ headerName: "Builtup area", field: "BUILTUP_AREA", cellClass: 'grid-align', width: 130, aggFunc: 'sum' },
        { headerName: "Property Address", field: "COMPLETE_ADDRESS", cellClass: 'grid-align', width: 130, suppressMenu: false, hide: true },


        { headerName: "Maintenance charges", field: "MAINTENANCE_CHARGES", cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Amenities Charges", field: "PM_LC_FF_CHARGES", cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Total Rent", field: "pm_les_tot_rent", cellClass: 'grid-align', width: 100 },
        //{ headerName: "Rent Revision", field: "RENT_REVISION", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
        { headerName: "Lease Escalation", field: "LEASE_ESCALATION", cellClass: 'grid-align', width: 80, },
        { headerName: "Rent Change Date", field: "ESCLATIONDATE", template: '<span>{{data.ESCLATIONDATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, },
        //{ headerName: "Escalation Percent", field: "ESCPERCENT", cellClass: 'grid-align', width: 70, },
        { headerName: "Payment Terms", field: "LEASE_PAYMENT_TERMS", cellClass: 'grid-align', width: 100 },
        { headerName: "Rent Payable", field: "Rent_type", cellClass: 'grid-align', width: 100 },
        { headerName: "Lease Status", field: "LEASE_STATUS", cellClass: 'grid-align', width: 90, },
        { headerName: "Requisition Status", field: "REQ_STATUS", cellClass: 'grid-align', width: 90, },
        { headerName: "Inspected By", field: "PM_PPT_INSPECTED_BY", cellClass: 'grid-align', width: 90, },
        { headerName: "Inspected Date", field: "PM_PPT_INSPECTED_DT", template: '<span>{{data.PM_PPT_INSPECTED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 90, },

        { headerName: "Lease Remarks", field: "PM_LES_REMARKS", cellClass: 'grid-align', width: 130, },
        { headerName: "Renewal Lease Remarks", field: "PM_LES_REN_L1_REMARKS", cellClass: 'grid-align', width: 130, },
        { headerName: "Lease Extension Remarks", field: "PM_LE_REMARKS", cellClass: 'grid-align', width: 130, },

        { headerName: "Surrender Date", field: "SURRENDER_DATE", template: '<span>{{data.SURRENDER_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Surrender Refund Date", field: "SURRENDER_REFUNDDATE", template: '<span>{{data.SURRENDER_REFUNDDATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Surrender out Standing Amount", field: "PM_SL_OUTSTAND_AMT", cellClass: 'grid-align', width: 90, },

        { headerName: "Surrender Lease Remarks", field: "PM_SL_REMARKS", cellClass: 'grid-align', width: 130, },

        { headerName: "Signed By POA", field: "PM_PPT_POA", cellClass: 'grid-align', width: 90, },
        { headerName: "Signage Length", field: "PM_PPT_SIG_LENGTH", cellClass: 'grid-align', width: 90, },
        { headerName: "Signage Width", field: "PM_PPT_SIG_WIDTH", cellClass: 'grid-align', width: 90, },
        { headerName: "AC Outdoor", field: "PM_PPT_AC_OUTDOOR", cellClass: 'grid-align', width: 90, },
        { headerName: "Space for GSB", field: "PM_PPT_GSB", cellClass: 'grid-align', width: 90, },
        { headerName: "Zone", field: "Zone", cellClass: 'grid-align', width: 90, },
        { headerName: "COMPANY", field: "COMPANYID", cellClass: 'grid-align', width: 90, },

        { headerName: "Car Parking", field: "PM_LO_ADDI_PARK_CHR", cellClass: 'grid-align', width: 90, },
        { headerName: "Lock in Period", field: "PM_LAD_LOCK_INSPEDATE", cellClass: 'grid-align', width: 90, },
        { headerName: "License Commencement Date", field: "PM_LAD_RENTT_DATE", cellClass: 'grid-align', width: 90, },
        { headerName: "Rent Commencement Date", field: "PM_LAD_RENTT_DATE", cellClass: 'grid-align', width: 90, },
        { headerName: "Maintenance", field: "PM_COST_MAINTENANCES", cellClass: 'grid-align', width: 90, },

        { headerName: "Landlord Monthly Rent Payable", field: "PM_LL_MON_RENT_PAYABLE", cellClass: 'grid-align', width: 90, },
        { headerName: "Landlord Security Deposit", field: "PM_LL_SECURITY_DEPOSIT", cellClass: 'grid-align', width: 90, },
        { headerName: "Landlord Maintenance Charges", field: "PM_LL_MAINT_CHARGES", cellClass: 'grid-align', width: 90, },
        { headerName: "Landlord FromDate", field: "PM_LL_FROM_DATE", template: '<span>{{data.PM_LL_FROM_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 90, },
        { headerName: "Landlord ToDate", field: "PM_LL_TO_DATE", template: '<span>{{data.PM_LL_TO_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 90, },



    ];
    $scope.columnDefs1 = [
        { headerName: "Location", field: "LCM_NAME", width: 100, cellClass: 'grid-align', },
        { headerName: "Property Name", field: "PM_PPT_NAME", width: 100, cellClass: 'grid-align', },
        { headerName: "Child Entity", field: "CHE_CODE", width: 100, cellClass: 'grid-align', },
        { headerName: "Invoice No.", field: "PM_LP_INVOICE_NO", width: 100, cellClass: 'grid-align', },
        { headerName: "Invoice Date", field: "PM_LP_INVOICE_DT", template: '<span>{{data.PM_LP_INVOICE_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        { headerName: "Property Address", field: "PM_PPT_ADDRESS", cellClass: 'grid-align', width: 150, },
        { headerName: "Branch Code", field: "PM_PPT_LOC_CODE", cellClass: 'grid-align', width: 80, },
        { headerName: "Lease ID", field: "PM_LPD_REQ_ID", cellClass: 'grid-align', width: 100, },
        { headerName: "Landlord Name", field: "PM_LPD_LL_NAME", cellClass: 'grid-align', width: 200, },
        { headerName: "Landlord Rent", field: "PM_LPD_LL_BASIC_RENT", cellClass: 'grid-align', width: 100 },
        { headerName: "Payment Term", field: "PM_LPD_LL_PAY_TERM", cellClass: 'grid-align', width: 100 },
        { headerName: "Payment Month", field: "PM_LPD_LL_PAY_MONTH", cellClass: 'grid-align', width: 100, },
        { headerName: "Payment Year", field: "PM_LPD_LL_PAY_YEAR", cellClass: 'grid-align', width: 100 },
        { headerName: "Withhold Amount", field: "withhold", cellClass: 'grid-align', width: 100 },
        { headerName: "Due Amount", field: "dueamnt", cellClass: 'grid-align', width: 100 },
        { headerName: "Rent Paid", field: "rent_paid", cellClass: 'grid-align', width: 100 },
        { headerName: "Status", field: "PM_LPD_LL_STATUS", cellClass: 'grid-align', width: 100, },
        { headerName: "Payment Date", field: "PM_LPD_CREATED_DT", cellClass: 'grid-align', width: 150, },
        { headerName: "Payment Mode", field: "PAY_MODE", cellClass: 'grid-align', width: 150, },
        { headerName: "Account NO.", field: "PM_LP_ACC_NO", cellClass: 'grid-align', width: 150, },
        { headerName: "Neft branch", field: "PM_LP_NEFT_BRNCH", cellClass: 'grid-align', width: 150, },
        { headerName: "Bank Name", field: "PM_LP_BANK_NAME", cellClass: 'grid-align', width: 150, },
        { headerName: "IFSC Code", field: "PM_LP_IFSC", cellClass: 'grid-align', width: 150, },

        { headerName: "Agreement Start Date", field: "PM_LAD_EFFE_DT_AGREEMENT", template: '<span>{{data.PM_LAD_EFFE_DT_AGREEMENT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        { headerName: "Agreement End Date", field: "PM_LAD_EXP_DT_AGREEMENT", template: '<span>{{data.PM_LAD_EXP_DT_AGREEMENT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },

    ];
    $scope.columnDefs2 = [

        { headerName: "Property Name", field: "PM_PPT_NAME", width: 100, cellClass: 'grid-align', rowGroupIndex: 0, hide: true },
        { headerName: "Basic Rent", field: "PM_LES_BASIC_RENT", width: 100, cellClass: 'grid-align', },
        { headerName: "Date of Aggrement", field: "PM_LAD_EFFE_DT_AGREEMENT", template: '<span>{{data.PM_LAD_EFFE_DT_AGREEMENT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        { headerName: "End Date of Aggrement", field: "PM_LAD_EXP_DT_AGREEMENT", template: '<span>{{data.PM_LAD_EXP_DT_AGREEMENT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        { headerName: "Escalation Type", field: "PM_LES_ESC_TYPE", cellClass: 'grid-align', width: 80, },
        { headerName: "Amount Value/percentage", field: "PM_LES_ESC_AMOUNTIN", cellClass: 'grid-align', width: 80, },
        { headerName: "Interval Duration", field: "PM_LES_ESC_INT_DUR", cellClass: 'grid-align', width: 100, },
        { headerName: "Escalation From Date", field: "PM_LES_ESC_INT_DATE", cellClass: 'grid-align', width: 200, },
        { headerName: "Escalation To Date", field: "PM_LES_ESC_TO_DATE", cellClass: 'grid-align', width: 200, },
        { headerName: "Escalated Amount/Percent", field: "PM_LES_ESC_AMTVAL", cellClass: 'grid-align', width: 100, },
        { headerName: "Lease Escalated Amount", field: "PM_LES_ESC_AMOUNT", cellClass: 'grid-align', width: 200, },
    ];
    $scope.columnDefs3 = [
        { headerName: "Lease ID", field: "LEASE_ID", width: 100, cellClass: 'grid-align', },
        { headerName: "Sub Lease Id", field: "SUB_LEASE_ID", width: 100, cellClass: 'grid-align', },
        { headerName: "Agreement Type", field: "PM_LES_AGR_TYPE", width: 100, cellClass: 'grid-align', },
        { headerName: "Main Entity", field: "main_Entity", cellClass: 'grid-align', width: 200, },
        { headerName: "Sub Entity", field: "sub_entity", cellClass: 'grid-align', width: 200, },
        { headerName: "Property Name", field: "PM_PPT_NAME", width: 100, cellClass: 'grid-align', },
        { headerName: "Property Code", field: "PM_PPT_CODE", width: 100, cellClass: 'grid-align', },
        { headerName: "SubLease Aggrement Date", field: "PM_SLA_AGREE_ST_DT", template: '<span>{{data.PM_SLA_AGREE_ST_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        { headerName: "SubLease Aggrement End Date", field: "PM_SLA_AGREE_END_DT", template: '<span>{{data.PM_SLA_AGREE_END_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 150, },
        { headerName: "Sub Lease Rent", field: "PM_SLA_COST", cellClass: 'grid-align', width: 80, },

        { headerName: "Maintenance Charges", field: "PM_SLA_MAINT_CHARGES", cellClass: 'grid-align', width: 100, },

        { headerName: "Security Deposit", field: "PM_SLA_SECURITY_DEPS", cellClass: 'grid-align', width: 200, },
        { headerName: "Sub Lease Status", field: "PM_SLA_STA_ID", cellClass: 'grid-align', width: 100, },
        { headerName: "Remarks", field: "PM_SLA_APPR_REMARKS", cellClass: 'grid-align', width: 100, },
    ];

    $scope.GridOptions = {
        columnDefs: $scope.columnDefs,

        enableFilter: false,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        pagination: true,

    };
    $scope.RentalGridOption = {
        columnDefs: $scope.columnDefs1,
        enableFilter: false,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,

    };

    $scope.EscalationGridOption = {
        columnDefs: $scope.columnDefs2,
        enableFilter: false,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        suppressRowTransform: true,
        groupMultiAutoColumn: true,
        groupColumnDef: {
            headerName: "Property Name",
            //field: "PLANID",
            cellRenderer: {
                renderer: "group"
            }
        },

    };
    $scope.SubLeaseGridOption = {
        columnDefs: $scope.columnDefs3,
        enableFilter: false,
        angularCompileRows: true,
        rowData: null,
        enableCellSelection: false,
        enableColResize: true,
        suppressRowTransform: true,

    };

    $scope.getyears = [];
    function getyears() {

        var myDate = new Date();
        var year = myDate.getFullYear();
        for (var i = 1990; i <= year + 50; i++) {
            $scope.getyears.push({ name: i.toString() });
        }
        angular.forEach($scope.getyears, function (value, key) {
            var a = _.find($scope.getyears, function (o) { return o.name == new Date().getFullYear() });
            a.ticked = false;
        });
    }
    getyears();

    $scope.LoadData = function () {
        $scope.display = 0;
        var Property = _.filter($scope.LeaseRentalandEscaltionModel.Property, function (o) { return o.ticked == true; }).map(function (x) { return x.PM_PPT_CODE; }).join(',');
        var ProReqType = _.filter($scope.LeaseRentalandEscaltionModel.ProReqType, function (o) { return o.ticked == true; }).map(function (x) { return x.PM_PPT_TYPE; }).join(',');
        var params = {
            //PM_PPT_CODE: $scope.LeaseRentalandEscaltionModel.Property[0].PM_PPT_CODE,
            PM_PPT_CODE: Property,
            //prptystats: $scope.LeaseRentalandEscaltionModel.proprty,
            PM_PPT_TYPE: ProReqType,
            year: _.filter($scope.LeaseRentalandEscaltionModel.getyears, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(','),
            //FromDate: $scope.LeaseRentalandEscaltionModel.FromDate,
            //ToDate: $scope.LeaseRentalandEscaltionModel.ToDate,

        };

        //var params = {
        //    SearchValue: searchval,
        //    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
        //    PageSize: 10,
        //    CNP_NAME: $scope.LeaseRep.CNP_NAME[0].CNP_ID,
        //    FromDate: $scope.LeaseRep.FromDate,
        //    ToDate: $scope.LeaseRep.ToDate,
        //    loclst: $scope.LeaseRep.Locations
        //};

        progress(0, 'Loading...', true);
        LeaseRentalandEscaltionService.GetGriddata(params).then(function (responce) {
            $scope.downloadExcel = true;
            $scope.gridata = responce.data;
            $scope.gridata1 = responce.data1;
            $scope.gridata2 = responce.data2;
            $scope.gridata3 = responce.data3;

            if ($scope.gridata == null) {
                $scope.GridOptions.api.setRowData([]);
                $scope.EscalationGridOption.api.setRowData([]);
                $scope.RentalGridOption.api.setRowData([]);
                $scope.SubLeaseGridOption.api.setRowData([]);
            }
            else {

                $scope.GridVisiblity = true;
                $scope.GridOptions.api.setRowData($scope.gridata);
                $scope.EscalationGridOption.api.setRowData($scope.gridata1);
                $scope.RentalGridOption.api.setRowData($scope.gridata2);
                $scope.SubLeaseGridOption.api.setRowData($scope.gridata3);

                LREC.GridOptionsArr = $scope.gridata;
                LREC.EscalationGridOptionArr = $scope.gridata1;
                LREC.RentalGridOptionArr = $scope.gridata2;
                LREC.SubLeaseGridOptionArr = $scope.gridata3;

                //for (var j in LREC.GridOptionsArr) {
                //    if (LREC.GridOptionsArr[j].MONTHLY_RENT)
                //        LREC.GridOptionsArr[j].MONTHLY_RENT = $filter('currency')(parseInt(LREC.GridOptionsArr[j].MONTHLY_RENT.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.GridOptionsArr[j].SECURITY_DEPOSIT)
                //        LREC.GridOptionsArr[j].SECURITY_DEPOSIT = $filter('currency')(parseInt(LREC.GridOptionsArr[j].SECURITY_DEPOSIT.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.GridOptionsArr[j].MAINTENANCE_CHARGES)
                //        LREC.GridOptionsArr[j].MAINTENANCE_CHARGES = $filter('currency')(parseInt(LREC.GridOptionsArr[j].MAINTENANCE_CHARGES.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.GridOptionsArr[j].PM_LC_FF_CHARGES)
                //        LREC.GridOptionsArr[j].PM_LC_FF_CHARGES = $filter('currency')(parseInt(LREC.GridOptionsArr[j].PM_LC_FF_CHARGES.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.GridOptionsArr[j].pm_les_tot_rent)
                //        LREC.GridOptionsArr[j].pm_les_tot_rent = $filter('currency')(parseInt(LREC.GridOptionsArr[j].pm_les_tot_rent.replace(/₹ /g, "").replace(/,/g, "")));
                //}

                //for (var i in LREC.EscalationGridOptionArr) {
                //    if (LREC.EscalationGridOptionArr[i].PM_LES_BASIC_RENT)
                //        LREC.EscalationGridOptionArr[i].PM_LES_BASIC_RENT = $filter('currency')(parseInt(LREC.EscalationGridOptionArr[i].PM_LES_BASIC_RENT.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.EscalationGridOptionArr[i].PM_LES_ESC_AMOUNT)
                //        LREC.EscalationGridOptionArr[i].PM_LES_ESC_AMOUNT = $filter('currency')(parseInt(LREC.EscalationGridOptionArr[i].PM_LES_ESC_AMOUNT.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.EscalationGridOptionArr[i].PM_LES_ESC_AMTVAL)
                //        LREC.EscalationGridOptionArr[i].PM_LES_ESC_AMTVAL = $filter('currency')(parseInt(LREC.EscalationGridOptionArr[i].PM_LES_ESC_AMTVAL.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.EscalationGridOptionArr[i].PM_LES_LL_ESC_RENT)
                //        LREC.EscalationGridOptionArr[i].PM_LES_LL_ESC_RENT = $filter('currency')(parseInt(LREC.EscalationGridOptionArr[i].PM_LES_LL_ESC_RENT.replace(/₹ /g, "").replace(/,/g, "")));
                //}

                //for (var z in LREC.RentalGridOptionArr) {
                //    if (LREC.RentalGridOptionArr[z].PM_LES_BASIC_RENT)
                //        LREC.RentalGridOptionArr[z].PM_LES_BASIC_RENT = $filter('currency')(parseInt(LREC.RentalGridOptionArr[z].PM_LES_BASIC_RENT.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.RentalGridOptionArr[z].PM_LES_ESC_AMTVAL)
                //        LREC.RentalGridOptionArr[z].PM_LES_ESC_AMTVAL = $filter('currency')(parseInt(LREC.RentalGridOptionArr[z].PM_LES_ESC_AMTVAL.replace(/₹ /g, "").replace(/,/g, "")));
                //    if (LREC.RentalGridOptionArr[z].PM_LES_ESC_AMOUNT)
                //        LREC.RentalGridOptionArr[z].PM_LES_ESC_AMOUNT = $filter('currency')(parseInt(LREC.RentalGridOptionArr[z].PM_LES_ESC_AMOUNT.replace(/₹ /g, "").replace(/,/g, "")));
                //}

                LREC.columnDefs = $scope.columnDefs
                LREC.columnDefs1 = $scope.columnDefs1
                LREC.columnDefs2 = $scope.columnDefs2
                LREC.columnDefs3 = $scope.columnDefs3
            }
            progress(0, 'Loading...', false);
        });
    };

    /*Excel Data Download */
    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Branch_Employee_Data.csv"
        };
        $scope.GridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    /*Excel Data Download */
    $scope.GenReport = function () {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }

});