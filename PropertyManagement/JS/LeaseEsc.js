﻿app.service("LeaseEsc", function ($http, $q, UtilityService) {
    this.GetPoprtyData = function (data) {
        deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LeaseEsc/GetPoprtyData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetLeaseinfobyReqid = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseEsc/GetLeaseinfobyReqid', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.GetLandlordinfobyReqid = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseEsc/GetLandlordinfobyReqid', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SaveLeaseData = function (propertyDetails) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseEsc/SaveLeaseData', propertyDetails)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
});
app.controller('LeaseEscController', function ($scope, $q, $http, LeaseEsc, UtilityService, $timeout, $filter) {
    $scope.Property = [];
    $scope.interveldur = true;
    $scope.Noofesc = false;
    $scope.YearandMonthWiseEsc = true;
    $scope.FlexibleWiseEsc = false;
    //$scope.btnAddesc = false;



    var PropertyObj = {
        PM_PPT_CODE: '', PM_LES_BASIC_RENT: '', PM_LAD_START_DT_AGREEMENT: '', PM_LAD_EXP_DT_AGREEMENT: '', PM_LO_ADDI_PARK_CHRG: '',
        PM_LES_SEC_DEPOSIT: '', PM_LC_MAINT_CHARGES: '', PM_LC_FF_CHARGES: '', PM_LR_REQ_ID: '', PM_LES_SNO: ''
    };

    var frmLED = {
        Intrveltype: '', ESCType: '', Interval_Duration: '', Lease: '', Noofescalation: ''
    }
    var PM_LAD_START_DT_AGREEMENT = '', PM_LAD_EXP_DT_AGREEMENT = '', diffYears = 0, Duration = 0;
    var EscalationType = [{ ID: 1, value: 'Every', NAME: 'Every' }, { ID: 2, value: 'After', NAME: 'After' }]
    var IntervalType = [{ ID: 1, value: 'Monthly', NAME: 'Monthly' }, { ID: 2, value: 'Yearly', NAME: 'Yearly' }, { ID: 3, value: 'Flexible', NAME: 'Flexible' }]
    var AmountIn = [{ ID: 1, value: 'Value', NAME: 'Value' }, { ID: 2, value: 'Percentage', NAME: 'Percentage' }]

    $scope.LeaseObj = {
        PropertyObj: angular.copy(PropertyObj), EscalationType: angular.copy(EscalationType), IntervalType: angular.copy(IntervalType), AmountIn: angular.copy(AmountIn), frmLED: angular.copy(frmLED),
        Esclationvalues: []
    };

    /* data formate */
    function frmtDt(dt, typ) {
        if (dt != '') {
            if (typ == 'Date')
                return (dt.getDate() > 9 ? dt.getDate() : '0' + dt.getDate()) + '-' + ((dt.getMonth() + 1) > 9 ? (dt.getMonth() + 1) : '0' + (dt.getMonth() + 1)) + '-' + dt.getFullYear();
            else if (typ == 'DateTime')
                return (dt.getMonth() + 1) + '-' + dt.getDate() + '-' + dt.getFullYear() + " " + (dt.getHours() > 9 ? dt.getHours() : '0' + dt.getHours()) + ':' + (dt.getMinutes() > 9 ? dt.getMinutes() : '0' + dt.getMinutes());
            else
                return (dt.getHours() > 9 ? dt.getHours() : '0' + dt.getHours()) + ':' + (dt.getMinutes() > 9 ? dt.getMinutes() : '0' + dt.getMinutes());
        } else
            return dt;
    }

    /* Date  Difference*/
    function yearsDiff(d1, d2) {
        let yearsdiff = d2.getFullYear() - d1.getFullYear();
        return yearsdiff;
    }

    /* */
    monthsDiff = function (M1, M2) {
        let monthsdiff = (M2.getFullYear() - M1.getFullYear()) * 12;
        return monthsdiff;
    }

    /* */
    function addYersFn(Y1, Y2) {
        var year = Y1.getFullYear();
        var month = Y1.getMonth();
        var day = Y1.getDate();
        return new Date(year + Y2, month, day);
    }

    /**/
    function addMonthsFn(M1, M2) {
        var year = M1.getFullYear();
        var month = M1.getMonth();
        var day = M1.getDate();
        return new Date(year, month + M2, day);
    }
    /*days*/
    function addDaysFn(d1, d2) {
        var year = d1.getFullYear();
        var month = d1.getMonth();
        var day = d1.getDate();
        return new Date(year, month, day + d2);
    }
    // clear function//

    function Clear() {
        $scope.LeaseObj.frmLED.Amountin = '';
        $scope.LeaseObj.frmLED.Interval_Duration = '';
        $scope.LeaseObj.frmLED.ESCType = '';
        $scope.LeaseObj.frmLED.Intrveltype = '';
        //$scope.LeaseObj.frmLED.Lease = '';
        $scope.LeaseObj.Esclationvalues = [];
        $scope.Noofesc = false;
        $scope.FlexibleWiseEsc = false;
        EscClear();
        //$scope.btnAddesc = false;
    }
    function EscClear() {
        $scope.LeaseObj.frmLED.Noofescalation = '';
        $scope.LeaseObj.frmLED.Noofsdescalation = '';
        $scope.LeaseObj.frmLED.Noofmcescalation = '';
        $scope.LeaseObj.frmLED.NoofAmescalation = '';
        $scope.LeaseObj.frmLED.NoofPcescalation = '';
    }
    function EscOnClear() {
        $scope.LeaseObj.PropertyObj.PM_PPT_CODE = '';
        $scope.LeaseObj.PropertyObj.PM_LES_BASIC_RENT = '';
        $scope.LeaseObj.PropertyObj.PM_LES_SEC_DEPOSIT = '';
        $scope.LeaseObj.PropertyObj.PM_LC_FF_CHARGES = '';
        $scope.LeaseObj.PropertyObj.PM_LC_MAINT_CHARGES = '';
        $scope.LeaseObj.PropertyObj.PM_LO_ADDI_PARK_CHRG = '';
        $scope.LeaseObj.PropertyObj.PM_LAD_START_DT_AGREEMENT = '';
        $scope.LeaseObj.PropertyObj.PM_LAD_EXP_DT_AGREEMENT = '';
        $scope.LeaseObj.PropertyObj.PM_LL_NAME = '';

    }
    /* */
    $scope.LeaseObj.PropertyFn = function (typ) {
        var obj = {
            PM_PPT_CODE: $scope.LeaseEscVM.Property[0].PM_PPT_CODE,
            ESCALATION: $scope.LeaseObj.frmLED.Lease,
        }

        LeaseEsc.GetLeaseinfobyReqid(obj).then(function (response) {
            if (response.data != null) {
                if ($scope.LeaseObj.frmLED.Lease == 'LandLord') {
                    $scope.Property1 = angular.copy(response.data);
                }
                else {
                    bindFn(response.data[0]);
                };
            }
        });
        EscOnClear();
    }

    /* Bind fun */
    function bindFn(dat) {
        if ($scope.LeaseObj.frmLED.Lease == 'LandLord') {
            $scope.LeaseObj.PropertyObj.PM_LL_NAME = dat.PM_LL_NAME;
            $scope.LeaseObj.PropertyObj.PM_LL_SNO = dat.PM_LL_SNO;

        }
        $scope.LeaseObj.PropertyObj.PM_PPT_CODE = dat.PM_PPT_CODE;
        $scope.LeaseObj.PropertyObj.PM_LES_BASIC_RENT = dat.PM_LES_BASIC_RENT;
        $scope.LeaseObj.PropertyObj.PM_LES_SEC_DEPOSIT = dat.PM_LES_SEC_DEPOSIT;
        $scope.LeaseObj.PropertyObj.PM_LC_MAINT_CHARGES = dat.PM_LC_MAINT_CHARGES;
        $scope.LeaseObj.PropertyObj.PM_LC_FF_CHARGES = dat.PM_LC_FF_CHARGES;
        $scope.LeaseObj.PropertyObj.PM_LO_ADDI_PARK_CHRG = dat.PM_LO_ADDI_PARK_CHRG;
        $scope.LeaseObj.PropertyObj.PM_LAD_START_DT_AGREEMENT = frmtDt(new Date(dat.PM_LAD_START_DT_AGREEMENT), 'Date');
        $scope.LeaseObj.PropertyObj.PM_LAD_EXP_DT_AGREEMENT = frmtDt(new Date(dat.PM_LAD_EXP_DT_AGREEMENT), 'Date');
        $scope.LeaseObj.PropertyObj.PM_LR_REQ_ID = dat.PM_LR_REQ_ID;
        $scope.LeaseObj.PropertyObj.PM_LES_SNO = dat.PM_LES_SNO;

        PM_LAD_START_DT_AGREEMENT = dat.PM_LAD_START_DT_AGREEMENT;
        PM_LAD_EXP_DT_AGREEMENT = dat.PM_LAD_EXP_DT_AGREEMENT;
        //PM_LR_REQ_ID = dat.PM_LR_REQ_ID;

        Clear();


    }

    $scope.LeaseObj.PropertyFn1 = function () {
        var obj = {
            PM_LL_NAME: $scope.LeaseEscVM.Property1[0].PM_LL_SNO,
            PM_LL_PM_LR_REQ_ID: $scope.LeaseEscVM.Property1[0].PM_LL_PM_LR_REQ_ID
        }
        LeaseEsc.GetLandlordinfobyReqid(obj).then(function (response) {
            console.log(response);
            if (response.data != null && response.data.length > 0) {
                bindFn(response.data[0]);
            };
        });
    }

    function GetPoprtyData() {
        LeaseEsc.GetPoprtyData().then(function (response) {
            if (response.data != null) {
                $scope.Property = response.data;
            };
        });
    }

    /*Esc Lease or landlord wise clear  function */
    $scope.LeaseObj.EscONFn = function () {
        if ($scope.LeaseObj.frmLED.Lease == 'Lease') {
            $scope.llname = false;
            $scope.llid = false;
        }
        else if ($scope.LeaseObj.frmLED.Lease == 'LandLord') {
            $scope.llname = true;
            $scope.llid = true;
        }
        $scope.LeaseObj.frmLED.Amountin = '';
        $scope.LeaseObj.frmLED.Interval_Duration = '';
        $scope.LeaseObj.frmLED.ESCType = '';
        $scope.LeaseObj.frmLED.Intrveltype = '';
        $scope.LeaseObj.Esclationvalues = [];
        $scope.LeaseObj.bsEsclationvalues = [];
        $scope.LeaseObj.Maintenance = [];
        //$scope.LeaseObj.Property1[0].PM_PPT_CODE;
        //$scope.btnAddesc = false;
        EscClear();
        EscOnClear();
        GetPoprtyData();

    }


    /*Esc Lease or landlord wise clear  function */
    $scope.LeaseObj.ESCTypeFn = function () {

        $scope.LeaseObj.frmLED.Amountin = '';
        $scope.LeaseObj.frmLED.Interval_Duration = '';
        //$scope.LeaseObj.frmLED.Intrveltype = '';
        $scope.LeaseObj.Esclationvalues = [];
        $scope.LeaseObj.bsEsclationvalues = [];
        $scope.LeaseObj.Maintenance = [];
        $scope.FlexibleWiseEsc = false;
        EscClear();
        $scope.LeaseObj.Esclationvalues = [];
        //$scope.LeaseObj.LeaseEscVM.Property = ''
    }



    /*get diff b/w years from start date and end date */
    $scope.LeaseObj.IntervalTypeFn = function () {
        $scope.LeaseObj.frmLED.Amountin = '';
        $scope.LeaseObj.frmLED.Interval_Duration = '';
        EscClear();
        $scope.LeaseObj.Esclationvalues = [];
        $scope.LeaseObj.Maintenance = [];
        $scope.LeaseObj.bsEsclationvalues = [];
        $scope.FlexibleWiseEsc = false;
        $scope.Noofesc = false;
        $scope.interveldur = true;
        $scope.YearandMonthWiseEsc = true;
        $scope.btnAddesc = false;
        $scope.escTypeShow = true;

        //if ($scope.LeaseObj.frmLED.ESCType == 'Every' || $scope.LeaseObj.frmLED.ESCType == 'After') {
        if ($scope.LeaseObj.frmLED.Intrveltype == 'Yearly') {
            diffYears = yearsDiff(new Date(PM_LAD_START_DT_AGREEMENT), new Date(PM_LAD_EXP_DT_AGREEMENT), '');
        }
        else if ($scope.LeaseObj.frmLED.Intrveltype == 'Monthly') {
            monthsDiff = monthsDiff(new Date(PM_LAD_START_DT_AGREEMENT), new Date(PM_LAD_EXP_DT_AGREEMENT));
        }
        else if ($scope.LeaseObj.frmLED.Intrveltype == 'Flexible') {
            /*monthsDiff = monthsDiff(new Date(PM_LAD_START_DT_AGREEMENT), new Date(PM_LAD_EXP_DT_AGREEMENT));*/
            $scope.interveldur = false;
            $scope.Noofesc = true;
            $scope.YearandMonthWiseEsc = false;
            $scope.FlexibleWiseEsc = true;
            $scope.btnAddesc = true;
            $scope.escTypeShow = false;
        }
        //}
    }

    /* ADD  */
    $scope.add = function (typ, id) {
        switch (id) {
            case "SecurityDepoist":
                if ($scope.LeaseObj.frmLED.Noofsdescalation == "" || $scope.LeaseObj.frmLED.Noofsdescalation == undefined || $scope.LeaseObj.frmLED.Noofsdescalation == 'NaN') {
                    $scope.LeaseObj.frmLED.Noofsdescalation = 0;
                    if (typ == "Minus") return;
                }
                $scope.LeaseObj.frmLED.Noofsdescalation = parseInt($scope.LeaseObj.frmLED.Noofsdescalation) + parseInt((typ == 'Add') ? 1 : -1);
                break;
            case "Maintenance":
                if ($scope.LeaseObj.frmLED.Noofmcescalation == "" || $scope.LeaseObj.frmLED.Noofmcescalation == undefined || $scope.LeaseObj.frmLED.Noofmcescalation == 'NaN') {
                    $scope.LeaseObj.frmLED.Noofmcescalation = 0;
                    if (typ == "Minus") return;
                }
                $scope.LeaseObj.frmLED.Noofmcescalation = parseInt($scope.LeaseObj.frmLED.Noofmcescalation) + parseInt((typ == 'Add') ? 1 : -1);
                break;
            case "Amenities":
                if ($scope.LeaseObj.frmLED.NoofAmescalation == "" || $scope.LeaseObj.frmLED.NoofAmescalation == undefined || $scope.LeaseObj.frmLED.NoofAmescalation == 'NaN') {
                    $scope.LeaseObj.frmLED.NoofAmescalation = 0;
                    if (typ == "Minus") return;
                }
                $scope.LeaseObj.frmLED.NoofAmescalation = parseInt($scope.LeaseObj.frmLED.NoofAmescalation) + parseInt((typ == 'Add') ? 1 : -1);
                break;
            case "Parking":
                if ($scope.LeaseObj.frmLED.NoofPcescalation == "" || $scope.LeaseObj.frmLED.NoofPcescalation == undefined || $scope.LeaseObj.frmLED.NoofPcescalation == 'NaN') {
                    $scope.LeaseObj.frmLED.NoofPcescalation = 0;
                    if (typ == "Minus") return;
                }
                $scope.LeaseObj.frmLED.NoofPcescalation = parseInt($scope.LeaseObj.frmLED.NoofPcescalation) + parseInt((typ == 'Add') ? 1 : -1);
                break;
            case "BasicRent":
            default:
                if ($scope.LeaseObj.frmLED.Noofescalation == "" || $scope.LeaseObj.frmLED.Noofescalation == undefined || $scope.LeaseObj.frmLED.Noofescalation == 'NaN') {
                    $scope.LeaseObj.frmLED.Noofescalation = 0;
                    if (typ == "Minus") return;
                }
                $scope.LeaseObj.frmLED.Noofescalation = parseInt($scope.LeaseObj.frmLED.Noofescalation) + parseInt((typ == 'Add') ? 1 : -1);
                break;
        }
        if (typ == 'Add') {
            //$scope.LeaseObj.frmLED.Noofescalation = parseInt($scope.LeaseObj.frmLED.Noofescalation) + 1;
            var obj = {
                ESCALATION: "Escalation " + $scope.LeaseObj.Esclationvalues.filter((x) => { return x.Type == id }).length,
                VALUE: '',
                Type: id,
                IndexNo: $scope.LeaseObj.Esclationvalues.filter((x) => { return x.Type == id }).length
            }
            $scope.LeaseObj.Esclationvalues.push(obj);
        }
        else {
            //$scope.LeaseObj.frmLED.Noofescalation = parseInt($scope.LeaseObj.frmLED.Noofescalation) - 1;
            var index = $scope.LeaseObj.Esclationvalues.map(e => e.Type).lastIndexOf(id);
            $scope.LeaseObj.Esclationvalues.splice(index, 1);
        }
    }

    /* for Flexible */
    $scope.LeaseObj.Esclationvalues = [];
    $scope.LeaseObj.NoofEscalationfn = function (type) {
        if ($scope.LeaseObj.frmLED.Noofescalation == "" || $scope.LeaseObj.frmLED.Noofescalation == undefined || $scope.LeaseObj.frmLED.Noofescalation == 0) {
            return;
        }
        $scope.LeaseObj.Esclationvalues = $scope.LeaseObj.Esclationvalues.filter(function (value, index, arr) {
            return value.Type != type;
        });
        switch (type) {
            case "BasicRent":
                Count = parseInt($scope.LeaseObj.frmLED.Noofescalation);
                break;
            case "SecurityDepoist":
                Count = parseInt($scope.LeaseObj.frmLED.Noofsdescalation);
                break;
            case "Maintenance":
                Count = parseInt($scope.LeaseObj.frmLED.Noofmcescalation);
                break;
            case "Amenities":
                Count = parseInt($scope.LeaseObj.frmLED.NoofAmescalation);
                break;
            case "Parking":
                Count = parseInt($scope.LeaseObj.frmLED.NoofPcescalation);
                break;
            default:
                Count = parseInt($scope.LeaseObj.frmLED.Interval_Duration);
                break;
        }
        if (Count > 0) {
            for (var i = 0; i < Count; i++) {
                var obj = {
                    ESCALATION: "Escalation " + i,
                    VALUE: '',
                    Type: type
                }
                $scope.LeaseObj.Esclationvalues.push(obj);
            }
        }
    }

    $scope.LeaseObj.YearonchangeFn = function (type, id) {
        var indexArray = $scope.LeaseObj.Esclationvalues.reduce(function (a, e, i) {
            if (e.Type === type)
                a.push(i);
            return a;
        }, []);
        //console.log(indexArray);

        for (var i = id; i < indexArray.length; i++) {
            if (($scope.LeaseObj.Esclationvalues[indexArray[i]].addyear == '' || $scope.LeaseObj.Esclationvalues[indexArray[i]].addyear == undefined)
                && ($scope.LeaseObj.Esclationvalues[indexArray[i]].addmonth == '' || $scope.LeaseObj.Esclationvalues[indexArray[i]].addmonth == undefined)
                && ($scope.LeaseObj.Esclationvalues[indexArray[i]].addday == '' || $scope.LeaseObj.Esclationvalues[indexArray[i]].addday == undefined)) {
                $scope.LeaseObj.Esclationvalues[indexArray[i]].EsclationDate = "";
                $scope.LeaseObj.Esclationvalues[indexArray[i]].DATE = "";
                continue;
            }
            var currentAgrementPeriod = new Date();
            if (id > 0) {
                if ($scope.LeaseObj.Esclationvalues[indexArray[i - 1]].EsclationDate == undefined || $scope.LeaseObj.Esclationvalues[indexArray[i - 1]].EsclationDate == "") {
                    //Need to validation popup like need to add above escalation then only you can add this
                    $scope.LeaseObj.Esclationvalues[indexArray[i]].EsclationDate = "";
                    $scope.LeaseObj.Esclationvalues[indexArray[i]].DATE = "";
                    $scope.LeaseObj.Esclationvalues[indexArray[i]].addyear = "";
                    $scope.LeaseObj.Esclationvalues[indexArray[i]].addmonth = "";
                    $scope.LeaseObj.Esclationvalues[indexArray[i]].addday = "";
                    continue;
                }
                currentAgrementPeriod = new Date($scope.LeaseObj.Esclationvalues[indexArray[i - 1]].EsclationDate);
            }
            else {
                currentAgrementPeriod = new Date(PM_LAD_START_DT_AGREEMENT);
            }
            if ($scope.LeaseObj.Esclationvalues[indexArray[i]].addyear != '' && $scope.LeaseObj.Esclationvalues[indexArray[i]].addyear > 0) {
                currentAgrementPeriod = addYersFn(currentAgrementPeriod, parseInt($scope.LeaseObj.Esclationvalues[indexArray[i]].addyear));
            }
            if ($scope.LeaseObj.Esclationvalues[indexArray[i]].addmonth != "" && $scope.LeaseObj.Esclationvalues[indexArray[i]].addmonth > 0) {
                currentAgrementPeriod = addMonthsFn(currentAgrementPeriod, parseInt($scope.LeaseObj.Esclationvalues[indexArray[i]].addmonth));
            }
            if ($scope.LeaseObj.Esclationvalues[indexArray[i]].addday != "" && $scope.LeaseObj.Esclationvalues[indexArray[i]].addday > 0) {
                currentAgrementPeriod = addDaysFn(currentAgrementPeriod, parseInt($scope.LeaseObj.Esclationvalues[indexArray[i]].addday));
            }
            var month = parseInt(currentAgrementPeriod.getMonth() + 1);//months (0-11)
            var day = (currentAgrementPeriod.getDate());//day (1-31)
            var year = currentAgrementPeriod.getFullYear();
            var expiryDate = $scope.LeaseObj.PropertyObj.PM_LAD_EXP_DT_AGREEMENT.split('-');
            if (currentAgrementPeriod > new Date(expiryDate[2], parseInt(expiryDate[1] - 1), expiryDate[0])) {
                $scope.LeaseObj.Esclationvalues[indexArray[i]].EsclationDate = "";
                $scope.LeaseObj.Esclationvalues[indexArray[i]].DATE = "";
                $scope.LeaseObj.Esclationvalues[indexArray[i]].addyear = "";
                $scope.LeaseObj.Esclationvalues[indexArray[i]].addmonth = "";
                $scope.LeaseObj.Esclationvalues[indexArray[i]].addday = "";
                //Need to keep Validation saying like current date should not be greater than exipry date
                continue;
            }
            //var formattedDate = year + "-" + month + "-" + day;
            $scope.LeaseObj.Esclationvalues[indexArray[i]].EsclationDate = currentAgrementPeriod;
            $scope.LeaseObj.Esclationvalues[indexArray[i]].DATE = year + "-" + month + "-" + day;//currentAgrementPeriod.toISOString().slice(0, 10);
        }

    }


    /* for Monthly and yearly */
    $scope.LeaseObj.IntervalDurationfn = function () {
        $scope.LeaseObj.Esclationvalues = [];
        if ($scope.LeaseObj.frmLED.Interval_Duration.length > 0) {
            //if ($scope.LeaseObj.frmLED.ESCType == 'Every') {
            if ($scope.LeaseObj.frmLED.Intrveltype == 'Monthly') {
                if ($scope.LeaseObj.frmLED.ESCType == 'Every')
                    Duration = monthsDiff / parseInt($scope.LeaseObj.frmLED.Interval_Duration);
                else Duration = 1;
                /* Duration = monthsDiff / parseInt($scope.LeaseObj.frmLED.Interval_Duration);*/
                if ($scope.LeaseObj.frmLED.Amountin == 'Value' || $scope.LeaseObj.frmLED.Amountin == 'Percentage') {
                    for (var i = 1; i <= parseInt(Duration); i++) {

                        if (i == 1) {
                            var date = frmtDt(new Date(addMonthsFn(new Date(PM_LAD_START_DT_AGREEMENT), parseInt($scope.LeaseObj.frmLED.Interval_Duration))), 'Date');
                            var dates = addMonthsFn(new Date(PM_LAD_START_DT_AGREEMENT), parseInt($scope.LeaseObj.frmLED.Interval_Duration));
                        }
                        else {
                            var date = frmtDt(new Date(addMonthsFn(new Date(dates), parseInt($scope.LeaseObj.frmLED.Interval_Duration))), 'Date');
                            var dates = addMonthsFn(new Date(dates), parseInt($scope.LeaseObj.frmLED.Interval_Duration));
                        }
                        var obj = {
                            ESCALATION: "Escalation " + i,
                            DATE: date,
                            EsclationDate: dates,
                            VALUE: '',
                            Type: "BasicRent"
                        }
                        if (i < parseInt(Duration) || (i == parseInt(Duration) && new Date(PM_LAD_EXP_DT_AGREEMENT) > dates))
                            $scope.LeaseObj.Esclationvalues.push(obj);
                    }

                }
            }
            if ($scope.LeaseObj.frmLED.Intrveltype == 'Yearly') {
                if ($scope.LeaseObj.frmLED.ESCType == 'Every')
                    Duration = diffYears / parseInt($scope.LeaseObj.frmLED.Interval_Duration);
                else Duration = 1;
                /* Duration = diffYears / parseInt($scope.LeaseObj.frmLED.Interval_Duration);*/
                if ($scope.LeaseObj.frmLED.Amountin == 'Value' || $scope.LeaseObj.frmLED.Amountin == 'Percentage') {

                    for (var i = 1; i <= parseInt(Duration); i++) {
                        if (i == 1) {
                            var date = frmtDt(new Date(addYersFn(new Date(PM_LAD_START_DT_AGREEMENT), parseInt($scope.LeaseObj.frmLED.Interval_Duration))), 'Date');
                            var dates = addYersFn(new Date(PM_LAD_START_DT_AGREEMENT), parseInt($scope.LeaseObj.frmLED.Interval_Duration));
                        }
                        else {
                            var date = frmtDt(new Date(addYersFn(new Date(dates), parseInt($scope.LeaseObj.frmLED.Interval_Duration))), 'Date');
                            var dates = addYersFn(new Date(dates), parseInt($scope.LeaseObj.frmLED.Interval_Duration));
                        }
                        var obj = {
                            ESCALATION: "Escalation " + i,
                            DATE: date,
                            EsclationDate: dates,
                            VALUE: '',
                            Type: "BasicRent"
                        }
                        if (i < parseInt(Duration) || (i == parseInt(Duration) && new Date(PM_LAD_EXP_DT_AGREEMENT) > dates))
                            $scope.LeaseObj.Esclationvalues.push(obj);
                    }

                }
            }
            //}
        }

    }

    $scope.LeaseSubmit = function (typ) {
        // $scope.LeaseObj.Esclationvalues = $scope.LeaseObj.Esclationvalues.filter((x) => { return (x.DATE != "" || x.DATE != undefined || x.EsclationDate != "" || x.EsclationDate != undefined) })
        if (typ == 'C') {
            var bool = true;
            if (bool && $scope.LeaseObj.frmLED.Lease.length == 0) {
                bool = false;
                showNotification('error', 8, 'bottom-right', 'Enter Escalation On ');
            }
            if (bool && $scope.LeaseEscVM.Property.length == 0) {
                bool = false;
                showNotification('error', 8, 'bottom-right', 'Select Property Name');
            }
            if (bool && $scope.LeaseObj.frmLED.Intrveltype.length == 0) {
                bool = false;
                showNotification('error', 8, 'bottom-right', 'Select Interval Type');
            }
            if (bool && $scope.LeaseObj.frmLED.Amountin.length == 0) {
                bool = false;
                showNotification('error', 8, 'bottom-right', 'Select Amount In');
            }
            //if (!bool)
            //    showNotification('error', 8, 'bottom-right', 'Enter Valid data');
            return bool;
        }

        if ($scope.LeaseSubmit('C')) {
            if ($scope.LeaseObj.Esclationvalues.length > 0) {
                var rentAmount = [];
                const distinctTypesList = [];
                const map = new Map();
                for (const item of $scope.LeaseObj.Esclationvalues) {
                    if (!map.has(item.Type)) {
                        map.set(item.Type, true);    // set any value to Map
                        distinctTypesList.push({
                            Type: item.Type
                        });
                    }
                }
                // console.log(distinctTypesList);
                angular.forEach(distinctTypesList, function (value) {
                    $scope.EsclationvaluesList = $scope.LeaseObj.Esclationvalues.filter(function (el) {
                        return el.Type == value.Type;
                    });
                    //console.log($scope.EsclationvaluesList);
                    for (var i = 0; i <= $scope.EsclationvaluesList.length; i++) {
                        var specficRentType = rentAmount.filter((e) => { return e.Type == $scope.EsclationvaluesList[0].Type });

                        var startDate = new Date();
                        var endDate = new Date();
                        var EscalationStartDate;
                        var EscalationEndDate;
                        var monthDiffence = 0;
                        var intervalAmount = 0;
                        var previousMonthAmount = 0;
                        var rentType;
                        var payFree;
                        var value;
                        if (i == 0) {
                            EscalationStartDate = $scope.LeaseObj.PropertyObj.PM_LAD_START_DT_AGREEMENT;
                            EscalationEndDate = $scope.EsclationvaluesList[i].DATE;
                            var aggrementDate = $scope.LeaseObj.PropertyObj.PM_LAD_START_DT_AGREEMENT.split('-');
                            startDate = new Date(aggrementDate[2], parseInt(aggrementDate[1] - 1), aggrementDate[0]);
                            endDate = $scope.EsclationvaluesList[i].EsclationDate;
                            switch ($scope.EsclationvaluesList[i].Type) {
                                case "SecurityDepoist":
                                    previousMonthAmount = $scope.LeaseObj.PropertyObj.PM_LES_SEC_DEPOSIT;
                                    rentType = "SecurityDepoist";
                                    break;
                                case "Maintenance":
                                    previousMonthAmount = $scope.LeaseObj.PropertyObj.PM_LC_MAINT_CHARGES;
                                    rentType = "Maintenance";
                                    break;
                                case "Amenities":
                                    previousMonthAmount = $scope.LeaseObj.PropertyObj.PM_LC_FF_CHARGES;
                                    rentType = "Amenities";
                                    break;
                                case "Parking":
                                    previousMonthAmount = $scope.LeaseObj.PropertyObj.PM_LO_ADDI_PARK_CHRG;
                                    rentType = "Parking";
                                    break;
                                default:
                                    previousMonthAmount = $scope.LeaseObj.PropertyObj.PM_LES_BASIC_RENT;
                                    rentType = "BasicRent";
                                    break;
                            }

                            payFree = $scope.EsclationvaluesList[i].payFree;
                            value = 0;
                        }
                        else if ((i + 1) == ($scope.EsclationvaluesList.length + 1)) {
                            EscalationStartDate = $scope.EsclationvaluesList[i - 1].DATE;
                            EscalationEndDate = $scope.LeaseObj.PropertyObj.PM_LAD_EXP_DT_AGREEMENT;
                            startDate = $scope.EsclationvaluesList[i - 1].EsclationDate;
                            var aggrementDate = $scope.LeaseObj.PropertyObj.PM_LAD_EXP_DT_AGREEMENT.split('-');
                            endDate = new Date(aggrementDate[2], parseInt(aggrementDate[1] - 1), aggrementDate[0]);
                            //}

                            payFree = $scope.EsclationvaluesList[i - 1].payFree;
                            intervalAmount = $scope.EsclationvaluesList[i - 1].VALUE;
                            rentType = ($scope.EsclationvaluesList[i - 1].Type == undefined || $scope.EsclationvaluesList[i - 1].Type == "") ? "BasicRent" : $scope.EsclationvaluesList[i - 1].Type;
                            value = $scope.EsclationvaluesList[i - 1].VALUE;
                        }
                        else {
                            EscalationStartDate = $scope.EsclationvaluesList[i - 1].DATE;
                            EscalationEndDate = $scope.EsclationvaluesList[i].DATE;
                            startDate = $scope.EsclationvaluesList[i - 1].EsclationDate;
                            endDate = $scope.EsclationvaluesList[i].EsclationDate;
                            intervalAmount = $scope.EsclationvaluesList[i - 1].VALUE;
                            payFree = $scope.EsclationvaluesList[i - 1].payFree;
                            rentType = ($scope.EsclationvaluesList[i].Type == undefined || $scope.EsclationvaluesList[i].Type == "") ? "BasicRent" : $scope.EsclationvaluesList[i].Type;
                            value = $scope.EsclationvaluesList[i - 1].VALUE;
                        }
                        monthDiffence = parseInt(parseInt(endDate.getFullYear() - startDate.getFullYear()) * 12);//monthsDiff(startDate, endDate);
                        monthDiffence -= startDate.getMonth();
                        monthDiffence += endDate.getMonth();
                        var finalAmount = 0;
                        if (payFree) {
                            if (i > 0) {
                                previousMonthAmount = specficRentType[i - 1].MonthWiseAmount;
                                finalAmount = 0;
                            }
                            else {
                                finalAmount = parseFloat(previousMonthAmount * monthDiffence);
                            }
                        }
                        else if ($scope.LeaseObj.frmLED.Amountin == 'Percentage') {
                            if (specficRentType != undefined && specficRentType.length > 0) {
                                previousMonthAmount = parseFloat(parseFloat(specficRentType[i - 1].MonthWiseAmount) + (parseFloat((parseFloat(specficRentType[i - 1].MonthWiseAmount) * parseFloat(intervalAmount)) / 100)));
                            }
                            finalAmount = parseFloat(previousMonthAmount * monthDiffence);
                        }
                        else {

                            if (specficRentType != undefined && specficRentType.length > 0) {
                                previousMonthAmount = parseFloat(specficRentType[i - 1].MonthWiseAmount) + parseFloat(intervalAmount);
                            }
                            finalAmount = parseFloat((previousMonthAmount) * monthDiffence);
                        }
                        rentType = (rentType == undefined || rentType == "") ? "BasicRent" : rentType; // ? "SecurityDepoist" : rentType ? "Maintenance" : rentType ? "Amenities" : rentType ? "Parking" : rentType;
                        //console.log(i);
                        //console.log($scope.EsclationvaluesList[i-1].VALUE);
                        var obj = {
                            StartDate: EscalationStartDate,
                            EndDate: EscalationEndDate,
                            MonthWiseAmount: previousMonthAmount,
                            Amount: finalAmount,
                            Type: rentType,
                            IndexNo: i,
                            VALUE: value
                            //Value: $scope.EsclationvaluesList[i - 1].VALUE
                        };
                        rentAmount.push(obj);
                    }
                });
                //, $scope.LeaseObj.frmLED, rentAmount
                var obj = {
                    PropertyBasicDetails: $scope.LeaseObj.PropertyObj,
                    EscalationAmountList: rentAmount,
                    EscalationTypeDetails: $scope.LeaseObj.frmLED
                };
                LeaseEsc.SaveLeaseData(obj).then(function (response) {
                    showNotification('success', 8, 'bottom-right', 'Escalation Details successfully saved');

                    EscClear();
                    EscOnClear();
                    GetPoprtyData();
                    $scope.YearandMonthWiseEsc = false;
                    $scope.FlexibleWiseEsc = false;
                    // ok//
                });
                //console.log(rentAmount);
                //console.log($scope.LeaseObj.PropertyObj);
                //console.log($scope.LeaseObj.frmLED);
            }
        }

    };


    /*page Load Function*/
    function loadFn() {
        GetPoprtyData();
    }
    loadFn();




});