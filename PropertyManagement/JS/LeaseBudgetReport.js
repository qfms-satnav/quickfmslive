﻿app.service("LeaseBudgetService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseBudget/GetCustomizedDetails', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };



    this.GetPoprtyData = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseBudget/GetPoprtyData', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.BindPropType = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseBudget/BindPropType', data)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetMonthwise = function (Params) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseBudget/GetMonthwise', Params)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };


}]);
app.controller('LeaseBudgetController', ['$scope', '$q', '$http', 'LeaseBudgetService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, LeaseBudgetService, UtilityService, $timeout, $filter) {
    $scope.Branch = {};
    $scope.Entity = [];
    $scope.getyears = [];
    $scope.GridVisiblity = false;
    $scope.DocTypeVisible = 0;
    $scope.Property = [];
    $scope.PropertyType = [];
    $scope.Gridmode = "YearGrid";

    $scope.gridOptions = {
        columnDefs: [],
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit()
        },
    }

    function getChildEntity() {
        UtilityService.getChildEntity(1).then(function (response) {
            if (response.data != null) {
                $scope.Entity = response.data;
                //$scope.Propertybind();
                //$scope.BindPropType();
                angular.forEach($scope.Entity, function (value, key) {
                    value.ticked = true;
                    if (key == ($scope.Entity.length - 1)) {
                        LeaseBudgetService.BindPropType({ propertyLists: $scope.Entity }).then(function (response1) {
                            $scope.PropertyType = "";
                            if (response1.data != null) {
                                $scope.PropertyType = response1.data;
                                angular.forEach($scope.PropertyType, function (value1, key1) {
                                    value1.ticked = true;
                                    if (key1 == ($scope.PropertyType.length - 1)) {
                                        LeaseBudgetService.GetPoprtyData({ propertyLists: $scope.Entity }).then(function (response2) {
                                            $scope.Property = "";
                                            if (response2.data != null) {
                                                $scope.Property = response2.data;
                                                angular.forEach($scope.Property, function (value2, key2) {
                                                    value2.ticked = true;
                                                    if (key2 == ($scope.Property.length - 1)) {
                                                        $scope.Binddata($scope.Gridmode);
                                                    }
                                                });
                                            };
                                        });
                                    }
                                });
                            };
                        });
                    }
                });
            }
        });
    };

    $scope.cnySelectAll = function () {
        $scope.Branch.Entity = $scope.Country;
        $scope.Propertybind();
        // $scope.BindPropType();
    }

    $scope.lcmSelectNone = function () {
        angular.forEach($scope.Property, function (value) {
            value.ticked = false;
        });
        angular.forEach($scope.PropertyType, function (value1) {
            value1.ticked = false;
        });
        //$scope.Branch.Entity = [];
        //$scope.Propertybind('E');
    }




    $scope.Propertybind = function (typ) {
        if ($scope.Branch.Entity == undefined || ($scope.Branch.Entity.length == 0 && typ != 'E')) {
            $scope.Branch.Entity = angular.copy($scope.Entity);
        }
        LeaseBudgetService.GetPoprtyData({ propertyLists: $scope.Branch.Entity }).then(function (response) {
            $scope.Property = "";
            if (response.data != null) {
                $scope.Property = response.data;
                angular.forEach($scope.Property, function (value1, key1) {
                    value1.ticked = true;
                });
            }    //$scope.Binddata($scope.Gridmode);
            LeaseBudgetService.BindPropType({ propertyLists: $scope.Branch.Entity }).then(function (response) {
                $scope.PropertyType = "";
                if (response.data != null) {
                    $scope.PropertyType = response.data;
                    angular.forEach($scope.PropertyType, function (value2, key2) {
                        value2.ticked = true;
                    });
                    $scope.Binddata($scope.Gridmode);
                };

            });
        });
    };


    $scope.BindPropType = function (typ) {
        if ($scope.Branch.Entity == undefined || ($scope.Branch.Entity.length == 0 && typ != 'E')) {
            $scope.Branch.Entity = angular.copy($scope.Entity);
        }
        LeaseBudgetService.BindPropType({ propertyLists: $scope.Branch.Entity }).then(function (response) {
            $scope.PropertyType = "";
            if (response.data != null) {
                $scope.PropertyType = response.data;
                angular.forEach($scope.PropertyType, function (value, key) {
                    value.ticked = true;
                });
                $scope.Binddata($scope.Gridmode);
            };
        });
    };


    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        $scope.gridmonth.api.setQuickFilter(value);
    }
    $scope.gridmonth = {
        columnDefs: [],
        enableCellSelection: false,
        enableFilter: true,
        rowData: [],
        enableSorting: true,
        angularCompileRows: true,
        rowSelection: 'multiple',
        enableColResize: true,
        onReady: function () {
            $scope.gridmonth.api.sizeColumnsToFit()
        },
    }


    $scope.Binddata = function (typ) {
        if (typ == 'YearGrid') {
            $scope.BindGrid_Year();
        }
        else {
            $scope.BindGrid_Month();
        }
    }

    $scope.BindGrid_Year = function () {
        progress(0, 'Loading...', true);
        $scope.Gridmode = "YearGrid";

        if ($scope.Branch.Property == undefined || $scope.Branch.Property.length == 0) {
            $scope.Branch.Property = $scope.Property;
        }
        var dataObj = {
            propertyLists: $scope.Branch.Property,
            Year: _.filter($scope.getyears, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(',')
            //entitylst: $scope.Branch.Entity,

        };
        LeaseBudgetService.GetGriddata(dataObj).then(function (response) {
            if (response != null && response.griddata != null) {
                $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData(response.griddata);
            }
            else {
                // $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData([]);
            }
            progress(0, 'Loading...', false);
        });
    }

    $scope.BindGrid_Month = function () {
        progress(0, 'Loading...', true);
        $scope.Gridmode = "MonthGrid";

        if ($scope.Branch.Property == undefined || $scope.Branch.Property.length == 0) {
            $scope.Branch.Property = $scope.Property;
        }
        var dataObj = {
            propertyLists: $scope.Branch.Property,
            Year: _.filter($scope.getyears, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(',')
            //entitylst: $scope.Branch.Entity,
        };
        LeaseBudgetService.GetMonthwise(dataObj).then(function (response) {
            if (response != null && response.griddata != null) {
                $scope.gridmonth.api.setColumnDefs(response.Coldef);
                $scope.gridmonth.api.setRowData(response.griddata);
            }
            else {
                // $scope.gridmonth.api.setColumnDefs(response.Coldef);
                $scope.gridmonth.api.setRowData([]);
            }
            progress(0, 'Loading...', false);
        });

    };


    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Lease_Budget_YearWise_Report.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);

        //$scope.gridmonth.api.exportDataAsCsv(Filterparams);
        //setTimeout(function () {
        //    progress(0, 'Loading...', false);
        //}, 1000);
    }

    //$scope.GenReport = function (Type) {
    //    progress(0, 'Loading...', true);
    //    $scope.GenerateFilterExcel();
    //}
    $scope.GenerateFilterExcel1 = function () {
        progress(0, 'Loading...', true);

        var Filterparams1 = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "Lease_Budget_MonthWise_Report.csv"
        };

        $scope.gridmonth.api.exportDataAsCsv(Filterparams1);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        if ($scope.Gridmode == "YearGrid") {
            $scope.GenerateFilterExcel();
        }
        else {
            $scope.GenerateFilterExcel1();
        }

    }

    function getyears() {
        var myDate = new Date();
        var year = myDate.getFullYear();
        for (var i = 1990; i <= year + 30; i++) {
            $scope.getyears.push({ name: i.toString() });
        }
        angular.forEach($scope.getyears, function (value, key) {
            var a = _.find($scope.getyears, function (o) { return o.name == new Date().getFullYear() });
            a.ticked = false;
        });
    }

    function LoadData() {
        getChildEntity();
        //BindPropType();
        getyears();

    }

    LoadData();


}]);
