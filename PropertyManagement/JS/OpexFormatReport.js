﻿//app.service("OpexFormatReportService", function ($http, $q, UtilityService) {
app.service("OpexFormatReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (data) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/OpexFormatReport/getOpexFormatDetails', data)

            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.SearchAllData = function (SearchSpaces) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/EmployeeLocationReport/SearchAllData', SearchSpaces)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('OpexFormatReportController', function ($scope, $q, $location, OpexFormatReportService, UtilityService, $filter) {

    $scope.OpexFormat = {};
    $scope.search = [];

    $scope.gridOptions = {
        columnDefs: '',
        enableColResize: true,
        enableCellSelection: false,
        enableFilter: true,
        enableSorting: true,
        enableScrollbars: false,
        groupHideGroupColumns: true,
        suppressHorizontalScroll: false,
        onReady: function () {
            $scope.gridOptions.api.sizeColumnsToFit();
        }
    };



    $scope.Pageload = function () {
        UtilityService.getCities(2).then(function (response) {

            if (response.data != null) {
                $scope.City = response.data;
                angular.forEach($scope.City, function (value, key) {
                    value.ticked = true;
                });
                UtilityService.getLocations(2).then(function (response) {
                    if (response.data != null) {
                        $scope.Locations = response.data;
                        angular.forEach($scope.Locations, function (value, key) {
                            value.ticked = true;
                        })
                    }
                    UtilityService.getChildEntity(parseInt(CompanySession)).then(function (response) {
                        //console.log(response);
                        if (response.data != null) {
                            $scope.Entity = response.data;
                            //console.log($scope.Entity);
                            angular.forEach($scope.Entity, function (value, key) {
                                //var a = _.find($scope.Entity, { CHE_CODE: parseInt(CompanySession) });
                                value.ticked = true;
                                //$scope.Customized.CHE_NAME.push(a);
                            });
                        }
                    });


                });

            }
        });
    }

    //$scope.Month = [
    //    { id: 0, name: 'January' },
    //    { id: 1, name: 'February' },
    //    { id: 2, name: 'March' },
    //    { id: 3, name: 'April' },
    //    { id: 4, name: 'May' },
    //    { id: 5, name: 'June' },
    //    { id: 6, name: 'July' },
    //    { id: 7, name: 'August' },
    //    { id: 8, name: 'September' },
    //    { id: 9, name: 'October' },
    //    { id: 10, name: 'November' },
    //    { id: 11, name: 'December' },

    //];
    ////angular.forEach($scope.Month, function (value, key) {
    //var a = _.find($scope.Month, function (o) { return o.id == new Date().getMonth() });
    //console.log(a);
    //a.ticked = true;
    //// });

    //$scope.getyears = [];
    //function getyears() {

    //    var myDate = new Date();
    //    var year = myDate.getFullYear();
    //    for (var i = 2000; i <= year + 1; i++) {
    //        $scope.getyears.push({ name: i.toString() });
    //    }
    //    angular.forEach($scope.getyears, function (value, key) {
    //        var a = _.find($scope.getyears, function (o) { return o.name == new Date().getFullYear() });
    //        a.ticked = true;
    //    });
    //}
    //getyears();

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.OpexFormat.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }
    $scope.cnySelectAll = function () {
        $scope.OpexFormat.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }
    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.OpexFormat.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.OpexFormat.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.OpexFormat.City = $scope.City;
        $scope.getLocationsByCity();
    }


    $scope.locSelectAll = function () {
        $scope.OpexFormat.Locations = $scope.Locations;
        $scope.Locations();
    }

    $scope.LocationChange = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.OpexFormat.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.OpexFormat.City[0] = cty;
            }
        });
    }

    //$scope.Pageload();p

    $scope.LoadData = function () {
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        progress(0, 'Loading...', true);
        var dataObj = {
            SearchValue: searchval,
            loclist: _.filter($scope.OpexFormat.Locations, function (o) { return o.ticked == true; }).map(function (x) { return x.LCM_CODE; }).join(','),
            entitylist: _.filter($scope.OpexFormat.Entity, function (o) { return o.ticked == true; }).map(function (x) { return x.CHE_CODE; }).join(','),
            //year: _.filter($scope.OpexFormat.getyears, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(','),
            //month: _.filter($scope.OpexFormat.Month, function (o) { return o.ticked == true; }).map(function (x) { return x.name; }).join(',')
            FromDate: moment($scope.OpexFormat.FromDate, "DD-MM-YYYY").format("MM-DD-YYYY"),
            ToDate: moment($scope.OpexFormat.ToDate, "DD-MM-YYYY").format("MM-DD-YYYY")
        };

        OpexFormatReportService.GetGriddata(dataObj).then(function (response) {
            if (response.Message == "OK") {

                $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData(response.griddata);
            }
            else {
                $scope.gridOptions.api.setColumnDefs(response.Coldef);
                $scope.gridOptions.api.setRowData([]);
            }
            progress(0, 'Loading...', false);
        });

    };

    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }


    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var ws = XLSX.utils.json_to_sheet($scope.gridOptions.rowData);
        var columnHeaders = Object.keys($scope.gridOptions.rowData[0]);
        /* Iterate over the column headers */
        columnHeaders.forEach(function (header, columnIndex) {
            var Length = [];
            var headerLength = header.length + 2; // Start with the length of the header
            /* Iterate over the rows to find the maximum length of cell content */
            $scope.gridOptions.rowData.forEach(function (row) {
                var cellValue = row[header] ? row[header].toString() : '';
                var maxLength = Math.max(headerLength, cellValue.length + 2); // Add 2 for padding
                Length.push(maxLength);
            });
            let maximumValue = Math.max.apply(null, Length);
            //let maximumValue = Math.max(...Length);
            var columnPixels = maximumValue;
            /* Set the column width in the worksheet */
            ws['!cols'] = ws['!cols'] || [];
            ws['!cols'][columnIndex] = { width: columnPixels };

        });
        /* add to workbook */
        var wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Opex_Format_Report");
        /* write workbook and force a download */
        XLSX.writeFile(wb, "Opex_Format_Report.xlsx");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        progress(0, 'Loading...', true);
        $scope.GenerateFilterExcel();
    }

    $scope.Pageload();
    setTimeout(function () { $scope.LoadData(); }, 3000);

    $scope.search = function () {
        $scope.LoadData();
    }
});