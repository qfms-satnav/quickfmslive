﻿app.service("LeaseCustomizedReportService", ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    this.GetGriddata = function (Customized) {
        deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LeaseCustomizedReport/GetCustomizedDetails', Customized)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
    this.getProjects = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LeaseCustomizedReport/getProjects')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };
}]);
app.controller('LeaseCustomizedReportController', ['$scope', '$q', '$http', 'LeaseCustomizedReportService', 'UtilityService', '$timeout', '$filter', function ($scope, $q, $http, LeaseCustomizedReportService, UtilityService, $timeout, $filter) {
    $scope.Customized = {};
    $scope.Request_Type = [];
    $scope.GridVisiblity = false;
    $scope.GridVisiblity2 = false;
    $scope.DocTypeVisible = 0;
    $scope.Columns = [];
    $scope.Country = [];
    $scope.City = [];
    $scope.Locations = [];
    $scope.Towers = [];
    $scope.Floors = [];
    $scope.CompanyVisible = 0;
    $scope.Company = [];
    $scope.CNP_NAME = [];


    $scope.Pageload = function () {

        UtilityService.getCountires(2).then(function (response) {
            if (response.data != null) {
                $scope.Country = response.data;
                angular.forEach($scope.Country, function (value, key) {
                    value.ticked = true;
                });

                UtilityService.getCities(2).then(function (response) {
                    if (response.data != null) {
                        $scope.City = response.data;
                        angular.forEach($scope.City, function (value, key) {
                            value.ticked = true;
                        });
                        UtilityService.getLocations(2).then(function (response) {
                            if (response.data != null) {
                                $scope.Locations = response.data;
                                angular.forEach($scope.Locations, function (value, key) {
                                    value.ticked = true;
                                })
                            }
                            LeaseCustomizedReportService.getProjects(parseInt(CompanySession)).then(function (response) {
                                //console.log(response);
                                if (response.data != null) {
                                    $scope.proprty = response.data;
                                    //console.log($scope.proprty);
                                    angular.forEach($scope.proprty, function (value, key) {
                                        //var a = _.find($scope.Entity, { CHE_CODE: parseInt(CompanySession) });
                                        value.ticked = true;
                                        //$scope.Customized.CHE_NAME.push(a);
                                    });
                                }
                            });
                            UtilityService.GetCompanies().then(function (response) {
                                if (response.data != null) {
                                    console.log(CompanySession);
                                    $scope.Company = response.data;
                                    $scope.CNP_NAME = []
                                    angular.forEach($scope.Company, function (value, key) {
                                        var a = _.find($scope.Company, { CNP_ID: parseInt(CompanySession) });
                                        a.ticked = true;
                                        $scope.Customized.CNP_NAME.push(a);
                                    });
                                    if (CompanySession == "1") { $scope.CompanyVisible = 0; }
                                    else { $scope.CompanyVisible = 1; }

                                    setTimeout(function () { $scope.LoadData(); }, 1000);
                                }
                                // progress(0, 'Loading...', false);
                            });
                            UtilityService.getChildEntity(parseInt(CompanySession)).then(function (response) {
                                //console.log(response);
                                if (response.data != null) {
                                    $scope.Entity = response.data;
                                    //console.log($scope.Entity);
                                    angular.forEach($scope.Entity, function (value, key) {
                                        //var a = _.find($scope.Entity, { CHE_CODE: parseInt(CompanySession) });
                                        value.ticked = true;
                                        //$scope.Customized.CHE_NAME.push(a);
                                    });
                                }
                            });
                            

                        });
                    }
                });
            }
        });



    }

    $scope.getCitiesbyCny = function () {
        UtilityService.getCitiesbyCny($scope.Customized.Country, 2).then(function (response) {
            $scope.City = response.data;
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cnySelectAll = function () {
        $scope.Customized.Country = $scope.Country;
        $scope.getCitiesbyCny();
    }

    $scope.getLocationsByCity = function () {
        UtilityService.getLocationsByCity($scope.Customized.City, 2).then(function (response) {
            $scope.Locations = response.data;
        }, function (error) {
            console.log(error);
        });
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });
    }

    $scope.ctySelectAll = function () {
        $scope.Customized.City = $scope.City;
        $scope.getLocationsByCity();
    }


    $scope.locSelectAll = function () {
        $scope.Customized.Locations = $scope.Locations;
        $scope.LocationChange();
    }

    $scope.LocationChange = function () {
        angular.forEach($scope.Country, function (value, key) {
            value.ticked = false;
        });
        angular.forEach($scope.City, function (value, key) {
            value.ticked = false;
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cny = _.find($scope.Country, { CNY_CODE: value.CNY_CODE });
            if (cny != undefined && value.ticked == true) {
                cny.ticked = true;
                $scope.Customized.Country[0] = cny;
            }
        });

        angular.forEach($scope.Locations, function (value, key) {
            var cty = _.find($scope.City, { CTY_CODE: value.CTY_CODE });
            if (cty != undefined && value.ticked == true) {
                cty.ticked = true;
                $scope.Customized.City[0] = cty;
            }
        });
    }


    $scope.Cols = [

        { COL: "Lease Name", value: "LEASE_NAME",width:200, ticked: false },
        //{ COL: "Lease Type", value: "PN_LEASE_TYPE", ticked: false },
        { COL: "Lease Start Date", value: "LEASE_START_DATE", template: '<span>{{data.REQ_DATE | date:"dd-MM-yyyy"}}</span>', ticked: false },
        { COL: "Lease End Date", value: "LEASE_END_DATE", template: '<span>{{data.REQ_DATE | date:"dd-MM-yyyy"}}</span>', ticked: false },

        { COL: "Country", value: "COUNTRY", ticked: false },
        { COL: "City", value: "CITY", ticked: false },
        { COL: "Location", value: "LOCATION", ticked: false },


        { COL: "Landlord Name", value: "LANDLORD_NAME", ticked: false },
        { COL: "Landlord Address", value: "LANDLORD_ADDRESS", ticked: false },
        { COL: "Lease Monthly Rent", value: "MONTHLY_RENT", ticked: false },
        //{ COL: "Lesse Name", value: "EMPLOYEE_NAME", ticked: false },
        //{ COL: "Amount Paid", value: "PAID_AMOUNT", ticked: false },


        { COL: "Property Type", value: "PN_PROPERTYTYPE", ticked: false },
        //{ COL: "Builtup area", value: "BUILTUP_AREA", ticked: false },
        //{ COL: "Property Address", value: "COMPLETE_ADDRESS", ticked: false },

        { COL: "Security Deposit", value: "SECURITY_DEPOSIT", ticked: false },
        { COL: "Maintenance Charges", value: "MAINTENANCE_CHARGES", ticked: false },
        { COL: "Service Tax", value: "SERVICE_TAX", ticked: false },
        //{ COL: "Rent Revision", value: "RENT_REVISION", ticked: false },
        { COL: "Lease Escalation", value: "LEASE_ESCALATION", ticked: false },

    ];

    $scope.columnDefs = [
        { headerName: "Lease name", field: "LEASE_NAME", cellClass: 'grid-align', width: 130, },
        { headerName: "Entity", field: "SUB_GROUP", cellClass: 'grid-align', width: 100, },
        { headerName: "Country", field: "STATE", width: 100, cellClass: 'grid-align', width: 100, },
        //{ headerName: "State", field: "STATE", cellClass: 'grid-align', suppressMenu: true, width: 100 },
        { headerName: "City", field: "CITY", cellClass: 'grid-align', width: 100, suppressMenu: false },
        { headerName: "Location", field: "LOCATION", cellClass: 'grid-align', width: 130, suppressMenu: false },
        { headerName: "Branch Code", field: "branch_code", width: 100 },
        { headerName: "Property name", field: "PM_PPT_NAME", cellClass: 'grid-align', width: 130, },
        { headerName: "Property Address", field: "PM_PPT_ADDRESS", cellClass: 'grid-align', width: 130, },
        { headerName: "Property Type", field: "PN_PROPERTYTYPE", cellClass: 'grid-align', width: 100 },
        { headerName: "Request/Type of Agreement", field: "Request_type", cellClass: 'grid-align', width: 100 },
        { headerName: "Agreement Type", field: "AGREEMENT_TYPE", cellClass: 'grid-align', width: 90, },
        { headerName: "Pooja/Inauguration Date", field: "PPT_ESTD", template: '<span>{{data.PPT_ESTD | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },

        //{ headerName: "Agreement Type", field: "agreement_type", cellClass: 'grid-align', width: 100 },
        { headerName: "Agreement Start Date", field: "AGREE_START_DATE", template: '<span>{{data.AGREE_START_DATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Lease Start Date", field: "LEASE_START_DATE", template: '<span>{{data.LEASE_START_DATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Lease End Date", field: "LEASE_END_DATE", template: '<span>{{data.LEASE_END_DATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Lease Extended Date", field: "EXTESNION_TODATE", template: '<span>{{data.EXTESNION_TODATE | date:"dd-MM-yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: false, },




        { headerName: "Landlord name", field: "LANDLORD_NAME", cellClass: 'grid-align', width: 170, },
        { headerName: "GST Applicable", field: "GST", cellClass: 'grid-align', width: 100, },
        { headerName: "Landlord GST No.", field: "GST_NO", cellClass: 'grid-align', width: 150, suppressMenu: true, },
        //{ headerName: "Landlord Address", field: "LANDLORD_ADDRESS", cellClass: 'grid-align', width: 200, suppressMenu: true, hide: true },
        { headerName: "Lease Monthly Rent", field: "MONTHLY_RENT", cellClass: 'grid-align', width: 90, },
        { headerName: "Security Deposit", field: "SECURITY_DEPOSIT", cellClass: 'grid-align', width: 130, suppressMenu: false, },
        //{ headerName: "Lesse Name", field: "lease_payment_terms", cellClass: 'grid-align', width: 130 },
        //{ headerName: "Amount Paid", field: "PAID_AMOUNT", cellClass: 'grid-align', width: 130 },


        { headerName: "Carpet Area[Sqft]", field: "CARPET_AREA", cellClass: 'grid-align', width: 130 },
        { headerName: "Rentable Area[Sqft]", field: "RENT_AREA", cellClass: 'grid-align', width: 130 },
        { headerName: "BuiltUp Area[Sqft]", field: "BUILT_UP", cellClass: 'grid-align', width: 130 },


        //{ headerName: "Builtup area", field: "BUILTUP_AREA", cellClass: 'grid-align', width: 130, aggFunc: 'sum' },
        { headerName: "Property Address", field: "COMPLETE_ADDRESS", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },


        { headerName: "Maintenance charges", field: "MAINTENANCE_CHARGES", cellClass: 'grid-align', width: 130, suppressMenu: false, },
        { headerName: "Amenities Charges", field: "Amenities", cellClass: 'grid-align', width: 130, suppressMenu: true, },
        { headerName: "Total Rent", field: "Total_Rent", cellClass: 'grid-align', width: 100 },
        //{ headerName: "Rent Revision", field: "RENT_REVISION", cellClass: 'grid-align', width: 130, suppressMenu: true, hide: true },
        { headerName: "Lease Escalation", field: "LEASE_ESCALATION", cellClass: 'grid-align', width: 80, },
        { headerName: "Rent Change Date", field: "ESCLATIONDATE", template: '<span>{{data.ESCLATIONDATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 100, },
        //{ headerName: "Escalation Percent", field: "ESCPERCENT", cellClass: 'grid-align', width: 70, },
        { headerName: "Payment Terms", field: "lease_payment_terms", cellClass: 'grid-align', width: 100 },
        { headerName: "Rent Payable", field: "Rent_type", cellClass: 'grid-align', width: 100 },
        { headerName: "Lease Status", field: "LEASE_STATUS", cellClass: 'grid-align', width: 90, },
        { headerName: "Requisition Status", field: "REQ_STATUS", cellClass: 'grid-align', width: 90, },
        { headerName: "Inspected By", field: "PM_PPT_INSPECTED_BY", cellClass: 'grid-align', width: 90, },
        { headerName: "Inspected Date", field: "PM_PPT_INSPECTED_DT", template: '<span>{{data.PM_PPT_INSPECTED_DT | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 90, },
        { headerName: "Notice Period", field: "NOT_PIRED", cellClass: 'grid-align', width: 100 },
        { headerName: "Lock In Period", field: "LOC_IN", cellClass: 'grid-align', width: 100 },
        { headerName: "Rent Free Period", field: "RENT_FREE", cellClass: 'grid-align', width: 120 },
        { headerName: "Lease Remarks", field: "PM_LES_REMARKS", cellClass: 'grid-align', width: 130, },
        { headerName: "Renewal Lease Remarks", field: "PM_LES_REN_L1_REMARKS", cellClass: 'grid-align', width: 130, },
        { headerName: "Lease Extension Remarks", field: "PM_LE_REMARKS", cellClass: 'grid-align', width: 130, },

        { headerName: "Surrender Date", field: "SURRENDER_DATE", template: '<span>{{data.SURRENDER_DATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: true, },
        { headerName: "Surrender Refund Date", field: "SURRENDER_REFUNDDATE", template: '<span>{{data.SURRENDER_REFUNDDATE | date:"dd MMM, yyyy"}}</span>', cellClass: 'grid-align', width: 130, suppressMenu: true, },
        { headerName: "Surrender out Standing Amount", field: "S_OUT_STANDINGAMOUNT", cellClass: 'grid-align', width: 90, },

        { headerName: "Surrender Lease Remarks", field: "PM_SL_REMARKS", cellClass: 'grid-align', width: 130, },

        { headerName: "Signed By POA", field: "PM_PPT_POA", cellClass: 'grid-align', width: 90, },
        { headerName: "Signage Length", field: "PM_PPT_SIG_LENGTH", cellClass: 'grid-align', width: 90, },
        { headerName: "Signage Width", field: "PM_PPT_SIG_WIDTH", cellClass: 'grid-align', width: 90, },
        { headerName: "AC Outdoor", field: "PM_PPT_AC_OUTDOOR", cellClass: 'grid-align', width: 90, },
        { headerName: "Space for GSB", field: "PM_PPT_GSB", cellClass: 'grid-align', width: 90, },
        { headerName: "Zone", field: "Zone", cellClass: 'grid-align', width: 90, },
        { headerName: "Region", field: "rgn_code", cellClass: 'grid-align', width: 90, },
        { headerName: "COMPANY", field: "COMPANYID", cellClass: 'grid-align', width: 90, },
        
        //{ headerName: "Status", field: "STATUS", cellClass: 'grid-align', width: 100, },
        //{ headerName: "PM_SL_POSS_LETR_NAME", field: "PM_SL_POSS_LETR_NAME", template:'<span></span>',cellClass: 'grid-align', width: 100 }

        // { headerName: "Lease Renewal Staus", field: "RENEWAL_STATUS", cellClass: 'grid-align', width: 100, suppressMenu: true, }
    ];

    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
        if (value) {
            $scope.DocTypeVisible = 1
        }
        else { $scope.DocTypeVisible = 0 }
    }
    //$("#filtertxt").change(function () {
    //    onFilterChanged($(this).val());
    //}).keydown(function () {
    //    onFilterChanged($(this).val());
    //}).keyup(function () {
    //    onFilterChanged($(this).val());
    //}).bind('paste', function () {
    //    onFilterChanged($(this).val());
    //})
    $scope.gridOptions = {
        columnDefs: $scope.columnDefs,
        enableCellSelection: false,
        enableFilter: false,
        enableSorting: true,
        enableColResize: true,
        showToolPanel: true,
        groupAggFunction: groupAggFunction,
        groupHideGroupColumns: true,
        groupColumnDef: {
            headerName: "Country", field: "COUNTRY",
            cellRenderer: {
                renderer: "group"
            }
        },
        angularCompileRows: true,
        onAfterFilterChanged: function () {
            if (angular.equals({}, $scope.gridOptions.api.getFilterModel()))
                $scope.DocTypeVisible = 0;
            else
                $scope.DocTypeVisible = 1;
        }
    };

    function groupAggFunction(rows) {
        var sums = {
            BUILTUP_AREA: 0
        };
        rows.forEach(function (row) {
            var data = row.data;
            sums.BUILTUP_AREA += parseFloat((data.BUILTUP_AREA).toFixed(2));
        });
        return sums;
    }

    //setTimeout(function () {
    //    progress(0, 'Loading...', true);


    // }, 500);
    
    $scope.LoadData = function () {
        progress(0, 'Loading...', true);
        var searchval = $("#filtertxt").val();
        $("#btLast").hide();
        $("#btFirst").hide();

        var dataSource = {
            rowCount: null,

            getRows: function (params)
            {
                var params = {
                    SearchValue: searchval,
                    PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
                    PageSize: 10,
                    CNP_NAME: $scope.Customized.CNP_NAME[0].CNP_ID,
                    FromDate: $scope.Customized.FromDate,
                    ToDate: $scope.Customized.ToDate,
                    loclst: $scope.Customized.Locations,
                    entitylst: $scope.Customized.Entity,
                    prptystats: $scope.Customized.proprty
                };
                LeaseCustomizedReportService.GetGriddata(params).then(function (data) {
            
                    $scope.gridata = data.data;
                    if ($scope.gridata == null) {
                        $("#btNext").attr("disabled", true);
                        $scope.GridVisiblity = false;
                        $scope.GridVisiblity2 = true;
                        $scope.gridOptions.api.setRowData([]);

                    }
                    else {
                        //progress(0, 'Loading...', true);
                        $scope.GridVisiblity = true;
                        $scope.GridVisiblity2 = true;

                        $scope.gridOptions.api.refreshHeader();
                        $scope.gridOptions.api.setRowData($scope.gridata);
                        var cols = [];
                        var unticked = _.filter($scope.Cols, function (item) {
                            return item.ticked == false;
                        });
                        var ticked = _.filter($scope.Cols, function (item) {
                            return item.ticked == true;
                        });
                        for (i = 0; i < unticked.length; i++) {
                            cols[i] = unticked[i].value;
                        }
                        $scope.gridOptions.columnApi.setColumnsVisible(cols, false);
                        cols = [];
                        for (i = 0; i < ticked.length; i++) {
                            cols[i] = ticked[i].value;
                        }
                        progress(0, '', false);
                        $scope.gridOptions.columnApi.setColumnsVisible(cols, true);
                    }
                    progress(0, '', false);
                });

            }

        }

        $scope.gridOptions.api.setDatasource(dataSource);
    };
    $scope.GenerateFilterPdf = function () {
        progress(0, 'Loading...', true);
        var columns = [{ title: "Country", key: "COUNTRY" }, { title: "City", key: "CITY" },
        { title: "Location", key: "LOCATION" }, { title: "Lease name", key: "LEASE_NAME" }, { title: "Landlord Name", key: "LANDLORD_NAME" }, { title: "Landlord Address", key: "LANDLORD_ADDRESS" },
        { title: "Lease Type", key: "PN_LEASE_TYPE" }, { title: "Property Type", key: "PN_PROPERTYTYPE" }, { title: "Month;y Rent", key: "MONTHLY_RENT" }, { title: "Builtup Area", key: "BUILTUP_AREA" }
        ];
        var model = $scope.gridOptions.api.getModel();
        var data = [];
        model.forEachNodeAfterFilter(function (node) {
            data.push(node.data);
        });
        var jsondata = JSON.parse(JSON.stringify(data));
        var doc = new jsPDF("landscape", "pt", "a4");
        doc.autoTable(columns, jsondata);
        doc.save("LeaseCustomizableReport.pdf");
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);
        var Filterparams = {
            skipHeader: false,
            skipFooters: false,
            skipGroups: false,
            allColumns: false,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "LeaseCustomizableReport.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Customized, Type) {
        var searchval = $("#filtertxt").val();
        progress(0, 'Loading...', true);

        var params = {
            SearchValue: searchval,
            PageNumber: $scope.gridOptions.api.grid.paginationController.currentPage + 1,
            PageSize: $scope.gridata[0].OVERALL_COUNT,
            CNP_NAME: $scope.Customized.CNP_NAME[0].CNP_ID,
            FromDate: $scope.Customized.FromDate,
            ToDate: $scope.Customized.ToDate,
            loclst: $scope.Customized.Locations,
            entitylst: $scope.Customized.Entity,
            prptystats: $scope.Customized.proprty,
            Type: Type,
            Request_Type : 2
        };

        //Customized.Type = Type;
        //Customized.Request_Type = 2;
        //Customized.SearchValue = searchval;
        //Customized.PageNumber = $scope.gridOptions.api.grid.paginationController.currentPage + 1;
        //Customized.PageSize = $scope.gridata[0].OVERALL_COUNT;
        //Customized.loclst = Customized.Locations;
        //Customized.entitylst = $scope.Customized.Entity;
        //Customized.prptystats= $scope.Customized.proprty
        if ($scope.gridOptions.api.isAnyFilterPresent($scope.columnDefs)) {
            if (Type == "pdf") {
                $scope.GenerateFilterPdf();
            }
            else {
                $scope.GenerateFilterExcel();
            }
        }
        else {
            $http({
                url: UtilityService.path + '/api/LeaseCustomizedReport/GetCustomizedData',
                method: 'POST',
                data: params,
                responseType: 'arraybuffer'

            }).then(function (data, status, headers, config) {

                var file = new Blob([data.data], {
                    type: 'application/' + Type
                });
                var fileURL = URL.createObjectURL(file);
                $("#reportcontainer").attr("src", fileURL);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'LeaseCustomizableReport.' + Type;
                document.body.appendChild(a);
                a.click();
                progress(0, '', false);
            })(function (error,data, status, headers, config) {

            });
        };
    }
    angular.forEach($scope.Cols, function (value, key) {
        value.ticked = true;
    });

    $scope.selVal = "THISYEAR";
    $scope.rptDateRanges = function () {
        switch ($scope.selVal) {
            case 'TODAY':
                $scope.Customized.FromDate = moment().format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'YESTERDAY':
                $scope.Customized.FromDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'days').format('MM/DD/YYYY');
                break;
            case '7':
                $scope.Customized.FromDate = moment().subtract(6, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case '30':
                $scope.Customized.FromDate = moment().subtract(29, 'days').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().format('MM/DD/YYYY');
                break;
            case 'THISMONTH':
                $scope.Customized.FromDate = moment().startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('month').format('MM/DD/YYYY');
                break;
            case 'LASTMONTH':
                $scope.Customized.FromDate = moment().subtract(1, 'month').startOf('month').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().subtract(1, 'month').endOf('month').format('MM/DD/YYYY');
                break;
            case 'THISYEAR':
                $scope.Customized.FromDate = moment().startOf('year').format('MM/DD/YYYY');
                $scope.Customized.ToDate = moment().endOf('year').format('MM/DD/YYYY');
                break;
        }
    }
    $scope.Pageload();

}]);
