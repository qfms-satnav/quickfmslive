﻿
app.service('LocUtilityService', ['$http', '$q', 'UtilityService', function ($http, $q, UtilityService) {

    var deferred = $q.defer();

    this.SaveData = function (Ddata) {
        var deferred = $q.defer();
        return $http.post(UtilityService.path + '/api/LocUtility/SaveData', Ddata)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.BindLocation = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LocUtility/BindLocation')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.BindExpenseHead = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LocUtility/BindExpenseHead')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.BindDepartment = function () {
        var deferred = $q.defer();
        return $http.get(UtilityService.path + '/api/LocUtility/BindDepartment')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.BindUtilityExpenseGrid = function () {
        var deferred = $q.defer();
        //  return $http.post(UtilityService.path + '/api/LocUtility/BindUtilityExpenseGrid')
        return $http.get(UtilityService.path + '/api/LocUtility/BindUtilityExpenseGrid')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.GetUtilityDetails = function (update) {

        var deferred = $q.defer();
        //return $http.post(UtilityService.path + '/api/LocUtility/GetUtilityDetails', update)
        return $http.get(UtilityService.path + '/api/LocUtility/GetUtilityDetails', update)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    //this.UpdateExpUtilityDetails = function (modify1) {
    this.UpdateExpUtilityDetails = function (modify) {
        var deferred = $q.defer();
        //return $http.post(UtilityService.path + '/api/LocUtility/UpdateExpUtilityDetails', modify1)
        return $http.post(UtilityService.path + '/api/LocUtility/UpdateExpUtilityDetails', modify)
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });
    };

    this.ExcelDownload = function () {

        var deferred = $q.defer();

        return $http.post(UtilityService.path + '/api/LocUtility/ExcelDownload')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    };
    this.LocationExcelDownload = function () {

        var deferred = $q.defer();

        return $http.post(UtilityService.path + '/api/LocUtility/LocationExcelDownload')
            .then(function (response) {
                deferred.resolve(response.data);
                return deferred.promise;
            }, function (response) {
                deferred.reject(response);
                return deferred.promise;
            });

    };

}]);


app.controller('LocUtilityController', ['$scope', '$q', 'LocUtilityService', 'UtilityService', '$timeout', '$http', function ($scope, $q, LocUtilityService, UtilityService, $timeout, $http) {

    $scope.StaDet = [{ Id: 1, Name: 'Active' }, { Id: 0, Name: 'Inactive' }];
    $scope.SaveExpUtility = {};
    $scope.LocUtilityList = [];
    $scope.ExpenseUtility = [];
    $scope.ExpenseMonth = [];
    $scope.UserList = [];
    $scope.ActionStatus = 0;
    $scope.GridVisible = 0;
    $scope.IsInEdit = false;
    $scope.Edit = false;
    $scope.ShowMessage = false;
    $scope.SaveExpUtility.STATUS = 'New';
    $scope.Month = [];
    $scope.IsUploadGrid = false;
    $scope.LoadData = function (typ) {
        progress(0, 'Loading...', true);

        LocUtilityService.BindLocation().then(function (Ldata) {
            $scope.LocUtilityList = Ldata;
            LocUtilityService.BindExpenseHead().then(function (data) {
                $scope.ExpenseUtility = data;
                LocUtilityService.BindDepartment().then(function (Udata) {
                    $scope.UserList = Udata;
                    LocUtilityService.BindUtilityExpenseGrid().then(function (Gdata) {
                        progress(0, ' ', false);
                        if (typ == 'U')
                            showNotification('success', 8, 'bottom-right', "Data uploaded Successfully.");
                        $scope.IsUploadGrid = false;
                        $scope.gridata = Gdata;
                        $scope.gridOptions.api.setRowData(Gdata);
                        setTimeout(function () {
                            $('.selectpicker').selectpicker('refresh');
                            progress(0, ' ', false);
                        }, 700);
                    });
                });
            });
        });
    }

    $scope.BindGrid = function () {
        LocUtilityService.BindUtilityExpenseGrid().then(function (Gdata) {
            $scope.IsUploadGrid = false;
            $scope.gridata = Gdata;
            $scope.gridOptions.api.setRowData([]);
            $scope.gridOptions.api.setRowData($scope.gridata);
            $scope.gridOptions.api.refreshHeader();
            $scope.tempAssetsobj = Gdata;
        });
    }
    $scope.GetUtilityDetails = function () {

        if ($scope.SaveExpUtility.LCM_CODE != "" && $scope.SaveExpUtility.LCM_CODE != undefined) {

            if ($scope.SaveExpUtility.EXP_CODE[0].EXP_CODE != "" && $scope.SaveExpUtility.EXP_CODE[0].EXP_CODE != undefined) {
                if ($scope.SaveExpUtility.EXP_CODE[0].EXP_CODE == 'PRST' || $scope.SaveExpUtility.EXP_CODE[0].EXP_CODE == 'ELEC' || $scope.SaveExpUtility.EXP_CODE[0].EXP_CODE == 'SEC' || $scope.SaveExpUtility.EXP_CODE[0].EXP_CODE == 'FAC') {
                    $scope.FirstValue = true;
                }
                else {
                    $scope.FirstValue = false;
                }


                //if ($scope.SaveExpUtility.FromDate != undefined) {
                //    LocUtilityService.GetUtilityDetails($scope.SaveExpUtility).then(function (response) {
                //        $scope.SaveExpUtility = response.LocWiseUtilityModel;
                //        setTimeout(function () {
                //            $('#DEP_CODE').selectpicker('refresh');
                //        }, 100);
                //        //$scope.GetUtilityDetails();
                //    });
                //}
            }
        }
    }
    $scope.EditFunction = function (Gdata) {
        $scope.SaveExpUtility.STATUS = 'Modify';

        $scope.LocUtilityList.forEach(function (item) {
            if (item.LCM_CODE === Gdata.LCM_CODE) {
                item.ticked = true;
            } else {
                item.ticked = false;
            }
        });

        //$scope.SaveExpUtility.LCM_CODE = Gdata.LCM_CODE; 

        //$scope.SaveExpUtility.EXP_CODE = Gdata.EXP_CODE;

        $scope.ExpenseUtility.forEach(function (item) {
            if (item.EXP_CODE === Gdata.EXP_CODE) {
                item.ticked = true;
            } else {
                item.ticked = false;
            }
        });
        $scope.SaveExpUtility.UTIL_ID = Gdata.UTIL_ID;
        $scope.SaveExpUtility.ExpenseMonth = new Date(moment(Gdata.ExpenseMonth).format('DD/MM/YYYY'));     //new Date(Gdata.ExpenseMonth);
        $scope.SaveExpUtility.FromDate = Gdata.FromDate;
        $scope.SaveExpUtility.BILL_NO = Gdata.BILL_NO;
        $scope.SaveExpUtility.BILL_INVOICE = Gdata.BILL_INVOICE;
        $scope.SaveExpUtility.AMT = Gdata.AMT;
        $scope.SaveExpUtility.VENDOR = Gdata.VENDOR;
        $scope.SaveExpUtility.VEN_PHNO = Gdata.VEN_PHNO;
        $scope.SaveExpUtility.VEN_MAIL = Gdata.VEN_MAIL;
        $scope.SaveExpUtility.CLIENTREMARKS = Gdata.CLIENTREMARKS;
        //$scope.SaveExpUtility.DEP_CODE = Gdata.DEP_CODE;

        $scope.UserList.forEach(function (item) {
            if (item.DEP_CODE === Gdata.DEP_CODE) {
                item.ticked = true;
            } else {
                item.ticked = false;
            }
        });
        $scope.IsInEdit = true;
        $scope.Edit = true;
        //if ($scope.SaveExpUtility.EXP_CODE != "" && $scope.SaveExpUtility.EXP_CODE != undefined) {
        //    if ($scope.SaveExpUtility.EXP_CODE == 'PRST' || $scope.SaveExpUtility.EXP_CODE == 'ELEC') {
        if (Gdata.EXP_CODE != "" && Gdata.EXP_CODE != undefined) {
            if (Gdata.EXP_CODE == 'PRST' || Gdata.EXP_CODE == 'ELEC' || Gdata.EXP_CODE == 'SEC' || Gdata.EXP_CODE == 'FAC') {
                $scope.FirstValue = true;
            }
            else {
                $scope.FirstValue = false;
            }
        }
        // $scope.element(document.querySelector("#LCM_CODE")).addClass("selectpicker");
    }
    //TO SAVE THE DATA
    $scope.SubmitData = function () {

        var valueArr = angular.copy($scope.gridata);
        //var bool = false;
        //for (var i in valueArr) {
        //    if (valueArr[i].BILL_INVOICE == BILL_INVOICE.value) {
        //        showNotification('error', 8, 'bottom-right', 'invoice Should Not be Same');
        //        bool = true;
        //        break;
        //    }
        //}
        //if (!bool)
        {


            $scope.SaveExpUtility.LCM_CODE = $scope.SaveExpUtility.LCM_CODE[0].LCM_CODE;
            $scope.SaveExpUtility.EXP_CODE = $scope.SaveExpUtility.EXP_CODE[0].EXP_CODE;
            $scope.SaveExpUtility.DEP_CODE = $scope.SaveExpUtility.DEP_CODE[0].DEP_CODE;
            $scope.SaveExpUtility.FromDate = moment($scope.SaveExpUtility.FromDate, "DD-MM-YYYY").format("MM-DD-YYYY")
            LocUtilityService.SaveData($scope.SaveExpUtility).then(function (response) {

                if (response.data != null) {
                    $scope.ShowMessage = true;
                    $scope.Success = "Data Successfully Inserted ";
                    var savedobj = {};
                    $scope.SaveExpUtility.FromDate = moment($scope.SaveExpUtility.FromDate, "MM-DD-YYYY").format("DD-MM-YYYY")
                    angular.copy($scope.SaveExpUtility, savedobj)
                    showNotification('success', 8, 'bottom-right', response.Message);
                    savedobj.ExpenseMonth = moment(savedobj.ExpenseMonth, "MMMM, yyyy").format("MM/DD/YYYY HH:mm:ss")
                    $scope.gridata.unshift(savedobj);
                    $scope.gridOptions.api.setRowData($scope.gridata);
                    LocUtilityService.BindUtilityExpenseGrid().then(function (Gdata) {
                        $scope.IsUploadGrid = false;
                        $scope.gridata = Gdata;
                        $scope.gridOptions.api.setRowData(Gdata);
                        $scope.ClearData();
                        $scope.SaveExpUtility.STATUS = 'New';
                    });

                    setTimeout(function () {
                        $scope.$apply(function () {
                            $scope.ShowMessage = false;
                            //window.location.reload();
                        });
                    }, 500);

                }
                else {
                    showNotification('error', 8, 'bottom-right', response.Message);
                }

            }, function (error) {
                showNotification('error', 8, 'bottom-right', error);
            });
            //$scope.SaveExpUtility.STATUS = 'New';

        }
    }

    $scope.reset = function () {
        $scope.LoadData();

    }

    $scope.UploadFile = function () {

        if ($('#File3', $('#form1')).val()) {
            progress(0, 'Loading...', true);
            var filetype = $('#File3', $('#form1')).val().substring($('#File3', $('#form1')).val().lastIndexOf(".") + 1);
            if (filetype == "xlsx" || filetype == "xls") {
                if (!window.FormData) {
                    redirect(); // if IE8
                }
                else {

                    var formData = new FormData();
                    var UplFile = $('#File3')[0];
                    var CurrObj = $scope.SaveExpUtility;
                    formData.append("ExUpload", UplFile.files[0]);
                    formData.append("CurrObj", JSON.stringify(CurrObj));
                    $.ajax({
                        url: UtilityService.path + "/api/LocUtility/UploadExcel",
                        type: "POST",
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (response) {
                            if (response.data != null) {
                                $scope.gridata = response.data;
                                $scope.gridOptions.api.setRowData([]);
                                $scope.IsUploadGrid = true;
                                $scope.gridOptions.api.setRowData($scope.gridata);
                                //$scope.gridOptions.api.refreshHeader();
                                $scope.tempAssetsobj = response.data;
                                progress(0, '', false);
                                showNotification('success', 8, 'bottom-right', response.Message);
                            }
                            else {

                                progress(0, '', false);
                                showNotification('error', 8, 'bottom-right', response.Message);
                            }
                        }
                    });
                }

                $("#File3").val('');
            }
            else
                progress(0, 'Loading...', false);
            showNotification('error', 8, 'bottom-right', 'Upload Excel File(s) only ');
        }
        else
            showNotification('error', 8, 'bottom-right', 'Select a file to upload');

    }
    $scope.ExcelDownload = function () {

        LocUtilityService.LocationExcelDownload().then(function (response) {
            var a = document.createElement('a');
            a.href = "../../UploadFiles/LocationWiseUtilityReport.xlsx";
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
        });

    }
    function save(blob, fileName) {
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, fileName);
            progress(0, '', false);
        } else {
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
            window.URL.revokeObjectURL(link.href);
            $scope.ToggleDiv = !$scope.ToggleDiv;
            progress(0, '', false);
        }
    }
    //ToUpdate
    $scope.UpdateExpUtilityDetails = function () {
        
        $scope.SaveExpUtility.STATUS = 'Modify';

        //var dt = new Date($scope.SaveExpUtility.ExpenseMonth).getDate();
        //if (dt = 1) {
        //    var result = new Date(new Date($scope.SaveExpUtility.ExpenseMonth).setDate(new Date($scope.SaveExpUtility.ExpenseMonth).getDate() + 1));
        //    $scope.SaveExpUtility.ExpenseMonth = result;
        //}

        var dt = new Date($scope.SaveExpUtility.ExpenseMonth).getDate();
        if (dt != 1) {
            var result = new Date(new Date($scope.SaveExpUtility.ExpenseMonth).setDate(1));
            $scope.SaveExpUtility.ExpenseMonth = result;
        }

        $scope.SaveExpUtility.LCM_CODE = $scope.SaveExpUtility.LCM_CODE[0].LCM_CODE;
        $scope.SaveExpUtility.EXP_CODE = $scope.SaveExpUtility.EXP_CODE[0].EXP_CODE;
        $scope.SaveExpUtility.DEP_CODE = $scope.SaveExpUtility.DEP_CODE[0].DEP_CODE;
        $scope.SaveExpUtility.FromDate = moment($scope.SaveExpUtility.FromDate, "DD-MM-YYYY").format("MM-DD-YYYY")

        LocUtilityService.UpdateExpUtilityDetails($scope.SaveExpUtility).then(function (response) {
            
            if (response.data != null) {
                $scope.ShowMessage = true;
                $scope.Success = "Data Updated Successfully";
                var updateobj = {};
                angular.copy($scope.SaveExpUtility, updateobj)
                showNotification('success', 8, 'bottom-right', response.Message);
                $scope.gridata.unshift(updateobj);
                $scope.gridOptions.api.setRowData($scope.gridata);
                LocUtilityService.BindUtilityExpenseGrid().then(function (Gdata) {
                    $scope.IsUploadGrid = false;
                    $scope.gridata = Gdata;
                    $scope.gridOptions.api.setRowData(Gdata);
                    $scope.ClearData();
                    $scope.SaveExpUtility.STATUS = 'New';
                });

                setTimeout(function () {
                    $scope.$apply(function () {
                        $scope.ShowMessage = false;
                        // window.location.reload();
                    });
                }, 500);



            }
            else {
                showNotification('error', 8, 'bottom-right', response.Message);
            }
        }, function (error) {
            showNotification('error', 8, 'bottom-right', error);
        });
    }

    function bindDate(grid) {
        var s = moment(grid.data.FromDate);

    }

    //for GridView
    var columnDefs = [
        { headerName: "Expense Head", field: "EXP_NAME", width: 200, cellClass: 'grid-align' },
        { headerName: "Location", field: "LCM_NAME", width: 250, cellClass: 'grid-align' },
        { headerName: "Bill Date", field: "FromDate", width: 150, cellClass: 'grid-align' },
        { headerName: "Bill No", field: "BILL_NO", width: 100, cellClass: 'grid-align' },
        { headerName: "Bill Invoice", field: "BILL_INVOICE", width: 200, cellClass: 'grid-align' },
        { headerName: "Amount", field: "AMT", width: 100, cellClass: 'grid-align' },
        { headerName: "Department", field: "DEP_NAME", width: 250, cellClass: 'grid-align' },
        { headerName: "Vendor", field: "VENDOR", width: 150, cellClass: 'grid-align' },
        { headerName: "No of Units/Deployment", field: "VEN_MAIL", width: 150, cellClass: 'grid-align' },
        { headerName: "Remarks", field: "REM", width: 150, cellClass: 'grid-align' },
        { headerName: "Expense Month", field: "ExpenseMonth", width: 200, cellClass: 'grid-align' },
        { headerName: "Edit", width: 70, height: 150, template: '<a ng-click = "EditFunction(data)"> <i class="fa fa-pencil" class="btn btn-default" fa-fw"></i> </a>', cellClass: 'grid-align', suppressMenu: true, hide: $scope.IsUploadGrid }
    ];
    $scope.gridOptions = {
        columnDefs: columnDefs,
        rowData: null,
        enableSorting: true,
        cellClass: 'grid-align',
        angularCompileRows: true,
        enableColResize: true,
        enableCellSelection: false,
        suppressHorizontalScroll: false,
        enableFilter: true,

        //onReady: function () {
        //    $scope.gridOptions.api.sizeColumnsToFit()
        //}
    };
    function onFilterChanged(value) {
        $scope.gridOptions.api.setQuickFilter(value);
    }
    $("#filtertxt").change(function () {
        onFilterChanged($(this).val());
    }).keydown(function () {
        onFilterChanged($(this).val());
    }).keyup(function () {
        onFilterChanged($(this).val());
    }).bind('paste', function () {
        onFilterChanged($(this).val());
    })

    $scope.GenerateFilterExcel = function () {
        progress(0, 'Loading...', true);

        var Filterparams = {

            columnGroups: true,
            allColumns: true,
            onlySelected: false,
            columnSeparator: ',',
            fileName: "utility_report.csv"
        };
        $scope.gridOptions.api.exportDataAsCsv(Filterparams);
        setTimeout(function () {
            progress(0, 'Loading...', false);
        }, 1000);
    }

    $scope.GenReport = function (Type) {
        //var searchval = $("#filtertxt").val();
        //OpexFormat.SearchValue = searchval;
        progress(0, 'Loading...', true);
        if ($scope.IsUploadGrid) {
            $scope.DownloadUploadedExcel();
        }
        else {
            $scope.GenerateFilterExcel();
        }
    }

    $scope.DownloadUploadedExcel = function () {
        var a = document.createElement('a');
        a.href = "../../UploadFiles/LocationWiseUtilityReport.xlsx";
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        progress(0, '', false);
        showNotification('success', 8, 'bottom-right', "File downloaded successfully");
    }

    $scope.ClearAllData = function () {

        var SaveExpUtility;
        $scope.ActionStatus = 0;
        $scope.UTIL_ID = "";
        $scope.IsInEdit = false;
        $scope.SaveExpUtility = {};

        $scope.LocUtilityList = ""
        LocUtilityService.BindLocation().then(function (Ldata) {
            $scope.LocUtilityList = Ldata;
        });
        $scope.ExpenseUtility = ""
        LocUtilityService.BindExpenseHead().then(function (data) {
            $scope.ExpenseUtility = data;
        });

        $scope.ExpenseUtility = ""
        LocUtilityService.BindDepartment().then(function (Udata) {
            $scope.UserList = Udata;
        });

        $scope.ExpenseMonth = [];
        $scope.SaveExpUtility.STATUS = 'New';
        $scope.frmLocationUtility.$submitted = false;
        $scope.FirstValue = false;
        $('.selectpicker').selectpicker('val', '');
        $("#File3").val('');
    }


    $scope.ClearData = function () {

        var SaveExpUtility;
        $scope.ActionStatus = 0;
        $scope.UTIL_ID = "";
        $scope.IsInEdit = false;
        $scope.SaveExpUtility = {};
        $scope.LocUtilityList = ""
        LocUtilityService.BindLocation().then(function (Ldata) {
            $scope.LocUtilityList = Ldata;
        });


        $scope.ExpenseUtility = ""
        LocUtilityService.BindExpenseHead().then(function (data) {
            $scope.ExpenseUtility = data;
        });

        $scope.ExpenseUtility = ""
        LocUtilityService.BindDepartment().then(function (Udata) {
            $scope.UserList = Udata;
        });

        $scope.ExpenseMonth = [];
        $scope.SaveExpUtility.STATUS = 'Modify';
        $scope.frmLocationUtility.$submitted = false;
        $scope.FirstValue = false;
        $('.selectpicker').selectpicker('val', '');
        $("#File3").val('');
    }
    setTimeout(function () {
        $scope.LoadData();
    }, 1000);
}]);