﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

    </script>

    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-body-viewport-wrapper.ag-layout-normal {
            overflow-x: scroll;
            overflow-y: scroll;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }
    </style>
</head>
<body data-ng-controller="OpexFormatReportController" class="amantra">


    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Opex Report</h3>
            </div>
            <div class="card">
                <form id="form1" name="OpexFormatReport" data-valid-submit="LoadData()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': OpexFormatReport.$submitted && OpexFormatReport.CTY_NAME.$invalid}">
                                <label class="control-label">City<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="OpexFormat.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="OpexFormat.City[0]" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="OpexFormatReport.$submitted && OpexFormatReport.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': OpexFormatReport.$submitted && OpexFormatReport.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="OpexFormat.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="OpexFormat.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="OpexFormatReport.$submitted && OpexFormatReport.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': OpexFormatReport.$submitted && OpexFormatReport.CHE_NAME.$invalid}">
                                <label for="txtcode">Entity</label>
                                <div isteven-multi-select data-input-model="Entity" data-output-model="OpexFormat.Entity" data-button-label="icon CHE_NAME" <%--data-is-disabled="EnableStatus==0"--%>
                                    data-item-label="icon CHE_NAME maker" data-tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="OpexFormat.CHE_NAME" name="CHE_NAME" style="display: none" />
                                <span class="error" data-ng-show="OpexFormatReport.$submitted && OpexFormatReport.Entity.$invalid" style="color: red">Please select Entity </span>
                            </div>
                        </div>

                        <%--  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 5px; padding-right: 30px; padding-top: 10px;">
                                         <div class="form-group" data-ng-class="{'has-error': OpexFormatReport.$submitted && OpexFormatReport.name.$invalid}">
                                            <label class="control-label">Year</label>
                                        
                                            <div isteven-multi-select data-input-model="getyears" data-output-model="OpexFormat.getyears" data-button-label="icon name" data-item-label="icon name maker"
                                                data-tick-property="ticked" data-max-labels="1"  data-on-item-click="getyears()" data-on-select-all="" data-on-select-none="" selection-mode="single">
                                               
                                            </div>
                                            <input type="text" data-ng-model="OpexFormat.Year" name="getyears" style="display: none" required="" />
                                           
                                        </div>--%>


                        <%--  <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 5px; padding-right: 30px; padding-top: 10px;">
                                        <div class="form-group" data-ng-class="{'has-error': OpexFormatReport.$submitted && OpexFormatReport.name.$invalid}">
                                            <label class="control-label">Year</label>
                                            <div isteven-multi-select data-input-model="getyears" data-output-model="OpexFormat.getyears" data-button-label="icon name" data-item-label="icon name"
                                                data-tick-property="ticked" data-max-labels="1"  data-on-item-click="PTypeChanged()" selection-mode="single">
                                            </div>
                                            <input type="text" data-ng-model="OpexFormatReport.name" name="name" style="display: none" required="" />
                                            <span class="error" data-ng-show="OpexFormatReport.$submitted && OpexFormatReport.name.$invalid" style="color: red">Please select Project Type </span>
                                        </div>
                                    </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left:15px; padding-right: 30px; padding-top: 10px;">
                                            <div class="form-group" data-ng-class="{'has-error': OpexFormatReport.$submitted && OpexFormatReport.CHE_NAME.$invalid}">
                                                <label for="txtcode">Month</label>
                                                <div isteven-multi-select data-input-model="Month" data-output-model="OpexFormat.Month" data-button-label="icon name" 
                                                    data-item-label="icon name maker" data-tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                          
                                                </div>
                                            </div>
                                        </div>--%>
                        </div>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='FromDate'>
                                    <input type="text" class="form-control" data-ng-model="OpexFormat.FromDate" id="Text1" name="FromDate" required="" placeholder="dd-mm-yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='ToDate'>
                                    <input type="text" class="form-control" data-ng-model="OpexFormat.ToDate" id="Text2" name="ToDate" required="" placeholder="dd-mm-yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('ToDate')"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 100px; padding-right: 30px; padding-top: 10px;">
                            <input type="submit" value="Search" data-ng-click="search()" class="btn btn-primary custom-button-color" />
                        </div>

        </div>
        </form>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12 pull-right">
                            <%--<a data-ng-click="GenReport(OpexFormat,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-middle"></i></a>--%>
                            <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>
                            <%--<a data-ng-click="GenReport(OpexFormat,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>--%>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-6 col-xs-12">
                        <div class="input-group" style="width: 25%">
                            <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                            <div class="input-group-btn">
                                <button class="btn btn-primary custom-button-color" type="submit">
                                    <i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div>
                </div>
            </div>
    </div>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts</div>/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().format('DD-MMM-YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().format('DD-MMM-YYYY')));
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
        });

    </script>

    <script defer>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    </script>
    <script src="../../SMViews/Utility.min.js" defer></script>
    <script src="../../SMViews/Utility.js" defer></script>
    <script src="../JS/OpexFormatReport.js" defer></script>
    <script src="../../BootStrapCSS/Scripts/leaflet/xlsx.full.min.js" defer></script>
    <%--  <script src="../JS/LeaseOpexFormat.js"></script>--%>
    <%--<script src="../JS/LeaseOpexFormat.js"></script>--%>
</body>
</html>

