﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <style>
        /* .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
*/
        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        /*  .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }*/
    </style>
</head>
<body data-ng-controller="LeaseCustomizedReportController" class="amantra">
    <%--    <div id="page-wrapper" class="row" ng-cloak>
        <div class="row form-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <fieldset>
                        <legend>Customizable Report</legend>
                    </fieldset>
                    <div class="well">--%>




    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Lease Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Lease Customizable Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>Lease Customizable Report </h3>
            </div>
            <div class="card">
                <form id="form1" name="CustomizedReport" data-valid-submit="LoadData()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid}">
                                <label class="control-label">Country <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Country" data-output-model="Customized.Country" data-button-label="icon CNY_NAME"
                                    data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CNY_NAME.$invalid" style="color: red">Please select country </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid}">
                                <label class="control-label">City<span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="Customized.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.City[0]" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">**</span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="Customized.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                            </div>
                        </div>
                        <%-- <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid}">
                                        <label class="control-label">Tower <span style="color: red;">**</span></label>
                                        <div isteven-multi-select data-input-model="Towers" data-output-model="Customized.Towers" data-button-label="icon TWR_NAME "
                                            data-item-label="icon TWR_NAME maker" data-on-item-click="getFloorByTower()" data-on-select-all="twrSelectAll()" data-on-select-none="twrSelectNone()"
                                            data-tick-property="ticked" data-max-labels="1">
                                        </div>
                                        <input type="text" data-ng-model="Customized.Towers[0]" name="TWR_NAME" style="display: none" required="" />
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.TWR_NAME.$invalid" style="color: red">Please select tower </span>
                                    </div>
                                </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': CustomizedReport.$submitted && CustomizedReport.Columns.$invalid}">
                                <label class="control-label">Select Columns<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Cols" data-output-model="COL" button-label="icon COL" item-label="icon COL" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="3">
                                </div>
                                <input type="text" data-ng-model="COL" name="Columns" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Columns.$invalid" style="color: red">Please select columns </span>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Company</label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="Customized.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                    item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="Customized.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Company.$invalid" style="color: red">Please select Company </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Entity</label>
                                <div isteven-multi-select data-input-model="Entity" data-output-model="Customized.Entity" button-label="icon CHE_NAME" data-is-disabled="EnableStatus==0"
                                    item-label="icon CHE_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="Customized.CHE_NAME" name="CHE_NAME" style="display: none" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Entity.$invalid" style="color: red">Please select Entity </span>
                            </div>
                        </div>
                        <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">Quick Select</label>
                                            <br />
                                            <select id="ddlRange" data-ng-model="selVal" data-ng-change="rptDateRanges()" class="selectpicker">
                                                <option value="TODAY">Today</option>
                                                <option value="YESTERDAY">Yesterday</option>
                                                <option value="7">Last 7 Days</option>
                                                <option value="30">Last 30 Days</option>
                                                <option value="THISMONTH">This Month</option>
                                                <option value="LASTMONTH">Last Month</option>
                                                <option value="THISYEAR">This Year</option>
                                            </select>
                                        </div>
                                    </div>--%>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">From Date</label>
                                <div class="input-group date" id='FromDate'>
                                    <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                    </span>
                                </div>
                            </div>
                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please select From Date</span>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">To Date</label>
                                <div class="input-group date" id='ToDate'>
                                    <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('ToDate')"></i>
                                    </span>
                                </div>
                            </div>
                            <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please select To Date</span>
                        </div>
                    </div>
                    <br />
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Status</label>
                                <div isteven-multi-select data-input-model="proprty" data-output-model="Customized.proprty" button-label="icon PROJECTNAM" data-is-disabled="EnableStatus==0"
                                    item-label="icon PROJECTNAM" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="Customized.PROJECTNAM" name="PROJECTNAM" style="display: none" />
                                <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.Company.$invalid" style="color: red">Please select Status </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 10px;">
                            <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                        </div>
                    </div>
                    <%--  </form>
                            <form id="form2" style="padding-top: 10px">--%>
                    <div class="form-group">
                        <div class="row" data-ng-show="GridVisiblity">
                            <div class="col-md-12 col-sm-6 col-xs-12 pull-right">

                                <a data-ng-click="GenReport(Customized,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(Customized,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(Customized,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                    </div>
                    </div>
                    <div class="row" data-ng-show="GridVisiblity2">
                        <%--<input type="text" class="selectpicker" id="filtertxt" placeholder="Filter by any..." style="width: 20%; height: 20%" />--%>
                        <div class="col-md-12">
                            <div class="input-group" style="width: 20%">
                                <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                <div class="input-group-btn">
                                    <button class="btn btn-primary custom-button-color" type="submit">
                                        <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                            <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%--  </div>
        </div>
    </div>--%>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
        });

    </script>

    <script src="../../SMViews/Utility.min.js"></script>
    <script src="../JS/LeaseCustomizedReport.js"></script>
    <%--  <script src="../JS/LeaseCustomizedReport.min.js"></script>--%>
</body>
</html>
