﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="frmLeaseRentalandEscaltionReport.aspx.cs" Inherits="PropertyManagement_Views_frmLeaseRentalandEscaltionReport" %>--%>
<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>


<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style>
        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>


<body data-ng-controller="LeaseRentalandEscaltionController as LREC" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3 class="panel-title">Lease Report</h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="LeaseRentalandEscaltionReport">
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': LeaseRentalandEscaltionReport.$submitted && LeaseRentalandEscaltionReport.PM_PPT_NAME.$invalid}">
                                    <label class="control-label">Property Name <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Property" data-output-model="LeaseRentalandEscaltionModel.Property" data-button-label="icon PM_PPT_NAME"
                                        data-item-label="icon PM_PPT_NAME maker" data-on-item-click="getProperty()"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="multiple">
                                    </div>
                                    <input type="text" data-ng-model="LeaseRentalandEscaltionModel.Property[0]" name="PM_PPT_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="LeaseRentalandEscaltionReport.$submitted && LeaseRentalandEscaltionReport.PM_PPT_NAME.$invalid" style="color: red">Please select Property </span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': LeaseRentalandEscaltionReport.$submitted && LeaseRentalandEscaltionReport.PN_PROPERTYTYPE.$invalid}">
                                    <label class="control-label">Request Type </label>
                                    <div isteven-multi-select data-input-model="ProReqType" data-output-model="LeaseRentalandEscaltionModel.ProReqType" data-button-label="icon PN_PROPERTYTYPE"
                                        data-item-label="icon PN_PROPERTYTYPE maker" data-on-item-click="GetReqType()"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" <%--style="padding-left: 5px; padding-right: 30px; padding-top: 10px;"--%>>
                                <div class="form-group" data-ng-class="{'has-error': LeaseRentalandEscaltionReport.$submitted && LeaseRentalandEscaltionReport.name.$invalid}">
                                    <label class="control-label">Year</label>
                                    <div isteven-multi-select data-input-model="getyears" data-output-model="LeaseRentalandEscaltionModel.getyears" data-button-label="icon name" data-item-label="icon name"
                                        data-tick-property="ticked" data-max-labels="1" data-on-item-click="PTypeChanged()" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="LeaseRentalandEscaltionReport.name" name="name" style="display: none" required="" />
                                    <span class="error" data-ng-show="LeaseRentalandEscaltionReport.$submitted && LeaseRentalandEscaltionReport.name.$invalid" style="color: red">Please select year </span>
                                </div>
                            </div>



                            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 17px;">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" ng-click="LoadData()" />
                            </div>
                        </div>
                        <div class="row" data-ng-if="downloadExcel">
                            <download-as-excel xlname="LeaseReport.xls" sheetname="sample" template="../../../LeaseAbstractReport.html" object="LREC"></download-as-excel>
                            <%--<div class="col-md-11 col-sm-6 col-xs-12" id="excel" style="padding-top: 10px">
                                                 <a data-ng-click="GenReport(SubLease,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                                <a data-ng-click="GenReport(SubLease,'xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                                <a data-ng-click="GenReport(SubLease,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                            </div>--%>
                        </div>

                        <div class="row" data-ng-show="display==0">
                            <div class="col-md-12" id="table2">
                                <br />
                                <h4 class="panel-title">Lease Report</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="box" id="GridShow">
                                    <div class="box-danger table-responsive">
                                        <div data-ag-grid="GridOptions" class="ag-blue" style="height: 150px; width: auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" data-ng-show="display==0">
                            <div class="col-md-12" id="table5">
                                <br />
                                <h4 class="panel-title">Lease Escalation Report</h4>
                            </div>

                            <div class="col-md-12">
                                <div class="box" id="GridShow3">
                                    <div class="box-danger table-responsive">
                                        <div data-ag-grid="EscalationGridOption" class="ag-blue" style="height: 150px; width: auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" data-ng-show="display==0">
                            <div class="col-md-12" id="table3">
                                <br />
                                <h4 class="panel-title">Lease Rental Report</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="box" id="GridShow1">
                                    <div class="box-danger table-responsive">
                                        <div data-ag-grid="RentalGridOption" class="ag-blue" style="height: 150px; width: auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" data-ng-show="display==0">
                            <div class="col-md-12" id="table4">
                                <br />
                                <h4 class="panel-title">Sub Lease Report</h4>
                            </div>
                            <div class="col-md-12">
                                <div class="box" id="GridShow4">
                                    <div class="box-danger table-responsive">
                                        <div data-ag-grid="SubLeaseGridOption" class="ag-blue" style="height: 150px; width: auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <%--    <script src="../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../Dashboard/C3/c3.min.js"></script>
    <link href="../../Dashboard/C3/c3.css" rel="stylesheet" />--%>
    <%--    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>--%>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>

    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            //$('#FromDate').datepicker('setDate', new Date(moment().subtract(29, 'days').format('DD-MMM-YYYY')));
            //$('#ToDate').datepicker('setDate', new Date(moment().format('DD-MMM-YYYY')));
            $('#FromDate').datepicker('setDate', new Date(moment().startOf('Month').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().endOf('Month').format('MM/DD/YYYY')));
        }
    </script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../../SMViews/Utility.min.js"></script>
    <script src="../JS/LeaseRentalandEscaltionReport.js"></script>
    <script src="../../Scripts/DownloadExcelScript.js"></script>
    <%--  <script type="text/javascript">
            $(document).ready(function () {
                setDateVals();
            });
        </script>--%>
</body>
</html>

