﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
   <style>
        /*.grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
*/
        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        /*.ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }*/
    </style>

</head>

<body data-ng-controller="SubLeaseReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Sub Lease Report </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>Sub Lease Report</h3>
            </div>
            <div class="card">

                <form id="form1" name="SubLeaseReport" data-valid-submit="LoadData()" novalidate>
                    <div class="clearfix">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': SubLeaseReport.$submitted && SubLeaseReport.CTY_NAME.$invalid}">
                                <label class="control-label">City<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="City" data-output-model="SubLease.City" data-button-label="icon CTY_NAME"
                                    data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SubLease.City[0]" name="CTY_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="SubLeaseReport.$submitted && SubLeaseReport.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                            </div>
                        </div>



                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': SubLeaseReport.$submitted && SubLeaseReport.LCM_NAME.$invalid}">
                                <label class="control-label">Location <span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Locations" data-output-model="SubLease.Locations" data-button-label="icon LCM_NAME"
                                    data-item-label="icon LCM_NAME maker" data-on-item-click="getTowerByLocation()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SubLease.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="SubLeaseReport.$submitted && SubLeaseReport.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': SubLeaseReport.$submitted && SubLeaseReport.CNP_NAME.$invalid}">
                                <label class="control-label">Company<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="Company" data-output-model="SubLease.Company" data-button-label="icon CNP_NAME"
                                    data-item-label="icon CNP_NAME maker" data-on-select-all="cpySelectAll()" data-on-select-none="cpySelectNone()"
                                    data-tick-property="ticked" data-max-labels="1">
                                </div>
                                <input type="text" data-ng-model="SubLease.Company[0]" name="CNP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="SubLeaseReport.$submitted && SubLeaseReport.CNP_NAME.$invalid" style="color: red">Please select a company </span>
                            </div>
                        </div>

                        <div class="col-md-11 col-sm-6 col-xs-12">
                            <br />
                            <div class="box-footer text-right">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-11 col-sm-6 col-xs-12" id="table2" data-ng-show="GridVisiblity" style="padding-top: 10px">
                                <a data-ng-click="GenReport(SubLease,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(SubLease,'xlsx')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                <a data-ng-click="GenReport(SubLease,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12"  style="overflow-x: scroll;">
                            <div id="Tabular">
                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 15%" />
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
    </div>
    </div>
            <%--</div>
        </div>--%>
    <%--  </div>--%>


    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../Scripts/jspdf.min.js"></script>
    <link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Dashboard/C3/c3.min.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';

    </script>

    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/SubLeaseReport.js"></script>
</body>
</html>
