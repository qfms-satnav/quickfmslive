﻿<%@ Page Language="C#" AutoEventWireup="true" %>


<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <style>
        /* .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }
        .panel > .panel-body {
            padding: 15px 10px;
        }
        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }
*/
        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        /*   .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }*/
    </style>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>

<body data-ng-controller="LeaseReportController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <%--   <div class="widgets">
                <div ba-panel ba-panel-title="Lease Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Landlord Wise Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>Landlord Wise Report </h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="LeaseReport" data-valid-submit="LoadData()" novalidate>
                    <div class="clearfix">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': LeaseReport.$submitted && LeaseReport.CNY_NAME.$invalid}">
                                    <label class="control-label">Country <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Country" data-output-model="LeaseRep.Country" data-button-label="icon CNY_NAME"
                                        data-item-label="icon CNY_NAME maker" data-on-item-click="getCitiesbyCny()" data-on-select-all="cnySelectAll()" data-on-select-none="cnySelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="LeaseRep.Country[0]" name="CNY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="LeaseReport.$submitted && LeaseReport.CNY_NAME.$invalid" style="color: red">Please select country </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': LeaseReport.$submitted && LeaseReport.CTY_NAME.$invalid}">
                                    <label class="control-label">City<span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="City" data-output-model="LeaseRep.City" data-button-label="icon CTY_NAME"
                                        data-item-label="icon CTY_NAME maker" data-on-item-click="getLocationsByCity()" data-on-select-all="ctySelectAll()" data-on-select-none="ctySelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="LeaseRep.City[0]" name="CTY_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="LeaseReport.$submitted && LeaseReport.CTY_NAME.$invalid" style="color: red">Please select a city </span>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group" data-ng-class="{'has-error': LeaseReport.$submitted && LeaseReport.LCM_NAME.$invalid}">
                                    <label class="control-label">Location <span style="color: red;">**</span></label>
                                    <div isteven-multi-select data-input-model="Locations" data-output-model="LeaseRep.Locations" data-button-label="icon LCM_NAME"
                                        data-item-label="icon LCM_NAME maker" data-on-item-click="LocationChange()" data-on-select-all="locSelectAll()" data-on-select-none="lcmSelectNone()"
                                        data-tick-property="ticked" data-max-labels="1">
                                    </div>
                                    <input type="text" data-ng-model="LeaseRep.Locations[0]" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="LeaseReport.$submitted && LeaseReport.LCM_NAME.$invalid" style="color: red">Please select a location </span>
                                </div>
                            </div>
                            <%--   <div class="clearfix" data-ng-show="CompanyVisible==0">--%>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">From Date</label>
                                    <div class="input-group date" id='FromDate'>
                                        <input type="text" class="form-control" data-ng-model="LeaseRep.FromDate" id="Fdate" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="LeaseReport.$submitted && LeaseReport.FromDate.$invalid" style="color: red;">Please select from date</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="txtcode">To Date</label>
                                    <div class="input-group date" id='ToDate'>
                                        <input type="text" class="form-control" data-ng-model="LeaseRep.ToDate" id="Tdate" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar" onclick="setup('ToDate')"></i>
                                        </span>
                                    </div>
                                    <span class="error" data-ng-show="LeaseReport.$submitted && LeaseReport.ToDate.$invalid" style="color: red;">Please select to date</span>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="CompanyVisible==0">
                                <div class="form-group">
                                    <label class="control-label">Company</label>
                                    <div isteven-multi-select data-input-model="Company" data-output-model="LeaseRep.CNP_NAME" button-label="icon CNP_NAME" data-is-disabled="EnableStatus==0"
                                        item-label="icon CNP_NAME" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1" selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="LeaseRep.CNP_NAME" name="CNP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="LeaseReport.$submitted && LeaseReport.Company.$invalid" style="color: red">Please select company </span>
                                </div>
                            </div>


                            <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 17px;">
                                <input type="submit" value="Search" class="btn btn-primary custom-button-color" />
                            </div>
                            <%--</div>--%>
                        </div>

                        <br />
                        <%--<div class="row" style="padding-left: 18px">
                                        
                                    </div>--%>


                        <div class="row" style="padding: 0px 15px;">
                            <%--<input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />--%>

                            <div class="col-md-10 p-0">
                                <div class="input-group" style="width: 28%">
                                    <input type="text" class="form-control" placeholder="Search" name="srch-term" id="filtertxt">
                                    <div class="input-group-btn">
                                        <button class="btn btn-primary custom-button-color" type="submit">
                                            <i class="glyphicon glyphicon-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="box-footer text-right" id="table2" data-ng-show="GridVisiblity" style="display: block !important;">
                                    <div class="icon_wrap" style="display: flex;">
                                        <a data-ng-click="GenReport(LeaseRep,'doc')"><i id="word" data-toggle="tooltip" data-ng-show="DocTypeVisible==0" title="Export to Word" class="fa fa-file-word-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(LeaseRep,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        <a data-ng-click="GenReport(LeaseRep,'pdf')"><i id="pdf" data-toggle="tooltip" title="Export to Pdf" class="fa fa-file-pdf-o fa-2x pull-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto;"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Dashboard/C3/d3.v3.min.js"></script>
    <script src="../../Dashboard/C3/c3.min.js"></script>
    <link href="../../Dashboard/C3/c3.css" rel="stylesheet" />
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/moment.min.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var CompanySession = '<%= Session["COMPANYID"]%>';
    </script>
    <script src="../JS/LeaseReport.js"></script>
    <script src="../../SMViews/Utility.min.js"></script>
    <script type="text/javascript">
        function setDateVals() {
            $('#FromDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#ToDate').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
            $('#FromDate').datepicker('setDate', new Date(moment().startOf('year').format('MM/DD/YYYY')));
            $('#ToDate').datepicker('setDate', new Date(moment().endOf('year').format('MM/DD/YYYY')));
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setDateVals();
        });
    </script>

</body>
</html>




