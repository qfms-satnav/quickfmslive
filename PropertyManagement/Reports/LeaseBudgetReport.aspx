﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<html lang="en" data-ng-app="QuickFMS">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
      <script defer>

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });

        };

    </script>
   <%-- <style>
        #pageRowSummaryPanel {
            display: none;
        }

        .grid-align {
            text-align: left;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-cell {
            color: black;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }

        #word {
            color: #4813CA;
        }

        #pdf {
            color: #FF0023;
        }

        #excel {
            color: #2AE214;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell {
            background-color: #1c2b36;
        }
    </style>--%>
    <!--[if lt IE 9]>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script defer src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->

    <script defer type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }
        function setup(id) {
            $('#' + id).datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
</head>
<body data-ng-controller="LeaseBudgetController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Area Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Lease Budget Report</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                        <div class="widgets">
                     <h3>Lease Budget Report</h3>
                    </div>
                      <div class="card">
                            <form id="form1" name="LeaseBudget"novalidate>

                                <div class="clearfix">


                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': LeaseBudget.$submitted && LeaseBudget.CHE_NAME.$invalid}">
                                            <label class="control-label">Entity </label>
                                            <div isteven-multi-select data-input-model="Entity" data-output-model="Branch.Entity" data-button-label="icon CHE_NAME"
                                                data-item-label="icon CHE_NAME maker" data-on-item-click="Propertybind()" data-on-select-all="cnySelectAll()" data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1">
                                            </div>
                                           <input type="text" data-ng-model="Branch.Entity[0]" name="CHE_NAME" style="display: none" required="" />
                                            <%--<span class="error" data-ng-show="LeaseBudget.$submitted && LeaseBudget.LCM_NAME.$invalid" style="color: red">Please select location </span>--%>
                                        </div>
                                    </div>

                                    <div class="col-md-3 col-sm-6 col-xs-12" >
                                        <div class="form-group">
                                            <label class="control-label">Property Type</label>

                                             <div isteven-multi-select data-input-model="PropertyType" data-output-model="Branch.PN_PROPERTYTYPE" data-button-label="icon PN_PROPERTYTYPE"
                                                data-item-label="icon PN_PROPERTYTYPE maker" data-on-item-click="BindPropType()" data-on-select-all="cnySelectAll()" data-on-select-none=""
                                                data-tick-property="ticked" data-max-labels="1" selection-mode="multiple">
                                            </div>

                                            <%--<div isteven-multi-select data-input-model="PropertyType" data-output-model="Branch.PN_PROPERTYTYPE" button-label="icon PropertyType" data-is-disabled="EnableStatus==0"
                                                item-label="icon PropertyType" data-on-click="BindPropType()" tick-property="ticked" data-on-select-all="" data-on-select-none="" data-max-labels="1">
                                            </div>--%>
                                            <input type="text" data-ng-model="Branch.PN_PROPERTYTYPE" name="PN_PROPERTYTYPE" style="display: none" required="" />
                                            <span class="error" data-ng-show="Branch.$submitted && Branch.Type.$invalid" style="color: red">Please Select Property Type</span>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group" data-ng-class="{'has-error': LeaseBudget.$submitted && LeaseBudget.PM_PPT_NAME.$invalid}">
                                            <label class="control-label">Property Name </label>
                                            <div isteven-multi-select data-input-model="Property" data-output-model="Branch.Property" data-button-label="icon PM_PPT_NAME"
                                                data-item-label="icon PM_PPT_NAME maker"  data-on-select-none="lcmSelectNone()"
                                                data-tick-property="ticked" data-max-labels="1" data-selection-mode="multiple">
                                            </div>
                                            <%--<input type="text" data-ng-model="Branch.Property[0]" name="PM_PPT_NAME" style="display: none" required="" />--%>
                                            <%--<span class="error" data-ng-show="LeaseBudget.$submitted && LeaseBudget.PM_PPT_NAME.$invalid" style="color: red">Please select location </span>--%>
                                        </div>
                                    </div>
                                    <%--<div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">From Date</label>
                                            <div class="input-group date" id='FromDate'>
                                                <input type="text" class="form-control" data-ng-model="Customized.FromDate" id="Text1" name="FromDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.FromDate.$invalid" style="color: red;">Please select From Date</span>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label for="txtcode">To Date</label>
                                            <div class="input-group date" id='ToDate'>
                                                <input type="text" class="form-control" data-ng-model="Customized.ToDate" id="Text2" name="ToDate" required="" placeholder="mm/dd/yyyy" data-ng-readonly="true" />
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar" onclick="setup('ToDate')"></i>
                                                </span>
                                            </div>
                                        </div>
                                        <span class="error" data-ng-show="CustomizedReport.$submitted && CustomizedReport.ToDate.$invalid" style="color: red;">Please select To Date</span>
                                    </div>--%>
                                    <div class="col-md-3 col-sm-6 col-xs-12" <%--style="padding-left: 5px; padding-right: 30px; padding-top: 10px;"--%>>
                                        <div class="form-group" data-ng-class="{'has-error': LeaseBudget.$submitted && LeaseBudget.name.$invalid}">
                                            <label class="control-label">Year</label>
                                            <div isteven-multi-select data-input-model="getyears" data-output-model="Branch.getyears" data-button-label="icon name" data-item-label="icon name"
                                                data-tick-property="ticked" data-max-labels="1" data-on-item-click="PTypeChanged()" selection-mode="multiple">
                                            </div>
                                            <%-- <input type="text" data-ng-model="LeaseRentalandEscaltionReport.name" name="name" style="display: none" required="" />
                                            <span class="error" data-ng-show="LeaseBudget.$submitted && LeaseBudget.name.$invalid" style="color: red">Please select year </span>--%>
                                        </div>
                                    </div>
                                    <div class="col-md-11 col-sm-6 col-xs-12">
                                        <br />
                                        <div class="box-footer text-right">
                                            <input type="submit" data-ng-click="Binddata(Gridmode)" value="Search" class="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                    <div class="row" style="padding-left: 18px">
                                        <div class="box-footer text-right" style="padding-left: 30px; padding-right: 30px; padding-bottom: 30px">
                                            <a data-ng-click="GenReport('xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="panel-heading ">
                                        <ul class="nav nav-tabs">
                                            <li ><a href="#tabSummary" class="active" data-ng-click="BindGrid_Year()" data-toggle="tab">Year Wise</a></li>
                                            <li><a href="#tabmonth" data-ng-click="BindGrid_Month()" data-toggle="tab">Month Wise</a></li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active show" id="tabSummary">
                                                <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 25%" />
                                                <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                                            </div>
                                            <div class="tab-pane fade" id="tabmonth">
                                                <input type="text" class="form-control" id="filtermonth" placeholder="Filter by any..." style="width: 25%" />
                                                <div data-ag-grid="gridmonth" class="ag-blue" style="height: 310px; width: auto"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
               <%-- </div>
            </div>
        </div>--%>
    </div>
</body>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script src="../../Scripts/Lodash/lodash.min.js"></script>
<script defer src="../../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
<script defer src="../../../Scripts/moment.min.js"></script>
<script defer>
    var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
    var CompanySession = '<%= Session["COMPANYID"]%>';
</script>

<script src="../../SMViews/Utility.js"></script>
<script src="../Js/LeaseBudgetReport.js"></script>


|</html>