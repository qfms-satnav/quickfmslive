<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSurrenderLease.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmApprovalLease" Title="Surrender Lease" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <%--<link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />--%>
    <%--<link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="View Or Modify Maintenance Contract" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Surrender Lease</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                         <div class="widgets">
                             <h3>Surrender Lease</h3>
                        </div>
                          <div class="card">

                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel runat="server" ID="upfelren">
                                    <ContentTemplate>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <strong>
                                            <asp:Label ID="lblMessage" runat="server" CssClass="clsLabel"></asp:Label></strong>
                                        <%--     <div class="clearfix">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search"></asp:TextBox>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
            <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
        </div>
    </div>--%>
                                        <div class="row" style="padding-top: 10px">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-10 control-label">Search By Lease ID Property Name / Code / City / Location<span style="color: red;">*</span></label>


                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                                Display="none" ErrorMessage="Please Search By Property / Lease / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <asp:Button ID="btnsearch" CssClass="btn btn-default btn-primary" runat="server" Text="Search" ValidationGroup="Val1"
                                                                CausesValidation="true" TabIndex="2" />
                                                            <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="padding-top: 10px">
                                            <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" SelectedRowStyle-VerticalAlign="Top"
                                                AllowPaging="True" PageSize="5" EmptyDataText="No Surrender Lease Records Found." CssClass="table GridStyle" GridLines="none"
                                                Style="font-size: 12px;">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLeaseName" runat="server" Text='<%#Eval("PM_LES_SNO")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Lease Id">
                                                        <ItemTemplate>

                                                            <asp:LinkButton ID="lblLseName" runat="server" Text='<%#Eval("LEASE_ID")%>' CommandName="Surrender"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Property Code">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPC" runat="server" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Property Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPN" runat="server" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="City">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCity" runat="server" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLCM" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Lease Start Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLsdate" runat="server" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Lease Expiry Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLedate" runat="server" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblstatus" runat="server" Text='<%#Eval("Status")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>

                                        <div id="pnl1" runat="Server" style="padding-top: 17px">
                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Lease Id</label>
                                                        <asp:TextBox ID="txtsno" runat="server" CssClass="form-control" Visible="false"></asp:TextBox>
                                                        <asp:TextBox ID="txtstore" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Surrender Date<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="rfvsdate" runat="Server" ControlToValidate="txtsurrender"
                                                            ErrorMessage="Please Enter Surrender Date" Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                        <div class='input-group date' id='effdate'>
                                                            <asp:TextBox ID="txtsurrender" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Refund Date<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="Server" ControlToValidate="txtrefund"
                                                            ErrorMessage="Please Enter Refund Date" Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                        <div class='input-group date' id='refdt'>
                                                            <asp:TextBox ID="txtrefund" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('refdt')"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Total Rent<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="Server" ControlToValidate="txtSDamount"
                                                            ErrorMessage="Please Enter Security Deposit" Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txttotamount" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Security Deposit<span style="color: red;">*</span></label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="Server" ControlToValidate="txtSDamount"
                                                            ErrorMessage="Please Enter Security Deposit" Display="None" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                        <asp:TextBox ID="txtSDamount" runat="server" CssClass="form-control" ReadOnly="true">50000</asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Damages</label>
                                                        <asp:RegularExpressionValidator ID="RegExpName15" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtDmg"
                                                            ErrorMessage="Enter Valid Damages"></asp:RegularExpressionValidator>
                                                        <asp:TextBox ID="txtDmg" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Damage Amount</label>
                                                        <asp:TextBox TextMode="Number" ID="txtDmgAmt" runat="server" CssClass="form-control" AutoPostBack="true">0</asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Outstanding Amount</label>
                                                        <asp:TextBox ID="txtOutAmt" runat="server" TextMode="Number" CssClass="form-control" AutoPostBack="true">0</asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Amount To Be Paid To Lesse</label>
                                                        <asp:TextBox ID="txtAmtPaidLesse" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Amount To Be Paid To Lessor</label>
                                                        <asp:TextBox ID="txtAmtPaidLessor" runat="server" CssClass="form-control" ReadOnly="true">0</asp:TextBox>
                                                    </div>
                                                </div>
                                                <%--       <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <label>Payee Name</label>
                                                <asp:TextBox ID="txtPayeeName" runat="server" CssClass="form-control" ReadOnly="true"></asp:TextBox>
                                            </div>
                                        </div>--%>
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Upload Possession Letter <a href="#" data-toggle="tooltip" title="Upload File Type:All and size should not be more than 20MB"></a></label>
                                                        <asp:RegularExpressionValidator ID="rfvupserv" Display="None" ControlToValidate="BrowsePossLtr"
                                                            ValidationGroup="Val2" runat="Server" ErrorMessage="Only doc,docx,xls,xlsx,pdf,txt,jpg,gif,tif files allowed"
                                                            ValidationExpression="^.+\.(([dD][oO][cC][xX])|([dD][oO][cC])|([pP][dD][fF])|([xX][lL][sS])|([xX][lL][sS][xX])|([tT][xX][tT])|([jJ][pP][gG])|([jJ][pP][eE][gG])|([gG][iI][fF])|([tT][iI][fF]))$">                        
                                                        </asp:RegularExpressionValidator>
                                                        <div class="btn-default">
                                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                                            <asp:FileUpload ID="BrowsePossLtr" runat="Server" onchange="showselectedfiles(this)" Width="90%" AllowMultiple="true" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Remarks<span style="color: red;">*</span></label>
                                                        <asp:RegularExpressionValidator ID="RegExpRem" runat="server" ControlToValidate="txtremarks"
                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Remarks upto 500 Characters, special characters (-/_@$)(&*,space) are allowed">
                                                        </asp:RegularExpressionValidator>
                                                        <asp:RequiredFieldValidator ID="rfvtxtremrk" runat="server" ControlToValidate="txtremarks" Display="None" ValidationGroup="Val2"
                                                            ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator>
                                                        <div onmouseover="Tip('Please Enter Remarks')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtremarks" runat="server" CssClass="form-control" TextMode="multiLine" MaxLength="1000" Height="30%"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                                    <div class="form-group">
                                                        <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-default btn-primary" Text="Submit" ValidationGroup="Val2" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="modal fade" id="myModal" tabindex='-1'>
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Lease Application form</h4>
                                                    </div>
                                                    <div class="modal-body" id="modelcontainer">
                                                        <%-- Content loads here --%>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <script>
                                            function showPopWin(id) {
                                                $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                                                    $("#myModal").modal('show');
                                                });
                                            }
                                        </script>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>
                    </div>
               <%-- </div>
            </div>
        </div>--%>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>






