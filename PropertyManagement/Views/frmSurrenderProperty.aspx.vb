Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmSurrenderProperty
    Inherits System.Web.UI.Page
    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim Flag As Integer
    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETPROPTEN")
            sp.Command.AddParameter("@user", Session("uid"), DbType.String)
            gvItems.DataSource = sp.GetDataSet()
            gvItems.DataBind()
            gvItems.PageIndex = Convert.ToInt32(Session("CurrentPageIndex"))
            gvItems.Visible = True
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindUser()
        Try
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENUSER")
            sp1.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.Int32)
            ddluser.DataSource = sp1.GetDataSet()
            ddluser.DataTextField = "AUR_FIRST_NAME"
            ddluser.DataValueField = "AUR_ID"
            ddluser.DataBind()
            ddluser.Items.Insert(0, New ListItem("--Select--", "0"))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub update_userstatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_UPT_USR_STATUS")
        sp.Command.AddParameter("@user", Session("uid"), DbType.String)
        sp.ExecuteScalar()
    End Sub
    Private Function ValidateDate()
        Dim validatedat As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_Validate_Surrender_Date")
        sp.Command.AddParameter("@SDATE", txtSdate.Text, DbType.Date)
        sp.Command.AddParameter("@USER", Session("uid"), DbType.String)
        validatedat = sp.ExecuteScalar()
        Return validatedat
    End Function
    Private Sub DamageAmountTotal()
        txtAmtPaidLesse.Text = ""
        txtAmtPaidLessor.Text = ""
        If txtDmgAmt.Text = "" Then
            txtDmgAmt.Text = 0
        End If
        If CInt(txtDmgAmt.Text) = 0 Then
            txtResultAmount.Text = Session("Result_Amount")
        Else
            txtResultAmount.Text = CInt(txtResultAmount.Text) + CInt(txtDmgAmt.Text)
        End If

        If CInt(txtDmgAmt.Text) > CInt(Security.Text) Then
            txtAmtPaidLesse.Text = Math.Abs(CInt(Security.Text) - CInt(txtDmgAmt.Text))
        Else
            txtAmtPaidLessor.Text = Math.Abs(CInt(Security.Text) - CInt(txtDmgAmt.Text))
        End If
    End Sub
    Public Sub SearchfilterGrid(Flag)
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_TENANT_DETAILS")
            sp2.Command.AddParameter("@FLAG", Flag, DbType.Int32)
            sp2.Command.AddParameter("@TEN_CODE", txtSearch.Text, DbType.String)
            Session("dataset") = sp2.GetDataSet()
            gvItems.DataSource = Session("dataset")
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Public Sub InsertSurrenderDetails()
        Dim param1(16) As SqlParameter
        param1(0) = New SqlParameter("@TENANT_CODE", SqlDbType.NVarChar, 100)
        param1(0).Value = txttcode.Text
        param1(1) = New SqlParameter("@TENANT_NAME", SqlDbType.NVarChar, 100)
        param1(1).Value = ddluser.SelectedItem.Value
        param1(2) = New SqlParameter("@PN_NAME", SqlDbType.NVarChar, 100)
        param1(2).Value = txtpropname.Text
        param1(3) = New SqlParameter("@PN_CODE", SqlDbType.NVarChar, 100)
        param1(3).Value = txtpropertycode.Text
        param1(4) = New SqlParameter("@SURRENDER_DATE", SqlDbType.DateTime, 200)
        param1(4).Value = txtSdate.Text
        param1(5) = New SqlParameter("@TOT_RENT_AMOUNT", SqlDbType.Decimal, 100)
        param1(5).Value = txtTotRent.Text
        param1(6) = New SqlParameter("@RENT_AMOUNT", SqlDbType.NVarChar, 100)
        param1(6).Value = RentAmount.Text
        param1(7) = New SqlParameter("@SECURITY_DEP", SqlDbType.NVarChar, 100)
        param1(7).Value = Security.Text
        param1(8) = New SqlParameter("@REMAIN_AMOUNT", SqlDbType.NVarChar, 100)
        param1(8).Value = txtResultAmount.Text
        param1(9) = New SqlParameter("@COMPANY", SqlDbType.NVarChar, 100)
        param1(9).Value = HttpContext.Current.Session("COMPANYID")
        param1(10) = New SqlParameter("@CREATED", SqlDbType.NVarChar, 100)
        param1(10).Value = HttpContext.Current.Session("Uid")
        param1(11) = New SqlParameter("@DMG", SqlDbType.NVarChar, 100)
        param1(11).Value = txtDmg.Text
        param1(12) = New SqlParameter("@DMG_AMT", SqlDbType.NVarChar, 100)
        param1(12).Value = txtDmgAmt.Text
        param1(13) = New SqlParameter("@TO_OWNER", SqlDbType.NVarChar, 100)
        param1(13).Value = txtAmtPaidLesse.Text
        param1(14) = New SqlParameter("@BY_OWNER", SqlDbType.NVarChar, 100)
        param1(14).Value = txtAmtPaidLessor.Text

        Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
        Dim IC As ImageClas
        If fu1.PostedFiles IsNot Nothing Then
            For Each File In fu1.PostedFiles
                If File.ContentLength > 0 Then
                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                    File.SaveAs(filePath)
                    IC = New ImageClas()
                    IC.Filename = File.FileName
                    IC.FilePath = Upload_Time & "_" & File.FileName
                    Imgclass.Add(IC)
                End If
            Next
        End If
        param1(15) = New SqlParameter("@IMAGES", SqlDbType.Structured)
        param1(15).Value = UtilityService.ConvertToDataTable(Imgclass)
        param1(16) = New SqlParameter("@T_SNO", SqlDbType.NVarChar, 100)
        param1(16).Value = HttpContext.Current.Session("Tenant_Count")
        SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_ADD_SURRENDER_DETAILS", param1)
        'ObjSubSonic.GetSubSonicExecute("PM_ADD_SURRENDER_DETAILS", param1)
        lblMsg.Text = "Property has successfully been surrendered and is awaiting approval."
    End Sub
    Public Class ImageClas
        Private _fn As String

        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String

        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Private Function ValidateConfirmSurrenderProperty()
        Dim ValidateCount As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_Validate_Surrender_Request")
        sp.Command.AddParameter("@SNO", HttpContext.Current.Session("Tenant_Count"), DbType.Int32)
        ValidateCount = sp.ExecuteScalar()
        If ValidateCount = 1 Then
            lblMsg.Text = "Property Has Already Been Surrendered"
        Else
            lblMsg.Text = ""
            InsertSurrenderDetails()
        End If
        Return ValidateCount
    End Function
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Try
            SearchfilterGrid(4)
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            Session("CurrentPageIndex") = 0
            BindGrid()
            panel1.Visible = False
            txtSdate.Text = getoffsetdate(Date.Today)
            'BindUser()
            'BindCity()
        End If
        txtSdate.Attributes.Add("readonly", "readonly")
    End Sub
    Protected Sub txtDmgAmt_TextChanged(sender As Object, e As EventArgs) Handles txtDmgAmt.TextChanged
        DamageAmountTotal()
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        lblMsg.Text = ""
        Try
            Dim validatedat As Integer
            validatedat = ValidateDate()
            If validatedat = 0 Then
                lblMsg.Text = "Surrendered Date Should be Less than Joining Date"
                lblMsg.Visible = True
            Else
                ValidateConfirmSurrenderProperty()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        Try
            If e.CommandName = "Surrender" Then
                panel1.Visible = True
                ddluser.Enabled = False
                txtTotRent.Enabled = False
                RentAmount.Enabled = False
                Security.Enabled = False
                txtResultAmount.Enabled = False
                Dim currentpageindex As Integer = 0
                '  ddlcity.SelectedIndex = 0
                If Session("CurrentPageIndex") <> Nothing Then
                    currentpageindex = (Integer.Parse(Session("CurrentPageIndex")))
                End If
                Dim rowIndex As Integer = Integer.Parse(e.CommandArgument.ToString()) - (currentpageindex * 5)
                Dim lblid As Label = DirectCast(gvItems.Rows(rowIndex).FindControl("lblsno"), Label)
                Dim Serial As Integer = lblid.Text
                Session("Tenant_Count") = Serial
                Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GETTENANT_DETAILS")
                sp4.Command.AddParameter("@SNO", Serial, DbType.Int32)
                sp4.Command.AddParameter("@flag", 1, DbType.Int32)
                Dim ds As New DataSet
                ds = sp4.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    txtpropertycode.Text = ds.Tables(0).Rows(0).Item("PM_TEN_PROPRTY")
                    txtpropname.Text = ds.Tables(0).Rows(0).Item("PROPERTY")
                    txttcode.Text = ds.Tables(0).Rows(0).Item("PM_TEN_CODE")
                    BindUser()
                    Dim li1 As ListItem = Nothing
                    li1 = ddluser.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_TEN_NAME"))
                    If Not li1 Is Nothing Then
                        li1.Selected = True
                    End If
                    txtTotRent.Text = ds.Tables(0).Rows(0).Item("PM_TD_TOT_RENT")
                    RentAmount.Text = ds.Tables(0).Rows(0).Item("PM_TD_RENT")
                    Security.Text = ds.Tables(0).Rows(0).Item("PM_TD_SECURITY_DEPOSIT")
                    Dim Rent As Double = ds.Tables(0).Rows(0).Item("PM_TD_TOT_RENT")
                    Dim SecurityDetails As Double = ds.Tables(0).Rows(0).Item("PM_TD_SECURITY_DEPOSIT")
                    Dim TotalAmount As Double = ds.Tables(0).Rows(0).Item("PM_TD_RENT")
                    Dim ResultAmount As Integer = ((SecurityDetails + Rent) - TotalAmount)
                    Session("Result_Amount") = ResultAmount
                    txtResultAmount.Text = Session("Result_Amount")
                End If
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex
        Session("CurrentPageIndex") = e.NewPageIndex
        BindGrid()
    End Sub
End Class