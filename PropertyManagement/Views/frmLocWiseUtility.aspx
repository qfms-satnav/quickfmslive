﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>
<html lang="en" data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }


        .ag-header-cell-menu-button {
            opacity: 1 !important;
            transition: opacity 0.5s, border 0.2s;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .modal-header-primary {
            color: #1D1C1C;
            padding: 9px 15px;
        }
    </style>--%>

    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
                todayHighlight: true
            });
        };
    </script>
    <script type="text/javascript">
        function prepareFrame() {
            var ifrm = document.createElement("iframe");
            ifrm.setAttribute("src", location.origin + "/PropertyManagement/frmExporttoExcelNew.aspx");
            ifrm.style.width = "640px";
            ifrm.style.height = "480px";
            document.body.appendChild(ifrm);
        }
    </script>
</head>
<body data-ng-controller="LocUtilityController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Add Utility Expenses </h3>
            </div>
            <div class="card">
                <form role="form" id="form1" name="frmLocationUtility" data-valid-submit="SubmitData()" novalidate>
                    <div class="row">
                        <div class="clearfix" style="width: 100%">
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <div class="form-group" onmouseover="Tip('choose file to upload')" onmouseout="UnTip()">
                                    <label>Upload File:</label>
                                    <input type="file" name="ExUpload" id="File3" required="" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" />

                                    <%--<a data-ng-click="ExcelDownload()">Click Here To Download Template</a>--%>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <input type="button" value="Upload Excel" class='btn btn-primary custom-button-color' id="btnUplaodExcel" data-ng-click="UploadFile()" />
                                <%--<input type="button" value="Download Excel" class='btn btn-primary custom-button-color' id="btnDownload" data-ng-click="ExcelDownload()" />--%>
                                <%--<div id="Excelframe" style="width: 1px; height: 1px; display: none">
                                            </div>
                                            <a class='btn btn-primary custom-button-color' id="btnDownload" onclick="prepareFrame()">Download Excel</a>--%>
                            </div>
                            <div class="col-md-3 col-sm-4 col-xs-6">
                                <input type="button" value="Download Excel" class='btn btn-primary custom-button-color' id="btnDownload" data-ng-click="ExcelDownload()" />
                               <%-- <a class='btn btn-primary custom-button-color' id="btnDownload" href="../../LocationExcelFiles/LocWiseUtility_Excel.xlsx">Download Excel</a>--%>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <%-- <label>Location<span style="color: red;">*</span></label>--%>
                                <div>
                                    <input id="UTIL_ID" type="hidden" name="UTIL_ID" data-ng-model="SaveExpUtility.UTIL_ID" autofocus class="form-control" />
                                    <label>Location<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="LocUtilityList" data-output-model="SaveExpUtility.LCM_CODE" button-label="icon LCM_NAME" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon LCM_NAME" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="SaveExpUtility.LCM_CODE" name="LCM_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.LCM_NAME.$invalid" style="color: red">Please Select Location </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group" data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.EXP_CODE.$invalid}">
                                <label>Expense Head<span style="color: red;">*</span></label>
                                <div isteven-multi-select data-input-model="ExpenseUtility" data-output-model="SaveExpUtility.EXP_CODE" button-label="icon EXP_NAME" data-is-disabled="EnableStatus==0"
                                    data-item-label="icon EXP_NAME" data-tick-property="ticked" data-on-item-click="GetUtilityDetails()" data-max-labels="1" data-selection-mode="single">
                                </div>
                                <input type="text" data-ng-model="SaveExpUtility.EXP_CODE" name="EXP_NAME" style="display: none" required="" />
                                <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.EXP_NAME.$invalid" style="color: red">Please Select Expense Head </span>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Expense Month</label>
                                <div>
                                    <input type="month" id="start" name="start" data-ng-model="SaveExpUtility.ExpenseMonth" data-ng-change="GetUtilityDetails()" class="form-control " data-live-search="true"
                                        required="required" min="2021" value="06">
                                </div>
                                <input type="text" data-ng-model="SaveExpUtility.ExpenseMonth" name="ExpenseMonth" style="display: none" required="" />
                                <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.ExpenseMonth.$invalid" style="color: red">Please Select Expense Month </span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="txtcode">Bill Date<span style="color: red;">*</span></label>
                                <div class="input-group date" id='FromDate'>
                                    <input type="text" class="form-control" data-ng-model="SaveExpUtility.FromDate" id="Text2" name="FromDate" required="" placeholder="dd-mm-yyyy" />
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar" onclick="setup('FromDate')"></i>
                                    </span>
                                </div>
                            </div>
                            <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.FromDate.$invalid" style="color: red;">Please select Bill Date</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Bill No.<span style="color: red;">*</span></label>
                                <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.BILL_NO.$invalid}">
                                    <input id="BILL_NO" type="text" name="BILL_NO" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="SaveExpUtility.BILL_NO" autofocus class="form-control" required="required" />&nbsp;
                                            <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.BILL_NO.$invalid" style="color: red">Please enter bill No.</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Invoice/PO No.<span style="color: red;">*</span></label>
                                <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.BILL_INVOICE.$invalid}">
                                    <input id="BILL_INVOICE" type="text" name="BILL_INVOICE" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9-_ /():. ]*$/" data-ng-model="SaveExpUtility.BILL_INVOICE" class="form-control" required="required" />&nbsp;
                                                <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.BILL_INVOICE.$invalid" style="color: red">Please enter valid bill invoice </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12" id="NOOFUNIQUA" data-ng-show='FirstValue'>
                            <div class="form-group">
                                <label>No of Units/Deployment</label>
                                <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.VEN_MAIL.$invalid}">
                                    <input id="VEN_MAIL" type="text" name="VEN_MAIL" maxlength="50" data-ng-model="SaveExpUtility.VEN_MAIL" class="form-control" />&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Amount (Including Tax)<span style="color: red;">*</span></label>
                                <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.AMT.$invalid}">
                                    <input id="AMT" type="number" min="0" name="AMT" maxlength="25" data-ng-pattern="/[0-9.]/" data-ng-model="SaveExpUtility.AMT" autofocus class="form-control" required="required" />&nbsp;
                                                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.AMT.$invalid" style="color: red">Please enter valid amount </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Vendor Name</label>
                                <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.VENDOR.$invalid}">
                                    <input id="VENDOR" type="text" name="VENDOR" maxlength="25" data-ng-pattern="/^[a-zA-Z0-9. ]*$/" data-ng-model="SaveExpUtility.VENDOR" autofocus class="form-control" />&nbsp; 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Vendor Phone No.</label>
                                <div data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.VEN_PHNO.$invalid}">
                                    <input id="VEN_PHNO" type="text" maxlength="10" name="VEN_PHNO"
                                        oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                        data-ng-model="SaveExpUtility.VEN_PHNO" class="form-control" />&nbsp;     
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <div class="form-group" data-ng-class="{'has-error': frmLocationUtility.$submitted && frmLocationUtility.DEP_CODE.$invalid}">
                                    <label>Department<span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="UserList" data-output-model="SaveExpUtility.DEP_CODE" button-label="icon DEP_NAME" data-is-disabled="EnableStatus==0"
                                        data-item-label="icon DEP_NAME" data-tick-property="ticked"
                                        data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <input type="text" data-ng-model="SaveExpUtility.DEP_CODE" name="DEP_NAME" style="display: none" required="" />
                                    <span class="error" data-ng-show="frmLocationUtility.$submitted && frmLocationUtility.DEP_NAME.$invalid" style="color: red">Please select department </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Remarks</label>
                                <textarea id="REM" runat="server" data-ng-model="SaveExpUtility.REM" class="form-control" style="height: 30%" maxlength="500"></textarea>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px;">
                            <div class="form-group">
                                <input type="submit" value="Submit" class='btn btn-primary custom-button-color' data-ng-show="SaveExpUtility.STATUS=='New'" />
                                <input type="button" value="Modify" data-ng-click="UpdateExpUtilityDetails()" class='btn btn-primary custom-button-color' data-ng-show="SaveExpUtility.STATUS=='Modify'" />
                                <input type="reset" value='Clear' class='btn btn-primary custom-button-color' data-ng-click="ClearAllData()" />
                                <input type="button" value='Reset' class='btn btn-primary custom-button-color' data-ng-click="reset()" />
                            </div>
                        </div>
                    </div>
          <a data-ng-click="GenReport(OpexFormat,'xls')"><i id="excel" data-toggle="tooltip" title="Export to Excel" class="fa fa-file-excel-o fa-2x pull-right "></i></a>

                </form>
                
                    

                <div class="row">

                    <div class="col-md-12">
                        <input type="text" class="form-control" id="filtertxt" placeholder="Filter by any..." style="width: 20%;" />
                        <div data-ag-grid="gridOptions" class="ag-blue" style="height: 310px; width: auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script> 
        async function BindExpenseHead() {
            var result;
            result = await $.ajax({
                url: '../api/LocUtility/BindExpenseHead',
                //data: param,
                type: 'GET',
            });
            console.log(result);
            var div = document.getElementById("NOOFUNIQUA");

            if (result.data[0].AUR_ROLE == 1) {
                div.style.display = "block";
            }
            else {

                div.style.display = "none";
            }
        }

    </script>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script src="../../Scripts/jspdf.min.js"></script>
    <script src="../../Scripts/Lodash/lodash.min.js"></script>
    <script src="../../Scripts/jspdf.plugin.autotable.src.js"></script>
    <script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>
    <script src="../../BlurScripts/BlurJs/moment.js"></script>
    <script>
        var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
        var UID = '<%= Session["UID"] %>';
    </script>
    <script src="../../SMViews/Utility.js"></script>
    <script src="../JS/LocationUtility.js"></script>

</body>
</html>

