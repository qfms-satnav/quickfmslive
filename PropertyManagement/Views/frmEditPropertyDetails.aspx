<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmEditPropertyDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_EditPropertyDetails" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href="../../BootStrapCSS/assets/css/GridView.css" rel="stylesheet" />
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Country Master" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">View Property Details</h3>
                        </div>--%>
            <div class="widgets">
                <h3>View Property Details </h3>
            </div>
            <div class="card">
                <div class="panel-body" style="padding-right: 50px;">
                    <form id="form1" runat="server">
                        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Val2" runat="server" />
                                <div class="row">

                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search By Request Id/Property Name"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                        <asp:Button ID="btnsrch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
                                    </div>

                                </div>
                                <br />
                                <div class="row" style="overflow-x:scroll">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvReqs" runat="server" EmptyDataText="No Records Found." AllowPaging="True" PageSize="5" AutoGenerateColumns="false" Width="1050px"
                                            CssClass="table GridStyle" GridLines="none">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Request Id">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnksurrender" runat="server" Text='<%#Eval("PM_REQ_ID")%>' CommandArgument='<%#Eval("PM_REQ_ID")%>'
                                                            CommandName="GetProperties"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPropType" runat="server" Text='<%#Eval("PM_CREATED_DT")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpropname" runat="server" Text='<%#Eval("PM_PPT_NAME")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Address">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpropadd" runat="server" Text='<%#Eval("PM_PPT_ADDRESS")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Requested Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPropCode" runat="server" Text='<%#Eval("STA_TITLE")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <asp:HiddenField ID="hdnReqid" runat="server" />

                                <div id="panel1" runat="Server" visible="false">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-5 col-sm-12 col-xs-12">
                                                <label>Search by Property Name/Location/Recommended/Owner Name</label>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:TextBox ID="txtPropName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <asp:Button ID="btnSearch" runat="server" Class="btn btn-primary btn-mm" Text="Search" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                                <div class="form-group">
                                                    <asp:Button ID="btnSubmit" runat="server" Class="btn btn-primary btn-mm" Text="Add more properties" CausesValidation="true" ValidationGroup="Val1" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12"  style="overflow-x:scroll ">
                                            <asp:GridView ID="gvrReqProperties" runat="server" EmptyDataText="No Property Details Found."
                                                AllowPaging="True" AllowSorting="false" PageSize="10" AutoGenerateColumns="false" CssClass="table GridStyle" GridLines="none">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Request Id" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblID" runat="server" Text='<%#Eval("PM_PPT_SNO")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:TemplateField HeaderText="Requested Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcreatedBy" runat="server" Text='<%#Eval("PM_PPT_CREATED_DT")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Requested By">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReqBy" runat="server" Text='<%#Eval("PM_PPT_CREATED_BY")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>

                                                    <asp:TemplateField HeaderText="Property Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPropName" runat="Server" Text='<%#Eval("PM_PPT_NAME")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Property Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPropType" runat="server" Text='<%#Eval("PN_PROPERTYTYPE")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Entity Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblentity" runat="Server" Text='<%#Eval("CHE_NAME")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Plot Area(Sqft)">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblplotArea" runat="Server" Text='<%#Eval("PM_AR_PLOT_AREA")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Property Address">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPropDesc" runat="Server" Text='<%#Eval("PM_PPT_ADDRESS")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Location">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblLoc" runat="Server" Text='<%#Eval("LCM_NAME")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Owner Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOwnName" runat="Server" Text='<%#Eval("PM_OWN_NAME")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Phone No">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPhno" runat="Server" Text='<%#Eval("PM_OWN_PH_NO")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Email">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmail" runat="Server" Text='<%#Eval("PM_OWN_EMAIL")%>'>
                                                            </asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Request Status">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReqSts" runat="Server" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Request Status" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStsValue" runat="Server" Text='<%#Eval("STA_VALUE")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <%--<a href='frmModifyPropertyDetails.aspx?id=<%#Eval("PM_PPT_SNO")%>'>EDIT</a>--%>
                                                            <%--<asp:LinkButton ID="linkbtn" runat="server" CommandArgument='<%#Eval("PM_PPT_SNO") %>' CommandName="VIEW" Text="View"></asp:LinkButton>--%>
                                                            <asp:LinkButton ID="linkbtn" runat="server" CommandArgument='<%#Eval("PM_PPT_SNO") %>' CommandName="VIEW" Text='<%# If(Eval("STA_VALUE").ToString() = "4001", "Edit", "View") %>'
                                                                PostBackUrl='<%# If(Eval("AST_SYSP_VAL1").ToString() = "11", String.Format("frmModifyPropertyDetails.aspx?id={0}&staid={1}", Eval("PM_PPT_SNO").ToString(), Eval("STA_VALUE").ToString()), String.Format("frmModifyPropertyDetails.aspx?id={0}&staid={1}", Eval("PM_PPT_SNO").ToString(), Eval("STA_VALUE").ToString()))%>'></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%--</div>
        </div>
    </div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>






