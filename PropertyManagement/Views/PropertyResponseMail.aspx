﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PropertyResponseMail.aspx.cs" Inherits="PropertyManagement_Views_PropertyResponseMail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <title></title>
    <style>
        .center-div {
            margin: auto;
            width: 60%;
            padding: 10px;
        }
    </style>
</head>
<body>
     <div class="center-div">
        <form id="form1" runat="server" >
            <div style="font-family: Open Sans,sans-serif; position: relative; font-size: 12px; height: 810px; mso-line-height-rule: exactly; line-height: 25px">
                <table style="border-bottom: #466788 1px solid; border-left: #466788 1px solid; width: 730px; border-collapse: collapse; border-top: #466788 1px solid; border-right: #466788 1px solid" border="0" bordercolor="0" cellpadding="0" cellspacing="0">
                    <tr><td bgcolor="#0069aa"><img height="45" src="http:\\product.quickfms.com\BootStrapCSS\images\logo_quick.gif" width="200">
                            <tr><td colspan="2"><br /> <br />
                                    <table align="center" width="90%"  style="border-bottom: #466788 4px solid; border-left: #466788 4px solid; width: 730px; border-collapse: collapse; border-top: #466788 4px solid; border-right: #466788 4px solid" border="0" bordercolor="0" cellpadding="0" cellspacing="0">
                                        <tr><td><div runat="server" id="Success" visible="false" align="center">
                                                    <label>Thank you. Your Lease Got Approved.</label>
                                                </div>
                                             <div runat="server" id="Wrong" visible="false" align="center">
                                                     <label>Something went wrong please login and check it. <br/>
                                                    </label>
                                                </div>
                                                <div runat="server" id="Fail" visible="false" align="center">
                                                     <label>Your Lease had not been approved . <br/>
                                                    </label>
                                                </div>
                                            </td>
                                        </tr>
                                    </table><br /> <br />
                                    <table align="left" style="float: left ; padding-left:5px">
                                        <tr ><td style="color: #00008b; font-size: 9.5pt; " colspan="3" ; >Click here to Logon:<br />
                                            <%--<div id="aditya" visible="false"  runat="server"><a href='https://live.quickfms.com/Aditya'>https://live.quickfms.com/Aditya</a><br /><br /></div>--%>
                                               <div id="other" visible="false" runat="server"> <a  href='https://live.quickfms.com'>https://live.quickfms.com</a><br /><br /></div>
                                                Regards,<br />
                                                QuickFMS
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                        </td>
                        <tr>
                            <td colspan="2" bgcolor="#0069aa" height="50">
                                <div align="right" style="color: #fff; font-weight: 700; font-size: 11px">
                                    <div align="center">www.QuickFMS.com</div>
                                </div>
                            </td>
                        </tr>
                </table>
            </div>
        </form>
    </div>
</body>
</html>
