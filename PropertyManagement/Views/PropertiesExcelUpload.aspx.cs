﻿  using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using UtiltiyVM;
using System.Drawing;

public partial class PropertyManagement_Views_PropertiesExcelUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblmsg.Visible = false;
            ParsedResult.Visible = false;
        }
    }
    protected void ImportExcel1(object sender, EventArgs e)

    {
        if (FileUpload1.PostedFile.InputStream.Length > 0)
        {
            DataTable dt = new DataTable();
            //Open the Excel file using ClosedXML.
            FillDataTable(dt);

            if (!dt.Columns.Contains("ReMark"))
            {
                //add new column to orginal for showing error
                dt.Columns.Add("ReMark", typeof(string));

                foreach (DataRow row2 in dt.Rows)
                {
                    //need to set value to NewColumn column
                    row2["ReMark"] = "Passed";   // or set it to some other value
                }
            }

            //getMasterData
            var masterData = FillMasterData();

            var listOFPropertyDetails = new List<PropertiesDetails>();

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row1 in dt.Rows)
                {
                    bool isSuccessfullyParesed;
                    string parsingError;
                    var getProperty = ExcelParserHelper.GetProperty(row1, out isSuccessfullyParesed, out parsingError);

                    if (!isSuccessfullyParesed)
                    {
                        //var reasonOfFailure = new StringBuilder();
                        //bool isValidRow;
                        //ExcelParserHelper.ValidateProperty(masterData, getProperty, out isValidRow, out reasonOfFailure);

                        //if (isValidRow)
                        //{
                        //    getProperty.Remark = "valid";
                        //    row1["ReMark"] = "valid";
                        //    row1.EndEdit();
                        //}
                        //else
                        //{
                        //    getProperty.Remark = "valid";
                        //    reasonOfFailure.Remove(reasonOfFailure.Length - 1, 1);
                        //    getProperty.Remark = reasonOfFailure.ToString();
                        //    row1["ReMark"] = reasonOfFailure;
                        //    row1.EndEdit();
                        //}

                        //}
                        //else
                        //{
                        row1["ReMark"] = parsingError;
                        row1.EndEdit();
                        //errorProneRowCollector.Rows.Add(row.ItemArray);
                    }
                    getProperty.Remark = "";
                    listOFPropertyDetails.Add(getProperty);
                }

                ParsedResult.Visible = true;
                gv1.DataSource = listOFPropertyDetails;
                gv1.DataBind();
            }
            else
            {
                lblmsg.Visible = true;
                lblmsg.Text = "Excel is empty";
            }

            //GridView1.DataSource = dt;
            //GridView1.DataBind();

        }
        else
        {
            lblmsg.Visible = true;
            lblmsg.Text = "Select Excel File";
        }


    }


    //protected void Validate_Click(object sender, EventArgs e)
    //{

    //    var listOFPropertyDetails = new List<PropertiesDetails>();

    //    foreach (GridViewRow g1 in gv1.Rows)
    //    {
    //        var prp = new PropertiesDetails();

    //        prp.REQUEST_TYPE = (g1.FindControl("REQUEST_TYPE") as Label).Text;
    //        prp.PROP_TYPE = (g1.FindControl("PROP_TYPE") as Label).Text;
    //        prp.PROP_NATURE = (g1.FindControl("PROP_NATURE") as Label).Text;
    //        prp.ACQ_TRH = (g1.FindControl("ACQ_TRH") as Label).Text;
    //        prp.PROP_CODE = (g1.FindControl("PROP_CODE") as Label).Text;
    //        prp.PROP_NAME = (g1.FindControl("PROP_NAME") as Label).Text;
    //        prp.PROP_ADDR = (g1.FindControl("PROP_ADDR") as Label).Text;
    //        prp.LCM_ZONE = (g1.FindControl("LCM_ZONE") as Label).Text;
    //        prp.State = (g1.FindControl("PropertyState") as Label).Text;
    //        prp.CITY = (g1.FindControl("CITY") as TextBox).Text;
    //        //4 location code
    //        prp.LOCATION_CODE = (g1.FindControl("LOCATION_CODE") as TextBox).Text;
    //        prp.LOCATION = (g1.FindControl("LOCATION") as TextBox).Text;
    //        prp.TOWER = (g1.FindControl("TOWER") as TextBox).Text;
    //        prp.FLOOR = (g1.FindControl("FLOOR") as TextBox).Text;


    //        prp.SecurityDepositedMonths = (g1.FindControl("SecurityDepositedMonths") as Label).Text;
    //        prp.RentFreePeriod = (g1.FindControl("RentFreePeriod") as Label).Text;
    //        prp.LeaseSignDate = (g1.FindControl("LeaseSignDate") as Label).Text;
    //        prp.LeaseStartDate = (g1.FindControl("LeaseStartDate") as Label).Text;
    //        prp.LeaseEndDate = (g1.FindControl("LeaseEndDate") as Label).Text;
    //        prp.RentStartDate = (g1.FindControl("RentStartDate") as Label).Text;
    //        prp.Escalation = (g1.FindControl("Escalation") as Label).Text;
    //        prp.NoticePeriodinMonths = (g1.FindControl("NoticePeriodinMonths") as Label).Text;
    //        prp.NoofLandlords = (g1.FindControl("NoofLandlords") as Label).Text;
    //        prp.LandlordName = (g1.FindControl("LandlordName") as Label).Text;
    //        prp.Address = (g1.FindControl("Address") as Label).Text;
    //        prp.LL_State = (g1.FindControl("LL_State") as Label).Text;
    //        prp.LL_City = (g1.FindControl("LL_City") as TextBox).Text;
    //        prp.GST = (g1.FindControl("GST") as Label).Text;
    //        prp.GSTNumber = (g1.FindControl("GSTNumber") as Label).Text;
    //        prp.PropertyTaxApplicable = (g1.FindControl("PropertyTaxApplicable") as Label).Text;
    //        prp.CITY = (g1.FindControl("PropertyTax") as Label).Text;
    //        prp.ContactDetails = (g1.FindControl("ContactDetails") as Label).Text;
    //        prp.PANNo = (g1.FindControl("PANNo") as Label).Text;
    //        prp.Email = (g1.FindControl("Email") as Label).Text;
    //        prp.BasicRent = (g1.FindControl("BasicRent") as Label).Text;
    //        prp.LandlordMaintenance = (g1.FindControl("LandlordMaintenance") as Label).Text;
    //        prp.LandlordAmenities = (g1.FindControl("LandlordAmenities") as Label).Text;
    //        prp.BankScopeRecovery = (g1.FindControl("BankScopeRecovery") as Label).Text;
    //        prp.SecurityDeposit = (g1.FindControl("SecurityDeposit") as Label).Text;
    //        prp.CarParkingCharges = (g1.FindControl("CarParkingCharges") as Label).Text;
    //        prp.Area = (g1.FindControl("Area") as Label).Text;

    //        prp.Remark = (g1.FindControl("Remark") as Label).Text;

    //        //prp.Inspection_By = (g1.FindControl("Inspection_By") as Label).Text;
    //        //prp.Inspection_date = (g1.FindControl("Inspection_date") as Label).Text;
    //        //prp.Super_Built_up_area = (g1.FindControl("Super_Built_up_area") as Label).Text;
    //        //prp.build_up_area = (g1.FindControl("build_up_area") as Label).Text;
    //        //prp.Carpet_area = (g1.FindControl("Carpet_area") as Label).Text;
    //        //prp.Rentable_area = (g1.FindControl("Rentable_area") as Label).Text;

    //        listOFPropertyDetails.Add(prp);

    //    }

    //    //getMasterData
    //    var masterData = FillMasterData();

    //    foreach (var getProperty in listOFPropertyDetails)
    //    {
    //        var reasonOfFailure = new StringBuilder();
    //        bool isValidRow;
    //        ExcelParserHelper.ValidateProperty(masterData, getProperty, out isValidRow, out reasonOfFailure);

    //        if (isValidRow)
    //        {
    //            getProperty.Remark = "valid";
    //        }
    //        else
    //        {
    //            reasonOfFailure.Remove(reasonOfFailure.Length - 1, 1);
    //            getProperty.Remark = reasonOfFailure.ToString();
    //        }
    //    }

    //    ParsedResult.Visible = true;
    //    gv1.DataSource = listOFPropertyDetails;
    //    gv1.DataBind();

    //}

    void IsAnyNullOrEmpty(PropertiesDetails myObject)
    {
        foreach (PropertyInfo pi in myObject.GetType().GetProperties())
        {
            if (pi.Name == "SecurityDeposit")
            {
                var s = 1;
            }
            if (pi.PropertyType == typeof(string) && ((string)pi.Name != "ValidationField" && (string)pi.Name != "Remark" && (string)pi.Name != "Remarks"))
            {
                string value = (string)pi.GetValue(myObject);
                if (string.IsNullOrEmpty(value))
                {
                    if (!string.IsNullOrEmpty(myObject.ValidationField))
                    {
                        myObject.ValidationField += ",";
                    }
                    myObject.ValidationField += (string)pi.Name;
                }
            }
        }
    }
    protected void UploadExcel_Click(object sender, EventArgs e)
    {

        var listOFPropertyDetails = new List<PropertiesDetails>();
        foreach (GridViewRow g1 in gv1.Rows)
        {
            var prp = new PropertiesDetails();
            //prp.Remark = (g1.FindControl("Remark") as Label).Text;
            prp.LCM_ZONE = (g1.FindControl("LCM_ZONE") as TextBox).Text;
            prp.State = (g1.FindControl("PropertyState") as TextBox).Text;
            prp.CITY = (g1.FindControl("CITY") as TextBox).Text;
            prp.LOCATION_CODE = (g1.FindControl("LOCATION_CODE") as TextBox).Text;
            prp.LOCATION = (g1.FindControl("LOCATION") as TextBox).Text;
            prp.TOWER = (g1.FindControl("TOWER") as TextBox).Text;
            prp.FLOOR = (g1.FindControl("FLOOR") as TextBox).Text;
            prp.PROP_TYPE = (g1.FindControl("PROP_TYPE") as TextBox).Text;
            prp.PROP_NATURE = (g1.FindControl("PROP_NATURE") as TextBox).Text;
            prp.ACQ_TRH = (g1.FindControl("ACQ_TRH") as TextBox).Text;
            prp.ENTITY = (g1.FindControl("ENTITY") as TextBox).Text;
            prp.REQUEST_TYPE = (g1.FindControl("REQUEST_TYPE") as TextBox).Text;
            prp.PROP_NAME = (g1.FindControl("PROP_NAME") as TextBox).Text;
            prp.PROP_ADDR = (g1.FindControl("PROP_ADDR") as TextBox).Text;
            prp.Owner_Name = (g1.FindControl("Owner_Name") as TextBox).Text;
            prp.Owner_Mobile_Number = (g1.FindControl("Owner_Mobile_Number") as TextBox).Text;
            prp.C_B_SB_Area = (g1.FindControl("C_B_SB_Area") as TextBox).Text;
            prp.Area = (g1.FindControl("Area") as TextBox).Text;
            prp.Chargeable_Area = (g1.FindControl("Chargeable_Area") as TextBox).Text;
            prp.Rentper_sqft = (g1.FindControl("Rentper_sqft") as TextBox).Text;
            prp.BasicRent = (g1.FindControl("BasicRent") as TextBox).Text;
            prp.LandlordAmenities = (g1.FindControl("LandlordAmenities") as TextBox).Text;
            prp.LandlordMaintenance = (g1.FindControl("LandlordMaintenance") as TextBox).Text;
            prp.Registration_Charges = (g1.FindControl("Registration_Charges") as TextBox).Text;
            prp.CarParkingCharges = (g1.FindControl("CarParkingCharges") as TextBox).Text;
            prp.TotalRent = (g1.FindControl("TotalRent") as TextBox).Text;
            prp.SecurityDeposit = (g1.FindControl("SecurityDeposit") as TextBox).Text;
            prp.Electricity_Deposit = (g1.FindControl("Electricity_Deposit") as TextBox).Text;
            prp.LeaseSignDate = (g1.FindControl("LeaseSignDate") as TextBox).Text;
            prp.LeaseStartDate = (g1.FindControl("LeaseStartDate") as TextBox).Text;
            prp.LeaseEndDate = (g1.FindControl("LeaseEndDate") as TextBox).Text;
            prp.RentStartDate = (g1.FindControl("RentStartDate") as TextBox).Text;
            prp.Escalation = (g1.FindControl("Escalation") as TextBox).Text;
            prp.LockinPeriod = (g1.FindControl("LockinPeriod") as TextBox).Text;
            prp.NoticePeriodinMonths = (g1.FindControl("NoticePeriodinMonths") as TextBox).Text;
            prp.No_Car_Parking = (g1.FindControl("No_Car_Parking") as TextBox).Text;
            prp.LandlordName = (g1.FindControl("LandlordName") as TextBox).Text;
            prp.Address = (g1.FindControl("Address") as TextBox).Text;
            prp.LL_State = (g1.FindControl("LL_State") as TextBox).Text;
            prp.LL_City = (g1.FindControl("LL_City") as TextBox).Text;
            //prp.State = (g1.FindControl("LL_State") as TextBox).Text;
            //prp.CITY = (g1.FindControl("LL_City") as TextBox).Text;
            prp.ContactDetails = (g1.FindControl("ContactDetails") as TextBox).Text;
            prp.Pin_Code = (g1.FindControl("Pin_Code") as TextBox).Text;
            prp.PANNo = (g1.FindControl("PANNo") as TextBox).Text;
            prp.Email = (g1.FindControl("Email") as TextBox).Text;
            prp.GST = (g1.FindControl("GST") as TextBox).Text;
            prp.GSTNumber = (g1.FindControl("GSTNumber") as TextBox).Text;
            prp.PropertyTaxApplicable = (g1.FindControl("PropertyTaxApplicable") as TextBox).Text;
            prp.PropertyTax = (g1.FindControl("PropertyTax") as TextBox).Text;
            prp.GL_code = (g1.FindControl("GL_code") as TextBox).Text;

           // prp.Remarks = (g1.FindControl("Remarks") as TextBox).Text;
            IsAnyNullOrEmpty(prp);
            if (!string.IsNullOrEmpty(prp.ValidationField))
            {
                foreach (var field in prp.ValidationField.Split(','))
                {
                    TextBox textBox = (g1.FindControl(field) as TextBox);
                    if (textBox != null)
                    {
                        textBox.Style.Add(System.Web.UI.HtmlTextWriterStyle.BorderColor, "red");
                        textBox.Style.Add(System.Web.UI.HtmlTextWriterStyle.BorderStyle, "double");
                    }
                }
            }
            listOFPropertyDetails.Add(prp);

        }
        if (listOFPropertyDetails.Any(x => !string.IsNullOrEmpty(x.ValidationField)))
        {
            lblValidationMsg.InnerText = "Please check red color highlighted fields";
            return;
        }

        var savedData = SaveDataIntoDataBase(listOFPropertyDetails);

        Session["DatabaseInsertResult"] = savedData;

        Response.Redirect("PropertiesExcelUploadStatus.aspx");
    }

    //private void FillDataTable(DataTable dt)
    //{
    //    using (XLWorkbook workBook = new XLWorkbook(FileUpload1.PostedFile.InputStream))
    //    {
    //        //Read the first Sheet from Excel file.
    //        IXLWorksheet workSheet = workBook.Worksheet(1);
    //        //Create a new DataTable.
    //        //Loop through the Worksheet rows.
    //        bool firstRow = true;
    //        foreach (IXLRow row in workSheet.Rows())
    //        {
    //            //Use the first row to add columns to DataTable.



    //            if (firstRow)
    //            {
    //                foreach (IXLCell cell in row.Cells())
    //                {
    //                    dt.Columns.Add(cell.Value.ToString());
    //                }
    //                firstRow = false;
    //            }
    //            else
    //            {
    //                if (row.IsEmpty())
    //                {
    //                    continue;
    //                }
    //                //Add rows to DataTable.
    //                dt.Rows.Add();
    //                int i = 0;
    //                foreach (IXLCell cell in row.Cells())
    //                {
    //                    try
    //                    {
    //                        dt.Rows[dt.Rows.Count - 1][i] = cell.Value.ToString();
    //                        i++;
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        var a = 5;
    //                    }

    //                }
    //            }

    //        }

    //    }
    //}
    private void FillDataTable(DataTable dt)
    {
        using (XLWorkbook workBook = new XLWorkbook(FileUpload1.PostedFile.InputStream))
        {
            // Read the first Sheet from Excel file.
            IXLWorksheet workSheet = workBook.Worksheet(1);

            // Create a new DataTable.
            // Loop through the Worksheet rows.
            bool firstRow = true;
            foreach (IXLRow row in workSheet.Rows())
            {
                // Use the first row to add columns to DataTable.
                if (firstRow)
                {
                    foreach (IXLCell cell in row.Cells())
                    {
                        dt.Columns.Add(cell.Value.ToString());
                    }
                    firstRow = false;
                }
                else
                {
                    if (row.IsEmpty())
                    {
                        continue;
                    }

                    // Add rows to DataTable.
                    DataRow newRow = dt.NewRow();
                    int i = 0;
                    foreach (IXLCell cell in row.Cells())
                    {
                        try
                        {
                            // Check if the value is a date
                            if (cell.DataType == XLDataType.DateTime)
                            {
                                DateTime dateValue = cell.GetDateTime();
                                // Format the date as "DD-MM-YYYY"
                                newRow[i] = dateValue.ToString("dd-MM-yyyy");
                            }
                            else
                            {
                                // For non-date values, just add them to the DataTable
                                newRow[i] = cell.Value.ToString();
                            }
                            i++;
                        }
                        catch (Exception ex)
                        {
                            // Handle exception if any
                        }
                    }
                    dt.Rows.Add(newRow);
                }
            }
        }
    }


    private string AddCommaToRemarks(string remark)
    {
        if (!string.IsNullOrEmpty(remark))
        {
            remark += ",";
        }
        return remark;
    }
    private string UpdateRemarks(PropertiesDetails property)
    {
        decimal number;
        property.Remark = "";
        if (!decimal.TryParse(property.Area, out number))
        {
            property.Remark += "Area, ";
        }
        //if (!decimal.TryParse(property.BankScopeRecovery, out number))
        //{
        //    property.Remark += "Bank scope recovery, ";
        //}
        if (!decimal.TryParse(property.SecurityDeposit, out number))
        {
            property.Remark += "Security deposit, ";
        }
        if (!decimal.TryParse(property.Electricity_Deposit, out number))
        {
            property.Remark += "Electricity deposit, ";
        }
        if (!decimal.TryParse(property.TotalRent, out number))
        {
            property.Remark += "Total rent, ";
        }
        if (!decimal.TryParse(property.CarParkingCharges, out number))
        {
            property.Remark += "Car parking charges, ";
        }
        if (!decimal.TryParse(property.No_Car_Parking, out number))
        {
            property.Remark += "No Of car parkings, ";
        }
        //if (!decimal.TryParse(property.Another_charges, out number))
        //{
        //    property.Remark += "Another charges, ";
        //}
        //if (!decimal.TryParse(property.DG_Charges, out number))
        //{
        //    property.Remark += "DG charges, ";
        //}
        if (!decimal.TryParse(property.BasicRent, out number))
        {
            property.Remark += "Basic rent, ";
        }
        if (!decimal.TryParse(property.LandlordAmenities, out number))
        {
            property.Remark += "Landlord Amenities, ";
        }
        if (!decimal.TryParse(property.LandlordMaintenance, out number))
        {
            property.Remark += "Landloard maintenance, ";
            //AddCommaToRemarks(property.Remark);
        }
        if (!string.IsNullOrEmpty(property.Remark))
        {
            property.Remark += " must be numbers";
        }
        return property.Remark;
    }
    public DatabaseSavedInfo SaveDataIntoDataBase(List<PropertiesDetails> savePropertyList)
    {

        var savedinfo = new DatabaseSavedInfo();
        var ticks = DateTime.Now.Ticks;
        var guid = Guid.NewGuid().ToString();
        savedinfo.ExcelUniqueId = ticks.ToString() + '-' + guid;
        try
        {
            var listOfFailureRecords = new List<PropertiesDetails>();
            var masterData = FillMasterData();
            List<PropertyReqInfo> lstpropertyReqInfos = new List<PropertyReqInfo>();

            foreach (var property in savePropertyList)
            {
                PropertyReqInfo PropertyReqInfo = new PropertyReqInfo();
                try
                {
                    var save_Requst_type = masterData.REQUEST_TYPES_LIST.FirstOrDefault(x => x.PM_RT_TYPE == property.REQUEST_TYPE);

                    var save_Prop_nature = masterData.REQUEST_NATURE_LIST.FirstOrDefault(x => x.Property_Nature_Name == property.PROP_NATURE);

                    var save_Prop_Type = masterData.ACTPROP_TYPE_LIST.FirstOrDefault(x => x.PN_PROPERTYTYPE == property.PROP_TYPE);

                    var save_Acq_Trh = masterData.ACQISITION_LIST.FirstOrDefault(x => x.ACQISITION_THROUGH_NAME == property.ACQ_TRH);
                    var save_ENTITY = "";
                    var save_Requst_type_value = "";
                    var save_Prop_nature_value = "";
                    var save_Prop_Type_value = "";
                    var save_Acq_Trh_value = "";
                    var city_value = "";

                    var location_code = "";
                    var tower_code = "";
                    var floor_code = "";

                    var location_Zone = property.LCM_ZONE;

                    var location_name = "";

                    if (save_Requst_type != null)
                    {
                        save_Requst_type_value = save_Requst_type.PM_RT_SNO;
                    }
                    else
                    {
                        save_Requst_type_value = "1";
                    }

                    if (save_Prop_nature != null)
                    {
                        save_Prop_nature_value = save_Prop_nature.Property_Nature_ID;
                    }
                    else
                    {
                        save_Prop_nature_value = "1";
                    }

                    if (save_Prop_Type != null)
                    {
                        save_Prop_Type_value = save_Prop_Type.PN_TYPEID;
                    }
                    else
                    {
                        save_Prop_Type_value = "5";
                    }

                    if (save_Acq_Trh != null)
                    {
                        save_Acq_Trh_value = save_Acq_Trh.ACQISITION_THROUGH_ID;
                    }
                    else
                    {
                        save_Acq_Trh_value = "1";
                    }

                    var city = masterData.ACTCTY_LIST.FirstOrDefault(x => x.CTY_NAME.ToLower() == property.CITY.ToLower());

                    if (city != null)
                    {
                        city_value = city.CTY_CODE;
                    }
                    else
                    {
                        city_value = property.CITY;
                    }

                    var location_Detail = masterData.LOCATION_DATA_LIST.Where(x => x.LCM_CTY_ID == city_value); //we can find data by locaton code 
                    var find_locaction_Data = location_Detail.FirstOrDefault(x => x.LCM_NAME.ToLower() == property.LOCATION.ToLower());

                    if (find_locaction_Data != null)
                    {
                        location_code = find_locaction_Data.LCM_CODE;
                        location_name = property.LOCATION;
                    }
                    else
                    {
                        location_code = property.LOCATION_CODE;
                        location_name = property.LOCATION;
                    }

                    var tower_detail = masterData.TOWER_DATA_LIST.Where(x => x.TWR_BDG_ID == location_code);
                    var find_tower_data = tower_detail.FirstOrDefault(x => x.TWR_NAME.ToLower() == property.TOWER.ToLower());

                    if (find_tower_data != null)
                    {
                        tower_code = find_tower_data.TWR_CODE;
                    }
                    else
                    {
                        tower_code = property.TOWER;
                    }

                    var floor_detail = masterData.FLOOR_DATA_LIST.Where(x => x.FLR_BDG_ID == location_code);
                    var find_floor_data = floor_detail.FirstOrDefault(x => x.FLR_NAME.ToLower() == property.FLOOR.ToLower());

                    if (find_floor_data != null)
                    {
                        floor_code = find_floor_data.FLR_CODE;
                    }
                    else
                    {
                        floor_code = property.FLOOR;
                    }

                    if (string.IsNullOrEmpty(city_value) || string.IsNullOrEmpty(location_code) || string.IsNullOrEmpty(tower_code) || string.IsNullOrEmpty(floor_code))
                    {
                        continue;
                    }

                    DateTime? inspectionDate = null;
                    decimal superBuildUpArea = 0;
                    decimal buildUpArea = 0;
                    decimal carpetArea = 0;
                    decimal rentatableArea = 0;

                    DateTime date;
                    //if (DateTime.TryParseExact(property.Inspection_date, "dd'-'MM'-'yyyy",
                    //                           CultureInfo.InvariantCulture,
                    //                           DateTimeStyles.None,
                    //                           out date))
                    //{
                    //    inspectionDate = date.Date;
                    //}



                    //if (Decimal.TryParse(property.build_up_area, out number))
                    //{
                    //    buildUpArea = number;
                    //}

                    //if (Decimal.TryParse(property.Carpet_area, out number))
                    //{
                    //    carpetArea = number;
                    //}

                    //if (Decimal.TryParse(property.Rentable_area, out number))
                    //{
                    //    rentatableArea = number;
                    //} 
                    property.Remark = UpdateRemarks(property);
                    if (!string.IsNullOrEmpty(property.Remark))
                    {
                        UpdateErrorStatus(savedinfo, property);
                        break;
                    }
                    var param1 = new SqlParameter[1];
                    param1[0] = new SqlParameter("@REQ_TYPE", SqlDbType.VarChar);
                    param1[0].Value = "ADDPROPERTY";

                    var requid = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "GENERATE_REQUESTID", param1);
                    var dt = DateTime.Now.ToString("MMddyyyy");
                    var modifiedReqId = dt + "/PRPREQ/" + Convert.ToString(requid);
                    PropertyReqInfo.PropertyReqId = modifiedReqId;


                    SqlParameter[] param = new SqlParameter[58];
                    param[0] = new SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
                    {
                        Value = modifiedReqId
                    };
                    param[1] = new SqlParameter("@LCM_ZONE", SqlDbType.VarChar);
                    param[1].Value = location_Zone;
                    param[2] = new SqlParameter("@State", SqlDbType.VarChar);
                    param[2].Value = property.State;
                    param[3] = new SqlParameter("@CITY", SqlDbType.VarChar);
                    param[3].Value = city_value;
                    param[4] = new SqlParameter("@LOCATION", SqlDbType.VarChar);
                    param[4].Value = location_code;
                    param[5] = new SqlParameter("@UPLOADEDLOCATION_NAME", SqlDbType.VarChar);
                    param[5].Value = location_name;  //should come from database
                    param[6] = new SqlParameter("@TOWER", SqlDbType.VarChar);
                    param[6].Value = tower_code;
                    param[7] = new SqlParameter("@FLOOR", SqlDbType.VarChar);
                    param[7].Value = floor_code;
                    param[8] = new SqlParameter("@PROP_TYPE", SqlDbType.VarChar);
                    param[8].Value = save_Prop_Type_value;
                    param[9] = new SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar);
                    param[9].Value = save_Requst_type_value;
                    param[10] = new SqlParameter("@PROP_NATURE", SqlDbType.VarChar);
                    param[10].Value = save_Prop_nature_value;
                    param[11] = new SqlParameter("@ACQ_TRH", SqlDbType.VarChar);
                    param[11].Value = save_Acq_Trh_value;
                    param[12] = new SqlParameter("@PROP_Name", SqlDbType.VarChar);
                    param[12].Value = property.PROP_NAME;
                    param[13] = new SqlParameter("@PROP_ADDR", SqlDbType.VarChar);
                    param[13].Value = property.PROP_ADDR;
                    param[14] = new SqlParameter("@OWN_NAME", SqlDbType.VarChar);
                    param[14].Value = property.Owner_Name;
                    param[15] = new SqlParameter("@OWN_PH", SqlDbType.VarChar);
                    param[15].Value = property.Owner_Mobile_Number;
                    param[16] = new SqlParameter("@C_B_SB_Area", SqlDbType.VarChar);  //need to check
                    param[16].Value = property.C_B_SB_Area;
                    param[17] = new SqlParameter("@LLCARPETAREA", SqlDbType.Decimal);  //need to check
                    param[17].Value = property.Area;
                    param[18] = new SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal);
                    param[18].Value = property.Chargeable_Area;
                    param[19] = new SqlParameter("@RENTPERSQFT", SqlDbType.Decimal);
                    param[19].Value = property.Rentper_sqft;
                    param[20] = new SqlParameter("@LAN_RENT", SqlDbType.Decimal);
                    param[20].Value = property.BasicRent;
                    param[21] = new SqlParameter("@LAN_AMENTS", SqlDbType.Decimal);
                    param[21].Value = property.LandlordAmenities;
                    param[22] = new SqlParameter("@LAN_MAINT", SqlDbType.Decimal);
                    param[22].Value = property.LandlordMaintenance;
                    param[23] = new SqlParameter("@Registration_Charges", SqlDbType.Decimal);
                    param[23].Value = property.Registration_Charges;
                    param[24] = new SqlParameter("@LLPARK_CHARGS", SqlDbType.Decimal);
                    param[24].Value = property.CarParkingCharges;
                    param[25] = new SqlParameter("@TotalRent", SqlDbType.Decimal);  //need to check
                    param[25].Value = property.TotalRent;
                    param[26] = new SqlParameter("@LAN_SECURITY", SqlDbType.Decimal);
                    param[26].Value = property.SecurityDeposit;
                    param[27] = new SqlParameter("@Electricity_Deposit", SqlDbType.Decimal); //need to check
                    param[27].Value = property.Electricity_Deposit;
                    //param[28] = new SqlParameter("@EFF_DOA", SqlDbType.VarChar);
                    //param[28].Value = property.LeaseSignDate;
                    //param[29] = new SqlParameter("@PM_LAD_START_DT_AGREEMENT", SqlDbType.VarChar);
                    //param[29].Value = property.LeaseStartDate;
                    //param[30] = new SqlParameter("@EXP_DOA", SqlDbType.VarChar);
                    //param[30].Value = property.LeaseEndDate;
                    //param[31] = new SqlParameter("@RENT_DATE", SqlDbType.VarChar);
                    //param[31].Value = property.RentStartDate;
                    param[28] = new SqlParameter("@EFF_DOA", SqlDbType.DateTime);
                    param[28].Value = DateTime.ParseExact(property.LeaseSignDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    param[29] = new SqlParameter("@PM_LAD_START_DT_AGREEMENT", SqlDbType.DateTime);
                    param[29].Value = DateTime.ParseExact(property.LeaseStartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    param[30] = new SqlParameter("@EXP_DOA", SqlDbType.DateTime);
                    param[30].Value = DateTime.ParseExact(property.LeaseEndDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    param[31] = new SqlParameter("@RENT_DATE", SqlDbType.DateTime);
                    param[31].Value = DateTime.ParseExact(property.RentStartDate, "dd-MM-yyyy", CultureInfo.InvariantCulture);
                    param[32] = new SqlParameter("@Escalation", SqlDbType.VarChar);
                    param[32].Value = property.Escalation;
                    param[33] = new SqlParameter("@LockinPeriod", SqlDbType.VarChar);
                    param[33].Value = property.LockinPeriod;
                    param[34] = new SqlParameter("@NoticePeriodinMonths", SqlDbType.VarChar);
                    param[34].Value = property.NoticePeriodinMonths;
                    param[35] = new SqlParameter("@No_Car_Parking", SqlDbType.Int);
                    param[35].Value = property.No_Car_Parking;
                    param[36] = new SqlParameter("@LAN_NAME", SqlDbType.VarChar);
                    param[36].Value = property.LandlordName;
                    param[37] = new SqlParameter("@LAN_ADDRESS1", SqlDbType.VarChar);
                    param[37].Value = property.Address;
                    param[38] = new SqlParameter("@LL_State", SqlDbType.VarChar);
                    param[38].Value = property.LL_State;
                    param[39] = new SqlParameter("@LL_City", SqlDbType.VarChar);
                    param[39].Value = property.LL_City;
                    //param[39] = new SqlParameter("@LL_State", SqlDbType.VarChar);
                    //param[39].Value = property.State;
                    //param[40] = new SqlParameter("@LL_City", SqlDbType.VarChar);
                    //param[40].Value = city_value;
                    param[40] = new SqlParameter("@LAN_CONTACT_DET", SqlDbType.VarChar);
                    param[40].Value = property.ContactDetails;
                    param[41] = new SqlParameter("@PANNo", SqlDbType.VarChar);
                    param[41].Value = property.PANNo;
                    param[42] = new SqlParameter("@Pin_Code", SqlDbType.VarChar);
                    param[42].Value = property.Pin_Code;
                    param[43] = new SqlParameter("@LAN_EMAIL", SqlDbType.VarChar);
                    param[43].Value = property.Email;
                    param[44] = new SqlParameter("@LAN_GST_TAX", SqlDbType.VarChar);
                    param[44].Value = property.GST;
                    param[45] = new SqlParameter("@LAN_GST_NUM", SqlDbType.VarChar);
                    param[45].Value = property.GSTNumber;
                    param[46] = new SqlParameter("@LAN_PROP_TAX_APP", SqlDbType.VarChar);
                    param[46].Value = property.PropertyTaxApplicable;
                    param[47] = new SqlParameter("@LAN_PROP_TAX", SqlDbType.Decimal);
                    param[47].Value = property.PropertyTax;
                    param[48] = new SqlParameter("@Remarks", SqlDbType.VarChar);
                    param[48].Value = property.Remarks;
                    param[49] = new SqlParameter("@GL_code", SqlDbType.VarChar);
                    param[49].Value = property.GL_code;
                    param[50] = new SqlParameter("@INSPECTION_BY", SqlDbType.VarChar);
                    param[50].Value = HttpContext.Current.Session["UID"].ToString();  // property.Inspection_By;
                    param[51] = new SqlParameter("@INSPECTION_DATE", SqlDbType.Date);
                    param[51].Value = inspectionDate;
                    param[52] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                    param[52].Value = HttpContext.Current.Session["UID"].ToString();
                    param[53] = new SqlParameter("@CMP_ID", SqlDbType.VarChar);
                    param[53].Value = HttpContext.Current.Session["COMPANYID"].ToString();
                    param[54] = new SqlParameter("@ENTITY", SqlDbType.VarChar);
                    //param[54].Value = "IDFCFIRSTBank";  //should come from database
                    param[54].Value = property.ENTITY;
                    param[55] = new SqlParameter("@ExcelId", SqlDbType.VarChar);
                    param[55].Value = savedinfo.ExcelUniqueId;

                    SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GENERATE_REQUESTID_ADD_LEASE");
                    sp3.Command.Parameters.Add("@REQ_TYPE", "ADDLEASE", DbType.String);
                    dt = DateTime.Now.ToString("MMddyyyy");
                    var REQUEST_ID = dt + "/LESREQ/" + Convert.ToString(sp3.ExecuteScalar());

                    param[56] = new SqlParameter("@LEASE_REQUEST_ID", SqlDbType.VarChar);
                    param[56].Value = REQUEST_ID;
                    param[57] = new SqlParameter("@TOI_BLKS", SqlDbType.VarChar);
                    param[57].Value = 0;
                    

                    object SavedStatus = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_PROPERTY_SAVEASDRAFT_DETAILS1", param);//

                    if (Convert.ToString(SavedStatus) == "SUCCESS")
                    {
                        savedinfo.TotalNoOfSavedRecords += 1;
                        lstpropertyReqInfos.Add(PropertyReqInfo);
                    }
                    else
                    {
                        property.Remark = "db issue"; //later we can get db issue
                        //listOfFailureRecords.Add(property);
                        UpdateErrorStatus(savedinfo, property);
                    }


                }
                catch (Exception ex)
                {
                    property.Remark = ex.Message; //later we can get db issue
                    UpdateErrorStatus(savedinfo, property);
                }
            }

            foreach (var item in lstpropertyReqInfos)
            {
                SqlParameter[] paramProp = new SqlParameter[3];
                paramProp[0] = new SqlParameter("@REQUEST_ID", SqlDbType.VarChar);
                paramProp[0].Value = item.PropertyReqId;

                paramProp[1] = new SqlParameter("@AUR_ID", SqlDbType.VarChar);
                paramProp[1].Value = HttpContext.Current.Session["UID"].ToString();

                paramProp[2] = new SqlParameter("@CMP_ID", SqlDbType.VarChar);
                paramProp[2].Value = HttpContext.Current.Session["COMPANYID"].ToString();

                SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "CHECK_CITY_LOCATION_TOWER_FLOOR_AND_INSERT", paramProp);//
            }
        }
        catch (Exception ex)
        {
            throw;
        }

        return savedinfo;
    }

    private DatabaseSavedInfo UpdateErrorStatus(DatabaseSavedInfo savedinfo, PropertiesDetails property)
    {
        savedinfo.TotalNoOfFailureRecords += 1;
        if (savedinfo.FailureRecordDesc == null)
        {
            savedinfo.FailureRecordDesc = new List<PropertiesDetails>();
        }
        savedinfo.FailureRecordDesc.Add(property);
        return savedinfo;
    }

    public MasterData FillMasterData()
    {
        var masterData = new MasterData();

        masterData.ACQISITION_LIST = new List<ACQISITION_THROUGH>();

        masterData.REQUEST_NATURE_LIST = new List<PROPERTY_NATURE>();

        masterData.REQUEST_NATURE_LIST.Add(new PROPERTY_NATURE { Property_Nature_ID = "1", Property_Nature_Name = "Lease" });
        masterData.REQUEST_NATURE_LIST.Add(new PROPERTY_NATURE { Property_Nature_ID = "2", Property_Nature_Name = "Own" });
        masterData.REQUEST_NATURE_LIST.Add(new PROPERTY_NATURE { Property_Nature_ID = "3", Property_Nature_Name = "Leave & Licence" });

        masterData.LOCATION_ZONE_LIST = new List<string> { "north", "south", "east", "west" };

        var acQisitonRequest = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_ACQISITION_THROUGH");
        acQisitonRequest.Command.AddParameter("@dummy", HttpContext.Current.Session["UID"].ToString(), DbType.String);

        using (IDataReader reader = acQisitonRequest.GetReader())
        {
            while (reader.Read())
            {
                var acq = new ACQISITION_THROUGH();
                acq.ACQISITION_THROUGH_NAME = reader["PN_ACQISITION_THROUGH"].ToString();
                acq.ACQISITION_THROUGH_ID = reader["PN_ACQISITION_ID"].ToString();
                masterData.ACQISITION_LIST.Add(acq);
            }
        }

        masterData.REQUEST_TYPES_LIST = new List<REQUEST_TYPES>();
        var reQuestTypeRequest = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_REQUEST_TYPES");

        using (IDataReader reader = reQuestTypeRequest.GetReader())
        {
            while (reader.Read())
            {
                var rt = new REQUEST_TYPES();
                rt.PM_RT_SNO = reader["PM_RT_SNO"].ToString();
                rt.PM_RT_TYPE = reader["PM_RT_TYPE"].ToString();
                masterData.REQUEST_TYPES_LIST.Add(rt);
            }
        }


        masterData.ACTPROP_TYPE_LIST = new List<ACTPROP_TYPE>();
        var proTypeRequest = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_ACTPROPTYPE");
        proTypeRequest.Command.AddParameter("@DUMMY", HttpContext.Current.Session["UID"].ToString(), DbType.String);

        using (IDataReader reader = proTypeRequest.GetReader())
        {
            while (reader.Read())
            {
                var rt = new ACTPROP_TYPE();
                rt.PN_PROPERTYTYPE = reader["PN_PROPERTYTYPE"].ToString();
                rt.PN_TYPEID = reader["PN_TYPEID"].ToString();
                masterData.ACTPROP_TYPE_LIST.Add(rt);
            }
        }
        masterData.ACTCTY_LIST = new List<ACTCTY>();
        var getCityRequest = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_ACTCTY");
        getCityRequest.Command.AddParameter("@DUMMY", "", DbType.String);
        getCityRequest.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);

        using (IDataReader reader = getCityRequest.GetReader())
        {
            while (reader.Read())
            {
                var rt = new ACTCTY();
                rt.CTY_CODE = reader["CTY_CODE"].ToString();
                rt.CTY_NAME = reader["CTY_NAME"].ToString();
                masterData.ACTCTY_LIST.Add(rt);
            }
        }

        masterData.LOCATION_DATA_LIST = new List<LOCATION_DATA>();
        var locationRequest = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "PM_GET_ALL_LOCATION_EXCELUPLOAD");
        locationRequest.Command.AddParameter("@USR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);

        using (IDataReader reader = locationRequest.GetReader())
        {
            while (reader.Read())
            {
                var rt = new LOCATION_DATA();
                rt.LCM_CODE = reader["LCM_CODE"].ToString();
                rt.LCM_NAME = reader["LCM_NAME"].ToString();
                rt.LCM_CTY_ID = reader["LCM_CTY_ID"].ToString();
                masterData.LOCATION_DATA_LIST.Add(rt);
            }
        }

        masterData.TOWER_DATA_LIST = new List<TOWER_DATA>();
        var towerRequest = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "GET_ALL_TOWERS_EXCELUPLOAD");
        towerRequest.Command.AddParameter("@USER_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);

        using (IDataReader reader = towerRequest.GetReader())
        {
            while (reader.Read())
            {
                var rt = new TOWER_DATA();
                rt.TWR_CODE = reader["TWR_CODE"].ToString();
                rt.TWR_NAME = reader["TWR_NAME"].ToString();
                rt.TWR_BDG_ID = reader["TWR_BDG_ID"].ToString();
                masterData.TOWER_DATA_LIST.Add(rt);
            }
        }



        masterData.FLOOR_DATA_LIST = new List<FLOOR_DATA>();
        var florRequest = new SubSonic.StoredProcedure(HttpContext.Current.Session["TENANT"] + "." + "USP_GET_ALLFLOOR_EXCELUPLOAD");
        florRequest.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);

        using (IDataReader reader = florRequest.GetReader())
        {
            while (reader.Read())
            {
                var rt = new FLOOR_DATA();
                rt.FLR_CODE = reader["FLR_CODE"].ToString();
                rt.FLR_NAME = reader["FLR_NAME"].ToString();
                rt.FLR_BDG_ID = reader["FLR_BDG_ID"].ToString();
                rt.UDM_CTY_CODE = reader["UDM_CTY_CODE"].ToString();
                rt.UDM_TWR_CODE = reader["UDM_TWR_CODE"].ToString();
                masterData.FLOOR_DATA_LIST.Add(rt);
            }
        }


        return masterData;

    }


}


public static class ExcelParserHelper
{
    public static PropertiesDetails GetProperty(DataRow row, out bool isSuccess, out string parsingError)
    {
        parsingError = "";
        isSuccess = true;
        var prp = new PropertiesDetails();
        try
        {
            prp.LCM_ZONE = row.Table.Columns.Contains("Zone") ? row.FieldOrDefault<string>("Zone") : "";
            prp.State = row.Table.Columns.Contains("State") ? row.FieldOrDefault<string>("State") : "";
            prp.CITY = row.Table.Columns.Contains("City") ? row.FieldOrDefault<string>("City") : "";
            prp.LOCATION_CODE = row.Table.Columns.Contains("Location Code") ? row.FieldOrDefault<string>("Location Code") : "";
            prp.LOCATION = row.Table.Columns.Contains("Location Name") ? row.FieldOrDefault<string>("Location Name") : "";
            prp.TOWER = row.Table.Columns.Contains("Tower") ? row.FieldOrDefault<string>("Tower") : "";
            prp.FLOOR = row.Table.Columns.Contains("Floor") ? row.FieldOrDefault<string>("Floor") : "";
            prp.PROP_TYPE = row.Table.Columns.Contains("Property Type") ? row.FieldOrDefault<string>("Property Type") : "";
            prp.REQUEST_TYPE = row.Table.Columns.Contains("Request Type") ? row.FieldOrDefault<string>("Request Type") : "";
            prp.ACQ_TRH = row.Table.Columns.Contains("Property Nature") ? row.FieldOrDefault<string>("Property Nature") : "";
            prp.ENTITY = row.Table.Columns.Contains("PropertyEntity") ? row.FieldOrDefault<string>("PropertyEntity") : "";
            prp.PROP_NATURE = row.Table.Columns.Contains("Acquisition Through") ? row.FieldOrDefault<string>("Acquisition Through") : "";
            prp.PROP_NAME = row.Table.Columns.Contains("Property Name") ? row.FieldOrDefault<string>("Property Name") : "";
            prp.PROP_ADDR = row.Table.Columns.Contains("Property Address") ? row.FieldOrDefault<string>("Property Address") : "";
            prp.Owner_Name = row.Table.Columns.Contains("Owner Name") ? row.FieldOrDefault<string>("Owner Name") : "";
            prp.Owner_Mobile_Number = row.Table.Columns.Contains("Owner Mobile Number") ? row.FieldOrDefault<string>("Owner Mobile Number") : "";
            prp.C_B_SB_Area = row.Table.Columns.Contains("C/B/SB Area") ? row.FieldOrDefault<string>("C/B/SB Area") : "";
            prp.Area = row.Table.Columns.Contains("Carpet Area") ? row.FieldOrDefault<string>("Carpet Area") : "";
            prp.Chargeable_Area = row.Table.Columns.Contains("Rentable Area") ? row.FieldOrDefault<string>("Rentable Area") : "";
            prp.Rentper_sqft = row.Table.Columns.Contains("Rent per sq.ft. (on carpet)(Rs.)") ? row.FieldOrDefault<string>("Rent per sq.ft. (on carpet)(Rs.)") : "";
            prp.BasicRent = row.Table.Columns.Contains("Basic Rent P.M") ? row.FieldOrDefault<string>("Basic Rent P.M") : "";
            prp.LandlordAmenities = row.Table.Columns.Contains("Amenities P.M") ? row.FieldOrDefault<string>("Amenities P.M") : "";
            prp.LandlordMaintenance = row.Table.Columns.Contains("Maintenance P.M") ? row.FieldOrDefault<string>("Maintenance P.M") : "";
            prp.Registration_Charges = row.Table.Columns.Contains("Registration Charges") ? row.FieldOrDefault<string>("Registration Charges") : "";
            prp.CarParkingCharges = row.Table.Columns.Contains("Car Parking Charges") ? row.FieldOrDefault<string>("Car Parking Charges") : "";
            prp.TotalRent = row.Table.Columns.Contains("Total Rent P.M") ? row.FieldOrDefault<string>("Total Rent P.M") : "";
            prp.SecurityDeposit = row.Table.Columns.Contains("Security Deposit") ? row.FieldOrDefault<string>("Security Deposit") : "";
            prp.Electricity_Deposit = row.Table.Columns.Contains("Electricity Deposit") ? row.FieldOrDefault<string>("Electricity Deposit") : "";
            prp.LeaseSignDate = row.Table.Columns.Contains("Effective Date Of Agreement(DD-MM-YYYY)") ? row.FieldOrDefault<string>("Effective Date Of Agreement(DD-MM-YYYY)") : "";
            prp.LeaseStartDate = row.Table.Columns.Contains("Start Date Of Agreement(DD-MM-YYYY)") ? row.FieldOrDefault<string>("Start Date Of Agreement(DD-MM-YYYY)") : "";
            prp.LeaseEndDate = row.Table.Columns.Contains("Expiry Date Of Agreement(DD-MM-YYYY)") ? row.FieldOrDefault<string>("Expiry Date Of Agreement(DD-MM-YYYY)") : "";
            prp.RentStartDate = row.Table.Columns.Contains("Rent Commencement Date(DD-MM-YYYY)") ? row.FieldOrDefault<string>("Rent Commencement Date(DD-MM-YYYY)") : "";
            prp.Escalation = row.Table.Columns.Contains("Escalation(Yes/No)") ? row.FieldOrDefault<string>("Escalation(Yes/No)") : "";
            prp.LockinPeriod = row.Table.Columns.Contains("Lock In") ? row.FieldOrDefault<string>("Lock In") : "";
            prp.NoticePeriodinMonths = row.Table.Columns.Contains("Notice Period in Months") ? row.FieldOrDefault<string>("Notice Period in Months") : "";
            prp.No_Car_Parking = row.Table.Columns.Contains("Nos Car Parking") ? row.FieldOrDefault<string>("Nos Car Parking") : "";
            prp.LandlordName = row.Table.Columns.Contains("Landlord Name") ? row.FieldOrDefault<string>("Landlord Name") : "";
            prp.Address = row.Table.Columns.Contains("Landlord Address") ? row.FieldOrDefault<string>("Landlord Address") : "";
            prp.LL_State = row.Table.Columns.Contains("Landlord State") ? row.FieldOrDefault<string>("Landlord State") : "";
            prp.LL_City = row.Table.Columns.Contains("Landlord City") ? row.FieldOrDefault<string>("Landlord City") : "";
            //prp.State = row.Table.Columns.Contains("Landlord State") ? row.FieldOrDefault<string>("Landlord State") : "";
            //prp.CITY  = row.Table.Columns.Contains("Landlord City") ? row.FieldOrDefault<string>("Landlord City") : "";
            prp.ContactDetails = row.Table.Columns.Contains("Landlord Contact Details") ? row.FieldOrDefault<string>("Landlord Contact Details") : "";
            prp.Pin_Code = row.Table.Columns.Contains("Pin Code") ? row.FieldOrDefault<string>("Pin Code") : "";
            prp.PANNo = row.Table.Columns.Contains("PAN No") ? row.FieldOrDefault<string>("PAN No") : "";
            prp.Email = row.Table.Columns.Contains("Email") ? row.FieldOrDefault<string>("Email") : "";
            prp.GST = row.Table.Columns.Contains("GST") ? row.FieldOrDefault<string>("GST") : "";
            prp.GSTNumber = row.Table.Columns.Contains("GSTNumber") ? row.FieldOrDefault<string>("GSTNumber") : "";
            prp.PropertyTaxApplicable = row.Table.Columns.Contains("Property Tax Applicable") ? row.FieldOrDefault<string>("Property Tax Applicable") : "";
            prp.PropertyTax = row.Table.Columns.Contains("Property Tax Iibility") ? row.FieldOrDefault<string>("Property Tax Iibility") : "";
            prp.GL_code = row.Table.Columns.Contains("GL Code/Vendor Code") ? row.FieldOrDefault<string>("GL Code/Vendor Code") : "";


            return prp;
        }
        catch (Exception ex)
        {
            //some thing unexpected
            isSuccess = false;

        }

        return prp;
    }

    public static void ValidateProperty(MasterData masterData, PropertiesDetails property, out bool isSuccess, out StringBuilder reasonOfFailure)
    {
        isSuccess = true;
        reasonOfFailure = new StringBuilder();
        var prp = new PropertiesDetails();
        try
        {
            //var save_Requst_type = masterData.REQUEST_TYPES_LIST.FirstOrDefault(x => x.PM_RT_TYPE == property.REQUEST_TYPE);

            var save_Prop_nature = masterData.REQUEST_NATURE_LIST.FirstOrDefault(x => x.Property_Nature_Name == property.PROP_NATURE);

            var save_Prop_Type = masterData.ACTPROP_TYPE_LIST.FirstOrDefault(x => x.PN_PROPERTYTYPE == property.PROP_TYPE);

            var save_Acq_Trh = masterData.ACQISITION_LIST.FirstOrDefault(x => x.ACQISITION_THROUGH_NAME == property.ACQ_TRH);
            var save_Entity = "";
            var city_value = "";

            var location_code = "";

            var location_zone = masterData.LOCATION_ZONE_LIST.Contains(property.LCM_ZONE.ToLower());

            //if (!location_zone)
            //{
            //    reasonOfFailure.Append("Invalid Zone Name,");
            //    isSuccess = false;
            //}

            var city = masterData.ACTCTY_LIST.FirstOrDefault(x => x.CTY_NAME.ToLower() == property.CITY.ToLower());

            //if (city == null)
            //{
            //    reasonOfFailure.Append("New City,");
            //    isSuccess = false;
            //}

            var location_Detail = masterData.LOCATION_DATA_LIST.Where(x => x.LCM_CTY_ID == city_value); //we can find data by locaton code 
            var find_locaction_Data = location_Detail.FirstOrDefault(x => x.LCM_NAME.ToLower() == property.LOCATION.ToLower());

            //if (find_locaction_Data == null)
            //{
            //    reasonOfFailure.Append("New Location,");
            //    isSuccess = false;
            //}

            var tower_detail = masterData.TOWER_DATA_LIST.Where(x => x.TWR_BDG_ID == location_code);
            var find_tower_data = tower_detail.FirstOrDefault(x => x.TWR_NAME.ToLower() == property.TOWER.ToLower());

            //if (find_tower_data == null)
            //{
            //    reasonOfFailure.Append("New Building,");
            //    isSuccess = false;
            //}

            var floor_detail = masterData.FLOOR_DATA_LIST.Where(x => x.FLR_BDG_ID == location_code);
            var find_floor_data = floor_detail.FirstOrDefault(x => x.FLR_NAME.ToLower() == property.FLOOR.ToLower());

            //if (find_floor_data != null)
            //{
            //    reasonOfFailure.Append("New floor,");
            //    isSuccess = false;
            //}


            DateTime? inspectionDate = null;

            //DateTime date;
            //if (DateTime.TryParseExact(property.LeaseSignDate, "dd'-'MM'-'yyyy",
            //                           CultureInfo.InvariantCulture,
            //                           DateTimeStyles.None,
            //                           out date))
            //{
            //    inspectionDate = date.Date;
            //}

            //else
            //{
            //    reasonOfFailure.Append("Invalid Date Lease Sign Date");
            //    isSuccess = false;
            //}

            //if (DateTime.TryParseExact(property.LeaseStartDate, "dd'-'MM'-'yyyy",
            //                         CultureInfo.InvariantCulture,
            //                         DateTimeStyles.None,
            //                         out date))
            //{
            //    inspectionDate = date.Date;
            //}

            //else
            //{
            //    reasonOfFailure.Append("Invalid Date Lease Start Date");
            //    isSuccess = false;
            //}

            //if (DateTime.TryParseExact(property.LeaseEndDate, "dd'-'MM'-'yyyy",
            //                         CultureInfo.InvariantCulture,
            //                         DateTimeStyles.None,
            //                         out date))
            //{
            //    inspectionDate = date.Date;
            //}

            //else
            //{
            //    reasonOfFailure.Append("Invalid Date Lease End Date");
            //    isSuccess = false;
            //}

            //if (DateTime.TryParseExact(property.RentStartDate, "dd'-'MM'-'yyyy",
            //                         CultureInfo.InvariantCulture,
            //                         DateTimeStyles.None,
            //                         out date))
            //{
            //    inspectionDate = date.Date;
            //}

            //else
            //{
            //    reasonOfFailure.Append("Invalid Date Rent Start Date");
            //    isSuccess = false;
            //}

            //decimal number;
            //if (Decimal.TryParse(property.Super_Built_up_area, out number))
            //{
            //    superBuildUpArea = number;
            //    isSuccess = false;
            //}
            //else
            //{
            //    reasonOfFailure.Append("Invalid Super BuildUpArea,");
            //    isSuccess = false;
            //}

            //if (Decimal.TryParse(property.build_up_area, out number))
            //{
            //    buildUpArea = number;
            //    isSuccess = false;
            //}
            //else
            //{
            //    reasonOfFailure.Append("Invalid BuildUpArea,");
            //    isSuccess = false;
            //}
            decimal Area = 0;
            decimal number;
            if (Decimal.TryParse(property.Area, out number))
            {
                Area = number;
                isSuccess = false;
            }
            else
            {
                reasonOfFailure.Append("Invalid CarpetArea,");
                isSuccess = false;
            }
            decimal Chargeable_Area = 0;
            if (Decimal.TryParse(property.Chargeable_Area, out number))
            {
                Chargeable_Area = number;
            }
            else
            {
                reasonOfFailure.Append("Invalid Chargeable Area,");
                isSuccess = false;
            }

            decimal BasicRent = 0, CarParkingCharges = 0, TotalRent = 0, SecurityDeposit = 0, Electricity_Deposit = 0, BankScopeRecovery = 0;

            if (Decimal.TryParse(property.BasicRent, out number))
            {
                BasicRent = number;
                isSuccess = false;
            }
            else
            {
                reasonOfFailure.Append("Invalid Rent P.M,");
                isSuccess = false;
            }

            if (Decimal.TryParse(property.CarParkingCharges, out number))
            {
                CarParkingCharges = number;
            }
            else
            {
                reasonOfFailure.Append("Invalid CarParking Charges,");
                isSuccess = false;
            }
            if (Decimal.TryParse(property.SecurityDeposit, out number))
            {
                SecurityDeposit = number;
            }
            else
            {
                reasonOfFailure.Append("Invalid Security Deposit,");
                isSuccess = false;
            }

            //if (Decimal.TryParse(property.BankScopeRecovery, out number))
            //{
            //    BankScopeRecovery = number;
            //}
            //else
            //{
            //    reasonOfFailure.Append("Invalid BankScopeRecovery,");
            //    isSuccess = false;
            //}

        }
        catch (Exception ex)
        {
            //some thing unexpected
            isSuccess = false;
            reasonOfFailure.Append(ex.Message);
        }


    }



    public static T FieldOrDefault<T>(this DataRow row, string columnName)
    {
        return row.IsNull(columnName) ? default(T) : row.Field<T>(columnName);
    }

}


