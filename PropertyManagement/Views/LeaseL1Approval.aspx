<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LeaseL1Approval.aspx.vb" Inherits="PropertyManagement_Views_LeaseL1Approval" Title="VP-HR Approval" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        /*body {
            color: #6a6c6f;
            background-color: #f1f3f6;
            margin-top: 30px;
        }*/

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        /*.panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }*/

        .panel-heading {
            border-radius:10px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
    <script>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

            <%--check box validation--%>
        function validateCheckBoxesMyReq(flag) {
            var gridView = document.getElementById("<%=gvLeases.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    if (flag == "Approve")
                        return true;
                    else
                        return confirm('Are you sure you want to reject this Lease(s)?');
                }
            }
            alert("Please select atleast one Lease");
            return false;
        }
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
           <%-- <div class="widgets">
                <div ba-panel ba-panel-title="Level1 Approval" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Lease Level1 Approval </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                            <div class="widgets">
                <h3>Lease Level1 Approval </h3>
            </div>
            <div class="card">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel ID="updatepnld" runat="server">
                                    <ContentTemplate>
                                        <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" DisplayMode="List" Style="padding-bottom: 20px" ForeColor="Red" />
                                        <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Val2" runat="server" DisplayMode="List" Style="padding-bottom: 20px" ForeColor="Red" />
                                        <asp:ValidationSummary ID="ValidationSummary3" ValidationGroup="Val3" runat="server" DisplayMode="List" Style="padding-bottom: 20px" ForeColor="Red" />

                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 10px;">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-6 control-label">Search by Lease ID / Property Name/Code/City/Location<span style="color: red;">*</span></label>

                                                        <asp:RequiredFieldValidator ID="rfvTxtEmpId" runat="server" ControlToValidate="txtSearch" Display="None" ErrorMessage="Please Enter Tenant Code/Tenant Name/Property Name"
                                                            ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtSearch" runat="Server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="col-md-12">
                                                    <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Search" ValidationGroup="Val1" />
                                                    <asp:Button ID="btnReset" runat="server" CausesValidation="false" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                                </div>
                                            </div>
                                        </div>

                                        <div style="margin-top: 10px">
                                            <div class="row form-inline">
                                                <div class="form-group col-md-12">
                                                    <asp:GridView ID="gvLeases" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                                        AllowPaging="True" PageSize="5" EmptyDataText="No lease(s) Found." CssClass="table GridStyle" GridLines="none">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    Select All
                                                           <input id="chkSelect1" type="checkbox" onclick="CheckAllDataGridCheckBoxes('chkselect', this.checked)">
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkselect" runat="server" />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Lease ID">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lblReqID" runat="server" Text='<%#Eval("PM_LES_PM_LR_REQ_ID")%>' CommandArgument='<%#Eval("PM_LES_SNO")%>' CommandName="GetLeaseDetails"></asp:LinkButton>
                                                                    <asp:Label ID="lblLeaseID" runat="server" Visible="false" Text='<%#Eval("PM_LES_PM_LR_REQ_ID")%>'></asp:Label>
                                                                    <asp:Label ID="lblLesSno" runat="server" Visible="false" Text='<%#Eval("PM_LES_SNO")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Property Code">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LR_PPT_CODE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Property Name">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCode" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--  <asp:TemplateField HeaderText="Entity">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblentity" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Location">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLocation" runat="server" CssClass="clsLabel" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Property Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("PRP_PS_NAME")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Lease Start Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Lease Expiry Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Total Rent">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRent" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LES_TOT_RENT", "{0:c2}")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Created By">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Lbluser" runat="server" CssClass="clsLabel" Text='<%#Eval("AUR_KNOWN_AS")%>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                        <PagerStyle CssClass="pagination-ys" />
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <asp:HiddenField ID="hdnLSNO" runat="server" />
                                            <div id="pnlBulk" runat="server" style="margin-top: 10px">
                                                <div class="form-group col-sm-4 col-xs-6">
                                                </div>
                                                <div class="col-md-5 col-sm-8 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="col-md-12">Remarks <span style="color: red;">*</span> </label>
                                                        <asp:RequiredFieldValidator ID="rfvtxtRemarks" runat="server" ControlToValidate="txtRemarks" Display="None" ErrorMessage="Please Enter Remarks"
                                                            ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                        <div class="col-md-12">
                                                            <asp:TextBox ID="txtRemarks" CssClass="form-control" Width="100%" Height="30%"
                                                                runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-8 col-xs-12" style="margin-top: 20px">
                                                    <div class="form-group">
                                                        <asp:Button ID="btnAppall" Text="Approve" runat="server" Class="btn btn-primary btn-mm" OnClientClick="javascript:return validateCheckBoxesMyReq('Approve');" ValidationGroup="Val2"></asp:Button>
                                                        <asp:Button ID="btnRejAll" Text="Reject" runat="server" Class="btn btn-primary btn-mm" OnClientClick="javascript:return validateCheckBoxesMyReq('Reject');" ValidationGroup="Val2"></asp:Button>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="updatepanel" runat="server" visible="false">
                                                <div ba-panel ba-panel-title="Add Lease" ba-panel-class="with-scroll">
                                                    <div class="panel">
                                                        <div class="panel-heading" style="height: 41px;">
                                                            <h3 class="panel-title">View Details
                                                        <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                                <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-plus " aria-hidden="true"></i></a>
                                                            </h3>
                                                        </div>
                                                        <div class="panel-body" style="padding-right: 10px;">
                                                            <div id="AddLeaseDetails" runat="server">
                                                                <div class=" panel panel-default" runat="server" id="divspace">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Lease Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOne" class="collapse show">

                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Property<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvpropaddr1" runat="server" ControlToValidate="ddlproperty" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Select Property " InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                        <asp:DropDownList ID="ddlproperty" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="true" Enabled="false" TabIndex="1">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Property Added By</label>
                                                                                        <asp:TextBox ID="txtPropAddedBy" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Approved By</label>
                                                                                        <asp:TextBox ID="txtApprovedBy" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Property Code<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtPropCode" runat="server" ControlToValidate="txtPropCode" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Enter Property Code"></asp:RequiredFieldValidator>
                                                                                        <asp:TextBox ID="txtPropCode" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Entity</label>
                                                                                        <asp:DropDownList ID="ddlentity" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true">
                                                                                        </asp:DropDownList>
                                                                                        <%--<asp:TextBox ID="txtentity" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>--%>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Property Status<span style="color: red;"></span></label>
                                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlstatus" Display="None" ValidationGroup="Val1"
                                                                        ErrorMessage="Please Enter Property Status"></asp:RequiredFieldValidator>--%>
                                                                                        <asp:DropDownList ID="ddlstatus" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="true" TabIndex="1" Enabled="false">
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Property Type</label>
                                                                                        <div>
                                                                                            <asp:DropDownList ID="ddlPropertyType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True" Enabled="false">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Property Nature<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvddlPprtNature" runat="server" ControlToValidate="ddlPprtNature"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Property Nature"
                                                                                            InitialValue="0"></asp:RequiredFieldValidator>
                                                                                        <asp:DropDownList ID="ddlPprtNature" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true">
                                                                                            <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                                            <asp:ListItem Value="1">Lease</asp:ListItem>
                                                                                            <asp:ListItem Value="2">Own</asp:ListItem>
                                                                                            <asp:ListItem Value="3">Leave & License</asp:ListItem>
                                                                                            <%--<asp:ListItem Value="3">Farm House</asp:ListItem>
                                                                                            <asp:ListItem Value="3">Software Park</asp:ListItem>--%>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Property Inspection By</label>
                                                                                        <asp:TextBox ID="txtInspection" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Property Inspection Date</label>
                                                                                        <div class='input-group date' id='Inspecteddate'>
                                                                                            <asp:TextBox ID="txtInspecteddate" runat="server" CssClass="form-control" TabIndex="55" Enabled="false"></asp:TextBox>
                                                                                            <span class="input-group-addon">
                                                                                                <span class="fa fa-calendar" onclick="setup('Inspecteddate')"></span>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <%--<div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Apartment No</label>
                                                                                <asp:TextBox ID="txtaptno" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Staff Incahrge</label>
                                                                                <asp:TextBox ID="txtstaffincharge" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Owner Name</label>
                                                                                <asp:TextBox ID="txtownername" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>--%>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel panel-default " role="tab" id="div2">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#Area">Area Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="Area" class="collapse show">
                                                                        <div class="panel-body">
                                                                            <div class="row">

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Super Built Up Area (Sqft) </label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtSuperBulArea" runat="server" ControlToValidate="txtSuperBulArea"
                                                                                            Display="None" ErrorMessage="Please Enter Super Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server" ControlToValidate="txtUsableArea"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Super Built Up Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtSuperBulArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Built Up Area (Sqft) </label>
                                                                                        <asp:RequiredFieldValidator ID="rfvBArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                                            Display="None" ErrorMessage="Please Enter Built Up Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <%--<asp:CompareValidator ID="cmpbuiltuparea" runat="server" ControlToValidate="txtBuiltupArea" ControlToCompare="txtSuperBulArea" Type="Integer" Operator="LessThanEqual" ValidationGroup="Val1" Display="None" ErrorMessage="BuiltUp Area must be less than or equal to SuperBuilt Up Area"></asp:CompareValidator>--%>
                                                                                        <asp:RegularExpressionValidator ID="revBuiltupArea" runat="server" ControlToValidate="txtBuiltupArea"
                                                                                            Display="None" ErrorMessage="Invalid Builtup Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtBuiltupArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Carpet Area (Sqft) </label>
                                                                                        <asp:RequiredFieldValidator ID="rfCarpetarea" runat="server" ControlToValidate="txtCarpetArea"
                                                                                            Display="None" ErrorMessage="Please Enter Carpet Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <%--<asp:CompareValidator ID="cmpcarpetarea" runat="server" ControlToValidate="txtCarpetArea" ControlToCompare="txtBuiltupArea" Type="Integer" Operator="LessThanEqual" ValidationGroup="Val1" Display="None" ErrorMessage="Carpet Area must be less than or equal to BuiltUp Up Area"></asp:CompareValidator>--%>
                                                                                        <asp:RegularExpressionValidator ID="revCarpetarea" runat="server" ControlToValidate="txtOptCapacity"
                                                                                            Display="None" ErrorMessage="Invalid Carpet Area" ValidationExpression="^[0-9]*\.?[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtCarpetArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Common Area (Sqft) </label>
                                                                                        <asp:RequiredFieldValidator ID="rfvCommonarea" runat="server" ControlToValidate="txtCommonArea"
                                                                                            Display="None" ErrorMessage="Please Enter Common Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revCommon" runat="server" ControlToValidate="txtCommonArea"
                                                                                            Display="None" ErrorMessage="Invalid Common Area" ValidationExpression="^[0-9]*\.?[0-9]*$"
                                                                                            ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtCommonArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Rentable Area (Sqft) </label>
                                                                                        <asp:RequiredFieldValidator ID="rfvRentable" runat="server" ControlToValidate="txtRentableArea"
                                                                                            Display="None" ErrorMessage="Please Enter Rentable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRentableArea"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Rentable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtRentableArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Usable Area (Sqft) </label>
                                                                                        <asp:RequiredFieldValidator ID="rfvUsable" runat="server" ControlToValidate="txtUsableArea"
                                                                                            Display="None" ErrorMessage="Please Enter Usable Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revUsable" runat="server" ControlToValidate="txtUsableArea"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Usable Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtUsableArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>



                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Plot Area (Sqft) </label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtPlotArea" runat="server" ControlToValidate="txtPlotArea"
                                                                                            Display="None" ErrorMessage="Please Enter Plot Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server" ControlToValidate="txtUsableArea"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Plot Area" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtPlotArea" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Floor to Ceiling Height(ft) </label>
                                                                                        <asp:RegularExpressionValidator ID="rfvtxtCeilingHight" runat="server" ControlToValidate="txtCeilingHight"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Ceiling Hight" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtCeilingHight" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Floor to Beam Bottom Height(ft) </label>
                                                                                        <asp:RegularExpressionValidator ID="rfvtxtBeamBottomHight" runat="server" ControlToValidate="txtBeamBottomHight"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Floor to Beam Bottom Height" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtBeamBottomHight" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Maximum Capacity </label>
                                                                                        <asp:RequiredFieldValidator ID="rfvMaxCapacity" runat="server" ControlToValidate="txtMaxCapacity"
                                                                                            Display="None" ErrorMessage="Please Enter Maximum Capacity" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revmaxcapacity" runat="server" ControlToValidate="txtMaxCapacity"
                                                                                            Display="None" ErrorMessage="Invalid Maximum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtMaxCapacity" runat="server" CssClass="form-control" MaxLength="6" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Optimum Capacity </label>
                                                                                        <asp:RegularExpressionValidator ID="revOptCapacity" runat="server" ControlToValidate="txtOptCapacity"
                                                                                            Display="None" ErrorMessage="Invalid Optimum Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtOptCapacity" runat="Server" CssClass="form-control" MaxLength="6" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Seating Capacity</label>
                                                                                        <asp:RegularExpressionValidator ID="rfvtxtSeatingCapacity" runat="server" ControlToValidate="txtSeatingCapacity"
                                                                                            Display="None" ErrorMessage="Invalid Seating Capacity" ValidationExpression="^[0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Numericals With Maximum length 6')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtSeatingCapacity" runat="Server" CssClass="form-control" MaxLength="6" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Flooring Type</label>
                                                                                        <div>
                                                                                            <asp:DropDownList ID="ddlFlooringType" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false">
                                                                                                <%-- <asp:ListItem Value="1"></asp:ListItem>--%>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>FSI (Floor Space Index)</label>
                                                                                        <div>
                                                                                            <asp:DropDownList ID="ddlFSI" runat="server" AutoPostBack="true" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false">
                                                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="form-group col-sm-3 col-xs-6" runat="server" id="divFSI" visible="false">
                                                                                    <div class="form-group">
                                                                                        <label>FSI Ratio (Sqft) </label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                                                            Display="None" ErrorMessage="Please Enter FSI Ratio" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revtxtFSI" runat="server" ControlToValidate="txtFSI"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Invalid FSI Ratio" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <div>
                                                                                            <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtFSI" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Preferred Efficiency (%) </label>
                                                                                        <asp:RegularExpressionValidator ID="revtxtEfficiency" runat="server" ControlToValidate="txtEfficiency"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Invalid Preferred Efficiency" ValidationExpression="^[0-9]*\.?[0-9]*$"></asp:RegularExpressionValidator>
                                                                                        <div onmouseover="Tip('Enter Decimals With Maximum length 14')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtEfficiency" runat="server" CssClass="form-control" MaxLength="14" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel panel-default " role="tab" id="div7">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#landlordscope">landlord's scope of work</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="landlordscope" class="collapse show">
                                                                        <div class="panel-body">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Vitrified flooring as per specifications</label>
                                                                                        <asp:TextBox ID="txtvitrified" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Remarks</label>
                                                                                        <asp:TextBox ID="txtvitriremarks" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Two washrooms</label>
                                                                                        <asp:TextBox ID="txtwashroom" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Remarks</label>
                                                                                        <asp:TextBox ID="txtwashroomremarks" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Pantry</label>
                                                                                        <asp:TextBox ID="txtpantry" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Remarks</label>

                                                                                        <asp:TextBox ID="txtpantryremarks" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>

                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Rolling shutter with boxing</label>
                                                                                        <asp:TextBox ID="txtshutter" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Remarks</label>
                                                                                        <asp:TextBox ID="txtshutterremarks" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Others</label>
                                                                                        <asp:TextBox ID="txtothers" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Remarks</label>
                                                                                        <asp:TextBox ID="txtothersremarks" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>LL work completion time(days)</label>
                                                                                        <asp:TextBox ID="txtllwork" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Electric load & meter existing</label>
                                                                                        <asp:TextBox ID="txtelectricex" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Electric load & meter requirement</label>
                                                                                        <asp:TextBox ID="txtelectricrq" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAreaCostDtls">Cost Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseAreaCostDtls" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>CTS Number<span style="color: red;"></span></label>
                                                                                        <%--<asp:RequiredFieldValidator ID="rfvtxtLnumber" runat="server" ControlToValidate="txtLnumber" Display="None" ValidationGroup="Val1"
                                                                                    ErrorMessage="Please Enter CTS Number"></asp:RequiredFieldValidator>--%>
                                                                                        <div onmouseover="Tip('Enter Alphabets,Numbers and some special characters like /-\ with maximum length 50')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtLnumber" runat="server" CssClass="form-control" MaxLength="50" TabIndex="2" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Entitled Lease Amount<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ControlToValidate="txtentitle" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Enter Entitled Lease Amount"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftbetxtentitle" runat="server" TargetControlID="txtentitle" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtentitle" runat="server" CssClass="form-control" TabIndex="3" Enabled="false">0</asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Basic Rent (PM)<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvInvestedArea" runat="server" ControlToValidate="txtInvestedArea" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Enter Basic Rent"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtInvestedArea" runat="server" TargetControlID="txtInvestedArea" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtInvestedArea" runat="server" CssClass="form-control" MaxLength="15" TabIndex="4" AutoPostBack="true" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Security Deposit<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="txtpay"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Security Deposit"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtpay" runat="server" TargetControlID="txtpay" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtpay" runat="server" CssClass="form-control" TabIndex="5" MaxLength="26" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Security Deposited Months<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ControlToValidate="ddlSecurityDepMonths" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Select Property Type" InitialValue="0"></asp:RequiredFieldValidator>
                                                                                        <asp:RequiredFieldValidator ID="rfvddlSecurityDepMonths" runat="server" ControlToValidate="ddlSecurityDepMonths" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Select Security Deposited Months" InitialValue="0"></asp:RequiredFieldValidator>
                                                                                        <asp:DropDownList ID="ddlSecurityDepMonths" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="6" Enabled="false">
                                                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                                                            <asp:ListItem Value="5">5</asp:ListItem>
                                                                                            <asp:ListItem Value="6">6</asp:ListItem>
                                                                                            <asp:ListItem Value="7">7</asp:ListItem>
                                                                                            <asp:ListItem Value="8">8</asp:ListItem>
                                                                                            <asp:ListItem Value="9"></asp:ListItem>
                                                                                            <asp:ListItem Value="10">10</asp:ListItem>
                                                                                            <asp:ListItem Value="11">11</asp:ListItem>
                                                                                            <asp:ListItem Value="12">12</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Rent Free Period (In Days)<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtRentFreePeriod" runat="server" ControlToValidate="txtRentFreePeriod" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Enter Rent Free Period"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtRentFreePeriod" runat="server" TargetControlID="txtRentFreePeriod" FilterType="Numbers" ValidChars="0123456789" />
                                                                                        <asp:TextBox ID="txtRentFreePeriod" runat="server" CssClass="form-control" MaxLength="50" TabIndex="7" Enabled="false">.</asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Interior Cost (Approx)</label>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtInteriorCost" runat="server" TargetControlID="txtInteriorCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <asp:TextBox ID="txtInteriorCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="10" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Tenure</label>
                                                                                        <asp:DropDownList ID="ddlTenure" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="24" Enabled="false"></asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Cost Type On<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvrblCostType" runat="server" ControlToValidate="rblCostType" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>
                                                                                        <asp:RadioButtonList ID="rblCostType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" Enabled="false">
                                                                                            <asp:ListItem Value="Sqft" Text="Sq.ft" />
                                                                                            <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                                                        </asp:RadioButtonList>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="Costype1" runat="server" visible="false">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Rent Per Sqft (On Carpet)<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftCarpet" runat="server" ControlToValidate="txtRentPerSqftCarpet" Display="None" ValidationGroup="Val1"
                                                                                                ErrorMessage="Please Enter Rent Per Sq.ft (On Carpet)"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftbetxtRentPerSqftCarpet" runat="server" TargetControlID="txtRentPerSqftCarpet" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtRentPerSqftCarpet" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8" Enabled="false">.</asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Rent Per Sqft (On BUA)<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtRentPerSqftBUA" runat="server" ControlToValidate="txtRentPerSqftBUA" Display="None" ValidationGroup="Val1"
                                                                                                ErrorMessage="Please Enter Rent Per Sq.ft (On BUA)"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtRentPerSqftBUA" runat="server" TargetControlID="txtRentPerSqftBUA" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtRentPerSqftBUA" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9" Enabled="false">.</asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="Costype2" runat="server" visible="false">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Seat Cost<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val1"
                                                                                                ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtSeatCost" runat="server" TargetControlID="txtSeatCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8" Enabled="false">.</asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseCharges">Charges</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseCharges" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Registration Charges<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtregcharges" runat="server" ControlToValidate="txtregcharges" ValidationGroup="Val1" Display="None"
                                                                                            ErrorMessage="Please Enter Registration Charges"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtregcharges" runat="server" TargetControlID="txtregcharges" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <asp:TextBox ID="txtregcharges" runat="server" CssClass="form-control" TabIndex="11" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Stamp Duty Charges<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtsduty" runat="server" ControlToValidate="txtsduty" ValidationGroup="Val1" Display="None"
                                                                                            ErrorMessage="Please Enter Stamp Duty Charges"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtsduty" runat="server" TargetControlID="txtsduty" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <asp:TextBox ID="txtsduty" runat="server" CssClass="form-control" MaxLength="12" TabIndex="12" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Furniture & Fixtures/Amenities Charges</label>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtfurniture" runat="server" TargetControlID="txtfurniture" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <asp:TextBox ID="txtfurniture" runat="server" TabIndex="13" MaxLength="17" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Amenities Paid by</label>
                                                                                        <asp:DropDownList ID="ddlamenpaid" runat="server" CssClass="form-control selectpicker with-search" Enabled="false" data-live-search="true" TabIndex="72" AutoPostBack="true">
                                                                                            <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                                            <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Maintenance Charges<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtmain1" runat="server" ControlToValidate="txtmain1"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Maintenance Charges "></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtmain1" runat="server" TargetControlID="txtmain1" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <div onmouseover="Tip('Enter Decimals with maximum length 15')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtmain1" runat="server" CssClass="form-control" MaxLength="15" TabIndex="16" AutoPostBack="true" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Maintenance Paid by</label>
                                                                                        <asp:DropDownList ID="ddlmaintpaid" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false" TabIndex="72" AutoPostBack="true">
                                                                                            <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                                            <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>GST<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtservicetax" runat="server" ControlToValidate="txtservicetax"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Select GST "></asp:RequiredFieldValidator>
                                                                                        <%--<cc1:FilteredTextBoxExtender ID="ftetxtservicetax" runat="server" TargetControlID="txtservicetax" FilterType="Numbers,custom" ValidChars="." />--%>
                                                                                        <%--<asp:TextBox ID="txtservicetax" runat="server" CssClass="form-control" MaxLength="15" TabIndex="17" AutoPostBack="true"></asp:TextBox>--%>
                                                                                        <asp:DropDownList ID="txtservicetax" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false"
                                                                                            ToolTip="--Select--" AutoPostBack="true">
                                                                                            <asp:ListItem Value="">--Select--</asp:ListItem>
                                                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Additional Parking Charges<span style="color: red;"></span></label>
                                                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtadditionalparking" FilterType="Numbers,custom" ValidChars="." />
                                                                                        <asp:TextBox ID="txtadditionalparking" runat="server" CssClass="form-control" MaxLength="15" TabIndex="16" AutoPostBack="true" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">
                                                                            <h4 class="panel-title">
                                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseLeaseExpenses">Lease Expenses</a>
                                                                            </h4>
                                                                        </div>
                                                                        <div id="collapseLeaseExpenses" class="collapse show">
                                                                            <div class="panel-body color">
                                                                                <div class="row">

                                                                                    <div class="col-md-12 col-sm-3 col-xs-12">
                                                                                        <div class="form-group">

                                                                                            <asp:GridView ID="gvLeaseExpences" DataKeyNames="PM_EXP_SNO" runat="server" AutoGenerateColumns="false"
                                                                                                CssClass="table GridStyle" GridLines="none">
                                                                                                <Columns>
                                                                                                    <asp:TemplateField HeaderText="Service Name">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblsername" runat="server" Text='<%# Eval("NAME")%>'></asp:Label>
                                                                                                            <asp:Label ID="lblcode" runat="server" Text='<%# Eval("CODE")%>' Visible="false"></asp:Label>
                                                                                                        </ItemTemplate>

                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Service Provide Name">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblSPNAME" runat="server" Text='<%# Eval("PM_SP_NAME")%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <EditItemTemplate>
                                                                                                            <asp:Label ID="lblspno" runat="server" Text='<%# Eval("PM_SP_SNO")%>' Visible="false"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddlServiceProvider" CssClass="form-control selectpicker with-search" data-live-search="true" runat="server">
                                                                                                            </asp:DropDownList>
                                                                                                        </EditItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Input Type">
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lbliptype" runat="server" Text='<%# Eval("PM_EXP_INP_TYPE")%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <EditItemTemplate>
                                                                                                            <asp:Label ID="lblipname" runat="server" Text='<%# Eval("PM_EXP_INP_TYPE")%>' Visible="false"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddliptype" CssClass="form-control selectpicker with-search" data-live-search="true" runat="server">
                                                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                                <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                                                                <asp:ListItem Value="Percentage">Percentage (%)</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </EditItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                    <asp:TemplateField HeaderText="Component of the Lease Value">
                                                                                                        <ItemStyle Width="10%" />
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblCompValue" runat="server" Text='<%# Eval("PM_EXP_COMP_LES_VAL")%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <EditItemTemplate>
                                                                                                            <asp:TextBox ID="txtCompValue" CssClass="form-control" runat="server" Text='<%# Eval("PM_EXP_COMP_LES_VAL")%>'></asp:TextBox>
                                                                                                        </EditItemTemplate>
                                                                                                    </asp:TemplateField>

                                                                                                    <asp:TemplateField HeaderText="Paid by">
                                                                                                        <ItemStyle Width="10%" />
                                                                                                        <ItemTemplate>
                                                                                                            <asp:Label ID="lblPaidbyname" runat="server" Text='<%# Eval("PM_EXP_PAID_BY")%>'></asp:Label>
                                                                                                        </ItemTemplate>
                                                                                                        <EditItemTemplate>
                                                                                                            <asp:Label ID="lblPaidby" runat="server" Text='<%# Eval("PM_EXP_PAID_BY")%>' Visible="false"></asp:Label>
                                                                                                            <asp:DropDownList ID="ddlPaidBy" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                                <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                                                <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                                                            </asp:DropDownList>
                                                                                                        </EditItemTemplate>
                                                                                                    </asp:TemplateField>
                                                                                                </Columns>
                                                                                            </asp:GridView>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" style="padding-top: 10px;">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>
                                                                                                Total Rent (Service Tax + Maintenance Cost + Basic Rent)</label>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxttotalrent" runat="server" TargetControlID="txttotalrent" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txttotalrent" runat="server" CssClass="form-control" Enabled="false" TabIndex="23"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 10px;">
                                                                                        <div class="form-group">
                                                                                            <label>Property Tax<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtproptax" runat="server" ControlToValidate="txtproptax"
                                                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Property Tax "></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtproptax" runat="server" TargetControlID="txtproptax" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtproptax" runat="server" CssClass="form-control" MaxLength="15" TabIndex="18" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Consultancy/Brokerage Charges</label>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtbrokerage" runat="server" TargetControlID="txtbrokerage" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtbrokerage" runat="server" CssClass="form-control" TabIndex="14" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Professional Fees</label>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtpfees" runat="server" TargetControlID="txtpfees" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtpfees" runat="server" CssClass="form-control" MaxLength="12" TabIndex="15"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAgrmntDetails">Agreement Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseAgrmntDetails" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Effective Date Of Agreement<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="startdate" runat="server" ControlToValidate="txtstartagreDT"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Effective Date of Agreement"></asp:RequiredFieldValidator>
                                                                                        <div class='input-group date' id='strtdate'>
                                                                                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtstartagreDT" runat="server" CssClass="form-control" TabIndex="55" AutoPostBack="true" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                            <span class="input-group-addon">
                                                                                                <span class="fa fa-calendar" onclick="setup('strtdate')"></span>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Start Date Of Agreement<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvsdate" runat="server" ControlToValidate="txtsdate"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Effective Date of Agreement"></asp:RequiredFieldValidator>
                                                                                        <div class='input-group date' id='effdate'>
                                                                                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtsdate" runat="server" CssClass="form-control" TabIndex="55" AutoPostBack="true" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                            <span class="input-group-addon">
                                                                                                <span class="fa fa-calendar" onclick="setup('effdate')"></span>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Expiry Date Of Agreement<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvedate" runat="server" ControlToValidate="txtedate"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Expiry Date of Agreement"></asp:RequiredFieldValidator>
                                                                                        <div class='input-group date' id='fromdate'>
                                                                                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtedate" runat="server" CssClass="form-control" TabIndex="56" AutoPostBack="true" Enabled="false"> </asp:TextBox>
                                                                                            </div>
                                                                                            <span class="input-group-addon">
                                                                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Rent Commencement Date<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvrentdate" runat="server" ControlToValidate="txtrentcommencementdate"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Select Rent Commencement Date"></asp:RequiredFieldValidator>
                                                                                        <div class='input-group date' id='rentdate'>
                                                                                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtrentcommencementdate" runat="server" CssClass="form-control" TabIndex="56" AutoPostBack="true"> </asp:TextBox>
                                                                                            </div>
                                                                                            <span class="input-group-addon">
                                                                                                <span class="fa fa-calendar" onclick="setup('rentdate')"></span>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>


                                                                            </div>

                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Lock in Period (In Months)</label>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtlock" runat="server" TargetControlID="txtlock" FilterType="Numbers" ValidChars="0123456789" />
                                                                                        <asp:TextBox ID="txtlock" runat="server" CssClass="form-control" MaxLength="10" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Notice Period (in Months)</label>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtNotiePeriod" runat="server" TargetControlID="txtNotiePeriod" FilterType="Numbers" ValidChars="0123456789" />
                                                                                        <asp:TextBox ID="txtNotiePeriod" runat="server" CssClass="form-control" MaxLength="12" TabIndex="59" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Lock in Period (Date)</label>
                                                                                        <div class='input-group date' id='rentlocinpamonth'>
                                                                                            <div onmouseover="Tip('Please click on the textbox to select Date')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="Txtlockinpeamonth" runat="server" CssClass="form-control" TabIndex="56" AutoPostBack="true" Enabled="false"> </asp:TextBox>
                                                                                            </div>
                                                                                            <span class="input-group-addon">
                                                                                                <span class="fa fa-calendar" onclick="setup('rentlocinpamonth')"></span>
                                                                                            </span>
                                                                                        </div>
                                                                                        <%--<cc1:FilteredTextBoxExtender ID="ftetxtLockinperiodaount" runat="server" TargetControlID="txtLeasePeiodinYears" FilterType="Numbers,custom" ValidChars="." />
                                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form-control" TabIndex="58"></asp:TextBox>--%>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Lease Period (In Years)</label>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtLeasePeiodinYears" runat="server" TargetControlID="txtLeasePeiodinYears" FilterType="Numbers" ValidChars="0123456789" />
                                                                                        <asp:TextBox ID="txtLeasePeiodinYears" runat="server" CssClass="form-control" TabIndex="58" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>


                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Agreement To be Signed By POA<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlAgreementbyPOA" Display="None" ValidationGroup="Val1"
                                                                                            ErrorMessage="Please Select Agreement To be Signed By POA" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                        <asp:DropDownList ID="ddlAgreementbyPOA" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="29" AutoPostBack="true" Enabled="false">
                                                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-6 col-sm-12 col-xs-12 ">
                                                                                    <label>Reminder Before<span style="color: red;">*</span></label>
                                                                                    <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one reminder in Lease Escalation details."
                                                                                        ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" Display="None" runat="server" ValidationGroup="Val1" />
                                                                                    <div class="bootstrap-tagsinput">
                                                                                        <asp:CheckBoxList ID="ReminderCheckList" runat="server" CellPadding="25" CellSpacing="100" RepeatColumns="4" RepeatLayout="Table" RepeatDirection="Vertical" CausesValidation="True" TabIndex="65" Enabled="false">
                                                                                            <asp:ListItem Value="730">2 Years</asp:ListItem>
                                                                                            <asp:ListItem Value="365">1 year</asp:ListItem>
                                                                                            <asp:ListItem Value="183">6 Months</asp:ListItem>
                                                                                            <asp:ListItem Value="90">90 Days</asp:ListItem>
                                                                                            <asp:ListItem Value="60">60 Days</asp:ListItem>
                                                                                            <asp:ListItem Value="30">30 Days</asp:ListItem>
                                                                                            <asp:ListItem Value="20">20 Days</asp:ListItem>
                                                                                            <asp:ListItem Value="10">10 Days</asp:ListItem>
                                                                                            <asp:ListItem Value="7">7 Days</asp:ListItem>
                                                                                            <asp:ListItem Value="3">3 Days</asp:ListItem>
                                                                                            <asp:ListItem Value="2">2 Days</asp:ListItem>
                                                                                            <asp:ListItem Value="1">1 Day</asp:ListItem>
                                                                                        </asp:CheckBoxList>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                                <%--<div id="RentRevisionPanel" runat="server" class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseRentRevision">Rent Revision (Variable % for every year)</a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseRentRevision" class="collapse show">
                                                                <div class="panel-body color">
                                                                    <asp:Panel ID="Panel10" runat="server"></asp:Panel>
                                                                    <asp:Repeater ID="rpRevision" runat="server">
                                                                        <ItemTemplate>
                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <asp:Label ID="lblRevYear" runat="server" Text='<%# Eval("RR_Year")%>'> </asp:Label>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtRevision" runat="server" TargetControlID="txtRevision" FilterType="Numbers" ValidChars="0123456789." />
                                                                                    <asp:TextBox ID="txtRevision" class="fa-percent" runat="server" CssClass="form-control" MaxLength="12" Text='<%# Eval("RR_Percentage")%>' Enabled="false"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </div>
                                                        </div>--%>

                                                                <div id="panPOA" runat="server" visible="false" class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapsepanPOA">Power of Attorney Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapsepanPOA" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Name<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvPOAName" runat="server" ControlToValidate="txtPOAName"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Name of Power of Attorney"></asp:RequiredFieldValidator>
                                                                                        <asp:TextBox ID="txtPOAName" runat="server" CssClass="form-control" TabIndex="32" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Address<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvPOAAddress" runat="server" ControlToValidate="txtPOAAddress"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Address of Power of Attorney"></asp:RequiredFieldValidator>
                                                                                        <asp:TextBox ID="txtPOAAddress" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" Rows="4" TabIndex="33" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Contact Details<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvPOAMobile" runat="server" ControlToValidate="txtPOAMobile"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Contact Details of Power of Attorney"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtPOAMobile" runat="server" TargetControlID="txtPOAMobile" FilterType="Numbers" ValidChars="0123456789" />
                                                                                        <asp:TextBox ID="txtPOAMobile" runat="server" CssClass="form-control" MaxLength="12" TabIndex="34" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Email-ID</label>
                                                                                        <asp:RegularExpressionValidator ID="revPOAEmail" runat="server" ControlToValidate="txtPOAEmail"
                                                                                            ErrorMessage="Please Enter valid Email of Power of Attorney" Display="None" ValidationGroup="Val1"
                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                        <asp:TextBox ID="txtPOAEmail" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="35" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Type of Landlord</label>
                                                                                        <asp:TextBox ID="txtLLtype" runat="SERVER" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id="LandlordEscalation" runat="server" visible="false" class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseleaseEscDetails">Lease Escalation Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseleaseEscDetails1" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Do You Wish To Enter Lease Escalation<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvesc" runat="server" ControlToValidate="ddlesc"
                                                                                            Display="None" ValidationGroup="Val1" InitialValue="--Select--" ErrorMessage="Please Select Lease Escalation"></asp:RequiredFieldValidator>
                                                                                        <asp:DropDownList ID="ddlesc" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="60" AutoPostBack="true" Enabled="false">
                                                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <%-- <div id="EscalationType" runat="server" visible="false">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Escalation On<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="rblEscalationType" Display="None" ValidationGroup="Val1"
                                                                                                ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>
                                                                                            <asp:RadioButtonList ID="rblEscalationType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" Enabled="false">
                                                                                                <asp:ListItem Value="Lease" Text="Lease Wise" />
                                                                                                <asp:ListItem Value="LandLoard" Text="LandLord Wise" />
                                                                                            </asp:RadioButtonList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="EscalationType1" runat="server" visible="false">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Escalation Type<span style="color: red;">*</span></label>
                                                                                            <asp:DropDownList ID="ddlEscalation" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="61" Enabled="false">
                                                                                                <asp:ListItem Value="Every">Every</asp:ListItem>
                                                                                                <asp:ListItem Value="After">After</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Interval Type<span style="color: red;">*</span></label>
                                                                                            <asp:DropDownList ID="ddlintervaltype" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="61" Enabled="false">
                                                                                                <asp:ListItem Value="Yearly">Yearly</asp:ListItem>
                                                                                                <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                                                                                                <asp:ListItem Value="Flexy">Flexible</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>--%>
                                                                            </div>
                                                                            <%--<div class="row">
                                                                                <div id="EscalationType2" runat="server" visible="false">

                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Amount In<span style="color: red;">*</span></label>
                                                                                            <asp:DropDownList ID="ddlamount" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false">
                                                                                                <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                                                <asp:ListItem Value="Percentage">Percentage</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12" id="Escduration" runat="server">
                                                                                        <div class="form-group">
                                                                                            <label>Interval Duration</label>
                                                                                            <asp:TextBox ID="txtIntervaldu" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="63" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12" id="Escflex" runat="server" visible="false">
                                                                                        <div class="form-group">
                                                                                            <label>No.of Escalations</label>
                                                                                            <asp:TextBox ID="txtnofEscltions" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="63" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>--%>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <%--<div id="RentRevisionPanel" runat="server" class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseRentRevision">Rent Revision</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseRentRevision" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <asp:Panel ID="Panel10" runat="server"></asp:Panel>
                                                                            <asp:Repeater ID="Repeater2" runat="server">
                                                                                <ItemTemplate>
                                                                                    <div class="col-md-6 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <fieldset style="width: 600px">
                                                                                                <div>
                                                                                                    <asp:Label ID="lblEscno" runat="server" Text='<%# Eval("RR_NOESC")%>'> </asp:Label>
                                                                                                    <asp:Label ID="lblRevYear" runat="server" Text='<%# Eval("RR_Year")%>'> </asp:Label>
                                                                                                </div>
                                                                                                <cc1:FilteredTextBoxExtender ID="ftetxtRevision" runat="server" TargetControlID="txtRevision" FilterType="Numbers,custom" ValidChars="." />
                                                                                                <asp:TextBox ID="txtRevision" class="fa-percent" Width="150px" runat="server" CssClass="form-control" MaxLength="12" Text='<%# Eval("RR_Percentage")%>'></asp:TextBox>
                                                                                                <br />
                                                                                                <div>
                                                                                                    <asp:TextBox ID="ddlYear" runat="server" AutoPostBack="True" Style="color: #666; border: 2px solid #cbcbcb; width: 40px; border-radius: 5px; background-color: #fff; box-shadow: none;" Text='<%# Eval("RR_YEARS")%>'></asp:TextBox>
                                                                                                    <asp:Label ID="lbyear" runat="server" Width="40px"> Years</asp:Label>
                                                                                                    <asp:TextBox ID="ddlMonth" runat="server" AutoPostBack="True" Width="40px" Style="color: #666; border: 2px solid #cbcbcb; border-radius: 5px; background-color: #fff; box-shadow: none;" Text='<%# Eval("RR_MONTHS")%>'></asp:TextBox>
                                                                                                    <asp:Label ID="lbmnt" runat="server" Width="50px"> Months</asp:Label>
                                                                                                    <asp:HiddenField ID="HDN_PM_LL_ID" runat="server" Value='<%# Eval("pm_les_esc_id")%>' />
                                                                                                    <asp:TextBox ID="ddlDay" runat="server" Width="40px" Style="color: #666; border: 2px solid #cbcbcb; border-radius: 5px; background-color: #fff; box-shadow: none;" Text='<%# Eval("RR_DAYS")%>'></asp:TextBox>
                                                                                                    <asp:Label ID="lbldy" runat="server" Width="40px">  Days </asp:Label>
                                                                                                    <asp:Label ID="lbldf" runat="server" Width="70px">  (%)/Value : </asp:Label>
                                                                                                    <asp:TextBox ID="TextBox1" runat="server" MaxLength="7" Width="60px" Style="color: #666; border: 2px solid #cbcbcb; border-radius: 5px; background-color: #fff; box-shadow: none;" Text='<%# Eval("RR_Percentage")%>'></asp:TextBox>
                                                                                                </div>
                                                                                            </fieldset>
                                                                                        </div>
                                                                                    </div>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </div>
                                                                    </div>
                                                                </div>--%>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseBrokegeDetails">Brokerage Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseBrokegeDetails" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Amount Of Brokerage Paid</label>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtbrkamount" runat="server" TargetControlID="txtbrkamount" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <asp:TextBox ID="txtbrkamount" runat="server" CssClass="form-control" MaxLength="12" TabIndex="66" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Broker Name</label>
                                                                                        <asp:TextBox ID="txtbrkname" runat="server" CssClass="form-control" TabIndex="67" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Broker PAN Number</label>
                                                                                        <asp:RegularExpressionValidator ID="regpanbrk" runat="server" ControlToValidate="txtbrkpan"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Broker Pan number in Alphanumerics only"
                                                                                            ValidationExpression="^[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                                                                        <asp:RegularExpressionValidator ID="regExTextBox1" runat="server" ControlToValidate="txtbrkpan"
                                                                                            Display="None" ValidationGroup="Val1" ErrorMessage="Broker Pan card Minimum length is 10"
                                                                                            ValidationExpression=".{10}.*" />
                                                                                        <asp:TextBox ID="txtbrkpan" runat="server" TabIndex="68" CssClass="form-control" MaxLength="10" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Contact Details</label>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtbrkmob" runat="server" TargetControlID="txtbrkmob" FilterType="Numbers" ValidChars="0123456789" />
                                                                                        <asp:TextBox ID="txtbrkmob" runat="server" CssClass="form-control" TabIndex="69" Width="97%" MaxLength="15" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Broker Email</label>
                                                                                        <asp:RegularExpressionValidator ID="revbrkremail" runat="server" ControlToValidate="txtbrkremail"
                                                                                            ErrorMessage="Please Enter valid Email" Display="None" ValidationGroup="Val1"
                                                                                            ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                        <asp:TextBox ID="txtbrkremail" runat="server" CssClass="form-control" MaxLength="50" TabIndex="70" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Broker Address</label>
                                                                                        <asp:TextBox ID="txtbrkaddr" runat="server" CssClass="form-control" MaxLength="1000" TextMode="MultiLine" TabIndex="71" Rows="5" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseUtilityDetails">Utility/Power Back Up</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseUtilityDetails" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>DG Set</label>
                                                                                        <asp:RequiredFieldValidator ID="rfvddlDgSet" runat="server" ControlToValidate="ddlDgSet" InitialValue="--Select--"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select DG Set"></asp:RequiredFieldValidator>
                                                                                        <asp:DropDownList ID="ddlDgSet" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false" TabIndex="72" AutoPostBack="true">
                                                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                            <asp:ListItem Value="Landlord">Landlord</asp:ListItem>
                                                                                            <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="Dgset" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>DG Set Commercials (If Provided by Landlord) Per Unit</label>
                                                                                        <%-- <asp:RequiredFieldValidator ID="rfvtxtDgSetPerUnit" runat="server" ControlToValidate="txtDgSetPerUnit"
                                                                                    ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter DG Set Commercials (If Provided by Landlord) Per Unit"></asp:RequiredFieldValidator>
                                                                                <cc1:FilteredTextBoxExtender ID="FTEtxtDgSetPerUnit" runat="server" TargetControlID="txtDgSetPerUnit" FilterType="Numbers" ValidChars="0123456789" />--%>
                                                                                        <asp:TextBox ID="txtDgSetPerUnit" runat="server" CssClass="form-control" TabIndex="73" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>DG Set Location</label>
                                                                                        <asp:TextBox ID="txtDgSetLocation" runat="server" TabIndex="74" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Space For Servo Stabilizer</label>
                                                                                        <asp:TextBox ID="txtSpaceServoStab" runat="server" CssClass="form-control" TabIndex="75" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Electrical Meter<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvddlElectricalMeter" runat="server" ControlToValidate="ddlElectricalMeter"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Electrical Meter"></asp:RequiredFieldValidator>
                                                                                        <asp:DropDownList ID="ddlElectricalMeter" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false" TabIndex="76" AutoPostBack="true">
                                                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div id="Meter" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Meter Location<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtMeterLocation" runat="server" ControlToValidate="txtMeterLocation"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Meter Location"></asp:RequiredFieldValidator>
                                                                                        <asp:TextBox ID="txtMeterLocation" runat="server" CssClass="form-control" TabIndex="77" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Earthing Pit</label>
                                                                                        <asp:TextBox ID="txtEarthingPit" runat="server" TabIndex="78" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Available Power (KWA)</label>
                                                                                        <asp:TextBox ID="txtAvailablePower" runat="server" CssClass="form-control" TabIndex="79" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Additional Power Required (KWA)</label>
                                                                                        <asp:TextBox ID="txtAdditionalPowerKWA" runat="server" CssClass="form-control" TabIndex="80" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Power Specification</label>
                                                                                        <asp:TextBox ID="txtPowerSpecification" runat="server" CssClass="form-control" TabIndex="81" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOtherServices">Other Services</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOtherServices" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>No Of Two Wheelers Parking</label>
                                                                                        <asp:TextBox ID="txtNoOfTwoWheelerParking" runat="server" CssClass="form-control" TabIndex="82" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>No Of  Cars Parking</label>
                                                                                        <asp:TextBox ID="txtNoOfCarsParking" runat="server" CssClass="form-control" TabIndex="83" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Distance From Airport</label>
                                                                                        <asp:TextBox ID="txtDistanceFromAirPort" runat="server" CssClass="form-control" MaxLength="12" TabIndex="84" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Distance From Railway Station</label>
                                                                                        <asp:TextBox ID="txtDistanceFromRailwayStation" runat="server" TabIndex="85" MaxLength="17" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Distance From Bus Stop</label>
                                                                                        <asp:TextBox ID="txtDistanceFromBustop" runat="server" CssClass="form-control" TabIndex="86" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOtherDetails">Other Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseOtherDetails" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Competitors in Vicinity</label>
                                                                                        <asp:TextBox ID="txtCompetitorsVicinity" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Rolling Shutter<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvddlRollingShutter" runat="server" ControlToValidate="ddlRollingShutter"
                                                                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Rolling Shutter"></asp:RequiredFieldValidator>
                                                                                        <asp:DropDownList ID="ddlRollingShutter" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false">
                                                                                            <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                            <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                            <asp:ListItem Value="No">No</asp:ListItem>
                                                                                        </asp:DropDownList>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Office Equipment</label>
                                                                                        <asp:TextBox ID="txtOfficeEquipments" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Upload Documents/Images</label>
                                                                                        <div class="btn btn-primary btn-mm">
                                                                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                                                                            <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" Enabled="false" />
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <strong>Documents</strong>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">                                                </asp:Label>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-12 col-sm-3 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <div id="tblGridDocs" runat="server">
                                                                                            <asp:DataGrid ID="grdDocs" runat="server" CssClass="table GridStyle" GridLines="none" DataKeyField="PM_LDOC_SNO"
                                                                                                EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                                                                <Columns>
                                                                                                    <asp:BoundColumn Visible="False" DataField="PM_LDOC_SNO" HeaderText="ID"></asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="PM_LDOC_PATH" HeaderText="Document Name">
                                                                                                        <HeaderStyle></HeaderStyle>
                                                                                                    </asp:BoundColumn>
                                                                                                    <asp:BoundColumn DataField="PM_LDOC_CREATED_DT" HeaderText="Document Date">
                                                                                                        <HeaderStyle></HeaderStyle>
                                                                                                    </asp:BoundColumn>
                                                                                                    <asp:ButtonColumn Text="Download" CommandName="Download">
                                                                                                        <HeaderStyle></HeaderStyle>
                                                                                                    </asp:ButtonColumn>
                                                                                                </Columns>
                                                                                                <HeaderStyle ForeColor="white" BackColor="Black" />
                                                                                                <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                                                            </asp:DataGrid>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="panel panel-default">
                                                                    <div class="panel-heading">
                                                                        <h4 class="panel-title">
                                                                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseLandlordDetails">Landlord Details</a>
                                                                        </h4>
                                                                    </div>
                                                                    <div id="collapseLandlordDetails" class="collapse show">
                                                                        <div class="panel-body color">
                                                                            <div class="row">
                                                                                <asp:HiddenField ID="hdnLandlordSNO" runat="server" />
                                                                                <div class="col-md-12 col-sm-3 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <asp:GridView ID="gvlandlordItems" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                                                                            AllowPaging="True" PageSize="10" EmptyDataText="No Lease Details Found."
                                                                                            CssClass="table GridStyle" GridLines="none">
                                                                                            <PagerSettings Mode="NumericFirstLast" />
                                                                                            <Columns>
                                                                                                <asp:TemplateField HeaderText="LSNO" Visible="false">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblsno" runat="server" Text='<%#Eval("PM_LL_SNO")%>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Landlord Name">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton ID="lnksurrender" runat="server" CausesValidation="false" Text='<%#Eval("PM_LL_NAME")%>' CommandArgument='<%#Eval("PM_LL_SNO")%>'
                                                                                                            CommandName="GetLandlordDetails"></asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Address 1">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblLLAddress1" runat="server" Text='<%#Eval("PM_LL_ADDRESS1")%>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="PAN No.">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblLLPAN" runat="server" Text='<%#Eval("PM_LL_PAN")%>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Rent">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblRentAmount" runat="server" Text='<%# Eval("PM_LL_MON_RENT_PAYABLE", "{0:c2}")%>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                                <asp:TemplateField HeaderText="Security Deposit Amount">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:Label ID="lblsecamount" runat="server" Text='<%# Eval("PM_LL_SECURITY_DEPOSIT", "{0:c2}") %>'></asp:Label>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>
                                                                                            </Columns>
                                                                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                                                            <PagerStyle CssClass="pagination-ys" />
                                                                                        </asp:GridView>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div id="Landlord" visible="false" runat="server">
                                                                            <div class="panel-body color">
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <strong>Landlord Documents</strong>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <asp:Label ID="lblmesge" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                                                            </asp:Label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <asp:HiddenField ID="hdnlldocSno" runat="server" />
                                                                                    <div class="col-md-12 col-sm-3 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <div id="Div3" runat="server">
                                                                                                <asp:DataGrid ID="lldocgrid" runat="server" CssClass="table GridStyle" GridLines="none" DataKeyField="PM_LL_DOC_SNO"
                                                                                                    EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                                                                    <Columns>
                                                                                                        <asp:BoundColumn Visible="False" DataField="PM_LL_DOC_SNO" HeaderText="ID"></asp:BoundColumn>
                                                                                                        <asp:BoundColumn DataField="PM_LL_DOC_PATH" HeaderText="Document Name">
                                                                                                            <HeaderStyle></HeaderStyle>
                                                                                                        </asp:BoundColumn>
                                                                                                        <%-- <asp:BoundColumn DataField="PM_LL_NAME" HeaderText="Document Date">
                                                                                                                            <HeaderStyle></HeaderStyle>
                                                                                                                        </asp:BoundColumn>--%>
                                                                                                        <asp:ButtonColumn Text="Download" CommandName="Download">
                                                                                                            <HeaderStyle></HeaderStyle>
                                                                                                        </asp:ButtonColumn>
                                                                                                        <asp:ButtonColumn Text="Delete" CommandName="Delete">
                                                                                                            <HeaderStyle></HeaderStyle>
                                                                                                        </asp:ButtonColumn>
                                                                                                    </Columns>
                                                                                                    <HeaderStyle ForeColor="white" BackColor="Black" />
                                                                                                    <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                                                                </asp:DataGrid>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Name<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtName" runat="server" ControlToValidate="txtName"
                                                                                                ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Landlord1 Name"></asp:RequiredFieldValidator>
                                                                                            <asp:TextBox ID="txtName" runat="server" CssClass="form-control" TabIndex="30" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Address 1<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtAddress" runat="server" ControlToValidate="txtAddress"
                                                                                                ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Landlord1 Address 1"></asp:RequiredFieldValidator>
                                                                                            <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control" TabIndex="31" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Address 2</label>
                                                                                            <asp:TextBox ID="txtAddress2" runat="server" TabIndex="32" CssClass="form-control"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Address 3</label>
                                                                                            <asp:TextBox ID="txtAddress3" runat="server" CssClass="form-control" TabIndex="33" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>State<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtState" runat="server" ControlToValidate="txtL1State"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Landlord1 State"></asp:RequiredFieldValidator>
                                                                                            <asp:TextBox ID="txtL1State" runat="server" CssClass="form-control" TabIndex="33" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>City<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="35" Enabled="false">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>PIN CODE<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvld1pin" runat="server" ControlToValidate="txtld1Pin"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Landlord1 Pin number"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtld1Pin" runat="server" TargetControlID="txtld1Pin" FilterType="Numbers" ValidChars="0123456789" />
                                                                                            <div onmouseover="Tip('Enter PIN No with maximum length 10')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtld1Pin" runat="server" CssClass="form-control" TabIndex="36" MaxLength="10" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>PAN No<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtPAN" runat="server" ControlToValidate="txtPAN"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Landlord1 PAN No"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="revtxtPAN" runat="server" ControlToValidate="txtPAN" Display="None" ValidationGroup="Val1"
                                                                                                ErrorMessage="Please Enter Landlord1 Pan number in Alphanumerics only" ValidationExpression="[a-zA-Z0-9 ]*"></asp:RegularExpressionValidator>
                                                                                            <asp:RegularExpressionValidator ID="revtxtPAN1" runat="server" ControlToValidate="txtPAN" Display="None" ValidationGroup="Val1"
                                                                                                ErrorMessage="Landlord1 Pan card Minimum length should be 10" ValidationExpression=".{10}.*" />
                                                                                            <div onmouseover="Tip('Enter PAN No with maximum length 10')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtPAN" runat="server" CssClass="form-control" TabIndex="37" MaxLength="10" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>GST<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtServiceTaxApplicable" runat="server" ControlToValidate="ddlServiceTaxApplicable"
                                                                                                ValidationGroup="Val1" Display="None" InitialValue="--Select--" ErrorMessage="Please Enter Landlord1 Service Tax Applicable"></asp:RequiredFieldValidator>
                                                                                            <asp:DropDownList ID="ddlServiceTaxApplicable" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="38" AutoPostBack="true">
                                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="Serv1" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>GST Number</label>
                                                                                            <%--<asp:RequiredFieldValidator ID="rfvtxtServiceTaxlnd" runat="server" ControlToValidate="txtServiceTaxlnd"
                                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Landlord1 Service Tax"></asp:RequiredFieldValidator>--%>
                                                                                            <asp:TextBox ID="txtServiceTaxlnd" runat="server" CssClass="form-control" MaxLength="12" TabIndex="39"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Property Tax Applicable<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvddlPropertyTaxApplicable" runat="server" ControlToValidate="ddlPropertyTaxApplicable"
                                                                                                ValidationGroup="ValLandlord" Display="None" InitialValue="--Select--" ErrorMessage="Please Enter Landlord1 Property Tax Applicable"></asp:RequiredFieldValidator>
                                                                                            <asp:DropDownList ID="ddlPropertyTaxApplicable" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="40" AutoPostBack="true" Enabled="false">
                                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                                <asp:ListItem Value="No">No</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="Property1" runat="server" class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Property Tax<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtPropertyTax" runat="server" ControlToValidate="txtPropertyTax"
                                                                                                ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Landlord1 Property Tax"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtPropertyTax" runat="server" TargetControlID="txtPropertyTax" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtPropertyTax" runat="server" CssClass="form-control" TabIndex="41" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Contact Details<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtContactDetails" runat="server" ControlToValidate="txtContactDetails"
                                                                                                ValidationGroup="ValLandlord" Display="None" ErrorMessage="Please Enter Lanlord1 Contact Details"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtContactDetails" runat="server" TargetControlID="txtContactDetails" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtContactDetails" runat="server" CssClass="form-control" TabIndex="42" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Email</label>
                                                                                            <asp:RegularExpressionValidator ID="revldemail" runat="server" ControlToValidate="txtldemail"
                                                                                                ErrorMessage="Please Enter valid Email of Landlord1" Display="None" ValidationGroup="Val1"
                                                                                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                                            <asp:TextBox ID="txtldemail" runat="server" CssClass="form-control" TabIndex="43" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Amount In<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="RFVddlAmountIn" runat="server" ControlToValidate="ddlAmountIn" Display="None" ValidationGroup="ValLandlord"
                                                                                                ErrorMessage="Please Select Amount In" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                                            <asp:DropDownList ID="ddlAmountIn" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false">
                                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                                                <asp:ListItem Value="Percentage">Percentage</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Rent Payable</label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtpmonthrent" runat="server" ControlToValidate="txtpmonthrent"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Lanlord1 Monthly Rent Payable"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtpmonthrent" runat="server" TargetControlID="txtpmonthrent" FilterType="Numbers" ValidChars="0123456789." />
                                                                                            <asp:TextBox ID="txtpmonthrent" runat="server" CssClass="form-control" MaxLength="15" TabIndex="44" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>TDS</label>
                                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddltds"
                                                                                                                ValidationGroup="Val1" Display="None" InitialValue="--Select--" ErrorMessage="Please Enter Landlord1 Property TDS"></asp:RequiredFieldValidator>--%>
                                                                                            <asp:DropDownList ID="ddltds" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="40" AutoPostBack="true">
                                                                                                <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Yes</asp:ListItem>
                                                                                                <asp:ListItem Value="0">No</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div id="TDSDIV" runat="server" class="col-md-3 col-sm-12 col-xs-12" visible="false">
                                                                                        <div class="form-group">
                                                                                            <label>TDS (%)</label>
                                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txttdsper"
                                                                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Landlord1 TDS(%)"></asp:RequiredFieldValidator>
                                                                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtPropertyTax" FilterType="Numbers,custom" ValidChars="." />--%>
                                                                                            <asp:TextBox ID="txttdsper" runat="server" CssClass="form-control" TabIndex="41"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Upload Documents<a href="#" data-toggle="tooltip" title="Upload File size should not be more than 20MB">?</a></label>
                                                                                            <div class="btn btn-primary btn-mm">
                                                                                                <i class="fa fa-folder-open-o fa-lg"></i>
                                                                                                <asp:FileUpload ID="TDSFu1" runat="Server" Width="90%" AllowMultiple="True" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtllmaint"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Landlord Maintenance"></asp:RequiredFieldValidator>
                                                                                            <label>Landlord Maintenance<span style="color: red;">*</span></label>
                                                                                            <asp:TextBox ID="txtllmaint" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" TabIndex="44"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Landlord Amenities<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtllAments"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Landlord Amenities"></asp:RequiredFieldValidator>
                                                                                            <asp:TextBox ID="txtllAments" runat="server" CssClass="form-control" MaxLength="15" Enabled="false" TabIndex="44">0</asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Rent Type<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddlrenttype"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Select Rent Type"></asp:RequiredFieldValidator>
                                                                                            <asp:DropDownList ID="ddlrenttype" runat="server" Enabled="false" CssClass="form-control selectpicker with-search" data-live-search="true">
                                                                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                                                                <asp:ListItem Value="1">Rent</asp:ListItem>
                                                                                                <asp:ListItem Value="2">Rent+Maintenance</asp:ListItem>
                                                                                                <asp:ListItem Value="3">Rent+Amenities</asp:ListItem>
                                                                                                <asp:ListItem Value="4">Rent+Maint+Amenities</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Security Deposit</label>
                                                                                            <asp:RequiredFieldValidator ID="rfvtxtpsecdep" runat="server" ControlToValidate="txtpsecdep"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Lanlord1 Security Deposit"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtpsecdep" runat="server" TargetControlID="txtpsecdep" FilterType="Numbers,custom" ValidChars="." />
                                                                                            <asp:TextBox ID="txtpsecdep" runat="server" TabIndex="45" CssClass="form-control" MaxLength="15" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Security Deposit Cheque No.<span style="color: red;"></span></label>
                                                                                            <%--<asp:RequiredFieldValidator ID="rvfcheqno" runat="server" ControlToValidate="txtcheqno"
                                                                                                        Display="None" ErrorMessage="Please Enter Cheque/Draft Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>--%>
                                                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ControlToValidate="txtcheqno"
                                                                                                ErrorMessage="Enter Valid Cheque/Draft Number" ValidationExpression="^[a-zA-Z0-9, ]*$"
                                                                                                ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                                                            <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                                                                onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtcheqno" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Security Deposit Cheque Date<span style="color: red;"></span></label>
                                                                                            <%--             <asp:RequiredFieldValidator ID="ddlcheque" runat="server" ControlToValidate="txtchedddt"
                                                                                                        Display="None" ErrorMessage="Please Select Draft/Cheque Date" ValidationGroup="Val2"></asp:RequiredFieldValidator>--%>
                                                                                            <div class='input-group date' id='cheqdt'>
                                                                                                <asp:TextBox ID="txtchedddt" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                                <span class="input-group-addon">
                                                                                                    <span class="fa fa-calendar" onclick="setup('cheqdt')"></span>
                                                                                                </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Payment Mode<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvddlpaymentmode" runat="server" ControlToValidate="ddlpaymentmode" Display="None" ValidationGroup="ValLandlord"
                                                                                                ErrorMessage="Please Select Payment Mode" InitialValue="0"></asp:RequiredFieldValidator>
                                                                                            <asp:DropDownList ID="ddlpaymentmode" runat="server" Enabled="false" CssClass="form-control selectpicker with-search" data-live-search="true" AutoPostBack="True" TabIndex="48">
                                                                                            </asp:DropDownList>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row" id="panel1" runat="server">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Bank Name <span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="revBankName" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtBankName"
                                                                                                ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtBankName" runat="server" Enabled="false" TabIndex="49" CssClass="form-control"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Account Number<span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                                                                Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtAccNo" runat="server" TargetControlID="txtAccNo" FilterType="Numbers" ValidChars="0123456789" />
                                                                                            <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtAccNo" runat="server" Enabled="false" TabIndex="50" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row" id="panel2" runat="server">
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Bank Name <span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtNeftBank" runat="server" ControlToValidate="txtNeftBank"
                                                                                            Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Bank Name"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revtxtNeftBank" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtNeftBank"
                                                                                            ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtNeftBank" runat="server" TabIndex="51" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Account Number <span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtNeftAccNo" runat="server" ControlToValidate="txtNeftAccNo"
                                                                                            Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Account Number"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtNeftAccNo" runat="server" TargetControlID="txtNeftAccNo" FilterType="Numbers" ValidChars="0123456789" />
                                                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtNeftAccNo" runat="server" TabIndex="52" CssClass="form-control" MaxLength="20" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Branch Name <span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtNeftBrnch" runat="server" ControlToValidate="txtNeftBrnch"
                                                                                            Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter Branch Name"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revtxtNeftBrnch" Display="None" ValidationGroup="Val1" runat="server" ControlToValidate="txtNeftBrnch"
                                                                                            ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtNeftBrnch" runat="server" TabIndex="53" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>IFSC Code <span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtNeftIFSC" runat="server" ControlToValidate="txtNeftIFSC"
                                                                                            Display="None" ValidationGroup="ValLandlord" ErrorMessage="Please Enter IFSC Code"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revtxtNeftIFSC" Display="None" ValidationGroup="Val1"
                                                                                            runat="server" ControlToValidate="txtNeftIFSC" ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$"></asp:RegularExpressionValidator>
                                                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtNeftIFSC" runat="server" TabIndex="54" CssClass="form-control" MaxLength="16" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        <%--    <div class="panel panel-default" id="llesctiondetils" runat="server">
                                                                                <div class="panel-heading">
                                                                                    <h4 class="panel-title">
                                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseleaseEscDetails">Lease Escalation Details</a>
                                                                                    </h4>
                                                                                </div>
                                                                                <div id="collapseleaseEscDetails" class="collapse show">
                                                                                    <div class="panel-body color">
                                                                                        <div class="row">
                                                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                                <div class="form-group">
                                                                                                    <label>Do You Wish To Enter Lease Escalation<span style="color: red;">*</span></label>
                                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddllandloardescalation"
                                                                                                        Display="None" ValidationGroup="Val1" InitialValue="--Select--" ErrorMessage="Please Select Lease Escalation"></asp:RequiredFieldValidator>
                                                                                                    <asp:DropDownList ID="ddllandloardescalation" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" TabIndex="60" AutoPostBack="true" Enabled="false">
                                                                                                        <asp:ListItem Value="--Select--">--Select--</asp:ListItem>
                                                                                                        <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                                                                                        <asp:ListItem Value="No">No</asp:ListItem>
                                                                                                    </asp:DropDownList>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div id="Landloardescalation" runat="server" visible="false">
                                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                                    <div class="form-group">
                                                                                                        <label>Escalation Type<span style="color: red;">*</span></label>
                                                                                                        <asp:DropDownList ID="ddllandloardescaltype" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="61" Enabled="false">
                                                                                                            <asp:ListItem Value="Every">Every</asp:ListItem>
                                                                                                            <asp:ListItem Value="After">After</asp:ListItem>
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                                    <div class="form-group">
                                                                                                        <label>Interval Type<span style="color: red;">*</span></label>
                                                                                                        <asp:DropDownList ID="ddllandloardinterval" runat="server" CssClass="form-control selectpicker" data-live-search="true" TabIndex="61" Enabled="false">
                                                                                                            <asp:ListItem Value="Yearly">Yearly</asp:ListItem>
                                                                                                            <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                                                                                                            <asp:ListItem Value="Flexy">Flexible</asp:ListItem>
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                                    <div class="form-group">
                                                                                                        <label>Amount In<span style="color: red;">*</span></label>
                                                                                                        <asp:DropDownList ID="ddllandloardescAmount" runat="server" CssClass="form-control selectpicker with-search" data-live-search="true" Enabled="false">
                                                                                                            <asp:ListItem Value="Value">Value</asp:ListItem>
                                                                                                            <asp:ListItem Value="Percentage">Percentage</asp:ListItem>
                                                                                                        </asp:DropDownList>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="row">
                                                                                            <div class="col-md-3 col-sm-12 col-xs-12" id="LLescduration" runat="server">
                                                                                                <div class="form-group">
                                                                                                    <label>Interval Duration</label>
                                                                                                    <asp:TextBox ID="txtllescdurtn" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="63" Enabled="false"></asp:TextBox>

                                                                                                </div>
                                                                                            </div>
                                                                                            <div id="Landloardescalation1" runat="server" visible="false">
                                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                                    <div class="form-group">
                                                                                                        <label>No.of escalations</label>
                                                                                                        <asp:TextBox ID="txtnoescduration" runat="server" CssClass="form-control" MaxLength="1000" TabIndex="63" Enabled="false"></asp:TextBox>

                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div id="LandloardRentRevisionPanel" runat="server" class="panel panel-default" visible="false">
                                                                                <div class="panel-heading">
                                                                                    <h4 class="panel-title">
                                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseRentRevision">Rent Revision</a>
                                                                                    </h4>
                                                                                </div>
                                                                                <div id="collapseRentRevision1" class="collapse show">
                                                                                    <div class="panel-body color">
                                                                                        <asp:Panel ID="Panel3" runat="server"></asp:Panel>
                                                                                        <asp:Repeater ID="Repeater1" runat="server">
                                                                                            <ItemTemplate>
                                                                                                <div class="col-md-6 col-sm-12 col-xs-12">
                                                                                                    <div class="form-group">
                                                                                                        <fieldset style="width: 600px">
                                                                                                            <div>
                                                                                                                <asp:Label ID="lblEscno" runat="server" Text='<%# Eval("RR_NOESC")%>'> </asp:Label>
                                                                                                                <asp:Label ID="lblRevYear" runat="server" Text='<%# Eval("RR_Year")%>'> </asp:Label>
                                                                                                            </div>
                                                                                                            <cc1:FilteredTextBoxExtender ID="ftetxtRevision" runat="server" TargetControlID="txtRevision" FilterType="Numbers,custom" ValidChars="." />
                                                                                                            <asp:TextBox ID="txtRevision" class="fa-percent" Width="150px" runat="server" CssClass="form-control" MaxLength="12" Text='<%# Eval("RR_Percentage")%>'></asp:TextBox>
                                                                                                            <br />
                                                                                                            <div>
                                                                                                                <asp:TextBox ID="ddlYear" runat="server" AutoPostBack="True" Style="color: #666; border: 2px solid #cbcbcb; width: 40px; border-radius: 5px; background-color: #fff; box-shadow: none;" Text='<%# Eval("RR_YEARS")%>'></asp:TextBox>
                                                                                                                <asp:Label ID="lbyear" runat="server" Width="40px"> Years</asp:Label>
                                                                                                                <asp:TextBox ID="ddlMonth" runat="server" AutoPostBack="True" Width="40px" Style="color: #666; border: 2px solid #cbcbcb; border-radius: 5px; background-color: #fff; box-shadow: none;" Text='<%# Eval("RR_MONTHS")%>'></asp:TextBox>
                                                                                                                <asp:Label ID="lbmnt" runat="server" Width="50px"> Months</asp:Label>
                                                                                                                <asp:HiddenField ID="HDN_PM_LL_ID" runat="server" Value='<%# Eval("pm_les_esc_id")%>' />
                                                                                                                <asp:TextBox ID="ddlDay" runat="server" Width="40px" Style="color: #666; border: 2px solid #cbcbcb; border-radius: 5px; background-color: #fff; box-shadow: none;" Text='<%# Eval("RR_DAYS")%>'></asp:TextBox>
                                                                                                                <asp:Label ID="lbldy" runat="server" Width="40px">  Days </asp:Label>
                                                                                                                <asp:Label ID="lbldf" runat="server" Width="70px">  (%)/Value : </asp:Label>
                                                                                                                <asp:TextBox ID="TextBox1" runat="server" MaxLength="7" Width="60px" Style="color: #666; border: 2px solid #cbcbcb; border-radius: 5px; background-color: #fff; box-shadow: none;" Text='<%# Eval("RR_Percentage")%>'></asp:TextBox>
                                                                                                            </div>
                                                                                                        </fieldset>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </ItemTemplate>
                                                                                        </asp:Repeater>
                                                                                    </div>
                                                                                </div>
                                                                            </div>--%>

                                                                            <div class="col-md-12 text-right">
                                                                                <asp:Button ID="btnBack" Enabled="True" runat="server" CssClass="btn btn-primary custom-button-color " Text="Back"></asp:Button>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>

                                                            <div id="pnlSingle" runat="server" style="margin-top: 10px">
                                                                <div class="form-group col-sm-4 col-xs-6">
                                                                    <div class="form-group">
                                                                        <label class="col-md-12">Modified Remarks <span style="color: red;">*</span> </label>
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="modifiedremrks" Display="None" ErrorMessage="Please Enter Remarks"></asp:RequiredFieldValidator>
                                                                        <div class="col-md-12">
                                                                            <asp:TextBox ID="modifiedremrks" CssClass="form-control" Width="100%" Height="30%"
                                                                                runat="server" Rows="3" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-5 col-sm-7 col-xs-12">
                                                                    <div class="form-group">
                                                                        <label class="col-md-12">Remarks<span style="color: red;">*</span> </label>
                                                                        <asp:RequiredFieldValidator ID="rfvtxtRemarksSingle" runat="server" ControlToValidate="txtRemarksSingle" Display="None" ErrorMessage="Please Enter Remarks"
                                                                            ValidationGroup="Val3"></asp:RequiredFieldValidator>
                                                                        <div class="col-md-12">
                                                                            <asp:TextBox ID="txtRemarksSingle" CssClass="form-control" Width="100%" Height="30%"
                                                                                runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-3 col-sm-8 col-xs-12" style="margin-top: 20px">
                                                                    <div class="form-group">
                                                                        <asp:Button ID="btnApprove" Text="Approve" runat="server" Class="btn btn-primary btn-mm" ValidationGroup="Val3"></asp:Button>
                                                                        <asp:Button ID="btnReject" Text="Reject" runat="server" Class="btn btn-primary btn-mm" ValidationGroup="Val3"></asp:Button>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>

                    </div>
                </div>
          <%--  </div>
        </div>--%>
    <%--</div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script>
    $('.closeall').click(function () {
        $('.panel-collapse.in')
            .collapse('hide');
    });
    $('.openall').click(function () {
        $('.panel-collapse:not(".in")')
            .collapse('show');
    });
</script>


