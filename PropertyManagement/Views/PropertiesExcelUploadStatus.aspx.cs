﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PropertyManagement_Views_PropertiesExcelUploadStatus : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["DatabaseInsertResult"] != null)
            {
                DatabaseSavedInfo databaseInsertResult = Session["DatabaseInsertResult"] as DatabaseSavedInfo;

                ExcelUniqueID.Text = "Saved Excel Id:-" + databaseInsertResult.ExcelUniqueId;
                TotalSavedRecords.Text = "Total Saved Records:- " + databaseInsertResult.TotalNoOfSavedRecords;
                TotalFailureRecors.Text = "Total Failed Records:- " + databaseInsertResult.TotalNoOfFailureRecords;

                ResultGridView.DataSource = databaseInsertResult.FailureRecordDesc;
                ResultGridView.DataBind();
            }
        }
    }
}