Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports System.Drawing

Partial Class WorkSpace_SMS_Webfiles_LeaseExtension
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnsubmit)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        RegExpRem.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        'If Session("UID") = "" Then
        '    Response.Redirect(Application("FMGLogout"))
        'End If
        If Not IsPostBack Then
            BindGrid()

            pnl1.Visible = False
        End If
        txttodate.Attributes.Add("readonly", "readonly")
    End Sub

    Private Sub BindGrid()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_GRID")
            sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            Dim ds As DataSet
            ds = sp.GetDataSet()
            ViewState("reqDetails") = ds
            gvitems.DataSource = ViewState("reqDetails")
            gvitems.DataBind()

        Catch ex As Exception
            ' Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        gvitems.DataSource = ViewState("reqDetails")
        gvitems.DataBind()
        lblMsg.Text = ""

    End Sub
    'Protected Sub OnSelectedIndexChanged(sender As Object, e As EventArgs)
    '    For Each row As GridViewRow In gvitems.Rows
    '        If row.RowIndex = gvitems.SelectedIndex Then
    '            row.BackColor = ColorTranslator.FromHtml("#A1DCF2")
    '        Else
    '            row.BackColor = ColorTranslator.FromHtml("#FFFFFF")
    '        End If
    '    Next
    'End Sub

    Protected Sub gvitems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        If e.CommandName = "Extension" Then
            lblMsg2.Text = ""
            Dim gvr As GridViewRow = CType(((CType(e.CommandSource, LinkButton)).NamingContainer), GridViewRow)
            Dim RowIndex As Integer = gvr.RowIndex
            For Each row As GridViewRow In gvitems.Rows
                If row.RowIndex = RowIndex Then
                    gvitems.Rows(RowIndex).BackColor = System.Drawing.Color.SkyBlue
                Else
                    gvitems.Rows(row.RowIndex).BackColor = Color.Empty
                End If
            Next


            Dim lnkExtend As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkExtend.NamingContainer, GridViewRow)
            Dim lbllname As Label = DirectCast(gvRow.FindControl("lbllname"), Label)
            Dim lbllsno As Label = DirectCast(gvRow.FindControl("lbllsno"), Label)
            Dim lblEdate As Label = DirectCast(gvRow.FindControl("lblEdate"), Label)
            txtstore.Text = lbllname.Text
            txtsno.Text = lbllsno.Text
            'txtstore1.Text = lblEdate.Text
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_LEASE_EXPIRY_DATE")
            sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
            sp.Command.AddParameter("@LEASNO", txtsno.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtfromdate.Text = ds.Tables(0).Rows(0).Item("START_DATE")
                txtrent.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("RENT_AMOUNT"), 2)
                propname.Text = ds.Tables(0).Rows(0).Item("pm_ppt_name")
                lblstrtdate.Text = ds.Tables(0).Rows(0).Item("START_DATE").ToString()
                lblenddate.Text = ds.Tables(0).Rows(0).Item("END_DATE").ToString()
                txtsdep.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("SECURITY_DEPOSIT"), 2)
            End If
            pnl1.Visible = True
        Else
            txtfromdate.Text = ""
            pnl1.Visible = False
        End If
    End Sub
    Public Class ImageClas
        Private _fn As String
        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String
        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click

        Dim ValidateCode As Integer
        Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_COUNT_VALIDATION")
        sp1.Command.AddParameter("@LEASE_REQ_ID", txtstore.Text, DbType.String)
        ValidateCode = sp1.ExecuteScalar()
        If ValidateCode <> 0 Then
            lblMsg2.Visible = True
            lblMsg2.Text = "Lease Extension Request Raised, Waiting For Approval"
            pnl1.Visible = False
            Exit Sub
        End If
        Dim orgfilename As String = Replace(Replace(BrowsePossLtr.FileName, " ", "_"), "&", "_")
        Dim repdocdatetime As String = ""
        Dim LeaseExtentionList As List(Of ImageClas) = New List(Of ImageClas)
        Dim IC As ImageClas
        Dim i As Int32 = 0
        Dim fileSize As Int64 = 0
        If BrowsePossLtr.PostedFiles IsNot Nothing Then
            Dim count = BrowsePossLtr.PostedFiles.Count
            While (i < count)
                fileSize = BrowsePossLtr.PostedFiles(i).ContentLength + fileSize
                i = i + 1
            End While
            If (fileSize > 20971520) Then
                lblMsg2.Text = "The maximum size for upload files was 20 MB."
                Exit Sub
            End If
            For Each File In BrowsePossLtr.PostedFiles
                If File.ContentLength > 0 Then
                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Upload_Time & "_" & File.FileName
                    File.SaveAs(filePath)
                    IC = New ImageClas()
                    IC.Filename = File.FileName
                    IC.FilePath = Upload_Time & "_" & File.FileName
                    LeaseExtentionList.Add(IC)

                End If
            Next
        End If

        Try
            If CDate(txttodate.Text) < CDate(txtfromdate.Text) Then
                lblMsg2.Text = "Lease End Date should be greater than Start date"
                Exit Sub
            Else
                lblMsg2.Text = ""
                Dim param As SqlParameter() = New SqlParameter(9) {}
                'General Details
                param(0) = New SqlParameter("@LEASE", DbType.String)
                param(0).Value = txtstore.Text
                param(1) = New SqlParameter("@FROM_DATE", DbType.Date)
                param(1).Value = txtfromdate.Text
                param(2) = New SqlParameter("@LESSNO", DbType.Date)
                param(2).Value = txtsno.Text
                param(3) = New SqlParameter("@TO_DATE", DbType.Date)
                param(3).Value = txttodate.Text
                param(4) = New SqlParameter("@RENT_AMOUNT", DbType.Decimal)
                param(4).Value = txtrent.Text
                param(5) = New SqlParameter("@SECURITY_DEPOSIT", DbType.Decimal)
                param(5).Value = txtsdep.Text
                param(6) = New SqlParameter("@REMARKS", DbType.String)
                param(6).Value = txtremarks.Text
                param(7) = New SqlParameter("@DOC", SqlDbType.Structured)
                param(7).Value = UtilityService.ConvertToDataTable(LeaseExtentionList)
                ''param(10).Value = filename
                param(8) = New SqlParameter("@AURID", DbType.String)
                'param(7).Value = ddlFloor.SelectedValue
                param(8).Value = Session("UID")
                param(9) = New SqlParameter("@COMPANYID", DbType.Int32)
                param(9).Value = Session("COMPANYID")

                Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[PM_INSERT_LEASE_EXTENSION_DETAILS]", param)

                'Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_INSERT_LEASE_EXTENSION_DETAILS")
                'sp.Command.AddParameter("@LEASE", txtstore.Text, DbType.String)
                'sp.Command.AddParameter("@FROM_DATE", txtfromdate.Text, DbType.Date)
                'sp.Command.AddParameter("@LESSNO", txtsno.Text, DbType.String)
                'sp.Command.AddParameter("@TO_DATE", txttodate.Text, DbType.Date)
                'sp.Command.AddParameter("@RENT_AMOUNT", txtrent.Text, DbType.Decimal)
                'sp.Command.AddParameter("@SECURITY_DEPOSIT", txtsdep.Text, DbType.Decimal)
                'sp.Command.AddParameter("@REMARKS", txtremarks.Text, DbType.String)
                'sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
                'sp.Command.AddParameter("@AURID", Session("UID"), DbType.String)
                'sp.Command.AddParameter("@DOC", repdocdatetime, DbType.String)
                'sp.ExecuteScalar()
                'Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=60", False)


                lblMsg2.Visible = True
                lblMsg2.Text = "Lease Extension Request Successfully Submitted " + " " + txtstore.Text
                Cleardata()
                pnl1.Visible = False
                BindGrid()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub Cleardata()
        txtfromdate.Text = ""
        txttodate.Text = ""
        txtremarks.Text = ""
    End Sub

    Public Sub fillgridOnSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_EXTENSION_FILTER_GRID")
            sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            sp2.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            sp2.Command.AddParameter("@SEARCH", txtReqId.Text, DbType.String)
            Dim ds As DataSet
            ds = sp2.GetDataSet()
            ViewState("reqDetails") = ds
            gvitems.DataSource = ViewState("reqDetails")
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        Try
            fillgridOnSearch()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtReqId.Text = ""
        BindGrid()
    End Sub
End Class

Public Class ImageClas
End Class
