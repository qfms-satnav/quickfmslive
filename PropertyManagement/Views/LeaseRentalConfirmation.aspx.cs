﻿using System;
using System.Data;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using System.Globalization;
using System.Text;
using System.Data.OleDb;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Drawing;
using UtiltiyVM;
using Mono.Math;
using ClosedXML.Excel;

public partial class PropertyManagement_LeaseRentalConfirmation : System.Web.UI.Page
{
    Decimal TotalRent = 0;
    private int i;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Page.Form.Attributes.Add("enctype", "multipart/form-data");
        //ScriptManager scriptManager1 = ScriptManager.GetCurrent(this);
        //if (scriptManager1 != null)
        //{
        //    scriptManager1.RegisterPostBackControl(LinkButton1);
        //    scriptManager1.RegisterPostBackControl(gvLandlordPay);
        //}
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "anything", "refreshSelectpicker();", true);
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string host = HttpContext.Current.Request.Url.Host;
        ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
        scriptManager.RegisterPostBackControl(this.btndownload);
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 10);
        param[0].Value = HttpContext.Current.Session["UID"];
        param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
        param[1].Value = path;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
            if (Session["UID"] == "")
            {
                Response.Redirect("../login.aspx");
            }
            else
            {
                if (sdr.HasRows) { }
                else if (Request.UrlReferrer == null)
                {
                    Response.Redirect("../login.aspx");
                }
            }
        if (!IsPostBack)
        {
            BindGrid();
            BindCompany();
            panel1.Visible = false;
            panel3.Visible = false;
            panel2.Visible = false;
        }

    }
    protected void btnsearch_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA_SEARCH_BY");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@LEASE_ID", txtReqId.Text, DbType.String);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
    }
    protected void txtreset_Click(object sender, EventArgs e)
    {
        BindGrid();
        txtReqId.Text = "";
    }
    protected void BindPayMode()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_PAYMENT_MODE");
        ddlpaymentmode.DataSource = sp.GetReader();
        ddlpaymentmode.DataValueField = "CODE";
        ddlpaymentmode.DataTextField = "NAME";
        ddlpaymentmode.DataBind();
    }


    //public void LinkButton1_Click(object sender, EventArgs e)
    //{
    //    SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LL_PAYMENT_HISTORY");
    //    sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
    //    sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
    //    sp.Command.AddParameter("@REQID", txtLeaseId.Text, DbType.String);
    //    sp.Command.AddParameter("@SNO", llsno.Text, DbType.String);
    //    DataSet ds = new DataSet();
    //    ds = sp.GetDataSet();
    //    gvLandlordPay.DataSource = ds;
    //    gvLandlordPay.DataBind();
    //    mp1.Show();
    //   // Panel2.Visible = true;
    //}
    private void BindGrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();

    }
    private void BindCompany()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_COMPANIES");
        ddlcompany.DataSource = sp.GetReader();
        ddlcompany.DataValueField = "CNP_ID";
        ddlcompany.DataTextField = "CNP_NAME";
        ddlcompany.DataBind();

    }
    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gvItems.PageIndex = e.NewPageIndex;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
        lblMsg.Text = "";
    }

    string sdate = "";
    string edate = "";
    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "LeaseRent")
        {
            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int RowIndex = gvr.RowIndex;
            foreach (GridViewRow row in gvItems.Rows)
            {
                if (row.RowIndex == RowIndex)
                {
                    gvItems.Rows[RowIndex].BackColor = System.Drawing.Color.SkyBlue;
                }
                else
                {
                    gvItems.Rows[row.RowIndex].BackColor = System.Drawing.Color.Empty;
                }
            }
            lblMsg.Text = string.Empty;
            //txtWithhold.Text = string.Empty;
            ddlMonths.Items.Clear();
            ddlYears.Items.Clear();
            LinkButton lnkLease = (LinkButton)e.CommandSource;
            GridViewRow gvRow = (GridViewRow)lnkLease.NamingContainer;
            Label lblLseName = (Label)gvRow.FindControl("lbllname");
            Label lblLseSno = (Label)gvRow.FindControl("lblsno");
            Label lblRent = (Label)gvRow.FindControl("lblRent");
            Label lblSertax = (Label)gvRow.FindControl("lblSertax");
            Label TotalRentAmt = (Label)gvRow.FindControl("lblTotalRent");
            Label PayTerm = (Label)gvRow.FindControl("lblPayTerm");
            Label startDt = (Label)gvRow.FindControl("lblsdate");
            Label lblgst = (Label)gvRow.FindControl("lblgst");
            Label ToDt = (Label)gvRow.FindControl("lblEdate");
            //int dt = Convert.ToDateTime(startDt.Text).Month;
            //Dec,Mar, Jun, Sep
            sdate = startDt.Text;
            edate = ToDt.Text;
            panel1.Visible = true;
            txtSerTax.Text = lblSertax.Text;
            txtPaymentTerm.Text = PayTerm.Text;
            txtLeaseId.Text = lblLseName.Text;
            llsno.Value = lblLseSno.Text;
            ddlGST.SelectedValue = lblgst.Text;
            if (lblgst.Text == "No")
            {
                divgst.Visible = false;
            }
            PaymentDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
            LoadMonth(txtPaymentTerm.Text, startDt.Text);
            if (txtPaymentTerm.Text == "Monthly")
            {
                ddlMonths.SelectedValue = DateTime.Now.Month.ToString();
            }
            LoadYear(startDt.Text, ToDt.Text);
            ddlYears.SelectedValue = DateTime.Now.Year.ToString();
            BindLandlords(lblLseName.Text, ddlMonths.SelectedValue, ddlYears.SelectedValue);
            txtCost.Text = Session["TotalAmount"].ToString();
            //gvLandLord.DataSource = sp.GetDataSet();
            //gvLandLord.DataBind();
            BindPayMode();

        }
        else
        {
            panel1.Visible = false;
        }

    }
    private void BindLandlords(string leaseid, string month, string year)
    {
        var ad = String.Join(",", ddlMonths.Items.OfType<ListItem>().Where(r => r.Selected).Select(r => r.Value));
        Session["TotalAmount"] = 0;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_BIND_LANDLORDS_PAYMENT");
        sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@LEASE_ID", leaseid, DbType.String);
        sp.Command.AddParameter("@LEASE_MONTH", String.Join(",", ddlMonths.Items.OfType<ListItem>().Where(r => r.Selected).Select(r => r.Value)) + ",", DbType.String);
        //sp.Command.AddParameter("@LEASE_MONTH", month, DbType.String);
        sp.Command.AddParameter("@LEASE_YEAR", year, DbType.String);
        sp.Command.AddParameter("@LEASE_SNO", llsno.Value, DbType.String);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        gvLandLord.DataSource = ds;
        ViewState["reqDetails1"] = ds;
        gvLandLord.DataBind();
        if (ds.Tables[0].Rows.Count >= 1)
        {
            panel3.Visible = false;
            panel2.Visible = false;
            updatefooter();
        }
        
    }
    protected void ddlpaymentmode_selectedindexchanged(object sender, System.EventArgs e)// handles ddlpaymentmode.se
    {

        if (ddlpaymentmode.SelectedItem.Value == "2" | ddlpaymentmode.SelectedItem.Value == "4")
        {
            panel3.Visible = true;
            panel2.Visible = false;
        }
        else if (ddlpaymentmode.SelectedItem.Value == "3")
        {
            panel3.Visible = false;
            panel2.Visible = true;
        }
        else
        {
            panel3.Visible = false;
            panel2.Visible = false;
        }
    }

    private void LoadMonth(string Pterms, string StartMonth)
    {
        Dictionary<int, string> mnth = new Dictionary<int, string>();
        mnth.Add(1, "January");
        mnth.Add(2, "February");
        mnth.Add(3, "March");
        mnth.Add(4, "April");
        mnth.Add(5, "May");
        mnth.Add(6, "June");
        mnth.Add(7, "July");
        mnth.Add(8, "August");
        mnth.Add(9, "September");
        mnth.Add(10, "October");
        mnth.Add(11, "November");
        mnth.Add(12, "December");

        // Pterms = "Yearly";
        int dt = Convert.ToDateTime(StartMonth).Month;
        DateTime datetimeAd = Convert.ToDateTime(StartMonth);
        if (Pterms == "Quaterly")
        {
            int cnt = mnth.AsEnumerable().Where(x => x.Key == dt).FirstOrDefault().Key;
            ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == dt).FirstOrDefault().Value, cnt.ToString()));
            for (int i = 0; i < 3; i++)
            {
                datetimeAd = datetimeAd.AddMonths(3);
                ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Value, mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Key.ToString()));
            }
        }

        else if (Pterms == "Monthly")
        {
            var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            for (int i = 0; i < months.Length - 1; i++)
            {
                ddlMonths.Items.Add(new ListItem(months[i], (i + 1).ToString()));
            }
        }

        else if (Pterms == "HalfYearly")
        {
            int cnt = mnth.AsEnumerable().Where(x => x.Key == dt).FirstOrDefault().Key;
            ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == dt).FirstOrDefault().Value, cnt.ToString()));
            datetimeAd = datetimeAd.AddMonths(6);
            ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Value, mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Key.ToString()));
        }
        else
        {
            var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            ddlMonths.Items.Add(new ListItem(mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Value, mnth.AsEnumerable().Where(x => x.Key == datetimeAd.Month).FirstOrDefault().Key.ToString()));
        }
    }

    private void LoadYear(string StartMonth, string EndMonth)
    {
        int today = DateTime.Now.Year;
        int i = Convert.ToDateTime(StartMonth).Year;
        int j = Convert.ToDateTime(EndMonth).Year;
       // int j = DateTime.Now.Year;
        for (; i <= j; i++)
        {
            ddlYears.Items.Add(Convert.ToString(i));
            //if (i > today)
            //{              
            //  ddlYears.Items.FindByText(Convert.ToString(i)).Attributes.Add("disabled", "true");
            //}

        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            if (!Page.IsValid)
            {
                return;
            }

            //public object ValidateRent()
            //{
            //int ValidateCode = 0;
            //string LeaseId = "";
            //string requisition_Sno = "";

            //foreach (GridViewRow row in gvLandLord.Rows)
            //{
            //    if (row.RowType == DataControlRowType.DataRow)
            //    {
            //        Label SNO = (Label)row.FindControl("llId");
            //        Label lblreqid = (Label)row.FindControl("lblLeaseId");
            //        Label requisition = (Label)row.FindControl("requisition_Sno");
            //        Label lblLLmonth = (Label)row.FindControl("lblmonth");
            //        LeaseId = lblreqid.Text;
            //        requisition_Sno = requisition.Text;
            //    }
            //}
            SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_VALIDATE_LEASE_RENT");
            sp1.Command.AddParameter("@PAY_TERM", txtPaymentTerm.Text, DbType.String);
            sp1.Command.AddParameter("@LEASE_ID", txtLeaseId.Text, DbType.String);
            sp1.Command.AddParameter("@REQ_SNO", llsno.Value, DbType.String);
            sp1.Command.AddParameter("@PAY_MONTH", String.Join(",", ddlMonths.Items.OfType<ListItem>().Where(r => r.Selected).Select(r => r.Value)) + ",", DbType.String);
            sp1.Command.AddParameter("@PAY_YEAR", ddlYears.SelectedValue, DbType.String);
            sp1.Command.AddParameter("@SNO", ddlYears.SelectedValue, DbType.String);
            DataSet da = new DataSet();
            da = sp1.GetDataSet();
            if (da.Tables.Count > 0)
            {
                string ValidateCode = da.Tables[0].Rows[0]["RETURNSTATUS"].ToString();
                string months = da.Tables[0].Rows[0]["months"].ToString();
                string yrs = da.Tables[0].Rows[0]["yar"].ToString();
                lblMsg.Visible = true;
                lblMsg.Text = "The Selected Month(" + months + ") " + yrs + " is not in Between Start And End Date of the lease";
            }
            else
            {
                InsertRecord();
            }
            //}
            //int ValidateCode = 0;
            //ValidateCode = Convert.ToInt32(ValidateRent());
            //if (ValidateCode == 0)
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Rent Payment  Done for the Selected Criteria";
            //}
            //else if (ValidateCode == 2)
            //{
            //    lblMsg.Visible = true;
            //    lblMsg.Text = "Please Select The Month Between Lease Start And End Date";
            //}
            //else if (ValidateCode == 1)
            //{
            //    InsertRecord();
            //}

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void InsertRecord()
    {

        try
        {
            foreach (GridViewRow row in gvLandLord.Rows)
            {
                CheckBox chck = (CheckBox)row.FindControl("chkSelect");
                DropDownList ddlstatus = (DropDownList)row.FindControl("ddlrentpay");
                if (chck.Checked == true)
                {
                    if (ddlstatus.SelectedValue == "0")
                    {
                        lblMsg.Text = "";
                        lblMsg.Text = "Select Payment Type for Selected Checkbox";
                        return;
                    }
                }

            }
                foreach (GridViewRow row in gvLandLord.Rows)
            {

                CheckBox chck = (CheckBox)row.FindControl("chkSelect");
                Label SNO = (Label)row.FindControl("llId");
                Label LLNumber = (Label)row.FindControl("LL_NUM");
                Label lblreqid = (Label)row.FindControl("lblLeaseId");
                Label lblWHAmt = (Label)row.FindControl("lblWHAmt");
                Label lblLLName = (Label)row.FindControl("lblLseName");
                TextBox txtTDSAmt = (TextBox)row.FindControl("txtTDSAmt");
                Label lblLLTotRent = (Label)row.FindControl("lblTLLRent");
                Label lblLLBasicRent = (Label)row.FindControl("lblRentAmt");
                Label lblLLmonth = (Label)row.FindControl("lblmonth");
                Label lblPayable = (Label)row.FindControl("lblPayable");
                Label lblDue = (Label)row.FindControl("lblDue");
                Label lblMAINT = (Label)row.FindControl("lblmaintcrgs");
                Label lblAmenty = (Label)row.FindControl("lblAmenties");
               // DropDownList ddlstatus = (DropDownList)row.FindControl("ddlstatus");
                Label lblLLSno = (Label)row.FindControl("requisition_Sno");
                DropDownList amntpay = (DropDownList)gvLandLord.Rows[i].FindControl("ddlrentpay");

                if (chck.Checked == true)
                {
                    SqlParameter[] param = new SqlParameter[30];
                    param[0] = new SqlParameter("@LEASEID", lblreqid.Text);
                    param[1] = new SqlParameter("@PAY_TERMS", txtPaymentTerm.Text);
                    param[2] = new SqlParameter("@MONTH", lblLLmonth.Text);
                    param[3] = new SqlParameter("@YEAR", ddlYears.SelectedValue);
                    param[4] = new SqlParameter("@PAY_DATE", PaymentDate.Text);
                    param[5] = new SqlParameter("@WITHHOLD_AMT", lblWHAmt.Text);
                    param[6] = new SqlParameter("@LANDLORD_NAME", lblLLName.Text);
                    param[7] = new SqlParameter("@TDS_AMT", txtTDSAmt.Text);
                    param[8] = new SqlParameter("@LL_TOTAL_RENT", lblPayable.Text);
                    param[9] = new SqlParameter("@LL_BASIC_RENT", lblLLBasicRent.Text);
                    param[10] = new SqlParameter("@AUR_ID", Session["UID"].ToString());
                    param[11] = new SqlParameter("@COMPANYID", Session["COMPANYID"].ToString());
                    param[12] = new SqlParameter("@REMARKS", txtRemarks.Text);
                    param[13] = new SqlParameter("@SNO", SNO.Text);
                    param[14] = new SqlParameter("@DUE_AMT", lblDue.Text);
                    param[15] = new SqlParameter("@STATUS", 1);
                    param[16] = new SqlParameter("@LLNUM", LLNumber.Text);
                    param[24] = new SqlParameter("@LES_SNO", lblLLSno.Text);
                    param[17] = new SqlParameter("@INV_NO", txtinvno.Text);
                    param[18] = new SqlParameter("@INV_DT", txtinvdt.Text);
                    param[19] = new SqlParameter("@PAY_MODE", ddlpaymentmode.SelectedValue);
                    if (ddlpaymentmode.SelectedItem.Value == "2" || ddlpaymentmode.SelectedItem.Value == "3")
                    {
                        param[20] = new SqlParameter("@LAN_BANK", txtBankName.Text);
                        param[21] = new SqlParameter("@LAN_ACCNO","" ); /*txtAccNo.Text*/
                        param[22] = new SqlParameter("@LAN_NEFT_BRANCH", txtbrnch.Text);
                        param[23] = new SqlParameter("@LAN_NEFT_IFSC", "");
                        param[28] = new SqlParameter("@CHEQUESNO", txtChequeNo.Text);
                        param[29] = new SqlParameter("@DATE", txtChequedate.Text);
                    }
                    else if(ddlpaymentmode.SelectedItem.Value == "4") 
                    {
                        param[20] = new SqlParameter("@LAN_BANK", txtNeftBank.Text);
                        //param[21] = new SqlParameter("@LAN_ACCNO", txtNeftAccNo.Text);
                        param[21] = new SqlParameter("@LAN_ACCNO", "");
                        param[22] = new SqlParameter("@LAN_NEFT_BRANCH", txtNeftBrnch.Text);
                        param[23] = new SqlParameter("@LAN_NEFT_IFSC", txtNeftIFSC.Text);
                        //param[28] = new SqlParameter("@CHEQUESNO", txtNEFTNo.Text);
                        param[28] = new SqlParameter("@CHEQUESNO", "");
                        param[29] = new SqlParameter("@DATE", txtNEFTdate.Text);
                    }
                    else
                    { 
                        param[20] = new SqlParameter("@LAN_BANK", "");
                        param[21] = new SqlParameter("@LAN_ACCNO", "");
                        param[22] = new SqlParameter("@LAN_NEFT_BRANCH", "");
                        param[23] = new SqlParameter("@LAN_NEFT_IFSC", "");
                        param[28] = new SqlParameter("@CHEQUESNO", "");
                        param[29] = new SqlParameter("@DATE", "");
                    }
                    param[25] = new SqlParameter("@MAINT_CRGS", lblMAINT.Text);
                    param[26] = new SqlParameter("@AMENT_CRGS", lblAmenty.Text);
                    param[27] = new SqlParameter("@AMENT_PAY", amntpay.SelectedValue);                    
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_INSERT_LL_PAYMENT_DETAILS", param);
                }
                lblMsg.Visible = true;
                panel1.Visible = false;
                lblMsg.Text = "Rent Payment Payed Successfully " + lblreqid.Text;
                txtRemarks.Text = string.Empty;
                txtinvno.Text = "";
                txtinvdt.Text = "";
                txtChequeNo.Text = "";
                txtChequedate.Text = "";
                txtNEFTdate.Text = "";
                //txtNEFTNo.Text = "";
            }
        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    protected void gvLandLord_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvLandLord.PageIndex = e.NewPageIndex;
        gvLandLord.DataSource = ViewState["reqDetails1"];
        gvLandLord.DataBind();
        lblMsg.Text = "";
    }

    protected void txtTDSAmt_TextChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        TextBox txt = (TextBox)sender;
        GridViewRow gvRow = (GridViewRow)txt.Parent.Parent;
        TextBox txtTDS = (TextBox)gvRow.FindControl("txtTDSAmt");
        TextBox txtWithhold = (TextBox)gvRow.FindControl("txtWithhold");
        Label lblmaintnce = (Label)gvLandLord.Rows[i].FindControl("lblmaintcrgs");
        Label lblAments = (Label)gvLandLord.Rows[i].FindControl("lblAmenties");
        Label lblTotal = (Label)gvRow.FindControl("lblTLLRent");
        Label lblWH = (Label)gvRow.FindControl("lblWHAmt");
        Label lblLLRentAmt = (Label)gvRow.FindControl("lblRentAmt");
        Label lblPayable = (Label)gvRow.FindControl("lblPayable");
        Label lblDue = (Label)gvRow.FindControl("lblDue");
        Decimal TDS = Convert.ToDecimal((txtTDS.Text == "" ? "0" : txtTDS.Text));
        Decimal WithHold = Convert.ToDecimal((txtWithhold.Text == "" ? "0" : txtWithhold.Text));
        Decimal Servicetax = Convert.ToDecimal(txtSerTax.Text);
        Decimal LLRentAmt = Convert.ToDecimal(lblLLRentAmt.Text);
        Decimal Due = Convert.ToDecimal(lblDue.Text);
        Decimal Total = Convert.ToDecimal(lblTotal.Text);
        Decimal TotPayable = Convert.ToDecimal(lblPayable.Text);
        Decimal TDSAmt = ((LLRentAmt * TDS) / 100);
        Decimal Maint = Convert.ToDecimal(lblmaintnce.Text);
        Decimal Amentis = Convert.ToDecimal(lblAments.Text);
        //lblTotal.Text = (LLRentAmt - TDSAmt + Due).ToString();
        //lblPayable.Text = Convert.ToDecimal((LLRentAmt - TDSAmt + Due) - WH).ToString();

        Total = LLRentAmt - TDSAmt + Due;
        Decimal WithholdAmount = ((Total * WithHold) / 100);
        lblWH.Text = WithholdAmount.ToString();

        //TotPayable = Convert.ToDecimal((LLRentAmt - TDSAmt + Due) - WithholdAmount).ToString();
        TotPayable = ((LLRentAmt - TDSAmt + Due) - WithholdAmount);
        Servicetax = ((TotPayable * Servicetax) / 100);
        lblPayable.Text = Convert.ToDecimal(((LLRentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
        updatefooter();

    }
    protected void ddlrentpay_TextChanged(object sender,EventArgs e)
    {
        holdrent.Visible = true;
        btnSubmit.Text = "Pay";
        panel3.Visible = false;
        panel2.Visible = false;
        for (int i = 0; i < gvLandLord.Rows.Count; i++)
        {
            DropDownList amntpay = (DropDownList)gvLandLord.Rows[i].FindControl("ddlrentpay");
            Label lblWHAmt = (Label)gvLandLord.Rows[i].FindControl("lblWHAmt");
            Label lblRentAmt = (Label)gvLandLord.Rows[i].FindControl("lblRentAmt");
            Label lblTotal = (Label)gvLandLord.Rows[i].FindControl("lblTLLRent");
            Label lblmaintnce = (Label)gvLandLord.Rows[i].FindControl("lblmaintcrgs");
            Label lblAments = (Label)gvLandLord.Rows[i].FindControl("lblAmenties");
            TextBox txtTDS = (TextBox)gvLandLord.Rows[i].FindControl("txtTDSAmt");
            TextBox txtWithhold = (TextBox)gvLandLord.Rows[i].FindControl("txtWithhold");
            Label lblPayable = (Label)gvLandLord.Rows[i].FindControl("lblPayable");
            Label rentpaid = (Label)gvLandLord.Rows[i].FindControl("paidrent");
            Label lblDue = (Label)gvLandLord.Rows[i].FindControl("lblDue");
            Decimal TDS = Convert.ToDecimal((txtTDS.Text == "" ? "0" : txtTDS.Text));
            Decimal WithHold = Convert.ToDecimal((txtWithhold.Text == "" ? "0" : txtWithhold.Text));
            Decimal RentAmt = Convert.ToDecimal(lblRentAmt.Text);
            Decimal WHAmt = Convert.ToDecimal(lblWHAmt.Text);
            Decimal Due = Convert.ToDecimal(lblDue.Text);
            Decimal Total = Convert.ToDecimal(lblTotal.Text);
            Decimal TotPayable = Convert.ToDecimal(lblPayable.Text);
            Decimal Servicetax = Convert.ToDecimal(txtSerTax.Text);
            Decimal Maint = Convert.ToDecimal(lblmaintnce.Text);
            Decimal Amentis = Convert.ToDecimal(lblAments.Text);

            if (rentpaid.Text == "Rent")
            {
                RentAmt = 0;
            }
            else if(rentpaid.Text == "Maintainance")
            {
                Maint = 0;
            }
            else if (rentpaid.Text == "Amenities")
            {
                Amentis = 0;
            }
            else if (rentpaid.Text == "Rent+Maint")
            {
                Maint = 0;
                RentAmt = 0;
            }
            else if (rentpaid.Text == "Maint+Amenit")
            {
                Maint = 0;
                Amentis = 0;
            }
            if (amntpay.SelectedValue == "Rent")
            {
                Amentis = 0;
                Maint = 0;
                Decimal TDSAmt = ((RentAmt * TDS) / 100);
                Total = RentAmt - TDSAmt + Due;
                Decimal WithholdAmount = ((Total * WithHold) / 100);
                lblWHAmt.Text = WithholdAmount.ToString();
                TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
                Servicetax = ((TotPayable * Servicetax) / 100);
                lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
            }
            else if(amntpay.SelectedValue == "Rent+Maint+Amenit")
            {
                Decimal TDSAmt = ((RentAmt * TDS) / 100);
                Total = RentAmt - TDSAmt + Due;
                Decimal WithholdAmount = ((Total * WithHold) / 100);
                lblWHAmt.Text = WithholdAmount.ToString();
                TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
                Servicetax = ((TotPayable * Servicetax) / 100);
                lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
            }
            else if (amntpay.SelectedValue == "Rent+Ameni")
            {
                Decimal TDSAmt = ((RentAmt * TDS) / 100);
                Total = RentAmt - TDSAmt + Due;
                Decimal WithholdAmount = ((Total * WithHold) / 100);
                lblWHAmt.Text = WithholdAmount.ToString();
                TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
                Servicetax = ((TotPayable * Servicetax) / 100);
                lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due +Amentis) - WithholdAmount) + Servicetax).ToString();
            }
            else if (amntpay.SelectedValue == "Maintainance")
            {
                Amentis = 0;
                RentAmt = 0;
                Decimal TDSAmt = ((RentAmt * TDS) / 100);
                Total = RentAmt - TDSAmt + Due;
                Decimal WithholdAmount = ((Total * WithHold) / 100);
                lblWHAmt.Text = WithholdAmount.ToString();
                TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
                Servicetax = ((TotPayable * Servicetax) / 100);
                lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
            }
            else if (amntpay.SelectedValue == "Amenities")
            {
                Maint = 0;
                RentAmt = 0;
                Decimal TDSAmt = ((RentAmt * TDS) / 100);
                Total = RentAmt - TDSAmt + Due;
                Decimal WithholdAmount = ((Total * WithHold) / 100);
                lblWHAmt.Text = WithholdAmount.ToString();
                TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
                Servicetax = ((TotPayable * Servicetax) / 100);
                lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
            }
            else if (amntpay.SelectedValue == "Rent+Maint")
            {
                Amentis = 0;
                Decimal TDSAmt = ((RentAmt * TDS) / 100);
                Total = RentAmt - TDSAmt + Due;
                Decimal WithholdAmount = ((Total * WithHold) / 100);
                lblWHAmt.Text = WithholdAmount.ToString();
                TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
                Servicetax = ((TotPayable * Servicetax) / 100);
                lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
            }
            else if (amntpay.SelectedValue == "Maint+Amenit")
            {
                RentAmt = 0;
                Decimal TDSAmt = ((RentAmt * TDS) / 100);
                Total = RentAmt - TDSAmt + Due;
                Decimal WithholdAmount = ((Total * WithHold) / 100);
                lblWHAmt.Text = WithholdAmount.ToString();
                TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
                Servicetax = ((TotPayable * Servicetax) / 100);
                lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
            }
            else if (amntpay.SelectedValue == "Hold")
            {
                Decimal TDSAmt = ((RentAmt * TDS) / 100);
                Total = RentAmt - TDSAmt + Due;
                Decimal WithholdAmount = ((Total * WithHold) / 100);
                lblWHAmt.Text = WithholdAmount.ToString();
                TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
                Servicetax = ((TotPayable * Servicetax) / 100);
                lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
                holdrent.Visible = false;
                btnSubmit.Text = "Submit";
            }
        }

        updatefooter();
    }
    protected void txtWithhold_TextChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        for (int i = 0; i < gvLandLord.Rows.Count; i++)
        {

            Label lblWHAmt = (Label)gvLandLord.Rows[i].FindControl("lblWHAmt");
            Label lblRentAmt = (Label)gvLandLord.Rows[i].FindControl("lblRentAmt");
            Label lblTotal = (Label)gvLandLord.Rows[i].FindControl("lblTLLRent");
            Label lblmaintnce = (Label)gvLandLord.Rows[i].FindControl("lblmaintcrgs");
            Label lblAments = (Label)gvLandLord.Rows[i].FindControl("lblAmenties");
            TextBox txtTDS = (TextBox)gvLandLord.Rows[i].FindControl("txtTDSAmt");
            TextBox txtWithhold = (TextBox)gvLandLord.Rows[i].FindControl("txtWithhold");
            Label lblPayable = (Label)gvLandLord.Rows[i].FindControl("lblPayable");
            Label rentpaid = (Label)gvLandLord.Rows[i].FindControl("paidrent");
            Label lblDue = (Label)gvLandLord.Rows[i].FindControl("lblDue");
            Decimal TDS = Convert.ToDecimal((txtTDS.Text == "" ? "0" : txtTDS.Text));
            Decimal WithHold = Convert.ToDecimal((txtWithhold.Text == "" ? "0" : txtWithhold.Text));
            Decimal RentAmt = Convert.ToDecimal(lblRentAmt.Text);
            Decimal WHAmt = Convert.ToDecimal(lblWHAmt.Text);
            Decimal Due = Convert.ToDecimal(lblDue.Text);
            Decimal Total = Convert.ToDecimal(lblTotal.Text);
            Decimal TotPayable = Convert.ToDecimal(lblPayable.Text);
            Decimal Servicetax = Convert.ToDecimal(txtSerTax.Text);
            Decimal Maint = Convert.ToDecimal(lblmaintnce.Text);
            Decimal Amentis = Convert.ToDecimal(lblAments.Text);
            if (rentpaid.Text == "Rent")
            {
                RentAmt = 0;
                TotPayable = Maint + Amentis+ Due;
            }
            else if (rentpaid.Text == "Maintainance")
            {
                Maint = 0;
                TotPayable = RentAmt + Amentis + Due;
            }
            else if (rentpaid.Text == "Amenities")
            {
                Amentis = 0;
                TotPayable = RentAmt + Maint + Due;
            }
            else if (rentpaid.Text == "Rent+Maint")
            {
                Maint = 0;
                RentAmt = 0;
                TotPayable =  Amentis + Due;
            }
            else if (rentpaid.Text == "Maint+Amenit")
            {
                Maint = 0;
                Amentis = 0;
                TotPayable = RentAmt + Due;
            }

            Decimal TDSAmt = ((TotPayable * TDS) / 100);
            Total = TotPayable - TDSAmt + Due;

            Decimal WithholdAmount = ((Total * WithHold) / 100);
            lblWHAmt.Text = WithholdAmount.ToString();

            TotPayable = (TotPayable - WithholdAmount);
            Servicetax = ((TotPayable * Servicetax) / 100);
            //lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();
            lblPayable.Text = Convert.ToDecimal(TotPayable).ToString();
        }

        updatefooter();

    }
    protected void txtSerTax_TextChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        for (int i = 0; i < gvLandLord.Rows.Count; i++)
        {

            Label lblWHAmt = (Label)gvLandLord.Rows[i].FindControl("lblWHAmt");
            Label lblRentAmt = (Label)gvLandLord.Rows[i].FindControl("lblRentAmt");
            Label lblTotal = (Label)gvLandLord.Rows[i].FindControl("lblTLLRent");
            TextBox txtTDS = (TextBox)gvLandLord.Rows[i].FindControl("txtTDSAmt");
            TextBox txtWithhold = (TextBox)gvLandLord.Rows[i].FindControl("txtWithhold");
            Label lblmaintnce = (Label)gvLandLord.Rows[i].FindControl("lblmaintcrgs");
            Label lblAments = (Label)gvLandLord.Rows[i].FindControl("lblAmenties");
            Label lblPayable = (Label)gvLandLord.Rows[i].FindControl("lblPayable");
            Label lblDue = (Label)gvLandLord.Rows[i].FindControl("lblDue");
            Decimal TDS = Convert.ToDecimal((txtTDS.Text == "" ? "0" : txtTDS.Text));
            Decimal WithHold = Convert.ToDecimal((txtWithhold.Text == "" ? "0" : txtWithhold.Text));
            Decimal RentAmt = Convert.ToDecimal(lblRentAmt.Text);
            Decimal WHAmt = Convert.ToDecimal(lblWHAmt.Text);
            Decimal Due = Convert.ToDecimal(lblDue.Text);
            Decimal Total = Convert.ToDecimal(lblTotal.Text);
            Decimal TotPayable = Convert.ToDecimal(lblPayable.Text);
            Decimal Servicetax = Convert.ToDecimal(txtSerTax.Text);
            Decimal Maint = Convert.ToDecimal(lblmaintnce.Text);
            Decimal Amentis = Convert.ToDecimal(lblAments.Text);


            Decimal TDSAmt = ((RentAmt * TDS) / 100);
            Total = RentAmt - TDSAmt + Due + Maint + Amentis;

            Decimal WithholdAmount = ((Total * WithHold) / 100);
            lblWHAmt.Text = WithholdAmount.ToString();

            TotPayable = ((RentAmt - TDSAmt + Due) - WithholdAmount);
            Servicetax = ((TotPayable * Servicetax) / 100);
            lblPayable.Text = Convert.ToDecimal(((RentAmt - TDSAmt + Due + Maint + Amentis) - WithholdAmount) + Servicetax).ToString();

        }

        updatefooter();
    }
    public void updatefooter()
    {
        decimal dblTotalAmount = 0;


        foreach (GridViewRow row in gvLandLord.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                Label lblPayable = (Label)row.FindControl("lblPayable");
                dblTotalAmount += Decimal.Parse(lblPayable.Text);
            }
        }
        Label lblTotalRent = gvLandLord.FooterRow.FindControl("lblTotalRent") as Label;
        lblTotalRent.Text = "0";
        txtCost.Text = "0";
        lblTotalRent.Text = dblTotalAmount.ToString();
        txtCost.Text = dblTotalAmount.ToString();
    }

    protected void gvLandLord_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        decimal dblTotalAmount = Convert.ToDecimal(Session["TotalAmount"]);
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblLLTotal = (Label)e.Row.FindControl("lblTLLRent");
            Label lblRentAmt = (Label)e.Row.FindControl("lblRentAmt");
            Label lblPayable = (Label)e.Row.FindControl("lblPayable");
            Label lblmaintnce = (Label)e.Row.FindControl("lblmaintcrgs");
            Label lblAments = (Label)e.Row.FindControl("lblAmenties");
            Label lblmaintpaid = (Label)e.Row.FindControl("mntpiadby");
            Label lblAmentspaid = (Label)e.Row.FindControl("amenpidby");
            Label lblDue = (Label)e.Row.FindControl("lblDue");
            Decimal TotPayable = Convert.ToDecimal(lblPayable.Text);
            DropDownList lblstatus = (DropDownList)e.Row.FindControl("ddlrentpay");
           // ViewState ["Paymode"] = lblstatus.Text;
            Decimal Due = Convert.ToDecimal(lblDue.Text);
            Decimal Rent = Convert.ToDecimal(lblRentAmt.Text);
            Decimal Maint = Convert.ToDecimal(lblmaintnce.Text);
            Decimal Amentis = Convert.ToDecimal(lblAments.Text);
            Decimal Servicetax = Convert.ToDecimal(txtSerTax.Text);
            if (Due != 0)
            {
                lblstatus.Enabled = false;
                if (lblmaintpaid.Text == "Company" && lblAmentspaid.Text == "Company")
                {
                    lblLLTotal.Text = ( Rent + Maint + Amentis).ToString();
                    TotPayable = (Due);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text != "Company" && lblAmentspaid.Text != "Company")
                {
                    lblLLTotal.Text = ( Rent).ToString();
                    TotPayable = (Due);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text == "Company" && lblAmentspaid.Text != "Company")
                {
                    lblLLTotal.Text = (  Rent + Maint).ToString();
                    TotPayable = (Due);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text != "Company" && lblAmentspaid.Text == "Company")
                {
                    lblLLTotal.Text = ( Rent + Amentis).ToString();
                    TotPayable = (Due);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
            }
            //else if (lblstatus.SelectedValue == "Rent+Maint+Amenit")
            //{
            //    if (lblmaintpaid.Text == "Company" && lblAmentspaid.Text == "Company")
            //    {
            //        lblLLTotal.Text = (Due + Rent + Maint + Amentis).ToString();
            //        TotPayable = (Due);
            //        TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
            //        lblPayable.Text = TotPayable.ToString();
            //        dblTotalAmount += Decimal.Parse(lblPayable.Text);
            //        Session["TotalAmount"] = dblTotalAmount;
            //        TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
            //    }
            //    else if (lblmaintpaid.Text != "Company" && lblAmentspaid.Text != "Company")
            //    {
            //        lblLLTotal.Text = (Due + Rent).ToString();
            //        TotPayable = (Due);
            //        TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
            //        lblPayable.Text = TotPayable.ToString();
            //        dblTotalAmount += Decimal.Parse(lblPayable.Text);
            //        Session["TotalAmount"] = dblTotalAmount;
            //        TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
            //    }
            //    else if (lblmaintpaid.Text == "Company" && lblAmentspaid.Text != "Company")
            //    {
            //        lblLLTotal.Text = (Due + Rent + Maint).ToString();
            //        TotPayable = (Due);
            //        TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
            //        lblPayable.Text = TotPayable.ToString();
            //        dblTotalAmount += Decimal.Parse(lblPayable.Text);
            //        Session["TotalAmount"] = dblTotalAmount;
            //        TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
            //    }
            //    else if (lblmaintpaid.Text != "Company" && lblAmentspaid.Text == "Company")
            //    {
            //        lblLLTotal.Text = (Due + Rent + Amentis).ToString();
            //        TotPayable = (Due);
            //        TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
            //        lblPayable.Text = TotPayable.ToString();
            //        dblTotalAmount += Decimal.Parse(lblPayable.Text);
            //        Session["TotalAmount"] = dblTotalAmount;
            //        TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
            //    }
            //}
            else if(lblstatus.SelectedValue == "0" || lblstatus.SelectedValue == "Hold" )
            {
                if (lblmaintpaid.Text == "Company" && lblAmentspaid.Text == "Company")
                {
                    lblLLTotal.Text = (Due + Rent + Maint + Amentis).ToString();
                    TotPayable = (Due + Rent + Maint + Amentis);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text != "Company" && lblAmentspaid.Text != "Company")
                {
                    lblLLTotal.Text = (Due + Rent).ToString();
                    TotPayable = (Due + Rent);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text == "Company" && lblAmentspaid.Text != "Company")
                {
                    lblLLTotal.Text = (Due + Rent + Maint).ToString();
                    TotPayable = (Due + Rent + Maint);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text != "Company" && lblAmentspaid.Text == "Company")
                {
                    lblLLTotal.Text = (Due + Rent + Amentis).ToString();
                    TotPayable = (Due + Rent + Amentis);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
            }
            else if (lblstatus.SelectedValue == "Rent")
            {
                lblstatus.Enabled = false;
                lblstatus.SelectedValue = "Maint+Amenit";
                if (lblmaintpaid.Text == "Company" && lblAmentspaid.Text == "Company")
                {
                    lblLLTotal.Text = (Due  + Maint + Amentis).ToString();
                    TotPayable = (Due + Maint + Amentis);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text != "Company" && lblAmentspaid.Text != "Company")
                {
                    lblLLTotal.Text = (Due ).ToString();
                    TotPayable = (Due);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text == "Company" && lblAmentspaid.Text != "Company")
                {
                    lblLLTotal.Text = (Due  + Maint).ToString();
                    TotPayable = (Due  + Maint);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text != "Company" && lblAmentspaid.Text == "Company")
                {
                    lblLLTotal.Text = (Due  + Amentis).ToString();
                    TotPayable = (Due  + Amentis);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
            }
            else if (lblstatus.SelectedValue == "Maintainance")
            {
                lblstatus.Enabled = false;
                lblstatus.SelectedValue = "Rent+Ameni";
                if (lblAmentspaid.Text == "Company")
                {
                    lblLLTotal.Text = (Due + Rent + Amentis).ToString();
                    TotPayable = (Due + Rent + Amentis);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblAmentspaid.Text != "Company")
                {
                    lblLLTotal.Text = (Due + Rent).ToString();
                    TotPayable = (Due + Rent);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
            }
            else if (lblstatus.SelectedValue == "Amenities")
            {
                lblstatus.Enabled = false;
                lblstatus.SelectedValue = "Rent+Maint";
                if (lblmaintpaid.Text == "Company")
                {
                    lblLLTotal.Text = (Due + Rent + Maint).ToString();
                    TotPayable = (Due + Rent + Maint);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else if (lblmaintpaid.Text != "Company")
                {
                    lblLLTotal.Text = (Due + Rent).ToString();
                    TotPayable = (Due + Rent);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
            }
            else if (lblstatus.SelectedValue == "Rent+Maint")
            {
                lblstatus.Enabled = false;
                lblstatus.SelectedValue = "Amenities";
                if (lblAmentspaid.Text == "Company")
                {
                    lblLLTotal.Text = (Due + Amentis).ToString();
                    TotPayable = (Due + Amentis);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
                else 
                {
                    lblLLTotal.Text = (Due ).ToString();
                    TotPayable = (Due );
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
                }
            }
            else if (lblstatus.SelectedValue == "Maint+Amenit")
            {
                lblstatus.Enabled = false;
                lblstatus.SelectedValue = "Rent";
                lblLLTotal.Text = (Due + Rent).ToString();
                    TotPayable = (Due + Rent);
                    TotPayable = TotPayable + ((TotPayable * Servicetax) / 100);
                    lblPayable.Text = TotPayable.ToString();
                    dblTotalAmount += Decimal.Parse(lblPayable.Text);
                    Session["TotalAmount"] = dblTotalAmount;
                    TotalRent = TotalRent + Decimal.Parse(lblPayable.Text);
            }
          
        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {

            Label lblTotalPrice = (Label)e.Row.FindControl("lblTotalRent");
            lblTotalPrice.Text = "0";
            lblTotalPrice.Text = Session["TotalAmount"].ToString();
        }
    }

    //public object ValidateRent()
    //{
    //    //int ValidateCode = 0;
    //    //string LeaseId = "";
    //    //string requisition_Sno = "";

    //    //foreach (GridViewRow row in gvLandLord.Rows)
    //    //{
    //    //    if (row.RowType == DataControlRowType.DataRow)
    //    //    {
    //    //        Label SNO = (Label)row.FindControl("llId");
    //    //        Label lblreqid = (Label)row.FindControl("lblLeaseId");
    //    //        Label requisition = (Label)row.FindControl("requisition_Sno");
    //    //        Label lblLLmonth = (Label)row.FindControl("lblmonth");
    //    //        LeaseId = lblreqid.Text;
    //    //        requisition_Sno = requisition.Text;
    //    //    }
    //    //}
    //    SubSonic.StoredProcedure sp1 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_VALIDATE_LEASE_RENT");
    //    sp1.Command.AddParameter("@PAY_TERM", txtPaymentTerm.Text, DbType.String);
    //    sp1.Command.AddParameter("@LEASE_ID", txtLeaseId.Text, DbType.String);
    //    sp1.Command.AddParameter("@REQ_SNO", llsno.Text, DbType.String);
    //    sp1.Command.AddParameter("@PAY_MONTH", String.Join(",", ddlMonths.Items.OfType<ListItem>().Where(r => r.Selected).Select(r => r.Value)) + ",", DbType.String);
    //    sp1.Command.AddParameter("@PAY_YEAR", ddlYears.SelectedValue, DbType.String);
    //    sp1.Command.AddParameter("@SNO", ddlYears.SelectedValue, DbType.String);
    //    DataSet da = new DataSet();
    //    da = sp1.GetDataSet();
    //    return da;

    //}

    protected void ddlGST_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtRemarks.Focus();
        if (ddlGST.SelectedValue == "Yes")
        {
            divgst.Visible = true;
        }
        else
        {
            divgst.Visible = false;
        }

    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        BindLandlords(txtLeaseId.Text, ddlMonths.SelectedValue, ddlYears.SelectedValue);
        //updatefooter();
    }
    protected void ddlYears_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblMsg.Text = "";
        BindLandlords(txtLeaseId.Text, ddlMonths.SelectedValue, ddlYears.SelectedValue);
        //updatefooter();
    }
    protected void btndownload_click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_BIND_LANDLORDS_PAYMENT_Details");
        sp2.Command.AddParameter("@AUR_ID", Session["UID"].ToString());
        sp2.Command.AddParameter("@fromdate", Convert.ToDateTime(FROM_DATE.Text));
        sp2.Command.AddParameter("@todate", Convert.ToDateTime(TO_DATE.Text));
        //sp2.Command.AddParameter("@Rent", (renttype.SelectedValue));
        sp2.Command.AddParameter("@COMPANYID", ddlcompany.SelectedValue.ToString());
        DataSet ds = sp2.GetDataSet();
        DataTable dt = ds.Tables[0];
        using (XLWorkbook wb = new XLWorkbook())
        {
            wb.Worksheets.Add(dt, "Customers");

            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=RentalPayment.xlsx");
            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                wb.SaveAs(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.Flush();
                Response.End();
            }
        }
    }
    //    protected void btndownload_click(object sender, EventArgs e)
    //    {
    //    try
    //    {
    //        Export export = new Export();
    //        // export.Export("InventoryCategoryMaster.xls", gvitems)
    //        SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_BIND_LANDLORDS_PAYMENT_Details");
    //        sp2.Command.AddParameter("@AUR_ID", Session["UID"].ToString());
    //        sp2.Command.AddParameter("@fromdate", Convert.ToDateTime(FROM_DATE.Text));
    //        sp2.Command.AddParameter("@todate", Convert.ToDateTime(TO_DATE.Text));
    //        //sp2.Command.AddParameter("@Rent", (renttype.SelectedValue));
    //        sp2.Command.AddParameter("@COMPANYID", ddlcompany.SelectedValue.ToString());
    //        DataSet ds = sp2.GetDataSet();
    //        DataTable table = ds.Tables[0];

    //        HttpContext.Current.Response.Clear();
    //        HttpContext.Current.Response.ClearContent();
    //        HttpContext.Current.Response.ClearHeaders();
    //        HttpContext.Current.Response.Buffer = true;
    //        HttpContext.Current.Response.ContentType = "application/ms-excel";
    //        HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
    //        HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + "RentalPayment.xls" + "");
    //        HttpContext.Current.Response.Charset = "utf-8";
    //        HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
    //        HttpContext.Current.Response.Write("<TABLE border='1' bgColor='#ffffff' " +
    //          "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
    //          "style='font-size:10.0pt;background:white;'><TR>");
    //        int columnscount = table.Columns.Count;
    //        for (int j = 0; j < columnscount; j++)
    //        {
    //            HttpContext.Current.Response.Write(@"<TD><B>" + table.Columns[j].ToString() + "</B></TD>");
    //        }
    //        HttpContext.Current.Response.Write("</TR>");
    //        foreach (DataRow row in table.Rows)
    //        {
    //            HttpContext.Current.Response.Write("<TR>");
    //            for (int i = 0; i < table.Columns.Count; i++)
    //            {
    //                if (table.Columns[i].ToString() == "Agreement_Start_Date" || table.Columns[i].ToString() == "Agreement_End_Date")
    //                {

    //                    String sDate = row[i].ToString();
    //                    DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));
    //                    String dy = datevalue.Day.ToString();
    //                    String mn = datevalue.Month.ToString();
    //                    String yy = datevalue.Year.ToString();
    //                    DateTime d = Convert.ToDateTime(mn + '/' + dy + '/' + yy);

    //                    HttpContext.Current.Response.Write("<TD>");
    //                    HttpContext.Current.Response.Write(d.ToString("dd/MM/yyyy"));
    //                    HttpContext.Current.Response.Write("</TD>");

    //                }
    //                else if (table.Columns[i].ToString() == "Account_no")
    //                {
    //                    var input = row[i].ToString();


    //                    // Displays the same value with four decimal digits.

    //                    HttpContext.Current.Response.Write("<TD>");
    //                    HttpContext.Current.Response.Write("'" + input);
    //                    HttpContext.Current.Response.Write("</TD>");


    //                }

    //                //}
    //                else
    //                {

    //                    HttpContext.Current.Response.Write("<TD>");
    //                    HttpContext.Current.Response.Write(row[i].ToString());
    //                    HttpContext.Current.Response.Write("</TD>");
    //                }

    //                //if (row[i].ToString().Trim().Length > 1 && row[i].ToString().Trim().StartsWith("0") && row[i].GetType().Name == "String")
    //                //    HttpContext.Current.Response.Write("<TD style=\"mso-number-format:\\@\">");
    //                //else
    //                //    HttpContext.Current.Response.Write("<TD>");
    //                //HttpContext.Current.Response.Write(String.Format("{0:0.00}", row[i].ToString()));
    //                //HttpContext.Current.Response.Write("</TD>");
    //            }
    //            HttpContext.Current.Response.Write("</TR>");
    //        }
    //        HttpContext.Current.Response.Write("</TABLE>");
    //        HttpContext.Current.Response.Flush();
    //        HttpContext.Current.Response.SuppressContent = true;
    //        HttpContext.Current.ApplicationInstance.CompleteRequest();

    //    }
    //    catch (Exception ex)
    //    {

    //        Response.Write(ex.Message);
    //    }
    //}



}


