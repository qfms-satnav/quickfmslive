Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.Drawing

Partial Class WorkSpace_SMS_Webfiles_frmApprovalLease
    Inherits System.Web.UI.Page
    Shared refund As Integer = 0
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnsubmit)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        'If Session("UID") = "" Then
        '    Response.Redirect(Application("FMGLogout"))
        'End If
        If Not IsPostBack Then
            RegExpRem.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
            RegExpName15.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
            BindGrid()
            If gvItems.Rows.Count > 0 Then
                lblMessage.Visible = True
            Else
                lblMessage.Visible = False
            End If
            'LeaseSurrenderAmt()
            pnl1.Visible = False
            txtsurrender.Text = Date.Today.ToString("MM/dd/yyyy")
        End If
        txtsurrender.Attributes.Add("readonly", "readonly")
        txtrefund.Attributes.Add("readonly", "readonly")
    End Sub
    Private Sub LeaseSurrenderAmt()
        txtAmtPaidLesse.Text = 0
        txtAmtPaidLessor.Text = 0
        If CInt(txtSDamount.Text) > (CInt(IIf(txtDmgAmt.Text = "", 0, txtDmgAmt.Text)) + CInt(IIf(txtOutAmt.Text = "", 0, txtOutAmt.Text))) Then
            txtAmtPaidLesse.Text = CInt(txtSDamount.Text) - (CInt(IIf(txtDmgAmt.Text = "", 0, txtDmgAmt.Text)) + CInt(IIf(txtOutAmt.Text = "", 0, txtOutAmt.Text)))

        Else
            txtAmtPaidLessor.Text = (CInt(IIf(txtDmgAmt.Text = "", 0, txtDmgAmt.Text)) + CInt(IIf(txtOutAmt.Text = "", 0, txtOutAmt.Text))) - CInt(txtSDamount.Text)

        End If

    End Sub
    Private Sub Clear()
        txtOutAmt.Text = ""
        txtAmtPaidLessor.Text = ""
        txtAmtPaidLesse.Text = ""
    End Sub
    Private Sub BindGrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_SURRENDER_LEASE_GRID")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.Int32)
        Dim ds As DataSet
        ds = sp.GetDataSet()
        ViewState("reqDetails") = ds
        gvItems.DataSource = ViewState("reqDetails")
        gvItems.DataBind()

    End Sub
    Protected Sub txtOutAmt_TextChanged(sender As Object, e As EventArgs) Handles txtOutAmt.TextChanged

        txtAmtPaidLesse.Text = 0
        txtAmtPaidLessor.Text = 0
        LeaseSurrenderAmt()

    End Sub
    Protected Sub gvItems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvItems.PageIndexChanging
        gvItems.PageIndex = e.NewPageIndex()
        gvItems.DataSource = ViewState("reqDetails")
        gvItems.DataBind()
    End Sub

    Protected Sub gvItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvItems.RowCommand
        If e.CommandName = "Surrender" Then

            Dim gvr As GridViewRow = CType(((CType(e.CommandSource, LinkButton)).NamingContainer), GridViewRow)
            Dim RowIndex As Integer = gvr.RowIndex
            For Each row As GridViewRow In gvItems.Rows
                If row.RowIndex = RowIndex Then
                    gvItems.Rows(RowIndex).BackColor = System.Drawing.Color.SkyBlue
                Else
                    gvItems.Rows(row.RowIndex).BackColor = Color.Empty
                End If
            Next

            Dim lnkSurrender As LinkButton = DirectCast(e.CommandSource, LinkButton)
            Dim gvRow As GridViewRow = DirectCast(lnkSurrender.NamingContainer, GridViewRow)
            Dim lblLseName As LinkButton = DirectCast(gvRow.FindControl("lblLseName"), LinkButton)
            Dim LseName As Label = DirectCast(gvRow.FindControl("lblLeaseName"), Label)
            txtstore.Text = lblLseName.Text
            txtsno.Text = LseName.Text

            Dim sp4 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SURRENDER_LEASE_DETAILS")
            sp4.Command.AddParameter("@REQ_ID", txtsno.Text, DbType.String)
            Dim ds As New DataSet
            ds = sp4.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtSDamount.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_LES_SEC_DEPOSIT"), 2, MidpointRounding.AwayFromZero)
                txtOutAmt.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_LES_TOT_RENT"), 2, MidpointRounding.AwayFromZero)
                txttotamount.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("PM_LES_TOT_RENT"), 2, MidpointRounding.AwayFromZero)
            End If
            LeaseSurrenderAmt()
            pnl1.Visible = True
        Else
            pnl1.Visible = False
        End If
    End Sub
    Public Class ImageClas
        Private _fn As String
        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String
        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class


    Private Sub InsertSurrenderDetails()

        Dim orgfilename As String = BrowsePossLtr.FileName
        Dim filename As String = ""
        lblMsg.Text = ""
        'Try
        '    If (BrowsePossLtr.HasFile) Then
        '        Dim fileExt As String
        '        fileExt = System.IO.Path.GetExtension(BrowsePossLtr.FileName)
        '        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & orgfilename
        '        BrowsePossLtr.PostedFile.SaveAs(filePath)
        '        filename = orgfilename
        '        'lblMsg.Visible = True
        '        'lblMsg.Text = "File upload successfully"

        '    End If
        'Catch ex As Exception
        '    Response.Write(ex.Message)
        'End Try
        Dim SurrenderList As List(Of ImageClas) = New List(Of ImageClas)
        Dim IC As ImageClas
        Dim i As Int32 = 0
        Dim fileSize As Int64 = 0
        If BrowsePossLtr.PostedFiles IsNot Nothing Then
            Dim count = BrowsePossLtr.PostedFiles.Count
            While (i < count)
                fileSize = BrowsePossLtr.PostedFiles(i).ContentLength + fileSize
                i = i + 1
            End While
            If (fileSize > 20971520) Then
                lblMsg.Text = "The maximum size for upload files was 20 MB."
                Exit Sub
            End If
            For Each File In BrowsePossLtr.PostedFiles
                If File.ContentLength > 0 Then
                    Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                    Dim filePath As String = Request.PhysicalApplicationPath.ToString & "UploadFiles\" & Upload_Time & "_" & File.FileName
                    File.SaveAs(filePath)
                    IC = New ImageClas()
                    IC.Filename = File.FileName
                    IC.FilePath = Upload_Time & "_" & File.FileName
                    SurrenderList.Add(IC)

                End If
            Next
        End If

            Try

            Dim param As SqlParameter() = New SqlParameter(13) {}

            'General Details
            param(0) = New SqlParameter("@LEASEID", DbType.String)
            param(0).Value = txtstore.Text
            param(1) = New SqlParameter("@SURRENDER_DATE", DbType.Date)
            param(1).Value = txtsurrender.Text
            param(2) = New SqlParameter("@REFUND_DT", DbType.Date)
            param(2).Value = txtrefund.Text
            param(3) = New SqlParameter("@SECU_DEPO", DbType.Decimal)
            param(3).Value = txtSDamount.Text
            param(4) = New SqlParameter("@DAMAGES", DbType.String)
            param(4).Value = txtDmg.Text
            param(5) = New SqlParameter("@DAMAGE_AMT", DbType.Decimal)
            param(5).Value = txtDmgAmt.Text
            param(6) = New SqlParameter("@LESSE_AMT", DbType.Decimal)
            param(6).Value = txtAmtPaidLesse.Text

            param(7) = New SqlParameter("@LESSOR_AMT", DbType.Decimal)
            'param(7).Value = ddlFloor.SelectedValue
            param(7).Value = txtAmtPaidLessor.Text
            param(8) = New SqlParameter("@OUTSTAND_AMT", DbType.Decimal)
            param(8).Value = txtOutAmt.Text
            param(9) = New SqlParameter("@REMARKS", DbType.String)
            param(9).Value = txtremarks.Text
            param(10) = New SqlParameter("@POSSESS_LETTER_UPLOAD", SqlDbType.Structured)
            param(10).Value = UtilityService.ConvertToDataTable(SurrenderList)
            ''param(10).Value = filename
            param(11) = New SqlParameter("@AUR_ID", DbType.String)
            'param(7).Value = ddlFloor.SelectedValue
            param(11).Value = Session("UID")
            param(12) = New SqlParameter("@COMPANYID", DbType.Int32)
            param(12).Value = Session("COMPANYID")
            param(13) = New SqlParameter("@SNO", DbType.Int32)
            param(13).Value = txtsno.Text

            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_INSERT_LEASE_SURRENDER_DETAILS", param)
            lblMsg.Text = "Lease Details Successfully Surrendered  " + txtstore.Text
            pnl1.Visible = False
            BindGrid()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        ''04012021''
        'If (Convert.ToDecimal(txtSDamount.Text) = 0) Then
        '    'lblMsg.Text = "the refund amount should be less or equal to" + refund.ToString() + " and greater than 0"
        '    lblMsg.Text = "The Security Deposit Should Be Greater Than 0"
        '    Exit Sub
        'Else
        '    InsertSurrenderDetails()
        'End If
        ''Response.Redirect("~/WorkSpace/SMS_Webfiles/frmThanks.aspx?id=47")
        InsertSurrenderDetails()
        lblMsg.Visible = True




    End Sub

    Protected Sub txtDmgAmt_TextChanged(sender As Object, e As EventArgs) Handles txtDmgAmt.TextChanged

        txtAmtPaidLesse.Text = 0
        txtAmtPaidLessor.Text = 0
        LeaseSurrenderAmt()

    End Sub

    Public Sub fillgridOnTenantCodeSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_FILTER_DETAILS")
            sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            sp2.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            sp2.Command.AddParameter("@SEARCH", txtReqId.Text, DbType.String)
            Dim ds As DataSet
            ds = sp2.GetDataSet()
            ViewState("reqDetails") = ds
            gvItems.DataSource = ViewState("reqDetails")
            gvItems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        Try
            fillgridOnTenantCodeSearch()
            pnl1.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        txtReqId.Text = ""
        BindGrid()
        pnl1.Visible = False
    End Sub
End Class
