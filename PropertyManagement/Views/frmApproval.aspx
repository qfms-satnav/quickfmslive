<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmApproval.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmApproval" Title="Approval" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
<%--    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
       <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
   
   <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />--%>



    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)

            var theForm = document.forms['aspnetForm'];

            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name))
                        elm.checked = checkVal
                }
            }
        }

        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('.date').datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
           <%-- <div class="widgets">
                <div ba-panel ba-panel-title="Surrender Approve/Reject" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Surrender Approve/Reject</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>Surrender Approve/Reject</h3>
            </div>
                           <div class="card">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" ForeColor="Red" />

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search By Any..."></asp:TextBox>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvItems" runat="server" AutoGenerateColumns="False" CssClass="table GridStyle" GridLines="none"
                                            AllowPaging="True" PageSize="5" EmptyDataText="No Surrender Request Found.">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Select">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false" HeaderText="SNO">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsno" runat="server" Text='<%#Eval("SNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField Visible="false" HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblUniqueNo" runat="server" Text='<%#Eval("SNO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpropertyname" runat="server" Text='<%#Eval("PN_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltenantcode" runat="server" Text='<%#Eval("TEN_CODE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbltenant" runat="server" Text='<%#Eval("TEN_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant Email">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblemail" runat="server" Text='<%#Eval("TEN_EMAIL") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="View Details">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkApprove" runat="server" Text="View Details" CommandArgument="<%# Container.DataItemIndex %>" CommandName="Approve"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                        <asp:HiddenField ID="hdnPN" runat="server" />
                                        <asp:HiddenField ID="hdnSNO" runat="server" />
                                    </div>
                                </div>
                                <br />
                                <div id="panel1" runat="server">

                                    <div class="row">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Tenant Code</label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Tenant Name<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtname" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Property Type<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlproptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">City<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                        AutoPostBack="True" Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top:10px">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Location<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                        AutoPostBack="True" Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Property<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                                        <asp:ListItem>--Select Property--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Tenant Occupied Area<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtTenantOccupiedArea" runat="server" CssClass="form-control"
                                                        MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Tenant Rent<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtRent" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top:10px">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Joining Date</label>
                                                <div class="col-sm-12">
                                                    <div class='input-group date' id='fromdate'>
                                                        <asp:TextBox ID="txtDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Next Payable Date<span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="txtPayableDate"
                                                    Display="None" ErrorMessage="Please Select Next Payable Date" ValidationGroup="Val1"
                                                    InitialValue=""></asp:RequiredFieldValidator>
                                                <div class="col-sm-12">
                                                    <div class='input-group date' id='todate'>
                                                        <asp:TextBox ID="txtPayableDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                        <span class="input-group-addon">
                                                            <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Security Deposit</label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Payment Terms<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddlPaymentTerms" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                                        <asp:ListItem>--Payment Terms--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top:10px">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Parking Spaces<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Society Maintenance / CAM<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtfees" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Outstanding amount<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="5" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Tenant<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:DropDownList ID="ddluser" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="padding-top:10px">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Surrender Date<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <div>
                                                        <asp:TextBox ID="txtSurrenderDt" runat="server" CssClass="form-control" MaxLength="10" Enabled="false"> </asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label" runat="server" id="Label1">Total Rent Amount</label>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtTotalRent" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label" runat="server" id="Label2">Rent Amount Paid</label>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtRentPaid" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label" runat="server" id="lblClosingBalMsg">Remaining Outstanding Amount</label>
                                                <div class="col-md-12">
                                                    <asp:TextBox ID="txtTenClosingBal" ReadOnly="true" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row" style="padding-top:10px">
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Remarks<span style="color: red;">*</span></label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" MaxLength="500" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <label class="col-sm-12 control-label">Approval Remarks</label>
                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtApprRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" MaxLength="500"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="padding-top:10px">
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <strong>Documents</strong>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-3 col-xs-12">
                                            <div class="form-group">
                                                <div id="tblGridDocs" runat="server">
                                                    <asp:DataGrid ID="grdDocs" runat="server" CssClass="table GridStyle" GridLines="none" DataKeyField="PM_TS_DOC_SNO"
                                                        EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                        <Columns>
                                                            <asp:BoundColumn Visible="False" DataField="PM_TS_DOC_SNO" HeaderText="ID"></asp:BoundColumn>
                                                            <asp:BoundColumn DataField="PM_TS_DOC_PATH" HeaderText="Document Name">
                                                                <HeaderStyle></HeaderStyle>
                                                            </asp:BoundColumn>
                                                            <asp:BoundColumn DataField="PM_TS_DOC_CREATED_DT" HeaderText="Document Date">
                                                                <HeaderStyle></HeaderStyle>
                                                            </asp:BoundColumn>
                                                            <asp:ButtonColumn Text="Download" CommandName="Download">
                                                                <HeaderStyle></HeaderStyle>
                                                            </asp:ButtonColumn>
                                                            <asp:ButtonColumn Text="Delete" CommandName="Delete">
                                                                <HeaderStyle></HeaderStyle>
                                                            </asp:ButtonColumn>
                                                        </Columns>
                                                        <HeaderStyle ForeColor="white" BackColor="Black" />
                                                        <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                    </asp:DataGrid>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Approve" ValidationGroup="Val1" />&nbsp;
                                        <asp:Button ID="btnReject" CssClass="btn btn-primary custom-button-color" runat="server" Text="Reject" />&nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
          <%--  </div>
        </div>--%>
    <%--</div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#Approval1_txtfees').keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });
            $('#Approval1_txtamount').keyup(function () {
                this.value = this.value.replace(/[^0-9\.]/g, '');
            });
        });

        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "frmTenantDetails.aspx?tenant=" + id);
            $("#myModal").modal('show');
        }
    </script>
</body>
</html>

