Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports clsSubSonicCommonFunctions
Imports SqlHelper
Imports System.Drawing

Partial Class WorkSpace_SMS_Webfiles_frmRenewalLease
    Inherits System.Web.UI.Page


    Dim ObjSubSonic As New clsSubSonicCommonFunctions
    Dim param() As SqlParameter
    Dim ds As New DataSet
    Dim RentRevision As DataTable

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnSubmit)
            scriptManager.RegisterPostBackControl(grdDocs)
            scriptManager.RegisterPostBackControl(lldocgrid)
        End If
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Session("UID") = "" Then
            Response.Redirect(Application("FMGLogout"))
        End If
        If Not IsPostBack Then
            Landlord.Visible = False
            btnAddNewLandlord.Visible = False
            btnApproveLease.Visible = False
            NoLanlords.Visible = False
            BindGrid()
            BindProperty()
            BindCity()
            BindPropType()
            BindStatus()
            BindPayMode()
            BindEntity()
            BindRejected()
            BindFlooringTypes()
            BindTenure()
            panel1.Visible = False
            panel2.Visible = False
            txtsdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
            txtedate.Text = DateTime.Now.AddYears(1).ToString("MM/dd/yyyy")
        End If
    End Sub

    Private Sub BindGrid()
        Try
            'lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASES_FOR_RENEWAL")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            ds = sp.GetDataSet()
            ViewState("reqDetails") = ds
            gvitems.DataSource = ViewState("reqDetails")
            gvitems.DataBind()

        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindRejected()
        Try
            'lblMsg.Text = ""
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASES_FOR_RENEWAL_REJECTED")
            sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            sp.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            ds = sp.GetDataSet()
            Dim rejecteddetails As DataTable
            rejecteddetails = ds.Tables(0)
            If (rejecteddetails.Rows.Count > 0) Then
                ViewState("rjectDetails") = ds
                gvrejected.DataSource = ViewState("rjectDetails")
                gvrejected.DataBind()
                rejected.Visible = True
            End If
        Catch ex As Exception
            'Response.Write(ex.Message)
        End Try
    End Sub
    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Public Sub fillgridOnTenantCodeSearch()
        Try
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_LEASE_RENEWAL_FILTER_DETAILS")
            sp2.Command.AddParameter("@AUR_ID", Session("Uid"), DbType.String)
            sp2.Command.AddParameter("@COMPANYID", Session("COMPANYID"), DbType.String)
            sp2.Command.AddParameter("@SEARCH", txtReqId.Text, DbType.String)
            ds = sp2.GetDataSet()
            ViewState("reqDetails") = ds
            gvitems.DataSource = ViewState("reqDetails")
            gvitems.DataBind()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnsearch.Click
        Try
            fillgridOnTenantCodeSearch()
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub txtreset_Click(sender As Object, e As EventArgs) Handles txtreset.Click
        BindGrid()
        txtReqId.Text = ""
        AddLeaseDetails.Visible = False
    End Sub


    Protected Sub gvitems_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvitems.RowCommand
        Try
            lblMsg.Text = String.Empty
            If e.CommandName = "GetLeases" Then
                Dim gvr As GridViewRow = CType(((CType(e.CommandSource, LinkButton)).NamingContainer), GridViewRow)
                Dim RowIndex1 As Integer = gvr.RowIndex
                For Each row1 As GridViewRow In gvitems.Rows
                    If row1.RowIndex = RowIndex1 Then
                        gvitems.Rows(RowIndex1).BackColor = System.Drawing.Color.SkyBlue
                    Else
                        gvitems.Rows(row1.RowIndex).BackColor = Color.Empty
                    End If
                Next

                panel1.Visible = True
                hdnLSNO.Value = e.CommandArgument
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = row.RowIndex
                Dim ReqID As String = DirectCast(row.FindControl("lnkReqId"), LinkButton).Text.ToString()
                Session("REQ_ID") = ReqID

                lblLeaseReqId.Text = ReqID
                '  btnSubmit.Text = "Add more properties to " + hdnLSNO.Value
                GetSelectedLeaseDetails(hdnLSNO.Value)
                'txtsdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
                'txtedate.Text = DateTime.Now.AddYears(1).ToString("MM/dd/yyyy")
                'BindRentRevision()
                BindDocuments()
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GetSelectedLeaseDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GRID_VIEW_MODIFY")
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        'gvlandlordItems.DataSource = ds.Tables(1)
        'gvlandlordItems.DataBind()
        'btnAddNewLandlord.Enabled = False
        SetLeaseValuesToUpdate(ds)
        updatepanel.Visible = True
    End Sub
    Private Sub BindEntity()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ENTITY_TYPES")
        ddlentity.DataSource = sp3.GetDataSet()
        ddlentity.DataTextField = "CHE_NAME"
        ddlentity.DataValueField = "CHE_CODE"
        ddlentity.DataBind()
        ddlentity.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub SetLeaseValuesToUpdate(ByVal leaseAllSections As DataSet)
        Dim LeaseNAreaNDetails As DataTable
        Dim charges As DataTable
        Dim agreementDetails As DataTable
        Dim brokerageDt As DataTable
        Dim UtilityDt As DataTable
        Dim OtherServicesDt As DataTable
        Dim OtherDetailsDt As DataTable
        Dim LeaseDetailsDt As DataTable
        Dim LeaseEscalationDt As DataTable
        Dim LeaseExpDt As DataTable
        Dim POA As DataTable
        Dim AreaDetails As DataTable
        Dim LLScope As DataTable


        LeaseNAreaNDetails = leaseAllSections.Tables(0)
        charges = leaseAllSections.Tables(2)
        agreementDetails = leaseAllSections.Tables(3)
        brokerageDt = leaseAllSections.Tables(4)
        UtilityDt = leaseAllSections.Tables(5)
        OtherServicesDt = leaseAllSections.Tables(6)
        OtherDetailsDt = leaseAllSections.Tables(7)
        LeaseDetailsDt = leaseAllSections.Tables(8)
        LeaseEscalationDt = leaseAllSections.Tables(9)
        LeaseExpDt = leaseAllSections.Tables(10)
        POA = leaseAllSections.Tables(11)
        AreaDetails = leaseAllSections.Tables(12)
        ''RentRevision = leaseAllSections.Tables(11)
        LLScope = leaseAllSections.Tables(13)

        BindLeaseExpences()

        ddlproperty.ClearSelection()
        'Lease Details Section Start
        If LeaseNAreaNDetails.Rows(0)("PM_LES_STA_ID") = 4024 Then
            lblMsg.Text = "Add at least one landlord in order to successfully add a lease."
        Else
            lblMsg.Text = ""
        End If
        Dim leaseproperty As String = LeaseNAreaNDetails.Rows(0)("PM_PPT_SNO")
        ddlproperty.Items.FindByValue(leaseproperty).Selected = True

        param = New SqlParameter(0) {}
        param(0) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar, 200)
        param(0).Value = ddlproperty.SelectedValue
        ds = New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("BIND_DETAILS_BY_PROP", param)
        If ds.Tables(0).Rows.Count > 0 Then
            txtPropAddedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
            txtApprovedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
            ddlentity.ClearSelection()
            ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("CHE_CODE").ToString()).Selected = True
            txtInspection.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_BY").ToString()
            If (ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT").ToString() <> "") Then
                txtInspecteddate.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT")
            End If
            ''txtInspecteddate.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT").ToString()
            'txtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_PPT_CODE").ToString()
        End If
        txtPropCode.Text = LeaseNAreaNDetails.Rows(0)("PM_PPT_CODE")
        txtNoLanlords.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_NO_OF_LL")
        ddlstatus.ClearSelection()
        Dim STATUS As String = LeaseNAreaNDetails.Rows(0)("PM_LES_STATUS")
        If (STATUS <> "") Then
            ddlstatus.Items.FindByValue(STATUS).Selected = True
        End If

        ddlPropertyType.ClearSelection()
        Dim PropertyType As String = LeaseNAreaNDetails.Rows(0)("PM_PPT_TYPE")
        If (PropertyType <> "") Then
            ddlPropertyType.Items.FindByValue(PropertyType).Selected = True
        End If
        'Lease Details Section End

        'Area & Cost Details Section start
        ddlSecurityDepMonths.ClearSelection()
        Dim secrtyDepositMonths As String = LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEP_MONTHS")
        If (secrtyDepositMonths <> "0") Then
            ddlSecurityDepMonths.Items.FindByValue(secrtyDepositMonths).Selected = True
        End If

        ddlTenure.ClearSelection()
        Dim tenure As String = LeaseNAreaNDetails.Rows(0)("PM_LES_TENURE")
        ddlTenure.Items.FindByValue(tenure).Selected = True
        Dim costType As String = LeaseNAreaNDetails.Rows(0)("PM_LES_COST_TYPE")
        If (costType <> "") Then
            rblCostType.Items.FindByValue(costType).Selected = True
        End If

        If String.Equals(costType, "Seat") Then
            Costype1.Visible = False
            Costype2.Visible = True

        Else
            Costype1.Visible = True
            Costype2.Visible = False

        End If

        txtSeatCost.Text = IIf(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST") = 0, 0, Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEAT_COST")))
        txtRentPerSqftCarpet.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_CARPET"))
        txtRentPerSqftBUA.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_PER_SQFT_BUA"))

        txtLnumber.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_CTS_NO")
        txtentitle.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_ENTITLED_AMT"))
        txtInvestedArea.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_BASIC_RENT"))
        txtpay.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_SEC_DEPOSIT"))
        txtRentFreePeriod.Text = LeaseNAreaNDetails.Rows(0)("PM_LES_RENT_FREE_PERIOD")
        txtInteriorCost.Text = Convert.ToDouble(LeaseNAreaNDetails.Rows(0)("PM_LES_INTERIOR_COST"))
        'Area & Cost Details Section end

        'Charges Section start
        txtregcharges.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_REGS_CHARGES"))
        txtsduty.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_STMP_DUTY_CHARGES"))
        txtfurniture.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_FF_CHARGES"))
        txtbrokerage.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_CONSLBROKER_CHARGES"))
        txtpfees.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROF_CHARGES"))
        txtmain1.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_MAINT_CHARGES"))
        ddlmaintpaid.ClearSelection()
        ddlmaintpaid.Items.FindByValue(charges.Rows(0)("PM_LC_MAINT_PAIDBY")).Selected = True
        ddlamenpaid.ClearSelection()
        ddlamenpaid.Items.FindByValue(charges.Rows(0)("PM_LC_AMENT_PAIDBY")).Selected = True
        txtservicetax.ClearSelection()
        Dim GSTTax As String = charges.Rows(0)("PM_LC_GST").ToString()
        If (GSTTax <> "") Then
            txtservicetax.Items.FindByValue(GSTTax).Selected = True
        End If
        txtproptax.Text = Convert.ToDouble(charges.Rows(0)("PM_LC_PROPERTY_TAX"))
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtfurniture.Text = "", 0, txtfurniture.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
        'Charges Section end

        'Agreement Details Section start
        txtsdate.Text = agreementDetails.Rows(0)("PM_LAD_EFFE_DT_AGREEMENT")
        txtedate.Text = agreementDetails.Rows(0)("PM_LAD_EXP_DT_AGREEMENT")

        If (agreementDetails.Rows(0)("PM_LAD_RENT_DATE").ToString() <> "") Then
            txtrentcommencementdate.Text = agreementDetails.Rows(0)("PM_LAD_RENT_DATE")
        End If

        If (agreementDetails.Rows(0)("PM_LAD_START_DT_AGREEMENT").ToString() <> "") Then
            txtstartagreDT.Text = agreementDetails.Rows(0)("PM_LAD_START_DT_AGREEMENT")
        End If

        If (agreementDetails.Rows(0)("PM_LAD_LOCK_INPEDATE").ToString() <> "") Then
            Txtlockinpeamonth.Text = agreementDetails.Rows(0)("PM_LAD_LOCK_INPEDATE")
        End If

        txtlock.Text = agreementDetails.Rows(0)("PM_LAD_LOCK_INPERIOD")
        txtLeasePeiodinYears.Text = agreementDetails.Rows(0)("PM_LAD_LEASE_PERIOD")
        txtNotiePeriod.Text = agreementDetails.Rows(0)("PM_LAD_NOTICE_PERIOD")
        ddlAgreementbyPOA.ClearSelection()
        Dim AgreementbyPOA As String = agreementDetails.Rows(0)("PM_LES_SIGNED_POA")
        'ddlAgreementbyPOA.Items.FindByValue(AgreementbyPOA).Selected = True
        If (AgreementbyPOA <> "") Then
            ddlAgreementbyPOA.Items.FindByValue(AgreementbyPOA).Selected = True
        End If
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
            txtPOAName.Text = POA.Rows(0)("PM_POA_NAME")
            txtPOAAddress.Text = POA.Rows(0)("PM_POA_ADDRESS")
            txtPOAMobile.Text = POA.Rows(0)("PM_POA_PHONE_NO")
            txtPOAEmail.Text = POA.Rows(0)("PM_POA_EMAIL")
            txtLLtype.Text = POA.Rows(0)("PM_POA_LL_TYPE")
        Else
            panPOA.Visible = False
        End If

        'Agreement Details Section end
        ''BindRentRevision()

        'Brokerage Section start
        txtbrkamount.Text = Convert.ToDouble(brokerageDt.Rows(0)("PM_LBD_PAID_AMOUNT"))
        txtbrkname.Text = brokerageDt.Rows(0)("PM_LBD_NAME")
        txtbrkpan.Text = brokerageDt.Rows(0)("PM_LBD_PAN_NO")
        txtbrkmob.Text = brokerageDt.Rows(0)("PM_LBD_PHONE_NO")
        txtbrkremail.Text = brokerageDt.Rows(0)("PM_LBD_EMAIL")
        txtbrkaddr.Text = brokerageDt.Rows(0)("PM_LBD_ADDRESS")
        'Brokerage Section end

        'landlord scope start
        If (LLScope.Rows.Count > 0) Then
            txtvitrified.Text = LLScope.Rows(0)("PM_LLS_VITRIFIED")
            txtvitriremarks.Text = LLScope.Rows(0)("PM_LLS_VITRIFIED_RMKS")
            txtwashroom.Text = LLScope.Rows(0)("PM_LLS_WASHROOMS")
            txtwashroomremarks.Text = LLScope.Rows(0)("PM_LLS_WASH_RMKS")
            txtpantry.Text = LLScope.Rows(0)("PM_LLS_PANTRY")
            txtpantryremarks.Text = LLScope.Rows(0)("PM_LLS_PANTRY_RMKS")
            txtshutter.Text = LLScope.Rows(0)("PM_LLS_SHUTTER")
            txtshutterremarks.Text = LLScope.Rows(0)("PM_LLS_SHUTTER_RMKS")
            txtothers.Text = LLScope.Rows(0)("PM_LLS_OTHERS")
            txtothersremarks.Text = LLScope.Rows(0)("PM_LLS_OTHERS_RMKS")
            txtllwork.Text = LLScope.Rows(0)("PM_LLS_WORK_DAYS")
            txtelectricex.Text = LLScope.Rows(0)("PM_LLS_ELEC_EXISTING")
            txtelectricrq.Text = LLScope.Rows(0)("PM_LLS_ELEC_REQUIRED")
            'landlord scope end
        End If
        'Utility/Power back up Details Section start
        ddlDgSet.ClearSelection()
        Dim dgSetVal As String = UtilityDt.Rows(0)("PM_LUP_DGSET")
        ''ddlDgSet.Items.FindByValue(dgSetVal).Selected = True
        If (dgSetVal <> "") Then
            ddlDgSet.Items.FindByValue(dgSetVal).Selected = True
        End If

        If ddlDgSet.SelectedValue = "Landlord" Then
            Dgset.Visible = True
        Else
            Dgset.Visible = False
        End If

        txtDgSetPerUnit.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_COMMER_PER_UNIT")
        txtDgSetLocation.Text = UtilityDt.Rows(0)("PM_LUP_DGSET_LOCATION")
        txtSpaceServoStab.Text = UtilityDt.Rows(0)("PM_LUP_SPACE_SERVO_STABILIZER")

        ddlElectricalMeter.ClearSelection()
        Dim eleMeter As String = UtilityDt.Rows(0)("PM_LUP_ELE_METER")
        ''ddlElectricalMeter.Items.FindByValue(eleMeter).Selected = True
        If (eleMeter <> "") Then
            ddlElectricalMeter.Items.FindByValue(eleMeter).Selected = True
        End If

        If ddlElectricalMeter.SelectedValue = "Yes" Then
            Meter.Visible = True
            txtMeterLocation.Text = ""
        Else
            Meter.Visible = False
            txtMeterLocation.Text = ""
        End If

        txtMeterLocation.Text = UtilityDt.Rows(0)("PM_LUP_MET_LOCATION")
        txtEarthingPit.Text = UtilityDt.Rows(0)("PM_LUP_EARTHING_PIT")
        txtAvailablePower.Text = UtilityDt.Rows(0)("PM_LUP_AVAIL_POWER")
        txtAdditionalPowerKWA.Text = UtilityDt.Rows(0)("PM_LUP_ADDITIONAL_POWER")
        txtPowerSpecification.Text = UtilityDt.Rows(0)("PM_LUP_POWER_SPECIFICATION")
        ' ele meter yes end

        'Utility/Power back up Details Section end

        'Other Services Section start
        txtNoOfTwoWheelerParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_TWO_PARK")
        txtNoOfCarsParking.Text = OtherServicesDt.Rows(0)("PM_LO_NO_CAR_PARK")
        txtDistanceFromAirPort.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_AIRPT")
        txtDistanceFromRailwayStation.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_RAIL")
        txtDistanceFromBustop.Text = OtherServicesDt.Rows(0)("PM_LO_DIST_FRM_BUS")
        txtadditionalparking.Text = Convert.ToDouble(OtherServicesDt.Rows(0)("PM_LO_ADDI_PARK_CHRG"))
        'Other Services Section end

        'Other Details Section start
        txtCompetitorsVicinity.Text = OtherDetailsDt.Rows(0)("PM_LO_COMP_VICINITY")
        ddlRollingShutter.ClearSelection()
        Dim rolShutter As String = OtherDetailsDt.Rows(0)("PM_LO_ROL_SHUTTER")
        ''ddlRollingShutter.Items.FindByValue(rolShutter).Selected = True
        If (rolShutter <> "") Then
            ddlRollingShutter.Items.FindByValue(rolShutter).Selected = True
        End If

        txtOfficeEquipments.Text = OtherDetailsDt.Rows(0)("PM_LO_OFF_EQUIPMENTS")

        ddlesc.ClearSelection()
        Dim leaseEsc As String = LeaseEscalationDt.Rows(0)("PM_LD_LES_ESCALATON")
        ''ddlDgSet.Items.FindByValue(dgSetVal).Selected = True
        If (leaseEsc <> "") Then
            ddlesc.Items.FindByValue(leaseEsc).Selected = True
        End If
        'Other Details Section end

        'Lease Escalation Details Section start
        'EscalationType.Visible = False
        'RentRevisionPanel.Visible = False
        'EscalationType1.Visible = False
        'EscalationType2.Visible = False
        'If (LeaseEscalationDt.Rows.Count > 0) Then
        'If (LeaseEscalationDt.Rows.Count > 0) Then
        '    ddlesc.ClearSelection()
        '    Dim leaseEsc As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC")
        '    If (leaseEsc = "Yes") Then
        '        LandlordEscalation.Visible = True
        '        EscalationType.Visible = True
        '        ''EscalationType1.Visible = True
        '    End If
        '    ''ddlesc.Items.FindByValue(leaseEsc).Selected = True
        '    If (leaseEsc <> "") Then
        '        ddlesc.Items.FindByValue(leaseEsc).Selected = True
        '    End If

        '    rblEscalationType.ClearSelection()
        '    rblEscalationType.SelectedValue = LeaseEscalationDt.Rows(0)("PM_LES_ESC_ON")
        '    Dim RblEscType As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_ON")
        '    If (RblEscType = "Lease") Then

        '        EscalationType1.Visible = True
        '        EscalationType2.Visible = True

        '        RentRevisionPanel.Visible = True
        '        Dim RR_lst As New List(Of RentRevision)()
        '        Dim rr_obj As New RentRevision()
        '        'For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)

        '        For i As Integer = 0 To leaseAllSections.Tables(9).Rows.Count - 1
        '            rr_obj = New RentRevision()
        '            'd = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtIntervaldu.Text))
        '            rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(i + 1) + ":"
        '            rr_obj.RR_Year = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_INT_DATE")
        '            rr_obj.RR_YEARS = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_YEARS")
        '            rr_obj.RR_MONTHS = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_MONTHS")
        '            rr_obj.RR_DAYS = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_DAYS")
        '            If LeaseEscalationDt.Rows(0)("PM_LES_ESC_AMOUNTIN") = "Value" Then
        '                rr_obj.RR_Percentage = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_AMTVAL")
        '            Else
        '                rr_obj.RR_Percentage = leaseAllSections.Tables(9).Rows(i)("PM_LES_ESC_AMTPER")
        '            End If
        '            rr_obj.pm_les_esc_id = leaseAllSections.Tables(9).Rows(i)("pm_les_esc_id")
        '            RR_lst.Add(rr_obj)

        '        Next
        '        Repeater2.DataSource = RR_lst
        '        Repeater2.DataBind()
        '        Dim Intervaltype1 As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_INT_TYPE")
        '        If (Intervaltype1 = "Flexy") Then
        '            For Each item As RepeaterItem In Repeater2.Items
        '                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
        '                    Dim lbl = CType(item.FindControl("lblRevYear"), Label)
        '                    Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
        '                    'lbl.Visible = False
        '                    lbtxt.Visible = False
        '                End If
        '            Next
        '            Escflex.Visible = True
        '            Escduration.Visible = False
        '            txtnofEscltions.Text = LeaseEscalationDt.Rows(0)("PM_LES_ESC_INT_DUR")
        '        Else
        '            For Each item As RepeaterItem In Repeater2.Items
        '                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
        '                    'Dim lbl = CType(item.FindControl("lblEscno"), Label)
        '                    Dim lbtxt = CType(item.FindControl("ddlYear"), TextBox)
        '                    Dim lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
        '                    Dim lbday = CType(item.FindControl("ddlDay"), TextBox)
        '                    Dim lbesc = CType(item.FindControl("TextBox1"), TextBox)
        '                    Dim lbyr = CType(item.FindControl("lbyear"), Label)
        '                    Dim lbmth = CType(item.FindControl("lbmnt"), Label)
        '                    Dim lbdy = CType(item.FindControl("lbldy"), Label)
        '                    Dim lble = CType(item.FindControl("lbldf"), Label)
        '                    'lbl.Visible = False
        '                    lbtxt.Visible = False
        '                    lbmnth.Visible = False
        '                    lbesc.Visible = False
        '                    lbday.Visible = False
        '                    lbyr.Visible = False
        '                    lbmth.Visible = False
        '                    lbdy.Visible = False
        '                    lble.Visible = False
        '                    Escduration.Visible = True
        '                    Escflex.Visible = False
        '                End If
        '            Next
        '        End If
        '    Else
        '        RentRevisionPanel.Visible = False

        '    End If
        '    ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
        '    If (RblEscType <> "") Then
        '        rblEscalationType.Items.FindByValue(RblEscType).Selected = True
        '    End If
        '    ddlEscalation.ClearSelection()
        '    Dim leaseEscType As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_TYPE")
        '    ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
        '    If (leaseEscType <> "") Then
        '        ddlEscalation.Items.FindByValue(leaseEscType).Selected = True
        '    End If
        '    ddlintervaltype.ClearSelection()
        '    Dim Intervaltype As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_INT_TYPE")
        '    ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
        '    If (Intervaltype <> "") Then
        '        ddlintervaltype.Items.FindByValue(Intervaltype).Selected = True
        '    End If
        '    ddlamount.ClearSelection()
        '    Dim Amountin As String = LeaseEscalationDt.Rows(0)("PM_LES_ESC_AMOUNTIN")
        '    ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
        '    If (Amountin <> "") Then
        '        ddlamount.Items.FindByValue(Amountin).Selected = True
        '    End If

        '    txtIntervaldu.Text = LeaseEscalationDt.Rows(0)("PM_LES_ESC_INT_DUR")
        '    Session("interval") = txtIntervaldu.Text
        '    Session("Total") = leaseAllSections.Tables(9).Rows.Count

        'Else
        '    ddlesc.ClearSelection()
        '    ddlesc.Items.FindByValue("No").Selected = True
        '    Dim SecondDate2 As Date = Date.Parse(txtsdate.Text)
        '    Session("escdt") = SecondDate2
        'End If
        ''LandlordEscalation.Visible = True
        Dim reminderBefore As String
        reminderBefore = LeaseDetailsDt.Rows(0).Item("PM_LRE_REMINDER_BEFORE")
        Dim parts As String() = reminderBefore.Split(New Char() {","c})
        For i As Integer = 0 To parts.Length - 1
            For j As Integer = 0 To ReminderCheckList.Items.Count - 1
                If ReminderCheckList.Items(j).Value = parts(i) Then
                    ReminderCheckList.Items(j).Selected = True
                End If
            Next
        Next
        txtBuiltupArea.Text = AreaDetails.Rows(0)("PM_AR_BUA_AREA")
        txtSuperBulArea.Text = AreaDetails.Rows(0)("PM_AR_SBU_AREA")
        txtCarpetArea.Text = AreaDetails.Rows(0)("PM_AR_CARPET_AREA")
        txtCommonArea.Text = AreaDetails.Rows(0)("PM_AR_COM_AREA")
        txtRentableArea.Text = AreaDetails.Rows(0)("PM_AR_RENT_AREA")
        txtUsableArea.Text = AreaDetails.Rows(0)("PM_AR_USABEL_AREA")
        txtPlotArea.Text = AreaDetails.Rows(0)("PM_AR_PLOT_AREA")
        txtCeilingHight.Text = AreaDetails.Rows(0)("PM_AR_FTC_HIGHT")
        txtBeamBottomHight.Text = AreaDetails.Rows(0)("PM_AR_FTBB_HIGHT")
        txtMaxCapacity.Text = AreaDetails.Rows(0)("PM_AR_MAX_CAP")
        txtOptCapacity.Text = AreaDetails.Rows(0)("PM_AR_OPT_CAP")
        txtSeatingCapacity.Text = AreaDetails.Rows(0)("PM_AR_SEATING_CAP")


        ddlFlooringType.ClearSelection()
        Dim FlooringType As String = AreaDetails.Rows(0)("PM_AR_FLOOR_TYPE")

        If (FlooringType <> "") Then
            ddlFlooringType.Items.FindByValue(FlooringType).Selected = True
        End If

        ''ddlFlooringType.SelectedValue = AreaDetails.Rows(0)("PM_AR_FLOOR_TYPE")
        If (AreaDetails.Rows(0)("PM_AR_FSI").ToString() = "") Then
            ddlFSI.SelectedValue = ""
        Else
            ddlFSI.SelectedValue = IIf(AreaDetails.Rows(0).Item("PM_AR_FSI") = True, 1, 0)
        End If
        'ddlFSI.SelectedValue = AreaDetails.Rows(0)("PM_AR_FSI")
        txtEfficiency.Text = AreaDetails.Rows(0)("PM_AR_PREF_EFF")
        txtFSI.Text = AreaDetails.Rows(0)("PM_AR_FSI_RATIO")
        'Lease Escalation Details Section end

        ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
        ViewState("LES_TOT_RENT") = Convert.ToDecimal(txttotalrent.Text)
        ViewState("LES_SEC_DEPOSIT") = Convert.ToDecimal(txtpay.Text)

        'If LeaseNAreaNDetails.Rows(0)("PM_LES_STA_ID") = 4024 Or LeaseNAreaNDetails.Rows(0)("PM_LES_STA_ID") = 4004 Then
        '    btnUpdateLease.Visible = True
        '    btnApproveLease.Visible = False
        'ElseIf LeaseNAreaNDetails.Rows(0)("PM_LES_STA_ID") = 4023 Then
        '    btnApproveLease.Visible = True
        '    btnUpdateLease.Visible = False
        'Else
        '    btnUpdateLease.Visible = False
        '    btnApproveLease.Visible = False
        'End If
    End Sub

    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select Flooring Type--", 0))
    End Sub
    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

    Protected Sub gvlldata_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvlandlordItems.RowCommand
        Try
            If e.CommandName = "GetLandlordDetails" Then
                Landlord.Visible = True
                btnAddNewLandlord.Visible = True
                hdnLandlordSNO.Value = e.CommandArgument
                GetSelectedLandlordDetails(hdnLandlordSNO.Value)
                LandlordDetails.Visible = True
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetSelectedLandlordDetails(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORD_DETAILS_BY_ID")
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@FLAG", 2, DbType.String)
        sp.Command.AddParameter("@HDN_LL_VALUE", Reqid, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        updatepanel.Visible = True
        Landlord.Visible = True
        'escaltionlandlord.Visible = False
        'If rblEscalationType.SelectedValue = "LandLoard" Then
        '    LandlordEscalation.Visible = True
        '    RentRevisionPanel.Visible = True
        'End If
        SetLandlordValsToUpdate(ds.Tables(0))
    End Sub
    Private Sub SetLandlordValsToUpdate(ByVal landlorddt As DataTable)
        'Dim selectedProp As String = landlorddt.Rows(0)("AIR_ITEM_SUBCAT")
        'ddlproperty.Items.FindByValue(selectedProp).Selected = True
        btnLandlord.Text = "Update"
        btnBack.Text = "Close"
        btnDelLandlord.Visible = True
        txtName.Text = landlorddt.Rows(0)("PM_LL_NAME")

        txtAddress.Text = landlorddt.Rows(0)("PM_LL_ADDRESS1")
        txtAddress2.Text = landlorddt.Rows(0)("PM_LL_ADDRESS2")
        txtAddress3.Text = landlorddt.Rows(0)("PM_LL_ADDRESS3")
        txtL1State.Text = landlorddt.Rows(0)("PM_LL_STATE")


        ddlCity.ClearSelection()
        Dim selectedCity As String = landlorddt.Rows(0)("PM_LL_CITY_CODE")
        ddlCity.Items.FindByValue(selectedCity).Selected = True

        txtld1Pin.Text = landlorddt.Rows(0)("PM_LL_PINCODE")
        txtPAN.Text = landlorddt.Rows(0)("PM_LL_PAN")


        txtldemail.Text = landlorddt.Rows(0)("PM_LL_EMAIL")


        ddlServiceTaxApplicable.ClearSelection()
        Dim serviceTax As String = landlorddt.Rows(0)("PM_LL_GST").ToString()
        If (serviceTax <> "") Then
            ddlServiceTaxApplicable.Items.FindByValue(serviceTax).Selected = True
        End If

        txtServiceTaxlnd.Text = landlorddt.Rows(0)("PM_LL_GST_NO").ToString()


        ddlPropertyTaxApplicable.ClearSelection()
        Dim propertyTax As String = landlorddt.Rows(0)("PM_LL_PPTAX_APPLICABLE")
        ddlPropertyTaxApplicable.Items.FindByValue(propertyTax).Selected = True


        txtPropertyTax.Text = landlorddt.Rows(0)("PM_LL_PROPERTY_TAX")

        txtContactDetails.Text = landlorddt.Rows(0)("PM_LL_PHONE_NO")


        ddlAmountIn.ClearSelection()
        Dim amountIn As String = landlorddt.Rows(0)("PM_LL_AMOUNT_IN")
        ddlAmountIn.Items.FindByValue(amountIn).Selected = True



        txtpmonthrent.Text = landlorddt.Rows(0)("PM_LL_MON_RENT_PAYABLE")
        txtpsecdep.Text = landlorddt.Rows(0)("PM_LL_SECURITY_DEPOSIT")


        ddlpaymentmode.ClearSelection()
        Dim paymentMode As String = landlorddt.Rows(0)("PM_LL_PAYMENT_MODE")
        ddlpaymentmode.Items.FindByValue(paymentMode).Selected = True


        txtBankName.Text = landlorddt.Rows(0)("PM_LL_BANK")
        txtAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")

        'NEFT Transfer
        txtNeftBank.Text = landlorddt.Rows(0)("PM_LL_BANK")
        txtNeftAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")
        txtNeftBrnch.Text = landlorddt.Rows(0)("PM_LL_BRANCH")
        txtNeftIFSC.Text = landlorddt.Rows(0)("PM_LL_IFSC")
        txtNeftAccNo.Text = landlorddt.Rows(0)("PM_LL_ACC_NO")
        txtcheqno.Text = landlorddt.Rows(0)("PM_LL_CHEQDDNO")
        txtchedddt.Text = Convert.ToString(landlorddt.Rows(0)("PM_LL_CHEQDDDT"))
        txtllmaint.Text = landlorddt.Rows(0)("PM_LL_MAINT_CHARGES")
        txtllAments.Text = landlorddt.Rows(0)("PM_LL_AMENITIES_CHARGES")
        ddlrenttype.ClearSelection()
        Dim renttype As String = landlorddt.Rows(0)("PM_LL_RENTTYPE")
        If (renttype <> "") Then
            ddlrenttype.Items.FindByValue(renttype).Selected = True
        End If

        ddltds.ClearSelection()
        Dim TDS As String = landlorddt.Rows(0)("PM_LL_TDS")
        If (TDS <> "") Then
            ddltds.Items.FindByValue(TDS).Selected = True
            txttdsper.Text = landlorddt.Rows(0)("PM_LL_TDS_PERCENTAGE")
        End If
        If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
            panel1.Visible = True
            panel2.Visible = False
        ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
            panel1.Visible = False
            panel2.Visible = True
        Else
            panel1.Visible = False
            panel2.Visible = False
        End If

        'txtName.Text = landlorddt.Rows(0)("PM_LL_FROM_DATE")
        'txtName.Text = landlorddt.Rows(0)("PM_LL_TO_DATE")

        'txtName.Text = landlorddt.Rows(0)("PM_LL_TDS")
        'txtName.Text = landlorddt.Rows(0)("PM_LL_TDS_REMARKS")

    End Sub
    Private Sub BindPayMode()
        ObjSubSonic.Binddropdown(ddlpaymentmode, "PM_GET_PAYMENT_MODE", "NAME", "CODE")
    End Sub
    Private Sub BindStatus()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PCL_GET_PROJECTTYPES")
        ddlstatus.DataSource = sp3.GetDataSet()
        ddlstatus.DataTextField = "PROJECTNAM"
        ddlstatus.DataValueField = "PROJECTCD"
        ddlstatus.DataBind()
        ddlstatus.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Protected Sub ddlpaymentmode_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlpaymentmode.SelectedIndexChanged
        If ddlpaymentmode.SelectedIndex > 0 Then
            If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                panel1.Visible = True
                panel2.Visible = False
            ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                panel1.Visible = False
                panel2.Visible = True
            Else
                panel1.Visible = False
                panel2.Visible = False
            End If
        End If
    End Sub

    Private Sub BindCity()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_CITY_ADM")
        sp.Command.AddParameter("@MODE", 2, DbType.Int32)
        sp.Command.AddParameter("@AUR_ID", Session("Uid").ToString, DbType.String)
        ddlCity.DataSource = sp.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Private Sub BindProperty()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_BIND_PROPERTIES_FOR_LEASE")
        '  sp.Command.AddParameter("@dummy", 0, DbType.String)
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.String)
        sp.Command.AddParameter("@FLAG", 1, DbType.String)
        ddlproperty.DataSource = sp.GetDataSet()
        ddlproperty.DataTextField = "PN_NAME"
        ddlproperty.DataValueField = "PM_PPT_SNO"
        ddlproperty.DataBind()
        ddlproperty.Items.Insert(0, New ListItem("--Select--", "--Select--"))
    End Sub

    Protected Sub ddlproperty_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlproperty.SelectedIndexChanged
        If ddlproperty.SelectedIndex > 0 Then
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar, 200)
            param(0).Value = ddlproperty.SelectedValue
            ds = New DataSet
            ds = ObjSubSonic.GetSubSonicDataSet("BIND_DETAILS_BY_PROP", param)
            If ds.Tables(0).Rows.Count > 0 Then
                txtPropAddedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
                txtApprovedBy.Text = ds.Tables(0).Rows(0).Item("ADDED_BY").ToString()
                'txtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_PPT_CODE").ToString()
            End If
        Else
            txtPropAddedBy.Text = ""
            txtApprovedBy.Text = ""
        End If
    End Sub

    Dim txtEscId As String
    'Private Sub SetLandlordValsToUpdate(ByVal landlorddt As DataSet)
    '    'Dim selectedProp As String = landlorddt.Rows(0)("AIR_ITEM_SUBCAT")
    '    'ddlproperty.Items.FindByValue(selectedProp).Selected = True
    '    btnLandlord.Text = "Update"
    '    btnBack.Text = "Cancel"
    '    btnDelLandlord.Visible = True
    '    txtName.Text = landlorddt.Tables(0).Rows(0)("PM_LL_NAME")

    '    txtAddress.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ADDRESS1")
    '    txtAddress2.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ADDRESS2")
    '    txtAddress3.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ADDRESS3")
    '    txtL1State.Text = landlorddt.Tables(0).Rows(0)("PM_LL_STATE")

    '    ddlCity.ClearSelection()
    '    Dim selectedCity As String = landlorddt.Tables(0).Rows(0)("PM_LL_CITY_CODE")
    '    ddlCity.Items.FindByValue(selectedCity).Selected = True
    '    txtld1Pin.Text = landlorddt.Tables(0).Rows(0)("PM_LL_PINCODE")
    '    txtPAN.Text = landlorddt.Tables(0).Rows(0)("PM_LL_PAN")
    '    txtldemail.Text = landlorddt.Tables(0).Rows(0)("PM_LL_EMAIL")

    '    ddlServiceTaxApplicable.ClearSelection()
    '    Dim serviceTax As String = landlorddt.Tables(0).Rows(0)("PM_LL_SERVTAX_APPLICABLE")
    '    If (serviceTax <> "") Then
    '        ddlServiceTaxApplicable.Items.FindByValue(serviceTax).Selected = True
    '    End If

    '    txtServiceTaxlnd.Text = landlorddt.Tables(0).Rows(0)("PM_LL_SERVICE_TAX")
    '    ddlPropertyTaxApplicable.ClearSelection()
    '    Dim propertyTax As String = landlorddt.Tables(0).Rows(0)("PM_LL_PPTAX_APPLICABLE")
    '    If (propertyTax <> "") Then
    '        ddlPropertyTaxApplicable.Items.FindByValue(propertyTax).Selected = True
    '    End If

    '    txtPropertyTax.Text = landlorddt.Tables(0).Rows(0)("PM_LL_PROPERTY_TAX")
    '    txtContactDetails.Text = landlorddt.Tables(0).Rows(0)("PM_LL_PHONE_NO")
    '    ddlAmountIn.ClearSelection()
    '    Dim amountIn As String = landlorddt.Tables(0).Rows(0)("PM_LL_AMOUNT_IN")
    '    If (amountIn <> "") Then
    '        ddlAmountIn.Items.FindByValue(amountIn).Selected = True
    '    End If
    '    txtpmonthrent.Text = Convert.ToDouble(landlorddt.Tables(0).Rows(0)("PM_LL_MON_RENT_PAYABLE"))
    '    txtpsecdep.Text = Convert.ToDouble(landlorddt.Tables(0).Rows(0)("PM_LL_SECURITY_DEPOSIT"))

    '    'ddlpaymentmode.ClearSelection()
    '    'If Not IsDBNull(landlorddt.Tables(0).Rows(0)("PM_LL_PAYMENT_MODE")) Then
    '    '    sdr2.Value = dt.Rows(0)("name")
    '    'End If
    '    ddlpaymentmode.ClearSelection()
    '    Dim paymentMode As String = landlorddt.Tables(0).Rows(0)("PM_LL_PAYMENT_MODE")
    '    If (paymentMode <> "") Then
    '        ddlpaymentmode.Items.FindByValue(paymentMode).Selected = True
    '    End If
    '    txtBankName.Text = landlorddt.Tables(0).Rows(0)("PM_LL_BANK")
    '    txtAccNo.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ACC_NO")
    '    'NEFT Transfer
    '    txtNeftBank.Text = landlorddt.Tables(0).Rows(0)("PM_LL_BANK")
    '    txtNeftAccNo.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ACC_NO")
    '    txtNeftBrnch.Text = landlorddt.Tables(0).Rows(0)("PM_LL_BRANCH")
    '    txtNeftIFSC.Text = landlorddt.Tables(0).Rows(0)("PM_LL_IFSC")
    '    txtNeftAccNo.Text = landlorddt.Tables(0).Rows(0)("PM_LL_ACC_NO")
    '    'txtcheqno.Text = landlorddt.Tables(0).Rows(0)("PM_LL_CHEQDDNO")
    '    'txtchedddt.Text = Convert.ToString(landlorddt.Tables(0).Rows(0)("PM_LL_CHEQDDDT"))
    '    If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
    '        panel1.Visible = True
    '        panel2.Visible = False
    '    ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
    '        panel1.Visible = False
    '        panel2.Visible = True
    '    Else
    '        panel1.Visible = False
    '        panel2.Visible = False
    '    End If


    '    'Lease Escalation Details Section start

    '    If (landlorddt.Tables(1).Rows.Count > 0) Then
    '        ddllandloardescalation.ClearSelection()
    '        Dim LLleaseEsc As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC")
    '        'If (LLleaseEsc = "Yes") Then
    '        '    LandlordEscalation.Visible = True
    '        '    EscalationType.Visible = True
    '        '    ''EscalationType1.Visible = True
    '        'End If
    '        ''ddlesc.Items.FindByValue(leaseEsc).Selected = True
    '        If (LLleaseEsc <> "") Then
    '            ddllandloardescalation.Items.FindByValue(LLleaseEsc).Selected = True
    '        End If

    '        'rblEscalationType.ClearSelection()
    '        'rblEscalationType.SelectedValue = landlorddt.Tables(0).Rows(0)("PM_LES_ESC_ON")
    '        'Dim RblEscType As String = landlorddt.Tables(0).Rows(0)("PM_LES_ESC_ON")
    '        'If (RblEscType = "Lease") Then

    '        '    EscalationType1.Visible = True
    '        '    EscalationType2.Visible = True
    '        'End If
    '        '''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
    '        'If (RblEscType <> "") Then
    '        '    rblEscalationType.Items.FindByValue(RblEscType).Selected = True
    '        'End If
    '        ddllandloardescaltype.ClearSelection()
    '        Dim LLleaseEscType As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_TYPE")
    '        ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
    '        If (LLleaseEscType <> "") Then
    '            ddllandloardescaltype.Items.FindByValue(LLleaseEscType).Selected = True
    '        End If
    '        ddllandloardinterval.ClearSelection()
    '        Dim LLIntervaltype As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_INT_TYPE")
    '        ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
    '        If (LLIntervaltype <> "") Then
    '            ddllandloardinterval.Items.FindByValue(LLIntervaltype).Selected = True
    '        End If
    '        ddllandloardescAmount.ClearSelection()
    '        Dim LLAmountin As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_AMOUNTIN")
    '        ''ddlLeaseEscType.Items.FindByValue(leaseEscType).Selected = True
    '        If (LLAmountin <> "") Then
    '            ddllandloardescAmount.Items.FindByValue(LLAmountin).Selected = True
    '        End If

    '        txtescduration.Text = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_INT_DUR")


    '        Dim RblEscType As String = landlorddt.Tables(1).Rows(0)("PM_LES_ESC_ON")
    '        If (RblEscType = "LandLord") Then
    '            Landloardescalation.Visible = True
    '            Landloardescalation1.Visible = True
    '            RentRevisionPanel.Visible = True
    '            Dim RR_lst As New List(Of RentRevision)()
    '            Dim rr_obj As New RentRevision()
    '            ''For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)
    '            For i As Integer = 0 To landlorddt.Tables(1).Rows.Count - 1
    '                rr_obj = New RentRevision()
    '                'd = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtIntervaldu.Text))
    '                rr_obj.RR_Year = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_INT_DATE")
    '                If ddllandloardescAmount.SelectedValue = "Value" Then
    '                    rr_obj.RR_Percentage = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_AMTVAL")
    '                Else
    '                    rr_obj.RR_Percentage = landlorddt.Tables(1).Rows(i)("PM_LES_ESC_AMTPER")
    '                End If
    '                rr_obj.pm_les_esc_id = landlorddt.Tables(1).Rows(i)("pm_les_esc_id")
    '                RR_lst.Add(rr_obj)
    '            Next
    '            Repeater1.DataSource = RR_lst
    '            Repeater1.DataBind()

    '        End If

    '    End If

    'End Sub

    Public Sub ClearLandlord()
        txtName.Text = ""
        txtAddress.Text = ""
        txtAddress2.Text = ""
        txtAddress3.Text = ""
        txtL1State.Text = ""
        ddlCity.ClearSelection()
        txtld1Pin.Text = ""
        txtPAN.Text = ""
        ddlServiceTaxApplicable.ClearSelection()
        txtServiceTaxlnd.Text = "0"
        ddlPropertyTaxApplicable.ClearSelection()
        txtPropertyTax.Text = "0"
        txtContactDetails.Text = ""
        txtldemail.Text = ""
        'ddlAmountIn.ClearSelection()
        txtpmonthrent.Text = "0"
        txtpsecdep.Text = "0"
        ddlpaymentmode.ClearSelection()
        txtBankName.Text = ""
        txtAccNo.Text = ""
        ddltds.ClearSelection()
        txttdsper.Text = ""
        txtNeftBank.Text = ""
        txtNeftAccNo.Text = ""
        txtNeftBrnch.Text = ""
        txtNeftIFSC.Text = ""
        txtcheqno.Text = ""
        txtchedddt.Text = ""
        ddlrenttype.ClearSelection()
        txtllmaint.Text = 0
        txtllAments.Text = 0
    End Sub

    Protected Sub gvitems_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvitems.PageIndexChanging
        gvitems.PageIndex = e.NewPageIndex()
        gvitems.DataSource = ViewState("reqDetails")
        gvitems.DataBind()
    End Sub

    Private Sub BindTenure()
        ObjSubSonic.Binddropdown(ddlTenure, "PM_GET_PAYMENT_TERMS", "PM_PT_NAME", "PM_PT_SNO")
        ddlTenure.Items.RemoveAt(0)
    End Sub




    Protected Sub rblCostType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblCostType.SelectedIndexChanged
        If rblCostType.SelectedValue = "Sqft" Then
            Costype1.Visible = True
            Costype2.Visible = False
            txtSeatCost.Text = ""
        ElseIf rblCostType.SelectedValue = "Seat" Then
            Costype1.Visible = False
            Costype2.Visible = True
            txtRentPerSqftCarpet.Text = ""
            txtRentPerSqftBUA.Text = ""
        End If
    End Sub
    'Private Sub ddllandloardinterval_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddllandloardinterval.SelectedIndexChanged
    '    If ddllandloardinterval.SelectedValue = "Flexy" Then
    '        Landloardescalation1.Visible = True
    '        LLescduration.Visible = False
    '        LandloardRentRevisionPanel.Visible = False
    '    Else
    '        Landloardescalation1.Visible = False
    '        LLescduration.Visible = True
    '        LandloardRentRevisionPanel.Visible = False
    '    End If
    'End Sub
    'Private Sub BindRentRevision()
    '    Dim rowCount As Integer = 0
    '    rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
    '    If rowCount > 1 Then
    '        RentRevisionPanel.Visible = True
    '        Dim RR_lst As New List(Of RentRevision)()
    '        Dim rr_obj As New RentRevision()
    '        For i As Integer = 1 To rowCount - 1
    '            rr_obj = New RentRevision()
    '            rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '            rr_obj.RR_Percentage = "0"
    '            RR_lst.Add(rr_obj)
    '        Next
    '        Repeater2.DataSource = RentRevision
    '        Repeater2.DataBind()
    '    Else
    '        RentRevisionPanel.Visible = False
    '    End If
    'End Sub

    'Private Sub BindRentRevisionOnTextChanged()
    '    Dim rowCount As Integer = 0
    '    rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
    '    If rowCount > 1 Then
    '        RentRevisionPanel.Visible = True
    '        Dim RR_lst As New List(Of RentRevision)()
    '        Dim rr_obj As New RentRevision()
    '        For i As Integer = 1 To rowCount - 1
    '            rr_obj = New RentRevision()
    '            rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '            rr_obj.RR_Percentage = "0"
    '            RR_lst.Add(rr_obj)
    '        Next
    '        Repeater2.DataSource = RR_lst
    '        Repeater2.DataBind()
    '    Else
    '        RentRevisionPanel.Visible = False
    '    End If
    'End Sub

    'Protected Sub txtedate_TextChanged(sender As Object, e As EventArgs) Handles txtedate.TextChanged
    '    Dim firstDate1 As Date = Date.Parse(txtsdate.Text)
    '    Dim SecondDate1 As Date = Date.Parse(txtedate.Text)
    '    If firstDate1.Date < SecondDate1.Date Then
    '        Dim years1 As Integer = DateDiff(DateInterval.Year, CDate(SecondDate1), CDate(firstDate1))
    '        'Dim frmdate As DateTime = txtsdate.Text
    '        'Dim todate As DateTime = txtedate.Text
    '        txtLeasePeiodinYears.Text = years1
    '        lblmesg.Text = ""
    '        If ddlintervaltype.SelectedValue = "Flexy" Then
    '            BindRentRevisiondtflex()
    '            For Each item As RepeaterItem In Repeater2.Items
    '                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '                    Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '                    Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '                    lbl.Visible = False
    '                    lbtxt.Visible = False
    '                End If
    '            Next
    '        Else
    '            BindRentRevisiondt()
    '            For Each item As RepeaterItem In Repeater2.Items
    '                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '                    'Dim lbl = CType(item.FindControl("lblEscno"), Label)
    '                    Dim lbtxt = CType(item.FindControl("ddlYear"), TextBox)
    '                    Dim lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
    '                    Dim lbday = CType(item.FindControl("ddlDay"), TextBox)
    '                    Dim lbesc = CType(item.FindControl("TextBox1"), TextBox)
    '                    Dim lbyr = CType(item.FindControl("lbyear"), Label)
    '                    Dim lbmth = CType(item.FindControl("lbmnt"), Label)
    '                    Dim lbdy = CType(item.FindControl("lbldy"), Label)
    '                    Dim lble = CType(item.FindControl("lbldf"), Label)
    '                    'lbl.Visible = False
    '                    lbtxt.Visible = False
    '                    lbmnth.Visible = False
    '                    lbesc.Visible = False
    '                    lbday.Visible = False
    '                    lbyr.Visible = False
    '                    lbmth.Visible = False
    '                    lbdy.Visible = False
    '                    lble.Visible = False
    '                End If
    '            Next
    '        End If
    '    Else
    '        lblmesg.Text = "Agreement Start Date must be less than Agreement End Date"
    '        RentRevisionPanel.Visible = False
    '    End If
    'End Sub

    'Protected Sub txtsdate_TextChanged(sender As Object, e As EventArgs) Handles txtsdate.TextChanged
    '    'BindRentRevisionOnTextChanged()
    '    Dim Date3 As Date = txtsdate.Text
    '    txtrentcommencementdate.Text = Date3.AddDays(txtRentFreePeriod.Text).ToString("MM/dd/yyyy")
    'End Sub

    Protected Sub ddlAgreementbyPOA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgreementbyPOA.SelectedIndexChanged
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
        End If
    End Sub

    Protected Sub btnDelLandlord_Click(sender As Object, e As EventArgs) Handles btnDelLandlord.Click
        DeleteLandlord()
        Landlord.Visible = False
        btnAddNewLandlord.Enabled = True
    End Sub

    Protected Sub DeleteLandlord()
        Try
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_DELETE_LANDLORD_DETAILS")
            sp.Command.AddParameter("@FLAG", 2, DbType.Int32) ' UPDATE FLAG = 1, DELETE FLAG = 2
            sp.Command.AddParameter("@ID", Convert.ToInt32(hdnLandlordSNO.Value), DbType.Int32)
            Dim res As String = sp.ExecuteScalar()
            If res = "SUCCESS" Then
                ddlAmountIn.Enabled = False
                btnBack.Enabled = True
                lblMsg.Text = "Landlord Successfully Deleted"
                ClearLandlord()
                txtName.Focus()
                'GetSelectedLeaseDetails(hdnLSNO.Value)
                Landlord.Visible = False
            Else
                lblMsg.Text = "Something went wrong; try again"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub txtInvestedArea_TextChanged(sender As Object, e As EventArgs) Handles txtInvestedArea.TextChanged
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtfurniture.Text = "", 0, txtfurniture.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
    End Sub

    Protected Sub txtmain1_TextChanged(sender As Object, e As EventArgs) Handles txtmain1.TextChanged
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtfurniture.Text = "", 0, txtfurniture.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
    End Sub

    Protected Sub txtfurniture_TextChanged(sender As Object, e As EventArgs) Handles txtfurniture.TextChanged
        txttotalrent.Text = Val(IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)) + Val(IIf(txtfurniture.Text = "", 0, txtfurniture.Text)) + Val(IIf(txtmain1.Text = "", 0, txtmain1.Text))
    End Sub

    Public Sub ClearDropdown(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearDropdown(ctrl)
            If TypeOf ctrl Is DropDownList Then
                CType(ctrl, DropDownList).ClearSelection()
            End If
        Next ctrl
    End Sub

    Public Sub ClearTextBox(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearTextBox(ctrl)
            If TypeOf ctrl Is TextBox Then
                CType(ctrl, TextBox).Text = ""
            End If
        Next ctrl
    End Sub

    Public Sub AssigTextValues()
        txtInteriorCost.Text = 0
        txtfurniture.Text = 0
        txtbrokerage.Text = 0
        txtpfees.Text = 0
        txttotalrent.Text = 0
        txttotalrent.Text = 0
        'txtFirstYear.Text = 0
        'txtSecondYear.Text = 0
        'txtThirdYear.Text = 0
        txtlock.Text = 0
        txtLeasePeiodinYears.Text = 0
        txtNotiePeriod.Text = 0
        txtAvailablePower.Text = 0
        txtAdditionalPowerKWA.Text = 0
        txtNoOfTwoWheelerParking.Text = 0
        txtNoOfCarsParking.Text = 0
        txtDistanceFromAirPort.Text = 0
        txtDistanceFromRailwayStation.Text = 0
        txtDistanceFromBustop.Text = 0
        txtpmonthrent.Text = 0
        txtpsecdep.Text = 0
        txtbrkamount.Text = 0
        txtbrkamount.Text = 0
        txtbrkmob.Text = 0
    End Sub

    Public Class ImageClas
        Private _fn As String

        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String

        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Protected Sub CancelEdit(sender As Object, e As GridViewCancelEditEventArgs)
        gvLeaseExpences.EditIndex = -1
        BindLeaseExpences()
    End Sub

    Protected Sub EditLeaseExpense(sender As Object, e As GridViewEditEventArgs)
        gvLeaseExpences.EditIndex = e.NewEditIndex
        BindLeaseExpences()
    End Sub

    Protected Sub UpdateLeaseExpense(sender As Object, e As GridViewUpdateEventArgs)
        Dim ddlServiceProvider As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddlServiceProvider"), DropDownList).SelectedItem.Value
        Dim ddliptype As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddliptype"), DropDownList).SelectedItem.Value
        Dim ddlPaidBy As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("ddlPaidBy"), DropDownList).SelectedItem.Value
        Dim PM_EXP_SNO As String = gvLeaseExpences.DataKeys(e.RowIndex).Value.ToString()
        Dim code As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("lblcode"), Label).Text
        Dim compValue As String = TryCast(gvLeaseExpences.Rows(e.RowIndex).FindControl("txtCompValue"), TextBox).Text
        Dim Flag As Integer
        Dim param(9) As SqlParameter
        'Area &Cost
        param(0) = New SqlParameter("@PM_EXP_SERV_PROVIDER", SqlDbType.VarChar)
        param(0).Value = ddlServiceProvider
        param(1) = New SqlParameter("@PM_EXP_INP_TYPE", SqlDbType.VarChar)
        param(1).Value = ddliptype
        param(2) = New SqlParameter("@PM_EXP_COMP_LES_VAL", SqlDbType.VarChar)
        param(2).Value = compValue
        param(3) = New SqlParameter("@PM_EXP_PAID_BY", SqlDbType.VarChar)
        param(3).Value = ddlPaidBy

        param(4) = New SqlParameter("@PM_EXP_SNO", SqlDbType.VarChar)
        param(4).Value = PM_EXP_SNO

        If PM_EXP_SNO = "" Then
            Flag = 2
            ' Insert
            'Else
            '    Flag = 1 ' Update
        End If

        param(5) = New SqlParameter("@FLAG", SqlDbType.VarChar)
        param(5).Value = Flag

        param(6) = New SqlParameter("@PM_EXP_PM_LES_SNO", SqlDbType.VarChar)
        param(6).Value = hdnLSNO.Value

        param(7) = New SqlParameter("@PM_EXP_HEAD", SqlDbType.VarChar)
        param(7).Value = code

        param(8) = New SqlParameter("@REQ_ID", SqlDbType.VarChar)
        param(8).Value = Session("REQ_ID")

        param(9) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
        param(9).Value = Session("Uid").ToString

        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_UPDATE_LEASE_EXPENSES", param)
            While sdr.Read()
                If sdr("result").ToString() = "SUCCESS" Then
                    lblMsg.Text = "Lease Expenses Successfully Updated" + Session("REQ_ID")
                    BindLeaseExpences()
                Else
                    lblMsg.Text = "Something went wrong; try again"
                End If
            End While
            sdr.Close()
        End Using
    End Sub

    Protected Sub RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow AndAlso gvLeaseExpences.EditIndex = e.Row.RowIndex Then
            Dim ddlSerpr As DropDownList = DirectCast(e.Row.FindControl("ddlServiceProvider"), DropDownList)
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_SERVICE_PROVIDER_DDL")
            ddlSerpr.DataSource = sp.GetDataSet().Tables(0)
            ddlSerpr.DataTextField = "PM_SP_NAME"
            ddlSerpr.DataValueField = "PM_SP_SNO"
            ddlSerpr.DataBind()

            If Not TryCast(e.Row.FindControl("lblspno"), Label).Text = "" Then
                ddlSerpr.Items.FindByValue(TryCast(e.Row.FindControl("lblspno"), Label).Text).Selected = True
            End If

            If Not TryCast(e.Row.FindControl("lblipname"), Label).Text = "" Then
                Dim ddliptype As DropDownList = DirectCast(e.Row.FindControl("ddliptype"), DropDownList)
                ddliptype.Items.FindByValue(TryCast(e.Row.FindControl("lblipname"), Label).Text).Selected = True
            End If

            If Not TryCast(e.Row.FindControl("lblPaidby"), Label).Text = "" Then

                Dim ddlPaidBy As DropDownList = DirectCast(e.Row.FindControl("ddlPaidBy"), DropDownList)
                ddlPaidBy.Items.FindByValue(TryCast(e.Row.FindControl("lblPaidby"), Label).Text).Selected = True
            End If
        End If
    End Sub

    Private Sub BindLeaseExpences()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_EXPENSES_OF_LEASE")
        sp.Command.AddParameter("@PM_LES_SNO", hdnLSNO.Value, DbType.String)
        gvLeaseExpences.DataSource = sp.GetDataSet()
        gvLeaseExpences.DataBind()
    End Sub

    Public Sub BindDocuments()
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@PM_LDOC_PM_LES_SNO", SqlDbType.NVarChar, 50)
        param(0).Value = hdnLSNO.Value
        param(1) = New SqlParameter("@COMPANYID", SqlDbType.Int)
        param(1).Value = Session("COMPANYID")
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("PM_GET_LEASE_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            grdDocs.DataSource = ds
            grdDocs.DataBind()
            tblGridDocs.Visible = True
            lblDocsMsg.Text = ""
        Else
            tblGridDocs.Visible = False
            lblMsg.Visible = True
            'lblDocsMsg.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub

    Private Sub grdDocs_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.DeleteCommand
        If grdDocs.EditItemIndex = -1 Then
            Dim Bid As Integer
            Bid = grdDocs.DataKeys(e.Item.ItemIndex)
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PM_LDOC_SNO", SqlDbType.NVarChar, 50)
            param(0).Value = Bid
            ObjSubSonic.GetSubSonicDataSet("PM_DELETE_LEASE_DOCS", param)
            BindDocuments()
        End If
    End Sub

    Private Sub grdDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdDocs.ItemCommand
        If e.CommandName = "Download" Then
            Dim pm_ldoc_sno = grdDocs.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = grdDocs.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = "~\Images\Property_Images\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\Images\Property_Images", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub

    Protected Sub ddlElectricalMeter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlElectricalMeter.SelectedIndexChanged
        If ddlElectricalMeter.SelectedValue = "Yes" Then
            Meter.Visible = True
            txtMeterLocation.Text = ""
        Else
            Meter.Visible = False
            txtMeterLocation.Text = ""
        End If
    End Sub

    Protected Sub ddlDgSet_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDgSet.SelectedIndexChanged
        If ddlDgSet.SelectedValue = "Landlord" Then
            Dgset.Visible = True
        Else
            Dgset.Visible = False
        End If
    End Sub

    Protected Sub txtNoLanlords_TextChanged(sender As Object, e As EventArgs) Handles txtNoLanlords.TextChanged
        If txtNoLanlords.Text = "" Then
            txtNoLanlords.Text = "0"
        End If
        ViewState("NO_OF_LL") = Convert.ToInt32(txtNoLanlords.Text)
        btnAddNewLandlord.Enabled = True
    End Sub

    Protected Sub btnAddNewLandlord_Click(sender As Object, e As EventArgs) Handles btnAddNewLandlord.Click
        ClearLandlord()
        Landlord.Visible = True
        LandlordDetails.Visible = True
        btnDelLandlord.Visible = False
        btnLandlord.Text = "Submit"
        btnBack.Text = "Close"
        lblMsg.Text = ""
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORD_DETAILS_BY_REQSNO")
        sp.Command.AddParameter("@LM_SNO", Session("ID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvlandlordItems.DataSource = ds.Tables(0)
        gvlandlordItems.DataBind()
    End Sub
    Dim successMsg As String
    Protected Sub btnLandlord_Click(sender As Object, e As EventArgs) Handles btnLandlord.Click
        lblMsg.Text = ""
        'lblMsgLL.Text = ""
        Try
            Dim spToCall As String
            Dim id As Integer


            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_RENT_DETAILS")
            sp1.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
            sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp1.Command.AddParameter("@ID", hdnLSNO.Value, DbType.Int32)
            Dim ds As New DataSet
            ds = sp1.GetDataSet
            If ViewState("NO_OF_LL") = ds.Tables(0).Rows(0).Item("LL_COUNT") + 1 Then
                btnAddNewLandlord.Enabled = False
            End If

            If String.Equals(btnLandlord.Text, "Submit") Then
                spToCall = "PM_ADD_LANDLORD_DETAILS_RENEWAL"
                id = Convert.ToInt32(hdnLSNO.Value)
                successMsg = "Landlord Details Successfully Added"
            Else
                spToCall = "PM_UPDATE_LANDLORD_DETAILS"
                id = Convert.ToInt32(hdnLandlordSNO.Value)
                successMsg = "Landlord Details Successfully Updated"
            End If

            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas
            Dim i As Int32 = 0
            Dim fileSize As Int64 = 0
            If TDSFu1.PostedFiles IsNot Nothing Then
                Dim count = TDSFu1.PostedFiles.Count
                While (i < count)
                    fileSize = TDSFu1.PostedFiles(i).ContentLength + fileSize
                    i = i + 1
                End While
                If (fileSize > 20971520) Then
                    lblMsg.Text = "Upload GSB Document shouldn't be larger than 20 MB."
                    Exit Sub
                End If
                For Each File In TDSFu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            Dim param As SqlParameter() = New SqlParameter(33) {}
            param(0) = New SqlParameter("@ID", DbType.Int32)
            param(0).Value = id
            param(1) = New SqlParameter("@REQ_ID", DbType.String)
            param(1).Value = Session("REQ_ID")
            param(2) = New SqlParameter("@LAN_NAME", DbType.String)
            param(2).Value = txtName.Text
            param(3) = New SqlParameter("@LAN_ADDRESS1", DbType.String)
            param(3).Value = txtAddress.Text
            param(4) = New SqlParameter("@LAN_ADDRESS2", DbType.String)
            param(4).Value = txtAddress2.Text
            param(5) = New SqlParameter("@LAN_ADDRESS3", DbType.String)
            param(5).Value = txtAddress3.Text
            param(6) = New SqlParameter("@LAN_STATE", DbType.String)
            param(6).Value = txtL1State.Text
            param(7) = New SqlParameter("@LAN_CITY", DbType.String)
            param(7).Value = ddlCity.SelectedValue
            param(8) = New SqlParameter("@LAN_PINCODE", DbType.String)
            param(8).Value = txtld1Pin.Text
            param(9) = New SqlParameter("@LAN_PAN", DbType.String)
            param(9).Value = txtPAN.Text
            param(10) = New SqlParameter("@LAN_GST_TAX", DbType.String)
            param(10).Value = ddlServiceTaxApplicable.SelectedValue
            param(11) = New SqlParameter("@LAN_GST_NUM", DbType.String)
            param(11).Value = IIf(txtServiceTaxlnd.Text = "", DBNull.Value, txtServiceTaxlnd.Text)
            param(12) = New SqlParameter("@LAN_PROP_TAX_APP", DbType.String)
            param(12).Value = ddlPropertyTaxApplicable.SelectedValue
            param(13) = New SqlParameter("@LAN_PROP_TAX", DbType.Decimal)
            param(13).Value = IIf(txtPropertyTax.Text = "", DBNull.Value, txtPropertyTax.Text)
            param(14) = New SqlParameter("@LAN_CONTACT_DET", DbType.String)
            param(14).Value = txtContactDetails.Text
            param(15) = New SqlParameter("@LAN_AMOUNT_IN", DbType.String)
            param(15).Value = ddlAmountIn.SelectedValue
            param(16) = New SqlParameter("@LAN_RENT", DbType.Decimal)
            param(16).Value = IIf(txtpmonthrent.Text = "", 0, txtpmonthrent.Text)
            param(17) = New SqlParameter("@LAN_SECURITY", DbType.Decimal)
            param(17).Value = IIf(txtpsecdep.Text = "", 0, txtpsecdep.Text)
            param(18) = New SqlParameter("@LAN_PAY_MODE", DbType.Int32)
            param(18).Value = ddlpaymentmode.SelectedValue
            param(19) = New SqlParameter("@CHEQUENO", DbType.String)
            param(19).Value = txtcheqno.Text

            param(20) = New SqlParameter("@RENT_TYPE", DbType.String)
            param(20).Value = ddlrenttype.SelectedValue
            param(21) = New SqlParameter("@LAN_MAINT", DbType.Decimal)
            param(21).Value = txtllmaint.Text
            param(22) = New SqlParameter("@LAN_AMENTS", DbType.Decimal)
            param(22).Value = txtllAments.Text

            If txtchedddt.Text <> "" Then
                param(23) = New SqlParameter("@CHEQUEDT", DbType.DateTime)
                param(23).Value = txtchedddt.Text
            Else
                param(23) = New SqlParameter("@CHEQUEDT", DbType.DateTime)
                param(23).Value = DBNull.Value
            End If

            If ddlpaymentmode.SelectedIndex > 0 Then
                If ddlpaymentmode.SelectedItem.Value = "2" Or ddlpaymentmode.SelectedItem.Value = "4" Then
                    param(24) = New SqlParameter("@LAN_BANK", DbType.String)
                    param(24).Value = txtNeftBank.Text
                    param(25) = New SqlParameter("@LAN_ACCNO", DbType.String)
                    param(25).Value = txtAccNo.Text
                    param(26) = New SqlParameter("@LAN_NEFT_BRANCH", DbType.String)
                    param(26).Value = DBNull.Value
                    param(27) = New SqlParameter("@LAN_NEFT_IFSC", DbType.String)
                    param(27).Value = DBNull.Value
                ElseIf ddlpaymentmode.SelectedItem.Value = "3" Then
                    param(24) = New SqlParameter("@LAN_BANK", DbType.String)
                    param(24).Value = txtNeftBank.Text
                    param(25) = New SqlParameter("@LAN_ACCNO", DbType.String)
                    param(25).Value = txtNeftAccNo.Text
                    param(26) = New SqlParameter("@LAN_NEFT_BRANCH", DbType.String)
                    param(26).Value = txtNeftBrnch.Text
                    param(27) = New SqlParameter("@LAN_NEFT_IFSC", DbType.String)
                    param(27).Value = txtNeftIFSC.Text
                Else
                    param(24) = New SqlParameter("@LAN_BANK", DbType.String)
                    param(24).Value = DBNull.Value
                    param(25) = New SqlParameter("@LAN_ACCNO", DbType.String)
                    param(25).Value = DBNull.Value
                    param(26) = New SqlParameter("@LAN_NEFT_BRANCH", DbType.String)
                    param(26).Value = DBNull.Value
                    param(27) = New SqlParameter("@LAN_NEFT_IFSC", DbType.String)
                    param(27).Value = DBNull.Value
                End If
            End If
            param(28) = New SqlParameter("@COMPANYID", DbType.Int32)
            param(28).Value = Session("COMPANYID")
            param(29) = New SqlParameter("@PM_LL_TDS", DbType.String)
            param(29).Value = ddltds.SelectedValue
            param(30) = New SqlParameter("@TDS_PERCENTAGE", DbType.String)
            param(30).Value = txttdsper.Text
            param(31) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(31).Value = UtilityService.ConvertToDataTable(Imgclass)
            param(32) = New SqlParameter("@LAN_EMAIL", DbType.String)
            param(32).Value = txtldemail.Text
            param(33) = New SqlParameter("@AUR_ID", DbType.String)
            param(33).Value = Session("Uid")


            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, spToCall, param)
            'If (rblEscalationType.SelectedValue = "Lease") Then
            '    Escalation()
            'Else
            '    'Landloardescalationdeatis()
            'End If
            If res = "SUCCESS" Then
                ddlAmountIn.Enabled = False
                '  btnFinalize.Enabled = True
                btnBack.Enabled = True
                lblMsg.Text = successMsg
                ClearLandlord()
                txtName.Focus()
                Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORD_DETAILS_BY_REQSNO")
                sp5.Command.AddParameter("@LM_SNO", Session("ID"), DbType.String)
                Dim ds1 As New DataSet
                ds1 = sp5.GetDataSet
                gvlandlordItems.DataSource = ds1.Tables(0)
                gvlandlordItems.DataBind()
                'GetSelectedLeaseDetails(hdnLSNO.Value)
                'GetLandlords()
                Landlord.Visible = True
                LandlordDetails.Visible = False
            Else
                lblMsg.Text = "Something went wrong; try again"
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnBack_Click(sender As Object, e As EventArgs)
        Landlord.Visible = True
        LandlordDetails.Visible = False
    End Sub
    Dim z As Integer = 0
    Dim w As Integer = 0
    Dim deleteCount As Integer = 0
    'Public Sub Escalation()

    '    Dim Basicrent As Int32 = txtInvestedArea.Text
    '    'If (txtpmonthrent.Text = "") Then
    '    '    txtpmonthrent.Text = 0
    '    'End If
    '    Dim LandBasicrent As Int32 = 0

    '    Dim startdate As DateTime = txtsdate.Text
    '    Dim S = startdate.Month
    '    Dim days = startdate.Day
    '    Dim h = startdate.DayOfWeek
    '    Dim CurrentMonthDays As Int16 = DateTime.DaysInMonth(startdate.Year, startdate.Month)

    '    Dim T1 As Decimal
    '    Dim T2 As Decimal
    '    Dim T3 As Decimal
    '    Dim T4 As Decimal
    '    Dim T5 As Decimal
    '    Dim prorata As Decimal
    '    Dim T7 As Decimal
    '    Dim escaltedrent As Decimal

    '    Dim L1 As Decimal
    '    Dim L2 As Decimal
    '    Dim L3 As Decimal
    '    Dim L4 As Decimal
    '    Dim L5 As Decimal
    '    Dim Landloardprorata As Decimal
    '    Dim L7 As Decimal
    '    Dim Landloardescaltedrent As Decimal

    '    Dim i As Integer
    '    i = 0
    '    Dim current As Integer
    '    current = 0
    '    Dim Landloardcurrent As Integer
    '    Landloardcurrent = 0
    '    Dim SDATE As DateTime = txtsdate.Text
    '    Dim EDATE As DateTime
    '    'If (Session("interval") <> txtIntervaldu.Text) Then
    '    Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOTAL_LANDLOARDS_LIST")
    '    sp.Command.AddParameter("@REQID", Session("REQ_ID"), DbType.String)
    '    sp.Command.AddParameter("@REQSNO", Session("ID"), DbType.String)
    '    ds = sp.GetDataSet()
    '    Dim GridrowCount As Int32 = ds.Tables(0).Rows.Count
    '    Dim c As Int32 = Repeater2.Items.Count
    '    For j As Integer = 0 To GridrowCount - 1
    '        If (j > 0) Then
    '            w = w + 1
    '            i = 0
    '        End If
    '        If (j < GridrowCount) Then
    '            Dim Revision As String
    '            Dim lblYear As Integer
    '            Dim lblDay As Integer
    '            Dim lblMonth As Integer
    '            Dim PM_LL_ID As String
    '            Dim durationesc As Integer
    '            For Each item As RepeaterItem In Repeater2.Items
    '                'Dim lblName As String = CType(item.FindControl("lblRevYear"), Label).Text
    '                'Dim Revision As String = CType(item.FindControl("txtRevision"), TextBox).Text
    '                'Dim PM_LL_ID As String = CType(item.FindControl("HDN_PM_LL_ID"), HiddenField).Value
    '                If (ddlintervaltype.SelectedValue = "Flexy") Then
    '                    lblYear = IIf(CType(item.FindControl("ddlYear"), TextBox).Text = "", 0, CType(item.FindControl("ddlYear"), TextBox).Text)
    '                    lblMonth = IIf(CType(item.FindControl("ddlMonth"), TextBox).Text = "", 0, CType(item.FindControl("ddlMonth"), TextBox).Text)
    '                    lblDay = IIf(CType(item.FindControl("ddlDay"), TextBox).Text = "", 0, CType(item.FindControl("ddlDay"), TextBox).Text)
    '                    Revision = CType(item.FindControl("TextBox1"), TextBox).Text
    '                    PM_LL_ID = CType(item.FindControl("HDN_PM_LL_ID"), HiddenField).Value
    '                    SDATE = Convert.ToDateTime(SDATE).AddYears(Convert.ToInt32(lblYear))
    '                    SDATE = Convert.ToDateTime(SDATE).AddMonths(Convert.ToInt32(lblMonth))
    '                    SDATE = Convert.ToDateTime(SDATE).AddDays(Convert.ToInt32(lblDay))
    '                    Dim lblName As DateTime = startdate
    '                    durationesc = Convert.ToInt32(txtnofEscltions.Text)
    '                    EDATE = Convert.ToDateTime(SDATE).AddDays(-1)
    '                Else
    '                    SDATE = CType(item.FindControl("lblRevYear"), Label).Text
    '                    Revision = CType(item.FindControl("txtRevision"), TextBox).Text
    '                    PM_LL_ID = CType(item.FindControl("HDN_PM_LL_ID"), HiddenField).Value
    '                    durationesc = Convert.ToInt32(txtIntervaldu.Text)
    '                    EDATE = Convert.ToDateTime(SDATE).AddDays(-1)
    '                End If
    '                If (Revision <> Nothing) Then
    '                    Dim RevisionValue As Integer = Revision
    '                    Dim a As String = lblYear
    '                    Dim totalRent As Integer = txttotalrent.Text
    '                    Dim Tenure As Integer = ddlTenure.SelectedItem.Value
    '                    Dim Rent As Integer = 0

    '                    rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year

    '                    Duration = rowCount / durationesc 'Convert.ToInt32(txtIntervaldu.Text)
    '                    LandBasicrent = ds.Tables(0).Rows(w)("PM_LL_MON_RENT_PAYABLE").ToString()
    '                    If ddlamount.SelectedItem.Value = "Value" Then
    '                        If (i = 0) Then
    '                            T1 = Basicrent / CurrentMonthDays
    '                            T2 = CurrentMonthDays - days + 1
    '                            T3 = T1 * T2
    '                            T4 = RevisionValue + T3
    '                            T5 = CurrentMonthDays - T2
    '                            T7 = T5 * T1
    '                            prorata = T7 + T4
    '                            escaltedrent = Basicrent + (RevisionValue)
    '                            FinalRent = escaltedrent


    '                            L1 = LandBasicrent / CurrentMonthDays
    '                            L2 = CurrentMonthDays - days + 1
    '                            L3 = L1 * L2
    '                            L4 = (RevisionValue) / GridrowCount + L3
    '                            L5 = CurrentMonthDays - L2
    '                            L7 = L5 * L1
    '                            Landloardprorata = L7 + L4
    '                            Landloardescaltedrent = LandBasicrent + (RevisionValue) / GridrowCount
    '                            LandloardFinalRent = Landloardescaltedrent
    '                        End If


    '                        i = i + 1
    '                        If (i > 1) Then
    '                            FinalRent = current + (RevisionValue)
    '                            T1 = current / CurrentMonthDays
    '                            T2 = CurrentMonthDays - days + 1
    '                            T3 = T1 * T2
    '                            T4 = RevisionValue + T3
    '                            T5 = CurrentMonthDays - T2
    '                            T7 = T5 * T1
    '                            prorata = T7 + T4

    '                            LandloardFinalRent = (RevisionValue) / GridrowCount + Landloardcurrent
    '                            L1 = Landloardcurrent / CurrentMonthDays
    '                            L2 = CurrentMonthDays - days + 1
    '                            L3 = L1 * L2
    '                            L4 = (RevisionValue) / GridrowCount + L3
    '                            L5 = CurrentMonthDays - L2
    '                            L7 = L5 * L1
    '                            Landloardprorata = L7 + L4

    '                        End If
    '                        current = FinalRent
    '                        Landloardcurrent = LandloardFinalRent

    '                    Else ddlamount.SelectedItem.Value = "Percentage"
    '                        If (i = 0) Then
    '                            T1 = Basicrent / CurrentMonthDays
    '                            T2 = CurrentMonthDays - days + 1
    '                            T3 = T1 * T2
    '                            T4 = T3 * (RevisionValue / 100) + T3
    '                            T5 = CurrentMonthDays - T2
    '                            T7 = T5 * T1
    '                            prorata = T7 + T4
    '                            escaltedrent = Basicrent * (RevisionValue / 100) + Basicrent
    '                            FinalRent = escaltedrent

    '                            L1 = LandBasicrent / CurrentMonthDays
    '                            L2 = CurrentMonthDays - days + 1
    '                            L3 = L1 * L2
    '                            L4 = L3 * (RevisionValue / 100) + L3
    '                            L5 = CurrentMonthDays - L2
    '                            L7 = L5 * L1
    '                            Landloardprorata = L7 + L4
    '                            Landloardescaltedrent = LandBasicrent * (RevisionValue / 100) + LandBasicrent
    '                            LandloardFinalRent = Landloardescaltedrent
    '                        End If
    '                        i = i + 1
    '                        If (i > 1) Then
    '                            FinalRent = current * (RevisionValue / 100) + current
    '                            T1 = current / CurrentMonthDays
    '                            T2 = CurrentMonthDays - days + 1
    '                            T3 = T1 * T2
    '                            T4 = T3 * (RevisionValue / 100) + T3
    '                            T5 = CurrentMonthDays - T2
    '                            T7 = T5 * T1
    '                            prorata = T7 + T4

    '                            LandloardFinalRent = Landloardcurrent * (RevisionValue / 100) + Landloardcurrent
    '                            L1 = Landloardcurrent / CurrentMonthDays
    '                            L2 = CurrentMonthDays - days + 1
    '                            L3 = L1 * L2
    '                            L4 = L3 * (RevisionValue / 100) + L3
    '                            L5 = CurrentMonthDays - L2
    '                            L7 = L5 * L1
    '                            Landloardprorata = L7 + L4
    '                        End If
    '                        current = FinalRent
    '                        Landloardcurrent = LandloardFinalRent

    '                    End If
    '                End If
    '                Dim params(22) As SqlParameter
    '                'Area &Cost
    '                params(0) = New SqlParameter("@PM_LES_ESC_PLR_REQ_ID", SqlDbType.VarChar)
    '                params(0).Value = Session("REQ_ID")
    '                params(1) = New SqlParameter("@PM_LES_ESC", SqlDbType.VarChar)
    '                params(1).Value = ddlesc.SelectedItem.Value
    '                params(2) = New SqlParameter("@PM_LES_ESC_ON", SqlDbType.VarChar)
    '                params(2).Value = rblEscalationType.SelectedItem.Value
    '                params(3) = New SqlParameter("@PM_LES_ESC_TYPE", SqlDbType.VarChar)
    '                params(3).Value = ddlEscalation.SelectedItem.Value
    '                params(4) = New SqlParameter("@PM_LES_ESC_MONTHS", SqlDbType.Int)
    '                params(4).Value = lblMonth 'IIf(lblMonth = "", 0, lblMonth)
    '                params(5) = New SqlParameter("@PM_LES_ESC_AMOUNTIN", SqlDbType.VarChar)
    '                params(5).Value = ddlamount.SelectedItem.Value
    '                params(6) = New SqlParameter("@PM_LES_ESC_INT_DUR", SqlDbType.Int)
    '                params(6).Value = durationesc
    '                'params(7) = New SqlParameter("@PM_LES_ESC_INT_DATE", SqlDbType.DateTime)
    '                params(7) = New SqlParameter("@PM_LES_ESC_YEARS", SqlDbType.Int)
    '                params(7).Value = lblYear ' IIf(lblYear = "", 0, lblYear)
    '                params(20) = New SqlParameter("@PM_LES_ESC_DAYS", SqlDbType.Int)
    '                params(20).Value = lblDay ' IIf(lblDay = "", 0, lblDay)
    '                params(21) = New SqlParameter("@PM_LES_ESC_INT_DATE", SqlDbType.DateTime)
    '                params(21).Value = SDATE
    '                params(22) = New SqlParameter("@PM_LES_ESC_INT_TYPE", SqlDbType.VarChar)
    '                params(22).Value = ddlintervaltype.SelectedItem.Value

    '                If ddlamount.SelectedItem.Value = "Value" Then
    '                    params(8) = New SqlParameter("@PM_LES_ESC_AMTVAL", SqlDbType.Float)
    '                    params(8).Value = Revision
    '                    params(9) = New SqlParameter("@PM_LES_ESC_AMTPER", SqlDbType.Float)
    '                    params(9).Value = "0"

    '                Else
    '                    params(8) = New SqlParameter("@PM_LES_ESC_AMTVAL", SqlDbType.Float)
    '                    params(8).Value = "0"
    '                    params(9) = New SqlParameter("@PM_LES_ESC_AMTPER", SqlDbType.Float)
    '                    params(9).Value = Revision
    '                    'params(10) = New SqlParameter("@PM_LES_ESC_AMOUNT", SqlDbType.Float)
    '                    'params(10).Value = FinalRent
    '                End If
    '                params(10) = New SqlParameter("@PM_LES_ESC_AMOUNT", SqlDbType.Float)
    '                params(10).Value = FinalRent

    '                'If z < GridrowCount Then
    '                'w = 0
    '                params(11) = New SqlParameter("@PM_LES_ESC_SNO", SqlDbType.VarChar)
    '                params(11).Value = ds.Tables(0).Rows(w)("PM_LL_PM_LES_SNO").ToString()
    '                params(12) = New SqlParameter("@PM_LES_ESC_LL_NO", SqlDbType.VarChar)
    '                params(12).Value = ds.Tables(0).Rows(w)("PM_LL_NUMBER").ToString()
    '                params(13) = New SqlParameter("@PM_LES_ESC_LL_ID", SqlDbType.VarChar)
    '                params(13).Value = ds.Tables(0).Rows(w)("PM_LL_SNO").ToString()
    '                'Else
    '                '    w = w + 1
    '                '    params(11) = New SqlParameter("@PM_LES_ESC_SNO", SqlDbType.VarChar)
    '                '    params(11).Value = ds.Tables(w).Rows(w)("PM_LL_PM_LES_SNO").ToString()
    '                '    params(12) = New SqlParameter("@PM_LES_ESC_LL_NO", SqlDbType.VarChar)
    '                '    params(12).Value = ds.Tables(w).Rows(w)("PM_LL_NUMBER").ToString()
    '                '    params(13) = New SqlParameter("@PM_LES_ESC_LL_ID", SqlDbType.VarChar)
    '                '    params(13).Value = ds.Tables(w).Rows(w)("PM_LL_SNO").ToString()
    '                '    z = z + 1
    '                'End If


    '                params(14) = New SqlParameter("@pm_les_esc_id", SqlDbType.VarChar)
    '                params(14).Value = PM_LL_ID
    '                params(15) = New SqlParameter("@deleteCount", SqlDbType.Int)
    '                params(15).Value = deleteCount

    '                params(16) = New SqlParameter("@PM_LES_ESC_PRORATA", SqlDbType.Float)
    '                params(16).Value = prorata
    '                params(17) = New SqlParameter("@PM_LES_LL_ESC_RENT", SqlDbType.Float)
    '                params(17).Value = LandloardFinalRent
    '                params(18) = New SqlParameter("@PM_ESC_LL_PRORATA", SqlDbType.Float)
    '                params(18).Value = Landloardprorata
    '                params(19) = New SqlParameter("@PM_ESC_AGREEMENT_SDATE", SqlDbType.DateTime)
    '                params(19).Value = startdate
    '                'params(23) = New SqlParameter("@PM_LES_ESC_EDT", SqlDbType.DateTime)
    '                'params(23).Value = EDATE

    '                SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_UPDATELEASE_ESCALATIONDATA", params)
    '                deleteCount = deleteCount + 1
    '            Next
    '        End If
    '    Next
    '    'End If
    'End Sub

    'Public Sub Landloardescalationdeatis()
    '    Dim Basicrent As Int32 = txtInvestedArea.Text
    '    ''Dim LeaseAmount As Int32 = txttotalrent.Text
    '    Dim LandBasicrent As Int32 = txtpmonthrent.Text

    '    Dim startdate As DateTime = txtsdate.Text
    '    Dim S = startdate.Month
    '    Dim days = startdate.Day
    '    Dim h = startdate.DayOfWeek
    '    Dim CurrentMonthDays As Int16 = DateTime.DaysInMonth(startdate.Year, startdate.Month)

    '    Dim T1 As Decimal
    '    Dim T2 As Decimal
    '    Dim T3 As Decimal
    '    Dim T4 As Decimal
    '    Dim T5 As Decimal
    '    Dim prorata As Decimal
    '    Dim T7 As Decimal
    '    Dim escaltedrent As Decimal

    '    Dim L1 As Decimal
    '    Dim L2 As Decimal
    '    Dim L3 As Decimal
    '    Dim L4 As Decimal
    '    Dim L5 As Decimal
    '    Dim Landloardprorata As Decimal
    '    Dim L7 As Decimal
    '    Dim Landloardescaltedrent As Decimal

    '    Dim i As Integer
    '    i = 0
    '    Dim current As Integer
    '    current = 0
    '    Dim Landloardcurrent As Integer
    '    Landloardcurrent = 0
    '    For Each item As RepeaterItem In Repeater1.Items
    '        Dim lblName As String = CType(item.FindControl("lblRevYear"), Label).Text
    '        Dim Revision As String = CType(item.FindControl("txtRevision"), TextBox).Text
    '        Dim PM_LL_ID As String = CType(item.FindControl("HDN_PM_LL_ID"), HiddenField).Value
    '        If (Revision <> Nothing) Then
    '            Dim RevisionValue As Integer = Revision
    '            Dim a As String = lblName
    '            Dim totalRent As Integer = txtInvestedArea.Text
    '            Dim Tenure As Integer = ddlTenure.SelectedItem.Value
    '            Dim Rent As Integer = 0
    '            rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
    '            ''rowCount = DateTime.Parse(Session("EscendDate")).Year - DateTime.Parse(Session("AggstartDate")).Year
    '            Duration = rowCount / Convert.ToInt32(txtescduration.Text)

    '            'If ddlEscalation.SelectedItem.Value = "" Then

    '            'End If
    '            If ddllandloardescAmount.SelectedItem.Value = "Value" Then
    '                If (i = 0) Then
    '                    T1 = Basicrent / CurrentMonthDays
    '                    T2 = CurrentMonthDays - days + 1
    '                    T3 = T1 * T2
    '                    T4 = RevisionValue + T3
    '                    T5 = CurrentMonthDays - T2
    '                    T7 = T5 * T1
    '                    prorata = T7 + T4
    '                    escaltedrent = Basicrent + (RevisionValue)
    '                    FinalRent = escaltedrent


    '                    L1 = LandBasicrent / CurrentMonthDays
    '                    L2 = CurrentMonthDays - days + 1
    '                    L3 = L1 * L2
    '                    L4 = RevisionValue + L3
    '                    L5 = CurrentMonthDays - L2
    '                    L7 = L5 * L1
    '                    Landloardprorata = L7 + L4
    '                    Landloardescaltedrent = LandBasicrent + (RevisionValue)
    '                    LandloardFinalRent = Landloardescaltedrent
    '                End If
    '                i = i + 1
    '                If (i > 1) Then
    '                    FinalRent = current + (RevisionValue)
    '                    T1 = current / CurrentMonthDays
    '                    T2 = CurrentMonthDays - days + 1
    '                    T3 = T1 * T2
    '                    T4 = RevisionValue + T3
    '                    T5 = CurrentMonthDays - T2
    '                    T7 = T5 * T1
    '                    prorata = T7 + T4

    '                    LandloardFinalRent = Landloardcurrent + Landloardescaltedrent
    '                    L1 = LandBasicrent / CurrentMonthDays
    '                    L2 = CurrentMonthDays - days + 1
    '                    L3 = L1 * L2
    '                    L4 = RevisionValue + L3
    '                    L5 = CurrentMonthDays - L2
    '                    L7 = L5 * L1
    '                    Landloardprorata = L7 + L4

    '                End If
    '                current = FinalRent
    '                Landloardcurrent = LandloardFinalRent

    '            Else ddllandloardescAmount.SelectedItem.Value = "Percentage"
    '                If (i = 0) Then
    '                    T1 = Basicrent / CurrentMonthDays
    '                    T2 = CurrentMonthDays - days + 1
    '                    T3 = T1 * T2
    '                    T4 = T3 * (RevisionValue / 100) + T3
    '                    T5 = CurrentMonthDays - T2
    '                    T7 = T5 * T1
    '                    prorata = T7 + T4
    '                    escaltedrent = Basicrent * (RevisionValue / 100) + Basicrent
    '                    FinalRent = escaltedrent

    '                    L1 = LandBasicrent / CurrentMonthDays
    '                    L2 = CurrentMonthDays - days + 1
    '                    L3 = L1 * L2
    '                    L4 = L3 * (RevisionValue / 100) + L3
    '                    L5 = CurrentMonthDays - L2
    '                    L7 = L5 * L1
    '                    Landloardprorata = L7 + L4
    '                    Landloardescaltedrent = LandBasicrent * (RevisionValue / 100) + LandBasicrent
    '                    LandloardFinalRent = Landloardescaltedrent
    '                End If
    '                i = i + 1
    '                If (i > 1) Then
    '                    FinalRent = current * (RevisionValue / 100) + current
    '                    'FinalRent = current  * (RevisionValue / 100) + escaltedrent
    '                    T1 = current / CurrentMonthDays
    '                    T2 = CurrentMonthDays - days + 1
    '                    T3 = T1 * T2
    '                    T4 = T3 * (RevisionValue / 100) + T3
    '                    T5 = CurrentMonthDays - T2
    '                    T7 = T5 * T1
    '                    prorata = T7 + T4


    '                    LandloardFinalRent = Landloardcurrent * (RevisionValue / 100) + Landloardcurrent
    '                    L1 = Landloardcurrent / CurrentMonthDays
    '                    L2 = CurrentMonthDays - days + 1
    '                    L3 = L1 * L2
    '                    L4 = L3 * (RevisionValue / 100) + L3
    '                    L5 = CurrentMonthDays - L2
    '                    L7 = L5 * L1
    '                    Landloardprorata = L7 + L4

    '                End If
    '                current = FinalRent
    '                Landloardcurrent = LandloardFinalRent

    '            End If
    '        End If
    '        Dim params(19) As SqlParameter
    '        'Area &Cost
    '        params(0) = New SqlParameter("@PM_LES_ESC_PLR_REQ_ID", SqlDbType.VarChar)
    '        params(0).Value = Session("REQ_ID")
    '        params(1) = New SqlParameter("@PM_LES_ESC", SqlDbType.VarChar)
    '        params(1).Value = ddllandloardescalation.SelectedItem.Value
    '        params(2) = New SqlParameter("@PM_LES_ESC_ON", SqlDbType.VarChar)
    '        params(2).Value = rblEscalationType.SelectedItem.Value
    '        params(3) = New SqlParameter("@PM_LES_ESC_TYPE", SqlDbType.VarChar)
    '        params(3).Value = ddllandloardescaltype.SelectedItem.Value
    '        params(4) = New SqlParameter("@PM_LES_ESC_INT_TYPE", SqlDbType.VarChar)
    '        params(4).Value = ddllandloardinterval.SelectedItem.Value
    '        params(5) = New SqlParameter("@PM_LES_ESC_AMOUNTIN", SqlDbType.VarChar)
    '        params(5).Value = ddllandloardescAmount.SelectedItem.Value
    '        params(6) = New SqlParameter("@PM_LES_ESC_INT_DUR", SqlDbType.Int)
    '        params(6).Value = txtescduration.Text
    '        params(7) = New SqlParameter("@PM_LES_ESC_INT_DATE", SqlDbType.DateTime)
    '        params(7).Value = lblName

    '        If ddllandloardescAmount.SelectedItem.Value = "Value" Then
    '            params(8) = New SqlParameter("@PM_LES_ESC_AMTVAL", SqlDbType.Float)
    '            params(8).Value = Revision
    '            params(9) = New SqlParameter("@PM_LES_ESC_AMTPER", SqlDbType.Float)
    '            params(9).Value = "0"

    '        Else
    '            params(8) = New SqlParameter("@PM_LES_ESC_AMTVAL", SqlDbType.Float)
    '            params(8).Value = "0"
    '            params(9) = New SqlParameter("@PM_LES_ESC_AMTPER", SqlDbType.Float)
    '            params(9).Value = Revision
    '            'params(10) = New SqlParameter("@PM_LES_ESC_AMOUNT", SqlDbType.Float)
    '            'params(10).Value = FinalRent
    '        End If
    '        params(10) = New SqlParameter("@PM_LES_ESC_AMOUNT", SqlDbType.Float)
    '        params(10).Value = FinalRent
    '        params(11) = New SqlParameter("@PM_LES_ESC_SNO", SqlDbType.VarChar)
    '        params(11).Value = hdnLandlordSNO.Value
    '        params(12) = New SqlParameter("@pm_les_esc_id", SqlDbType.VarChar)
    '        params(12).Value = PM_LL_ID
    '        params(13) = New SqlParameter("@deleteCount", SqlDbType.VarChar)
    '        params(13).Value = 1
    '        params(14) = New SqlParameter("@PM_ESC_AGREEMENT_SDATE", SqlDbType.DateTime)
    '        params(14).Value = startdate
    '        params(15) = New SqlParameter("@PM_LES_ESC_PRORATA", SqlDbType.Float)
    '        params(15).Value = prorata
    '        params(16) = New SqlParameter("@PM_LES_LL_ESC_RENT", SqlDbType.Float)
    '        params(16).Value = LandloardFinalRent
    '        params(17) = New SqlParameter("@PM_ESC_LL_PRORATA", SqlDbType.Float)
    '        params(17).Value = Landloardprorata
    '        params(18) = New SqlParameter("@PM_LES_ESC_LL_NO", SqlDbType.VarChar)
    '        params(18).Value = ""
    '        params(19) = New SqlParameter("@PM_LES_ESC_LL_ID", SqlDbType.VarChar)
    '        params(19).Value = ""

    '        SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "SP_UPDATELEASE_ESCALATIONDATA", params)
    '    Next
    '    lblMsg.Text = successMsg
    'End Sub

    'Protected Sub btnUpdateLease_Click(sender As Object, e As EventArgs) Handles btnUpdateLease.Click
    '    UpdateLeaseDetails()
    'End Sub
    Private Function checkPptyReqisition() As Integer
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_CHECK_PPTY_RENE_REQ")
        sp.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
        Return sp.ExecuteScalar
    End Function
    Private Sub txtNotiePeriod_TextChanged(sender As Object, e As EventArgs) Handles txtNotiePeriod.TextChanged
        Dim LOCKINPERIOD As String = txtlock.Text
        If LOCKINPERIOD = "0" Then
            Dim Month6 As Date = txtrentcommencementdate.Text
            Dim Month7 As Date = Convert.ToDateTime(Month6).AddMonths(txtNotiePeriod.Text)
            Txtlockinpeamonth.Text = Month7
        Else
            Dim Month3 As Date = txtrentcommencementdate.Text
            Dim TOTALMONTHS As Int32 = Convert.ToInt32(txtNotiePeriod.Text) + Convert.ToInt32(txtlock.Text)
            Dim Month2 As Date = Convert.ToDateTime(Month3).AddMonths(TOTALMONTHS)
            ''Dim Month3 As Date = txtNotiePeriod.Text
            'Dim Month4 As Date = Convert.ToDateTime(Month2).AddMonths(txtNotiePeriod.Text)
            Txtlockinpeamonth.Text = Month2
        End If

    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            If txtSuperBulArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
                If CInt(txtBuiltupArea.Text) > CInt(txtSuperBulArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be more than Super Builtup Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtBuiltupArea.Text <> 0 And txtCarpetArea.Text <> 0 Then
                If CInt(txtBuiltupArea.Text) < CInt(txtCarpetArea.Text) Then 'Or CInt(txtBuiltupArea.Text) > CInt(txtRentableArea.Text) 
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be less than Carpet Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtCarpetArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
                If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than Builtup Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtBuiltupArea.Text <> 0 And txtRentableArea.Text <> 0 Then
                If CInt(txtBuiltupArea.Text) < CInt(txtRentableArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be less than Rentable Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtCarpetArea.Text <> 0 And txtRentableArea.Text <> 0 Then
                If CInt(txtCarpetArea.Text) < CInt(txtRentableArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be less than Rentable Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtSuperBulArea.Text <> 0 And txtRentableArea.Text <> 0 Then
                If CInt(txtSuperBulArea.Text) < CInt(txtRentableArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Built-up Area cannot be less than Rentable Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            If txtCarpetArea.Text <> 0 And txtSuperBulArea.Text <> 0 Then
                If CInt(txtCarpetArea.Text) > CInt(txtSuperBulArea.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Built-up Area cannot be less than Carpet Area');</SCRIPT>", False)
                    Exit Sub
                End If
            End If
            Dim firstDate1 As Date = Date.Parse(txtsdate.Text)
            Dim SecondDate1 As Date = Date.Parse(txtedate.Text)
            If firstDate1.Date > SecondDate1.Date Then
                lblmesg.Text = "Agreement Start Date must be less than Agreement End Date"
                Exit Sub
            End If
            If Session("escdt") >= SecondDate1 Then
                lblmesg.Text = "The Escalation Date must be earlier than the Expired Date"
                Exit Sub
            End If
            updatepnl.Update()
            lblMsg.Text = ""
            'lblMsgLL.Text = ""

            If Not Page.IsValid Then
                Exit Sub
            End If

            'Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_RENT_DETAILS")
            'sp1.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
            'sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            'sp1.Command.AddParameter("@ID", hdnLSNO.Value, DbType.Int32)
            'Dim ds As New DataSet
            'ds = sp1.GetDataSet

            'If ds.Tables(0).Rows(0).Item("LL_COUNT") <> ViewState("NO_OF_LL") Then
            '    lblMsgLL.Text = "Total number of Landlords entered " + ds.Tables(0).Rows(0).Item("LL_COUNT").ToString() + " is not matching with the No.Of Landlords : " + ViewState("NO_OF_LL").ToString()
            '    Exit Sub
            'Else
            '    Dim TOT_LL_RENT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_RENT"))
            '    Dim TOT_LL_SEC_DEPOSIT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT"))

            '    If TOT_LL_RENT <> ViewState("LES_TOT_RENT") Then
            '        lblMsgLL.Text = "Landlords Total Rent Payable Should Match With Lease Total Rent " + Math.Round(ViewState("LES_TOT_RENT"), 0).ToString()
            '        Exit Sub
            '    End If
            '    If TOT_LL_SEC_DEPOSIT <> ViewState("LES_SEC_DEPOSIT") Then
            '        lblMsgLL.Text = "Landlords Total Security Deposit Should Match With Lease Total Security Deposit " + Math.Round(ViewState("LES_SEC_DEPOSIT"), 0).ToString()
            '        Exit Sub
            '    End If
            'End If
            Dim res As Integer = checkPptyReqisition()
            If res > 0 Then
                lblMsg.Text = Convert.ToString(Session("REQ_ID")) & " is Already Under Renewal Requisition or L1 Renewal Requisition Approval Process"
                Exit Sub
            End If

            Dim param(92) As SqlParameter
            'Area &Cost
            param(0) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(0).Value = lblLeaseReqId.Text
            param(60) = New SqlParameter("@CTS_NUMBER", SqlDbType.VarChar)
            param(60).Value = txtLnumber.Text
            param(1) = New SqlParameter("@ENTITLE_LEASE_AMOUNT", SqlDbType.Decimal)
            param(1).Value = IIf(txtentitle.Text = "", 0, txtentitle.Text)
            param(2) = New SqlParameter("@LEASE_RENT", SqlDbType.Decimal)
            param(2).Value = IIf(txtInvestedArea.Text = "", 0, txtInvestedArea.Text)
            param(3) = New SqlParameter("@SECURITY_DEPOSIT", SqlDbType.Decimal)
            param(3).Value = IIf(txtpay.Text = "", 0, txtpay.Text)
            param(4) = New SqlParameter("@SECURITY_DEP_MONTHS", SqlDbType.Int)
            param(4).Value = Convert.ToInt32(ddlSecurityDepMonths.SelectedValue)
            param(5) = New SqlParameter("@RENT_FREE_PRD", SqlDbType.Int)
            param(5).Value = Convert.ToInt32(txtRentFreePeriod.Text)
            param(6) = New SqlParameter("@RENT_SFT_CARPET", SqlDbType.Decimal)
            param(6).Value = IIf(txtRentPerSqftCarpet.Text = "", 0, txtRentPerSqftCarpet.Text)
            param(7) = New SqlParameter("@RENT_SFT_BUA", SqlDbType.Decimal)
            param(7).Value = IIf(txtRentPerSqftBUA.Text = "", 0, txtRentPerSqftBUA.Text)
            param(8) = New SqlParameter("@INTERIORCOST", SqlDbType.Decimal)
            param(8).Value = IIf(txtInteriorCost.Text = "", 0, txtInteriorCost.Text)

            'Charges
            param(9) = New SqlParameter("@REGISTRATION_CHARGES", SqlDbType.Decimal)
            param(9).Value = IIf(txtregcharges.Text = "", 0, txtregcharges.Text)
            param(10) = New SqlParameter("@STAMP_DUTY", SqlDbType.Decimal)
            param(10).Value = IIf(txtsduty.Text = "", 0, txtsduty.Text)
            param(11) = New SqlParameter("@FURNITURE", SqlDbType.Decimal)
            param(11).Value = IIf(txtfurniture.Text = "", 0, txtfurniture.Text)
            param(12) = New SqlParameter("@PROFESSIONAL_FEES", SqlDbType.Decimal)
            param(12).Value = IIf(txtpfees.Text = "", 0, txtpfees.Text)
            param(13) = New SqlParameter("@MAINTENANCE_CHARGES", SqlDbType.Decimal)
            param(13).Value = IIf(txtmain1.Text = "", 0, txtmain1.Text)
            'param(14) = New SqlParameter("@SERVICE_TAX", SqlDbType.Decimal)
            'param(14).Value = IIf(txtservicetax.Text = "", 0, txtservicetax.Text)
            param(14) = New SqlParameter("@GST_TAX", SqlDbType.VarChar)
            param(14).Value = txtservicetax.SelectedValue
            param(15) = New SqlParameter("@PROPERTY_TAX", SqlDbType.Decimal)
            param(15).Value = IIf(txtproptax.Text = "", 0, txtproptax.Text)
            param(16) = New SqlParameter("@BROKERAGE_AMOUNT_CHARGE", SqlDbType.Decimal)
            param(16).Value = IIf(txtbrokerage.Text = "", 0, txtbrokerage.Text)

            'AGREEMENT DETAILS 
            param(17) = New SqlParameter("@EFF_DOA", SqlDbType.DateTime)
            param(17).Value = Convert.ToDateTime(txtsdate.Text)
            param(18) = New SqlParameter("@EXP_DOA", SqlDbType.DateTime)
            param(18).Value = Convert.ToDateTime(txtedate.Text)
            param(19) = New SqlParameter("@PPT_SNO", SqlDbType.Int)
            param(19).Value = ddlproperty.SelectedValue
            param(20) = New SqlParameter("@LOCKINPERIOD", SqlDbType.Int)
            param(20).Value = Convert.ToInt32(IIf(txtlock.Text = "", 0, txtlock.Text))
            param(21) = New SqlParameter("@LEASEPERIOD", SqlDbType.Int)
            param(21).Value = Convert.ToInt32(IIf(txtLeasePeiodinYears.Text = "", 0, txtLeasePeiodinYears.Text))
            param(22) = New SqlParameter("@NOTICEPERIOD", SqlDbType.Int)
            param(22).Value = Convert.ToInt32(IIf(txtNotiePeriod.Text = "", 0, txtNotiePeriod.Text))

            'BROKAGE DETAILS
            param(23) = New SqlParameter("@BROKERAGE_AMOUNT", SqlDbType.Decimal)
            param(23).Value = IIf(txtbrkamount.Text = "", 0, txtbrkamount.Text)
            param(24) = New SqlParameter("@BROKER_NAME", SqlDbType.VarChar)
            param(24).Value = txtbrkname.Text
            param(25) = New SqlParameter("@BROKER_ADDR", SqlDbType.VarChar)
            param(25).Value = txtbrkaddr.Text
            param(26) = New SqlParameter("@BROKER_PAN", SqlDbType.VarChar)
            param(26).Value = txtbrkpan.Text
            param(27) = New SqlParameter("@BROKER_EMAIL", SqlDbType.VarChar)
            param(27).Value = txtbrkremail.Text
            param(28) = New SqlParameter("@BROKER_CONTACT", SqlDbType.VarChar)
            param(28).Value = txtbrkmob.Text

            'UTILITY/POWER BACKUP DETAILS
            param(29) = New SqlParameter("@DGSET", SqlDbType.VarChar)
            param(29).Value = ddlDgSet.SelectedValue
            param(30) = New SqlParameter("@DGSET_COM_UNIT", SqlDbType.VarChar)
            param(30).Value = txtDgSetPerUnit.Text
            param(31) = New SqlParameter("@DGSET_LOCATION", SqlDbType.VarChar)
            param(31).Value = txtDgSetLocation.Text
            param(32) = New SqlParameter("@SPACE_SERVO_STAB", SqlDbType.VarChar)
            param(32).Value = txtSpaceServoStab.Text
            param(33) = New SqlParameter("@ELEC_METER", SqlDbType.VarChar)
            param(33).Value = ddlElectricalMeter.SelectedValue
            param(34) = New SqlParameter("@METER_LOC", SqlDbType.VarChar)
            param(34).Value = txtMeterLocation.Text
            param(35) = New SqlParameter("@EARTHING_PIT", SqlDbType.VarChar)
            param(35).Value = txtEarthingPit.Text
            param(36) = New SqlParameter("@AVAIL_POWER", SqlDbType.Float)
            param(36).Value = IIf(txtAvailablePower.Text = "", 0, txtAvailablePower.Text)
            param(37) = New SqlParameter("@ADDITIONAL_POWER", SqlDbType.Float)
            param(37).Value = IIf(txtAdditionalPowerKWA.Text = "", 0, txtAdditionalPowerKWA.Text)
            param(38) = New SqlParameter("@POWER_SPEC", SqlDbType.VarChar)
            param(38).Value = txtPowerSpecification.Text

            'OTHER SERVICES
            param(39) = New SqlParameter("@TWO_WHL_PARK", SqlDbType.Int)
            param(39).Value = Convert.ToInt32(txtNoOfTwoWheelerParking.Text)
            param(40) = New SqlParameter("@CAR_PARK", SqlDbType.Int)
            param(40).Value = Convert.ToInt32(txtNoOfCarsParking.Text)
            param(41) = New SqlParameter("@AIRPRT_DIST", SqlDbType.Float)
            param(41).Value = IIf(txtDistanceFromAirPort.Text = "", 0, txtDistanceFromAirPort.Text)
            param(42) = New SqlParameter("@RAIL_DIST", SqlDbType.Float)
            param(42).Value = IIf(txtDistanceFromRailwayStation.Text = "", 0, txtDistanceFromRailwayStation.Text)
            param(43) = New SqlParameter("@RAIL_BUSTOP", SqlDbType.Float)
            param(43).Value = IIf(txtDistanceFromBustop.Text = "", 0, txtDistanceFromBustop.Text)

            'LEASE ESCALATION DETAILS   
            param(44) = New SqlParameter("@OPTION_LEASE_ESC", SqlDbType.VarChar)
            param(44).Value = ddlesc.SelectedValue
            'param(45) = New SqlParameter("@ESC_TYPE", SqlDbType.VarChar)
            'param(45).Value = ddlLeaseEscType.SelectedValue
            'param(46) = New SqlParameter("@LEASEHOLD_IMPROVEMENTS", SqlDbType.VarChar)
            'param(46).Value = txtLeaseHoldImprovements.Text
            'param(47) = New SqlParameter("@LEASE_COMMENTS", SqlDbType.VarChar)
            'param(47).Value = txtComments.Text
            'param(48) = New SqlParameter("@DUE_DILIGENCE", SqlDbType.VarChar)
            'param(48).Value = ddlDueDilegence.SelectedValue
            Dim selectedItems As String = [String].Join(",", ReminderCheckList.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value))
            param(49) = New SqlParameter("@REMINDER_BEFORE", SqlDbType.VarChar)
            param(49).Value = selectedItems

            'Rent Revision
            param(50) = New SqlParameter("@TOTAL_RENT", SqlDbType.Decimal)
            param(50).Value = IIf(txttotalrent.Text = "", 0, txttotalrent.Text)
            param(51) = New SqlParameter("@TENURE", SqlDbType.VarChar)
            param(51).Value = ddlTenure.SelectedValue

            'Other Details
            param(52) = New SqlParameter("@COMPETE_VISCINITY", SqlDbType.VarChar)
            param(52).Value = txtCompetitorsVicinity.Text
            param(53) = New SqlParameter("@ROLLING_SHUTTER", SqlDbType.VarChar)
            param(53).Value = ddlRollingShutter.SelectedValue
            param(54) = New SqlParameter("@OFFCIE_EQUIPMENTS", SqlDbType.VarChar)
            param(54).Value = txtOfficeEquipments.Text


            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas
            Dim i As Int32 = 0
            Dim fileSize As Int64 = 0
            If fu1.PostedFiles IsNot Nothing Then
                Dim count = fu1.PostedFiles.Count
                While (i < count)
                    fileSize = fu1.PostedFiles(i).ContentLength + fileSize
                    i = i + 1
                End While
                If (fileSize > 20971520) Then
                    lblMsg.Text = "Upload files size should not be greater than 20 MB."
                    Exit Sub
                End If
                For Each File In fu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            param(55) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(55).Value = UtilityService.ConvertToDataTable(Imgclass)
            param(56) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(56).Value = Session("Uid").ToString
            param(57) = New SqlParameter("@COMPANYID", SqlDbType.Int)
            param(57).Value = Session("COMPANYID")
            param(58) = New SqlParameter("@PPT_CODE", SqlDbType.VarChar)
            param(58).Value = txtPropCode.Text
            param(59) = New SqlParameter("@POA_SIGNED", SqlDbType.VarChar)
            param(59).Value = ddlAgreementbyPOA.SelectedValue
            'param(60) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            'param(60).Value = lblLeaseReqId.Text

            'POA Details
            param(61) = New SqlParameter("@POA_NAME", SqlDbType.VarChar)
            param(61).Value = txtPOAName.Text
            param(62) = New SqlParameter("@POA_ADDRESS", SqlDbType.VarChar)
            param(62).Value = txtPOAAddress.Text
            param(63) = New SqlParameter("@POA_MOBILE", SqlDbType.VarChar)
            param(63).Value = txtPOAMobile.Text
            param(64) = New SqlParameter("@POA_EMAIL", SqlDbType.VarChar)
            param(64).Value = txtPOAEmail.Text

            'Rent Revision
            'Dim RR_lst As New List(Of RentRevision)()
            'Dim rr_obj As New RentRevision()
            'For Each i As RepeaterItem In rpRevision.Items
            '    rr_obj = New RentRevision()
            '    Dim txtRevision As TextBox = DirectCast(i.FindControl("txtRevision"), TextBox)
            '    Dim lblRevYear As Label = DirectCast(i.FindControl("lblRevYear"), Label)
            '    If txtRevision IsNot Nothing Then
            '        rr_obj.RR_Year = lblRevYear.Text
            '        rr_obj.RR_Percentage = IIf(txtRevision.Text = "", 0, txtRevision.Text)
            '        RR_lst.Add(rr_obj)
            '    End If
            'Next
            'param(65) = New SqlParameter("@REVISIONLIST", SqlDbType.Structured)
            'param(65).Value = UtilityService.ConvertToDataTable(RR_lst)


            param(65) = New SqlParameter("@LEASEEXPENSESLIST", SqlDbType.NVarChar)
            param(65).Value = ""
            param(66) = New SqlParameter("@COST_TYPE", SqlDbType.VarChar)
            param(66).Value = rblCostType.SelectedValue
            param(67) = New SqlParameter("@SEAT_COST", SqlDbType.Decimal)
            param(67).Value = IIf(txtSeatCost.Text = "", 0, txtSeatCost.Text)
            param(68) = New SqlParameter("@LEASE_STATUS", SqlDbType.VarChar)
            param(68).Value = ddlstatus.SelectedValue
            param(45) = New SqlParameter("@PM_LES_SNO", SqlDbType.Int)
            param(45).Value = Convert.ToInt32(hdnLSNO.Value)

            param(46) = New SqlParameter("@PM_LO_ADDI_PARK_CHRG", SqlDbType.Decimal)
            param(46).Value = IIf(txtadditionalparking.Text = "", 0, txtadditionalparking.Text)
            param(47) = New SqlParameter("@RENT_DATE", SqlDbType.DateTime)
            param(47).Value = Convert.ToDateTime(txtrentcommencementdate.Text)
            param(48) = New SqlParameter("@NO_OF_LL", SqlDbType.Int)
            param(48).Value = Convert.ToInt32(txtNoLanlords.Text)


            param(69) = New SqlParameter("@PM_LAD_START_DT_AGREEMENT", SqlDbType.DateTime)
            param(69).Value = Convert.ToDateTime(txtstartagreDT.Text)
            param(70) = New SqlParameter("@PM_LAD_LOCK_INPEDATE", SqlDbType.DateTime)
            'param(70).Value = Convert.ToDateTime(Txtlockinpeamonth.Text)
            param(70).Value = IIf(Txtlockinpeamonth.Text = "", DBNull.Value, Txtlockinpeamonth.Text)

            param(71) = New SqlParameter("@BUILTUP_AREA", SqlDbType.VarChar)
            param(71).Value = txtBuiltupArea.Text
            param(72) = New SqlParameter("@COMMON_AREA", SqlDbType.VarChar)
            param(72).Value = txtCommonArea.Text
            param(73) = New SqlParameter("@RENTABLE_AREA", SqlDbType.VarChar)
            param(73).Value = txtRentableArea.Text

            param(74) = New SqlParameter("@USABLE_AREA", SqlDbType.VarChar)
            param(74).Value = txtUsableArea.Text

            param(75) = New SqlParameter("@SUPER_BLT_AREA", SqlDbType.VarChar)
            param(75).Value = txtSuperBulArea.Text
            param(76) = New SqlParameter("@PLOT_AREA", SqlDbType.VarChar)
            param(76).Value = txtPlotArea.Text

            param(77) = New SqlParameter("@FTC_HIGHT", SqlDbType.VarChar)
            param(77).Value = txtCeilingHight.Text
            param(78) = New SqlParameter("@FTBB_HIGHT", SqlDbType.VarChar)
            param(78).Value = txtBeamBottomHight.Text


            param(79) = New SqlParameter("@MAX_CAPACITY", SqlDbType.VarChar)
            param(79).Value = txtMaxCapacity.Text
            param(80) = New SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.VarChar)
            param(80).Value = txtOptCapacity.Text

            param(81) = New SqlParameter("@SEATING_CAPACITY", SqlDbType.VarChar)
            param(81).Value = txtSeatingCapacity.Text
            param(82) = New SqlParameter("@FLR_TYPE", SqlDbType.VarChar)
            param(82).Value = ddlFlooringType.SelectedValue
            param(83) = New SqlParameter("@FSI", SqlDbType.VarChar)
            param(83).Value = ddlFSI.SelectedValue
            param(84) = New SqlParameter("@FSI_RATIO", SqlDbType.VarChar)
            param(84).Value = txtFSI.Text
            param(85) = New SqlParameter("@PFR_EFF", SqlDbType.VarChar)
            param(85).Value = txtEfficiency.Text
            param(86) = New SqlParameter("@CARPET_AREA", SqlDbType.VarChar)
            param(86).Value = txtCarpetArea.Text
            param(87) = New SqlParameter("@PROPERTY_TYPE", SqlDbType.VarChar)
            param(87).Value = ddlPropertyType.SelectedValue
            param(88) = New SqlParameter("@POA_LL_TYPE", SqlDbType.VarChar)
            param(88).Value = txtLLtype.Text
            param(89) = New SqlParameter("@RENEW_STATUS", SqlDbType.Int)
            param(89).Value = 1
            param(90) = New SqlParameter("@REMARKS", SqlDbType.VarChar)
            param(90).Value = txtRemarks.Text
            param(91) = New SqlParameter("@AMENTIPAID", SqlDbType.VarChar)
            param(91).Value = ddlamenpaid.SelectedValue
            param(92) = New SqlParameter("@MAINPAID", SqlDbType.VarChar)
            param(92).Value = ddlmaintpaid.SelectedValue

            'Dim sp5 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_ADD_LEASE_RENEWAL")
            'ds = sp5.GetDataSet
            Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "PM_ADD_LEASE_RENEWAL", param)

                While sdr.Read()
                    If sdr("result").ToString() = "SUCCESS" Then
                        'lblMsg.Text = "Request Suubmitted for Renewal of lease Successfully : " + lblLeaseReqId.Text
                        lblLeaseReqId.Text = ""
                        Session("ID") = sdr("ID").ToString()
                        Dim LESID = sdr("ID").ToString()
                        Session("REQ_ID") = sdr("REQUEST_ID").ToString()
                        'If (rblEscalationType.SelectedValue = "Lease") Then
                        '    Escalation()
                        'End If
                        panPOA.Visible = False
                        'ClearTextBox(Me)
                        'AssigTextValues()
                        'ClearDropdown(Me)
                        Landlord.Visible = True
                        btnAddNewLandlord.Visible = True
                        AddLeaseDetails.Visible = False
                        btnApproveLease.Visible = True
                        NoLanlords.Visible = True
                        nooflandlord.Text = txtNoLanlords.Text
                        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORD_DETAILS_BY_REQSNO")
                        sp.Command.AddParameter("@LM_SNO", Session("ID"), DbType.String)
                        Dim ds As New DataSet
                        ds = sp.GetDataSet
                        gvlandlordItems.DataSource = ds.Tables(0)
                        gvlandlordItems.DataBind()
                    Else
                        lblMsg.Text = "Something went wrong; try again"
                    End If
                End While
                sdr.Close()
            End Using

        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub btnApproveLease_click(sender As Object, e As EventArgs) Handles btnApproveLease.Click
        Try

            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LEASE_RENT_DETAILS")
            sp1.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
            sp1.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
            sp1.Command.AddParameter("@ID", Session("ID"), DbType.Int32)
            sp1.Command.AddParameter("@NOOFLANDORD", Convert.ToInt32(nooflandlord.Text), SqlDbType.Int)
            Dim ds As New DataSet
            ds = sp1.GetDataSet

            If ds.Tables(0).Rows(0).Item("LL_COUNT") <> Convert.ToInt32(nooflandlord.Text) Then
                lblMsg.Text = "Total number of Landlords entered " + ds.Tables(0).Rows(0).Item("LL_COUNT").ToString() + " is not matching with the No.Of Landlords : " + ViewState("NO_OF_LL").ToString()
                Exit Sub
            Else
                Dim TOT_LL_RENT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_RENT"))
                Dim TOT_LL_SEC_DEPOSIT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_SEC_DEPOSIT"))
                Dim TOT_LL_MAINT As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_MAINT"))
                Dim TOT_LL_AMENTIES As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("TOT_LL_AMENITIES"))

                If TOT_LL_RENT <> txtInvestedArea.Text Then
                    lblMsg.Text = "Landlords Total Rent Payable Should Match With Lease Basic Rent " + txtInvestedArea.Text.ToString()
                    Exit Sub
                End If
                If TOT_LL_SEC_DEPOSIT <> txtpay.Text Then
                    lblMsg.Text = "Landlords Total Security Deposit Should Match With Lease Total Security Deposit " + txtpay.Text.ToString()
                    Exit Sub
                End If
                If ddlmaintpaid.SelectedValue = "Company" Then
                    If TOT_LL_MAINT <> txtmain1.Text Then
                        lblMsg.Text = "Landlords' Total Maintenance Should Match the Lease Maintenance Amount " + txtmain1.Text.ToString()
                        Exit Sub
                    End If
                End If
                If ddlamenpaid.SelectedValue = "Company" Then
                    If TOT_LL_AMENTIES <> txtfurniture.Text Then
                        lblMsg.Text = "Landlords Total Amenities Should Match With Lease Amenities Amount " + txtfurniture.Text.ToString()
                        Exit Sub
                    End If
                End If
            End If
            Landlord.Visible = False
            btnAddNewLandlord.Visible = False
            btnApproveLease.Visible = False
            NoLanlords.Visible = False
            BindGrid()
            lblMsg.Text = "Request raised for Lease renewal : " + Session("REQ_ID")
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try

    End Sub
    Private Sub GetLandlords()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LANDLORDS")
        sp.Command.AddParameter("@COMPANY_ID", Session("COMPANYID"), DbType.Int32)
        sp.Command.AddParameter("@REQ_ID", Session("REQ_ID"), DbType.String)
        sp.Command.AddParameter("@ID", hdnLSNO.Value, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvlandlordItems.DataSource = ds
        gvlandlordItems.DataBind()

    End Sub
    Protected Sub txtRentFreePeriod_TextChanged(sender As Object, e As EventArgs) Handles txtRentFreePeriod.TextChanged
        Dim days As Integer
        Dim Date1 As Date = txtsdate.Text
        Dim Date2 As Date = DateTime.Now.ToString("MM/dd/yyyy")
        days = DateDiff("d", Date1, Date2)

        If txtRentFreePeriod.Text <> "" Then
            txtrentcommencementdate.Text = DateTime.Now.AddDays(days + txtRentFreePeriod.Text).ToString("MM/dd/yyyy")
        Else
            txtrentcommencementdate.Text = DateTime.Now.ToString("MM/dd/yyyy")
        End If

    End Sub
    'Public Sub TextBox1_TextChanged()
    '    BindRentRevisiondtflex()
    '    For Each item As RepeaterItem In Repeater2.Items
    '        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '            Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '            lbl.Visible = True
    '            lbtxt.Visible = False
    '        End If
    '    Next
    'End Sub
    'Public Sub ddlDay_TextChanged()
    '    BindRentRevisiondtflex()
    '    For Each item As RepeaterItem In Repeater2.Items
    '        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '            Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '            lbl.Visible = True
    '            lbtxt.Visible = False
    '        End If
    '    Next
    'End Sub
    'Public Sub ddlMonth_TextChanged()
    '    BindRentRevisiondtflex()
    '    For Each item As RepeaterItem In Repeater2.Items
    '        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '            Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '            lbl.Visible = True
    '            lbtxt.Visible = False
    '        End If
    '    Next
    'End Sub
    'Public Sub ddlYear_TextChanged()
    '    BindRentRevisiondtflex()
    '    For Each item As RepeaterItem In Repeater2.Items
    '        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '            Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '            lbl.Visible = True
    '            lbtxt.Visible = False
    '        End If
    '    Next
    'End Sub
    Protected Sub txtrentcommencementdate_TextChanged(sender As Object, e As EventArgs) Handles txtrentcommencementdate.TextChanged
        Dim days As Integer

        Dim Date1 As Date = txtsdate.Text
        Dim Date2 As Date = txtrentcommencementdate.Text
        days = DateDiff("d", Date1, Date2)


        txtRentFreePeriod.Text = days

    End Sub

    'Private Sub ddlintervaltype_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlintervaltype.SelectedIndexChanged
    '    If ddlintervaltype.SelectedValue = "Flexy" Then
    '        Escflex.Visible = True
    '        Escduration.Visible = False
    '        RentRevisionPanel.Visible = False
    '        txtnofEscltions.Text = ""
    '    Else
    '        Escflex.Visible = False
    '        Escduration.Visible = True
    '        RentRevisionPanel.Visible = False
    '        txtIntervaldu.Text = ""
    '    End If
    'End Sub

    'Private Sub ddlesc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlesc.SelectedIndexChanged
    '    If ddlesc.SelectedValue = "Yes" Then
    '        EscalationType.Visible = True
    '    Else ddlesc.SelectedValue = "No"
    '        EscalationType.Visible = False
    '        EscalationType1.Visible = False
    '        EscalationType2.Visible = False
    '    End If
    'End Sub

    'Private Sub rblEscalationType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rblEscalationType.SelectedIndexChanged
    '    If rblEscalationType.SelectedValue = "Lease" Then
    '        EscalationType1.Visible = True
    '        EscalationType2.Visible = True
    '        txtIntervaldu.Text = ""
    '        txtnofEscltions.Text = ""
    '    ElseIf rblEscalationType.SelectedValue = "LandLoard" Then
    '        EscalationType1.Visible = False
    '        EscalationType2.Visible = False
    '        RentRevisionPanel.Visible = False
    '    End If
    'End Sub
    'Public Sub txtnofEscltions_TextChanged(sender As Object, e As EventArgs)
    '    BindRentRevisiondtflex()
    '    For Each item As RepeaterItem In Repeater2.Items
    '        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '            Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '            lbl.Visible = False
    '            lbtxt.Visible = False
    '        End If
    '    Next
    'End Sub
    'Public Sub BindRentRevisiondtflex()
    '    If txtnofEscltions.Text <> "" Then
    '        Dim RR_lst As New List(Of RentRevision)()
    '        Dim rr_obj As New RentRevision()
    '        Dim SubEscDATE As DateTime = txtsdate.Text
    '        For i As Integer = 1 To Convert.ToInt32(txtnofEscltions.Text)
    '            rr_obj = New RentRevision()
    '            d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtnofEscltions.Text))
    '            'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '            'rr_obj.RR_Year = d
    '            txtInterval = d
    '            rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(i) + ":"
    '            rr_obj.RR_Percentage = "0"
    '            rr_obj.pm_les_esc_id = ""
    '            'i = i + 1
    '            'rr_obj.
    '            For Each item As RepeaterItem In Repeater2.Items
    '                If (item.ItemIndex = i - 1) Then
    '                    If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '                        Dim tect = CType(item.FindControl("TextBox1"), TextBox)
    '                        ' If (tect.Text <> 0) Then
    '                        'Dim tectr = CType(item.FindControl("TextBox1"), TextBox).Text
    '                        Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '                        Dim lblYear = IIf(CType(item.FindControl("ddlYear"), TextBox).Text = "", 0, CType(item.FindControl("ddlYear"), TextBox).Text)
    '                        Dim lblMonth = IIf(CType(item.FindControl("ddlMonth"), TextBox).Text = "", 0, CType(item.FindControl("ddlMonth"), TextBox).Text)
    '                        Dim lblDay = IIf(CType(item.FindControl("ddlDay"), TextBox).Text = "", 0, CType(item.FindControl("ddlDay"), TextBox).Text)
    '                        SubEscDATE = Convert.ToDateTime(SubEscDATE).AddYears(Convert.ToInt32(lblYear))
    '                        SubEscDATE = Convert.ToDateTime(SubEscDATE).AddMonths(Convert.ToInt32(lblMonth))
    '                        SubEscDATE = Convert.ToDateTime(SubEscDATE).AddDays(Convert.ToInt32(lblDay))
    '                        rr_obj.RR_Year = SubEscDATE
    '                        rr_obj.RR_YEARS = lblYear
    '                        rr_obj.RR_MONTHS = lblMonth
    '                        rr_obj.RR_DAYS = lblDay
    '                        rr_obj.RR_Percentage = tect.Text
    '                        lbl.Visible = True
    '                        'Else
    '                        '    rr_obj.RR_Year = ""
    '                        'End If
    '                    End If
    '                End If
    '            Next
    '            RR_lst.Add(rr_obj)
    '        Next
    '        Repeater2.DataSource = RR_lst
    '        Repeater2.DataBind()
    '        Session("escdt") = SubEscDATE
    '        If (SubEscDATE >= Convert.ToDateTime(txtedate.Text)) Then
    '            lblmesg.Text = "The Escalation Date must be earlier than the Expired Date"
    '        Else
    '            lblmesg.Text = ""
    '        End If
    '        'RentRevisionPanelflex.Visible = True
    '        RentRevisionPanel.Visible = True
    '    End If
    'End Sub
    'Public Sub txtIntervaldu_TextChanged(sender As Object, e As EventArgs)
    '    BindRentRevisiondt()
    '    For Each item As RepeaterItem In Repeater2.Items
    '        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '            'Dim lbl = CType(item.FindControl("lblEscno"), Label)
    '            Dim lbtxt = CType(item.FindControl("ddlYear"), TextBox)
    '            Dim lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
    '            Dim lbday = CType(item.FindControl("ddlDay"), TextBox)
    '            Dim lbesc = CType(item.FindControl("TextBox1"), TextBox)
    '            Dim lbyr = CType(item.FindControl("lbyear"), Label)
    '            Dim lbmth = CType(item.FindControl("lbmnt"), Label)
    '            Dim lbdy = CType(item.FindControl("lbldy"), Label)
    '            Dim lble = CType(item.FindControl("lbldf"), Label)
    '            'lbl.Visible = False
    '            lbtxt.Visible = False
    '            lbmnth.Visible = False
    '            lbesc.Visible = False
    '            lbday.Visible = False
    '            lbyr.Visible = False
    '            lbmth.Visible = False
    '            lbdy.Visible = False
    '            lble.Visible = False
    '        End If
    '    Next
    'End Sub

    Dim rowCount As Integer = 0
    Dim Duration As Integer = 0
    Dim d As DateTime
    Dim txtInterval As String
    Dim FinalRent As Integer = 0
    Dim LandloardFinalRent As Integer = 0

    'Public Sub BindRentRevisiondt()
    '    lblmesg.Text = ""
    '    If txtIntervaldu.Text <> "" Then

    '        If ddlintervaltype.SelectedValue = "Yearly" Then
    '            rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
    '            txtInterval = txtsdate.Text
    '            Duration = rowCount / Convert.ToInt32(txtIntervaldu.Text)
    '            If Duration >= 1 Then
    '                'RentRevisionPanelflex.Visible = False
    '                RentRevisionPanel.Visible = True
    '                Dim RR_lst As New List(Of RentRevision)()
    '                Dim rr_obj As New RentRevision()
    '                If Convert.ToInt32(txtIntervaldu.Text) > rowCount Then
    '                    lblmesg.Text = "Interval Duration should be in between Lease Agreement Dates"
    '                End If
    '                Dim j As Integer = 1
    '                For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To rowCount
    '                    rr_obj = New RentRevision()
    '                    d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtIntervaldu.Text))
    '                    'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '                    If (d <= DateTime.Parse(txtedate.Text)) Then
    '                        rr_obj.RR_Year = d
    '                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(j) + ":"
    '                        txtInterval = d
    '                        rr_obj.RR_Percentage = "0"
    '                        rr_obj.pm_les_esc_id = ""
    '                        For Each item As RepeaterItem In Repeater2.Items
    '                            If (item.ItemIndex = j - 1) Then
    '                                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '                                    Dim tect = IIf(CType(item.FindControl("txtRevision"), TextBox).Text = "", 0, CType(item.FindControl("txtRevision"), TextBox).Text)
    '                                    'If (tect <> 0) Then
    '                                    'd = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtIntervaldu.Text))
    '                                    'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '                                    Dim lbl = CType(item.FindControl("lblRevYear"), Label).Text
    '                                    Dim revs = CType(item.FindControl("txtRevision"), TextBox).Text
    '                                    If (d <= System.DateTime.Now()) Then
    '                                        rr_obj.RR_Year = lbl
    '                                        rr_obj.RR_Percentage = revs
    '                                        rr_obj.validat = 1
    '                                    Else
    '                                        rr_obj.RR_Year = d
    '                                    End If
    '                                    'Else
    '                                    'rr_obj.RR_Year = ""
    '                                    'End If
    '                                End If
    '                            End If
    '                        Next
    '                        i = i + (Convert.ToInt32(txtIntervaldu.Text) - 1)
    '                        j = j + 1
    '                        RR_lst.Add(rr_obj)
    '                    End If
    '                Next
    '                Repeater2.DataSource = RR_lst
    '                Repeater2.DataBind()
    '                Dim coun As Integer = 0
    '                For Each item As RepeaterItem In Repeater2.Items
    '                    Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '                    If RR_lst(coun).validat = 1 Then
    '                        lbtxt.ReadOnly = True
    '                    End If
    '                    coun = coun + 1
    '                Next
    '            Else
    '                'RentRevisionPanelflex.Visible = False
    '                RentRevisionPanel.Visible = False
    '                If Convert.ToInt32(txtIntervaldu.Text) > rowCount Then
    '                    lblmesg.Text = "Interval Duration should be in between Lease Agreement Dates"
    '                End If
    '            End If
    '        Else ddlintervaltype.SelectedValue = "Monthly"
    '            Dim rowCount As Integer = 0
    '            Dim months1 As Integer = (DateTime.Parse(txtedate.Text).Year * 12 + DateTime.Parse(txtedate.Text).Month) - (DateTime.Parse(txtsdate.Text).Year * 12 + DateTime.Parse(txtsdate.Text).Month)
    '            Dim j As Integer = 1
    '            txtInterval = txtsdate.Text
    '            Dim Duration As Integer = 0
    '            Duration = months1 / Convert.ToInt32(txtIntervaldu.Text)
    '            If Convert.ToInt32(txtIntervaldu.Text) > months1 Then
    '                lblmesg.Text = "Interval Duration should be in between Lease Agreement Dates"
    '            End If
    '            If Duration >= 1 Then
    '                'RentRevisionPanelflex.Visible = False
    '                RentRevisionPanel.Visible = True
    '                Dim RR_lst As New List(Of RentRevision)()
    '                Dim rr_obj As New RentRevision()
    '                ''For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)
    '                For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1
    '                    d = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtIntervaldu.Text))
    '                    rr_obj = New RentRevision()
    '                    If (d <= DateTime.Parse(txtedate.Text)) Then
    '                        rr_obj.RR_Year = d
    '                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(j) + ":"
    '                        txtInterval = d
    '                        rr_obj.RR_Percentage = "0"
    '                        rr_obj.pm_les_esc_id = ""
    '                        For Each item As RepeaterItem In Repeater2.Items
    '                            If (item.ItemIndex = j - 1) Then
    '                                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '                                    Dim tect = IIf(CType(item.FindControl("txtRevision"), TextBox).Text = "", 0, CType(item.FindControl("txtRevision"), TextBox).Text)
    '                                    'If (tect <> 0) Then
    '                                    'd = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtIntervaldu.Text))
    '                                    'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '                                    Dim lbl = CType(item.FindControl("lblRevYear"), Label).Text
    '                                    Dim revs = CType(item.FindControl("txtRevision"), TextBox).Text
    '                                    If (d <= System.DateTime.Now()) Then
    '                                        rr_obj.RR_Year = lbl
    '                                        rr_obj.RR_Percentage = revs
    '                                        If (lbl <= System.DateTime.Now()) Then
    '                                            rr_obj.validat = 1
    '                                        End If
    '                                    Else
    '                                        rr_obj.RR_Year = d
    '                                    End If
    '                                    'Else
    '                                    'rr_obj.RR_Year = ""
    '                                    'End If
    '                                End If
    '                            End If
    '                        Next
    '                        i = i + (Convert.ToInt32(txtIntervaldu.Text) - 1)
    '                        j = j + 1
    '                        RR_lst.Add(rr_obj)
    '                    End If
    '                Next
    '                Repeater2.DataSource = RR_lst
    '                Repeater2.DataBind()
    '            Else
    '                'RentRevisionPanelflex.Visible = False
    '                RentRevisionPanel.Visible = False
    '            End If
    '        End If
    '    End If
    'End Sub
    'Public Sub txtnoescduration_TextChanged(sender As Object, e As EventArgs)
    '    BindRentLLrevisionflex()
    '    For Each item As RepeaterItem In Repeater1.Items
    '        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '            Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '            Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '            lbl.Visible = False
    '            lbtxt.Visible = False
    '        End If
    '    Next
    'End Sub
    'Public Sub txtllescdurtn_TextChanged(sender As Object, e As EventArgs)
    '    BindRentLLRevisiondt()
    '    For Each item As RepeaterItem In Repeater1.Items
    '        If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '            'Dim lbl = CType(item.FindControl("lblEscno"), Label)
    '            Dim lbtxt = CType(item.FindControl("ddlYear"), TextBox)
    '            Dim lbmnth = CType(item.FindControl("ddlMonth"), TextBox)
    '            Dim lbday = CType(item.FindControl("ddlDay"), TextBox)
    '            Dim lbesc = CType(item.FindControl("TextBox1"), TextBox)
    '            Dim lbyr = CType(item.FindControl("lbyear"), Label)
    '            Dim lbmth = CType(item.FindControl("lbmnt"), Label)
    '            Dim lbdy = CType(item.FindControl("lbldy"), Label)
    '            Dim lble = CType(item.FindControl("lbldf"), Label)
    '            'lbl.Visible = False
    '            lbtxt.Visible = False
    '            lbmnth.Visible = False
    '            lbesc.Visible = False
    '            lbday.Visible = False
    '            lbyr.Visible = False
    '            lbmth.Visible = False
    '            lbdy.Visible = False
    '            lble.Visible = False
    '        End If
    '    Next
    'End Sub
    'Public Sub BindRentLLrevisionflex()
    '    LandloardRentRevisionPanel.Visible = True
    '    lblmesg.Text = ""
    '    If txtnoescduration.Text <> "" Then
    '        Dim RR_lst As New List(Of RentRevision)()
    '        Dim rr_obj As New RentRevision()
    '        Dim SubEscDATE As DateTime = txtsdate.Text
    '        For i As Integer = 1 To Convert.ToInt32(txtnoescduration.Text)

    '            rr_obj = New RentRevision()
    '            d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtnoescduration.Text))
    '            'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '            rr_obj.RR_Year = d
    '            rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(i) + ":"
    '            txtInterval = d
    '            rr_obj.RR_Percentage = "0"
    '            rr_obj.pm_les_esc_id = ""
    '            'i = i + 1
    '            'rr_obj.
    '            For Each item As RepeaterItem In Repeater2.Items
    '                If (item.ItemIndex = i - 1) Then
    '                    If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '                        Dim tect = CType(item.FindControl("TextBox1"), TextBox)
    '                        ' If (tect.Text <> 0) Then
    '                        'Dim tectr = CType(item.FindControl("TextBox1"), TextBox).Text
    '                        Dim lbl = CType(item.FindControl("lblRevYear"), Label)
    '                        Dim lblYear = IIf(CType(item.FindControl("ddlYear"), TextBox).Text = "", 0, CType(item.FindControl("ddlYear"), TextBox).Text)
    '                        Dim lblMonth = IIf(CType(item.FindControl("ddlMonth"), TextBox).Text = "", 0, CType(item.FindControl("ddlMonth"), TextBox).Text)
    '                        Dim lblDay = IIf(CType(item.FindControl("ddlDay"), TextBox).Text = "", 0, CType(item.FindControl("ddlDay"), TextBox).Text)
    '                        SubEscDATE = Convert.ToDateTime(SubEscDATE).AddYears(Convert.ToInt32(lblYear))
    '                        SubEscDATE = Convert.ToDateTime(SubEscDATE).AddMonths(Convert.ToInt32(lblMonth))
    '                        SubEscDATE = Convert.ToDateTime(SubEscDATE).AddDays(Convert.ToInt32(lblDay))
    '                        rr_obj.RR_Year = SubEscDATE
    '                        rr_obj.RR_YEARS = lblYear
    '                        rr_obj.RR_MONTHS = lblMonth
    '                        rr_obj.RR_DAYS = lblDay
    '                        rr_obj.RR_Percentage = tect.Text
    '                        lbl.Visible = True
    '                        'Else
    '                        '    rr_obj.RR_Year = ""
    '                        'End If
    '                    End If
    '                End If
    '            Next
    '            RR_lst.Add(rr_obj)
    '        Next
    '        Repeater1.DataSource = RR_lst
    '        Repeater1.DataBind()
    '        Session("escdt") = SubEscDATE
    '        'If (SubEscDATE >= Convert.ToDateTime(txtedate.Text)) Then
    '        '    lblmesg.Text = "The Escalation Date must be earlier than the Expired Date"
    '        'Else
    '        '    lblmesg.Text = ""
    '        'End If
    '        LandloardRentRevisionPanel.Visible = True
    '    End If
    'End Sub
    'Public Sub BindRentLLRevisiondt()
    '    If txtllescdurtn.Text <> "" Then
    '        If ddllandloardinterval.SelectedValue = "Yearly" Then
    '            rowCount = DateTime.Parse(txtedate.Text).Year - DateTime.Parse(txtsdate.Text).Year
    '            txtInterval = txtsdate.Text
    '            Duration = rowCount / Convert.ToInt32(txtllescdurtn.Text)
    '            If Duration >= 1 Then
    '                LandloardRentRevisionPanel.Visible = True
    '                Dim RR_lst As New List(Of RentRevision)()
    '                Dim rr_obj As New RentRevision()
    '                Dim j As Integer = 1
    '                For i As Integer = Convert.ToInt32(txtllescdurtn.Text) To rowCount
    '                    rr_obj = New RentRevision()
    '                    d = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtllescdurtn.Text))
    '                    ''rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '                    If (d <= DateTime.Parse(txtedate.Text)) Then
    '                        rr_obj.RR_Year = d
    '                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(j) + ":"
    '                        txtInterval = d
    '                        rr_obj.RR_Percentage = "0"
    '                        rr_obj.pm_les_esc_id = ""
    '                        For Each item As RepeaterItem In Repeater1.Items
    '                            If (item.ItemIndex = j - 1) Then
    '                                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '                                    Dim tect = IIf(CType(item.FindControl("txtRevision"), TextBox).Text = "", 0, CType(item.FindControl("txtRevision"), TextBox).Text)
    '                                    'If (tect <> 0) Then
    '                                    'd = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtIntervaldu.Text))
    '                                    'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '                                    Dim lbl = CType(item.FindControl("lblRevYear"), Label).Text
    '                                    Dim revs = CType(item.FindControl("txtRevision"), TextBox).Text
    '                                    If (d <= System.DateTime.Now()) Then
    '                                        rr_obj.RR_Year = lbl
    '                                        rr_obj.RR_Percentage = revs
    '                                        rr_obj.validat = 1
    '                                    Else
    '                                        rr_obj.RR_Year = d
    '                                    End If
    '                                    'Else
    '                                    'rr_obj.RR_Year = ""
    '                                    'End If
    '                                End If
    '                            End If
    '                        Next
    '                        i = i + (Convert.ToInt32(txtllescdurtn.Text) - 1)
    '                        j = j + 1
    '                        RR_lst.Add(rr_obj)
    '                    End If
    '                Next
    '                Repeater1.DataSource = RR_lst
    '                Repeater1.DataBind()
    '                Dim coun As Integer = 0
    '                For Each item As RepeaterItem In Repeater1.Items
    '                    Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '                    If RR_lst(coun).validat = 1 Then
    '                        lbtxt.ReadOnly = True
    '                    End If
    '                    coun = coun + 1
    '                Next
    '            Else
    '                LandloardRentRevisionPanel.Visible = False
    '            End If
    '        Else ddllandloardinterval.SelectedValue = "Monthly"
    '            Dim rowCount As Integer = 0
    '            Dim months1 As Integer = (DateTime.Parse(txtedate.Text).Year * 12 + DateTime.Parse(txtedate.Text).Month) - (DateTime.Parse(txtsdate.Text).Year * 12 + DateTime.Parse(txtsdate.Text).Month)

    '            txtInterval = txtsdate.Text
    '            Dim Duration As Integer = 0
    '            Dim j As Integer = 1
    '            Duration = months1 / Convert.ToInt32(txtllescdurtn.Text)
    '            If Duration >= 1 Then
    '                LandloardRentRevisionPanel.Visible = True
    '                Dim RR_lst As New List(Of RentRevision)()
    '                Dim rr_obj As New RentRevision()
    '                ''For i As Integer = Convert.ToInt32(txtIntervaldu.Text) To months1 - Convert.ToInt32(txtIntervaldu.Text)
    '                For i As Integer = Convert.ToInt32(txtllescdurtn.Text) To months1
    '                    'rr_obj = New RentRevision()
    '                    d = Convert.ToDateTime(txtInterval).AddMonths(Convert.ToInt32(txtllescdurtn.Text))
    '                    rr_obj = New RentRevision()
    '                    If (d <= DateTime.Parse(txtedate.Text)) Then
    '                        rr_obj.RR_Year = d
    '                        rr_obj.RR_NOESC = "<strong>Escalation </strong>" + Convert.ToString(j) + ":"
    '                        txtInterval = d
    '                        rr_obj.RR_Percentage = "0"
    '                        rr_obj.pm_les_esc_id = ""
    '                        For Each item As RepeaterItem In Repeater1.Items
    '                            If (item.ItemIndex = j - 1) Then
    '                                If item.ItemType = ListItemType.Item OrElse item.ItemType = ListItemType.AlternatingItem Then
    '                                    Dim tect = IIf(CType(item.FindControl("txtRevision"), TextBox).Text = "", 0, CType(item.FindControl("txtRevision"), TextBox).Text)
    '                                    'If (tect <> 0) Then
    '                                    'd = Convert.ToDateTime(txtInterval).AddYears(Convert.ToInt32(txtIntervaldu.Text))
    '                                    'rr_obj.RR_Year = DateTime.Parse(txtsdate.Text).AddYears(i).Year
    '                                    Dim lbl = CType(item.FindControl("lblRevYear"), Label).Text
    '                                    Dim revs = CType(item.FindControl("txtRevision"), TextBox).Text
    '                                    If (d <= System.DateTime.Now()) Then
    '                                        rr_obj.RR_Year = lbl
    '                                        rr_obj.RR_Percentage = revs
    '                                        If (lbl <= System.DateTime.Now()) Then
    '                                            rr_obj.validat = 1
    '                                        End If
    '                                    Else
    '                                        rr_obj.RR_Year = d
    '                                    End If
    '                                    'Else
    '                                    'rr_obj.RR_Year = ""
    '                                    'End If
    '                                End If
    '                            End If
    '                        Next
    '                        i = i + (Convert.ToInt32(txtllescdurtn.Text) - 1)
    '                        j = j + 1
    '                        RR_lst.Add(rr_obj)
    '                    End If
    '                Next
    '                Repeater1.DataSource = RR_lst
    '                Repeater1.DataBind()
    '                Dim coun As Integer = 0
    '                For Each item As RepeaterItem In Repeater1.Items
    '                    Dim lbtxt = CType(item.FindControl("txtRevision"), TextBox)
    '                    If RR_lst(coun).validat = 1 Then
    '                        lbtxt.ReadOnly = True
    '                    End If
    '                    coun = coun + 1
    '                Next
    '            Else
    '                LandloardRentRevisionPanel.Visible = False
    '            End If
    '        End If
    '    End If
    'End Sub

    Protected Sub ddltds_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddltds.SelectedIndexChanged

        '' TDSDIV.Visible = IIf(ddltds.SelectedValue = "1", True, False)
        If ddltds.SelectedValue = "1" Then
            TDSDIV.Visible = True
        Else
            TDSDIV.Visible = False
            txttdsper.Text = 0
        End If
    End Sub

    Public Sub BindLLDocuments()
        Dim dtDocs As New DataTable("Documents")
        param = New SqlParameter(1) {}
        param(0) = New SqlParameter("@HDN_LL_VALUE", SqlDbType.NVarChar, 50)
        param(0).Value = hdnLandlordSNO.Value
        param(1) = New SqlParameter("@COMPANY_ID", SqlDbType.Int)
        param(1).Value = Session("COMPANYID")
        Dim ds As New DataSet
        ds = ObjSubSonic.GetSubSonicDataSet("PM_GET_LEASE_LL_DOCS", param)
        If ds.Tables(0).Rows.Count > 0 Then
            lldocgrid.DataSource = ds
            lldocgrid.DataBind()
            Div3.Visible = True
            lblmesge.Text = ""
        Else
            Div3.Visible = False
            lblMsg.Visible = True
            lblmesge.Text = "No Documents Available"
        End If
        dtDocs = Nothing
    End Sub

    Private Sub lldocgrid_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lldocgrid.DeleteCommand
        If lldocgrid.EditItemIndex = -1 Then
            Dim Bid As Integer
            Bid = lldocgrid.DataKeys(e.Item.ItemIndex)
            param = New SqlParameter(0) {}
            param(0) = New SqlParameter("@PM_LL_DOC_SNO", SqlDbType.NVarChar, 50)
            param(0).Value = Bid
            ObjSubSonic.GetSubSonicDataSet("PM_DELETE_LEASE_LL_DOCS", param)
            BindLLDocuments()
        End If
    End Sub

    Private Sub lldocgrid_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles lldocgrid.ItemCommand
        If e.CommandName = "Download" Then
            Dim pm_ldoc_sno = lldocgrid.DataKeys(e.Item.ItemIndex)
            e.Item.BackColor = Drawing.Color.LightSteelBlue
            Dim filePath As String
            filePath = lldocgrid.Items(e.Item.ItemIndex).Cells(1).Text
            filePath = "~\Images\Property_Images\" & filePath
            Response.ContentType = "application/CSV"
            Response.AddHeader("Content-Disposition", "attachment;filename=""" & Replace(filePath, "~\Images\Property_Images", "") & """")
            Response.TransmitFile(Server.MapPath(filePath))
            Response.[End]()
        End If
    End Sub
End Class