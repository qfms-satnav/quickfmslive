Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Partial Class WorkSpace_SMS_Webfiles_frmUpdateWorkRequest
    Inherits System.Web.UI.Page
    Public Sub Clear()
        txtStartDate.Text = ""
        txtExpiryDate.Text = ""
        txtpamount.Text = ""
        txtoamount.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtRemarks.Text = ""
        'ddlstatus.SelectedIndex = -1
        ddlwstatus.SelectedIndex = -1
        txtcity.Text = ""
        txtproptype.Text = ""
        txtProperty.Text = ""
    End Sub
    Public Sub fillgrid()

        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_WORKREQ1_DETAILS")
        sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
        sp.Command.AddParameter("@COMPANY", HttpContext.Current.Session("COMPANYID"), DbType.Int32)
        gvwrequest.DataSource = sp.GetDataSet()
        gvwrequest.DataBind()
        For i As Integer = 0 To gvwrequest.Rows.Count - 1
            Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblstatus"), Label)
            If lblstatus.Text = "5013" Then
                lblstatus.Text = "Requested"
            ElseIf lblstatus.Text = "5014" Then
                lblstatus.Text = "Approved"
            Else
                lblstatus.Text = "5015"
                lblstatus.Text = "Rejected"

            End If
        Next

    End Sub
    Public Sub Cleardata()
        getrequest()
        ddlWorkRequest.SelectedValue = 0
        txtStartDate.Text = ""
        txtExpiryDate.Text = ""
        txtpamount.Text = ""
        txtoamount.Text = ""
        txtWorkTitle.Text = ""
        txtWorkSpec.Text = ""
        txtamount.Text = ""
        txtRemarks.Text = ""
        ddlwstatus.SelectedIndex = 0
    End Sub
    Public Sub getrequest()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_APPROVED_REQ")
        sp.Command.AddParameter("@User", Session("uid"), DbType.String)
        sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.Int32)
        ddlWorkRequest.DataSource = sp.GetDataSet()
        ddlWorkRequest.DataTextField = "PN_WORKREQUEST_REQ"
        ddlWorkRequest.DataBind()
        ddlWorkRequest.Items.Insert(0, New ListItem("--Select WorkRequest--", "0"))
    End Sub
    Public Sub GetWorkStatus()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_WORK_STATUS")
        ddlwstatus.DataSource = sp.GetDataSet()
        ddlwstatus.DataTextField = "STA_NAME"
        ddlwstatus.DataValueField = "STA_CODE"
        ddlwstatus.DataBind()
        ddlwstatus.Items.Insert(0, New ListItem("--Select Work Status--", "0"))
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        If Not IsPostBack Then
            getrequest()
            GetWorkStatus()
        End If
        txtStartDate.Attributes.Add("readonly", "readonly")
        txtExpiryDate.Attributes.Add("readonly", "readonly")
    End Sub
    Protected Sub txtpamount_TextChanged(sender As Object, e As EventArgs) Handles txtpamount.TextChanged
        If txtpamount.Text <> String.Empty Then
            lblmsg.Text = String.Empty
            Dim totalPaid As Decimal = 0
            If IsNumeric(txtpamount.Text) Then
                If gvwrequest.Rows.Count > 0 Then
                    For i = 0 To gvwrequest.Rows.Count - 1
                        Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblpamount"), Label)
                        totalPaid = totalPaid + CDec(lblstatus.Text)
                    Next
                    If (totalPaid + txtpamount.Text) <= txtamount.Text Then
                        txtoamount.Text = txtamount.Text - (totalPaid + txtpamount.Text)
                        txtoamount.ReadOnly = True
                        'If CInt(txtamount.Text) >= CInt(txtpamount.Text) Then
                        'txtoamount.Text = (CInt(txtamount.Text) - CInt(txtpamount.Text)).ToString()
                    Else
                        lblmsg.Text = "The Paid amount should be less than the Estimated amount"
                        lblmsg.Visible = True
                    End If
                Else
                    If CDec(txtamount.Text) >= CDec(txtpamount.Text) Then
                        txtoamount.Text = (CDec(txtamount.Text) - CDec(txtpamount.Text)).ToString()
                        txtoamount.ReadOnly = True
                    End If
                End If
            Else
                lblmsg.Text = "Enter numeric in Paid amount"
                lblmsg.Visible = True
            End If
        End If
        If txtoamount.Text = "0.00" Then
            GetWorkStatus()
            ddlwstatus.Items.FindByValue(3).Selected = True
        Else
            ddlwstatus.SelectedIndex = -1
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim totalPaid As Decimal = 0
        If (ddlwstatus.SelectedValue = 2) Then
            If gvwrequest.Rows.Count > 0 Then
                For i = 0 To gvwrequest.Rows.Count - 1
                    Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblpamount"), Label)
                    totalPaid = totalPaid + CDec(lblstatus.Text)
                Next
                If (CDec(totalPaid + txtpamount.Text)).Equals(CDec(txtamount.Text)) Then
                Else
                    lblmsg.Text = "Total paid amount Should be Equal to Estimated Amount"
                    lblmsg.Visible = True
                    Return
                End If
            Else
                If (CDec(txtpamount.Text)).Equals(CDec(txtamount.Text)) Then
                Else
                    lblmsg.Text = "Total paid amount Should be Equal to Estimated Amount"
                    lblmsg.Visible = True
                    Return
                End If
            End If
        Else
            If gvwrequest.Rows.Count > 0 Then
                For i = 0 To gvwrequest.Rows.Count - 1
                    Dim lblstatus As Label = CType(gvwrequest.Rows(i).FindControl("lblpamount"), Label)
                    totalPaid = totalPaid + CDec(lblstatus.Text)
                Next
                If (CDec(totalPaid + txtpamount.Text)) > (CDec(txtamount.Text)) Then

                    lblmsg.Text = "Total paid amount Should be Less than or Equal to Estimated Amount"
                    lblmsg.Visible = True
                    Return
                End If
            Else
                If (CDec(txtpamount.Text)) > (CDec(txtamount.Text)) Then
                    lblmsg.Text = "Total paid amount Should be Less than or Equal to Estimated Amount"
                    lblmsg.Visible = True
                    Return
                End If
            End If
        End If
        If txtStartDate.Text > txtExpiryDate.Text Then
            lblmsg.Text = "End Date Must Be Later Than Start Date"
        Else
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_UPDATE_WORKREQ")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            sp.Command.AddParameter("@START_DATE", txtStartDate.Text, DbType.Date)
            sp.Command.AddParameter("@END_DATE", txtExpiryDate.Text, DbType.Date)
            sp.Command.AddParameter("@PAID_AMOUNT", txtpamount.Text, DbType.Decimal)
            sp.Command.AddParameter("@OUTSTANDING_AMOUNT", txtoamount.Text, DbType.Decimal)
            sp.Command.AddParameter("@REQUEST_STATUS", ddlwstatus.SelectedItem.Value, DbType.Int32)
            sp.Command.AddParameter("@WR_CONDITION", txtRemarks.Text, DbType.String)
            sp.Command.AddParameter("@WR_CREATED", Session("Uid"), DbType.String)
            sp.Command.AddParameter("@COMPANY", Session("COMPANYID"), DbType.String)
            sp.ExecuteScalar()
            Dim sp1 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "UPT_STATUS")
            sp1.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            sp1.ExecuteScalar()
            lblmsg.Text = "Work Request Successfully Updated "
            fillgrid()
            Cleardata()
        End If


    End Sub
    Protected Sub ddlWorkRequest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWorkRequest.SelectedIndexChanged
        fillgrid()
        If ddlWorkRequest.SelectedIndex > 0 Then
            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_WR")
            sp.Command.AddParameter("@PN_WORKREQUEST_REQ", ddlWorkRequest.SelectedItem.Text, DbType.String)
            Dim ds As New DataSet()
            ds = sp.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtcity.Text = ds.Tables(0).Rows(0).Item("PN_BLDG_CODE")
                txtProperty.Text = ds.Tables(0).Rows(0).Item("PN_LOC_ID")
                txtproptype.Text = ds.Tables(0).Rows(0).Item("PROP_TYPE")
                txtLocation.Text = ds.Tables(0).Rows(0).Item("PN_CTY_CODE")
                txtWorkTitle.Text = ds.Tables(0).Rows(0).Item("WORK_TITLE")
                txtWorkSpec.Text = ds.Tables(0).Rows(0).Item("WORK_SPECIFICATIONS")
                txtamount.Text = Decimal.Round(ds.Tables(0).Rows(0).Item("ESTIMATED_AMOUNT"), 2, MidpointRounding.AwayFromZero)
                txtRemarks.Text = ds.Tables(0).Rows(0).Item("Remarks")
                'ddlwstatus.SelectedValue = ds.Tables(0).Rows(0).Item("REQUEST_STATUS")
            End If
        Else
            txtcity.Text = ""
            txtProperty.Text = ""
            txtWorkTitle.Text = ""
            txtWorkSpec.Text = ""
            txtamount.Text = ""
            txtRemarks.Text = ""
            txtproptype.Text = ""
        End If
    End Sub
End Class