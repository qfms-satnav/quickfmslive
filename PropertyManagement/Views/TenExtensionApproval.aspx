﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TenExtensionApproval.aspx.cs" Inherits="PropertyManagement_Views_TenExtensionApproval" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
  <%--  <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
      <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
--%>



    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
</head>
<script type="text/javascript">
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Add Tenant" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Tenant Extension Approve/Reject</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                         <div class="widgets">
                        <h3>Tenant Extension Approve/Reject </h3>
                         </div>
                           <div class="card">
                            <form id="form1" runat="server">
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" EmptyDataText="No Current Tenant Details Found."
                                            CssClass="table GridStyle" GridLines="none" OnPageIndexChanging="gvLDetails_Lease_PageIndexChanging"
                                            OnRowCommand="gvLDetails_Lease_RowCommand" DataKeyNames="PROPERTY_NAME, PM_TEN_TO_DT">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Property Code" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpropTenSNO" runat="server" Text='<%#Eval("SNO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblpname" runat="server" Text='<%#Eval("PROPERTY_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("PM_TEN_FRM_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant Expiry Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("PM_TEN_TO_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant Requested Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExtsdate" runat="server" Text='<%#Eval("PM_TE_START_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant Requested To Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExtEdate" runat="server" Text='<%#Eval("PM_TE_END_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Tenant">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblten" runat="server" Text='<%#Eval("TENANT_NAME")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Extension Requested By">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Lbluser" runat="server" Text='<%#Eval("PM_TE_CREATED_BY")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Requested Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblRequestesDt" runat="server" Text='<%#Eval("PM_TE_CREATED_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblExtSNO" runat="server" Text='<%#Eval("PM_TE_SNO")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkExtn" runat="server" Text="Approve/Reject" CommandArgument='<%#Eval("SNO")%>' CommandName="ViewDetails"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <asp:HiddenField ID="hdnPropCode" runat="server" />
                                <asp:HiddenField ID="hdnExtSNO" runat="server" />
                                <asp:HiddenField ID="hdnExtFrmDt" runat="server" />
                                <asp:HiddenField ID="hdnExtToDt" runat="server" />
                                <div id="panel1" runat="server" visible="false">
                                    <h4>Tenant Agreement Extension Details</h4>
                                    <div class="panel panel-default " role="tab" runat="server" id="div0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-multiselectable="true">Property Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="collapse show">
                                            <div class="panel-body color">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Property Type <span style="color: red;">*</span></label>
                                                            <%--  <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                                                        Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                                                        InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <asp:Label ID="lblproptype" runat="server"></asp:Label>

                                                                <%--<asp:DropDownList ID="ddlproptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                                        </asp:DropDownList>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">City <span style="color: red;">*</span></label>
                                                            <%--<asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <asp:Label ID="lblcity" runat="server"></asp:Label>
                                                                <%--<asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Location <span style="color: red;">*</span></label>
                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <%--<asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                                            <asp:ListItem Value="">--Select--</asp:ListItem>
                                                        </asp:DropDownList>--%>
                                                                <asp:Label ID="lblLocation" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Property <span style="color: red;">*</span></label>
                                                            <%--<asp:RequiredFieldValidator ID="cvbuilding" runat="server" ControlToValidate="ddlBuilding"
                                                        Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <%--<asp:DropDownList ID="ddlBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                            <asp:ListItem Value="">--Select--</asp:ListItem>
                                                        </asp:DropDownList>--%>
                                                                <asp:Label ID="lblProperty" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Tenant Occupied Area (Sqft)<span style="color: red;">*</span></label>
                                                            <%--   <asp:RequiredFieldValidator ID="rfarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                                                        Display="None" ErrorMessage="Please Enter Tenant Occupied Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                                                        ErrorMessage="Please Enter Valid Tenant Occupied Area in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                        Display="None" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>--%>
                                                            <div class="col-md-12">
                                                                <%--  <div onmouseover="Tip('Enter Tenant Occupied Area in numbers upto 2 decimal places.')"
                                                            onmouseout="UnTip()">--%>
                                                                <asp:Label ID="lblOccArea" runat="server"></asp:Label>
                                                                <%--<asp:TextBox ID="txtTenantOccupiedArea" runat="server" CssClass="form-control" ReadOnly></asp:TextBox>--%>
                                                                <%--</div>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Tenant From Date</label>
                                                            <div class="col-md-12">
                                                                <asp:Label ID="lblTenFromDt" runat="server"></asp:Label>
                                                                <%--<asp:TextBox ID="txtTenfromDt" runat="server" CssClass="form-control" ReadOnly></asp:TextBox>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Tenant End Date</label>
                                                            <div class="col-md-12">
                                                                <asp:Label ID="lblTenToDate" runat="server"></asp:Label>
                                                                <%--<asp:TextBox ID="txtTenToDt" runat="server" CssClass="form-control" ReadOnly></asp:TextBox>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row" style="padding-top:10px">
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <strong>Documents</strong>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                                </asp:Label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12 col-sm-3 col-xs-12">
                                                            <div class="form-group">
                                                                <div id="tblGridDocs" runat="server">
                                                                    <asp:DataGrid ID="grdDocs" runat="server" CssClass="table GridStyle" GridLines="none" DataKeyField="PM_TE_DOC_SNO"
                                                                        EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5" OnDeleteCommand="grdDocs_DeleteCommand1" OnItemCommand="grdDocs_ItemCommand1">
                                                                        <Columns>
                                                                            <asp:BoundColumn Visible="False" DataField="PM_TE_DOC_SNO" HeaderText="ID"></asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="PM_TE_DOC_PATH" HeaderText="Document Name">
                                                                                <HeaderStyle></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:BoundColumn DataField="PM_LDOC_CREATED_DT" HeaderText="Document Date">
                                                                                <HeaderStyle></HeaderStyle>
                                                                            </asp:BoundColumn>
                                                                            <asp:ButtonColumn Text="Download" CommandName="Download">
                                                                                <HeaderStyle></HeaderStyle>
                                                                            </asp:ButtonColumn>
                                                                            <asp:ButtonColumn Text="Delete" CommandName="Delete">
                                                                                <HeaderStyle></HeaderStyle>
                                                                            </asp:ButtonColumn>
                                                                        </Columns>
                                                                        <HeaderStyle ForeColor="white" BackColor="Black" />
                                                                        <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                                    </asp:DataGrid>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" runat="server" id="div1">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Tenant Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="collapse show">
                                            <div class="panel-body color">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Tenant <span style="color: red;">*</span></label>
                                                            <%--<asp:RequiredFieldValidator ID="cvuser" runat="server" ControlToValidate="ddluser"
                                                        Display="None" ErrorMessage="Please Select User" ValidationGroup="Val1" InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <%--<asp:DropDownList ID="ddluser" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                        </asp:DropDownList>--%>
                                                                <asp:Label ID="lblTenant" runat="server"></asp:Label>
                                                                <%--<asp:TextBox ID="txtTenant" runat="server" CssClass="form-control" ReadOnly></asp:TextBox>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Tenant Code</label>
                                                            <%--<asp:RequiredFieldValidator ID="rfvtcode" runat="server" ControlToValidate="txttcode"
                                                        Display="None" ErrorMessage="Please Enter Tenant Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <asp:Label ID="lblTenCode" runat="server"></asp:Label>
                                                                <%--<asp:TextBox ID="txtTencode" runat="server" CssClass="form-control" ReadOnly></asp:TextBox>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Parking Spaces</label>
                                                            <%--<asp:RegularExpressionValidator ID="revNoofParking" ValidationGroup="Val1" runat="server"
                                                        Display="none" ControlToValidate="txtNoofparking" ErrorMessage="Please Enter Valid Number of Parking"
                                                        ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>--%>
                                                            <div class="col-md-12">
                                                                <%--<div onmouseover="Tip('Enter numbers only with maximum length 20')" onmouseout="UnTip()">--%>
                                                                <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control" ReadOnly></asp:TextBox>
                                                                <%--</div>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel panel-default " role="tab" runat="server" id="div2">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Payment Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="collapse show">
                                            <div class="panel-body color">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Tenant Rent <span style="color: red;">*</span></label>

                                                            <asp:RequiredFieldValidator ID="rfRent" runat="server" ControlToValidate="txtRent"
                                                                Display="None" ErrorMessage="Please Enter Tenant Rent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="revRent" runat="server" ControlToValidate="txtRent"
                                                                Display="None" ErrorMessage="Please Enter Valid Tenant Rent in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                                ValidationExpression="((\d+)((\.\d{1,4})?))$"></asp:RegularExpressionValidator>
                                                            <div class="col-md-12">
                                                                <div onmouseover="Tip('Enter tenant rent in numbers upto 2 decimal places.')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtRent" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="20" OnTextChanged="txtRent_TextChanged"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Security Deposit</label>
                                                           <%-- <asp:RequiredFieldValidator ID="rfdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                                Display="None" ErrorMessage="Please Enter Security Deposit" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <asp:RegularExpressionValidator ID="revdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                                Display="None" ErrorMessage="Please Enter Valid Security Deposit in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                                ValidationExpression="((\d+)((\.\d{1,4})?))$"></asp:RegularExpressionValidator>
                                                            <div class="col-md-12">
                                                                <div onmouseover="Tip('Enter Security Deposit only in numbers upto 2 decimal places.')"
                                                                    onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Joining Date</label>
                                                         <%--   <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Joining Date"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <%--  <div class='input-group date' id='fromdate'>--%>
                                                                <asp:TextBox ID="txtDate" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>
                                                                <%--  <span class="input-group-addon">
                                                                <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                            </span>--%>
                                                                <%--</div>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Payment Terms <span style="color: red;">*</span></label>
                                                            <%--<asp:RequiredFieldValidator ID="rfvpayment" runat="server" ControlToValidate="ddlPaymentTerms"
                                                        ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Payment Terms"
                                                        InitialValue="--Select--"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <asp:TextBox ID="txtpayterms" runat="server" ReadOnly CssClass="form-control"></asp:TextBox>
                                                                <%--<asp:DropDownList ID="ddlPaymentTerms" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                            <asp:ListItem>--Select--</asp:ListItem>
                                                        </asp:DropDownList>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Society Maintenance / CAM</label>

                                                            <asp:RegularExpressionValidator ID="revfees" runat="server" ControlToValidate="txtfees"
                                                                Display="none" ErrorMessage="Please Enter Valid Maintenance fees in Numbers or Decimal Number with 2 Decimal Places." ValidationExpression="((\d+)((\.\d{1,4})?))$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                            <div class="col-md-12">
                                                                <div onmouseover="Tip('Enter Maintenance Fees in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtfees" runat="server" CssClass="form-control" AutoPostBack="true" MaxLength="20" OnTextChanged="txtfees_TextChanged"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Additional Car Parking Fees </label>
                                                            <div class="col-md-12">
                                                                <asp:TextBox ID="txtcar" runat="server" CssClass="form-control" Rows="3" MaxLength="500"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Total Rent Amount <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvamt" runat="server" ControlToValidate="txtamount"
                                                                Display="None" ValidationGroup="Val1" ErrorMessage="Please Enter Total Rent Amount."></asp:RequiredFieldValidator>
                                                            <asp:RegularExpressionValidator ID="regexpamount" runat="server" ControlToValidate="txtamount" Display="None"
                                                                ValidationGroup="Val1" ErrorMessage="Please Enter Total Rent Amount in Numbers or Decimal Number with 2 Decimal Places" ValidationExpression="((\d+)((\.\d{1,4})?))$">

                                                            </asp:RegularExpressionValidator>
                                                            <div class="col-md-12">
                                                                <div onmouseover="Tip('Enter Total Rent Amount in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Requestor Remarks <span style="color: red;">*</span></label>
                                                            <%--<asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                                        Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                                            <div class="col-md-12">
                                                                <asp:TextBox ID="txtReqRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" Rows="3" ReadOnly></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                                        <div class="form-group">
                                                            <label class="col-md-12 control-label">Remarks <span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRemarks"
                                                                Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <div class="col-md-12">
                                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%"
                                                                    Rows="3" MaxLength="500"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="col-md-12 text-right" style="padding-top: 5px">
                                        <asp:Button ID="btnApprove" CssClass="btn btn-primary custom-button-color" runat="server" Text="Approve" ValidationGroup="Val1" OnClick="btnApprove_Click" />
                                        <asp:Button ID="btnReject" CssClass="btn btn-primary custom-button-color" runat="server" Text="Reject" ValidationGroup="Val1" OnClick="btnReject_Click" />
                                    </div>

                                </div>

                            </form>
                        </div>
                    </div>
                <%--</div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
