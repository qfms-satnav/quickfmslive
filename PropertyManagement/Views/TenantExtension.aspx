﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TenantExtension.aspx.cs" Inherits="PropertyManagement_Views_TenantExtension" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%--<style>
        body {
            color: #6a6c6f;
            background-color: #f1f3f6;
            /*margin-top: 30px;*/
        }

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>--%>
</head>
<script type="text/javascript">
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <h3>Tenant Extension</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-sm-12 col-xs-6">
                                    <label>Search by Property Name/ Location/ Tenant ID/ Tenant Code</label>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search"></asp:TextBox>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" OnClick="btnSearch_Click" />
                                </div>
                            </div>

                            <div class="row form-inline">
                                <div class="form-group col-md-12" style="padding-top: 10px">
                                    <asp:GridView ID="gvLDetails_Lease" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="5" EmptyDataText="No Current Tenant Details Found."
                                        CssClass="table GridStyle" GridLines="none" OnPageIndexChanging="gvLDetails_Lease_PageIndexChanging"
                                        OnRowCommand="gvLDetails_Lease_RowCommand" DataKeyNames="PROPERTY_NAME, PM_TEN_TO_DT">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Property Code" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpropTenSNO" runat="server" Text='<%#Eval("PROPERTY_CODE")%>'></asp:Label>
                                                    <asp:Label ID="lblSNO" runat="server" Text='<%#Eval("SNO")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblpname" runat="server" Text='<%#Eval("PROPERTY_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Location">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Start Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("PM_TEN_FRM_DT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Expiry Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("PM_TEN_TO_DT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblten" runat="server" Text='<%#Eval("TENANT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Created By">
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbluser" runat="server" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="PAY TERMS" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblPAYTName" runat="server" Text='<%#Eval("PM_PT_NAME")%>'></asp:Label>
                                                    <asp:Label ID="lblPAYid" runat="server" Text='<%#Eval("PM_TD_PAY_TERMS")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblTenRent" runat="server" Text='<%#Eval("PM_TD_RENT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkExtn" runat="server" Text="Extension" CommandArgument='<%#Eval("PROPERTY_CODE")%>'
                                                        CommandName="Extension"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="View Details">
                                                <ItemTemplate>
                                                    <a href="#" onclick="showPopWin('<%# Eval("SNO") %>')">
                                                        <asp:Label ID="lbltencode" runat="server" Text="View Details"></asp:Label>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                            <br />
                            <br />
                            <asp:HiddenField ID="hdnPropCode" runat="server" />
                            <asp:HiddenField ID="hdnpayId" runat="server" />
                            <div id="panel1" runat="server" visible="false">
                                <h4>Tenant Agreement Extension Details</h4>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Property Name<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rftPN" runat="server" ControlToValidate="txtPname"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Property Name"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtPname" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Payment Terms<span style="color: red;">*</span></label>
                                            <%--<asp:RequiredFieldValidator ID="rfvPt" runat="server" ControlToValidate="txtPterms"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Pa"></asp:RequiredFieldValidator>--%>
                                            <asp:TextBox ID="txtPterms" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">No of Payment Terms <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvPaycnt" runat="server" ControlToValidate="txtpayCount"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter No of Payment Terms"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtpayCount" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtpayCount_TextChanged"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label class="col-md-12 control-label">Rent Amount<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvRent" runat="server" ControlToValidate="txtRent"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Rent Amount"></asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtRent" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tenant From Date <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvSt" runat="server" ControlToValidate="TntFrm"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Tenant From Date"></asp:RequiredFieldValidator>
                                            <div class='input-group date' id='TFrm'>
                                                <asp:TextBox ID="TntFrm" runat="server" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                <%--  <span class="input-group-addon">
                                                <span class="fa fa-calendar" onclick="setup('TFrm')"></span>
                                            </span>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Tenant To Date <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvTo" runat="server" ControlToValidate="txttoDt"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Tenant To Date"></asp:RequiredFieldValidator>
                                            <div class='input-group date' id='Tto'>
                                                <asp:TextBox ID="txttoDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('Tto')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Security Deposit</label>
                                            <%--    <asp:RequiredFieldValidator ID="rfdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                        Display="None" ErrorMessage="Please Enter Security Deposit" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                            <asp:RegularExpressionValidator ID="revdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                Display="None" ErrorMessage="Please Enter Valid Security Deposit in Numbers or Decimal Number with 2 Decimal Places." ValidationGroup="Val1"
                                                ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                            <div onmouseover="Tip('Enter Security Deposit only in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Remarks<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="rfvtxtRecmRemarks" runat="server" ControlToValidate="txtRemarks"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Extension Remarks"></asp:RequiredFieldValidator>
                                            <div onmouseover="Tip('Enter Remarks with maximum 500 characters')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%"
                                                    Rows="3" TextMode="Multiline" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Upload Files/Images</label>
                                            <div class="btn btn-primary btn-mm">
                                                <i class="fa fa-folder-open-o fa-lg"></i>
                                                <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <div class="col-md-12 col-sm-6 col-xs-12 text-right" style="padding-top: 5px">
                                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" OnClick="btnSubmit_Click" />
                                </div>
                            </div>
                            <br />
                            <br />
                            <%-- <h4>Rejected (Extension) Tenants</h4>
                        <div class="row" style="margin-top: 10px">
                            <div class="col-md-12">
                                <asp:GridView ID="gvrejected" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                    AllowPaging="True" PageSize="5" EmptyDataText="No Rejected Lease Details Found."
                                    CssClass="table table-condensed table-bordered table-hover table-striped">
                                    <PagerSettings Mode="NumericFirstLast" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Property Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblpname" runat="server" Text='<%#Eval("PROPERTY_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Location">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoc" runat="server" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenant Start Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("PM_TEN_FRM_DT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Tenant Expiry Date">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEdate" runat="server" Text='<%#Eval("PM_TEN_TO_DT")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>                                       
                                        <asp:TemplateField HeaderText="Tenant">
                                            <ItemTemplate>
                                                <asp:Label ID="lblten" runat="server" Text='<%#Eval("TENANT_NAME")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Created By">
                                            <ItemTemplate>
                                                <asp:Label ID="Lbluser" runat="server" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsno" runat="server" Text='<%#Eval("SNO")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                    <PagerStyle CssClass="pagination-ys" />
                                </asp:GridView>
                            </div>
                        </div>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="align-items-center justify-content-between">
                        <h5 class="modal-title" id="H1">View Tenant Renewal Details</h5>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <iframe id="modalcontentframe" width="100%" height="450px" frameborder="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>

    <script>
        function showPopWin(id) {
            $("#modalcontentframe").attr("src", "TenantRenew.aspx?Renewal=" + id + "&ExType=1");
            $("#myModal").modal('show');
        }

        // No of Payments
        jQuery('#txtpayCount').keyup(function () {
            this.value = this.value.replace(/[^1-9]/g, '');
        });

    </script>

</body>
</html>
