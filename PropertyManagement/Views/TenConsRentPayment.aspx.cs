﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PropertyManagement_Views_TenConsRentPayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "anything", "refreshSelectpicker();", true);
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string host = HttpContext.Current.Request.Url.Host;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50);
        param[0].Value = Session["UID"];
        param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
        param[1].Value = path;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
        {
            if (Session["UID"].ToString() == "")
            {
                Response.Redirect(Application["FMGLogout"].ToString());
            }
            else
            {
                if (sdr.HasRows)
                {
                }
                else
                {
                    Response.Redirect(Application["FMGLogout"].ToString());
                }
            }
        }
        if (!IsPostBack)
        {
            gridVisStatus.Visible = false;
            btnsubmit.Visible = false;
            BindPropertyType();
            BindCity();
            panel1.Visible = false;
            panel2.Visible = false;

            divPayterms.Visible = false;
            divRems.Visible = false;
        }
    }

    private void BindPropertyType()
    {
        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_ACTPROPTYPE");
        sp3.Command.AddParameter("@dummy", Session["uid"], DbType.String);
        ddlproptype.DataSource = sp3.GetDataSet();
        ddlproptype.DataTextField = "PN_PROPERTYTYPE";
        ddlproptype.DataValueField = "PN_TYPEID";
        ddlproptype.DataBind();
        ddlproptype.Items.Insert(0, new ListItem("--Select Property Type--", "0"));
    }

    private void BindCity()
    {
        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_ACTCTY");
        sp3.Command.AddParameter("@DUMMY", "", DbType.String);
        sp3.Command.AddParameter("@AUR_ID", Session["uid"], DbType.String);
        ddlCity.DataSource = sp3.GetDataSet();
        ddlCity.DataTextField = "CTY_NAME";
        ddlCity.DataValueField = "CTY_CODE";
        ddlCity.DataBind();
        ddlCity.Items.Insert(0, new ListItem("--Select City--", "0"));
    }

    protected void ddlCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCity.SelectedIndex > 0)
        {
            SubSonic.StoredProcedure sp2 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LOCATION_CITY");
            sp2.Command.AddParameter("@CITY", ddlCity.SelectedValue);
            sp2.Command.AddParameter("@USR_ID", Session["uid"]);
            ddlLocation.DataSource = sp2.GetDataSet();
            ddlLocation.DataTextField = "LCM_NAME";
            ddlLocation.DataValueField = "LCM_CODE";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--Select Location--", "0"));
        }
        else
        {
            ddlLocation.Items.Clear();
        }
    }

    protected void btnsearch_Click(object sender, EventArgs e)
    {
        BindGrid();
        txtTerms.Text = "1";
        if (gvitems.Rows.Count >= 1)
            gvitems.Rows[0].BackColor = System.Drawing.Color.Aqua;

        txtTDS.Text = string.Empty;
        ddlTDStype.SelectedIndex = 0;
        lblhistHeading.Visible = true;
    }
    protected void grdView_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvitems.PageIndex = e.NewPageIndex;
            BindGrid();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    
    private void BindGrid()
    {
        gridVisStatus.Visible = true;
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_RENT_HISTORY");
        sp.Command.AddParameter("@PROP_ID", ddlBuilding.SelectedValue, DbType.String);
        DataSet ds = sp.GetDataSet();
        gvitems.DataSource = ds.Tables[0];
        gvitems.DataBind();

        gvpaid.DataSource = ds.Tables[1];
        gvpaid.DataBind();
    }

    protected void ddlpaymentmode_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlpaymentmode.SelectedIndex > 0)
        {
            if (ddlpaymentmode.SelectedItem.Value == "1")
            {
                panel1.Visible = true;
                panel2.Visible = false;
            }
            else if (ddlpaymentmode.SelectedItem.Value == "2")
            {
                panel1.Visible = false;
                panel2.Visible = false;
            }
            else if (ddlpaymentmode.SelectedItem.Value == "3")
            {
                panel1.Visible = false;
                panel2.Visible = true;
            }
        }
        else
        {
            panel1.Visible = false;
            panel2.Visible = false;
        }
    }

    protected void txtTerms_TextChanged(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtTerms.Text))
        {
            lblmsg.Visible = true;
            lblmsg.Text = "Enter at least 1 for the payments";
            //txtTerms.Text = 0
            return;
        }
        else if (Convert.ToInt32(txtTerms.Text) > gvitems.Rows.Count)
        {
            lblmsg.Visible = true;
            lblmsg.Text = "Number of payments value should be less than or equal to selected payments " + Convert.ToString(gvitems.Rows.Count);
            return;
        }
        else
        {
            lblmsg.Text = string.Empty;
            int cnt = Convert.ToInt32(txtTerms.Text);
            gvitems.BackColor = System.Drawing.Color.White;
            for (int i = 0; i <= gvitems.Rows.Count - 1; i++)
            {
                gvitems.Rows[i].BackColor = System.Drawing.Color.White;
            }
            if (gvitems.Rows.Count >= cnt)
            {
                for (int i = 0; i <= cnt - 1; i++)
                {
                    gvitems.Rows[i].BackColor = System.Drawing.Color.Aqua;
                }
            }
        }
        txtTDS.Text = string.Empty;
        ddlTDStype.SelectedIndex = 0;
    }

    protected void btnCalc_Click(object sender, EventArgs e)
    {
        int terms = Convert.ToInt32(txtTerms.Text);
        for (int i = 0; i < terms; i++)
        {
            decimal OutsAmt = Convert.ToDecimal(((Label)gvitems.Rows[i].FindControl("lblOSAmount")).Text);
            decimal TDSTxt = Convert.ToDecimal(txtTDS.Text);
            decimal amenity = txtamenitycharges.Text == "" ? 0 : Convert.ToDecimal(txtamenitycharges.Text);

            if (ddlTDStype.SelectedItem.Value == "1")
            {

                gvitems.Rows[i].Cells[9].Text = Convert.ToString(OutsAmt + TDSTxt + amenity);
            }
            else
            {
                decimal PerTDS = (OutsAmt * TDSTxt) / 100;

                gvitems.Rows[i].Cells[9].Text = Convert.ToString(PerTDS + OutsAmt + amenity);

            }

        }


        if (gvitems.Rows.Count > 0)
        {
            txtTerms.Visible = true;
            btnsubmit.Visible = true;
            divPayterms.Visible = true;
            divRems.Visible = true;
        }
        else
        {
            btnsubmit.Visible = false;
            txtTerms.Visible = false;
            divPayterms.Visible = false;
            divRems.Visible = false;
        }
    }

    protected void ddlproptype_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            lblmsg.Text = string.Empty;
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_PROP_RENT_PAYMENTS");
            sp.Command.AddParameter("@CTY_CODE", ddlCity.SelectedItem.Value, DbType.String);
            sp.Command.AddParameter("@proptype", ddlproptype.SelectedItem.Value, DbType.String);
            sp.Command.AddParameter("@USER_ID", Session["uid"], DbType.String);
            sp.Command.AddParameter("@LCM_CODE", ddlLocation.SelectedItem.Value, DbType.String);
            ddlBuilding.DataSource = sp.GetDataSet();
            ddlBuilding.DataTextField = "PN_NAME";
            ddlBuilding.DataValueField = "PM_PPT_CODE";
            ddlBuilding.DataBind();
            if (ddlBuilding.Items.Count == 0)
            {
                lblmsg.Visible = true;
                lblmsg.Text = "There are no properties listed under this location..";
            }
            ddlBuilding.Items.Insert(0, new ListItem("--Select Property--", "0"));
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }

    public DateTime getoffsetdatetime(DateTime date)
    {
        Object O = HttpContext.Current.Session["useroffset"];
        if (O != null)
        {
            string Temp = O.ToString().Trim();
            if (!Temp.Contains("+") && !Temp.Contains("-"))
                Temp = Temp.Insert(0, "+");
            ReadOnlyCollection<TimeZoneInfo> timeZones = TimeZoneInfo.GetSystemTimeZones();
            DateTime startTime = DateTime.Parse(DateTime.UtcNow.ToString());
            DateTime _now = DateTime.Parse(DateTime.UtcNow.ToString());
            foreach (TimeZoneInfo timeZoneInfo__1 in timeZones)
            {
                if (timeZoneInfo__1.ToString().Contains(Temp))
                {
                    // Response.Write(timeZoneInfo.ToString());
                    // string _timeZoneId = "Pacific Standard Time";
                    TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById(timeZoneInfo__1.Id);
                    _now = TimeZoneInfo.ConvertTime(startTime, TimeZoneInfo.Utc, tst);
                    break;
                }
            }
            return _now;
        }
        else
            return DateTime.Now;
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (Convert.ToInt32(txtTerms.Text) <= gvitems.Rows.Count)
        {
            SubSonic.StoredProcedure sp4 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "GET_PM_TEN_RP_SNO");
            DataSet ds = new DataSet();
            ds = sp4.GetDataSet();
            string Invno = "INV-" + getoffsetdatetime(DateTime.Now).ToString("ddMMyyyy") + "-" + ds.Tables[0].Rows[0]["PM_RP_ID"].ToString();

            int cnt = Convert.ToInt32(txtTerms.Text);
            for (int i = 0; i <= cnt - 1; i++)
            {
                string lblTC = Convert.ToString(((Label)gvitems.Rows[i].FindControl("lblTenCode")).Text);

                Label lblTenAurId = (Label)gvitems.Rows[i].FindControl("lblTenAurID");

                string lblOSamt = gvitems.Rows[i].Cells[9].Text;
                Label lblsnoid = (Label)gvitems.Rows[i].FindControl("lblSNO");

                try
                {
                   

                    SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_TEN_UPDATE_RENTDETAILS");
                    sp3.Command.AddParameter("@TENANTID", lblTC, DbType.String);
                    sp3.Command.AddParameter("@OUTSTANDING", Convert.ToDecimal(lblOSamt), DbType.Decimal);

                    sp3.Command.AddParameter("@TDS_TYPE", ddlTDStype.SelectedValue, DbType.Int32);
                    sp3.Command.AddParameter("@TDS", Convert.ToDecimal(txtTDS.Text), DbType.Decimal);
                    sp3.Command.AddParameter("@PAYMENT_MODE", ddlpaymentmode.SelectedItem.Text, DbType.String);
                    if (ddlpaymentmode.SelectedItem.Value == "1")
                    {
                        sp3.Command.AddParameter("@CHEQUENO", txtCheque.Text, DbType.String);
                        sp3.Command.AddParameter("@ISSUINGBANK", txtBankName.Text, DbType.String);
                        sp3.Command.AddParameter("@DEPOSITEDBANK", "", DbType.String);
                        sp3.Command.AddParameter("@ACCOUNTNUMBER", txtAccNo.Text, DbType.String);
                        sp3.Command.AddParameter("@IFCB", "", DbType.String);
                    }
                    else if (ddlpaymentmode.SelectedItem.Value == "3")
                    {
                        sp3.Command.AddParameter("@CHEQUENO", "", DbType.String);
                        sp3.Command.AddParameter("@ISSUINGBANK", txtIBankName.Text, DbType.String);
                        sp3.Command.AddParameter("@DEPOSITEDBANK", txtDeposited.Text, DbType.String);
                        sp3.Command.AddParameter("@ACCOUNTNUMBER", "", DbType.String);
                        sp3.Command.AddParameter("@IFCB", txtIFCB.Text, DbType.String);
                    }
                    else
                    {
                        sp3.Command.AddParameter("@CHEQUENO", "", DbType.String);
                        sp3.Command.AddParameter("@ISSUINGBANK", "", DbType.String);
                        sp3.Command.AddParameter("@DEPOSITEDBANK", "", DbType.String);
                        sp3.Command.AddParameter("@ACCOUNTNUMBER", "", DbType.String);
                        sp3.Command.AddParameter("@IFCB", "", DbType.String);
                    }
                    sp3.Command.AddParameter("@FK_SNO", lblsnoid.Text, DbType.Int32);
                    sp3.Command.AddParameter("@REMARKS", txtRemarks.Text, DbType.String);
                    sp3.Command.AddParameter("@CREATED_BY", Session["uid"], DbType.String);
                    sp3.Command.AddParameter("@TEN_AURID", lblTenAurId.Text, DbType.String);
                    sp3.Command.AddParameter("@PROPERTY", ddlBuilding.SelectedItem.Text, DbType.String);
                    sp3.Command.AddParameter("@LOC", ddlLocation.SelectedValue, DbType.String);
                    sp3.Command.AddParameter("@CMPNYID", Session["COMPANYID"], DbType.String);
                    sp3.Command.AddParameter("@PM_RP_TRN_NO", txtTRN.Text, DbType.String);
                    sp3.Command.AddParameter("@PM_RP_INV_NO", Invno, DbType.String);
                    sp3.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    Response.Write(ex.Message);
                }
            }
            lblmsg.Visible = true;
            lblmsg.Text = "Rent Details Successfully Updated ";
            Cleardata();
        }
    }

    public void Cleardata()
    {
        gridVisStatus.Visible = false;
        btnsubmit.Visible = false;
        ddlproptype.SelectedIndex = 0;
        ddlCity.SelectedIndex = 0;
        ddlLocation.Items.Clear();
        ddlBuilding.Items.Clear();

        txtTDS.Text = string.Empty;
        panel1.Visible = false;
        panel2.Visible = false;

        divPayterms.Visible = false;
        divRems.Visible = false;
        lblhistHeading.Visible = false;
        //gvpaid.Visible = false;
    }
}