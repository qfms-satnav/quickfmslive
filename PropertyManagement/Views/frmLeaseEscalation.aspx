﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html>

<html data-ng-app="QuickFMS">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />
    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />--%>
    <%-- <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />--%>
    <style>
        .grid-align {
            text-align: center;
        }

        a:hover {
            cursor: pointer;
        }

        .ag-header-cell-filtered {
            background-color: #4682B4;
        }

        .cssCircle {
            -webkit-border-radius: 999px;
            -moz-border-radius: 999px;
            border-radius: 999px;
            behavior: url(PIE.htc);
            width: 20px;
            height: 20px;
            background: #008CBA;
            border: 1px solid #003580;
            color: #fff;
            text-align: center;
            -webkit-transition: background 0.2s linear;
            -moz-transition: background 0.2s linear;
            -ms-transition: background 0.2s linear;
            -o-transition: background 0.2s linear;
            transition: background 0.2s linear;
            transition: color 0.2s linear;
            font: 18px Arial, sans-serif
        }

            .cssCircle:hover {
                background: #3F69A0;
                cursor: pointer;
            }

        .minusSign {
            line-height: 0.9em;
            margin-bottom: 1px;
        }

        .plusSign {
            line-height: 1.1em;
        }

            .minusSign:hover,
            .plusSign:hover {
                color: white;
            }
            .form-control {
                 width:100%;
            }
    </style>
</head>

<body data-ng-controller="LeaseEscController" class="amantra">
    <div class="animsition">
        <div class="al-content">
            <%-- <div class="widgets">
                <div ba-panel ba-panel-title="Warranty Expired Assets Report" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Lease Escalation</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>Lease Escalation</h3>
            </div>
            <div class="card">
                <form role="form" id="form2" name="frmLED">
                    <div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 col-xs-12 form-inline">
                                <label>Escalation On</label>
                                <div>
                                    <input type="radio" data-ng-model="LeaseObj.frmLED.Lease" name="Lease" value="Lease" data-ng-change="LeaseObj.EscONFn()" />Lease Wise
                                                <input type="radio" data-ng-model="LeaseObj.frmLED.Lease" name="Landlord" value="LandLord" data-ng-change="LeaseObj.EscONFn()" />Landlord Wise
                                               
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">

                                <div class="form-group">
                                    <label class="control-label">Property Name / Request ID <span style="color: red;">*</span></label>
                                    <div isteven-multi-select data-input-model="Property" data-output-model="LeaseEscVM.Property" data-button-label="icon PM_PPT_NAME"
                                        data-item-label="icon PM_PPT_NAME maker" data-on-item-click="LeaseObj.PropertyFn()"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>
                                    <%--<input type="text" data-ng-model="LeaseEscVM.Property[0]" name="PM_PPT_NAME" style="display: none" required="" />--%>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="llid">
                                <div class="form-group">
                                    <label class="control-label">Landlord Name / id </label>
                                    <%--<span style="color: red;">*</span>--%>
                                    <div isteven-multi-select data-input-model="Property1" data-output-model="LeaseEscVM.Property1" data-button-label="icon llname"
                                        data-item-label="icon llname maker" data-on-item-click="LeaseObj.frmLED.Lease=='LandLord' && LeaseObj.PropertyFn1('S')"
                                        data-tick-property="ticked" data-max-labels="1" data-selection-mode="single">
                                    </div>

                                    <%--<input type="text" data-ng-model="LeaseEscVM.Property[0]" name="PM_PPT_NAME" style="display: none" required="" />--%>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12" data-ng-show="llname">
                                <div class="form-group">
                                    <label>Landlord Name</label>
                                    <input type="hidden" data-ng-model="LeaseObj.PropertyObj.PM_LL_SNO" data-ng-show="hide" />
                                    <input type="text" class="form-control" name="PM_LL_NAME" id="txtllname" data-ng-model="LeaseObj.PropertyObj.PM_LL_NAME" />&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Property Code</label>
                                    <input type="hidden" data-ng-model="LeaseObj.PropertyObj.PM_LR_REQ_ID" data-ng-show="hide" />
                                    <input type="hidden" data-ng-model="LeaseObj.PropertyObj.PM_LES_SNO" data-ng-show="hide" />
                                    <input type="text" class="form-control" name="PM_PPT_CODE" id="txtprocode" data-ng-model="LeaseObj.PropertyObj.PM_PPT_CODE" />&nbsp;
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Basic Rent (PM)</label>
                                    <input type="text" class="form-control" name="PM_LES_BASIC_RENT" id="txtbasicrent" data-ng-model="LeaseObj.PropertyObj.PM_LES_BASIC_RENT" />&nbsp;
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Security Deposit</label>
                                    <input type="text" class="form-control" name="PM_LES_SEC_DEPOSIT" id="txtsecudiposit" data-ng-model="LeaseObj.PropertyObj.PM_LES_SEC_DEPOSIT" />&nbsp;
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Furniture & Fixtures/Amenities Charges</label>
                                    <input type="text" class="form-control" name="PM_LC_FF_CHARGES" id="txtfacilitycharges" data-ng-model="LeaseObj.PropertyObj.PM_LC_FF_CHARGES" />&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Maintenance Charges</label>
                                    <input type="text" class="form-control" name="PM_LC_MAINT_CHARGES" id="txtmain" data-ng-model="LeaseObj.PropertyObj.PM_LC_MAINT_CHARGES" />&nbsp;
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label>Parking Charges</label>
                                    <input type="text" class="form-control" name="PM_LO_ADDI_PARK_CHRG" id="txtparking" data-ng-model="LeaseObj.PropertyObj.PM_LO_ADDI_PARK_CHRG" />&nbsp;
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Start Date Of Lease</label>
                                    <div class="input-group date" id='Effectdate'>
                                        <%--<div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>--%>
                                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" data-ng-model="LeaseObj.PropertyObj.PM_LAD_START_DT_AGREEMENT"
                                            id="startdate" name="PM_LAD_START_DT_AGREEMENT" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask />
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('Effectdate')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Expiry Date Of Agreement</label>
                                    <div class="input-group date" id='Expiry'>
                                        <%--<div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>--%>
                                        <%-- <div class="input-group date" style="width: 207px" id='Expiry'>--%>
                                        <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="PM_LAD_EXP_DT_AGREEMENT" name="PM_LAD_EXP_DT_AGREEMENT"
                                            data-ng-model="LeaseObj.PropertyObj.PM_LAD_EXP_DT_AGREEMENT" />
                                        <span class="input-group-addon">
                                            <span class="fa fa-calendar" onclick="setup('Expiry')"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix">
                        <div class="box-footer text-right">
                            <div class="form-group">
                                <%--<input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-show="ActionStatus==0" />--%>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Interval Type </label>
                                <select class="form-control" data-ng-options="item.NAME as item.value for item in LeaseObj.IntervalType track by item.ID"
                                    data-ng-model="LeaseObj.frmLED.Intrveltype" data-ng-change="LeaseObj.IntervalTypeFn()">
                                    <option value="" data-ng-selected="true">select</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12" data-ng-show="escTypeShow">
                            <div class="form-group">
                                <label>Escalation Type</label>
                                <select class="form-control" data-ng-options="item.NAME as item.value for item in LeaseObj.EscalationType track by item.ID"
                                    data-ng-model="LeaseObj.frmLED.ESCType" data-ng-change="LeaseObj.ESCTypeFn()">
                                    <option value="" data-ng-selected="true">select</option>
                                </select>
                            </div>
                        </div>


                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Amount In </label>
                                <select class="form-control" data-ng-options="item.NAME as item.value for item in LeaseObj.AmountIn track by item.ID"
                                    data-ng-model="LeaseObj.frmLED.Amountin" data-ng-change="LeaseObj.EscClear()">
                                    <option value="" data-ng-selected="true">select</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12" data-ng-show="interveldur">
                            <div class="form-group">
                                <label>Interval Duration</label>
                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="LeaseObj.frmLED.Interval_Duration" class="form-control" data-ng-change="LeaseObj.IntervalDurationfn(fsgdf)" />
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default" id="RentRevisionPanel">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Rent Revision
                                </a>
                                <%--<input type="button" value="Add Escalation" class="btn btn-primary custom-button-color" data-ng-show="btnAddesc" />--%>
                            </h4>
                        </div>

                        <div class="collapse show">

                            <div class="panel-body color" data-ng-show="YearandMonthWiseEsc">
                                <div class="col-md-2 col-sm-6 col-xs-12" data-ng-repeat="sp in LeaseObj.Esclationvalues">
                                    <%--data-ng-if="frmLED.Intrveltype!='Flexy'"--%>
                                    <div class="form-group">
                                        <label>{{sp.ESCALATION}}:{{sp.DATE}}</label>
                                        <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="sp.VALUE" width="150px" cssclass="form-control" />
                                        <%--<input type="checkbox" data-ng-model="sp.CHECKED" data-ng-true-value="'Y'" data-ng-false-value="'N'" />--%>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body color" data-ng-show="FlexibleWiseEsc">
                                <div class="clearfix">
                                    <%--data-ng-if="frmLED.Intrveltype=='Flexy'" data-ng-repeat="sp in LeaseObj.Esclationvalues"--%>


                                    <div class="row">
                                        <div class="col-md-5 col-sm-12 col-xs-12" data-ng-show="Noofesc">
                                            <div class="form-group">
                                                <label>Basic Rent (No. of Escalations) </label>

                                                <div class="cssCircle minusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px; margin-right: 10px" data-ng-click="add('Minus','BasicRent')">
                                                    &#8211;
                                                </div>
                                                <div class="cssCircle plusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px" data-ng-click="add('Add','BasicRent')">
                                                    &#43;
                                                </div>

                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="LeaseObj.frmLED.Noofescalation" class="form-control" data-ng-change="LeaseObj.NoofEscalationfn('BasicRent')" />
                                            </div>
                                        </div>

                                        <div class="col-md-10 col-sm-16 col-xs-22" style="width: auto; margin-top: 10px">
                                            <div class="form-group form-inline" data-ng-repeat="sp in LeaseObj.Esclationvalues | filter: {Type: 'BasicRent'}">
                                                <label style="margin-right: 10px">{{sp.ESCALATION}}:{{sp.DATE}}</label>
                                                &nbsp;&nbsp;&nbsp;
                                                         <label>Years</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addyear" data-ng-change="LeaseObj.YearonchangeFn('BasicRent',$index)" />
                                                <label style="margin-left: 10px">Months</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addmonth" data-ng-change="LeaseObj.YearonchangeFn('BasicRent',$index)" />
                                                <label style="margin-left: 10px">Days</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addday" data-ng-change="LeaseObj.YearonchangeFn('BasicRent',$index)" />
                                                <label style="margin-left: 10px">Rent free</label>
                                                <input type="checkbox" data-ng-model="sp.payFree" />
                                                <label style="margin-left: 10px">(%)/Value </label>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="sp.VALUE" />
                                                <input type="hidden" data-ng-model="sp.EsclationDate" />
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>



                                    <div class="row">
                                        <div class="col-md-5 col-sm-12 col-xs-12" data-ng-show="Noofesc">
                                            <div class="form-group">
                                                <label>Security Deposit(No. of Escalations)</label>
                                                <div class="cssCircle minusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px; margin-right: 10px" data-ng-click="add('Minus','SecurityDeposit')">
                                                    &#8211;
                                                </div>
                                                <div class="cssCircle plusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px" data-ng-click="add('Add','SecurityDeposit')">
                                                    &#43;
                                                </div>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="LeaseObj.frmLED.Noofsdescalation" class="form-control" data-ng-change="LeaseObj.NoofEscalationfn('SecurityDeposit')" />
                                            </div>
                                        </div>

                                        <div class="col-md-10 col-sm-16 col-xs-22" style="width: auto; margin-top: 10px">
                                            <div class="form-group form-inline" data-ng-repeat="sp in LeaseObj.Esclationvalues | filter: {Type: 'SecurityDeposit'}">
                                                <label style="margin-right: 10px">{{sp.ESCALATION}}:{{sp.DATE}}</label>
                                                &nbsp;&nbsp;&nbsp;
                                                         <label>Years</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addyear" data-ng-change="LeaseObj.YearonchangeFn('SecurityDeposit',$index)" />
                                                <label style="margin-left: 10px">Months</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addmonth" data-ng-change="LeaseObj.YearonchangeFn('SecurityDeposit',$index)" />
                                                <label style="margin-left: 10px">Days</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addday" data-ng-change="LeaseObj.YearonchangeFn('SecurityDeposit',$index)" />
                                                <label style="margin-left: 10px">Rent free</label>
                                                <input type="checkbox" data-ng-model="sp.payFree" />
                                                <label style="margin-left: 10px">(%)/Value </label>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="sp.VALUE" />
                                                <input type="hidden" data-ng-model="sp.EsclationDate" />
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-12 col-xs-12" data-ng-show="Noofesc">
                                            <div class="form-group">
                                                <label>Maintenance Charges(No. of Escalations)</label>
                                                <div class="cssCircle minusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px; margin-right: 10px" data-ng-click="add('Minus','Maintenance')">
                                                    &#8211;
                                                </div>
                                                <div class="cssCircle plusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px" data-ng-click="add('Add','Maintenance')">
                                                    &#43;
                                                </div>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="LeaseObj.frmLED.Noofmcescalation" class="form-control" data-ng-change="LeaseObj.NoofEscalationfn('Maintenance')" />
                                            </div>
                                        </div>
                                        <div class="col-md-10 col-sm-16 col-xs-22" style="width: auto; margin-top: 10px">
                                            <div class="form-group form-inline" data-ng-repeat="sp in LeaseObj.Esclationvalues | filter: {Type: 'Maintenance'}">
                                                <label style="margin-right: 10px">{{sp.ESCALATION}}:{{sp.DATE}}</label>
                                                &nbsp;&nbsp;&nbsp;
                                                         <label>Years</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addyear" data-ng-change="LeaseObj.YearonchangeFn('Maintenance',$index)" />
                                                <label style="margin-left: 10px">Months</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addmonth" data-ng-change="LeaseObj.YearonchangeFn('Maintenance',$index)" />
                                                <label style="margin-left: 10px">Days</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addday" data-ng-change="LeaseObj.YearonchangeFn('Maintenance',$index)" />
                                                <label style="margin-left: 10px">Rent free</label>
                                                <input type="checkbox" data-ng-model="sp.payFree" />
                                                <label style="margin-left: 10px">(%)/Value </label>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="sp.VALUE" />
                                                <input type="hidden" data-ng-model="sp.EsclationDate" />
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-12 col-xs-12" data-ng-show="Noofesc">
                                            <div class="form-group">
                                                <label>Furniture & Amenities Charges (No. of Escalations)</label>
                                                <div class="cssCircle minusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px; margin-right: 10px" data-ng-click="add('Minus','Amenities')">
                                                    &#8211;
                                                </div>
                                                <div class="cssCircle plusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px" data-ng-click="add('Add','Amenities')">
                                                    &#43;
                                                </div>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="LeaseObj.frmLED.NoofAmescalation" class="form-control" data-ng-change="LeaseObj.NoofEscalationfn('Amenities')" />
                                            </div>
                                        </div>

                                        <div class="col-md-10 col-sm-16 col-xs-22" style="width: auto; margin-top: 10px">
                                            <div class="form-group form-inline" data-ng-repeat="sp in LeaseObj.Esclationvalues | filter: {Type: 'Amenities'}">

                                                <label style="margin-right: 10px">{{sp.ESCALATION}}:{{sp.DATE}}</label>
                                                &nbsp;&nbsp;&nbsp;
                                                         <label>Years</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addyear" data-ng-change="LeaseObj.YearonchangeFn('Amenities',$index)" />
                                                <label style="margin-left: 10px">Months</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addmonth" data-ng-change="LeaseObj.YearonchangeFn('Amenities',$index)" />
                                                <label style="margin-left: 10px">Days</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addday" data-ng-change="LeaseObj.YearonchangeFn('Amenities',$index)" />
                                                <label style="margin-left: 10px">Rent free</label>
                                                <input type="checkbox" data-ng-model="sp.payFree" />
                                                <label style="margin-left: 10px">(%)/Value </label>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="sp.VALUE" />
                                                <input type="hidden" data-ng-model="sp.EsclationDate" />
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-12 col-xs-12" data-ng-show="Noofesc">
                                            <div class="form-group">
                                                <label>Parking Charges(No. of Escalations)</label>
                                                <div class="cssCircle minusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px; margin-right: 10px" data-ng-click="add('Minus','Parking')">
                                                    &#8211;
                                                </div>
                                                <div class="cssCircle plusSign" style="display: inline-block; margin-left: 10px; margin-top: 10px" data-ng-click="add('Add','Parking')">
                                                    &#43;
                                                </div>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="LeaseObj.frmLED.NoofPcescalation" class="form-control" data-ng-change="LeaseObj.NoofEscalationfn('Parking')" />
                                            </div>
                                        </div>

                                        <div class="col-md-10 col-sm-16 col-xs-22" style="width: auto; margin-top: 10px">
                                            <div class="form-group form-inline" data-ng-repeat="sp in LeaseObj.Esclationvalues | filter: {Type: 'Parking'}">
                                                <label style="margin-right: 10px">{{sp.ESCALATION}}:{{sp.DATE}}</label>
                                                &nbsp;&nbsp;&nbsp;
                                                         <label>Years</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addyear" data-ng-change="LeaseObj.YearonchangeFn('Parking',$index)" />
                                                <label style="margin-left: 10px">Months</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addmonth" data-ng-change="LeaseObj.YearonchangeFn('Parking',$index)" />
                                                <label style="margin-left: 10px">Days</label>
                                                <input type="text" onkeypress="return isNumberKey(event)" style="width: 50px" data-ng-model="sp.addday" data-ng-change="LeaseObj.YearonchangeFn('Parking',$index)" />
                                                <label style="margin-left: 10px">Rent free</label>
                                                <input type="checkbox" data-ng-model="sp.payFree" />
                                                <label style="margin-left: 10px">(%)/Value </label>
                                                <input type="text" onkeypress="return isNumberKey(event)" data-ng-model="sp.VALUE" />
                                                <input type="hidden" data-ng-model="sp.EsclationDate" />
                                                <br />
                                                <br />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="box-footer text-right">
                                    <%--<div class="col-md-2 col-sm-6 col-xs-12">--%>
                                    <div class="form-group">
                                        <input type="submit" value="Submit" class="btn btn-primary custom-button-color" data-ng-click="LeaseSubmit('S')" />
                                    </div>
                                    <%--</div>--%>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12" data-ng-if="pgVar.activeDiv">
                                    <%--<input type="text" class="form-control" data-ng-change="pgVar.filterFn(pgVar.filterName)" data-ng-model="pgVar.filterName" placeholder="Filter by any..." style="width: 20%; height: 20%" />--%>
                                    <div data-ag-grid="Escgrid" class="ag-blue" style="height: 310px; width: 750px"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <%--  </div>
            </div>
        </div>--%>
    </div>
</body>
<%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
<script type="text/javascript" defer>
    function setup(id) {
        $('#' + id).datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
    //$(function () {
    //    //Datemask2 MM/dd/yyyy
    //    $("#datemask2").inputmask("dd/mm/yyyy", { "placeholder": "dd/mm/yyyy" });
    //    //Money Euro
    //    $("[data-mask]").inputmask();

    //});

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

</script>

<script>
    var app = angular.module('QuickFMS', ["agGrid", "isteven-multi-select"]);
</script>
<%--<script src="../../HDM/HDM_Webfiles/js/SLA.js"></script>--%>
<script src="../../Scripts/Lodash/lodash.min.js"></script>
<script src="../JS/LeaseEsc.js"></script>
<script src="../../SMViews/Utility.js"></script>
<script src="../../Scripts/DropDownCheckBoxList/isteven-multi-select.js"></script>

</html>
