﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PropertyManagement_Views_ViewLeasePayments : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        Bindpayments(Request.QueryString["Lease"], Request.QueryString["lsno"]); 

         
    }
    public void Bindpayments(String Renew,string lsno)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LL_PAYMENT_HISTORY");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@REQID", Renew, DbType.String);
        sp.Command.AddParameter("@SNO", lsno, DbType.String);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvLandlordPay.DataSource = ViewState["reqDetails"];
        gvLandlordPay.DataBind();
        
        // Panel2.Visible = true;
    }
    protected void gvLandlordPay_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gvLandlordPay.PageIndex = e.NewPageIndex;
        gvLandlordPay.DataSource = ViewState["reqDetails"];
        gvLandlordPay.DataBind();
    }
}