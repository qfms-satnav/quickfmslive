Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.Drawing

Partial Class WorkSpace_SMS_Webfiles_EditPropertyDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            fillReqs()
            If Request.QueryString("back") <> Nothing Then
                panel1.Visible = True
                btnSubmit.Text = "Add more properties to " + Request.QueryString("back")
                GetProperties(Request.QueryString("back"))
            Else
                panel1.Visible = False
            End If
        End If
    End Sub

    Private Sub fillReqs()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTY_REQS")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvReqs.DataSource = ds
        gvReqs.DataBind()
    End Sub

    Protected Sub gvReqs_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvReqs.PageIndexChanging
        gvReqs.PageIndex = e.NewPageIndex()
        fillReqs()
    End Sub

    Protected Sub gvrReqProperties_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvrReqProperties.PageIndexChanging
        gvrReqProperties.PageIndex = e.NewPageIndex()
        gvrReqProperties.DataSource = Session("reqDetails")
        gvrReqProperties.DataBind()
    End Sub

    Private Sub GetProperties(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTIES_BY_REQID")
        sp.Command.AddParameter("@REQ_ID", Reqid, DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvrReqProperties.DataSource = ds
        gvrReqProperties.DataBind()
        Session("reqDetails") = ds
    End Sub

    Protected Sub gvReqs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqs.RowCommand
        Try
            If e.CommandName = "GetProperties" Then
                Dim gvr As GridViewRow = CType(((CType(e.CommandSource, LinkButton)).NamingContainer), GridViewRow)
                Dim RowIndex1 As Integer = gvr.RowIndex
                For Each row1 As GridViewRow In gvReqs.Rows
                    If row1.RowIndex = RowIndex1 Then
                        gvReqs.Rows(RowIndex1).BackColor = System.Drawing.Color.SkyBlue
                    Else
                        gvReqs.Rows(row1.RowIndex).BackColor = Color.Empty
                    End If
                Next
                panel1.Visible = True
                hdnReqid.Value = e.CommandArgument
                btnSubmit.Text = "Add more properties to " + hdnReqid.Value
                GetProperties(hdnReqid.Value)
            Else
                panel1.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub gvrReqProperties_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvrReqProperties.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim lbl As Label = e.Row.FindControl("lblStsValue")
            Dim sts As String = DirectCast(e.Row.DataItem, System.Data.DataRowView).Row.ItemArray(13)
            'e.Row.Cells(9).Enabled = IIf(sts <> "1", True, False)

            If sts = "4001" Then
                btnSubmit.Enabled = True
                ' e.Row.Cells(11).Text = "Edit"
                Dim txt = e.Row.Cells(11).Text
            Else
                Dim txt = e.Row.Cells(11).Text
                btnSubmit.Enabled = False
                ' e.Row.Cells(11).Text = "View"
            End If
        End If
    End Sub

    Protected Sub gvrReqProperties_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvrReqProperties.RowCommand
        Try
            If e.CommandName = "VIEW" Then
                Dim lnkView As LinkButton = DirectCast(e.CommandSource, LinkButton)
                Dim dealId As String = lnkView.CommandArgument
                Dim sts = gvrReqProperties.Rows(0).Cells(10).Text
            Else

            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Response.Redirect("~/PropertyManagement/Views/frmAddPropertyDetails.aspx?Rid=" + hdnReqid.Value)
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTIES_BY_REQID")
        sp.Command.AddParameter("@REQ_ID", hdnReqid.Value, DbType.String)
        sp.Command.AddParameter("@PROP_NAME", txtPropName.Text, DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvrReqProperties.DataSource = ds
        gvrReqProperties.DataBind()
        Session("reqDetails") = ds
    End Sub
    Protected Sub btnsrch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsrch.Click
        Try
            panel1.Visible = False

            Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTIES_BY_SEARCH")
            sp.Command.AddParameter("@PROP_NAME", txtSearch.Text, DbType.String)
            sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
            Dim ds As New DataSet
            ds = sp.GetDataSet
            gvReqs.DataSource = ds
            gvReqs.DataBind()
            Session("reqDetails") = ds
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try


    End Sub
End Class
