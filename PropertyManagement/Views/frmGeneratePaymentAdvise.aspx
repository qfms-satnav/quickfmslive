<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmGeneratePaymentAdvise.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmGeneratePaymentAdvise" Title="Generate Payment Advise" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />--%>
    <%-- <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <style>
        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
    <script type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>

</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Add Tenant" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Update Work Payment Details</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>Update Work Payment Details</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" />
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="row">
                                    <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                    </asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Work Request <span style="color: red;">*</span></label>
                                <asp:RequiredFieldValidator ID="cvProperty" runat="server" ControlToValidate="ddlWorkRequest"
                                    Display="None" ErrorMessage="Please Select Work Request" ValidationGroup="Val1"
                                    InitialValue="0" Enabled="true"></asp:RequiredFieldValidator>
                                <div class="col-md-12">
                                    <asp:DropDownList ID="ddlWorkRequest" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">City</label>
                                <div class="col-md-12">
                                    <asp:TextBox ID="txtBuilding" runat="server" CssClass="form-control"
                                        MaxLength="50" Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Property</label>
                                <div class="col-md-12">
                                    <asp:TextBox ID="txtproperty" runat="server" CssClass="form-control"
                                        MaxLength="50" Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Work Title</label>
                                <div class="col-md-12">
                                    <asp:TextBox ID="txtWorkTitle" runat="server" CssClass="form-control"
                                        MaxLength="50" Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Work Specifications</label>
                                <div class="col-md-12">
                                    <asp:TextBox ID="txtWorkSpec" runat="server" CssClass="form-control"
                                        Rows="3" TextMode="SingleLine" MaxLength="1000" Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Estimated Amount</label>
                                <div class="col-md-12">
                                    <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Vendor Name</label>
                                <div class="col-md-12">
                                    <asp:TextBox ID="txtVendorName" runat="server" CssClass="form-control"
                                        Enabled="False"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Work Status</label>
                                <div class="col-md-12">
                                    <asp:DropDownList ID="ddlwstatus" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true" Enabled="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Payment Mode <span style="color: red;">*</span></label>
                                <asp:RequiredFieldValidator ID="rfvpay" runat="server" ControlToValidate="ddlpay"
                                    Display="None" ErrorMessage="Please Select Payment Mode" ValidationGroup="Val1"
                                    InitialValue="--Select Payment Mode--" Enabled="true"></asp:RequiredFieldValidator>
                                <div class="col-md-12">
                                    <asp:DropDownList ID="ddlpay" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true">
                                        <asp:ListItem>--Select Payment Mode--</asp:ListItem>
                                        <asp:ListItem Value="1">Cheque</asp:ListItem>
                                        <asp:ListItem Value="2">Cash</asp:ListItem>
                                        <asp:ListItem Value="3">Bank Transfer</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="col-md-12 control-label">Remarks</label>
                                <div class="col-md-12">
                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" TextMode="MultiLine" Height="30%" MaxLength="250"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div id="panel1" runat="server">
                        <div class="panel panel-default " role="tab" runat="server" id="div3">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Cheque Details</a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="panel-collapse collapse in">
                                <div class="panel-body color">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Cheque No<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvCheque" runat="server" ControlToValidate="txtCheque"
                                                        Display="None" ErrorMessage="Please Enter Cheque Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="rvcheque" Display="None" runat="server" ControlToValidate="txtCheque"
                                                        ErrorMessage="Invalid Cheque Number" ValidationExpression="^[A-Za-z0-9]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div class="col-md-12">
                                                        <div onmouseover="Tip('Enter Numbers and No Special Characters allowed ')" onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtCheque" runat="Server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Bank Name<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                                                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div class="col-md-12">
                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                            onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Account Number<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                                                        ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div class="col-md-12">
                                                        <div onmouseover="Tip('Enter  Numbers only and No Special Characters allowed ')"
                                                            onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtAccNo" runat="Server" CssClass="form-control"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="panel2" runat="Server">
                        <div class="panel panel-default " role="tab" runat="server" id="div1">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">Bank Details</a>
                                </h4>
                            </div>
                            <div id="collapseFive" class="panel-collapse collapse in">
                                <div class="panel-body color">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Issuing Bank Name<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIBankName"
                                                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                        ControlToValidate="txtIBankName" ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div class="col-md-12">
                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                            onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtIBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">Deposited Bank<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtDeposited"
                                                        Display="None" ErrorMessage="Please Enter Deposited Bank" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtDeposited"
                                                        ErrorMessage="Enter Valid Bank Branch" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                        ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div class="col-md-12">
                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                            onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtDeposited" runat="Server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12">
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label">IFSC Code<span style="color: red;">*</span></label>
                                                    <asp:RequiredFieldValidator ID="rfvIFCB" runat="server" ControlToValidate="txtIFCB"
                                                        Display="none" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                    <asp:RegularExpressionValidator ID="REVIFCB" Display="None" runat="server" ControlToValidate="txtIFCB"
                                                        ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val1"></asp:RegularExpressionValidator>
                                                    <div class="col-md-12">
                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                            onmouseout="UnTip()">
                                                            <asp:TextBox ID="txtIFCB" runat="Server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <div class="form-group">

                                <asp:Button ID="btnsubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1"
                                    CausesValidation="true" />

                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <%--</div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>



