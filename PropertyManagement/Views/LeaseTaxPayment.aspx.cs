﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using SubSonic;
using System.Drawing;

public partial class PropertyManagement_Views_LeaseTaxPayment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string path = HttpContext.Current.Request.Url.AbsolutePath;
        string host = HttpContext.Current.Request.Url.Host;
        SqlParameter[] param = new SqlParameter[2];
        param[0] = new SqlParameter("@ROL_ID", SqlDbType.VarChar, 50);
        param[0].Value = Session["UID"];
        param[1] = new SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200);
        param[1].Value = path;
        using (SqlDataReader sdr = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param))
        {
            if (Session["UID"].ToString() == "")
            {
                Response.Redirect(Application["FMGLogout"].ToString());
            }
            else
            {
                if (sdr.HasRows)
                {
                }
                else
                {
                    Response.Redirect(Application["FMGLogout"].ToString());
                }
            }
        }
        if (!IsPostBack)
        {
            BindGrid();
            panel1.Visible = false;         
            
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA_SEARCH_BY");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@LEASE_ID", txtReqId.Text, DbType.String);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
    }

    protected void txtreset_Click(object sender, EventArgs e)
    {
        BindGrid();
        txtReqId.Text = "";
    }

    private void BindGrid()
    {
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_LEASE_DATA");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        ViewState["reqDetails"] = ds;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();

    }
    
    protected void gvItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvItems.PageIndex = e.NewPageIndex;
        gvItems.DataSource = ViewState["reqDetails"];
        gvItems.DataBind();
        lblMsg.Text = "";
    }
    protected void gvItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblMsg.Text = "";
        if (e.CommandName == "Tax")
        {
            GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            int RowIndex = gvr.RowIndex;
            foreach (GridViewRow row in gvItems.Rows)
            {
                if (row.RowIndex == RowIndex)
                {
                    gvItems.Rows[RowIndex].BackColor = System.Drawing.Color.SkyBlue;
                }
                else
                {
                    gvItems.Rows[row.RowIndex].BackColor = Color.Empty;
                }
            }
            LinkButton lnkLease = (LinkButton)e.CommandSource;
            GridViewRow gvRow = (GridViewRow)lnkLease.NamingContainer;
            Label lblLseName = (Label)gvRow.FindControl("lblLseName");
            Label llsno = (Label)gvRow.FindControl("LBLSno");
            panel1.Visible = true;

             SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_BIND_LANDLORDS");
             sp.Command.AddParameter("@AUR_ID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
             sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
             sp.Command.AddParameter("@LEASE_ID", lblLseName.Text, DbType.String);
            sp.Command.AddParameter("@LEASE_SNO", llsno.Text, DbType.String);
            gvLandlords.DataSource = sp.GetDataSet();
             gvLandlords.DataBind();
        }
        else
        {
            panel1.Visible = false;
        }         
        
    }
  

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
           foreach (GridViewRow row in gvLandlords.Rows)
            {
                
                Label lblreqid = (Label)row.FindControl("lbllname");
                DropDownList ddlTDS = (DropDownList)row.FindControl("ddlTDS");
                Label lblSNO = (Label)row.FindControl("LBLSNO");
                Label lessnoo = (Label)row.FindControl("LabSno");

                SqlParameter[] param = new SqlParameter[6];
                    param[0] = new SqlParameter("@LEASEID", lblreqid.Text);
                    param[1] = new SqlParameter("@TDS", ddlTDS.SelectedItem.Value);
                    param[2] = new SqlParameter("@TDS_REMARKS", txtRemarks.Text);
                    param[3] = new SqlParameter("@AUR_ID", Session["UID"].ToString());
                    param[4] = new SqlParameter("@SNO", lblSNO.Text);
                param[5] = new SqlParameter("@lesSNO", lessnoo.Text);
                SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_LEASE_TAX_PAYMENT", param);

                    lblMsg.Visible = true;
                    lblMsg.Text = "TDS for Landlord(s) Successfully Confirmed";
                panel1.Visible = false;
            }


        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
    }
   
}