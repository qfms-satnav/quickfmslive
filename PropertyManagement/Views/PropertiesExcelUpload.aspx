﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PropertiesExcelUpload.aspx.cs" Inherits="PropertyManagement_Views_PropertiesExcelUpload" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style type="text/css">
        .panel-heading {
            height: auto;
            background: white;
            border-radius: 5px 5px 0px 0px;
        }

        .panel-title {
            font-weight: 400;
            color: #666;
            font-size: 16px;
        }

        .panel-body {
            background: white;
            border-radius: 0px 0px 5px 5px;
        }

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
</head>
<body>
    <div class="animsition">
        <%--<div class="container-fluid page-content-inner">--%>
        <%--  <div ba-panel ba-panel-title="Shift Master" ba-panel-class="with-scroll">
            <div class="bgc-gray p-20 m-b-25">
                <div class="panel-heading">
                    <h3 class="panel-title">Upload Property</h3>
                </div>--%>
        <div class="widgets">
            <h3>Upload Property </h3>
        </div>
        <div class="card">
            <%--<div class="panel-body" style="padding-right: 50px;">--%>
            <form id="form1" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                <div class="panel panel-default " role="tab" id="div5" style="width: 700px;">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#Insurance">Click here:Please follow the below instuctions</a>
                        </h4>
                    </div>
                    <div id="Insurance" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-10 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>1.Enter all amount related fields in numbers</label><br />
                                        <label>2.Enter all area related fields in numbers</label>
                                        <label>3.Enter all dates related fields in(DD-MM-YYYY)</label>
                                        <label>4.Notice period in months Enter only number(ex.3)</label>
                                        <label>5.Lock In period Enter only number(ex.3, if Lock In period Nill enter 0)</label>
                                        <label>6.No Of car parking Enter only number(ex.3)</label>
                                        <label>7.Escalation Enter only (Yes/No)</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="form-group">
                            <div class="row">
                                <asp:Label ID="lblmsg" runat="server" Style="font: caption" CssClass="col-md-12 control-label" ForeColor="Red">
                                </asp:Label>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="clearfix">
                        <div class="col-md-3">
                            <label>Upload File :</label>
                            <asp:FileUpload ID="FileUpload1" runat="server" CssClass="form-control-file" />
                        </div>
                        <div class="col-md-9">
                            <asp:Button ID="btnImport" runat="server" Text="Import Excel" OnClick="ImportExcel1" CssClass="btn btn-success" />

                            <a class='btn btn-primary custom-button-color' id="btnDownload" href="../../AddPropertyExcelFile/AddProperty_Sample_Excel.xlsx">Download Sample Excel</a>

                        </div>
                    </div>
                </div>

                <div id="ParsedResult" runat="server" style="margin-top: 50px;">
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-8">
                            <h4>
                                <label id="lblValidationMsg" runat="server" style="color: red"></label>
                            </h4>
                        </div>


                        <div class="col-md-4">
                            <%--<asp:Button ID="Button1" runat="server" Text="Validate Excel" OnClick="Validate_Click" CssClass="btn btn-success custom-button-color" />--%>
                            <asp:Button ID="UploadExcel" runat="server" Text="Upload Excel" OnClick="UploadExcel_Click" CssClass="btn btn-primary custom-button-color" />
                        </div>

                    </div>

                    <div class="row" style="margin-top: 5px; margin-left: 10px;">
                        <div style="width: 100%; height: 100%; overflow-x: scroll; max-height: 300px;" runat="server" id="ExcelParserDiv">
                            <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" CssClass="table GridStyle" GridLines="none">
                                <Columns>
                                    <asp:TemplateField HeaderText="SNO." ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Zone">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LCM_ZONE" Text='<%# Eval("LCM_ZONE") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="PropertyState" Text='<%# Eval("State") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="CITY" Text='<%# Eval("CITY") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location Code">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LOCATION_CODE" Text='<%# Eval("LOCATION_CODE") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location Name">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LOCATION" Text='<%# Eval("LOCATION") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tower">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="TOWER" Text='<%# Eval("TOWER") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Floor">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="FLOOR" Text='<%# Eval("FLOOR") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Type">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="PROP_TYPE" Text='<%# Eval("PROP_TYPE") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Request Type">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="REQUEST_TYPE" Text='<%# Eval("REQUEST_TYPE") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Nature">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="PROP_NATURE" Text='<%# Eval("PROP_NATURE") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Acquisition Through">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="ACQ_TRH" Text='<%# Eval("ACQ_TRH") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Entity">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="ENTITY" Text='<%# Eval("ENTITY") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Name">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="PROP_NAME" Text='<%# Eval("PROP_NAME") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Address">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="PROP_ADDR" Text='<%# Eval("PROP_ADDR") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Owner Name">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Owner_Name" Text='<%# Eval("Owner_Name") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Owner Mobile Number">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Owner_Mobile_Number" Text='<%# Eval("Owner_Mobile_Number") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C/B/SB Area">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="C_B_SB_Area" Text='<%# Eval("C_B_SB_Area") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Carpet Area">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Area" Text='<%# Eval("Area") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rentable Area">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Chargeable_Area" Text='<%# Eval("Chargeable_Area") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rent per sq.ft. (on carpet)(Rs.)">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Rentper_sqft" Text='<%# Eval("Rentper_sqft") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Basic Rent P.M">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="BasicRent" Text='<%# Eval("BasicRent") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amenities P.M">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LandlordAmenities" Text='<%# Eval("LandlordAmenities") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maintenance P.M">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LandlordMaintenance" Text='<%# Eval("LandlordMaintenance") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Registration Charges">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Registration_Charges" Text='<%# Eval("Registration_Charges") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Car Parking Charges">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="CarParkingCharges" Text='<%# Eval("CarParkingCharges") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total Rent P.M">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="TotalRent" Text='<%# Eval("TotalRent") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Security Deposit">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="SecurityDeposit" Text='<%# Eval("SecurityDeposit") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Electricity Deposit">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Electricity_Deposit" Text='<%# Eval("Electricity_Deposit") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Effective Date Of Agreement(DD-MM-YYYY)">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LeaseSignDate" Text='<%# Eval("LeaseSignDate") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Start Date Of Agreement(DD-MM-YYYY)">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LeaseStartDate" Text='<%# Eval("LeaseStartDate") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Expiry Date Of Agreement(DD-MM-YYYY)">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LeaseEndDate" Text='<%# Eval("LeaseEndDate") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rent Commencement Date(DD-MM-YYYY)">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="RentStartDate" Text='<%# Eval("RentStartDate") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Escalation(Yes/No)">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Escalation" Text='<%# Eval("Escalation") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lock In">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LockinPeriod" Text='<%# Eval("LockinPeriod") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Notice Period in Months">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="NoticePeriodinMonths" Text='<%# Eval("NoticePeriodinMonths") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nos Car Parking">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="No_Car_Parking" Text='<%# Eval("No_Car_Parking") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Landlord Name">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LandlordName" Text='<%# Eval("LandlordName") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Landlord Address">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Address" Text='<%# Eval("Address") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Landlord State">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LL_State" Text='<%# Eval("LL_State") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Landlord City">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="LL_City" Text='<%# Eval("LL_City") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Landlord Contact Details">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="ContactDetails" Text='<%# Eval("ContactDetails") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pin Code">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Pin_Code" Text='<%# Eval("Pin_Code") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PAN No">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="PANNo" Text='<%# Eval("PANNo") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="Email" Text='<%# Eval("Email") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GST">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="GST" Text='<%# Eval("GST") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GSTNumber">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="GSTNumber" Text='<%# Eval("GSTNumber") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Tax Applicable">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="PropertyTaxApplicable" Text='<%# Eval("PropertyTaxApplicable") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Tax Iibility">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="PropertyTax" Text='<%# Eval("PropertyTax") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GL code/Vendor Code">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" Width="150" ID="GL_code" Text='<%# Eval("GL_code") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <%--</div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>


