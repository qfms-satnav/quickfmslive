﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;


public partial class PropertyManagement_Views_RentalInvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            LoadReport();
        }
    }

    public void LoadReport()
    {
        string rid;
        rid = Request.QueryString["rid"];
        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_TEN_LOAD_RENTAL_INVOICE");
        sp.Command.AddParameter("@INVC_NO", rid, DbType.String);
        DataSet ds = new DataSet();
        ds = sp.GetDataSet();
        if (ds.Tables[0].Rows.Count > 0)
        {
            ReportDataSource rds = new ReportDataSource();
            rds.Name = "RentalInvoice";
            rds.Value = ds.Tables[0];
            ReportViewer1.Reset();
            ReportViewer1.LocalReport.DataSources.Add(rds);
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports_RDLC/Property_Mgmt/RentalInvReport.rdlc");       
            ReportViewer1.SizeToReportContent = true;
            ReportViewer1.Visible = true;
            ReportViewer1.LocalReport.EnableExternalImages = true;
            string imagePath = BindLogo();
            ReportParameter parameter = new ReportParameter("Imagepath", imagePath);
            ReportViewer1.LocalReport.SetParameters(parameter);
            // ' Dim imagePath As String = New Uri(Server.MapPath("~/BootStrapCSS/images/logo_quick.gif")).AbsoluteUri
            // 'Dim parameter As New ReportParameter("ImagePath", imagePath)
            // 'ReportViewer1.LocalReport.SetParameters(parameter)
            ReportViewer1.LocalReport.DisplayName = "Rental Invoice";
            ReportViewer1.LocalReport.Refresh();
        }
    }

    public string BindLogo()
    {
        // Dim imagePath As String
        SubSonic.StoredProcedure sp3 = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "Update_Get_LogoImage");
        sp3.Command.AddParameter("@type", "2", DbType.String);
        sp3.Command.AddParameter("@AUR_ID", Session["uid"], DbType.String);
        sp3.Command.AddParameter("@Tenant", Session["TENANT"], DbType.String);
        DataSet ds3 = sp3.GetDataSet();
        if (ds3.Tables[0].Rows.Count > 0)
            return "https://live.quickfms.com/BootStrapCSS/images/" + ds3.Tables[0].Rows[0]["IMAGENAME"];
        else
            return "https://live.quickfms.com/BootStrapCSS/images/yourlogo.png";
    }


}