Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic

Partial Class WorkSpace_SMS_Webfiles_frmSendRentRemainder
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using

        If Not IsPostBack Then
            gvTenantRent.PageIndex = 0
        End If
        fillgrid()
    End Sub

    Public Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROP_REMINDERS_SENT_TO_TENANTS")
        sp.Command.AddParameter("@AUR_ID", Session("UID"), DbType.String)
        gvTenantRent.DataSource = sp.GetDataSet()
        gvTenantRent.DataBind()
        'lbtn1.Visible = False
    End Sub

    Protected Sub gvTenantRent_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvTenantRent.PageIndexChanging
        gvTenantRent.PageIndex = e.NewPageIndex
        fillgrid()
    End Sub

End Class
