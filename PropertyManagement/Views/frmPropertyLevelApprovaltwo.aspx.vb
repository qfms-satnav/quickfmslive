Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_frmPropertyLevelApproval
    Inherits System.Web.UI.Page

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindAcquisitionthrough()
            fillgrid()
            BindRequestTypes()
            BindPropType()
            BindInsuranceType()
            BindUserCities()
            BindFlooringTypes()
        End If
    End Sub

    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select Flooring Type--", 0))
    End Sub

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select Request Type--", 0))
    End Sub

    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select City--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select Property Type--", 0))
    End Sub
    Private Sub BindAcquisitionthrough()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACQISITION_THROUGH")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlAcqThr.DataSource = sp3.GetDataSet()
        ddlAcqThr.DataTextField = "PN_ACQISITION_THROUGH"
        ddlAcqThr.DataValueField = "PN_ACQISITION_ID"
        ddlAcqThr.DataBind()
        ddlAcqThr.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindInsuranceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INSURANCE_TYPE")
        ddlInsuranceType.DataSource = sp.GetDataSet()
        ddlInsuranceType.DataTextField = "INSURANCE_TYPE"
        ddlInsuranceType.DataValueField = "ID"
        ddlInsuranceType.DataBind()
        ddlInsuranceType.Items.Insert(0, New ListItem("--Select Insurance Type--", 0))
    End Sub

    Private Sub GetLocationsbyCity(ByVal city As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
        sp2.Command.AddParameter("@CITY", city)
        sp2.Command.AddParameter("@USR_ID", Session("uid"))
        ddlLocation.DataSource = sp2.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            GetLocationsbyCity(ddlCity.SelectedValue)
            ddlTower.Items.Clear()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Private Sub GetTowerbyLoc(ByVal loc As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
        sp2.Command.AddParameter("@LCMID", loc, DbType.String)
        sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        ddlTower.DataSource = sp2.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, New ListItem("--Select Tower--", "0"))
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            GetTowerbyLoc(ddlLocation.SelectedValue)
        Else
            ddlTower.Items.Clear()
        End If
    End Sub

    Private Sub GetFloorsbyTwr(ByVal tower As String, ByVal cty As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
        sp2.Command.AddParameter("@TWR_ID", tower, DbType.String)
        sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@FLR_CITY_CODE", cty, DbType.String)
        sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlFloor.DataSource = sp2.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select Floor--", "0"))
        txtFloor.Text = ddlFloor.Items.Count - 1
    End Sub


    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            GetFloorsbyTwr(ddlTower.SelectedValue, ddlCity.SelectedValue)
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub

    Protected Sub txtESTD_TextChanged(sender As Object, e As EventArgs) Handles txtESTD.TextChanged
        If Integer.TryParse(txtESTD.Text, 0) Then
            txtAge.Text = DateTime.Now.Year - Convert.ToInt32(txtESTD.Text)
        Else
            txtAge.Text = ""
        End If
    End Sub

    Protected Sub ddlRecommended_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRecommended.SelectedIndexChanged
        divRecommanded.Visible = IIf(ddlRecommended.SelectedValue = "1", True, False)
    End Sub

    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

    Private Sub fillgrid()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PROPERTY_REQS_FORAPPROVAL")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvReqs.DataSource = ds
        gvReqs.DataBind()
        Session("Reqs") = ds
    End Sub

    Protected Sub gvReqs_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvReqs.PageIndexChanging
        gvReqs.PageIndex = e.NewPageIndex()
        gvReqs.DataSource = Session("Reqs")
        gvReqs.DataBind()
    End Sub

    Protected Sub gvReqs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvReqs.RowCommand
        Try
            If e.CommandName = "GetProperties" Then
                panel1.Visible = True
                hdnReqid.Value = e.CommandArgument
                GetProperties(hdnReqid.Value)
            Else
                panel1.Visible = False
            End If
            panel2.Visible = False
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub GetProperties(ByVal Reqid As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PPTS_FOR_APPROVAL")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@REQ_ID", Reqid, DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvrReqProperties.DataSource = ds
        gvrReqProperties.DataBind()
        Session("reqDetails") = ds
    End Sub

    'Protected Sub gvrReqProperties_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvrReqProperties.PageIndexChanging
    '    gvrReqProperties.PageIndex = e.NewPageIndex()
    '    gvrReqProperties.DataSource = Session("reqDetails")
    '    gvrReqProperties.DataBind()
    'End Sub

    Protected Sub gvrReqProperties_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvrReqProperties.RowCommand
        Try
            If e.CommandName = "ViewDetails" Then
                panel2.Visible = True
                hdnSno.Value = e.CommandArgument
                BindDetails(hdnSno.Value)

                'bind curr loc details
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PPTY_CURR_LOC_SUMMARY")
                sp.Command.AddParameter("@PROP_ID", hdnSno.Value, DbType.Int32)
                sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)

                Dim ds As New DataSet
                ds = sp.GetDataSet()
                If ds.Tables(0).Rows.Count > 0 Then
                    'txtLocation.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
                    lblLoc.Text = ds.Tables(0).Rows(0).Item("LCM_NAME")
                    txtpptsCount.Text = ds.Tables(0).Rows(0).Item("TOTPPTS")
                    txtMonth.Text = ds.Tables(0).Rows(0).Item("TOTRENT")
                    txtsqft.Text = ds.Tables(0).Rows(0).Item("TOTAREA")
                End If

                'panel2.Visible = True
                ' ClientScript.RegisterStartupScript(Me.[GetType](), "none", "ShowPopup()", True)
            Else
                panel2.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindDetails(ByVal sno As Integer)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PRPTY_DETAILS_TWO")
        sp.Command.AddParameter("@PROP_ID", sno, DbType.Int32)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'General Details

            txtReqID.Text = ds.Tables(0).Rows(0).Item("PM_PPT_PM_REQ_ID")
            ddlReqType.ClearSelection()
            ddlReqType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE")).Selected = True
            'ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE") = ddlReqType.SelectedValue
            ddlPprtNature.ClearSelection()
            ddlPprtNature.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_NATURE")).Selected = True
            ddlAcqThr.ClearSelection()
            ddlAcqThr.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_ACQ_THR")).Selected = True

            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE")).Selected = True
            GetLocationsbyCity(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            ddlLocation.ClearSelection()
            ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")).Selected = True
            GetTowerbyLoc(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE"))
            ddlTower.ClearSelection()
            ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")).Selected = True

            GetFloorsbyTwr(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE"), ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            ddlFloor.ClearSelection()
            For Each item As String In ds.Tables(0).Rows(0).Item("FLOORLIST").ToString().Split(",")
                ddlFloor.Items.FindByValue(item).Selected = True
            Next
            'ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_FLR_CODE")).Selected = True
            txtToilet.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOT_TOI")  'IIf(txtToilet.Text = "", 0, txtToilet.Text)
            ddlPropertyType.SelectedValue = ds.Tables(0).Rows(0).Item("PM_PPT_TYPE")
            txtPropIDName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_NAME")

            txtESTD.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ESTD")  'IIf(txtESTD.Text = "", "", Convert.ToString(txtESTD.Text))
            txtAge.Text = ds.Tables(0).Rows(0).Item("PM_PPT_AGE")   'IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            txtPropDesc.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
            txtSignageLocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SIGN_LOC")

            txtSocityName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SOC_NAME")
            txtlat.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LAT")
            txtlong.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LONG")
            txtOwnScopeWork.Text = ds.Tables(0).Rows(0).Item("PM_PPT_OWN_SCOPE")
            ddlRecommended.ClearSelection()
            ddlRecommended.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED") = True, 1, 0)
            'ddlRecommended.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED")).Selected = True
            If ddlRecommended.SelectedValue = 1 Then
                txtRecmRemarks.Visible = True
                txtRecmRemarks.Text = ds.Tables(0).Rows(0).Item("PM_PPT_RECO_REM")
            Else
                divRecommanded.Visible = False
            End If

            txtApartmentno.Text = ds.Tables(0).Rows(0).Item("PM_PPT_APT_NO")
            txtNoOfBedrooms.Text = ds.Tables(0).Rows(0).Item("PM_PPT_NO_OF_BEDRMS")

            If Not IsDBNull(ds.Tables(0).Rows(0).Item("PM_STAFFINCHARGE")) Then
                Txtstaffincharge.Text = ds.Tables(0).Rows(0).Item("PM_STAFFINCHARGE")
            End If
            'Txtstaffincharge.Text = ds.Tables(0).Rows(0).Item("PM_STAFFINCHARGE")
            If Not IsDBNull(ds.Tables(0).Rows(0).Item("PM_STAFFINCH_EMAIL")) Then
                Txtstaffinchargeemail.Text = ds.Tables(0).Rows(0).Item("PM_STAFFINCH_EMAIL")
            End If
            ''Txtstaffinchargeemail.Text = ds.Tables(0).Rows(0).Item("PM_STAFFINCH_EMAIL")
            If Not IsDBNull(ds.Tables(0).Rows(0).Item("PM_AR_FURNISHED_SEMIFURNISHED")) Then
                txtfurnished.Text = ds.Tables(0).Rows(0).Item("PM_AR_FURNISHED_SEMIFURNISHED")
            End If

            'Owner Details
            txtownrname.Text = ds.Tables(0).Rows(0).Item("PM_OWN_NAME")
            txtphno.Text = ds.Tables(0).Rows(0).Item("PM_OWN_PH_NO")
            txtPrvOwnName.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_NAME")
            txtPrvOwnPhNo.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_PH_NO")

            txtOwnEmail.Text = ds.Tables(0).Rows(0).Item("PM_OWN_EMAIL")

            'Owner Bank Details

            txtBankName.Text = ds.Tables(0).Rows(0).Item("PM_OBD_BANK_NAME")
            txtISFCCode.Text = ds.Tables(0).Rows(0).Item("PM_OBD_ISFC_CODE")
            txtBankAddress.Text = ds.Tables(0).Rows(0).Item("PM_OBD_BANK_ADDRESS")
            txtAcctnumber.Text = ds.Tables(0).Rows(0).Item("PM_OBD_ACCOUNT_NUM")

            'Area Details
            txtCarpetArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_CARPET_AREA"))
            txtBuiltupArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_BUA_AREA"))
            txtCommonArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_COM_AREA"))
            txtRentableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_RENT_AREA"))

            txtUsableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_USABEL_AREA"))
            txtSuperBulArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_SBU_AREA"))
            txtPlotArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_PLOT_AREA"))
            txtCeilingHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTC_HIGHT"))

            txtBeamBottomHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTBB_HIGHT"))
            txtMaxCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_MAX_CAP"))
            txtOptCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_OPT_CAP"))
            txtSeatingCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_SEATING_CAP"))

            ddlFlooringType.ClearSelection()
            ddlFlooringType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE")).Selected = True
            ddlFSI.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_AR_FSI") = True, 1, 0)
            txtFSI.Text = ds.Tables(0).Rows(0).Item("PM_AR_FSI_RATIO")
            txtEfficiency.Text = ds.Tables(0).Rows(0).Item("PM_AR_PREF_EFF")

            'PURCHASE DETAILS
            txtPurPrice.Text = ds.Tables(0).Rows(0).Item("PM_PUR_PRICE")
            txtPurDate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PUR_DATE"))
            txtMarketValue.Text = ds.Tables(0).Rows(0).Item("PM_PUR_MARKET_VALUE")

            'GOVT DETAILS
            txtIRDA.Text = ds.Tables(0).Rows(0).Item("PM_GOV_IRDA")
            txtPCcode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PC_CODE")
            txtGovtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PROP_CODE")
            txtUOM_CODE.Text = ds.Tables(0).Rows(0).Item("PM_GOV_UOM_CODE")

            'INSURANCE DETAILS
            ddlInsuranceType.ClearSelection()
            ddlInsuranceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_INS_TYPE")).Selected = True
            txtInsuranceVendor.Text = ds.Tables(0).Rows(0).Item("PM_INS_VENDOR")
            txtInsuranceAmt.Text = ds.Tables(0).Rows(0).Item("PM_INS_AMOUNT")
            txtInsurancePolNum.Text = ds.Tables(0).Rows(0).Item("PM_INS_PNO")

            txtInsuranceStartdate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_INS_START_DT"))
            txtInsuranceEnddate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_INS_END_DT"))

            '  If ds.Tables(1).Rows.Count > 0 Then
            gvPropdocs.DataSource = ds.Tables(1)
            gvPropdocs.DataBind()
            'End If

        End If
    End Sub

    Protected Sub gvPropdocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPropdocs.RowCommand
        Try
            If e.CommandName = "Download" Then
                Response.ContentType = ContentType
                Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
                Response.WriteFile(imgPath)
                Response.End()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    'multi approval
    Protected Sub btnApprove_Click(sender As Object, e As EventArgs) Handles btnApprove.Click
        Try
            If txtMultiRemarks.Text <> Nothing Then
                Dim ds As New DataSet
                For Each row As GridViewRow In gvrReqProperties.Rows
                    Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                    Dim lblPropSno As Label = DirectCast(row.FindControl("lblID"), Label)
                    Dim param As SqlParameter() = New SqlParameter(5) {}
                    param(0) = New SqlParameter("@PROP_SNO", lblPropSno.Text)

                    param(2) = New SqlParameter("@AUR_ID", Session("uid"))
                    param(3) = New SqlParameter("@REMARKS", txtMultiRemarks.Text)
                    param(4) = New SqlParameter("@REQ_ID", hdnReqid.Value)
                    param(5) = New SqlParameter("@CMP_ID", Session("COMPANYID"))
                    If chkselect.Checked = True Then
                        param(1) = New SqlParameter("@STA_ID", 4002)
                    Else
                        param(1) = New SqlParameter("@STA_ID", 4003)
                    End If
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_MULTI_PROP_APP_REJ", param)
                Next

                lblmsg.Text = "Property Approved Successfully"
                txtMultiRemarks.Text = String.Empty
                fillgrid()
                panel1.Visible = False
            Else
                lblmsg.Text = "Please Enter Remarks"
            End If
        Catch ex As Exception

        End Try
    End Sub

    'multi rejection
    Protected Sub btnReject_Click(sender As Object, e As EventArgs) Handles btnReject.Click
        Try
            If txtMultiRemarks.Text <> Nothing Then
                Dim ds As New DataSet
                For Each row As GridViewRow In gvrReqProperties.Rows
                    Dim chkselect As CheckBox = DirectCast(row.FindControl("chkselect"), CheckBox)
                    Dim lblPropSno As Label = DirectCast(row.FindControl("lblID"), Label)

                    Dim param As SqlParameter() = New SqlParameter(5) {}
                    param(0) = New SqlParameter("@PROP_SNO", lblPropSno.Text)
                    param(1) = New SqlParameter("@STA_ID", 4003)
                    param(2) = New SqlParameter("@AUR_ID", Session("uid"))
                    param(3) = New SqlParameter("@REMARKS", txtMultiRemarks.Text)
                    param(4) = New SqlParameter("@REQ_ID", hdnReqid.Value)
                    param(5) = New SqlParameter("@CMP_ID", Session("COMPANYID"))
                    SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_MULTI_PROP_APP_REJ", param)
                Next

                lblmsg.Text = "Property Rejected Successfully"
                txtMultiRemarks.Text = String.Empty
                fillgrid()
                panel1.Visible = False
            Else
                lblmsg.Text = "Please Enter Remarks"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PPTS_FOR_APPROVAL_BYSERACH")
        sp.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        sp.Command.AddParameter("@PROP_NAME", txtsearch.Text, DbType.String)
        sp.Command.AddParameter("@REQ_ID", hdnReqid.Value, DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvrReqProperties.DataSource = ds
        gvrReqProperties.DataBind()
        Session("reqDetails") = ds
    End Sub


End Class
