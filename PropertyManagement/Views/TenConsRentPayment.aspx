﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TenConsRentPayment.aspx.cs" Inherits="PropertyManagement_Views_TenConsRentPayment"
    MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">

            <div class="widgets">
                <h3>Rent Payment</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="Val1" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val1" />
                            <asp:ValidationSummary ID="vs2" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val2" />
                            <asp:ValidationSummary ID="vsTDS" runat="server" CssClass="alert alert-danger" ForeColor="Red" ValidationGroup="Val3" />
                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red"></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>City <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlCity" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlCity_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Location  <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                        <%--<asp:UpdatePanel id="upLoc" ChildrenAsTriggers="false" UpdateMode="Conditional" runat="server">
                                                <ContentTemplate>--%>
                                        <asp:DropDownList ID="ddlLocation" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                                        <%--</ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlCity" EventName="OnSelectedIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>--%>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Property Type <span style="color: red;">*</span> </label>
                                        <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype" Display="None"
                                            ErrorMessage="Please Select Property Type" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlproptype" runat="server" CssClass="selectpicker" data-live-search="true" AutoPostBack="true" OnSelectedIndexChanged="ddlproptype_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label>Property <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="cvbuilding" runat="server" ControlToValidate="ddlBuilding"
                                            Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                        <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="selectpicker" data-live-search="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12 text-right">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClick="btnsearch_Click"></asp:Button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div id="gridVisStatus" runat="server">
                                <div class="row">
                                    <div class="form-group col-sm-9 col-xs-6">
                                        <br />
                                        <br />
                                        <h5><b>Pending Rent Amounts </b></h5>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Enter no of payments<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtTerms"
                                                Display="None" ErrorMessage="Please enter no of payments" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtTerms" ErrorMessage="Please enter a valid number"
                                                Type="Integer" Operator="DataTypeCheck" Display="None" ValidationGroup="Val2"></asp:CompareValidator>
                                            <asp:TextBox ID="txtTerms" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtTerms_TextChanged"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>

                                <div class="row form-inline">
                                    <div class="form-group col-md-12">
                                        <asp:GridView ID="gvitems" runat="server" AllowPaging="True" AllowSorting="False" PageSize="20" AutoGenerateColumns="false"
                                            DataKeyNames="PM_RP_OUST_AMOUNT, PM_RP_ID"
                                            CssClass="table GridStyle" GridLines="none" EmptyDataText="No Records Found." OnPageIndexChanging="grdView_PageIndexChanging">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Property">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPropertyName" runat="server" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                        <%--<asp:Label ID="lblProperty" runat="server" Text='<%#Eval("PROPID")%>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Tenant Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTenCode" runat="server" Text='<%#Bind("PM_RP_TEN_CODE")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Tenant">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTenAurID" Visible="false" runat="server" Text='<%#Eval("PM_RP_TEN_AUR_ID")%>'></asp:Label>
                                                        <asp:Label ID="lblTenant" runat="server" Text='<%#Eval("TENANT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Rent Amount">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblRent" runat="server" Visible="false" Text='<%#Eval("PM_RP_TEN_RENT")%>'></asp:Label>--%>
                                                        <asp:Label ID="lblRCurr" runat="server" Text='<%#Eval("PM_RP_TEN_RENT", "{0:c2}")%>'></asp:Label>
                                                        <%--<asp:Label ID="lblRCurr" runat="server" Text='<%# String.Format(New System.Globalization.RegionInfo(Session("userculture").ToString()).ISOCurrencySymbol + " {0:0.00}", Eval("TEN_RENT_PER_SFT"))%>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Maintenance Cost">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblMaint" runat="server" Visible="false" Text='<%# Eval("PM_RP_TEN_MAINT_FEES")%>'></asp:Label>--%>
                                                        <asp:Label ID="lblMaintCurr" runat="server" Text='<%# Eval("PM_RP_TEN_MAINT_FEES", "{0:c2}")%>'></asp:Label>
                                                        <%--<asp:Label ID="lblMaintCurr" runat="server" Text='<%# String.Format(New System.Globalization.RegionInfo(Session("userculture").ToString()).ISOCurrencySymbol + " {0:0.00}", Eval("TEN_MAINT_FEE"))%>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Additional Car Parking Fee">
                                                    <ItemTemplate>
                                                        <%--<asp:Label ID="lblCPFee" runat="server" Visible="false" Text='<%# Eval("PM_RP_ADDN_CAR_PKFEE")%>'></asp:Label>--%>
                                                        <asp:Label ID="lblCPFeeCurr" runat="server" Text='<%# Eval("PM_RP_ADDN_CAR_PKFEE", "{0:c2}")%>'></asp:Label>
                                                        <%--<asp:Label ID="lblCPFeeCurr" runat="server" Text='<%# String.Format(New System.Globalization.RegionInfo(Session("userculture").ToString()).ISOCurrencySymbol + " {0:0.00}", Eval("TEN_ADDN_CAR_PKFEE"))%>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Outstanding Amount">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOSAmount" runat="server" Visible="false" Text='<%# Eval("PM_RP_OUST_AMOUNT")%>'></asp:Label>
                                                        <asp:Label ID="lblOSAmountCurr" runat="server" Text='<%# Eval("PM_RP_OUST_AMOUNT", "{0:c2}")%>'></asp:Label>
                                                        <%--<asp:Label ID="lblOSAmountCurr" runat="server" Text='<%# String.Format(New System.Globalization.RegionInfo(Session("userculture").ToString()).ISOCurrencySymbol + " {0:0.00}", Eval("TEN_OUTSTANDING_AMOUNT"))%>'></asp:Label>--%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Rent To be paid On">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRentPayDate" runat="server" Text='<%#Eval("PM_RP_TEN_PAYABLE_DT")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="SNO" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSNO" runat="server" Text='<%#Eval("PM_RP_ID")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Outstanding + GST">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTDSValue" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>

                                <br />
                                <br />
                                <div class="row">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Select GST Type<span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTDStype" Display="None"
                                                ErrorMessage="Please Select TDS type" ValidationGroup="Val3" InitialValue="0"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlTDStype" runat="server" CssClass="selectpicker" data-live-search="true">
                                                <asp:ListItem Value="0">--select TDS type--</asp:ListItem>
                                                <asp:ListItem Value="1">Value</asp:ListItem>
                                                <asp:ListItem Value="2">%</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Enter GST Value<span style="color: red;">*</span></label>
                                            <div class="col-md-12">
                                                <asp:RequiredFieldValidator ID="rfvTds" runat="server" ControlToValidate="txtTDS"
                                                    Display="None" ErrorMessage="Please enter TDS" ValidationGroup="Val3">
                                                </asp:RequiredFieldValidator>
                                                <asp:CompareValidator ID="cvTds" runat="server" ControlToValidate="txtTDS" ErrorMessage="Please enter valid TDS"
                                                    Type="double" Operator="DataTypeCheck" Display="None" ValidationGroup="Val3"></asp:CompareValidator>
                                                <asp:TextBox ID="txtTDS" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>

                                        </div>


                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="form-group">
                                            <label>Maintenance and Amenity Charges<span style="color: red;"></span></label>
                                            <%--<cc1: ID="fteamenity" runat="server" TargetControlID="txtamenitycharges" FilterType="Numbers" ValidChars="0123456789"/>--%>
                                            <cc1:FilteredTextBoxExtender ID="ftbetxtamenitycharges" runat="server" TargetControlID="txtamenitycharges" FilterType="Numbers" ValidChars="0123456789." />
                                            <asp:TextBox ID="txtamenitycharges" placeholder="Please Enter Amount..." runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>

                                    </div>
                                    <div class="form-group col-sm-2 col-xs-6">
                                        <br />
                                        <br />
                                        <asp:Button ID="btnCalc" runat="server" Text="Calculate" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val3" OnClick="btnCalc_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>

                                    <div class="row" runat="server" id="divPayterms">
                                        <div class="form-group col-sm-4 col-xs-6">
                                            <label>Select Payment Mode <span style="color: red;">*</span></label>
                                            <asp:RequiredFieldValidator ID="cvmode" runat="server" ControlToValidate="ddlpaymentmode"
                                                Display="None" ErrorMessage="Please Select Payment Mode" ValidationGroup="Val2"
                                                InitialValue="--Select--"></asp:RequiredFieldValidator>
                                            <asp:DropDownList ID="ddlpaymentmode" runat="server"
                                                OnSelectedIndexChanged="ddlpaymentmode_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Value="--Select--">--select Payment Mode--</asp:ListItem>
                                                <asp:ListItem Value="1">Cheque</asp:ListItem>
                                                <asp:ListItem Value="2">Cash</asp:ListItem>
                                                <asp:ListItem Value="3">Bank Transfer</asp:ListItem>
                                            </asp:DropDownList>&nbsp;                                                                                        
                                        </div>

                                    </div>
                                    <br />
                                    <div id="panel1" class="pnl1" runat="Server">
                                        <div class="row">
                                            <div class="form-group col-sm-4 col-xs-6">
                                                <label>Cheque No <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvCheque" runat="server" ControlToValidate="txtCheque"
                                                    Display="None" ErrorMessage="Please Enter Cheque Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="rvcheque" Display="None" runat="server" ControlToValidate="txtCheque"
                                                    ErrorMessage="Invalid Cheque Number" ValidationExpression="[0-9]{3,8}" ValidationGroup="Val2"></asp:RegularExpressionValidator>

                                                <div onmouseover="Tip('Enter Numbers and No Special Characters allowed ')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtCheque" runat="Server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-4 col-xs-6">
                                                <label>Bank Name <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                    Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                                                    ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>

                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-4 col-xs-6">
                                                <label>Account Number <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                    Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                                                    ErrorMessage="Enter Valid Account Number" ValidationExpression="^[0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter  Numbers only and No Special Characters allowed ')"
                                                    onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtAccNo" runat="Server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="panel2" runat="Server">
                                        <div class="row">
                                            <div class="form-group col-sm-4 col-xs-6">
                                                <label>Issuing Bank Name <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtIBankName"
                                                    Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                    ControlToValidate="txtIBankName" ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                    ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtIBankName" runat="Server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-4 col-xs-6">
                                                <label>Deposited Bank <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtDeposited"
                                                    Display="None" ErrorMessage="Please Enter Deposited Bank" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtDeposited"
                                                    ErrorMessage="Enter Valid Bank Branch" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                    ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtDeposited" runat="Server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group col-sm-4 col-xs-6">
                                                <label>IFSC Code <span style="color: red;">*</span></label>
                                                <asp:RequiredFieldValidator ID="rfvIFCB" runat="server" ControlToValidate="txtIFCB"
                                                    Display="None" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="REVIFCB" Display="None" runat="server" ControlToValidate="txtIFCB"
                                                    ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')" onmouseout="UnTip()">
                                                    <asp:TextBox ID="txtIFCB" runat="Server" CssClass="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </ContentTemplate>
                            </asp:UpdatePanel>


                            <div class="row" runat="server" id="divRems">
                                <div class="form-group col-sm-3 col-xs-6">
                                    <label>Tax Registration No. (TRN)<span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvTRN" runat="server" ControlToValidate="txtTRN"
                                        Display="None" ErrorMessage="Please Enter TRN" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                    <asp:TextBox ID="txtTRN" runat="Server" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div class="form-group col-sm-3 col-xs-6">
                                    <label>Payment Remarks <span style="color: red;">*</span></label>
                                    <asp:RequiredFieldValidator ID="rfvremarks" runat="server" ControlToValidate="txtRemarks"
                                        Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revremarks" Display="None" runat="server" ControlToValidate="txtRemarks"
                                        ErrorMessage="Enter Valid Remarks" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                    <div onmouseover="Tip('Enter Remarks with Maximum 250 characters and No special characters allowed ')" onmouseout="UnTip()">
                                        <asp:TextBox ID="txtRemarks" runat="Server" CssClass="form-control" MaxLength="250"
                                            Rows="3" TextMode="MultiLine"></asp:TextBox>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-group">
                                        <asp:Button ID="btnsubmit" runat="server" Text="Pay" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val2" OnClick="btnsubmit_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                            <h5><b>
                                <asp:Label ID="lblhistHeading" runat="server" Text="Rent History Details" Visible="false"></asp:Label></b></h5>

                            <div class="row form-inline">
                                <div class="form-group col-md-12">
                                    <asp:GridView ID="gvpaid" runat="server" AllowPaging="True" AllowSorting="False" RowStyle-HorizontalAlign="center"
                                        HeaderStyle-HorizontalAlign="Center" Width="100%" PageSize="20" AutoGenerateColumns="false" EmptyDataText="No Records Found."
                                        CssClass="table GridStyle" GridLines="none">
                                        <PagerSettings Mode="NumericFirstLast" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Property">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblProperty" runat="server" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant Code">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTenCode" runat="server" Text='<%#Bind("PM_RP_TEN_CODE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Tenant">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTenant" runat="server" Text='<%#Eval("TENANT")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Rent Amount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRent" runat="server" Text='<%#Eval("PM_RP_TEN_RENT", "{0:c2}")%>'></asp:Label>
                                                    <%--<asp:Label ID="lblRent" runat="server" Text='<%# String.Format(New System.Globalization.RegionInfo(Session("userculture").ToString()).ISOCurrencySymbol + " {0:0.00}", Eval("TEN_RENT_PER_SFT"))%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Maintenance Cost">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblMaint" runat="server" Text='<%# Eval("PM_RP_TEN_MAINT_FEES", "{0:c2}")%>'></asp:Label>
                                                    <%--<asp:Label ID="lblMaint" runat="server" Text='<%# String.Format(New System.Globalization.RegionInfo(Session("userculture").ToString()).ISOCurrencySymbol + " {0:0.00}", Eval("TEN_MAINT_FEE"))%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Additional Car Parking Fee">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCPFee" runat="server" Text='<%# Eval("PM_RP_ADDN_CAR_PKFEE", "{0:c2}")%>'></asp:Label>
                                                    <%--<asp:Label ID="lblCPFee" runat="server" Text='<%# String.Format(New System.Globalization.RegionInfo(Session("userculture").ToString()).ISOCurrencySymbol + " {0:0.00}", Eval("TEN_ADDN_CAR_PKFEE"))%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Outstanding Amount + TDS">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOSAmount" runat="server" Text='<%# Eval("PM_RP_OUST_TDS_AMOUNT", "{0:c2}")%>'></asp:Label>
                                                    <%--<asp:Label ID="lblOSAmount" runat="server" Text='<%# String.Format(New System.Globalization.RegionInfo(Session("userculture").ToString()).ISOCurrencySymbol + " {0:0.00}", Eval("TEN_OUTSTANDING_AMOUNT"))%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Rent Paid Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstat" runat="server" Text='<%#Eval("CREATED_DATE")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                        <PagerStyle CssClass="pagination-ys" />
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>

<script type="text/javascript">
    $(document).ready(function () {
        // No of Payments
        jQuery('#txtTerms').keyup(function () {
            this.value = this.value.replace(/[^0-9]/g, '');
        });

        // No of Payments
        jQuery('#txtTDS').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');
        });
    });
</script>
<script>
    function refreshSelectpicker() {
        $("#<%=ddlBuilding.ClientID%>").selectpicker();
        $("#<%=ddlCity.ClientID%>").selectpicker();
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%=ddlTDStype.ClientID%>").selectpicker();
        $("#<%=ddlpaymentmode.ClientID%>").selectpicker();

        $("#<%=ddlproptype.ClientID%>").selectpicker();


    }
    refreshSelectpicker();


</script>




