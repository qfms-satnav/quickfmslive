﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubLeaseAgreement.aspx.cs" Inherits="WorkSpace_SMS_Webfiles_SubLeaseAgreement" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%-- <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />--%>
    <%--   <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
    <style>
        .panel-heading {
            border-radius:10px;
        }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Sub Lease Agreement" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Sub Lease Agreement                                
                            </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>Sub Lease Agreement</h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel runat="server" ID="upfelren">
                        <ContentTemplate>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />

                            <div class="row" style="padding-top: 10px">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-10 control-label">Search By Lease ID Property Code / Name / City/ Location<span style="color: red;">*</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <asp:RequiredFieldValidator ID="rfPropertyType" runat="server" ControlToValidate="txtReqId"
                                                    Display="none" ErrorMessage="Please Search By Property / Lease / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                <asp:TextBox ID="txtReqId" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Button ID="btnsearch" CssClass="btn btn-default btn-primary" runat="server" Text="Search"
                                                    TabIndex="2" OnClick="btnsearch_Click" ValidationGroup="Val1" />
                                                <asp:Button ID="txtreset" CssClass="btn btn-default btn-primary" runat="server" Text="Reset"
                                                    CausesValidation="false" TabIndex="2" OnClick="txtreset_Click" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div style="margin-top: 10px">
                                <div class="row form-inline">
                                    <div class="form-group col-md-12">
                                        <asp:GridView ID="gvItems" runat="server" AllowPaging="True" AutoGenerateColumns="false"
                                            EmptyDataText="No Records Found." CssClass="table GridStyle" GridLines="none" PageSize="5"
                                            OnPageIndexChanging="gvItems_PageIndexChanging" OnRowCommand="gvItems_RowCommand">
                                            <PagerSettings Mode="NumericFirstLast" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Lease Name" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLseName" runat="server" Text='<%#Eval("LEASE_ID") %>'></asp:Label>
                                                        <asp:Label ID="lblLseSno" runat="server" Text='<%#Eval("PM_LES_SNO") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease ID">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkLeaseId" runat="server" CssClass="lnkLeaseId" Text='<%#Eval("LEASE_ID")%>' CommandName="SubLeaseAgreement"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPropCd" Text='<%#Eval("PM_PPT_CODE")%>' runat="server" CssClass="lblPropCd"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPropType" Text='<%#Eval("PM_PPT_NAME")%>' runat="server" CssClass="lblPropType"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Entity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEntityType" Text='<%#Eval("CHE_NAME")%>' runat="server" CssClass="lblEntityType"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="City">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCity" Text='<%#Eval("CTY_NAME")%>' runat="server" CssClass="lblCity"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLocation" Text='<%#Eval("LCM_NAME")%>' runat="server" CssClass="lblLocation"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Start Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeaseStartDate" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>' runat="server" CssClass="lblLeaseStartDate"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease End Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLeaseEndDate" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT") %>' runat="server" CssClass="lblLeaseEndDate"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Total Rent">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRent" Text='<%#Eval("PM_LES_TOT_RENT","{0:c2}")%>' runat="server" CssClass="lblRent"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Lease Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLseSta" Text='<%#Eval("LEASE_STATUS")%>' runat="server" CssClass="lblLseSta"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="true" ShowValidationErrors="true" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                                <div id="panel1" runat="Server">
                                    <div ba-panel ba-panel-title="Add Sub Lease" ba-panel-class="with-scroll">
                                        <div class="panel">
                                            <div class="panel-heading" style="height: 41px;">
                                                <h3 class="panel-title">View Lease Details
                                                    <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                                    <a href="#" class="btn btn-default openall  pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-plus " aria-hidden="true"></i></a>
                                                </h3>
                                            </div>
                                            <div class="panel-body" style="padding-right: 10px;">
                                                <div id="AddSubLeaseDetails" runat="server">
                                                    <div class="panel panel-default " role="tab" runat="server" id="div0">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false">Lease Details</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="collapse show">
                                                            <div class="panel-body color">
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lease Id</label>
                                                                            <asp:TextBox ID="ldlsno" runat="server" Visible="false"></asp:TextBox>
                                                                            <asp:TextBox ID="txtleaseid" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Property Code</label>
                                                                            <asp:TextBox ID="txtPropCd" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Property Name</label>
                                                                            <asp:TextBox ID="txtPropName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Country</label>
                                                                            <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>City</label>
                                                                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Location</label>
                                                                            <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lease Start Date</label>
                                                                            <asp:TextBox ID="txtLseStrDt" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Lease End Date</label>
                                                                            <asp:TextBox ID="txtLseEndDt" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default ">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Area & Cost Details</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseTwo" class="collapse show">
                                                            <div class="panel-body color">
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>CTS Number</label>
                                                                            <asp:TextBox ID="txtLnumber" runat="server" CssClass="form-control" MaxLength="50"
                                                                                TabIndex="2" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Entitled Lease Amount</label>
                                                                            <asp:TextBox ID="txtentitle" runat="server" CssClass="form-control" TabIndex="12" Enabled="false">0</asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Basic Rent</label>
                                                                            <asp:TextBox ID="txtBasicRent" runat="server" CssClass="form-control" MaxLength="15" TabIndex="13" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Security Deposit</label>
                                                                            <asp:TextBox ID="txtSD" runat="server" CssClass="form-control" TabIndex="15" MaxLength="26" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Security Deposited Months</label>
                                                                            <asp:TextBox ID="txtSDMonths" runat="server" CssClass="form-control" TabIndex="15"
                                                                                MaxLength="26" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Rent Free Period</label>
                                                                            <asp:TextBox ID="txtRentFreePeriod" runat="server" CssClass="form-control" MaxLength="50"
                                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Carpet Area (Sqft)</label>
                                                                            <asp:TextBox ID="txtCarArea" runat="server" CssClass="form-control" MaxLength="50"
                                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Rent Per Sqft (On Carpet)</label>
                                                                            <asp:TextBox ID="txtRentPerSqftCarpet" runat="server" CssClass="form-control" MaxLength="50"
                                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Built Up Area (Sqft)</label>
                                                                            <asp:TextBox ID="txtBUA" runat="server" CssClass="form-control" MaxLength="50"
                                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Rent Per Sqft (On BUA)</label>
                                                                            <asp:TextBox ID="txtRentPerSqftBUA" runat="server" CssClass="form-control" MaxLength="50"
                                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Interior Cost (Approx)</label>
                                                                            <asp:TextBox ID="txtInteriorCost" runat="server" CssClass="form-control" MaxLength="50"
                                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Agreement Details</a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseThree" class="collapse show">
                                                            <div class="panel-body color">
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Sub Entity<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvven" runat="server" ControlToValidate="ddlSubGrp"
                                                                                Display="none" ErrorMessage="Please Select Sub Group" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                            <asp:DropDownList ID="ddlSubGrp" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                                                ToolTip="--Select--">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Agreement Type<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlagrementType"
                                                                                Display="none" ErrorMessage="Please Select Agreement Type" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                            <asp:DropDownList ID="ddlagrementType" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                                                ToolTip="--Select--">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Agreement Start Date<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtagredate"
                                                                                Display="none" ErrorMessage="Please Select Agreement Start Date" ValidationGroup="Val2">
                                                                            </asp:RequiredFieldValidator>
                                                                            <div class='input-group date' id='Agrefromdate'>
                                                                                <asp:TextBox ID="txtagredate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar" onclick="setup('Agrefromdate')"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Start Date<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvstart" runat="server" ControlToValidate="AgreeStartDate"
                                                                                Display="none" ErrorMessage="Please Select Agreement Start Date" ValidationGroup="Val2">
                                                                            </asp:RequiredFieldValidator>
                                                                            <div class='input-group date' id='fromdate'>
                                                                                <asp:TextBox ID="AgreeStartDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>End Date<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvend" runat="server" ControlToValidate="AgreeEndDate"
                                                                                Display="none" ErrorMessage="Please Select Agreement End Date" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                            <div class='input-group date' id='todate'>
                                                                                <asp:TextBox ID="AgreeEndDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Address</label>
                                                                            <div onmouseover="Tip('Enter Asset Description')" onmouseout="UnTip()">
                                                                                <asp:TextBox ID="txtaddress" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Remaining Area (Sqft) / No. of Seating</label>
                                                                            <asp:TextBox ID="txtRemain" TextMode="Number" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Area (Sqft) / No. of Seating<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvagree" runat="server" ControlToValidate="txtSeat"
                                                                                Display="none" ErrorMessage="Please Enter Area(Sqft) / No.Of Seating" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                            <asp:TextBox ID="txtSeat" TextMode="Number" runat="server" CssClass="form-control" Enabled="true"
                                                                                OnTextChanged="txtSeat_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Security Deposit<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCost"
                                                                                Display="none" ErrorMessage="Please Enter Rent" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                            <asp:TextBox ID="txtsecdep" TextMode="Number" runat="server" CssClass="form-control">0</asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Rent<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCost"
                                                                                Display="none" ErrorMessage="Please Enter Rent" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                            <asp:TextBox ID="txtCost" TextMode="Number" runat="server" CssClass="form-control" AutoPostBack="true" OnTextChanged="txtCost_TextChanged">0</asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Maintenance Charges</label>
                                                                            <asp:TextBox ID="txtMaintChrg" TextMode="Number" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" OnTextChanged="txtMaintChrg_TextChanged">0</asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Total Cost</label>
                                                                            <asp:TextBox ID="txtTotal" TextMode="Number" runat="server" CssClass="form-control" Enabled="false">0</asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="Row">
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Payment Terms<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvpayterm" runat="server" ControlToValidate="ddlPaymentTerm"
                                                                                Display="none" ErrorMessage="Please Select Payment Terms" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                            <asp:DropDownList ID="ddlPaymentTerm" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                                                ToolTip="--Select--">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Payment Date<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="PaymentDate"
                                                                                Display="none" ErrorMessage="Please Select Payment Date" ValidationGroup="Val2">
                                                                            </asp:RequiredFieldValidator>
                                                                            <div class='input-group date' id='PayDate'>
                                                                                <asp:TextBox ID="PaymentDate" runat="server" CssClass="form-control" MaxLength="10"> </asp:TextBox>
                                                                                <span class="input-group-addon">
                                                                                    <span class="fa fa-calendar" onclick="setup('PayDate')"></span>
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                        <div class="form-group">
                                                                            <label>Payment Mode<span style="color: red;">*</span></label>
                                                                            <asp:RequiredFieldValidator ID="rfvpaytype" runat="server" ControlToValidate="ddlPaymentType"
                                                                                Display="none" ErrorMessage="Please Select Payment Type" ValidationGroup="Val2" InitialValue="--Select--"></asp:RequiredFieldValidator>
                                                                            <asp:DropDownList ID="ddlPaymentType" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                                                ToolTip="--Select--" OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged" AutoPostBack="true">
                                                                            </asp:DropDownList>
                                                                        </div>
                                                                    </div>

                                                                    <div id="panel2" runat="Server">
                                                                        <div class="row">
                                                                            <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Account / Serial Number <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvAccNo" runat="server" ControlToValidate="txtAccNo"
                                                                                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="revAccno" Display="None" runat="server" ControlToValidate="txtAccNo"
                                                                                        ErrorMessage="Enter Valid Account / Serial Number" ValidationExpression="^[a-zA-Z0-9, ]*$"
                                                                                        ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')"
                                                                                        onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtAccNo" runat="server" CssClass="form-control">0</asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>--%>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Bank Name <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvBankName" runat="server" ControlToValidate="txtBankName"
                                                                                        Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="revBankName" Display="None" runat="server" ControlToValidate="txtBankName"
                                                                                        ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9, ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                                        onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Branch Name <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtbrnch"
                                                                                        Display="None" ErrorMessage="Please Enter Branch Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="None" runat="server" ControlToValidate="txtbrnch"
                                                                                        ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                                        onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="textbrch" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Cheque/DD Number<span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="rfvChequeNo" runat="server" ControlToValidate="txtChequeNo"
                                                                                        Display="None" ValidationGroup="Val2" ErrorMessage="Please Enter Cheque/DD Number"></asp:RequiredFieldValidator>
                                                                                    <cc1:FilteredTextBoxExtender ID="ftetxtChequeNo" runat="server" TargetControlID="txtChequeNo" FilterType="Numbers" ValidChars="0123456789" />
                                                                                    <div onmouseover="Tip('Enter alphabets and  Numbers, No Special Characters allowed ')" onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtChequeNo" runat="server" TabIndex="50" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>


                                                                        <div id="panel3" runat="Server" visible="false">
                                                                            <div class="row">
                                                                                <%-- <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                <div class="form-group">
                                                                                    <label>Account / Serial Number <span style="color: red;">*</span></label>
                                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAccTwo"
                                                                                        Display="None" ErrorMessage="Please Enter Account Number" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" Display="None" runat="server"
                                                                                        ControlToValidate="txtAccTwo" ErrorMessage="Enter Valid Account / Serial Number" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                                                        ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                                                    <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                                        onmouseout="UnTip()">
                                                                                        <asp:TextBox ID="txtAccTwo" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>--%>
                                                                                <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Bank Name <span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfDeposited" runat="server" ControlToValidate="txtBankTwo"
                                                                                            Display="None" ErrorMessage="Please Enter Bank Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                                        <asp:RegularExpressionValidator ID="revDeposited" Display="None" runat="server" ControlToValidate="txtBankTwo"
                                                                                            ErrorMessage="Enter Valid Bank Name" ValidationExpression="^[A-Za-z0-9 ]*$"
                                                                                            ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                                                        <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                                            onmouseout="UnTip()">
                                                                                            <asp:TextBox ID="txtBankTwo" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>Branch Name <span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvl3brnch" runat="server" ControlToValidate="txtbrnch"
                                                                                                Display="None" ErrorMessage="Please Enter Branch Name" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="revl3brnch" Display="None" runat="server" ControlToValidate="txtbrnch"
                                                                                                ErrorMessage="Enter Valid Branch Name" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                                                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                                                onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtbrnch" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                                                        <div class="form-group">
                                                                                            <label>UTR NO <span style="color: red;">*</span></label>
                                                                                            <asp:RequiredFieldValidator ID="rfvIFsc" runat="server" ControlToValidate="txtIFSC"
                                                                                                Display="None" ErrorMessage="Please Enter IFSC Code" ValidationGroup="Val2"></asp:RequiredFieldValidator>
                                                                                            <asp:RegularExpressionValidator ID="REVIFsc" Display="None" runat="server" ControlToValidate="txtIFSC"
                                                                                                ErrorMessage="Enter Valid IFSC" ValidationExpression="^[A-Za-z0-9 ]*$" ValidationGroup="Val2"></asp:RegularExpressionValidator>
                                                                                            <div onmouseover="Tip('Enter alphabets and Numbers only and No Special Characters allowed ')"
                                                                                                onmouseout="UnTip()">
                                                                                                <asp:TextBox ID="txtIFSC" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Cost Type On<span style="color: red;">*</span></label>
                                                                                <asp:RequiredFieldValidator ID="rfvrblCostType" runat="server" ControlToValidate="rblCostType"  ValidationGroup="Val2"
                                                                                    ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>
                                                                                <asp:RadioButtonList ID="rblCostType" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="rblCostType_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="Sqft" Text="Sq.ft" />
                                                                                    <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                                                </asp:RadioButtonList>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div id="Costype1" runat="server" >
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Rent Per Sq.ft (On Carpet)<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtSubCarpet" runat="server" ControlToValidate="txtSubCarpet"  ValidationGroup="Val2"
                                                                                            ErrorMessage="Please Enter Rent Per Sq.ft (On Carpet)"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftbetxtSubCarpet" runat="server" TargetControlID="txtSubCarpet" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <asp:TextBox ID="txtSubCarpet" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8">.</asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Rent Per Sq.ft (On BUA)<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtSubBUA" runat="server" ControlToValidate="txtSubBUA"  ValidationGroup="Val2"
                                                                                            ErrorMessage="Please Enter Rent Per Sq.ft (On BUA)"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtSubBUA" runat="server" TargetControlID="txtSubBUA" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <asp:TextBox ID="txtSubBUA" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9">.</asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div id="Costype2" runat="server" visible="false">
                                                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="form-group">
                                                                                        <label>Seat Cost<span style="color: red;">*</span></label>
                                                                                        <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val2"
                                                                                            ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                                                        <cc1:FilteredTextBoxExtender ID="ftetxtSeatCost" runat="server" TargetControlID="txtSeatCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                                        <asp:TextBox ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8"></asp:TextBox>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="row">
                                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                                            <div class="form-group">
                                                                                <label>Remarks</label>
                                                                                <div onmouseover="Tip('Enter Asset Description')" onmouseout="UnTip()">
                                                                                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" width="200" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                                                        <div class="form-group">
                                                                            <asp:Button ID="btnSubmit" CssClass="btn btn-default btn-primary" runat="server" Text="Submit" CausesValidation="true" ValidationGroup="Val2" OnClick="btnSubmit_Click" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
        <%--  </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        function refreshSelectpicker() {
            $("#<%=ddlPaymentType.ClientID%>").selectpicker();
            $("#<%=ddlPaymentTerm.ClientID%>").selectpicker();
            $("#<%=ddlSubGrp.ClientID%>").selectpicker();
            $("#<%=ddlagrementType.ClientID%>").selectpicker();
        }
        refreshSelectpicker();
    </script>
</body>
</html>
