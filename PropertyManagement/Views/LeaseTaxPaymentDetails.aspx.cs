﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SubSonic;
using System.IO;
using System.Net;
using SubSonic;

public partial class PropertyManagement_Views_LeaseTaxPaymentDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            Session["id"] = Request.QueryString["id"];
        }
        if (!IsPostBack)
        {

            LeaseDetails();
        }
    }

    protected void LeaseDetails()
    {

        SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure(Session["TENANT"] + "." + "PM_GET_SUB_LEASE_DETAILS");
        sp.Command.AddParameter("@AURID", HttpContext.Current.Session["UID"].ToString(), DbType.String);
        sp.Command.AddParameter("@COMPANYID", HttpContext.Current.Session["COMPANYID"].ToString(), DbType.Int32);
        sp.Command.AddParameter("@LEASEID", Request.QueryString["id"], DbType.String);
        IDataReader dr;
        dr = sp.GetReader();
        if (dr.Read())
        {
            txtPropCd.Text = Convert.ToString(dr["PM_PPT_CODE"]);
            txtPropName.Text = Convert.ToString(dr["PM_PPT_NAME"]);
            txtCountry.Text = Convert.ToString(dr["CNY_NAME"]);
            txtCity.Text = Convert.ToString(dr["CTY_NAME"]);
            txtLocation.Text = Convert.ToString(dr["LCM_NAME"]);
            txtLseStrDt.Text = Convert.ToString(dr["LEASE_ST_DT"]);
            txtLseEndDt.Text = Convert.ToString(dr["LEASE_END_DT"]);
            txtLnumber.Text = Convert.ToInt32(dr["PM_LES_CTS_NO"]).ToString();
            txtentitle.Text = Convert.ToDecimal(dr["PM_LES_ENTITLED_AMT"]).ToString();
            txtBasicRent.Text = Convert.ToDecimal(dr["PM_LES_BASIC_RENT"]).ToString();
            txtSD.Text = Convert.ToDecimal(dr["PM_LES_SEC_DEPOSIT"]).ToString();
            txtSDMonths.Text = Convert.ToInt32(dr["PM_LES_SEC_DEP_MONTHS"]).ToString();
            txtRentFreePeriod.Text = Convert.ToString(dr["PM_LES_RENT_FREE_PERIOD"]);
            txtRentPerSqftCarpet.Text = Convert.ToDecimal(dr["PM_LES_RENT_PER_SQFT_CARPET"]).ToString();
            txtRentPerSqftBUA.Text = Convert.ToDecimal(dr["PM_LES_RENT_PER_SQFT_BUA"]).ToString();
            txtInteriorCost.Text = Convert.ToDecimal(dr["PM_LES_INTERIOR_COST"]).ToString();
            txtCarArea.Text = Convert.ToDecimal(dr["PM_AR_CARPET_AREA"]).ToString();
            txtBUA.Text = Convert.ToDecimal(dr["PM_AR_BUA_AREA"]).ToString();
            ddlSubGrp.Text = Convert.ToString(dr["CNP_NAME"]);
            AgreeStartDate.Text = Convert.ToString(dr["PM_SLA_AGREE_ST_DT"]);
            AgreeEndDate.Text = Convert.ToString(dr["PM_SLA_AGREE_END_DT"]);
            ddlPaymentTerm.Text = Convert.ToString(dr["PM_SLA_PAY_TERMS"]);
            PaymentDate.Text = Convert.ToString(dr["PM_SLA_PAY_DATE"]);
            ddlPaymentType.Text = Convert.ToString(dr["PM_SLA_PAY_TYPE"]);
            txtSeat.Text = Convert.ToDecimal(dr["PM_SLA_AREA_OR_SEATING"]).ToString();
            txtCost.Text = Convert.ToDecimal(dr["PM_SLA_COST"]).ToString();
            txtMaintChrg.Text = Convert.ToDecimal(dr["PM_SLA_MAINT_CHARGES"]).ToString();
            txtTotal.Text = Convert.ToDecimal(dr["PM_SLA_COST"]).ToString();
            txtAccTwo.Text = Convert.ToString(dr["PM_SLA_ACC_SERIAL_NO"]);
            txtBankTwo.Text = Convert.ToString(dr["PM_SLA_BANK"]);
            txtbrnch.Text = Convert.ToString(dr["PM_SLA_BRANCH"]);
            txtIFSC.Text = Convert.ToString(dr["PM_SLA_IFSC"]);
            
        }

    }

    protected void btnback_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/PropertyManagement/Views/LeaseTaxPayment.aspx");
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {

            SqlParameter[] param = new SqlParameter[5];
            param[0] = new SqlParameter("@LEASEID", Request.QueryString["id"]);
            param[1] = new SqlParameter("@TDS", ddlTDS.SelectedItem.Value);
            param[2] = new SqlParameter("@TDS_REMARKS", txtRemarks.Text);
            param[3] = new SqlParameter("@AUR_ID", Session["UID"].ToString());
            param[4] = new SqlParameter("@COMPANYID", Session["COMPANYID"]);
            SqlHelper.ExecuteNonQuery(CommandType.StoredProcedure, "PM_LEASE_TAX_PAYMENT", param);

            lblMsg.Visible = true;
            lblMsg.Text = "Sub Lease Agreement Approved Successfully";
        }

        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }

      

    }
}