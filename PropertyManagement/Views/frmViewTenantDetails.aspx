<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmViewTenantDetails.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmViewTenantDetails" Title="View Tenant Details" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="ExportPanel" Namespace="ControlFreak" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href="../../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />--%>
    <%-- <link href="../../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script lang="javascript" type="text/javascript">
        function maxLength(s, args) {
            if (args.Value.length >= 500)
                args.IsValid = false;
        }

        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
                <div ba-panel ba-panel-title="Add Tenant" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">View Tenant Details</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
            <div class="widgets">
                <h3>View Tenant Details </h3>
            </div>
            <div class="card">
                <form id="form1" runat="server">
                    <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                    <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" ForeColor="Red" />
                    <asp:ValidationSummary ID="ValidationSummary2" ValidationGroup="Val2" runat="server" ForeColor="Red" />
                    <asp:UpdatePanel runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMsg" runat="server" CssClass="clsMessage"></asp:Label>
                            <br />
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Property Type <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                                            Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                                            InitialValue="0"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlproptype" runat="server" CssClass="form-control selectpicker" data-live-search="true"
                                                AutoPostBack="True" OnSelectedIndexChanged="ddlproptype_SelectedIndexChanged">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">City<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="cvcity" runat="server" ControlToValidate="ddlCity"
                                            Display="None" ErrorMessage="Please Select City" ValidationGroup="Val1" InitialValue="0"
                                            Enabled="true"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="TRUE">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Location <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12" style="padding-top: 17px">
                                    <div class="form-group ">
                                        <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />
                                    </div>
                                </div>
                            </div>
                            <br />
                            <div id="t1" runat="Server">
                                <div class="clearfix">
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="90%" placeholder="Search By Tenant Code/Property Name"></asp:TextBox>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 22px">
                                        <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Val2" CssClass="btn btn-primary custom-button-color pull-left" CausesValidation="true" />
                                    </div>
                                    <asp:LinkButton ID="LBTN1" runat="Server" Text="GetAllRecords"></asp:LinkButton>
                                </div>
                                <div class="row" style="margin-top: 10px;">
                                    <div class="col-md-12">
                                        <cc1:ExportPanel ID="exportpanel1" runat="server">
                                            <asp:GridView ID="gvPropType" runat="server" EmptyDataText="No Tenant Details Found."
                                                CssClass="table GridStyle" GridLines="none"
                                                AllowPaging="True" AllowSorting="false" PageSize="8" AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sno" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblid" runat="server" Text='<%# Eval("SNO") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tenant Code">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCode" runat="server" Text='<%# Eval("TEN_CODE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tenant Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("TEN_NAME") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tenant Email">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("TEN_EMAIL") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Property Name">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPrty" runat="server" Text='<%# Eval("PM_TEN_PROPRTY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Occupied Area">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblarea" runat="server" Text='<%# Eval("TEN_OCCUPIEDAREA") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Maintenance Fee">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblfee" runat="server" Text='<%# Eval("TEN_MAINT_FEE", "{0:c2}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tenant Rent">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRent" runat="server" Text='<%# Eval("TEN_RENT_PER_SFT", "{0:c2}")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tenant Start Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStart" runat="server" Text='<%# Eval("PM_TEN_FRM_DT")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Tenant End Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEnd" runat="server" Text='<%# Eval("PM_TEN_TO_DT")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Remarks">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRemarks" runat="server" Text='<%# Eval("TEN_REMARKS") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Edit">
                                                        <ItemTemplate>
                                                            <a href='../../WorkSpace/SMS_Webfiles/frmEditTenantDetails.aspx?ID=<%# Eval("SNO") %>'>Edit</a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status">
                                                        <ItemTemplate>
                                                            <asp:LinkButton runat="server" ID="lnkStauts" Text='<% #Bind("PM_TEN_STATUS")%>' CommandArgument='<% #Bind("SNO")%>' OnClick="lnkStauts_Click"></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle HorizontalAlign="Center" />
                                                <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                <PagerStyle CssClass="pagination-ys" />
                                            </asp:GridView>
                                        </cc1:ExportPanel>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-group">
                                            <asp:Button ID="btnexporttoexcel" runat="Server" Text="Export to Excel" Visible="false" CssClass="btn btn-primary custom-button-color" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </form>
            </div>
        </div>
        <%-- </div>
            </div>
        </div>--%>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
<script>
    function refreshSelectpicker() {

        $("#<%=ddlCity.ClientID%>").selectpicker();
        $("#<%=ddlLocation.ClientID%>").selectpicker();

        $("#<%=ddlproptype.ClientID%>").selectpicker();

    }
    refreshSelectpicker();


</script>
