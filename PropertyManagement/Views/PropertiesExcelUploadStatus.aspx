﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PropertiesExcelUploadStatus.aspx.cs" Inherits="PropertyManagement_Views_PropertiesExcelUploadStatus" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <style type="text/css">
        /*body
        {
            color: #6a6c6f;
            background-color: #f1f3f6;
            margin-top: 30px;
        }*/

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>

</head>
<body>
    <div class="container-fluid page-content-inner">
        <div class="bgc-gray p-20 m-b-25">
            <div class="panel-heading-qfms">
                <h3 class="panel-title panel-heading-qfms-title">Excel Upload Status</h3>
            </div>
        </div>
        <div class="card">
            <form id="form1" runat="server">
                <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

                <div runat="server" id="ResultDiv">
                    <asp:Label ID="ExcelUniqueID" runat="server" /><br />
                    <asp:Label ID="TotalSavedRecords" runat="server" /><br />
                    <asp:Label ID="TotalFailureRecors" runat="server" />



                    <div class="row" style="margin-top: 5px">
                        <div class="col-md-12" style="width: 100%; height: 100%; overflow: scroll" runat="server" id="ExcelParserDiv">
                            <asp:GridView ID="ResultGridView" runat="server" AutoGenerateColumns="false" CssClass="table GridStyle">
                                <Columns>
                                    <asp:TemplateField HeaderText="SNO." ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%--<asp:BoundField DataField="Remark" HeaderText="Remark" ItemStyle-Width="150" />--%>

                                    <asp:TemplateField HeaderText="Zone">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LCM_ZONE" Text='<%# Eval("LCM_ZONE") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="PropertyState" Text='<%# Eval("State") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Building">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="TOWER" Text='<%# Eval("TOWER") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Floor">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="FLOOR" Text='<%# Eval("FLOOR") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Branch Name">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LOCATION" Text='<%# Eval("LOCATION") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="City">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="CITY" Text='<%# Eval("CITY") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Internal Category">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="PROP_TYPE" Text='<%# Eval("PROP_TYPE") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Branch Code">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LOCATION_CODE" Text='<%# Eval("LOCATION_CODE") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Supplier code">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="Supplier_code" Text='<%# Eval("Supplier_code") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <%--<asp:TemplateField HeaderText="PC">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="PC" Text='<%# Eval("PC") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CC">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="CC" Text='<%# Eval("CC") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Landlord Name">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LandlordName" Text='<%# Eval("LandlordName") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="C/B/SB Area">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="C_B_SB_Area" Text='<%# Eval("C_B_SB_Area") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText=" Carpet Area">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="Area" Text='<%# Eval("Area") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Chargeable Area">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="Chargeable_Area" Text='<%# Eval("Chargeable_Area") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rent per sq.ft. (on carpet)(Rs.)">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="Rentper_sqft" Text='<%# Eval("Rentper_sqft") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Rent P.M">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="BasicRent" Text='<%# Eval("BasicRent") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Maintenance P.M">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LandlordMaintenance" Text='<%# Eval("LandlordMaintenance") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Amenities P.M">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LandlordAmenities" Text='<%# Eval("LandlordAmenities") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="DG Charges P.M">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="DG_Charges" Text='<%# Eval("DG_Charges") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <%--<asp:TemplateField HeaderText="AMC & operation cost of HIVAC &  DG">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="AMC_HIVAC" Text='<%# Eval("AMC_HIVAC") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Car Parking Charges">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="CarParkingCharges" Text='<%# Eval("CarParkingCharges") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--  <asp:TemplateField HeaderText="Any other charges P.M">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="Another_charges" Text='<%# Eval("Another_charges") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <%--   <asp:TemplateField HeaderText="PT Charges.PM">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="PT_Charges" Text='<%# Eval("PT_Charges") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <%--<asp:TemplateField HeaderText="Signage P.M">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="Signage_PM" Text='<%# Eval("Signage_PM") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Total Rent P.M">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="TotalRent" Text='<%# Eval("TotalRent") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="LL Scope of recovery">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="BankScopeRecovery" Text='<%# Eval("BankScopeRecovery") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <%--<asp:TemplateField HeaderText="LL Scope of recovery Details">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="LLScopeRecoveryDetails" Text='<%# Eval("LLScopeRecoveryDetails") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <%--<asp:TemplateField HeaderText="Remarks">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="Remarks" Text='<%# Eval("Remarks") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <%--<asp:TemplateField HeaderText="RE PM">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="RE_PM" Text='<%# Eval("RE_PM") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                    <asp:TemplateField HeaderText="Security Deposit">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="SecurityDeposit" Text='<%# Eval("SecurityDeposit") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Electricity Deposit">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="Electricity_Deposit" Text='<%# Eval("Electricity_Deposit") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Branch/office Address">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="PROP_ADDR" Text='<%# Eval("PROP_ADDR") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lease Sign Date">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LeaseSignDate" Text='<%# Eval("LeaseSignDate") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Lease Start Date">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LeaseStartDate" Text='<%# Eval("LeaseStartDate") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Lease End Date">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LeaseEndDate" Text='<%# Eval("LeaseEndDate") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Rent Start Date">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="RentStartDate" Text='<%# Eval("RentStartDate") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Escalation(Yes/No)">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="Escalation" Text='<%# Eval("Escalation") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Lock In">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="LockinPeriod" Text='<%# Eval("LockinPeriod") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Nos Car Parking">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="No_Car_Parking" Text='<%# Eval("No_Car_Parking") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Tax Applicable">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="PropertyTaxApplicable" Text='<%# Eval("PropertyTaxApplicable") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Property Tax Iibility">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="PropertyTax" Text='<%# Eval("PropertyTax") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Notice Period in Months">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="NoticePeriodinMonths" Text='<%# Eval("NoticePeriodinMonths") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Landlord Address">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="Address" Text='<%# Eval("Address") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Landlord Contact Details">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="ContactDetails" Text='<%# Eval("ContactDetails") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PAN No">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="PANNo" Text='<%# Eval("PANNo") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Email">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="Email" Text='<%# Eval("Email") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GST">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="GST" Text='<%# Eval("GST") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="GSTNumber">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="GSTNumber" Text='<%# Eval("GSTNumber") %>'> </asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agreement Category">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="PROP_NATURE" Text='<%# Eval("PROP_NATURE") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Source of Property">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" CssClass="form-control" Width="100" ID="ACQ_TRH" Text='<%# Eval("ACQ_TRH") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%-- <asp:TemplateField HeaderText="Type of Property">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="REQUEST_TYPE" Text='<%# Eval("REQUEST_TYPE") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Code">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="PROP_CODE" Text='<%# Eval("PROP_CODE") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Property Name">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="PROP_NAME" Text='<%# Eval("PROP_NAME") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Security Deposited Months">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="SecurityDepositedMonths" Text='<%# Eval("SecurityDepositedMonths") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Rent Free Period">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="RentFreePeriod" Text='<%# Eval("RentFreePeriod") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="No. of Landlords">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="NoofLandlords" Text='<%# Eval("NoofLandlords") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Landlord State">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="LL_State" Text='<%# Eval("LL_State") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Landlord City">
                                                <ItemTemplate>
                                                    <asp:TextBox runat="server" ID="LL_City" Text='<%# Eval("LL_City") %>'> </asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>

                                    <%--  <asp:TemplateField HeaderText="Inspection By">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Inspection_By" Text='<%# Eval("Inspection_By") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Inspection Date (dd-MM-yyyy)">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Inspection_date" Text='<%# Eval("Inspection_date") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Super Built Up Area(Sqft)">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Super_Built_up_area" Text='<%# Eval("Super_Built_up_area") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Built Up Area(Sqft) ">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="build_up_area" Text='<%# Eval("build_up_area") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Carpet Area(Sqft)">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Carpet_area" Text='<%# Eval("Carpet_area") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Rentable Area(Sqft)">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="Rentable_area" Text='<%# Eval("Rentable_area") %>'> </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
