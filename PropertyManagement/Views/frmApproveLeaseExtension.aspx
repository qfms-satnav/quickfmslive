<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmApproveLeaseExtension.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmApproveLeaseExtension"
    Title="Approve Lease Extension" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <%--<link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />--%>
   <%-- <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
    <script>
        function CheckAllDataGridCheckBoxes(aspCheckBoxID, checkVal) {
            re = new RegExp(aspCheckBoxID)
            for (i = 0; i < form1.elements.length; i++) {
                elm = document.forms[0].elements[i]
                if (elm.type == 'checkbox') {
                    if (re.test(elm.name)) {
                        if (elm.disabled == false)
                            elm.checked = checkVal
                    }
                }
            }
        }

            <%--check box validation--%>
        function validateCheckBoxesMyReq(flag) {
            var gridView = document.getElementById("<%=gvLDetailsLease.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    if (flag == "Approve")
                        return true;
                    else
                        return confirm('Are you sure you want to reject this Lease(s)?');
                }
            }
            alert("Please select atleast one Lease");
            return false;
        }

    </script>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
           <%-- <div class="widgets">
                <div ba-panel ba-panel-title="View Or Modify Maintenance Contract" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Approve / Reject Lease Extension</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                        <div class="widgets">
                             <h3>Approve / Reject Lease Extension</h3>
                        </div>
                          <div class="card">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel runat="server" ID="upfelren">
                                    <ContentTemplate>
                                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                        </asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="Tr1" runat="server" visible="false">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-12 control-label">Select Lease Type </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <asp:RequiredFieldValidator ID="rfvLtype" runat="server" ControlToValidate="ddlLtype"
                                                                Display="None" ErrorMessage="Please select LeaseType" ValidationGroup="Val1"
                                                                InitialValue="0"></asp:RequiredFieldValidator>
                                                            <asp:DropDownList ID="ddlLtype" runat="Server" CssClass="Selectpicker" data-live-search="true" AutoPostBack="True">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <label class="col-md-10 control-label">Search by Lease ID / Property Code / Name / City / Location <span style="color: red;">*</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <asp:RequiredFieldValidator ID="rfvEmpID" runat="server" ControlToValidate="txtLeaseId"
                                                                Display="None" ErrorMessage="Please Search By Property / Lease / Location Name" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                                            <asp:TextBox ID="txtLeaseId" runat="Server" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <asp:Button ID="btnsubmit" runat="server" CssClass="btn btn-primary custom-button-color" Text="Search" ValidationGroup="Val1" />
                                                            <asp:Button ID="btnReset" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" style="padding-top: 10px;">
                                            <div class="col-md-12">
                                                <asp:GridView ID="gvLDetailsLease" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                                    AllowPaging="True" PageSize="5" EmptyDataText="No Approved Lease Details Found." CssClass="table GridStyle" GridLines="none"
                                                    Style="font-size: 12px;">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="chkAll" runat="server" TextAlign="Right" onclick="javascript:CheckAllDataGridCheckBoxes('chkItem', this.checked);"
                                                                    ToolTip="Click to check all" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="chkItem" runat="server" ToolTip="Click to check" onclick="javascript:ChildClick(this);" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="50px" HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lease">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hLinkDetails" runat="server" NavigateUrl='<%#Eval("PM_LES_SNO", "~/PropertyManagement/Views/LeaseExtensionAppRej.aspx?id={0}")%>'
                                                                    Text='<%# Eval("LEASE_ID")%> '></asp:HyperLink>
                                                                <asp:Label ID="lbllname" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LES_SNO")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblsno" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_ID")%>' Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Property Code">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCode" runat="server" CssClass="lblCODE" Text='<%#Eval("PM_PPT_CODE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Property Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="City">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblcity" runat="server" CssClass="clsLabel" Text='<%#Eval("CTY_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Location">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbllcm" runat="server" CssClass="lblLCM" Text='<%#Eval("LCM_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lease Start Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblsdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EFFE_DT_AGREEMENT")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lease Expiry Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblEdate" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LAD_EXP_DT_AGREEMENT")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lease Extended Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLL" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LE_END_DT")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lease Status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_STATUS")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Created By">
                                                            <ItemTemplate>
                                                                <asp:Label ID="Lbluser" runat="server" CssClass="clsLabel" Text='<%#Eval("CREATED_BY")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                                <br />
                                                <div class="row" id="pnlbutton" runat="server">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remarks<span style="color: red;">*</span> </label>
                                                            <asp:RegularExpressionValidator ID="RegExpRem" runat="server" ControlToValidate="txtremarks"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter Remarks upto 500 Characters, special characters (-/_@$)(&*,space) are allowed">
                                                            </asp:RegularExpressionValidator>
                                                            <asp:TextBox ID="txtRemarks" CssClass="form-control" Width="100%" Height="30%" runat="server" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                                        <div class="form-group">
                                                            <asp:Button ID="btnAppall" Text="Approve" runat="server" CssClass="btn btn-primary custom-button-color" ValidationGroup="Val1" OnClientClick="javascript:return validateCheckBoxesMyReq('Approve');"></asp:Button>
                                                            <asp:Button ID="btnRejAll" Text="Reject" runat="server" CssClass="btn btn-primary custom-button-color" OnClientClick="javascript:return validateCheckBoxesMyReq('Reject');"></asp:Button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        </br>
                                          
                                       <%--  <div class="row" style="padding-top: 10px;">
                                             <div class="col-md-3 col-sm-6 col-xs-12">
                                                 <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" Width="100%" placeholder="Search"></asp:TextBox>
                                             </div>
                                             <div class="col-md-3 col-sm-6 col-xs-12" style="padding-left: 10px">

                                                 <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn btn-primary custom-button-color pull-left" />
                                                 &nbsp
                                                     <asp:Button ID="BtnClear" runat="server" CssClass="btn btn-primary custom-button-color" Text="Reset" />

                                             </div>
                                         </div>--%>
                                        <%--<div class="row" style="padding-top: 10px;">
                                            <div class="col-md-12">
                                                <asp:GridView ID="Documentsbindinggrid" runat="server" AutoGenerateColumns="False" AllowSorting="True"
                                                    AllowPaging="True" PageSize="5" EmptyDataText="No Approved Lease Details Found." CssClass="table table-condensed table-bordered table-hover table-striped"
                                                    Style="font-size: 12px;">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Lease ID">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblsno" runat="server" CssClass="clsLabel" Text='<%#Eval("LEASE_ID")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Property Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblName" runat="server" CssClass="lblName" Text='<%#Eval("PM_PPT_NAME")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Lease Extended Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLL" runat="server" CssClass="clsLabel" Text='<%#Eval("PM_LE_END_DT")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lease Extension Status">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLstatus" runat="server" CssClass="clsLabel" Text='<%#Eval("STA_TITLE")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Lease Extension Document">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lblLseN" runat="server" Text='<%#Eval("PM_LE_POS_LETTER_NAME")%>' CommandName="Document"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" />
                                                    <PagerStyle CssClass="pagination-ys" />
                                                </asp:GridView>
                                                <br />

                                            </div>
                                        </div>--%>

                                        

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </form>
                        </div>
                    </div>
              <%--  </div>
            </div>
        </div>--%>
    </div>
    <div class="modal fade" id="myModal" tabindex='-1'>
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Lease Application form</h4>
                </div>
                <div class="modal-body" id="modelcontainer">
                    <%-- Content loads here --%>
                </div>
            </div>
        </div>
    </div>

    <script>
        function showPopWin(id) {
            $("#modelcontainer").load("frmViewLeaseDetailsuser.aspx?id=" + id, function (responseTxt, statusTxt, xhr) {
                $("#myModal").modal('show');
            });
        }
    </script>

    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
