<%@ Page Language="VB" AutoEventWireup="false" CodeFile="frmSendRentRemainder.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_frmSendRentRemainder" Title="Send Rent Remainder" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <%--<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />--%>

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <%--<div class="widgets">
               -div ba-panel ba-panel-title="View Work Order" ba-panel-class="with-scroll">

                    <div class="panel">
                        <<div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Sent Rent Reminder</h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">--%>
                        
                       <div class="widgets">
                        <h3>Sent Rent Reminder</h3>
                         </div>
                           <div class="card">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" ValidationGroup="Val1" runat="server" />
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvTenantRent" runat="server" EmptyDataText="No Rent Reminder Found."
                                            AutoGenerateColumns="false" AllowPaging="true"
                                            CssClass="table table-condensed table-bordered table-hover table-striped">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Property Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcode" runat="server" Text='<%# Eval("Property_Code")%>'>
                                                        </asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Property Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblname" runat="server" Text='<%# Eval("Property_Name")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Location">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLoc" runat="server" Text='<%# Eval("Location")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reminder Sent Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRSdate" runat="server" Text='<%# Eval("SENTDATE") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#366599" ForeColor="#E2E2E2" HorizontalAlign="Center" />
                                            <PagerStyle CssClass="pagination-ys" />
                                        </asp:GridView>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
           <%-- </div>
        </div>--%>
    <%--</div>--%>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
