Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration.ConfigurationManager
Imports SubSonic
Imports System.IO

Partial Class WorkSpace_SMS_Webfiles_frmAddPropertyDetails
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Page.Form.Attributes.Add("enctype", "multipart/form-data")
        Dim scriptManager As ScriptManager = ScriptManager.GetCurrent(Me)

        If scriptManager IsNot Nothing Then
            scriptManager.RegisterPostBackControl(btnSubmit)
            scriptManager.RegisterPostBackControl(btnsaveasdraft)
            scriptManager.RegisterPostBackControl(btnAddMore)
            scriptManager.RegisterPostBackControl(gvPropdocs)
        End If
        If IsPostBack Then
            ScriptManager.RegisterClientScriptBlock(Me, Me.[GetType](), "anything", "refreshSelectpicker();", True)
        End If
        Dim path As String = HttpContext.Current.Request.Url.AbsolutePath
        Dim host As String = HttpContext.Current.Request.Url.Host
        Dim param(1) As SqlParameter
        param(0) = New SqlParameter("@ROL_ID", SqlDbType.VarChar, 50)
        param(0).Value = Session("UID")
        param(1) = New SqlParameter("@ROL_PATH", SqlDbType.VarChar, 200)
        param(1).Value = path
        Using sdr As SqlDataReader = SqlHelper.ExecuteReader(CommandType.StoredProcedure, "GN_VALIDATE_USR_PATH", param)
            If Session("UID") = "" Then
                Response.Redirect(Application("FMGLogout"))
            Else
                If sdr.HasRows Then
                Else
                    Response.Redirect(Application("FMGLogout"))
                End If
            End If
        End Using
        revtxtToilet.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        revpropertyname.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator6.ValidationExpression = User_Validation.GetValidationExpressionForLat.VAL_EXPR()
        RegularExpressionValidator9.ValidationExpression = User_Validation.GetValidationExpressionForLong.VAL_EXPR()
        REVPHNO.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        RegularExpressionValidator10.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        rfv.ValidationExpression = User_Validation.GetValidationExpressionForEmail.VAL_EXPR()
        RegularExpressionValidator12.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revBuiltupArea.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revCarpetarea.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revCommon.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revRent.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revUsable.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        RegularExpressionValidator13.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        'rfvtxtCeilingHight.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        'rfvtxtBeamBottomHight.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revmaxcapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        'revOptCapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        'rfvtxtSeatingCapacity.ValidationExpression = User_Validation.GetValidationExpressionForNumber.VAL_EXPR()
        revtxtFSI.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtEfficiency.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtPurPrice.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revtxtMarketValue.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        revgovt.ValidationExpression = User_Validation.GetValidationExpressionForRemarks.VAL_EXPR()
        RegularExpressionValidator1.ValidationExpression = User_Validation.GetValidationExpressionForDecimalValues.VAL_EXPR()
        rfvflooringtype.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator2.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        revsft.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator7.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator8.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        refsecdepmonths.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator11.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator14.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator15.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        revrentfree.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        RegularExpressionValidator16.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()
        revAgree.ValidationExpression = User_Validation.GetValidationExpressionForName.VAL_EXPR()


        If Not IsPostBack Then
            filSaveDraftlReqs()
            BindAcquisitionthrough()
            BindRequestTypes()
            AssigTextValues()
            BindPropType()
            BindInsuranceType()
            BindUserCities()
            BindFlooringTypes()
            BindEntity()
            ddlFSI.SelectedIndex = 1
            ddlRecommended.SelectedIndex = 1
            If Request.QueryString("Rid") <> Nothing Then   'Getting Rid from View Property Details
                txtReqID.Text = Request.QueryString("Rid")
                Request.QueryString("Rid") = hdnReqid.Value
            Else
                BindReqId()
            End If

            ''txtcity.Visible = False
            txtfloors.Visible = False
            txtlocation.Visible = False
            txttower.Visible = False
            txtnoflrs.Visible = False
            Relocaddress.Visible = False
            Relocation.Visible = False
        End If

    End Sub
    Protected Sub ddlAgreementbyPOA_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAgreementbyPOA.SelectedIndexChanged
        If ddlAgreementbyPOA.SelectedValue = "Yes" Then
            panPOA.Visible = True
        Else
            panPOA.Visible = False
        End If
    End Sub
    Protected Sub ddlFlooring_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFlooring.SelectedIndexChanged
        If ddlFlooring.SelectedValue = "No" Then
            flrRmks.Visible = True
        Else
            flrRmks.Visible = False
        End If
    End Sub


    Public Sub insertnewrecord()
        Try
            Dim param As SqlParameter() = New SqlParameter(132) {}

            'General Details
            param(0) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(0).Value = txtReqID.Text
            param(1) = New SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar)
            param(1).Value = ddlReqType.SelectedValue
            param(2) = New SqlParameter("@PROP_NATURE", SqlDbType.VarChar)
            param(2).Value = ddlPprtNature.SelectedValue
            param(3) = New SqlParameter("@ACQ_TRH", SqlDbType.VarChar)
            param(3).Value = ddlAcqThr.SelectedValue
            param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
            param(4).Value = ddlCity.SelectedValue
            If ddlReqType.SelectedItem.Text = "New" Then
                param(122) = New SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar)
                param(122).Value = txtnoflrs.Text
                param(123) = New SqlParameter("@RELOCATION", SqlDbType.VarChar)
                param(123).Value = Txtrelocation.Text
                param(124) = New SqlParameter("@RELOCADDRS", SqlDbType.VarChar)
                param(124).Value = txtrelocadd.Text
                param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
                param(5).Value = txtlocation.Text
                param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
                param(6).Value = txttower.Text
                param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
                param(7).Value = txtfloors.Text
            ElseIf ddlReqType.SelectedItem.Text = "Relocation" Then
                param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
                param(5).Value = ddlLocation.SelectedValue
                param(122) = New SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar)
                param(122).Value = txtnoflrs.Text
                param(123) = New SqlParameter("@RELOCATION", SqlDbType.VarChar)
                param(123).Value = Txtrelocation.Text
                param(124) = New SqlParameter("@RELOCADDRS", SqlDbType.VarChar)
                param(124).Value = txtrelocadd.Text
                param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
                param(6).Value = txttower.Text
                param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
                param(7).Value = txtfloors.Text
            Else
                'param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
                'param(4).Value = ddlCity.SelectedValue
                param(122) = New SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar)
                param(122).Value = txtFloor.Text
                param(123) = New SqlParameter("@RELOCATION", SqlDbType.VarChar)
                param(123).Value = ""
                param(124) = New SqlParameter("@RELOCADDRS", SqlDbType.VarChar)
                param(124).Value = ""
                param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
                param(5).Value = ddlLocation.SelectedValue
                param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
                param(6).Value = ddlTower.SelectedValue
                param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
                'param(7).Value = ddlFloor.SelectedValue
                param(7).Value = ([String].Join(",", ddlFloor.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value)))
            End If
            param(8) = New SqlParameter("@TOI_BLKS", SqlDbType.Int)
            param(8).Value = IIf(txtToilet.Text = "", 0, txtToilet.Text)
            param(9) = New SqlParameter("@PROP_TYPE", SqlDbType.VarChar)
            param(9).Value = ddlPropertyType.SelectedValue
            param(10) = New SqlParameter("@PROP_NAME", SqlDbType.VarChar)
            param(10).Value = txtPropIDName.Text

            param(11) = New SqlParameter("@ESTD_YR", SqlDbType.Date)
            param(11).Value = IIf(txtESTD.Text = "", DBNull.Value, txtESTD.Text)
            param(12) = New SqlParameter("@AGE", SqlDbType.VarChar)
            param(12).Value = IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            param(13) = New SqlParameter("@PROP_ADDR", SqlDbType.VarChar)
            param(13).Value = txtPropDesc.Text
            param(14) = New SqlParameter("@SING_LOC", SqlDbType.VarChar)
            param(14).Value = txtSignageLocation.Text

            param(15) = New SqlParameter("@SOC_NAME", SqlDbType.VarChar)
            param(15).Value = txtSocityName.Text
            param(16) = New SqlParameter("@LATITUDE", SqlDbType.VarChar)
            param(16).Value = txtlat.Text
            param(17) = New SqlParameter("@LONGITUDE", SqlDbType.VarChar)
            param(17).Value = txtlong.Text
            param(18) = New SqlParameter("@SCOPE_WK", SqlDbType.VarChar)
            param(18).Value = txtOwnScopeWork.Text

            param(19) = New SqlParameter("@RECM_PROP", SqlDbType.VarChar)
            param(19).Value = ddlRecommended.SelectedValue
            param(20) = New SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar)
            param(20).Value = txtRecmRemarks.Text

            'Owner Details
            param(21) = New SqlParameter("@OWN_NAME", SqlDbType.VarChar)
            param(21).Value = txtownrname.Text
            param(22) = New SqlParameter("@OWN_PH", SqlDbType.VarChar)
            param(22).Value = txtphno.Text
            param(23) = New SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar)
            param(23).Value = txtPrvOwnName.Text
            param(24) = New SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar)
            param(24).Value = txtPrvOwnPhNo.Text
            param(25) = New SqlParameter("@OWN_EMAIL", SqlDbType.VarChar)
            param(25).Value = txtOwnEmail.Text
            param(125) = New SqlParameter("@OWN_ADDRESS", SqlDbType.VarChar)
            param(125).Value = txtownadd.Text

            'Area Details
            param(26) = New SqlParameter("@CARPET_AREA", SqlDbType.Decimal)
            param(26).Value = Convert.ToDecimal(txtCarpetArea.Text)
            param(27) = New SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal)
            param(27).Value = Convert.ToDecimal(txtBuiltupArea.Text)
            param(28) = New SqlParameter("@COMMON_AREA", SqlDbType.Decimal)
            param(28).Value = Convert.ToDecimal(txtCommonArea.Text)
            param(29) = New SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal)
            param(29).Value = Convert.ToDecimal(txtRentableArea.Text)

            param(30) = New SqlParameter("@USABLE_AREA", SqlDbType.Decimal)
            param(30).Value = Convert.ToDecimal(txtUsableArea.Text)
            param(31) = New SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal)
            param(31).Value = Convert.ToDecimal(txtSuperBulArea.Text)
            param(32) = New SqlParameter("@PLOT_AREA", SqlDbType.Decimal)
            param(32).Value = Convert.ToDecimal(txtPlotArea.Text)
            param(33) = New SqlParameter("@FTC_HIGHT", SqlDbType.Decimal)
            param(33).Value = IIf(txtCeilingHight.Text = "", 0, txtCeilingHight.Text)

            param(34) = New SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal)
            param(34).Value = IIf(txtBeamBottomHight.Text = "", 0, txtBeamBottomHight.Text)
            param(35) = New SqlParameter("@MAX_CAPACITY", SqlDbType.Int)
            param(35).Value = Convert.ToInt32(txtMaxCapacity.Text)
            param(36) = New SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int)
            param(36).Value = IIf(txtOptCapacity.Text = "", 0, txtOptCapacity.Text)
            param(37) = New SqlParameter("@SEATING_CAPACITY", SqlDbType.Int)
            param(37).Value = IIf(txtSeatingCapacity.Text = "", 0, txtSeatingCapacity.Text)

            param(38) = New SqlParameter("@FLR_TYPE", SqlDbType.VarChar)
            param(38).Value = ddlFlooringType.SelectedValue
            param(39) = New SqlParameter("@FSI", SqlDbType.VarChar)
            param(39).Value = ddlFSI.SelectedValue
            param(40) = New SqlParameter("@FSI_RATIO", SqlDbType.Decimal)
            param(40).Value = IIf(txtFSI.Text = "", 0, txtFSI.Text)
            param(41) = New SqlParameter("@PFR_EFF", SqlDbType.VarChar)
            param(41).Value = txtEfficiency.Text

            'PURCHASE DETAILS
            param(42) = New SqlParameter("@PUR_PRICE", SqlDbType.Decimal)
            param(42).Value = IIf(txtPurPrice.Text = "", 0, txtPurPrice.Text)
            param(43) = New SqlParameter("@PUR_DATE", SqlDbType.Date)
            param(43).Value = IIf(txtPurDate.Text = "", DBNull.Value, txtPurDate.Text)
            param(44) = New SqlParameter("@MARK_VALUE", SqlDbType.Decimal)
            param(44).Value = IIf(txtMarketValue.Text = "", 0, txtMarketValue.Text)

            'GOVT DETAILS
            param(45) = New SqlParameter("@IRDA", SqlDbType.VarChar)
            param(45).Value = txtIRDA.Text
            param(46) = New SqlParameter("@PC_CODE", SqlDbType.VarChar)
            param(46).Value = txtPCcode.Text
            param(47) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar)
            param(47).Value = txtGovtPropCode.Text
            param(48) = New SqlParameter("@UOM_CODE", SqlDbType.VarChar)
            param(48).Value = txtUOM_CODE.Text

            'INSURANCE DETAILS
            param(49) = New SqlParameter("@IN_TYPE", SqlDbType.VarChar)
            param(49).Value = ddlInsuranceType.SelectedValue
            param(50) = New SqlParameter("@IN_VENDOR", SqlDbType.VarChar)
            param(50).Value = txtInsuranceVendor.Text
            param(51) = New SqlParameter("@IN_AMOUNT", SqlDbType.Decimal)
            param(51).Value = IIf(txtInsuranceAmt.Text = "", 0, txtInsuranceAmt.Text)
            param(52) = New SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar)
            param(52).Value = txtInsurancePolNum.Text

            param(53) = New SqlParameter("@IN_SDATE", SqlDbType.VarChar)
            param(53).Value = IIf(txtInsuranceStartdate.Text = "", DBNull.Value, txtInsuranceStartdate.Text)
            param(54) = New SqlParameter("@IN_EDATE", SqlDbType.VarChar)
            param(54).Value = IIf(txtInsuranceEnddate.Text = "", DBNull.Value, txtInsuranceEnddate.Text)

            param(55) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(55).Value = Session("uid")
            param(56) = New SqlParameter("@CMP_ID", SqlDbType.VarChar)
            param(56).Value = Session("COMPANYID")
            param(57) = New SqlParameter("@OWN_LANDLINE", SqlDbType.VarChar)
            param(57).Value = IIf(txtllno.Text = "", DBNull.Value, txtllno.Text)
            param(58) = New SqlParameter("@POA_SIGNED", SqlDbType.VarChar)
            param(58).Value = IIf(ddlAgreementbyPOA.SelectedValue = "", DBNull.Value, ddlAgreementbyPOA.SelectedValue)
            'POA Details
            param(59) = New SqlParameter("@POA_NAME", SqlDbType.VarChar)
            param(59).Value = IIf(txtPOAName.Text = "", DBNull.Value, txtPOAName.Text)
            param(60) = New SqlParameter("@POA_ADDRESS", SqlDbType.VarChar)
            param(60).Value = IIf(txtPOAAddress.Text = "", DBNull.Value, txtPOAAddress.Text)
            param(61) = New SqlParameter("@POA_MOBILE", SqlDbType.VarChar)
            param(61).Value = IIf(txtPOAMobile.Text = "", DBNull.Value, txtPOAMobile.Text)
            param(62) = New SqlParameter("@POA_EMAIL", SqlDbType.VarChar)
            param(62).Value = IIf(txtPOAEmail.Text = "", DBNull.Value, txtPOAEmail.Text)
            param(63) = New SqlParameter("@POA_LLTYPE", SqlDbType.VarChar)
            param(63).Value = IIf(txtLLtype.Text = "", DBNull.Value, txtLLtype.Text)


            ' PROPERTY IMAGES UPLOAD
            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas

            If fu1.PostedFiles IsNot Nothing Then
                For Each File In fu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            Dim Imgclass1 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC1 As ImageClas

            If fu2.PostedFiles IsNot Nothing Then
                For Each File In fu2.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC1 = New ImageClas()
                        IC1.Filename = File.FileName
                        IC1.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass1.Add(IC1)
                    End If
                Next
            End If
            Dim Imgclass2 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC2 As ImageClas

            If fu3.PostedFiles IsNot Nothing Then
                For Each File In fu3.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC2 = New ImageClas()
                        IC2.Filename = File.FileName
                        IC2.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass2.Add(IC2)
                    End If
                Next
            End If

            Dim Imgclass3 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC3 As ImageClas

            If fu4.PostedFiles IsNot Nothing Then
                For Each File In fu4.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC3 = New ImageClas()
                        IC3.Filename = File.FileName
                        IC3.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass3.Add(IC3)
                    End If
                Next
            End If

            param(64) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(64).Value = UtilityService.ConvertToDataTable(Imgclass)

            param(65) = New SqlParameter("@ENTITY", SqlDbType.VarChar)
            param(65).Value = IIf(ddlentity.SelectedValue = "", DBNull.Value, ddlentity.SelectedValue)

            'Physical Condition

            param(66) = New SqlParameter("@PHYCDTN_WALLS", SqlDbType.VarChar)
            param(66).Value = IIf(txtWalls.Text = "", DBNull.Value, txtWalls.Text)
            param(67) = New SqlParameter("@PHYCDTN_ROOF", SqlDbType.VarChar)
            param(67).Value = IIf(txtRoof.Text = "", DBNull.Value, txtRoof.Text)
            param(68) = New SqlParameter("@PHYCDTN_CEILING", SqlDbType.VarChar)
            param(68).Value = IIf(txtCeiling.Text = "", DBNull.Value, txtCeiling.Text)
            param(69) = New SqlParameter("@PHYCDTN_WINDOWS", SqlDbType.VarChar)
            param(69).Value = IIf(txtwindows.Text = "", DBNull.Value, txtwindows.Text)
            param(70) = New SqlParameter("@PHYCDTN_DAMAGE", SqlDbType.VarChar)
            param(70).Value = IIf(txtDamage.Text = "", DBNull.Value, txtDamage.Text)
            param(71) = New SqlParameter("@PHYCDTN_SEEPAGE", SqlDbType.VarChar)
            param(71).Value = IIf(txtSeepage.Text = "", DBNull.Value, txtSeepage.Text)
            param(72) = New SqlParameter("@PHYCDTN_OTHRTNT", SqlDbType.VarChar)
            param(72).Value = IIf(txtOthrtnt.Text = "", DBNull.Value, txtOthrtnt.Text)
            param(126) = New SqlParameter("@PHYCDTN_OTHRBLDG", SqlDbType.VarChar)
            param(126).Value = IIf(txtBuilding.Text = "", DBNull.Value, txtBuilding.Text)


            'Landlord's Scope of work 
            param(73) = New SqlParameter("@LL_VITRIFIED", SqlDbType.VarChar)
            param(73).Value = IIf(ddlvitrified.SelectedValue = "", DBNull.Value, ddlvitrified.SelectedValue)
            param(74) = New SqlParameter("@LL_VITRIFIED_RMKS", SqlDbType.VarChar)
            param(74).Value = IIf(txtVitriRemarks.Text = "", DBNull.Value, txtVitriRemarks.Text)
            param(75) = New SqlParameter("@LL_WASHRMS", SqlDbType.VarChar)
            param(75).Value = IIf(ddlWashroom.SelectedValue = "", DBNull.Value, ddlWashroom.SelectedValue)
            param(76) = New SqlParameter("@LL_WASHRM_RMKS", SqlDbType.VarChar)
            param(76).Value = IIf(txtWashroom.Text = "", DBNull.Value, txtWashroom.Text)
            param(77) = New SqlParameter("@LL_PANTRY", SqlDbType.VarChar)
            param(77).Value = IIf(ddlPantry.SelectedValue = "", DBNull.Value, ddlPantry.SelectedValue)
            param(78) = New SqlParameter("@LL_PANTRY_RMKS", SqlDbType.VarChar)
            param(78).Value = IIf(txtPantry.Text = "", DBNull.Value, txtPantry.Text)
            param(79) = New SqlParameter("@LL_SHUTTER", SqlDbType.VarChar)
            param(79).Value = IIf(ddlShutter.SelectedValue = "", DBNull.Value, ddlShutter.SelectedValue)
            param(80) = New SqlParameter("@LL_SHUTTER_RMKS", SqlDbType.VarChar)
            param(80).Value = IIf(txtShutter.Text = "", DBNull.Value, txtShutter.Text)
            param(81) = New SqlParameter("@LL_OTHERS", SqlDbType.VarChar)
            param(81).Value = IIf(ddlOthers.SelectedValue = "", DBNull.Value, ddlOthers.SelectedValue)
            param(82) = New SqlParameter("@LL_OTHERS_RMKS", SqlDbType.VarChar)
            param(82).Value = IIf(txtOthers.Text = "", DBNull.Value, txtOthers.Text)
            param(83) = New SqlParameter("@LL_WORK_DAYS", SqlDbType.VarChar)
            param(83).Value = IIf(txtllwork.Text = "", DBNull.Value, txtllwork.Text)
            param(84) = New SqlParameter("@LL_ELEC_EXISTING", SqlDbType.VarChar)
            param(84).Value = IIf(txtElectricEx.Text = "", DBNull.Value, txtElectricEx.Text)
            param(85) = New SqlParameter("@LL_ELEC_REQUIRED", SqlDbType.VarChar)
            param(85).Value = IIf(txtElectricRq.Text = "", DBNull.Value, txtElectricRq.Text)
            'Other Details
            param(89) = New SqlParameter("@OTHR_FLOORING", SqlDbType.VarChar)
            param(89).Value = IIf(ddlFlooring.SelectedValue = "", DBNull.Value, ddlFlooring.SelectedValue)
            param(90) = New SqlParameter("@OTHR_FLRG_RMKS", SqlDbType.VarChar)
            param(90).Value = IIf(txtFlooring.Text = "", DBNull.Value, txtFlooring.Text)
            param(91) = New SqlParameter("@OTHR_WASH_EXISTING", SqlDbType.VarChar)
            param(91).Value = IIf(txtWashExisting.Text = "", DBNull.Value, txtWashExisting.Text)
            param(92) = New SqlParameter("@OTHR_WASH_REQUIRED", SqlDbType.VarChar)
            param(92).Value = IIf(txtWashRequirement.Text = "", DBNull.Value, txtWashRequirement.Text)
            param(93) = New SqlParameter("@OTHR_POTABLE_WTR", SqlDbType.VarChar)
            param(93).Value = IIf(ddlPotableWater.SelectedValue = "", DBNull.Value, ddlPotableWater.SelectedValue)
            param(127) = New SqlParameter("@OTHR_POTABLE_WTR_RMKS", SqlDbType.VarChar)
            param(127).Value = IIf(txtPotableRemarks.Text = "", DBNull.Value, txtPotableRemarks.Text)
            param(128) = New SqlParameter("@OTHR_IT_INSTALL", SqlDbType.VarChar)
            param(128).Value = IIf(txtIT.Text = "", DBNull.Value, txtIT.Text)
            param(129) = New SqlParameter("@OTHR_IT_INSTALL_RMKS", SqlDbType.VarChar)
            param(129).Value = IIf(txtITRemarks.Text = "", DBNull.Value, txtITRemarks.Text)
            param(130) = New SqlParameter("@OTHR_DG_INSTALL", SqlDbType.VarChar)
            param(130).Value = IIf(txtDG.Text = "", DBNull.Value, txtDG.Text)
            param(131) = New SqlParameter("@OTHR_DG_INSTALL_RMKS", SqlDbType.VarChar)
            param(131).Value = IIf(txtDGRemarks.Text = "", DBNull.Value, txtDGRemarks.Text)

            'Cost Details
            param(94) = New SqlParameter("@COST_RENT", SqlDbType.VarChar)
            param(94).Value = IIf(txtRent.Text = "", DBNull.Value, txtRent.Text)
            param(95) = New SqlParameter("@COST_RENT_SFT", SqlDbType.VarChar)
            param(95).Value = IIf(txtsft.Text = "", DBNull.Value, txtsft.Text)
            param(96) = New SqlParameter("@COST_RATIO", SqlDbType.VarChar)
            param(96).Value = IIf(txtRatio.Text = "", DBNull.Value, txtRatio.Text)
            param(97) = New SqlParameter("@COST_OWN_SHARE", SqlDbType.VarChar)
            param(97).Value = IIf(txtownshare.Text = "", DBNull.Value, txtownshare.Text)
            param(98) = New SqlParameter("@COST_SECDEP", SqlDbType.VarChar)
            param(98).Value = IIf(txtsecdepmonths.Text = "", DBNull.Value, txtsecdepmonths.Text)
            param(99) = New SqlParameter("@COST_GST", SqlDbType.VarChar)
            param(99).Value = IIf(ddlgst.SelectedValue = "", DBNull.Value, ddlgst.SelectedValue)
            param(100) = New SqlParameter("@COST_MAINTENANCE", SqlDbType.VarChar)
            param(100).Value = IIf(txtmaintenance.Text = "", DBNull.Value, txtmaintenance.Text)
            param(101) = New SqlParameter("@COST_ESC", SqlDbType.VarChar)
            param(101).Value = IIf(txtesc.Text = "", DBNull.Value, txtesc.Text)
            param(102) = New SqlParameter("@COST_RENT_FREE", SqlDbType.VarChar)
            param(102).Value = IIf(txtRentFree.Text = "", DBNull.Value, txtRentFree.Text)
            param(103) = New SqlParameter("@COST_STAMP", SqlDbType.VarChar)
            param(103).Value = IIf(ddlStampReg.SelectedValue = "", DBNull.Value, ddlStampReg.SelectedValue)
            param(104) = New SqlParameter("@COST_AGREE", SqlDbType.VarChar)
            param(104).Value = IIf(txtAgree.Text = "", DBNull.Value, txtAgree.Text)
            'DOCUMENTS VAILABLE
            param(105) = New SqlParameter("@DOC_TITLE", SqlDbType.VarChar)
            param(105).Value = IIf(ddlTitle.SelectedValue = "", DBNull.Value, ddlTitle.SelectedValue)
            param(106) = New SqlParameter("@DOC_TITLE_RMKS", SqlDbType.VarChar)
            param(106).Value = IIf(txtTitleRemarks.Text = "", DBNull.Value, txtTitleRemarks.Text)
            param(107) = New SqlParameter("@DOC_OCCUP", SqlDbType.VarChar)
            param(107).Value = IIf(ddlOccupancy.SelectedValue = "", DBNull.Value, ddlOccupancy.SelectedValue)
            param(108) = New SqlParameter("@DOC_OCCUP_RMKS", SqlDbType.VarChar)
            param(108).Value = IIf(txtOccupancyRemarks.Text = "", DBNull.Value, txtOccupancyRemarks.Text)
            param(109) = New SqlParameter("@DOC_BUILD", SqlDbType.VarChar)
            param(109).Value = IIf(ddlBuilding.SelectedValue = "", DBNull.Value, ddlBuilding.SelectedValue)
            param(110) = New SqlParameter("@DOC_BUILD_RMKS", SqlDbType.VarChar)
            param(110).Value = IIf(txtBuildingRemarks.Text = "", DBNull.Value, txtBuildingRemarks.Text)
            param(111) = New SqlParameter("@DOC_PAN", SqlDbType.VarChar)
            param(111).Value = IIf(ddlPAN.SelectedValue = "", DBNull.Value, ddlPAN.SelectedValue)
            param(112) = New SqlParameter("@DOC_PAN_RMKS", SqlDbType.VarChar)
            param(112).Value = IIf(txtPANRemarks.Text = "", DBNull.Value, txtPANRemarks.Text)
            param(86) = New SqlParameter("@DOC_TAX", SqlDbType.VarChar)
            param(86).Value = IIf(ddlTax.SelectedValue = "", DBNull.Value, ddlTax.SelectedValue)
            param(87) = New SqlParameter("@DOC_TAX_RMKS", SqlDbType.VarChar)
            param(87).Value = IIf(txtTaxRemarks.Text = "", DBNull.Value, txtTaxRemarks.Text)
            param(88) = New SqlParameter("@DOC_OTHR_INFO", SqlDbType.VarChar)
            param(88).Value = IIf(txtOtherInfo.Text = "", DBNull.Value, txtOtherInfo.Text)

            param(113) = New SqlParameter("@SIGNAGEIMAGES", SqlDbType.Structured)
            param(113).Value = UtilityService.ConvertToDataTable(Imgclass1)
            param(114) = New SqlParameter("@SIG_LENGTH", SqlDbType.VarChar)
            param(114).Value = IIf(txtsiglength.Text = "", DBNull.Value, txtsiglength.Text)
            param(115) = New SqlParameter("@SIG_WIDTH", SqlDbType.VarChar)
            param(115).Value = IIf(txtsigwidth.Text = "", DBNull.Value, txtsigwidth.Text)
            param(116) = New SqlParameter("@AC_OUTDOOR", SqlDbType.VarChar)
            param(116).Value = IIf(txtACOutdoor.Text = "", DBNull.Value, txtACOutdoor.Text)
            param(117) = New SqlParameter("@GSB", SqlDbType.VarChar)
            param(117).Value = IIf(txtGSB.Text = "", DBNull.Value, txtGSB.Text)
            param(118) = New SqlParameter("@AC_OUTDOOR_IMAGE", SqlDbType.Structured)
            param(118).Value = UtilityService.ConvertToDataTable(Imgclass2)
            param(119) = New SqlParameter("@GSB_IMAGE", SqlDbType.Structured)
            param(119).Value = UtilityService.ConvertToDataTable(Imgclass3)
            param(120) = New SqlParameter("@INSPECTION_DATE", SqlDbType.DateTime)
            param(120).Value = Convert.ToDateTime(txtsdate.Text)
            param(121) = New SqlParameter("@INSPECTION_BY", SqlDbType.VarChar)
            param(121).Value = IIf(txtInspection.Text = "", DBNull.Value, txtInspection.Text)
            param(132) = New SqlParameter("@STATUS", SqlDbType.VarChar)
            param(132).Value = "4001"
            'Dim ds As DataSet = New DataSet("DbDataSet")

            ' Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_ADD_PROPERTY_DETAILS", param)
            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "PM_PROPERTY_SAVEASDRAFT_DETAILS", param)
            If res = "SUCCESS" Then
                lblmsg.Text = "Property Successfully Added. (Request ID) " + txtReqID.Text
            Else
                lblmsg.Text = "Something went wrong; try again"
            End If
            'Cleardata()
            AssigTextValues()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub txttower_TextChanged(sender As Object, e As EventArgs) Handles txttower.TextChanged
        Dim loc As String = ddlLocation.SelectedItem.Value
        Dim Cty As String = ddlCity.SelectedItem.Value
        Dim entity As String = ddlentity.SelectedItem.Value
        Dim Twr As String = txttower.Text
        Txtrelocation.Text = entity + "/" + Twr + "/" + Cty
    End Sub
    Public Class ImageClas
        Private _fn As String
        Public Property Filename() As String
            Get
                Return _fn
            End Get
            Set(ByVal value As String)
                _fn = value
            End Set
        End Property

        Private _Fpath As String
        Public Property FilePath() As String
            Get
                Return _Fpath
            End Get
            Set(ByVal value As String)
                _Fpath = value
            End Set
        End Property

    End Class

    Private Sub BindFlooringTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_FLOORING_TYPES")
        ddlFlooringType.DataSource = sp3.GetDataSet()
        ddlFlooringType.DataTextField = "PM_FT_TYPE"
        ddlFlooringType.DataValueField = "PM_FT_SNO"
        ddlFlooringType.DataBind()
        ddlFlooringType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindReqId()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GENERATE_REQUESTID")
        sp3.Command.Parameters.Add("@REQ_TYPE", "ADDPROPERTY", DbType.String)
        Dim dt = DateTime.Now.ToString("MMddyyyy")
        txtReqID.Text = dt + "/PRPREQ/" + Convert.ToString(sp3.ExecuteScalar())
    End Sub

    Private Sub BindRequestTypes()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_REQUEST_TYPES")
        ddlReqType.DataSource = sp3.GetDataSet()
        ddlReqType.DataTextField = "PM_RT_TYPE"
        ddlReqType.DataValueField = "PM_RT_SNO"
        ddlReqType.DataBind()
        ddlReqType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Private Sub BindEntity()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ENTITY_TYPES")
        ddlentity.DataSource = sp3.GetDataSet()
        ddlentity.DataTextField = "CHE_NAME"
        ddlentity.DataValueField = "CHE_CODE"
        ddlentity.DataBind()
        ddlentity.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Private Sub BindUserCities()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTCTY")
        sp3.Command.AddParameter("@DUMMY", "", DbType.String)
        sp3.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlCity.DataSource = sp3.GetDataSet()
        ddlCity.DataTextField = "CTY_NAME"
        ddlCity.DataValueField = "CTY_CODE"
        ddlCity.DataBind()
        ddlCity.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindPropType()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACTPROPTYPE")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlPropertyType.DataSource = sp3.GetDataSet()
        ddlPropertyType.DataTextField = "PN_PROPERTYTYPE"
        ddlPropertyType.DataValueField = "PN_TYPEID"
        ddlPropertyType.DataBind()
        ddlPropertyType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub
    Private Sub BindAcquisitionthrough()
        Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_ACQISITION_THROUGH")
        sp3.Command.AddParameter("@dummy", Session("uid"), DbType.String)
        ddlAcqThr.DataSource = sp3.GetDataSet()
        ddlAcqThr.DataTextField = "PN_ACQISITION_THROUGH"
        ddlAcqThr.DataValueField = "PN_ACQISITION_ID"
        ddlAcqThr.DataBind()
        ddlAcqThr.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Private Sub BindInsuranceType()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_INSURANCE_TYPE")
        ddlInsuranceType.DataSource = sp.GetDataSet()
        ddlInsuranceType.DataTextField = "INSURANCE_TYPE"
        ddlInsuranceType.DataValueField = "ID"
        ddlInsuranceType.DataBind()
        ddlInsuranceType.Items.Insert(0, New ListItem("--Select--", 0))
    End Sub

    Public Sub AssigTextValues()
        txtSuperBulArea.Text = 0
        txtBuiltupArea.Text = 0
        txtCarpetArea.Text = 0
        txtCommonArea.Text = 0
        txtRentableArea.Text = 0
        txtUsableArea.Text = 0
        txtPlotArea.Text = 0
        txtCeilingHight.Text = 0
        txtBeamBottomHight.Text = 0
        txtMaxCapacity.Text = 0
        txtOptCapacity.Text = 0
        txtSeatingCapacity.Text = 0
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        'If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
        '    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than builtup area');</SCRIPT>", False)
        'If (Session("TENANT") <> "BNP.dbo") Then
        '    If (txtESTD.Text = "") Then
        '        lblmsg.Text = "Please Enter ESTD(Year)"
        '        txtESTD.Focus()
        '        Exit Sub
        '    End If
        'End If
        If Not Page.IsValid Then
            Exit Sub
        End If
        If txtSuperBulArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) > CInt(txtSuperBulArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Built-up Area cannot be more than Super Built-up Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtBuiltupArea.Text <> 0 And txtCarpetArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) < CInt(txtCarpetArea.Text) Then 'Or CInt(txtBuiltupArea.Text) > CInt(txtRentableArea.Text) 
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Built-up Area cannot be less than Carpet Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than Bulitup Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtBuiltupArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtSuperBulArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtSuperBulArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Built-up Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtSuperBulArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) > CInt(txtSuperBulArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Built-up Area cannot be less than Carpet Area');</SCRIPT>", False)
                Exit Sub
            Else
                insertnewrecord()
                Cleardata()
                AssigTextValues()
                BindReqId()
            End If
        Else
            insertnewrecord()
            Cleardata()
            AssigTextValues()
            BindReqId()
        End If

    End Sub

    Protected Sub btnAddMore_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddMore.Click
        'If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
        '    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than builtup area');</SCRIPT>", False)
        'ElseIf CInt(txtCommonArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtCommonArea.Text) > CInt(txtBuiltupArea.Text) Then
        '    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Common Area cannot be more than builtup area and Carpet');</SCRIPT>", False)

        'ElseIf CInt(txtRentableArea.Text) > CInt(txtBuiltupArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtCarpetArea.Text) Or CInt(txtRentableArea.Text) > CInt(txtUsableArea.Text) Then
        '    ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Rentable area cannot be more than built up, carpet area and usable area');</SCRIPT>", False)
        'Else
        '    insertnewrecord()
        '    Cleardata()
        'Cleardata()
        'AssigTextValues()
        'BindReqId()
        'End If
        If txtSuperBulArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) > CInt(txtSuperBulArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be more than Super Biultup Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtBuiltupArea.Text <> 0 And txtCarpetArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) < CInt(txtCarpetArea.Text) Then 'Or CInt(txtBuiltupArea.Text) > CInt(txtRentableArea.Text) 
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Built-up Area cannot be less than Carpet Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtBuiltupArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) > CInt(txtBuiltupArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be more than Bulitup Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtBuiltupArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtBuiltupArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Builtup Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Carpet Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtSuperBulArea.Text <> 0 And txtRentableArea.Text <> 0 Then
            If CInt(txtSuperBulArea.Text) < CInt(txtRentableArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Built-up Area cannot be less than Rentable Area');</SCRIPT>", False)
                Exit Sub
            End If
        End If
        If txtCarpetArea.Text <> 0 And txtSuperBulArea.Text <> 0 Then
            If CInt(txtCarpetArea.Text) > CInt(txtSuperBulArea.Text) Then
                ScriptManager.RegisterClientScriptBlock(Page, Me.GetType(), "s", "<SCRIPT>   alert('Super Built-up Area cannot be less than Carpet Area');</SCRIPT>", False)
                Exit Sub
            Else
                insertnewrecord()
                Cleardata()
            End If
        Else
            insertnewrecord()
            Cleardata()
            'AssigTextValues()
            'BindReqId()
        End If
    End Sub


    Private Sub Cleardata()
        ddlReqType.SelectedIndex = 0
        ddlPprtNature.SelectedIndex = 0
        ddlAcqThr.SelectedIndex = 0
        ddlCity.SelectedIndex = 0
        ddlLocation.Items.Clear()
        ddlTower.Items.Clear()
        txtFloor.Text = ""
        ddlFloor.Items.Clear()
        txtToilet.Text = ""
        ddlPropertyType.SelectedIndex = 0
        txtPropIDName.Text = ""
        txtESTD.Text = ""
        txtAge.Text = ""
        txtPropDesc.Text = ""
        txtSignageLocation.Text = ""
        txtSocityName.Text = ""
        txtlat.Text = ""
        txtlong.Text = ""
        txtOwnScopeWork.Text = ""
        txtRecmRemarks.Text = ""

        txtownrname.Text = ""
        txtphno.Text = ""
        txtPrvOwnName.Text = ""
        txtPrvOwnPhNo.Text = ""
        txtOwnEmail.Text = ""
        txtownadd.Text = ""

        txtCarpetArea.Text = ""
        txtBuiltupArea.Text = ""
        txtCommonArea.Text = ""
        txtRentableArea.Text = ""
        txtUsableArea.Text = ""
        txtSuperBulArea.Text = ""
        txtPlotArea.Text = ""
        txtCeilingHight.Text = ""
        txtBeamBottomHight.Text = ""
        txtMaxCapacity.Text = ""
        txtOptCapacity.Text = ""
        txtSeatingCapacity.Text = ""
        ddlFlooringType.SelectedIndex = 0
        txtFSI.Text = ""
        txtEfficiency.Text = ""

        txtPurPrice.Text = ""
        txtPurDate.Text = ""
        txtMarketValue.Text = ""

        txtIRDA.Text = ""
        txtPCcode.Text = ""
        txtGovtPropCode.Text = ""
        txtUOM_CODE.Text = ""

        ddlInsuranceType.SelectedIndex = 0
        txtInsuranceVendor.Text = ""
        txtInsuranceAmt.Text = ""
        txtInsurancePolNum.Text = ""
        txtInsuranceStartdate.Text = ""
        txtInsuranceEnddate.Text = ""

        ''txtcity.Text = ""
        txtlocation.Text = ""
        txttower.Text = ""
        txtfloors.Text = ""
        txtnoflrs.Text = ""
        txtrelocadd.Text = ""
        Txtrelocation.Text = ""

    End Sub

    Protected Sub ddlCity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCity.SelectedIndexChanged
        If ddlCity.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
            sp2.Command.AddParameter("@CITY", ddlCity.SelectedValue)
            sp2.Command.AddParameter("@USR_ID", Session("uid"))
            ddlLocation.DataSource = sp2.GetDataSet()
            ddlLocation.DataTextField = "LCM_NAME"
            ddlLocation.DataValueField = "LCM_CODE"
            ddlLocation.DataBind()
            ddlLocation.Items.Insert(0, New ListItem("--Select--", "0"))
            ddlTower.Items.Clear()
        Else
            ddlLocation.Items.Clear()
        End If
    End Sub

    Protected Sub ddlLocation_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLocation.SelectedIndexChanged
        If ddlLocation.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
            sp2.Command.AddParameter("@LCMID", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            ddlTower.DataSource = sp2.GetDataSet()
            ddlTower.DataTextField = "TWR_NAME"
            ddlTower.DataValueField = "TWR_CODE"
            ddlTower.DataBind()
            ddlTower.Items.Insert(0, New ListItem("--Select--", "0"))

            Dim sp3 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_ADDRESSBYLCMID")
            sp3.Command.AddParameter("@LCMID", ddlLocation.SelectedValue, DbType.String)
            sp3.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
            Dim ds As New DataSet
            ds = sp3.GetDataSet()
            If ds.Tables(0).Rows.Count > 0 Then
                txtrelocadd.Text = ds.Tables(0).Rows(0).Item("LCM_ADDR")
            End If
        Else
            ddlTower.Items.Clear()
        End If
    End Sub

    Protected Sub ddlTower_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTower.SelectedIndexChanged
        If ddlTower.SelectedIndex > 0 Then
            Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
            sp2.Command.AddParameter("@TWR_ID", ddlTower.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@FLR_CITY_CODE", ddlCity.SelectedValue, DbType.String)
            sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
            ddlFloor.DataSource = sp2.GetDataSet()
            ddlFloor.DataTextField = "FLR_NAME"
            ddlFloor.DataValueField = "FLR_CODE"
            ddlFloor.DataBind()
            ddlFloor.Items.Insert(0, New ListItem("--Select--", "0"))
            txtFloor.Text = ddlFloor.Items.Count - 1
        Else
            ddlFloor.Items.Clear()
        End If
    End Sub

    Protected Sub txtESTD_TextChanged(sender As Object, e As EventArgs) Handles txtESTD.TextChanged
        If (txtESTD.Text <> "") Then
            Dim frmdate As DateTime = txtESTD.Text
            Dim ts As Integer = (DateTime.Now - frmdate).Days
            Dim year As Integer = Math.Floor(ts / 365)
            Dim Month As Integer = (Math.Floor(ts / 30)) - (year * 12)
            txtAge.Text = (Convert.ToString(year) + " Year  " + Convert.ToString(Month)) + " Months"
        Else
            txtAge.Text = ""
        End If
    End Sub

    Protected Sub ddlRecommended_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlRecommended.SelectedIndexChanged
        divRecommanded.Visible = IIf(ddlRecommended.SelectedValue = "1", True, False)
    End Sub

    Protected Sub ddlFSI_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFSI.SelectedIndexChanged
        divFSI.Visible = IIf(ddlFSI.SelectedValue = "1", True, False)
    End Sub

    Private Sub ddlReqType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReqType.SelectedIndexChanged
        If ddlReqType.SelectedItem.Text = "New" Then
            ''txtcity.Visible = True
            txtfloors.Visible = True
            txtlocation.Visible = True
            txttower.Visible = True
            txtnoflrs.Visible = True
            ''ddlCity.Visible = False
            txtFloor.Visible = False
            ddlLocation.Visible = False
            ddlTower.Visible = False
            ddlFloor.Visible = False
            Relocaddress.Visible = False
            Relocation.Visible = False
            RequiredFieldValidator15.Enabled = False
            rfvddlTower.Enabled = False
            rfvddlFloor.Enabled = False
        ElseIf ddlReqType.SelectedItem.Text = "Relocation" Then
            '' txtcity.Visible = False
            txtfloors.Visible = True
            txtlocation.Visible = False
            txttower.Visible = True
            txtnoflrs.Visible = True
            '' ddlCity.Visible = True
            ddlLocation.Visible = True
            ddlTower.Visible = False
            ddlFloor.Visible = False
            txtFloor.Visible = False
            Relocaddress.Visible = True
            Relocation.Visible = True
            RequiredFieldValidator15.Enabled = False
            rfvddlTower.Enabled = False
            rfvddlFloor.Enabled = False
        Else
            txtfloors.Visible = False
            txtlocation.Visible = False
            txttower.Visible = False
            txtnoflrs.Visible = False
            '' ddlCity.Visible = True
            ddlLocation.Visible = True
            ddlTower.Visible = True
            ddlFloor.Visible = True
            RequiredFieldValidator15.Enabled = True
            rfvddlTower.Enabled = True
            rfvddlFloor.Enabled = True
        End If
        'ElseIf ddlReqType.SelectedIndex = "4" Then
        '    ''txtcity.Visible = False
        '    txtfloors.Visible = False
        '    txtlocation.Visible = False
        '    txttower.Visible = False
        '    ''ddlCity.Visible = True
        '    ddlLocation.Visible = True
        '    ddlTower.Visible = True
        '    ddlFloor.Visible = True
        '    txtnoflrs.Visible = False
        'End If
    End Sub

    Private Sub btnsaveasdraft_Click(sender As Object, e As EventArgs) Handles btnsaveasdraft.Click
        SubSaveasdraftrecords()
        'Cleardata()
        AssigTextValues()
        BindReqId()
    End Sub

    Public Sub SubSaveasdraftrecords()

        Try


            Dim param As SqlParameter() = New SqlParameter(132) {}

            'General Details
            param(0) = New SqlParameter("@REQUEST_ID", SqlDbType.VarChar)
            param(0).Value = txtReqID.Text
            param(1) = New SqlParameter("@REQUEST_TYPE", SqlDbType.VarChar)
            param(1).Value = ddlReqType.SelectedValue
            param(2) = New SqlParameter("@PROP_NATURE", SqlDbType.VarChar)
            param(2).Value = ddlPprtNature.SelectedValue
            param(3) = New SqlParameter("@ACQ_TRH", SqlDbType.VarChar)
            param(3).Value = ddlAcqThr.SelectedValue
            param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
            param(4).Value = ddlCity.SelectedValue
            If ddlReqType.SelectedItem.Text = "New" Then
                param(122) = New SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar)
                param(122).Value = txtnoflrs.Text
                param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
                param(5).Value = txtlocation.Text
                param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
                param(6).Value = txttower.Text
                param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
                param(7).Value = txtfloors.Text
                param(123) = New SqlParameter("@RELOCATION", SqlDbType.VarChar)
                param(123).Value = ""
                param(124) = New SqlParameter("@RELOCADDRS", SqlDbType.VarChar)
                param(124).Value = ""
            ElseIf ddlReqType.SelectedItem.Text = "Relocation" Then
                param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
                param(5).Value = ddlLocation.SelectedValue
                param(122) = New SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar)
                param(122).Value = txtnoflrs.Text
                param(123) = New SqlParameter("@RELOCATION", SqlDbType.VarChar)
                param(123).Value = Txtrelocation.Text
                param(124) = New SqlParameter("@RELOCADDRS", SqlDbType.VarChar)
                param(124).Value = txtrelocadd.Text
                param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
                param(6).Value = txttower.Text
                param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
                param(7).Value = txtfloors.Text
            Else
                'param(4) = New SqlParameter("@CITY", SqlDbType.VarChar)
                'param(4).Value = ddlCity.SelectedValue
                param(122) = New SqlParameter("@PM_PPT_TOT_FLRS", SqlDbType.VarChar)
                param(122).Value = txtFloor.Text
                param(123) = New SqlParameter("@RELOCATION", SqlDbType.VarChar)
                param(123).Value = ""
                param(124) = New SqlParameter("@RELOCADDRS", SqlDbType.VarChar)
                param(124).Value = ""
                param(5) = New SqlParameter("@LOCATION", SqlDbType.VarChar)
                param(5).Value = ddlLocation.SelectedValue
                param(6) = New SqlParameter("@TOWER", SqlDbType.VarChar)
                param(6).Value = ddlTower.SelectedValue
                param(7) = New SqlParameter("@FLOOR", SqlDbType.VarChar)
                'param(7).Value = ddlFloor.SelectedValue
                param(7).Value = ([String].Join(",", ddlFloor.Items.OfType(Of ListItem)().Where(Function(r) r.Selected).[Select](Function(r) r.Value)))
            End If
            param(8) = New SqlParameter("@TOI_BLKS", SqlDbType.Int)
            param(8).Value = IIf(txtToilet.Text = "", 0, txtToilet.Text)
            param(9) = New SqlParameter("@PROP_TYPE", SqlDbType.VarChar)
            param(9).Value = ddlPropertyType.SelectedValue
            param(10) = New SqlParameter("@PROP_NAME", SqlDbType.VarChar)
            param(10).Value = txtPropIDName.Text

            param(11) = New SqlParameter("@ESTD_YR", SqlDbType.Date)
            param(11).Value = IIf(txtESTD.Text = "", DBNull.Value, txtESTD.Text)
            param(12) = New SqlParameter("@AGE", SqlDbType.VarChar)
            param(12).Value = IIf(txtAge.Text = "", 0, Convert.ToString(txtAge.Text))
            param(13) = New SqlParameter("@PROP_ADDR", SqlDbType.VarChar)
            param(13).Value = txtPropDesc.Text
            param(14) = New SqlParameter("@SING_LOC", SqlDbType.VarChar)
            param(14).Value = txtSignageLocation.Text

            param(15) = New SqlParameter("@SOC_NAME", SqlDbType.VarChar)
            param(15).Value = txtSocityName.Text
            param(16) = New SqlParameter("@LATITUDE", SqlDbType.VarChar)
            param(16).Value = txtlat.Text
            param(17) = New SqlParameter("@LONGITUDE", SqlDbType.VarChar)
            param(17).Value = txtlong.Text
            param(18) = New SqlParameter("@SCOPE_WK", SqlDbType.VarChar)
            param(18).Value = txtOwnScopeWork.Text

            param(19) = New SqlParameter("@RECM_PROP", SqlDbType.VarChar)
            param(19).Value = ddlRecommended.SelectedValue
            param(20) = New SqlParameter("@RECM_PROP_REM", SqlDbType.VarChar)
            param(20).Value = txtRecmRemarks.Text

            'Owner Details
            param(21) = New SqlParameter("@OWN_NAME", SqlDbType.VarChar)
            param(21).Value = txtownrname.Text
            param(22) = New SqlParameter("@OWN_PH", SqlDbType.VarChar)
            param(22).Value = txtphno.Text
            param(23) = New SqlParameter("@PREV_OWN_NAME", SqlDbType.VarChar)
            param(23).Value = txtPrvOwnName.Text
            param(24) = New SqlParameter("@PREV_OWN_PH", SqlDbType.VarChar)
            param(24).Value = txtPrvOwnPhNo.Text
            param(25) = New SqlParameter("@OWN_EMAIL", SqlDbType.VarChar)
            param(25).Value = txtOwnEmail.Text
            param(125) = New SqlParameter("@OWN_ADDRESS", SqlDbType.VarChar)
            param(125).Value = txtownadd.Text

            'Area Details
            param(26) = New SqlParameter("@CARPET_AREA", SqlDbType.Decimal)
            param(26).Value = Convert.ToDecimal(txtCarpetArea.Text)
            param(27) = New SqlParameter("@BUILTUP_AREA", SqlDbType.Decimal)
            param(27).Value = Convert.ToDecimal(txtBuiltupArea.Text)
            param(28) = New SqlParameter("@COMMON_AREA", SqlDbType.Decimal)
            param(28).Value = Convert.ToDecimal(txtCommonArea.Text)
            param(29) = New SqlParameter("@RENTABLE_AREA", SqlDbType.Decimal)
            param(29).Value = Convert.ToDecimal(txtRentableArea.Text)

            param(30) = New SqlParameter("@USABLE_AREA", SqlDbType.Decimal)
            param(30).Value = Convert.ToDecimal(txtUsableArea.Text)
            param(31) = New SqlParameter("@SUPER_BLT_AREA", SqlDbType.Decimal)
            param(31).Value = Convert.ToDecimal(txtSuperBulArea.Text)
            param(32) = New SqlParameter("@PLOT_AREA", SqlDbType.Decimal)
            param(32).Value = Convert.ToDecimal(txtPlotArea.Text)
            param(33) = New SqlParameter("@FTC_HIGHT", SqlDbType.Decimal)
            param(33).Value = IIf(txtCeilingHight.Text = "", 0, txtCeilingHight.Text)

            param(34) = New SqlParameter("@FTBB_HIGHT", SqlDbType.Decimal)
            param(34).Value = IIf(txtBeamBottomHight.Text = "", 0, txtBeamBottomHight.Text)
            param(35) = New SqlParameter("@MAX_CAPACITY", SqlDbType.Int)
            param(35).Value = Convert.ToInt32(txtMaxCapacity.Text)
            param(36) = New SqlParameter("@OPTIMUM_CAPACITY", SqlDbType.Int)
            param(36).Value = IIf(txtOptCapacity.Text = "", 0, txtOptCapacity.Text)
            param(37) = New SqlParameter("@SEATING_CAPACITY", SqlDbType.Int)
            param(37).Value = IIf(txtSeatingCapacity.Text = "", 0, txtSeatingCapacity.Text)

            param(38) = New SqlParameter("@FLR_TYPE", SqlDbType.VarChar)
            param(38).Value = ddlFlooringType.SelectedValue
            param(39) = New SqlParameter("@FSI", SqlDbType.VarChar)
            param(39).Value = ddlFSI.SelectedValue
            param(40) = New SqlParameter("@FSI_RATIO", SqlDbType.Decimal)
            param(40).Value = IIf(txtFSI.Text = "", 0, txtFSI.Text)
            param(41) = New SqlParameter("@PFR_EFF", SqlDbType.VarChar)
            param(41).Value = txtEfficiency.Text

            'PURCHASE DETAILS
            param(42) = New SqlParameter("@PUR_PRICE", SqlDbType.Decimal)
            param(42).Value = IIf(txtPurPrice.Text = "", 0, txtPurPrice.Text)
            param(43) = New SqlParameter("@PUR_DATE", SqlDbType.Date)
            param(43).Value = IIf(txtPurDate.Text = "", DBNull.Value, txtPurDate.Text)
            param(44) = New SqlParameter("@MARK_VALUE", SqlDbType.Decimal)
            param(44).Value = IIf(txtMarketValue.Text = "", 0, txtMarketValue.Text)

            'GOVT DETAILS
            param(45) = New SqlParameter("@IRDA", SqlDbType.VarChar)
            param(45).Value = txtIRDA.Text
            param(46) = New SqlParameter("@PC_CODE", SqlDbType.VarChar)
            param(46).Value = txtPCcode.Text
            param(47) = New SqlParameter("@PROP_CODE", SqlDbType.VarChar)
            param(47).Value = txtGovtPropCode.Text
            param(48) = New SqlParameter("@UOM_CODE", SqlDbType.VarChar)
            param(48).Value = txtUOM_CODE.Text

            'INSURANCE DETAILS
            param(49) = New SqlParameter("@IN_TYPE", SqlDbType.VarChar)
            param(49).Value = ddlInsuranceType.SelectedValue
            param(50) = New SqlParameter("@IN_VENDOR", SqlDbType.VarChar)
            param(50).Value = txtInsuranceVendor.Text
            param(51) = New SqlParameter("@IN_AMOUNT", SqlDbType.Decimal)
            param(51).Value = IIf(txtInsuranceAmt.Text = "", 0, txtInsuranceAmt.Text)
            param(52) = New SqlParameter("@IN_POLICY_NUMBER", SqlDbType.VarChar)
            param(52).Value = txtInsurancePolNum.Text

            param(53) = New SqlParameter("@IN_SDATE", SqlDbType.VarChar)
            param(53).Value = IIf(txtInsuranceStartdate.Text = "", DBNull.Value, txtInsuranceStartdate.Text)
            param(54) = New SqlParameter("@IN_EDATE", SqlDbType.VarChar)
            param(54).Value = IIf(txtInsuranceEnddate.Text = "", DBNull.Value, txtInsuranceEnddate.Text)

            param(55) = New SqlParameter("@AUR_ID", SqlDbType.VarChar)
            param(55).Value = Session("uid")
            param(56) = New SqlParameter("@CMP_ID", SqlDbType.VarChar)
            param(56).Value = Session("COMPANYID")
            param(57) = New SqlParameter("@OWN_LANDLINE", SqlDbType.VarChar)
            param(57).Value = IIf(txtllno.Text = "", DBNull.Value, txtllno.Text)
            param(58) = New SqlParameter("@POA_SIGNED", SqlDbType.VarChar)
            param(58).Value = IIf(ddlAgreementbyPOA.SelectedValue = "", DBNull.Value, ddlAgreementbyPOA.SelectedValue)
            'POA Details
            param(59) = New SqlParameter("@POA_NAME", SqlDbType.VarChar)
            param(59).Value = IIf(txtPOAName.Text = "", DBNull.Value, txtPOAName.Text)
            param(60) = New SqlParameter("@POA_ADDRESS", SqlDbType.VarChar)
            param(60).Value = IIf(txtPOAAddress.Text = "", DBNull.Value, txtPOAAddress.Text)
            param(61) = New SqlParameter("@POA_MOBILE", SqlDbType.VarChar)
            param(61).Value = IIf(txtPOAMobile.Text = "", DBNull.Value, txtPOAMobile.Text)
            param(62) = New SqlParameter("@POA_EMAIL", SqlDbType.VarChar)
            param(62).Value = IIf(txtPOAEmail.Text = "", DBNull.Value, txtPOAEmail.Text)
            param(63) = New SqlParameter("@POA_LLTYPE", SqlDbType.VarChar)
            param(63).Value = IIf(txtLLtype.Text = "", DBNull.Value, txtLLtype.Text)


            ' PROPERTY IMAGES UPLOAD
            Dim Imgclass As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC As ImageClas

            If fu1.PostedFiles IsNot Nothing Then
                For Each File In fu1.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC = New ImageClas()
                        IC.Filename = File.FileName
                        IC.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass.Add(IC)
                    End If
                Next
            End If

            Dim Imgclass1 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC1 As ImageClas

            If fu2.PostedFiles IsNot Nothing Then
                For Each File In fu2.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC1 = New ImageClas()
                        IC1.Filename = File.FileName
                        IC1.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass1.Add(IC1)
                    End If
                Next
            End If
            Dim Imgclass2 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC2 As ImageClas

            If fu3.PostedFiles IsNot Nothing Then
                For Each File In fu3.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC2 = New ImageClas()
                        IC2.Filename = File.FileName
                        IC2.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass2.Add(IC2)
                    End If
                Next
            End If

            Dim Imgclass3 As List(Of ImageClas) = New List(Of ImageClas)
            Dim IC3 As ImageClas

            If fu4.PostedFiles IsNot Nothing Then
                For Each File In fu4.PostedFiles
                    If File.ContentLength > 0 Then
                        Dim Upload_Time As String = getoffsetdatetime(DateTime.Now).ToString("ddMMyyyyhhmm")
                        Dim filePath As String = Request.PhysicalApplicationPath.ToString & "images\Property_Images\" & Upload_Time & "_" & File.FileName
                        File.SaveAs(filePath)
                        IC3 = New ImageClas()
                        IC3.Filename = File.FileName
                        IC3.FilePath = Upload_Time & "_" & File.FileName
                        Imgclass3.Add(IC3)
                    End If
                Next
            End If

            param(64) = New SqlParameter("@IMAGES", SqlDbType.Structured)
            param(64).Value = UtilityService.ConvertToDataTable(Imgclass)

            param(65) = New SqlParameter("@ENTITY", SqlDbType.VarChar)
            param(65).Value = IIf(ddlentity.SelectedValue = "", DBNull.Value, ddlentity.SelectedValue)

            'Physical Condition

            param(66) = New SqlParameter("@PHYCDTN_WALLS", SqlDbType.VarChar)
            param(66).Value = IIf(txtWalls.Text = "", DBNull.Value, txtWalls.Text)
            param(67) = New SqlParameter("@PHYCDTN_ROOF", SqlDbType.VarChar)
            param(67).Value = IIf(txtRoof.Text = "", DBNull.Value, txtRoof.Text)
            param(68) = New SqlParameter("@PHYCDTN_CEILING", SqlDbType.VarChar)
            param(68).Value = IIf(txtCeiling.Text = "", DBNull.Value, txtCeiling.Text)
            param(69) = New SqlParameter("@PHYCDTN_WINDOWS", SqlDbType.VarChar)
            param(69).Value = IIf(txtwindows.Text = "", DBNull.Value, txtwindows.Text)
            param(70) = New SqlParameter("@PHYCDTN_DAMAGE", SqlDbType.VarChar)
            param(70).Value = IIf(txtDamage.Text = "", DBNull.Value, txtDamage.Text)
            param(71) = New SqlParameter("@PHYCDTN_SEEPAGE", SqlDbType.VarChar)
            param(71).Value = IIf(txtSeepage.Text = "", DBNull.Value, txtSeepage.Text)
            param(72) = New SqlParameter("@PHYCDTN_OTHRTNT", SqlDbType.VarChar)
            param(72).Value = IIf(txtOthrtnt.Text = "", DBNull.Value, txtOthrtnt.Text)
            param(126) = New SqlParameter("@PHYCDTN_OTHRBLDG", SqlDbType.VarChar)
            param(126).Value = IIf(txtBuilding.Text = "", DBNull.Value, txtBuilding.Text)

            'Landlord's Scope of work 
            param(73) = New SqlParameter("@LL_VITRIFIED", SqlDbType.VarChar)
            param(73).Value = IIf(ddlvitrified.SelectedValue = "", DBNull.Value, ddlvitrified.SelectedValue)
            param(74) = New SqlParameter("@LL_VITRIFIED_RMKS", SqlDbType.VarChar)
            param(74).Value = IIf(txtVitriRemarks.Text = "", DBNull.Value, txtVitriRemarks.Text)
            param(75) = New SqlParameter("@LL_WASHRMS", SqlDbType.VarChar)
            param(75).Value = IIf(ddlWashroom.SelectedValue = "", DBNull.Value, ddlWashroom.SelectedValue)
            param(76) = New SqlParameter("@LL_WASHRM_RMKS", SqlDbType.VarChar)
            param(76).Value = IIf(txtWashroom.Text = "", DBNull.Value, txtWashroom.Text)
            param(77) = New SqlParameter("@LL_PANTRY", SqlDbType.VarChar)
            param(77).Value = IIf(ddlPantry.SelectedValue = "", DBNull.Value, ddlPantry.SelectedValue)
            param(78) = New SqlParameter("@LL_PANTRY_RMKS", SqlDbType.VarChar)
            param(78).Value = IIf(txtPantry.Text = "", DBNull.Value, txtPantry.Text)
            param(79) = New SqlParameter("@LL_SHUTTER", SqlDbType.VarChar)
            param(79).Value = IIf(ddlShutter.SelectedValue = "", DBNull.Value, ddlShutter.SelectedValue)
            param(80) = New SqlParameter("@LL_SHUTTER_RMKS", SqlDbType.VarChar)
            param(80).Value = IIf(txtShutter.Text = "", DBNull.Value, txtShutter.Text)
            param(81) = New SqlParameter("@LL_OTHERS", SqlDbType.VarChar)
            param(81).Value = IIf(ddlOthers.SelectedValue = "", DBNull.Value, ddlOthers.SelectedValue)
            param(82) = New SqlParameter("@LL_OTHERS_RMKS", SqlDbType.VarChar)
            param(82).Value = IIf(txtOthers.Text = "", DBNull.Value, txtOthers.Text)
            param(83) = New SqlParameter("@LL_WORK_DAYS", SqlDbType.VarChar)
            param(83).Value = IIf(txtllwork.Text = "", DBNull.Value, txtllwork.Text)
            param(84) = New SqlParameter("@LL_ELEC_EXISTING", SqlDbType.VarChar)
            param(84).Value = IIf(txtElectricEx.Text = "", DBNull.Value, txtElectricEx.Text)
            param(85) = New SqlParameter("@LL_ELEC_REQUIRED", SqlDbType.VarChar)
            param(85).Value = IIf(txtElectricRq.Text = "", DBNull.Value, txtElectricRq.Text)
            'Other Details
            param(89) = New SqlParameter("@OTHR_FLOORING", SqlDbType.VarChar)
            param(89).Value = IIf(ddlFlooring.SelectedValue = "", DBNull.Value, ddlFlooring.SelectedValue)
            param(90) = New SqlParameter("@OTHR_FLRG_RMKS", SqlDbType.VarChar)
            param(90).Value = IIf(txtFlooring.Text = "", DBNull.Value, txtFlooring.Text)
            param(91) = New SqlParameter("@OTHR_WASH_EXISTING", SqlDbType.VarChar)
            param(91).Value = IIf(txtWashExisting.Text = "", DBNull.Value, txtWashExisting.Text)
            param(92) = New SqlParameter("@OTHR_WASH_REQUIRED", SqlDbType.VarChar)
            param(92).Value = IIf(txtWashRequirement.Text = "", DBNull.Value, txtWashRequirement.Text)
            param(93) = New SqlParameter("@OTHR_POTABLE_WTR", SqlDbType.VarChar)
            param(93).Value = IIf(ddlPotableWater.SelectedValue = "", DBNull.Value, ddlPotableWater.SelectedValue)
            param(127) = New SqlParameter("@OTHR_POTABLE_WTR_RMKS", SqlDbType.VarChar)
            param(127).Value = IIf(txtPotableRemarks.Text = "", DBNull.Value, txtPotableRemarks.Text)
            param(128) = New SqlParameter("@OTHR_IT_INSTALL", SqlDbType.VarChar)
            param(128).Value = IIf(txtIT.Text = "", DBNull.Value, txtIT.Text)
            param(129) = New SqlParameter("@OTHR_IT_INSTALL_RMKS", SqlDbType.VarChar)
            param(129).Value = IIf(txtITRemarks.Text = "", DBNull.Value, txtITRemarks.Text)
            param(130) = New SqlParameter("@OTHR_DG_INSTALL", SqlDbType.VarChar)
            param(130).Value = IIf(txtDG.Text = "", DBNull.Value, txtDG.Text)
            param(131) = New SqlParameter("@OTHR_DG_INSTALL_RMKS", SqlDbType.VarChar)
            param(131).Value = IIf(txtDGRemarks.Text = "", DBNull.Value, txtDGRemarks.Text)
            'Cost Details
            param(94) = New SqlParameter("@COST_RENT", SqlDbType.VarChar)
            param(94).Value = IIf(txtRent.Text = "", DBNull.Value, txtRent.Text)
            param(95) = New SqlParameter("@COST_RENT_SFT", SqlDbType.VarChar)
            param(95).Value = IIf(txtsft.Text = "", DBNull.Value, txtsft.Text)
            param(96) = New SqlParameter("@COST_RATIO", SqlDbType.VarChar)
            param(96).Value = IIf(txtRatio.Text = "", DBNull.Value, txtRatio.Text)
            param(97) = New SqlParameter("@COST_OWN_SHARE", SqlDbType.VarChar)
            param(97).Value = IIf(txtownshare.Text = "", DBNull.Value, txtownshare.Text)
            param(98) = New SqlParameter("@COST_SECDEP", SqlDbType.VarChar)
            param(98).Value = IIf(txtsecdepmonths.Text = "", DBNull.Value, txtsecdepmonths.Text)
            param(99) = New SqlParameter("@COST_GST", SqlDbType.VarChar)
            param(99).Value = IIf(ddlgst.SelectedValue = "", DBNull.Value, ddlgst.SelectedValue)
            param(100) = New SqlParameter("@COST_MAINTENANCE", SqlDbType.VarChar)
            param(100).Value = IIf(txtmaintenance.Text = "", DBNull.Value, txtmaintenance.Text)
            param(101) = New SqlParameter("@COST_ESC", SqlDbType.VarChar)
            param(101).Value = IIf(txtesc.Text = "", DBNull.Value, txtesc.Text)
            param(102) = New SqlParameter("@COST_RENT_FREE", SqlDbType.VarChar)
            param(102).Value = IIf(txtRentFree.Text = "", DBNull.Value, txtRentFree.Text)
            param(103) = New SqlParameter("@COST_STAMP", SqlDbType.VarChar)
            param(103).Value = IIf(ddlStampReg.SelectedValue = "", DBNull.Value, ddlStampReg.SelectedValue)
            param(104) = New SqlParameter("@COST_AGREE", SqlDbType.VarChar)
            param(104).Value = IIf(txtAgree.Text = "", DBNull.Value, txtAgree.Text)
            'DOCUMENTS VAILABLE
            param(105) = New SqlParameter("@DOC_TITLE", SqlDbType.VarChar)
            param(105).Value = IIf(ddlTitle.SelectedValue = "", DBNull.Value, ddlTitle.SelectedValue)
            param(106) = New SqlParameter("@DOC_TITLE_RMKS", SqlDbType.VarChar)
            param(106).Value = IIf(txtTitleRemarks.Text = "", DBNull.Value, txtTitleRemarks.Text)
            param(107) = New SqlParameter("@DOC_OCCUP", SqlDbType.VarChar)
            param(107).Value = IIf(ddlOccupancy.SelectedValue = "", DBNull.Value, ddlOccupancy.SelectedValue)
            param(108) = New SqlParameter("@DOC_OCCUP_RMKS", SqlDbType.VarChar)
            param(108).Value = IIf(txtOccupancyRemarks.Text = "", DBNull.Value, txtOccupancyRemarks.Text)
            param(109) = New SqlParameter("@DOC_BUILD", SqlDbType.VarChar)
            param(109).Value = IIf(ddlBuilding.SelectedValue = "", DBNull.Value, ddlBuilding.SelectedValue)
            param(110) = New SqlParameter("@DOC_BUILD_RMKS", SqlDbType.VarChar)
            param(110).Value = IIf(txtBuildingRemarks.Text = "", DBNull.Value, txtBuildingRemarks.Text)
            param(111) = New SqlParameter("@DOC_PAN", SqlDbType.VarChar)
            param(111).Value = IIf(ddlPAN.SelectedValue = "", DBNull.Value, ddlPAN.SelectedValue)
            param(112) = New SqlParameter("@DOC_PAN_RMKS", SqlDbType.VarChar)
            param(112).Value = IIf(txtPANRemarks.Text = "", DBNull.Value, txtPANRemarks.Text)
            param(86) = New SqlParameter("@DOC_TAX", SqlDbType.VarChar)
            param(86).Value = IIf(ddlTax.SelectedValue = "", DBNull.Value, ddlTax.SelectedValue)
            param(87) = New SqlParameter("@DOC_TAX_RMKS", SqlDbType.VarChar)
            param(87).Value = IIf(txtTaxRemarks.Text = "", DBNull.Value, txtTaxRemarks.Text)
            param(88) = New SqlParameter("@DOC_OTHR_INFO", SqlDbType.VarChar)
            param(88).Value = IIf(txtOtherInfo.Text = "", DBNull.Value, txtOtherInfo.Text)

            param(113) = New SqlParameter("@SIGNAGEIMAGES", SqlDbType.Structured)
            param(113).Value = UtilityService.ConvertToDataTable(Imgclass1)
            param(114) = New SqlParameter("@SIG_LENGTH", SqlDbType.VarChar)
            param(114).Value = IIf(txtsiglength.Text = "", DBNull.Value, txtsiglength.Text)
            param(115) = New SqlParameter("@SIG_WIDTH", SqlDbType.VarChar)
            param(115).Value = IIf(txtsigwidth.Text = "", DBNull.Value, txtsigwidth.Text)
            param(116) = New SqlParameter("@AC_OUTDOOR", SqlDbType.VarChar)
            param(116).Value = IIf(txtACOutdoor.Text = "", DBNull.Value, txtACOutdoor.Text)
            param(117) = New SqlParameter("@GSB", SqlDbType.VarChar)
            param(117).Value = IIf(txtGSB.Text = "", DBNull.Value, txtGSB.Text)
            param(118) = New SqlParameter("@AC_OUTDOOR_IMAGE", SqlDbType.Structured)
            param(118).Value = UtilityService.ConvertToDataTable(Imgclass2)
            param(119) = New SqlParameter("@GSB_IMAGE", SqlDbType.Structured)
            param(119).Value = UtilityService.ConvertToDataTable(Imgclass3)
            param(120) = New SqlParameter("@INSPECTION_DATE", SqlDbType.DateTime)
            param(120).Value = IIf(txtsdate.Text = "", DBNull.Value, txtsdate.Text)
            ''param(120).Value = Convert.ToDateTime(txtsdate.Text)
            param(121) = New SqlParameter("@INSPECTION_BY", SqlDbType.VarChar)
            param(121).Value = IIf(txtInspection.Text = "", DBNull.Value, txtInspection.Text)
            param(132) = New SqlParameter("@STATUS", SqlDbType.VarChar)
            param(132).Value = "5019"
            Dim res As String = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[PM_PROPERTY_SAVEASDRAFT_DETAILS]", param)
            If res = "SUCCESS" Then
                lblmsg.Text = "Property Saved Successfully. Request ID - " + txtReqID.Text
            Else
                lblmsg.Text = "Something went wrong; try again"
            End If
            'Dim ds As DataSet = New DataSet("DbDataSet")
            'Dim res As String
            'If hdnReqid.Value = "" Then
            '    res = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[PM_PROPERTY_SAVEASDRAFT_DETAILS]", param)
            '    If res = "SUCCESS" Then
            '        lblmsg.Text = "Property Saved draft Successfully"
            '    Else
            '        lblmsg.Text = "Something went wrong; try again"
            '    End If
            'Else
            '    res = SqlHelper.ExecuteScalar(CommandType.StoredProcedure, "[PM_PROPERTY_SAVEASDRAFT_DETAILS]", param)
            '    If res = "SUCCESS" Then
            '        lblmsg.Text = "Property Saved draft Successfully"
            '    Else
            '        lblmsg.Text = "Something went wrong; try again"
            '    End If
            'End If




            'Cleardata()
            AssigTextValues()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub GetFloorsbyTwr(ByVal tower As String, ByVal cty As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "USP_getFloors_masters")
        sp2.Command.AddParameter("@TWR_ID", tower, DbType.String)
        sp2.Command.AddParameter("@FLR_LOC_ID", ddlLocation.SelectedValue, DbType.String)
        sp2.Command.AddParameter("@FLR_CITY_CODE", cty, DbType.String)
        sp2.Command.AddParameter("@AUR_ID", Session("uid"), DbType.String)
        ddlFloor.DataSource = sp2.GetDataSet()
        ddlFloor.DataTextField = "FLR_NAME"
        ddlFloor.DataValueField = "FLR_CODE"
        ddlFloor.DataBind()
        ddlFloor.Items.Insert(0, New ListItem("--Select Floor--", "0"))
        txtFloor.Text = ddlFloor.Items.Count - 1
    End Sub

    Private Sub GetLocationsbyCity(ByVal city As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_LOCATION_CITY")
        sp2.Command.AddParameter("@CITY", city)
        sp2.Command.AddParameter("@USR_ID", Session("uid"))
        ddlLocation.DataSource = sp2.GetDataSet()
        ddlLocation.DataTextField = "LCM_NAME"
        ddlLocation.DataValueField = "LCM_CODE"
        ddlLocation.DataBind()
        ddlLocation.Items.Insert(0, New ListItem("--Select Location--", "0"))
    End Sub

    Private Sub GetTowerbyLoc(ByVal loc As String)
        Dim sp2 As New SubSonic.StoredProcedure(Session("TENANT") & "." & "GET_TOWERSBYLCMID")
        sp2.Command.AddParameter("@LCMID", loc, DbType.String)
        sp2.Command.AddParameter("@USER_ID", Session("uid"), DbType.String)
        ddlTower.DataSource = sp2.GetDataSet()
        ddlTower.DataTextField = "TWR_NAME"
        ddlTower.DataValueField = "TWR_CODE"
        ddlTower.DataBind()
        ddlTower.Items.Insert(0, New ListItem("--Select Tower--", "0"))
    End Sub



    Protected Sub btnShow_Click(ByVal sender As Object, ByVal e As EventArgs)
        ''TextBox1.Text = "hello"
        ''gvrReqProperties.
        filSaveDraftlReqs()
        mp1.Show()
    End Sub

    Private Sub filSaveDraftlReqs()
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "[PM_GET_PROPERTYSAVEASDRAFTBY_REQS]")
        sp.Command.AddParameter("@AUR_ID", Session("uid").ToString(), DbType.String)
        sp.Command.AddParameter("@CMP_ID", Session("COMPANYID"), DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet
        gvSaveDraftReqs.DataSource = ds
        gvSaveDraftReqs.DataBind()
    End Sub

    Protected Sub gvSaveDraftReqs_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvSaveDraftReqs.PageIndexChanging
        gvSaveDraftReqs.PageIndex = e.NewPageIndex()
        filSaveDraftlReqs()
        mp1.Show()
    End Sub

    Protected Sub gvSaveDraftReqs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvSaveDraftReqs.RowCommand
        Try
            If e.CommandName = "ViewDetails" Then
                '' panel2.Visible = True
                hdnReqid.Value = e.CommandArgument

                BindDetails(hdnReqid.Value)
            Else
                ''panel2.Visible = False
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub

    Private Sub BindDetails(ByVal sno As String)
        Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_GET_PRPTYSAVEASDRAFT_DETAILS")
        sp.Command.AddParameter("@PROP_ID", sno, DbType.String)
        Dim ds As New DataSet
        ds = sp.GetDataSet()
        If ds.Tables(0).Rows.Count > 0 Then
            'General Details

            txtReqID.Text = ds.Tables(0).Rows(0).Item("PM_PPT_PM_REQ_ID")
            ddlReqType.ClearSelection()
            ddlReqType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE")).Selected = True
            'ds.Tables(0).Rows(0).Item("PM_PPT_REQ_TYPE") = ddlReqType.SelectedValue
            ddlPprtNature.ClearSelection()
            ddlPprtNature.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_NATURE")).Selected = True
            ddlAcqThr.ClearSelection()
            ddlAcqThr.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_ACQ_THR")).Selected = True

            ddlCity.ClearSelection()
            ddlCity.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE")).Selected = True
            GetLocationsbyCity(ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
            If ddlReqType.SelectedItem.Text = "New" Then
                If (ds.Tables(0).Rows(0).Item("PM_PPT_TOT_FLRS").ToString() <> "") Then
                    txtnoflrs.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_TOT_FLRS"))
                End If
                txtFloor.Visible = False
                ''txtnoflrs.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")
                ''ddlCity.Visible = False
                If (ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE").ToString() <> "") Then
                    txtlocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")
                    txtlocation.Visible = True
                    RequiredFieldValidator15.Enabled = False
                    ddlLocation.Visible = False
                End If
                'txtlocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")
                'txtlocation.Visible = True
                'ddlLocation.Visible = False
                If (ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE").ToString() <> "") Then
                    txttower.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")
                    txttower.Visible = True
                    ddlTower.Visible = False
                End If
                'txttower.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")
                'txttower.Visible = True
                'ddlTower.Visible = False
                If (ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE").ToString() <> "") Then
                    txtfloors.Text = ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")
                    txtfloors.Visible = True
                    ddlFloor.Visible = False
                    Relocaddress.Visible = False
                    Relocation.Visible = False
                End If
                'txtfloors.Text = ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")
                'txtfloors.Visible = True
                'ddlFloor.Visible = False
                'Relocaddress.Visible = False
                'Relocation.Visible = False
            ElseIf ddlReqType.SelectedItem.Text = "Relocation" Then
                If (ds.Tables(0).Rows(0).Item("PM_PPT_TOT_FLRS").ToString() <> "") Then
                    txtnoflrs.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_TOT_FLRS"))
                End If
                txtFloor.Visible = False


                If ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE").ToString() <> "" Then
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")).Selected = True
                    ''GetTowerbyLoc(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE"))
                    txtlocation.Visible = False
                    RequiredFieldValidator15.Enabled = True
                    ddlLocation.Visible = True
                End If
                'ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")).Selected = True
                'txtlocation.Visible = False
                'ddlLocation.Visible = True
                If (ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE").ToString() <> "") Then
                    txttower.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")
                    txttower.Visible = True
                    ddlTower.Visible = False
                End If
                'txttower.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")
                'txttower.Visible = True
                'ddlTower.Visible = False
                If (ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE").ToString() <> "") Then
                    txtfloors.Text = ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")
                    txtfloors.Visible = True
                    ddlFloor.Visible = False
                    txtrelocadd.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
                    Txtrelocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_RELOC_NAME")
                    Relocaddress.Visible = True
                End If
                'txtfloors.Text = ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")
                'txtfloors.Visible = True
                'ddlFloor.Visible = False
                txtrelocadd.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
                Txtrelocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_RELOC_NAME")
                Relocaddress.Visible = True
                'Relocation.Visible = True
            Else
                If ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE").ToString() <> "" Then
                    ddlLocation.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE")).Selected = True
                    GetTowerbyLoc(ds.Tables(0).Rows(0).Item("PM_PPT_LOC_CODE"))
                    txtlocation.Visible = False
                    RequiredFieldValidator15.Enabled = True
                    ddlLocation.Visible = True
                End If
                If ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE").ToString() <> "" Then
                    ddlTower.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE")).Selected = True

                    GetFloorsbyTwr(ds.Tables(0).Rows(0).Item("PM_PPT_TOW_CODE"), ds.Tables(0).Rows(0).Item("PM_PPT_CTY_CODE"))
                    txttower.Visible = False
                    ddlTower.Visible = True
                End If
                If ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE").ToString() <> "" Then
                    ddlFloor.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_flr_CODE")).Selected = True
                    txtfloors.Visible = False
                    ddlFloor.Visible = True
                End If
                txtnoflrs.Visible = False
            End If

            If (ds.Tables(0).Rows(0).Item("pm_ppt_entity").ToString() = "") Then
                ddlentity.SelectedValue = ""
            Else
                ddlentity.ClearSelection()
                ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("pm_ppt_entity")).Selected = True
            End If
            'If Not ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("pm_ppt_entity")).Selected = True Then
            '    ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("pm_ppt_entity")).Selected = True
            'End If
            'ddlentity.Items.FindByValue(ds.Tables(0).Rows(0).Item("pm_ppt_entity")).Selected = True
            txtToilet.Text = ds.Tables(0).Rows(0).Item("PM_PPT_TOT_TOI")  'IIf(txtToilet.Text = "", 0, txtToilet.Text)
            ddlPropertyType.SelectedValue = ds.Tables(0).Rows(0).Item("PM_PPT_TYPE")
            txtPropIDName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_NAME")

            txtESTD.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_ESTD"))  'IIf(txtESTD.Text = "", "", Convert.ToString(txtESTD.Text))
            txtAge.Text = ds.Tables(0).Rows(0).Item("PM_PPT_AGE")   'IIf(txtAge.Text = "", "", Convert.ToString(txtAge.Text))
            txtPropDesc.Text = ds.Tables(0).Rows(0).Item("PM_PPT_ADDRESS")
            txtSignageLocation.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SIGN_LOC")

            txtSocityName.Text = ds.Tables(0).Rows(0).Item("PM_PPT_SOC_NAME")
            txtlat.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LAT")
            txtlong.Text = ds.Tables(0).Rows(0).Item("PM_PPT_LONG")
            txtOwnScopeWork.Text = ds.Tables(0).Rows(0).Item("PM_PPT_OWN_SCOPE")
            ddlRecommended.ClearSelection()
            ddlRecommended.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED") = True, 1, 0)
            'ddlRecommended.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_RECOMMENDED")).Selected = True
            If ddlRecommended.SelectedValue = 1 Then
                txtRecmRemarks.Visible = True
                txtRecmRemarks.Text = ds.Tables(0).Rows(0).Item("PM_PPT_RECO_REM")
            Else
                divRecommanded.Visible = False
            End If

            'Owner Details
            txtownrname.Text = ds.Tables(0).Rows(0).Item("PM_OWN_NAME")
            txtphno.Text = ds.Tables(0).Rows(0).Item("PM_OWN_PH_NO")
            txtPrvOwnName.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_NAME")
            txtPrvOwnPhNo.Text = ds.Tables(0).Rows(0).Item("PM_PREV_OWN_PH_NO")
            txtOwnEmail.Text = ds.Tables(0).Rows(0).Item("PM_OWN_EMAIL")
            txtownadd.Text = ds.Tables(0).Rows(0).Item("PM_OWN_ADDRESS")

            'Area Details
            txtCarpetArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_CARPET_AREA"))
            txtBuiltupArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_BUA_AREA"))
            txtCommonArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_COM_AREA"))
            txtRentableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_RENT_AREA"))

            txtUsableArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_USABEL_AREA"))
            txtSuperBulArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_SBU_AREA"))
            txtPlotArea.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_PLOT_AREA"))
            txtCeilingHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTC_HIGHT"))

            txtBeamBottomHight.Text = Convert.ToDecimal(ds.Tables(0).Rows(0).Item("PM_AR_FTBB_HIGHT"))
            txtMaxCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_MAX_CAP"))
            txtOptCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_OPT_CAP"))
            txtSeatingCapacity.Text = Convert.ToInt32(ds.Tables(0).Rows(0).Item("PM_AR_SEATING_CAP"))

            ddlFlooringType.ClearSelection()
            ddlFlooringType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_AR_FLOOR_TYPE")).Selected = True
            ddlFSI.SelectedValue = IIf(ds.Tables(0).Rows(0).Item("PM_AR_FSI") = True, 1, 0)
            txtFSI.Text = ds.Tables(0).Rows(0).Item("PM_AR_FSI_RATIO")
            txtEfficiency.Text = ds.Tables(0).Rows(0).Item("PM_AR_PREF_EFF")

            'PURCHASE DETAILS
            txtPurPrice.Text = ds.Tables(0).Rows(0).Item("PM_PUR_PRICE")
            txtPurDate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PUR_DATE"))
            txtMarketValue.Text = ds.Tables(0).Rows(0).Item("PM_PUR_MARKET_VALUE")

            'GOVT DETAILS
            txtIRDA.Text = ds.Tables(0).Rows(0).Item("PM_GOV_IRDA")
            txtPCcode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PC_CODE")
            txtGovtPropCode.Text = ds.Tables(0).Rows(0).Item("PM_GOV_PROP_CODE")
            txtUOM_CODE.Text = ds.Tables(0).Rows(0).Item("PM_GOV_UOM_CODE")

            'INSURANCE DETAILS
            ddlInsuranceType.ClearSelection()
            ddlInsuranceType.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_INS_TYPE")).Selected = True
            txtInsuranceVendor.Text = ds.Tables(0).Rows(0).Item("PM_INS_VENDOR")
            txtInsuranceAmt.Text = ds.Tables(0).Rows(0).Item("PM_INS_AMOUNT")
            txtInsurancePolNum.Text = ds.Tables(0).Rows(0).Item("PM_INS_PNO")

            txtInsuranceStartdate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_INS_START_DT"))
            txtInsuranceEnddate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_INS_END_DT"))
            'txtentity.Text = ds.Tables(0).Rows(0).Item("CHE_NAME")
            If (ds.Tables(0).Rows(0).Item("PM_OWN_LANDLINE").ToString() <> "") Then
                txtllno.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OWN_LANDLINE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_POA").ToString() = "") Then
                ddlAgreementbyPOA.SelectedValue = ""
            Else
                ddlAgreementbyPOA.ClearSelection()
                ddlAgreementbyPOA.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_PPT_POA")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_BY").ToString() <> "") Then
                txtInspection.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_BY"))
            End If
            ''txtInspection.Text = ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_BY")
            txtsdate.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_INSPECTED_DT"))
            'physical condition
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WALLS").ToString() <> "") Then
                txtWalls.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WALLS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_ROOF").ToString() <> "") Then
                txtRoof.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_ROOF"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_CEILING").ToString() <> "") Then
                txtCeiling.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_CEILING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WINDOWS").ToString() <> "") Then
                txtwindows.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_WINDOWS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_DAMAGE").ToString() <> "") Then
                txtDamage.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_DAMAGE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_SEEPAGE").ToString() <> "") Then
                txtSeepage.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_SEEPAGE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PHYCDTN_OTHR_TNT").ToString() <> "") Then
                txtOthrtnt.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PHYCDTN_OTHR_TNT"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_BLDG_NAME_ADDRESS").ToString() <> "") Then
                txtBuilding.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_BLDG_NAME_ADDRESS"))
            End If


            'LANDLORD SCOPE OF WORK
            If (ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED").ToString() = "") Then
                ddlvitrified.SelectedValue = ""
            Else
                ddlvitrified.ClearSelection()
                ddlvitrified.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED_RMKS").ToString() <> "") Then
                txtVitriRemarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED_RMKS")
            End If
            ''txtVitriRemarks.Text = ds.Tables(0).Rows(0).Item("PM_LLS_VITRIFIED_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_WASHROOMS").ToString() = "") Then
                ddlWashroom.SelectedValue = ""
            Else
                ddlWashroom.ClearSelection()
                ddlWashroom.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_WASHROOMS")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_WASH_RMKS").ToString() <> "") Then
                txtWashroom.Text = ds.Tables(0).Rows(0).Item("PM_LLS_WASH_RMKS")
            End If
            'txtWashroom.Text = ds.Tables(0).Rows(0).Item("PM_LLS_WASH_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY").ToString() = "") Then
                ddlPantry.SelectedValue = ""
            Else
                ddlPantry.ClearSelection()
                ddlPantry.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY_RMKS").ToString() <> "") Then
                txtPantry.Text = ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY_RMKS")
            End If
            ''txtPantry.Text = ds.Tables(0).Rows(0).Item("PM_LLS_PANTRY_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER").ToString() = "") Then
                ddlShutter.SelectedValue = ""
            Else
                ddlShutter.ClearSelection()
                ddlShutter.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER_RMKS").ToString() <> "") Then
                txtShutter.Text = ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER_RMKS")
            End If
            ''txtShutter.Text = ds.Tables(0).Rows(0).Item("PM_LLS_SHUTTER_RMKS")
            If (ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS").ToString() = "") Then
                ddlOthers.SelectedValue = ""
            Else
                ddlOthers.ClearSelection()
                ddlOthers.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS_RMKS").ToString() <> "") Then
                txtOthers.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_OTHERS_RMKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_WORK_DAYS").ToString() <> "") Then
                txtllwork.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_WORK_DAYS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_EXISTING").ToString() <> "") Then
                txtElectricEx.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_EXISTING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_REQUIRED").ToString() <> "") Then
                txtElectricRq.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_LLS_ELEC_REQUIRED"))
            End If
            'COST DETAILS
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT").ToString() <> "") Then
                txtRent.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT_SFT").ToString() <> "") Then
                txtsft.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT_SFT"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RATIO").ToString() <> "") Then
                txtRatio.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RATIO"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_OWN_SHARE").ToString() <> "") Then
                txtownshare.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_OWN_SHARE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_SECDEPOSIT").ToString() <> "") Then
                txtsecdepmonths.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_SECDEPOSIT"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_COST_GST").ToString() = "") Then
                ddlgst.SelectedValue = ""
            Else
                ddlgst.ClearSelection()
                ddlgst.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_COST_GST")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_MAINTENANCE").ToString() <> "") Then
                txtmaintenance.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_MAINTENANCE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_ESC_RENTALS").ToString() <> "") Then
                txtesc.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_ESC_RENTALS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_RENT_FREE").ToString() <> "") Then
                txtRentFree.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_RENT_FREE"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_STAMP").ToString() = "") Then
                ddlStampReg.SelectedValue = ""
            Else
                ddlStampReg.ClearSelection()
                ddlStampReg.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_COST_STAMP")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_COST_AGREEMENT_PERIOD").ToString() <> "") Then
                txtAgree.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_COST_AGREEMENT_PERIOD"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_FLOORING").ToString() = "") Then
                ddlFlooring.SelectedValue = ""
            Else
                ddlFlooring.ClearSelection()
                ddlFlooring.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_OTHR_FLOORING")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_FLRG_RMKS").ToString() <> "") Then
                txtFlooring.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_FLRG_RMKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_EXISTING").ToString() <> "") Then
                txtWashExisting.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_EXISTING"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_REQUIRED").ToString() <> "") Then
                txtWashRequirement.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_WASHRM_REQUIRED"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_POTABLE_WTR").ToString() = "") Then
                ddlPotableWater.SelectedValue = ""
            Else
                ddlPotableWater.ClearSelection()
                ddlPotableWater.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_OTHR_POTABLE_WTR")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_POTABLE_WTR_REMARKS").ToString() <> "") Then
                txtPotableRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_POTABLE_WTR_REMARKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_IT_INSTALL").ToString() <> "") Then
                txtIT.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_IT_INSTALL"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_IT_INSTALL_RMKS").ToString() <> "") Then
                txtITRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_IT_INSTALL_RMKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_DG_INSTALL").ToString() <> "") Then
                txtDG.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_DG_INSTALL"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_OTHR_DG_INSTALL_RMKS").ToString() <> "") Then
                txtDGRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_OTHR_DG_INSTALL_RMKS"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_SIG_LENGTH").ToString() <> "") Then
                txtsiglength.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_SIG_LENGTH"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_PPT_SIG_WIDTH").ToString() <> "") Then
                txtsigwidth.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_SIG_WIDTH"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_AC_OUTDOOR").ToString() <> "") Then
                txtACOutdoor.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_AC_OUTDOOR"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_PPT_GSB").ToString() <> "") Then
                txtGSB.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_PPT_GSB"))
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TITLE").ToString() = "") Then
                ddlTitle.SelectedValue = ""
            Else
                ddlTitle.ClearSelection()
                ddlTitle.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_TITLE")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TITLLE_RMKS").ToString() <> "") Then
                txtTitleRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_TITLLE_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCCUPANCY").ToString() = "") Then
                ddlOccupancy.SelectedValue = ""
            Else
                ddlOccupancy.ClearSelection()
                ddlOccupancy.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_OCCUPANCY")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS").ToString() <> "") Then
                txtOccupancyRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_BUILD").ToString() = "") Then
                ddlBuilding.SelectedValue = ""
            Else
                ddlBuilding.ClearSelection()
                ddlBuilding.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_BUILD")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_OCC_RMKS").ToString() <> "") Then
                txtBuildingRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_BUILD_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_PAN").ToString() = "") Then
                ddlPAN.SelectedValue = ""
            Else
                ddlPAN.ClearSelection()
                ddlPAN.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_PAN")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_PAN_RMKS").ToString() <> "") Then
                txtPANRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_PAN_RMKS"))
            End If


            If (ds.Tables(0).Rows(0).Item("PM_DOC_TAX").ToString() = "") Then
                ddlTax.SelectedValue = ""
            Else
                ddlTax.ClearSelection()
                ddlTax.Items.FindByValue(ds.Tables(0).Rows(0).Item("PM_DOC_TAX")).Selected = True
            End If
            If (ds.Tables(0).Rows(0).Item("PM_DOC_TAX_RMKS").ToString() <> "") Then
                txtTaxRemarks.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_TAX_RMKS"))
            End If

            If (ds.Tables(0).Rows(0).Item("PM_DOC_OTHER_INFO").ToString() <> "") Then
                txtOtherInfo.Text = Convert.ToString(ds.Tables(0).Rows(0).Item("PM_DOC_OTHER_INFO"))
            End If

            '  If ds.Tables(1).Rows.Count > 0 Then
            gvPropdocs.DataSource = ds.Tables(1)
            gvPropdocs.DataBind()
            'End If

        End If
    End Sub
    Protected Sub gvPropdocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvPropdocs.RowCommand
        Try
            If e.CommandName = "Download" Then
                Response.ContentType = ContentType
                Dim imgPath As String = "../../images/Property_Images/" + e.CommandArgument
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(imgPath)))
                Response.WriteFile(imgPath)
                Response.End()
            Else
                'DELETE
                Dim gvr As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)
                'gvPropdocs.DeleteRow(gvr.RowIndex)
                gvPropdocs.Rows(gvr.RowIndex).Visible = False
                Dim sp As New SubSonic.StoredProcedure(Session("TENANT") & "." & "PM_DELETE_DOCS")
                sp.Command.AddParameter("@DOC_ID", e.CommandArgument, DbType.String)
                sp.Command.AddParameter("@TYPE", "PROPERTY", DbType.String)
                sp.ExecuteScalar()
            End If
        Catch ex As Exception
            Response.Write(ex.Message)
        End Try
    End Sub
    Protected Sub gvPropdocs_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvPropdocs.RowDeleting

    End Sub

End Class
