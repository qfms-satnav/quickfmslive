﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SubLeaseApproveDetails.aspx.cs" Inherits="WorkSpace_SMS_Webfiles_SubLeaseApproveDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <%-- Works only in IE, Loads the page without any flickering on dropdon selected index change --%>
    <%--<meta http-equiv="Page-Enter" content="Alpha(opacity=99)">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0)">--%>

    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>

    <link href="../../BlurScripts/BlurCss/vendor-3bab2c9961.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/app-b2b3cfd0e7.css" rel="stylesheet" />
    <link href="../../BlurScripts/BlurCss/NonAngularScript.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <script type="text/javascript">
        function setup(id) {
            $('#' + id).datepicker({

                format: 'mm/dd/yyyy',
                autoclose: true

            });
        };
    </script>
    <style>
        /*body {
            color: #6a6c6f;
            background-color: #f1f3f6;
            margin-top: 30px;
        }*/

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>
    <style type="text/css">
        .table-striped {
            height: 161px;
        }
    </style>
</head>
<body>
    <div class="animsition">
        <div class="al-content">
            <div class="widgets">
                <div ba-panel ba-panel-title="Sub Lease Agreement" ba-panel-class="with-scroll">
                    <div class="panel">
                        <div class="panel-heading" style="height: 41px;">
                            <h3 class="panel-title">Approve / Reject Sub Lease Agreement
                                <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                                <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            </h3>
                        </div>
                        <div class="panel-body" style="padding-right: 10px;">
                            <form id="form1" runat="server">
                                <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" ValidationGroup="Val1" DisplayMode="List" />

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <asp:Label ID="lblMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                                </asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:ValidationSummary ID="ValidationSummary2" runat="server" ShowSummary="true" ShowValidationErrors="true" ForeColor="Red" ValidationGroup="Val2" DisplayMode="List" />
                                <div id="panel1" runat="Server">
                                    <div class="panel panel-default " role="tab" runat="server" id="div0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-multiselectable="true">Lease Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseOne" class="panel-collapse collapse in">
                                            <div class="panel-body color">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Lease Id</label>
                                                            <asp:TextBox ID="txtleaseid" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Property Code</label>
                                                            <asp:TextBox ID="txtPropCd" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Property Name</label>
                                                            <asp:TextBox ID="txtPropName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Country</label>
                                                            <asp:TextBox ID="txtCountry" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>City</label>
                                                            <asp:TextBox ID="txtCity" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Location</label>
                                                            <asp:TextBox ID="txtLocation" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Lease Start Date</label>
                                                            <asp:TextBox ID="txtLseStrDt" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Lease End Date</label>
                                                            <asp:TextBox ID="txtLseEndDt" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Sub Lease Status</label>
                                                            <asp:TextBox ID="txtsublesstatus" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default " role="tab" runat="server" id="div1">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Area & Cost Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseTwo" class="panel-collapse collapse in">
                                            <div class="panel-body color">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>CTS Number</label>
                                                            <asp:TextBox ID="txtLnumber" runat="server" CssClass="form-control" MaxLength="50"
                                                                TabIndex="2" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Entitled Lease Amount</label>
                                                            <asp:TextBox ID="txtentitle" runat="server" CssClass="form-control" TabIndex="12" Enabled="false">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Basic Rent</label>
                                                            <asp:TextBox ID="txtBasicRent" runat="server" CssClass="form-control" MaxLength="15" TabIndex="13" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Security Deposit</label>
                                                            <asp:TextBox ID="txtSD" runat="server" CssClass="form-control" TabIndex="15" MaxLength="26" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Security Deposited Months</label>
                                                            <asp:TextBox ID="txtSDMonths" runat="server" CssClass="form-control" TabIndex="15"
                                                                MaxLength="26" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Rent Free Period</label>
                                                            <asp:TextBox ID="txtRentFreePeriod" runat="server" CssClass="form-control" MaxLength="50"
                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Carpet Area (Sqft)</label>
                                                            <asp:TextBox ID="txtCarArea" runat="server" CssClass="form-control" MaxLength="50"
                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Rent Per Sqft (On Carpet)</label>
                                                            <asp:TextBox ID="txtRentPerSqftCarpet" runat="server" CssClass="form-control" MaxLength="50"
                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Built Up Area (Sqft)</label>
                                                            <asp:TextBox ID="txtBUA" runat="server" CssClass="form-control" MaxLength="50"
                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Rent Per Sqft (On BUA)</label>
                                                            <asp:TextBox ID="txtRentPerSqftBUA" runat="server" CssClass="form-control" MaxLength="50"
                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Interior Cost (Approx)</label>
                                                            <asp:TextBox ID="txtInteriorCost" runat="server" CssClass="form-control" MaxLength="50"
                                                                TabIndex="4" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default " role="tab" runat="server" id="div2">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">Agreement Details</a>
                                            </h4>
                                        </div>
                                        <div id="collapseThree" class="panel-collapse collapse in">
                                            <div class="panel-body color">
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Sub Group</label>
                                                            <asp:TextBox ID="txtSubGroup" runat="server" CssClass="form-control" data-live-search="true" Enabled="false">
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Agreement Start Date</label>
                                                            <div class='input-group date' id='fromdate'>
                                                                <asp:TextBox ID="AgreeStartDate" runat="server" CssClass="form-control" Enabled="false"> </asp:TextBox>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Agreement End Date</label>
                                                            <div class='input-group date' id='todate'>
                                                                <asp:TextBox ID="AgreeEndDate" runat="server" CssClass="form-control" Enabled="false"> </asp:TextBox>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('todate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Remaining Area(Sqft) / No. of Seating</label>
                                                            <asp:TextBox ID="txtRemain" TextMode="Number" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Area(Sqft) / No. of Seating</label>
                                                            <asp:TextBox ID="txtSeat" TextMode="Number" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Cost</label>
                                                            <asp:TextBox ID="txtCost" TextMode="Number" runat="server" CssClass="form-control" Enabled="false">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Maintenance Charges</label>
                                                            <asp:TextBox ID="txtMaintChrg" TextMode="Number" runat="server" CssClass="form-control" Enabled="false">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Total Cost</label>
                                                            <asp:TextBox ID="txtTotal" TextMode="Number" runat="server" CssClass="form-control" Enabled="false">0</asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Security Deposit</label>
                                                            <asp:TextBox ID="txtsecdpst" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">                                                               
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Payment Terms</label>
                                                            <asp:TextBox ID="txtPayTerm" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">                                                               
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Payment Date</label>
                                                            <div class='input-group date' id='PayDate'>
                                                                <asp:TextBox ID="PaymentDate" runat="server" CssClass="form-control" Enabled="false"> </asp:TextBox>
                                                                <span class="input-group-addon">
                                                                    <span class="fa fa-calendar" onclick="setup('PayDate')"></span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Payment Mode</label>
                                                            <asp:TextBox ID="txtPayType" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">                                                               
                                                            </asp:TextBox>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="panel2" runat="Server">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Account / Serial Number</label>
                                                                <asp:TextBox ID="txtAccNo" runat="server" CssClass="form-control" Enabled="false">0</asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Bank Name</label>
                                                                <asp:TextBox ID="txtBankName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="panel3" runat="Server" visible="false">
                                                    <div class="row">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Account / Serial Number</label>
                                                                <asp:TextBox ID="txtAccTwo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Bank Name</label>
                                                                <asp:TextBox ID="txtBankTwo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label>Branch Name</label>
                                                                    <asp:TextBox ID="txtbrnch" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 col-sm-12 col-xs-12">
                                                                <div class="form-group">
                                                                    <label>IFSC Code</label>
                                                                    <asp:TextBox ID="txtIFSC" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Cost Type On<span style="color: red;">*</span></label>
                                                            <asp:RequiredFieldValidator ID="rfvrblCostType" runat="server" ControlToValidate="rblCostType"  ValidationGroup="Val1"
                                                                ErrorMessage="Please Select Cost Type On"></asp:RequiredFieldValidator>
                                                            <asp:RadioButtonList ID="rblCostType" runat="server" RepeatDirection="Horizontal" Enabled="false">
                                                                <asp:ListItem Value="Sqft" Text="Sq.ft" />
                                                                <asp:ListItem Value="Seat" Text="Seat Wise" />
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                    <div id="Costype1" runat="server" >
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Rent Per Sqft (On Carpet)<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtSubCarpet" runat="server" ControlToValidate="txtSubCarpet"  ValidationGroup="Val1"
                                                                    ErrorMessage="Please Enter Rent Per Sq.ft (On Carpet)"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftbetxtSubCarpet" runat="server" TargetControlID="txtSubCarpet" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtSubCarpet" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8" Enabled="false">.</asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Rent Per Sqft (On BUA)<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtSubBUA" runat="server" ControlToValidate="txtSubBUA"  ValidationGroup="Val1"
                                                                    ErrorMessage="Please Enter Rent Per Sq.ft (On BUA)"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtSubBUA" runat="server" TargetControlID="txtSubBUA" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtSubBUA" runat="server" CssClass="form-control" MaxLength="50" TabIndex="9" Enabled="false">.</asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="Costype2" runat="server" visible="false">
                                                        <div class="col-md-3 col-sm-12 col-xs-12">
                                                            <div class="form-group">
                                                                <label>Seat Cost<span style="color: red;">*</span></label>
                                                                <asp:RequiredFieldValidator ID="rfvtxtSeatCost" runat="server" ControlToValidate="txtSeatCost" Display="None" ValidationGroup="Val1"
                                                                    ErrorMessage="Please Enter Seat Cost"></asp:RequiredFieldValidator>
                                                                <cc1:FilteredTextBoxExtender ID="ftetxtSeatCost" runat="server" TargetControlID="txtSeatCost" FilterType="Numbers" ValidChars="0123456789." />
                                                                <asp:TextBox ID="txtSeatCost" runat="server" CssClass="form-control" MaxLength="50" TabIndex="8" Enabled="false">.</asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label>Approver Remarks</label>
                                                            <div onmouseover="Tip('Enter Asset Description')" onmouseout="UnTip()">
                                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 col-sm-12 col-xs-12" style="padding-top: 17px">
                                                        <div class="form-group">
                                                            <asp:Button ID="btnapprove" runat="server" CssClass="btn btn-default btn-primary" CausesValidation="true"
                                                                Text="Approve" TabIndex="5" ValidationGroup="Val1" OnClick="btnapprove_Click" />
                                                           
                                                            <asp:Button ID="btnreject" runat="server" CssClass="btn btn-default btn-primary" CausesValidation="true"
                                                                Text="Reject" TabIndex="6" OnClick="btnreject_Click" />
                                                            <asp:Button ID="btnback" runat="server" CssClass="btn btn-default btn-primary" CausesValidation="true"
                                                                Text="Back" TabIndex="7" OnClick="btnback_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
</body>
</html>
