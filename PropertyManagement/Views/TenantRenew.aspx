﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TenantRenew.aspx.vb" Inherits="WorkSpace_SMS_Webfiles_TenantRenew" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Facility Management Services::a-mantra</title>
    <%=ScriptCombiner.GetScriptTags("css_scripts", "css", 1)%>
    <link href='/RCAP/RCAP/fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css' />

    <!--[if lt IE 9]>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/html5shiv.js")%>'></script>
        <script src='<%= ResolveUrl("~/BootStrapCSS/Scripts/respond.min.js")%>'></script>
    <![endif]-->
    <%-- <style>
        body {
            color: #6a6c6f;
            background-color: #f1f3f6;
            margin-top: 30px;
        }

        .container {
            max-width: 960px;
        }

        .collapsed {
            width: 100%;
        }

        .panel-default > .panel-heading {
            color: #333;
            background-color: #fff;
            border-color: #e4e5e7;
            padding: 0;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

            .panel-default > .panel-heading a {
                display: block;
                padding: 10px 15px;
            }

                .panel-default > .panel-heading a:after {
                    content: "";
                    position: relative;
                    top: 1px;
                    display: inline-block;
                    font-family: 'Glyphicons Halflings';
                    font-style: normal;
                    font-weight: 400;
                    line-height: 1;
                    -webkit-font-smoothing: antialiased;
                    -moz-osx-font-smoothing: grayscale;
                    float: right;
                    transition: transform .25s linear;
                    -webkit-transition: -webkit-transform .25s linear;
                }

                .panel-default > .panel-heading a[aria-expanded="true"] {
                    background-color: #eee;
                }

                    .panel-default > .panel-heading a[aria-expanded="true"]:after {
                        content: "\2212";
                        -webkit-transform: rotate(180deg);
                        transform: rotate(180deg);
                    }

                .panel-default > .panel-heading a[aria-expanded="false"]:after {
                    content: "\002b";
                    -webkit-transform: rotate(90deg);
                    transform: rotate(90deg);
                }

        .panel-heading {
            height: 38px;
        }

        .accordion-option {
            width: 100%;
            float: left;
            clear: both;
            margin: 15px 0;
        }

            .accordion-option .title {
                font-size: 20px;
                font-weight: bold;
                float: left;
                padding: 0;
                margin: 0;
            }

            .accordion-option .toggle-accordion {
                float: right;
                font-size: 16px;
                color: #6a6c6f;
            }

                .accordion-option .toggle-accordion:before {
                    content: "Expand All";
                }

                .accordion-option .toggle-accordion.active:before {
                    content: "Collapse All";
                }
    </style>--%>
</head>
<script lang="javascript" type="text/javascript">
    function setup(id) {
        $('.date').datepicker({
            format: 'mm/dd/yyyy',
            autoclose: true
        });
    };
</script>
<body>
    <div class="widgets" style="background-color: white">
        <div class="row">
            <div class="col-md-6">
                <asp:Label ID="lblmsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                </asp:Label>
            </div>
        </div>
        <div class="panel">
            <h3 class="panel-title">
                <a href="#" class="btn btn-default closeall pull-right" style="padding-bottom: initial;" title="Collapse All"><i class="fa fa-minus" aria-hidden="true"></i></a>
                <a href="#" class="btn btn-default openall pull-right" style="padding-bottom: initial;" title="Expand All"><i class="fa fa-plus" aria-hidden="true"></i></a>
            </h3>
        </div>
        <br />
    </div>
    <div class="card">
        <form id="form1" runat="server">
            <asp:ScriptManager ID="sc1" runat="server"></asp:ScriptManager>
            <%--<asp:updatepanel runat="server">
                                      <ContentTemplate>--%>
            <div id="divTermsDates" runat="server">
                <div class="panel panel-default " role="tab" runat="server" id="div0">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-multiselectable="true">Property Details</a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="collapse show">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Property Type <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvproptype" runat="server" ControlToValidate="ddlproptype"
                                            Display="None" ErrorMessage="Please Select Property Type" ValidationGroup="Val1"
                                            InitialValue="0"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlproptype" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">City<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvcity" runat="server" ControlToValidate="ddlCity"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select City" InitialValue="0"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Location <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ControlToValidate="ddlLocation"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Location" InitialValue="0"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlLocation" runat="server" CssClass="form-control selectpicker" data-live-search="true" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Property<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="cvbuilding" runat="server" ControlToValidate="ddlBuilding"
                                            Display="None" ErrorMessage="Please Select Property" ValidationGroup="Val1"
                                            InitialValue="0"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlBuilding" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tenant Occupied Area<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfarea" runat="server" ControlToValidate="txtTenantOccupiedArea"
                                            Display="None" ErrorMessage="Please enter Tenant Occupied Area" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Tenant Occupied Area in numbers upto 2 decimal places.')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtTenantOccupiedArea" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Subject of Agreement</label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtSubject" TextMode="MultiLine" Height="30%" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Purpose of Agreement</label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtPurpose" runat="server" Height="30%" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Enter No Of Months To Renew</label>
                                        <div class="col-md-12">
                                            <asp:RequiredFieldValidator ID="rfvTR" runat="server" ControlToValidate="txtrenew"
                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Enter No Of Months To Renew"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="revTR" ValidationGroup="Val1" runat="server"
                                                Display="none" ControlToValidate="txtrenew" ErrorMessage="Please Enter No Of Months To Renew"
                                                ValidationExpression="^[0-9]*$"></asp:RegularExpressionValidator>
                                            <asp:TextBox ID="txtrenew" AutoPostBack="true" runat="server" ValidationGroup="Val1" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tenant From Date  <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTFromdt" runat="server" ControlToValidate="txtTenFromDt"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Lease From Date"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div class='input-group date' id='tenFromDt'>
                                                <asp:TextBox ID="txtTenFromDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('tenFromDt')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tenant End Date  <span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvTEnddt" runat="server" ControlToValidate="txtTenEndDt"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Joining Date"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div class='input-group date' id='tenEndDt'>
                                                <asp:TextBox ID="txtTenEndDt" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('tenEndDt')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="panel panel-default " role="tab" runat="server" id="div1">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-multiselectable="true">Tenant Details</a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="collapse show">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tenant<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="cvuser" runat="server" ControlToValidate="ddluser"
                                            Display="None" ErrorMessage="Please Select User" ValidationGroup="Val1" InitialValue="0"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddluser" runat="server" CssClass="form-control selectpicker" data-live-search="true" Enabled="false">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tenant Code</label>
                                        <%-- <asp:RequiredFieldValidator ID="rfvtcode" runat="server" ControlToValidate="txttcode"
                                                                Display="None" ErrorMessage="Please Enter Tenant Code" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txttcode" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Parking Spaces</label>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtNoofparking" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label>Upload Files/Images</label>
                                        <div class="btn btn-primary btn-mm">
                                            <i class="fa fa-folder-open-o fa-lg"></i>
                                            <asp:FileUpload ID="fu1" runat="Server" Width="90%" AllowMultiple="True" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <strong>Documents</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <asp:Label ID="lblDocsMsg" runat="server" CssClass="col-md-12 control-label" ForeColor="Red">
                                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-group">
                                            <div id="tblGridDocs" runat="server">
                                                <asp:DataGrid ID="grdDocs" runat="server" CssClass="table GridStyle" GridLines="none" DataKeyField="PM_RT_DOC_SNO"
                                                    EmptyDataText="No Documents Found." AutoGenerateColumns="False" PageSize="5">
                                                    <Columns>
                                                        <asp:BoundColumn Visible="False" DataField="PM_RT_DOC_SNO" HeaderText="ID"></asp:BoundColumn>
                                                        <asp:BoundColumn DataField="PM_RT_DOC_PATH" HeaderText="Document Name">
                                                            <HeaderStyle></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:BoundColumn DataField="PM_LDOC_CREATED_DT" HeaderText="Document Date">
                                                            <HeaderStyle></HeaderStyle>
                                                        </asp:BoundColumn>
                                                        <asp:ButtonColumn Text="Download" CommandName="Download">
                                                            <HeaderStyle></HeaderStyle>
                                                        </asp:ButtonColumn>
                                                        <asp:ButtonColumn Text="Delete" CommandName="Delete">
                                                            <HeaderStyle></HeaderStyle>
                                                        </asp:ButtonColumn>
                                                    </Columns>
                                                    <HeaderStyle ForeColor="white" BackColor="Black" />
                                                    <PagerStyle CssClass="pagination-ys" NextPageText="Next" PrevPageText="Previous" Position="TopAndBottom"></PagerStyle>
                                                </asp:DataGrid>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="panel panel-default " role="tab" runat="server" id="div2">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-multiselectable="true">Payment Details</a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="collapse show">
                        <div class="panel-body color">
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Tenant Rent<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfRent" runat="server" ControlToValidate="txtRent"
                                            Display="None" ErrorMessage="Please enter Tenant Rent" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter tenant rent in numbers upto 2 decimal places.')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtRent" runat="server" AutoPostBack="true" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Security Deposit</label>
                                        <%--   <asp:RequiredFieldValidator ID="rfdeposit" runat="server" ControlToValidate="txtSecurityDeposit"
                                                                Display="None" ErrorMessage="Please enter Security Deposit" ValidationGroup="Val1"></asp:RequiredFieldValidator>--%>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Security Deposit only in numbers upto 2 decimal places.')"
                                                onmouseout="UnTip()">
                                                <asp:TextBox ID="txtSecurityDeposit" runat="server" CssClass="form-control"
                                                    MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">Joining Date</label>
                                        <%--  <asp:RequiredFieldValidator ID="rfvDate" runat="server" ControlToValidate="txtDate"
                                                                ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Joining Date"></asp:RequiredFieldValidator>--%>
                                        <div class="col-md-12">
                                            <div class='input-group date' id='fromdate'>
                                                <asp:TextBox ID="txtDate" runat="server" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon">
                                                    <span class="fa fa-calendar" onclick="setup('fromdate')"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Payment Terms<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="CompareValidator1" runat="server" ControlToValidate="ddlPaymentTerms"
                                            ValidationGroup="Val1" Display="None" ErrorMessage="Please Select Payment Terms"
                                            InitialValue="--Select Payment Terms--"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlPaymentTerms" runat="server" CssClass="form-control selectpicker" data-live-search="true">
                                                <asp:ListItem>--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Additional Car Parking Fee</label>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Maintenance Fees in Numericals.')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtAddnFee" runat="server" AutoPostBack="true" CssClass="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Society Maintenance / CAM</label>
                                        <asp:RequiredFieldValidator ID="rfvfees" runat="server" ControlToValidate="txtfees"
                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please enter Maintenance Fees"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter Maintenance Fees in numbers upto 2 decimal places.')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtfees" runat="server" AutoPostBack="true" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Outstanding amount<span style="color: red;">*</span></label>
                                        <asp:RequiredFieldValidator ID="rfvamt" runat="server" ControlToValidate="txtamount"
                                            Display="None" ValidationGroup="Val1" ErrorMessage="Please enter Outstanding amount"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revamt" Display="none" runat="server" ControlToValidate="txtamount"
                                            ErrorMessage="Please enter Valid Outstanding amount in Numbers or Decimal Number with 2 Decimal Places" ValidationExpression="((\d+)((\.\d{1,2})?))$"></asp:RegularExpressionValidator>
                                        <div class="col-md-12">
                                            <div onmouseover="Tip('Enter numbers with maximum length 20 only')" onmouseout="UnTip()">
                                                <asp:TextBox ID="txtamount" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="form-group">
                                        <label class="col-md-12 control-label">Remarks</label>
                                        <asp:RequiredFieldValidator ID="rfvRemarks" runat="server" ControlToValidate="txtRemarks"
                                            Display="None" ErrorMessage="Please Enter Remarks" ValidationGroup="Val1"></asp:RequiredFieldValidator>
                                        <div class="col-md-12">
                                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="form-control" Height="30%" TextMode="MultiLine" MaxLength="500"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default " role="tab" runat="server" id="div4">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">Tenant Reminder</a>
                        </h4>
                    </div>
                    <div id="collapseFour" class="collapse show">
                        <div class="panel-body color">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="col-md-12 control-label">Tenant Reminders For</label>
                                    <div class="col-md-12" style="width: 500px;">
                                        <div class="bootstrap-tagsinput">
                                            <asp:CheckBoxList ID="TntRem" runat="server" CellPadding="25" CellSpacing="100" RepeatColumns="6" RepeatLayout="Table" RepeatDirection="Vertical" CausesValidation="True">
                                                <asp:ListItem Value="10">10 Days</asp:ListItem>
                                                <asp:ListItem Value="3">3 Days</asp:ListItem>
                                                <asp:ListItem Value="7">7 Days</asp:ListItem>
                                                <asp:ListItem Value="2">2 Days</asp:ListItem>
                                                <asp:ListItem Value="5">5 Days</asp:ListItem>
                                                <asp:ListItem Value="1">1 Day</asp:ListItem>
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br />
                <div class="col-md-12 text-right" style="padding-top: 5px">
                    <asp:Button ID="btnApproval" CssClass="btn btn-primary custom-button-color" runat="server" Text="Approve" />
                    <asp:Button ID="btnRejection" CssClass="btn btn-primary custom-button-color" runat="server" Text="Reject" />
                    <asp:Button ID="btnSubmit" CssClass="btn btn-primary custom-button-color" runat="server" Text="Submit" ValidationGroup="Val1" />
                </div>
            </div>
            <%-- </ContentTemplate>
                                     </asp:updatepanel>--%>
        </form>
    </div>
    <%= ScriptCombiner.GetScriptTags("js_Scripts", "js", 1)%>
    <script>
        $('.closeall').click(function () {
            $('.panel-collapse.in')
                .collapse('hide');
        });
        $('.openall').click(function () {
            $('.panel-collapse:not(".in")')
                .collapse('show');
        });
    </script>
</body>
</html>
<script>
    function refreshSelectpicker() {
        $("#<%=ddlBuilding.ClientID%>").selectpicker();
        $("#<%=ddlCity.ClientID%>").selectpicker();
        $("#<%=ddlLocation.ClientID%>").selectpicker();
        $("#<%=ddlPaymentTerms.ClientID%>").selectpicker();
        $("#<%=ddlproptype.ClientID%>").selectpicker();
        $("#<%=ddluser.ClientID%>").selectpicker();

    }
    refreshSelectpicker();


</script>
